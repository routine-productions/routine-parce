<?php
class Modx_Gallery_Albums extends Illuminate\Database\Eloquent\Model
{
    public $timestamps = false;
    protected $table = "modx_gallery_albums";
}