<?php

define('__HOMEDIR__', __DIR__);

require_once __DIR__ . '/../core.php';

//Setup part
$allFiles = [];
$allLinks = [];

//$items_amount = 10;
$items_amount = 439;

$last_iteration = include __DIR__ . '/results/last_iteration.php';
$pages_parsed = $last_iteration[0];
$last_count = $last_iteration[1];

$counter = $last_count;


echo '<br> Last page ';

print_r($pages_parsed);
echo '<br> Last item parsed ';
print_r($last_count);

echo '<br> Pages to parse ';
print_r($items_amount);

$allreadyFetched = include __DIR__ . '/results/all_disks_outer.php';


echo '<br> Total items before parse ';
print_r(count($last_count));

//Setup part ends

//Execution part start

for ($i = $pages_parsed; $i < $items_amount; $i++) {

    echo '<h1> Parsing page ';
    echo $i + 1;
    echo '</h1>';

    domRetrieve($i, $Dom);


    $pages_parsed++;

}
echo '<br>';

echo '<br> Total items after parse ';
print_r(count($counter));


//Execution part ends


//Common part

function domRetrieve($i, $Dom)
{

    global $counter;
    global $pages_parsed;
    global $allreadyFetched;

    $curDom = $Dom->loadFromUrl('https://catalog.api.onliner.by/search/wheel?page=' . ($i + 1) . '');

    $taskList = json_decode($curDom, TRUE);// Декодировать в массив



    $fetched_products = array_values($taskList['products']);

    for ($i = 0; $i < count($fetched_products); $i++) {

        echo ' ';
        print_r(fetched_products[$i]);
        echo ' ';

        array_push($allreadyFetched, $fetched_products[$i]);

        $counter++;
    }

    file_put_contents(__DIR__ . '/results/last_iteration.php', "<?php \n    return " . var_export([$pages_parsed, $counter], true) . ";");
    file_put_contents(__DIR__ . '/results/all_disks_outer.php', "<?php \n    return " . var_export($allreadyFetched, true) . ";");

    unset($curDom);
    unset($taskList);
    unset($fetched_products);

}


