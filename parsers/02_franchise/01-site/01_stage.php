<?php
/*
 * Get categories
 */
define('__HOMEDIR__', __DIR__);
require_once __DIR__ . '/../../core.php';

// Load: Page with categories
$Dom->loadFromURL('http://topfranchise.ru/catalog');

// Set: Titles and links
$Categories = [];


foreach ($Dom->find('.submenu_page > li') as $Category_Key => $Category)
{
    $Categories[$Category_Key] = [
        'title' => $Category->find('> a')->text,
        'href' => $Category->find('> a')->href
    ];

    foreach ($Category->find('.wrapper a') as $Sub_Category_Key => $Sub_Category)
    {
        $Categories[$Category_Key]['children'][$Sub_Category_Key] = [
            'title' => $Sub_Category->text,
            'href' => $Sub_Category->href,
            'parent_id' => $Category_Key,
        ];
    }
}

// Fix: Clear first common category
unset($Categories[0]);

// Write: To file
file_put_contents(__DIR__ . '/results/01_stage.php', "<?php \n    return " . var_export($Categories, true) . ";");