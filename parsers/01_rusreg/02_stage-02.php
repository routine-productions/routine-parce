<?php
/*
 * Get: Persons
 */
define('__HOMEDIR__', __DIR__);
error_reporting(E_ALL);
$Start = microtime(true);

require_once __DIR__ . '/../core.php';

// Init: Excel parsing
$objPHPExcel = new PHPExcel();
$objPHPExcel->setActiveSheetIndex(0);

// Set: Active sheet
$Sheet = $objPHPExcel->getActiveSheet();

// Write: Headers
$Sheet->setCellValue('A1', 'fio');
$Sheet->setCellValue('B1', 'phone_number');
$Sheet->setCellValue('C1', 'postcode');
$Sheet->setCellValue('D1', 'mail');
$Sheet->setCellValue('E1', 'SNILS');
$Sheet->setCellValue('F1', 'certificate');
$Sheet->setCellValue('G1', 'Registry_number');
$Sheet->setCellValue('H1', 'Действие лицензии аттестата');
$Sheet->setCellValue('I1', 'Сокращенное наименование');
$Sheet->setCellValue('J1', 'Номер в Гос. реестре СРО');
$Sheet->setCellValue('k1', 'Дата вступления в СРО');
$Sheet->setCellValue('L1', 'Дата выхода из СРО');
$Sheet->setCellValue('M1', 'Дейст. СРО');


// Get: Person links
$Links = require __DIR__ . '/results/01_pages-02.php';

// Load: Persons
foreach ($Links as $Link_Key => $Link)
{
    // Load: Page
    $Page = $Dom->loadFromUrl('https://rosreestr.ru' . $Link);
    // Init: Arrays
    $Sam = [];
    $Con = [];
    $img = [];
    $Con_Textes = [];

    $Link_Key = $Link_Key + 2;

    // Parse: Page content
    $Contents = $Page->find('.brdw1000bg tr.h24t');

    foreach ($Contents as $Item)
    {
        if (isset($Item->find('td')[1]))
        {
            $Con[] = trim(html_entity_decode(strip_tags(($Item->find('td')[1]->innerHtml))));
            $img[] = $Item->find('td')[1]->find('img')[0]->src;
        } else
        {
            $Con[] = '';
            $img[] = '';
        }

        $Con_Textes[] = trim(html_entity_decode(strip_tags($Item->find('td')[0]->innerHtml)));
    }

    if ($img[5] == '/wps/PA_PUBFCCLRKIionstage/images/common/objects/denied.gif')
    {
        $image = 'Нет';
    } else
    {
        $image = 'Да';
    }

    if ($Con_Textes[6] == 'Предыдущий аттестат')
    {
        $Con[6] = $Con[7];
    }

    if ($Page = $Page->find('.block1')[1])
    {
        if ($Page = $Page->find('tr')[1])
        {
            $Samo_Uprav = $Page->find('td');
            foreach ($Samo_Uprav as $Item)
            {
                $Sam[] = trim(html_entity_decode(strip_tags($Item->text)));
            }
        }
    }

    $Sheet->setCellValue('A' . $Link_Key, html_entity_decode($Con[0]));
    $Sheet->setCellValue('B' . $Link_Key, html_entity_decode($Con[1]));
    $Sheet->setCellValue('C' . $Link_Key, html_entity_decode($Con[2]));
    $Sheet->setCellValue('D' . $Link_Key, html_entity_decode($Con[3]));
    $Sheet->setCellValue('E' . $Link_Key, html_entity_decode($Con[4]));
    $Sheet->setCellValue('F' . $Link_Key, html_entity_decode($Con[5]));
    $Sheet->setCellValue('G' . $Link_Key, html_entity_decode($Con[6]));
    $Sheet->setCellValue('H' . $Link_Key, html_entity_decode($image));
    $Sheet->setCellValue('I' . $Link_Key, !empty($Sam[0]) ? $Sam[0] : ' ');
    $Sheet->setCellValue('J' . $Link_Key, !empty($Sam[1]) ? $Sam[1] : ' ');
    $Sheet->setCellValue('K' . $Link_Key, !empty($Sam[2]) ? $Sam[2] : ' ');
    $Sheet->setCellValue('L' . $Link_Key, !empty($Sam[3]) ? $Sam[3] : ' ');
    $Sheet->setCellValue('M' . $Link_Key, !empty($Sam[4]) ? $Sam[4] : ' ');

    echo $Link_Key . ' ';
}

// Write: To XML File
$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
$objWriter->save(__DIR__ . "/results/02_persons-02.xlsx");

echo ' ' . (microtime(true) - $Start);
