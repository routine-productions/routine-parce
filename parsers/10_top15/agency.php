<?php

define('__HOMEDIR__', __DIR__);

require_once __DIR__ . '/../core.php';


$Dom->loadFromUrl('http://top15moscow.ru/partners/');
$agency = [];

foreach ($Dom->find('.partners-list-box') as $key => $partner)
{

    $photo = $partner->find('img')->src;
    $photo = explode('/', $photo);

    //remove domain from link
    unset($photo[0]);
    unset($photo[1]);
    unset($photo[2]);

    $photo = '/' . implode('/', $photo);

    Save_File($photo, 'http://top15moscow.ru');
    $agency[$key]['photo'] = $photo;
    $agency[$key]['title'] = $partner->find('.partners-list-name h3')->text;
}


// Write: To file

file_put_contents(__DIR__ . '/results/agency.php', "<?php \n    return " . var_export($agency, true) . ";");
