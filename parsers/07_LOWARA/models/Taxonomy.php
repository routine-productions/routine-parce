<?php

class Taxonomy extends Illuminate\Database\Eloquent\Model
{
    public $timestamps = false;
    protected $table = "wp_term_taxonomy";

}