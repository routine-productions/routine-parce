<?php 
    return array (
  'dekoratory' => 
  array (
    0 => 
    array (
      'href' => 'http://top15moscow.ru/dekoratory/mariya-german/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2017/03/IMG_d.jpg',
      'name' => 'Мария',
      'surname' => 'Герман',
      'alias' => 'mariya-german',
    ),
    1 => 
    array (
      'href' => 'http://top15moscow.ru/dekoratory/dina-yakushina/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/08/16.07.27-top15Moscow-453.jpg',
      'name' => 'Дина',
      'surname' => 'Якушина',
      'alias' => 'dina-yakushina',
    ),
    2 => 
    array (
      'href' => 'http://top15moscow.ru/dekoratory/flowers-lovers/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/08/CZ1V2775.jpg',
      'name' => 'FlowersLovers',
      'alias' => 'flowers-lovers',
    ),
    3 => 
    array (
      'href' => 'http://top15moscow.ru/dekoratory/mezhdu-nami/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/08/UUS_1081.jpg',
      'name' => 'Студия',
      'surname' => '«Между',
      'alias' => 'mezhdu-nami',
    ),
    4 => 
    array (
      'href' => 'http://top15moscow.ru/dekoratory/flowerbazar/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/08/321546546.jpg',
      'name' => 'Flowerbazar',
      'alias' => 'flowerbazar',
    ),
    5 => 
    array (
      'href' => 'http://top15moscow.ru/dekoratory/late-dekor/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/08/CZ1V2864.jpg',
      'name' => 'Latte',
      'surname' => 'Décor',
      'alias' => 'late-dekor',
    ),
    6 => 
    array (
      'href' => 'http://top15moscow.ru/dekoratory/mariya-kamenskaya/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/08/16.07.27-top15Moscow-725.jpg',
      'name' => 'Мария',
      'surname' => 'Каменская',
      'alias' => 'mariya-kamenskaya',
    ),
    7 => 
    array (
      'href' => 'http://top15moscow.ru/dekoratory/kovalishin/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/08/16.07.27-top15Moscow-525.jpg',
      'name' => 'Роман',
      'surname' => 'Ковалишин',
      'alias' => 'kovalishin',
    ),
    8 => 
    array (
      'href' => 'http://top15moscow.ru/dekoratory/shakirova-julia/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/08/16.07.28-Top15-Moscow-130.jpg',
      'name' => 'Юлия',
      'surname' => 'Шакирова',
      'alias' => 'shakirova-julia',
    ),
    9 => 
    array (
      'href' => 'http://top15moscow.ru/dekoratory/tamara-valenkova/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/08/16.07.27-top15Moscow-108.jpg',
      'name' => 'Тамара',
      'surname' => 'Валенкова',
      'alias' => 'tamara-valenkova',
    ),
    10 => 
    array (
      'href' => 'http://top15moscow.ru/dekoratory/gennadij-samohin/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/08/16.07.28-Top15-Moscow-140.jpg',
      'name' => 'Геннадий',
      'surname' => 'Самохин',
      'alias' => 'gennadij-samohin',
    ),
    11 => 
    array (
      'href' => 'http://top15moscow.ru/dekoratory/antizerskij/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/UUS_0656.jpg',
      'name' => 'Юрий',
      'surname' => 'Антизерский',
      'alias' => 'antizerskij',
    ),
  ),
  'fotografy' => 
  array (
    0 => 
    array (
      'href' => 'http://top15moscow.ru/fotografy/katya-romanova/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/08/Fotograf-Katya-Romanova.jpg',
      'name' => 'Катя',
      'surname' => 'Романова',
      'alias' => 'katya-romanova',
    ),
    1 => 
    array (
      'href' => 'http://top15moscow.ru/fotografy/igor-bulgak/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/08/Fotograf-Igor-Bulgak.jpg',
      'name' => 'Игорь',
      'surname' => 'Булгак',
      'alias' => 'igor-bulgak',
    ),
    2 => 
    array (
      'href' => 'http://top15moscow.ru/fotografy/maksim-koliberdin/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/Fotograph-Maksim-Koliberdin.jpg',
      'name' => 'Максим',
      'surname' => 'Колибердин',
      'alias' => 'maksim-koliberdin',
    ),
    3 => 
    array (
      'href' => 'http://top15moscow.ru/fotografy/katya-mukhina/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/Fotograph-Katya-Muhina.jpg',
      'name' => 'Катя',
      'surname' => 'Мухина',
      'alias' => 'katya-mukhina',
    ),
    4 => 
    array (
      'href' => 'http://top15moscow.ru/fotografy/aleksej-kinyapin/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/fotograf-Aleksej-Kinyapin.jpg',
      'name' => 'Алексей',
      'surname' => 'Киняпин',
      'alias' => 'aleksej-kinyapin',
    ),
    5 => 
    array (
      'href' => 'http://top15moscow.ru/fotografy/marina_fadeeva/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/Fotograph-Marina-Fadeeva.jpg',
      'name' => 'Марина',
      'surname' => 'Фадеева',
      'alias' => 'marina_fadeeva',
    ),
    6 => 
    array (
      'href' => 'http://top15moscow.ru/fotografy/dmitrij-markov/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/160711-top15photographers-1610.jpg',
      'name' => 'Дмитрий',
      'surname' => 'Марков',
      'alias' => 'dmitrij-markov',
    ),
    7 => 
    array (
      'href' => 'http://top15moscow.ru/fotografy/natalya-legenda/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/08/160711-top15photographers-047.jpg',
      'name' => 'Наталья',
      'surname' => 'Легенда',
      'alias' => 'natalya-legenda',
    ),
    8 => 
    array (
      'href' => 'http://top15moscow.ru/fotografy/sergej-zaporozhets/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/016-4.jpg',
      'name' => 'Сергей',
      'surname' => 'Запорожец',
      'alias' => 'sergej-zaporozhets',
    ),
    9 => 
    array (
      'href' => 'http://top15moscow.ru/fotografy/andrej-nastasenko/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/16.07.27-top15Moscow-6161.jpg',
      'name' => 'Андрей',
      'surname' => 'Настасенко',
      'alias' => 'andrej-nastasenko',
    ),
    10 => 
    array (
      'href' => 'http://top15moscow.ru/fotografy/liliya-gorlanova/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/160711-top15photographers-824.jpg',
      'name' => 'Лилия',
      'surname' => 'Горланова',
      'alias' => 'liliya-gorlanova',
    ),
    11 => 
    array (
      'href' => 'http://top15moscow.ru/fotografy/mihail-harlamov/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/160711-top15photographers-1531.jpg',
      'name' => 'Михаил',
      'surname' => 'Харламов',
      'alias' => 'mihail-harlamov',
    ),
    12 => 
    array (
      'href' => 'http://top15moscow.ru/fotografy/galina-nabatnikova/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/16.07.10-top15photographers-579.jpg',
      'name' => 'Галина',
      'surname' => 'Набатникова',
      'alias' => 'galina-nabatnikova',
    ),
    13 => 
    array (
      'href' => 'http://top15moscow.ru/fotografy/konstantin-semenihin/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/16.07.10-top15photographers-814.jpg',
      'name' => 'Константин',
      'surname' => 'Семенихин',
      'alias' => 'konstantin-semenihin',
    ),
    14 => 
    array (
      'href' => 'http://top15moscow.ru/fotografy/andrej-bajda/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/16.07.10-top15photographers-970.jpg',
      'name' => 'Андрей',
      'surname' => 'Байда',
      'alias' => 'andrej-bajda',
    ),
  ),
  'vedushie' => 
  array (
    0 => 
    array (
      'href' => 'http://top15moscow.ru/vedushie/leonid-margolin/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2017/04/Kopiya-Leonid-Margolin-2.jpg',
      'name' => 'Леонид',
      'surname' => 'Марголин',
      'alias' => 'leonid-margolin',
    ),
    1 => 
    array (
      'href' => 'http://top15moscow.ru/vedushie/aleksandr-belov/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2017/04/Kopiya-Aleksandr-Belov-2.jpg',
      'name' => 'Александр',
      'surname' => 'Белов',
      'alias' => 'aleksandr-belov',
    ),
    2 => 
    array (
      'href' => 'http://top15moscow.ru/vedushie/oleg-savelyev/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2015/03/Kopiya-Oleg-Savelev.jpg',
      'name' => 'Олег',
      'surname' => 'Савельев',
      'alias' => 'oleg-savelyev',
    ),
    3 => 
    array (
      'href' => 'http://top15moscow.ru/vedushie/oleg-beletskiy/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2015/03/Kopiya-Oleg-Beletskij.jpg',
      'name' => 'Олег',
      'surname' => 'Белецкий',
      'alias' => 'oleg-beletskiy',
    ),
    4 => 
    array (
      'href' => 'http://top15moscow.ru/vedushie/mihail-belyanin/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2015/03/Kopiya-Mihail-Belyanin.jpg',
      'name' => 'Михаил',
      'surname' => 'Белянин',
      'alias' => 'mihail-belyanin',
    ),
    5 => 
    array (
      'href' => 'http://top15moscow.ru/vedushie/maksim-markevich/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2015/03/Kopiya-Maksim-Markevich.jpg',
      'name' => 'Максим',
      'surname' => 'Маркевич',
      'alias' => 'maksim-markevich',
    ),
    6 => 
    array (
      'href' => 'http://top15moscow.ru/vedushie/konstantin-anisimov/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2015/03/Kopiya-Konstantin-Anisimov.jpg',
      'name' => 'Константин',
      'surname' => 'Анисимов',
      'alias' => 'konstantin-anisimov',
    ),
    7 => 
    array (
      'href' => 'http://top15moscow.ru/vedushie/egor-pirogov/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2015/03/Kopiya-Egor-Pirogov.jpg',
      'name' => 'Егор',
      'surname' => 'Пирогов',
      'alias' => 'egor-pirogov',
    ),
    8 => 
    array (
      'href' => 'http://top15moscow.ru/vedushie/dmitriy-babaev/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2015/03/Kopiya-Dmitrij-Babaev.jpg',
      'name' => 'Дмитрий',
      'surname' => 'Бабаев',
      'alias' => 'dmitriy-babaev',
    ),
    9 => 
    array (
      'href' => 'http://top15moscow.ru/vedushie/anton-abuchkaev/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2015/03/Kopiya-Anton-Abuchkaev.jpg',
      'name' => 'Антон',
      'surname' => 'Абучкаев',
      'alias' => 'anton-abuchkaev',
    ),
    10 => 
    array (
      'href' => 'http://top15moscow.ru/vedushie/adis-mammo/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2015/03/Kopiya-Adis-Mammo.jpg',
      'name' => 'Адис',
      'surname' => 'Маммо',
      'alias' => 'adis-mammo',
    ),
    11 => 
    array (
      'href' => 'http://top15moscow.ru/vedushie/aleksandr-slesarev/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2015/03/Kopiya-Aleksandr-Slesarev.jpg',
      'name' => 'Александр',
      'surname' => 'Слесарев',
      'alias' => 'aleksandr-slesarev',
    ),
    12 => 
    array (
      'href' => 'http://top15moscow.ru/vedushie/maksim-kritskiy/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2015/03/Kopiya-Maksim-Kritskij.jpg',
      'name' => 'Максим',
      'surname' => 'Крицкий',
      'alias' => 'maksim-kritskiy',
    ),
    13 => 
    array (
      'href' => 'http://top15moscow.ru/vedushie/roman-klyachkin/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2015/03/Kopiya-Roman-Klyachkin.jpg',
      'name' => 'Роман',
      'surname' => 'Клячкин',
      'alias' => 'roman-klyachkin',
    ),
    14 => 
    array (
      'href' => 'http://top15moscow.ru/vedushie/andrey-predelin/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2015/03/Kopiya-Andrej-Predelin.jpg',
      'name' => 'Андрей',
      'surname' => 'Пределин',
      'alias' => 'andrey-predelin',
    ),
  ),
  'videografy' => 
  array (
    0 => 
    array (
      'href' => 'http://top15moscow.ru/videografy/miras-film/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2017/08/portretGENA2680-1-1.jpg',
      'name' => 'Miras',
      'surname' => 'Film',
      'alias' => 'miras-film',
    ),
    1 => 
    array (
      'href' => 'http://top15moscow.ru/videografy/jenevastudio/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2017/04/Vildan-H-1600-2.jpg',
      'name' => 'Jenevastudio',
      'alias' => 'jenevastudio',
    ),
    2 => 
    array (
      'href' => 'http://top15moscow.ru/videografy/studiya-art-island/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2017/04/160711_top15photographers-1505.jpg',
      'name' => 'Студия',
      'surname' => '«Art',
      'alias' => 'studiya-art-island',
    ),
    3 => 
    array (
      'href' => 'http://top15moscow.ru/videografy/roman_khlustov',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2017/02/523A5348-Edit-Edit-2.jpg',
      'name' => 'Роман',
      'surname' => 'Хлюстов',
      'alias' => 'roman_khlustov',
    ),
    4 => 
    array (
      'href' => 'http://top15moscow.ru/videografy/vitalij-sidorenko/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/16.07.10-top15photographers-7152.jpg',
      'name' => 'Виталий',
      'surname' => 'Сидоренко',
      'alias' => 'vitalij-sidorenko',
    ),
    5 => 
    array (
      'href' => 'http://top15moscow.ru/videografy/sergej-novikov/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/160711-top15photographers-563.jpg',
      'name' => 'Сергей',
      'surname' => 'Новиков',
      'alias' => 'sergej-novikov',
    ),
    6 => 
    array (
      'href' => 'http://top15moscow.ru/videografy/artem-korzhavin/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/160711-top15photographers-379.jpg',
      'name' => 'Артем',
      'surname' => 'Коржавин',
      'alias' => 'artem-korzhavin',
    ),
    7 => 
    array (
      'href' => 'http://top15moscow.ru/videografy/yulya-vopilova/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/160711-top15photographers-880.jpg',
      'name' => 'Юля',
      'surname' => 'Вопилова',
      'alias' => 'yulya-vopilova',
    ),
    8 => 
    array (
      'href' => 'http://top15moscow.ru/videografy/family-frames/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/160711-top15photographers-1387.jpg',
      'name' => 'Павел',
      'surname' => 'Трепов',
      'alias' => 'family-frames',
    ),
    9 => 
    array (
      'href' => 'http://top15moscow.ru/videografy/ivan-zalevskij/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/020-3.jpg',
      'name' => 'Иван',
      'surname' => 'Залевский',
      'alias' => 'ivan-zalevskij',
    ),
    10 => 
    array (
      'href' => 'http://top15moscow.ru/videografy/artem-korchagin/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/16.07.10-top15photographers-308.jpg',
      'name' => 'Артем',
      'surname' => 'Корчагин',
      'alias' => 'artem-korchagin',
    ),
    11 => 
    array (
      'href' => 'http://top15moscow.ru/videografy/sergej-andreev/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/160711-top15photographers-288.jpg',
      'name' => 'Сергей',
      'surname' => 'Андреев',
      'alias' => 'sergej-andreev',
    ),
    12 => 
    array (
      'href' => 'http://top15moscow.ru/videografy/maksim-molchanov/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/022-2.jpg',
      'name' => 'Максим',
      'surname' => 'Молчанов',
      'alias' => 'maksim-molchanov',
    ),
    13 => 
    array (
      'href' => 'http://top15moscow.ru/videografy/mihail-levchuk/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/160711-top15photographers-1122.jpg',
      'name' => 'Михаил',
      'surname' => 'Левчук',
      'alias' => 'mihail-levchuk',
    ),
    14 => 
    array (
      'href' => 'http://top15moscow.ru/videografy/artisland/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/160711-top15photographers-1466.jpg',
      'name' => 'Студия',
      'surname' => '«Art',
      'alias' => 'artisland',
    ),
  ),
);