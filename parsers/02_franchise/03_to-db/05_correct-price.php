<?php
/*
 * Create: Pages 01
 */
define('__HOMEDIR__', __DIR__);
require_once __DIR__ . '/../../core.php';

$Data = Modx_Site_Tmplvar_Contentvalues::where('tmplvarid', 25)->get();

$Pages = require_once __DIR__ . '/../02_franchise-02/results/03_pages.php';

foreach ($Pages as $Page_Key => $Page)
{

    if (isset($Page['invest_price']))
    {
        echo $Page['invest_price'] . "\n";
        $Extra_Fields = new Modx_Site_Tmplvar_Contentvalues();
        $Extra_Fields->tmplvarid = 25;
        $Extra_Fields->contentid = 5000 + ($Page_Key + 1);;
        $Extra_Fields->value = $Page['invest_price'];
        $Extra_Fields->save();
    }
}