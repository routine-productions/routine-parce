<?php

define('__HOMEDIR__', __DIR__);

require_once __DIR__ . '/../core.php';
    $Links = [];

for ($i = 0; $i < 4; $i++)
{
    if ($i === 0)
    {
        $enter_data = require __DIR__ . '/enter_data/dekoratory.php';
        $profession = 'dekoratory';
    }
    elseif ($i === 1)
    {
        $enter_data = require __DIR__ . '/enter_data/fotografy.php';
        $profession = 'fotografy';
    }
    elseif ($i === 2)
    {
        $enter_data = require __DIR__ . '/enter_data/vedushie.php';
        $profession = 'vedushie';
    }
    elseif ($i === 3)
    {
        $enter_data = require __DIR__ . '/enter_data/videografy.php';
        $profession = 'videografy';
    }


    $Dom->load($enter_data);


    foreach ($Dom->find('.item') as $key => $item)
    {

        $Links[$profession][$key]['href'] = $item->find('a')->href;
        $Links[$profession][$key]['img'] = $item->find('img')->src;
        $Links[$profession][$key]['name'] = explode(' ', $item->find('.name span')->text)[0];

        if (isset(explode(' ', $item->find('.name span')->text)[1]) && !empty(explode(' ', $item->find('.name span')->text)[1]))
        {
            $Links[$profession][$key]['surname'] = explode(' ', $item->find('.name span')->text)[1];
        }

        $Links[$profession][$key]['alias'] = explode('/', $item->find('a')->href)[4];
    }

    // Write: To file
}
file_put_contents(__DIR__ . '/results/01_subscribers.php', "<?php \n    return " . var_export($Links, true) . ";");
