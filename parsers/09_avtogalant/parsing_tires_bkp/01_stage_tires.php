<?php

define('__HOMEDIR__', __DIR__);

require_once __DIR__ . '/../core.php';

//Setup part

$allFiles = [];
$allLinks = [];


$items_amount = 1278;

$last_iteration = include __DIR__ . '/results/last_iteration.php';
$last_amount = $last_iteration[0];
$last_count = $last_iteration[1];

$counter = $last_count;


echo '<br> Last page ';

print_r($last_amount);
echo '<br> Last item parsed ';
print_r($last_count);

echo '<br> Pages to parse ';
print_r($items_amount);

$allreadyFetched = include __DIR__ . '/results/all_tires_outer.php';
$allreadyLinked  = include __DIR__ . '/results/all_tires_inner_links.php';

//print_r($allreadyFetched);


echo '<br> Total items before parse ';
print_r(count($allreadyLinked));

//Setup part ends

//Execution part start

for ($i = $last_amount; $i < $items_amount; $i++) {

    echo '<h1> Parsing page ';
    echo $i + 1;
    echo '</h1>';

    domRetrieve($i,$Dom);

}
echo '<br>';

echo '<br> Total items after parse ';
print_r(count($allreadyLinked));


file_put_contents(__DIR__ . '/results/last_iteration.php', "<?php \n    return " . var_export([$items_amount,$counter], true) . ";");
file_put_contents(__DIR__ . '/results/all_tires_outer.php', "<?php \n    return " . var_export($allreadyFetched, true) . ";");
file_put_contents(__DIR__ . '/results/all_tires_inner_links.php', "<?php \n    return " . var_export($allreadyLinked, true) . ";");

//file_put_contents(__DIR__ . 'results/all_disks_outer.php', "<?php \n    return " . var_export($allFiles, true) . ";");
//file_put_contents(__DIR__ . 'results/all_disks_inner_links.php', "<?php \n    return " . var_export($allLinks, true) . ";");


//Execution part ends


//Common part

function domRetrieve($i,$Dom){

    global $allFiles;
    global $allLinks;
    global $counter;
    global $allreadyFetched;
    global $allreadyLinked;

    $curDom =  $Dom->loadFromUrl('https://catalog.api.onliner.by/search/tires?page=' . ($i + 1) . '');
//    $curDom =  $Dom->loadFromUrl('https://catalog.api.onliner.by/search/wheel?page=' . ($i + 1) . '');

//    file_put_contents(__DIR__ . '/results/data.json', $curDom);  // Перекодировать в формат и записать в файл.
//    $file = file_get_contents(__DIR__ . '/results/data.json');  // Открыть файл data.json
    $taskList = json_decode($curDom, TRUE);        // Декодировать в массив

    $fetched_products = array_values($taskList['products']);

    for ($i = 0; $i < count($fetched_products); $i++) {

        echo ' ';
        print_r($counter);
        echo ' ';

//        print_r($fetched_products[$i]);
//        print_r($fetched_products[$i]['html_url']);


        $allreadyFetched[$counter] =  $fetched_products[$i];
//        $allFiles[$counter] = $fetched_products[$i];
        $allreadyLinked[$counter] = $fetched_products[$i]['html_url'];

//        $allLinks[$counter] = $fetched_products[$i]['html_url'];


        $counter++;
    }


    unset($curDom);
    unset($file);
    unset($products);
    unset($taskList);
    unset($fetched_products);
}


