<?php
/*
 * Get: Person links
 */
define('__HOMEDIR__', __DIR__);
require_once __DIR__ . '/../core.php';

$Links = [];

// Get: All pages
for ($Page_Number = 1; $Page_Number < 3917; $Page_Number++)
{
    $Dom->loadFromUrl('https://rosreestr.ru/wps/portal/p/cc_ib_portal_services/ais_rki/!ut/p/c5/hY5ND4IwGIN_kXk7BeZ1oo5N-VBJhF3INISQ8OHBmPDvRb14Qdtj2zwlQ6M7-6gre6_7zjaUkfEKMFewwIGSqxgQmzRKHR0xgI957hVK6iTYSQZ52APKX59YIjiQ4M9ak6ma_jJyzi_yVzcOFy6E8FMd8uMcS-eT_yK99xMSoCjo25JyMnzyUexR3pSVvQ50a7PhpGbbJxrKI8M!/dl3/d3/L0lJSklna21BL0lKakFBQ3lBQkVSQ0pBISEvNEZHZ3NvMFZ2emE5SUFnIS83XzAxNUExSDQwSUdCTzAwQUVUTlQ0Sk4xME82LzhwSVVQNDQ1MTAzNTM!/?PC_7_015A1H40IGBO00AETNT4JN10O6000000_ru.fccland.ibmportal.spring.portlet.handler.BeanNameParameterHandlerMapping-PATH=%2fregister%2fsearch&search.currentPage=' . $i);

    // Get: All links
    foreach ($Dom->find('.td a') as $Link)
    {
        $Links[] = $Link->href;
    }

    // Show: Page number
    echo $Page_Number . ' ';
}

// Write: To file
file_put_contents(__DIR__ . '/results/01_pages.php', '<?php return ' . var_export($Links, true) . ';');