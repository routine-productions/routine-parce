<?php

class Modx_Product_Images extends Illuminate\Database\Eloquent\Model
{
    public $timestamps = false;
    protected $table = "modx_ms2_product_files";
}
