<?php
/*
 * Create: Categories 01
 */
define('__HOMEDIR__', __DIR__);
require_once __DIR__ . '/../../core.php';

$Categories = require_once __DIR__ . '/../02_franchise-01/results/01_stage.php';

foreach ($Categories as $Category_Key => $Category)
{
    $Model = new Modx_Site_Content();

    $Model->id = 1000 + $Category_Key;
    $Model->parent = 2;
    $Model->pagetitle = $Category['title'];
    $Model->alias = Create_Slug($Category['title']);
    $Model->published = 1;
    $Model->isfolder = 1;
    $Model->template = 3;
    $Model->menuindex = 3;
    $Model->uri = Create_Slug($Category['title']) . '/';
    $Model->save();

    if (isset($Category['children']))
    {
        foreach ($Category['children'] as $Sub_Category_Key => $Sub_Category)
        {
            $Model = new Modx_Site_Content();

            $Model->id = 1000 + ($Category_Key * 100) + $Sub_Category_Key;
            $Model->parent = 1000 + $Category_Key;
            $Model->pagetitle = $Sub_Category['title'];
            $Model->alias = Create_Slug($Sub_Category['title']);
            $Model->published = 1;
            $Model->isfolder = 1;
            $Model->template = 3;
            $Model->menuindex = 3;
            $Model->uri = Create_Slug($Sub_Category['title']) . '/';
            $Model->save();
        }
    }
}