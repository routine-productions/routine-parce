<?php

class Category extends Illuminate\Database\Eloquent\Model
{
    public $timestamps = false;
    protected $table = "wp_terms";

}