<?php
/*
 * Get: Pages links
 */
define('__HOMEDIR__', __DIR__);
require_once __DIR__ . '/../../core.php';
$Pages = [];

// Get: Categories
$Categories = require_once __DIR__ . '/results/01_categories.php';


foreach ($Categories as $Category_Key => $Category)
{
    // Load: Category
    $Dom->loadFromURL($Category['href']);

    // Parse: Links
    if($Dom->find('.fransh-catalog-section .fransh-block',0)){
        foreach ($Dom->find('.fransh-catalog-section .fransh-block') as $Page)
        {
            $Pages[] = [
                'href' => $Page->find('.fransh-title a', 0) ? $Page->find('.fransh-title a')->href : '',
                'img' => $Page->find('.fransh-img img', 0) ? $Page->find('.fransh-img img')->src : '',
                'parent' => $Category_Key
            ];
        }
    }
}

// Write: To file
file_put_contents(__DIR__ . '/results/02_links.php', "<?php \n    return " . var_export($Pages, true) . ";");