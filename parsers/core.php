<?php
require_once __DIR__ . "/../vendor/autoload.php";
require_once __DIR__ . "/eloquent.php";
require_once __DIR__ . "/Autoload.php";

use PHPHtmlParser\Dom;


$Dom = new Dom;


/*
 * Save file from URL:
 * Save_File('/path-to-image/name.jpg','http://topfranchise.ru');
*/

function Save_File($URL, $Domain)
{
    $Path = explode('/', $URL);
    unset($Path[count($Path) - 1]);
    unset($Path[0]);
    $Path_New = '';
    foreach ($Path as $Path_Item)
    {
        $Path_New = $Path_New . '/' . $Path_Item;
        if (!is_dir(__HOMEDIR__ . '/results' . $Path_New))
        {
            mkdir(__HOMEDIR__ . '/results' . $Path_New, 0777, true);
        }
    }
    if (!file_exists(__HOMEDIR__ . '/results' . $URL))
    {
        $NEW_URL = explode('/', $URL);
        $NEW_URL[count($NEW_URL) - 1] = transliterator_transliterate("Any-Latin; NFD; [:Nonspacing Mark:] Remove; NFC; Lower();", $NEW_URL[count($NEW_URL) - 1]);

        $NEW_URL = implode('/', $NEW_URL);

        file_put_contents(__HOMEDIR__ . '/results' . $NEW_URL, file_get_contents($Domain . $URL));
    }
}


function Create_Slug($Text)
{
    // Replace non letter or digits by -
    $Text = preg_replace('~[^\pL\d]+~u', '', $Text);
//    $Text = strtolower($Text);
    $Text = str_replace('ь', '', $Text);
    $Text = str_replace('ъ', '', $Text);
    $Text = str_replace('ё', '', $Text);
//
//
//    // Transliterate
    $Text = transliterator_transliterate("Any-Latin; NFD; [:Nonspacing Mark:] Remove; NFC; Lower();", $Text);
    $Text = str_replace('`', '', $Text);
    // Remove unwanted characters
    $Text = preg_replace('~[^-\w]+~', '', $Text);

//    // Trim
    $Text = trim($Text, '-');
//    // Remove Duplicate -
//    $Text = preg_replace('~-+~', '-', $Text);
//    // To Lowercase
    $Text = mb_strtolower($Text);
    $Text = mb_substr($Text, 0, 25);


    if (empty($Text))
    {
        return '';
    }
    else
    {
        return $Text;
    }
}