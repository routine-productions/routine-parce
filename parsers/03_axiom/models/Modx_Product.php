<?php

class Modx_Product extends Illuminate\Database\Eloquent\Model
{
    public $timestamps = false;
    protected $table = "modx_ms2_products";
}
