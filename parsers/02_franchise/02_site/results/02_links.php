<?php
return array(
    0 =>
        array(
            'href' => '/catalog/franchise/CeramicPro/',
            'img' => '/upload/iblock/e47/ceramicpro200.jpg',
            'parent' => 0,
        ),
    1 =>
        array(
            'href' => '/catalog/franchise/Era-Auto/',
            'img' => '/upload/iblock/8aa/era200.jpg',
            'parent' => 0,
        ),
    2 =>
        array(
            'href' => '/catalog/franchise/ecomotors/',
            'img' => '/upload/iblock/f00/eco200.jpg',
            'parent' => 0,
        ),
    3 =>
        array(
            'href' => '/catalog/franchise/AACARRU/',
            'img' => '/upload/iblock/3f5/02%20logo.jpg',
            'parent' => 0,
        ),
    4 =>
        array(
            'href' => '/catalog/franchise/mojka-maks/',
            'img' => '/upload/iblock/625/maks-200.jpg',
            'parent' => 0,
        ),
    5 =>
        array(
            'href' => '/catalog/franchise/Carvizor/',
            'img' => '/upload/iblock/ce6/200x100.jpg',
            'parent' => 0,
        ),
    6 =>
        array(
            'href' => '/catalog/franchise/Auto-in-Line/',
            'img' => '/upload/iblock/313/auto-200.jpg',
            'parent' => 0,
        ),
    7 =>
        array(
            'href' => '/catalog/franchise/devar_kids/',
            'img' => '/upload/iblock/f7f/logo200x100%20%281%29.png',
            'parent' => 1,
        ),
    8 =>
        array(
            'href' => '/catalog/franchise/Zamess/',
            'img' => '/upload/iblock/9fe/200x100%20%281%29.jpg',
            'parent' => 1,
        ),
    9 =>
        array(
            'href' => '/catalog/franchise/franshiza-magazina-detskikh-tovarov-oranzhevyy-slon/',
            'img' => '/upload/iblock/526/orange-200.png',
            'parent' => 1,
        ),
    10 =>
        array(
            'href' => '/catalog/franchise/smart-course/',
            'img' => '/upload/iblock/c68/smart-200.jpg',
            'parent' => 1,
        ),
    11 =>
        array(
            'href' => '/catalog/franchise/ButtonBlue/',
            'img' => '/upload/iblock/c98/button200.jpg',
            'parent' => 1,
        ),
    12 =>
        array(
            'href' => '/catalog/franchise/bolshe-chem-igrushki/',
            'img' => '/upload/iblock/c6c/200x100.jpg',
            'parent' => 1,
        ),
    13 =>
        array(
            'href' => '/catalog/franchise/franshiza-detskoy-dizaynerskoy-odezhdy-orby/',
            'img' => '/upload/iblock/5c3/orby-200.gif',
            'parent' => 1,
        ),
    14 =>
        array(
            'href' => '/catalog/franchise/nauchnoe-show/',
            'img' => '/upload/iblock/722/nauch-200.jpg',
            'parent' => 1,
        ),
    15 =>
        array(
            'href' => '/catalog/franchise/championika/',
            'img' => '/upload/iblock/e05/chempionika-200.jpg',
            'parent' => 1,
        ),
    16 =>
        array(
            'href' => '/catalog/franchise/tsentr-detskih-tovarov/',
            'img' => '/upload/iblock/390/tsdt-200.jpg',
            'parent' => 1,
        ),
    17 =>
        array(
            'href' => '/catalog/franchise/Goroda-Club/',
            'img' => '/upload/iblock/161/goroda-club200.jpg',
            'parent' => 2,
        ),
    18 =>
        array(
            'href' => '/catalog/franchise/citynews/',
            'img' => '/upload/iblock/0eb/cn_200-100.jpg',
            'parent' => 2,
        ),
    19 =>
        array(
            'href' => '/catalog/franchise/LookInHotels/',
            'img' => '/upload/iblock/a82/200%D1%85100.jpg',
            'parent' => 2,
        ),
    20 =>
        array(
            'href' => '/catalog/franchise/mst/',
            'img' => '/upload/iblock/ae4/msp-200.jpg',
            'parent' => 3,
        ),
    21 =>
        array(
            'href' => '/catalog/franchise/SafeCalls/',
            'img' => '/upload/iblock/03c/200x100.jpg',
            'parent' => 3,
        ),
    22 =>
        array(
            'href' => '/catalog/franchise/franshiza-kompanii-white-smile/',
            'img' => '/upload/iblock/fc5/Logo.jpg',
            'parent' => 3,
        ),
    23 =>
        array(
            'href' => '/catalog/franchise/magicprime/',
            'img' => '/upload/iblock/839/al-kashmir-200.jpg',
            'parent' => 3,
        ),
    24 =>
        array(
            'href' => '/catalog/franchise/energy-box/',
            'img' => '/upload/iblock/082/eb-200.jpg',
            'parent' => 3,
        ),
    25 =>
        array(
            'href' => '/catalog/franchise/GrandFloat/',
            'img' => '/upload/iblock/a0e/grandfloat200.jpg',
            'parent' => 3,
        ),
    26 =>
        array(
            'href' => '/catalog/franchise/personalnoe-reshenie/',
            'img' => '/upload/iblock/966/200x100.jpg',
            'parent' => 3,
        ),
    27 =>
        array(
            'href' => '/catalog/franchise/dizajn-i-biznes/',
            'img' => '/upload/iblock/02d/db200.jpg',
            'parent' => 3,
        ),
    28 =>
        array(
            'href' => '/catalog/franchise/CITYCARD-UNITY/',
            'img' => '/upload/iblock/55f/citycard200.jpg',
            'parent' => 3,
        ),
    29 =>
        array(
            'href' => '/catalog/franchise/Laro-Power/',
            'img' => '/upload/iblock/729/200%D1%85100.jpg',
            'parent' => 3,
        ),
    30 =>
        array(
            'href' => '/catalog/franchise/franshiza-magazina-elektroinstrumenta-220-volt/',
            'img' => '/upload/iblock/8d8/200-100.png',
            'parent' => 3,
        ),
    31 =>
        array(
            'href' => '/catalog/franchise/zaryazhaj-ka/',
            'img' => '/upload/iblock/479/z-200.jpg',
            'parent' => 3,
        ),
    32 =>
        array(
            'href' => '/catalog/franchise/LookInHotels/',
            'img' => '/upload/iblock/a82/200%D1%85100.jpg',
            'parent' => 3,
        ),
    33 =>
        array(
            'href' => '/catalog/franchise/kiss-energy/',
            'img' => '/upload/iblock/3d6/kiss-200.jpg',
            'parent' => 3,
        ),
    34 =>
        array(
            'href' => '/catalog/franchise/Float-me/',
            'img' => '/upload/iblock/52a/floatme200.jpg',
            'parent' => 3,
        ),
    35 =>
        array(
            'href' => '/catalog/franchise/vysokoe-napryazhenie/',
            'img' => '/upload/iblock/593/vn-200.jpg',
            'parent' => 3,
        ),
    36 =>
        array(
            'href' => '/catalog/franchise/boss-kompas/',
            'img' => '/upload/iblock/586/logo_BC_200x100_black.png',
            'parent' => 3,
        ),
    37 =>
        array(
            'href' => '/catalog/franchise/tajmmet/',
            'img' => '/upload/iblock/50c/Logo_200x100.jpg',
            'parent' => 3,
        ),
    38 =>
        array(
            'href' => '/catalog/franchise/laboratoriya/',
            'img' => '/upload/iblock/0aa/lab-200.jpg',
            'parent' => 3,
        ),
    39 =>
        array(
            'href' => '/catalog/franchise/Cameron/',
            'img' => '/upload/iblock/e0d/200x100.jpg',
            'parent' => 4,
        ),
    40 =>
        array(
            'href' => '/catalog/franchise/PR-Partner/',
            'img' => '/upload/iblock/447/pr200.jpg',
            'parent' => 4,
        ),
    41 =>
        array(
            'href' => '/catalog/franchise/reklama-na-knopke/',
            'img' => '/upload/iblock/031/logo200x100.png',
            'parent' => 4,
        ),
    42 =>
        array(
            'href' => '/catalog/franchise/Studia+12/',
            'img' => '/upload/iblock/c23/studia12-200.jpg',
            'parent' => 4,
        ),
    43 =>
        array(
            'href' => '/catalog/franchise/helix/',
            'img' => '/upload/iblock/de1/helix-200.jpg',
            'parent' => 5,
        ),
    44 =>
        array(
            'href' => '/catalog/franchise/dobrotaru/',
            'img' => '/upload/iblock/9ae/logo-200.jpg',
            'parent' => 5,
        ),
    45 =>
        array(
            'href' => '/catalog/franchise/Coni-Beauty/',
            'img' => '/upload/iblock/76e/20spa200.jpg',
            'parent' => 5,
        ),
    46 =>
        array(
            'href' => '/catalog/franchise/S-Parfum/',
            'img' => '/upload/iblock/6e3/SParfum-200.jpg',
            'parent' => 5,
        ),
    47 =>
        array(
            'href' => '/catalog/franchise/bigudi/',
            'img' => '/upload/iblock/08f/bigudi-200.jpg',
            'parent' => 5,
        ),
    48 =>
        array(
            'href' => '/catalog/franchise/Familia/',
            'img' => '/upload/iblock/6a1/familia-200.jpg',
            'parent' => 5,
        ),
    49 =>
        array(
            'href' => '/catalog/franchise/kladovaya-zdorovya/',
            'img' => '/upload/iblock/e8f/kz-200.jpg',
            'parent' => 5,
        ),
    50 =>
        array(
            'href' => '/catalog/franchise/franshiza-seti-sportivno-ozdorovitelnykh-tsentrov-tonus-klub/',
            'img' => '/upload/iblock/a09/200%D1%85100.png',
            'parent' => 5,
        ),
    51 =>
        array(
            'href' => '/catalog/franchise/MyWay/',
            'img' => '/upload/iblock/64e/MYWAY-LOGO-200x100.jpg',
            'parent' => 5,
        ),
    52 =>
        array(
            'href' => '/catalog/franchise/rakurs/',
            'img' => '/upload/iblock/58d/rakurs-200.jpg',
            'parent' => 5,
        ),
    53 =>
        array(
            'href' => '/catalog/franchise/mej-tan/',
            'img' => '/upload/iblock/3d1/mejtan-200.jpg',
            'parent' => 5,
        ),
    54 =>
        array(
            'href' => '/catalog/franchise/Apriori/',
            'img' => '/upload/iblock/0e5/apriori200.jpg',
            'parent' => 5,
        ),
    55 =>
        array(
            'href' => '/catalog/franchise/CMD/',
            'img' => '/upload/iblock/925/cmd-20.jpg',
            'parent' => 5,
        ),
    56 =>
        array(
            'href' => '/catalog/franchise/ArtistHostel/',
            'img' => '/upload/iblock/f35/artist-200.jpg',
            'parent' => 6,
        ),
    57 =>
        array(
            'href' => '/catalog/franchise/Ost-roff/',
            'img' => '/upload/iblock/f7a/ostroff200.jpg',
            'parent' => 6,
        ),
    58 =>
        array(
            'href' => '/catalog/franchise/nice-hostel/',
            'img' => '/upload/iblock/631/nice-200.jpg',
            'parent' => 6,
        ),
    59 =>
        array(
            'href' => '/catalog/franchise/kazhan/',
            'img' => '/upload/iblock/9b4/kazhan200.jpg',
            'parent' => 7,
        ),
    60 =>
        array(
            'href' => '/catalog/franchise/diverse/',
            'img' => '/upload/iblock/bd9/diverse-200.jpg',
            'parent' => 7,
        ),
    61 =>
        array(
            'href' => '/catalog/franchise/Zamess/',
            'img' => '/upload/iblock/9fe/200x100%20%281%29.jpg',
            'parent' => 7,
        ),
    62 =>
        array(
            'href' => '/catalog/franchise/MEDICINE/',
            'img' => '/upload/iblock/8ad/med-200.jpg',
            'parent' => 7,
        ),
    63 =>
        array(
            'href' => '/catalog/franchise/zz/',
            'img' => '/upload/iblock/9b8/zz200.jpg',
            'parent' => 7,
        ),
    64 =>
        array(
            'href' => '/catalog/franchise/franshiza-detskoy-dizaynerskoy-odezhdy-orby/',
            'img' => '/upload/iblock/5c3/orby-200.gif',
            'parent' => 7,
        ),
    65 =>
        array(
            'href' => '/catalog/franchise/VILATTE/',
            'img' => '/upload/iblock/46a/vilatte-200.jpg',
            'parent' => 7,
        ),
    66 =>
        array(
            'href' => '/catalog/franchise/franshiza-seti-bulochnykh-pekaren-bonape/',
            'img' => '/upload/iblock/c42/c42a2809e9815e545e97386e5912d2aa.gif',
            'parent' => 8,
        ),
    67 =>
        array(
            'href' => '/catalog/franchise/sjem-slona/',
            'img' => '/upload/iblock/04a/slon%20200%D1%85100.JPG',
            'parent' => 8,
        ),
    68 =>
        array(
            'href' => '/catalog/franchise/Sushi-Love/',
            'img' => '/upload/iblock/042/sushilove200.jpg',
            'parent' => 8,
        ),
    69 =>
        array(
            'href' => '/catalog/franchise/chayburg/',
            'img' => '/upload/iblock/17b/chayburg-200.jpg',
            'parent' => 8,
        ),
    70 =>
        array(
            'href' => '/catalog/franchise/pizza-bombs/',
            'img' => '/upload/iblock/a69/pb-200.jpg',
            'parent' => 8,
        ),
    71 =>
        array(
            'href' => '/catalog/franchise/vafelnitsa/',
            'img' => '/upload/iblock/c22/wafl-200.jpg',
            'parent' => 8,
        ),
    72 =>
        array(
            'href' => '/catalog/franchise/Ellip6/',
            'img' => '/upload/iblock/bd6/Ellip6-200.jpg',
            'parent' => 8,
        ),
    73 =>
        array(
            'href' => '/catalog/franchise/PASE/',
            'img' => '/upload/iblock/f8e/pase%20200x100.jpg',
            'parent' => 8,
        ),
    74 =>
        array(
            'href' => '/catalog/franchise/sushi-wok/',
            'img' => '/upload/iblock/0b0/sushi200.jpg',
            'parent' => 8,
        ),
    75 =>
        array(
            'href' => '/catalog/franchise/franshiza-seti-magazinov-sushishop/',
            'img' => '/upload/iblock/bd2/sushoshop-200.gif',
            'parent' => 8,
        ),
    76 =>
        array(
            'href' => '/catalog/franchise/thebarxxxx/',
            'img' => '/upload/iblock/c55/xxxx-200.jpg',
            'parent' => 8,
        ),
    77 =>
        array(
            'href' => '/catalog/franchise/jaga-jazz/',
            'img' => '/upload/iblock/dcf/jagajazz200.jpg',
            'parent' => 8,
        ),
    78 =>
        array(
            'href' => '/catalog/franchise/burgerclub/',
            'img' => '/upload/iblock/97a/burgerclub-200.jpg',
            'parent' => 8,
        ),
    79 =>
        array(
            'href' => '/catalog/franchise/Subway/',
            'img' => '/upload/iblock/9ed/subway-200.jpg',
            'parent' => 8,
        ),
    80 =>
        array(
            'href' => '/catalog/franchise/givemewaffle/',
            'img' => '/upload/iblock/71c/waffle-200.jpg',
            'parent' => 8,
        ),
    81 =>
        array(
            'href' => '/catalog/franchise/burlim/',
            'img' => '/upload/iblock/d64/02%20logo.jpg',
            'parent' => 8,
        ),
    82 =>
        array(
            'href' => '/catalog/franchise/pivoriba/',
            'img' => '/upload/iblock/2ef/pivoriba200.jpg',
            'parent' => 8,
        ),
    83 =>
        array(
            'href' => '/catalog/franchise/my-coffee/',
            'img' => '/upload/iblock/e3b/mycoffee-200.jpg',
            'parent' => 8,
        ),
    84 =>
        array(
            'href' => '/catalog/franchise/Carbonara-fresh-pasta-bar/',
            'img' => '/upload/iblock/076/carbonara-200.jpg',
            'parent' => 8,
        ),
    85 =>
        array(
            'href' => '/catalog/franchise/vafelniza/',
            'img' => '/upload/iblock/9cb/vaf-200.jpg',
            'parent' => 8,
        ),
    86 =>
        array(
            'href' => '/catalog/franchise/milknball/',
            'img' => '/upload/iblock/141/milk200.jpg',
            'parent' => 8,
        ),
    87 =>
        array(
            'href' => '/catalog/franchise/dada/',
            'img' => '/upload/iblock/a60/dada200.jpg',
            'parent' => 8,
        ),
    88 =>
        array(
            'href' => '/catalog/franchise/gotovye-programmy-dlya-provedeniya-treningov-i-konsultatsiy/',
            'img' => '/upload/iblock/a42/logo100x200.jpg',
            'parent' => 9,
        ),
    89 =>
        array(
            'href' => '/catalog/franchise/shkola-prodazh-ltsvetovoj/',
            'img' => '/upload/iblock/9d4/shkola-200.jpg',
            'parent' => 9,
        ),
    90 =>
        array(
            'href' => '/catalog/franchise/godograf/',
            'img' => '/upload/iblock/f36/za-250.jpg',
            'parent' => 9,
        ),
    91 =>
        array(
            'href' => '/catalog/franchise/Ellip6/',
            'img' => '/upload/iblock/bd6/Ellip6-200.jpg',
            'parent' => 10,
        ),
    92 =>
        array(
            'href' => '/catalog/franchise/biznes-po-testirovaniyu-sposobnostey-cheloveka-genetic-test/',
            'img' => '/upload/iblock/152/gt-200.jpg',
            'parent' => 10,
        ),
    93 =>
        array(
            'href' => '/catalog/franchise/infolife/',
            'img' => '/upload/iblock/2d0/infolife-200.jpg',
            'parent' => 10,
        ),
    94 =>
        array(
            'href' => '/catalog/franchise/amorefiori/',
            'img' => '/upload/iblock/126/af-200.jpg',
            'parent' => 11,
        ),
    95 =>
        array(
            'href' => '/catalog/franchise/franshiza-magazina-elektroinstrumenta-220-volt/',
            'img' => '/upload/iblock/8d8/200-100.png',
            'parent' => 11,
        ),
    96 =>
        array(
            'href' => '/catalog/franchise/pivoriba/',
            'img' => '/upload/iblock/2ef/pivoriba200.jpg',
            'parent' => 11,
        ),
    97 =>
        array(
            'href' => '/catalog/franchise/vipera-cosmetics/',
            'img' => '/upload/iblock/fc1/vipera-200.gif',
            'parent' => 11,
        ),
    98 =>
        array(
            'href' => '/catalog/franchise/mr-stekolli/',
            'img' => '/upload/iblock/376/stekolli-200.jpg',
            'parent' => 12,
        ),
    99 =>
        array(
            'href' => '/catalog/franchise/franshiza-betonbaza-torgovlya-stroitelnym-materialom-beton-po-vsey-rossii-i-sng/',
            'img' => '/upload/iblock/31f/bb-200.jpg',
            'parent' => 12,
        ),
    100 =>
        array(
            'href' => '/catalog/franchise/gorod-instrumenta/',
            'img' => '/upload/iblock/034/02%20logo.jpg',
            'parent' => 12,
        ),
    101 =>
        array(
            'href' => '/catalog/franchise/dasms/',
            'img' => '/upload/iblock/667/dasms200.jpg',
            'parent' => 13,
        ),
    102 =>
        array(
            'href' => '/catalog/franchise/Satomru/',
            'img' => '/upload/iblock/220/logo_200x100.png',
            'parent' => 13,
        ),
    103 =>
        array(
            'href' => '/catalog/franchise/stroytaxi/',
            'img' => '/upload/iblock/18d/stroytaxi-200.jpg',
            'parent' => 13,
        ),
    104 =>
        array(
            'href' => '/catalog/franchise/PickPoint/',
            'img' => '/upload/iblock/85a/pickpoint-200.jpg',
            'parent' => 13,
        ),
    105 =>
        array(
            'href' => '/catalog/franchise/orrla/',
            'img' => '/upload/iblock/002/orrla200.jpg',
            'parent' => 13,
        ),
    106 =>
        array(
            'href' => '/catalog/franchise/mr-stekolli/',
            'img' => '/upload/iblock/376/stekolli-200.jpg',
            'parent' => 13,
        ),
    107 =>
        array(
            'href' => '/catalog/franchise/rbr/',
            'img' => '/upload/iblock/07b/rbr-200.jpg',
            'parent' => 13,
        ),
    108 =>
        array(
            'href' => '/catalog/franchise/Tenderlife/',
            'img' => '/upload/iblock/b44/zakazlife-200.jpg',
            'parent' => 13,
        ),
    109 =>
        array(
            'href' => '/catalog/franchise/shokoladnaya-mechta/',
            'img' => '/upload/iblock/99e/shoko200.jpg',
            'parent' => 13,
        ),
    110 =>
        array(
            'href' => '/catalog/franchise/pereezd-igrayuchi/',
            'img' => '/upload/iblock/469/pereezd200.jpg',
            'parent' => 13,
        ),
    111 =>
        array(
            'href' => '/catalog/franchise/argus-pechati/',
            'img' => '/upload/iblock/ca6/argus-200.jpg',
            'parent' => 13,
        ),
    112 =>
        array(
            'href' => '/catalog/franchise/gruzchikov-servis/',
            'img' => '/upload/iblock/73d/gruz-200.jpg',
            'parent' => 13,
        ),
    113 =>
        array(
            'href' => '/catalog/franchise/grosh/',
            'img' => '/upload/iblock/f70/grosh200.jpg',
            'parent' => 14,
        ),
);