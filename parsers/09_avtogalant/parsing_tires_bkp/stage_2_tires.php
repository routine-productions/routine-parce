<?php

define('__HOMEDIR__', __DIR__);

require_once __DIR__ . '/../core.php';

$fetched_outer = include __DIR__ . '/results/all_tires_outer.php';
$count = include __DIR__ . '/db_tires_seeded_count.php';
$counter_saved = $count[0];
$amount = count($fetched_outer);

//

//$fetched_inner = include __DIR__ . '/results/allInnerDom.php';

function set_options($product_id, $options)
{


//    product_id feature_id   lang_id  value   translit

    foreach ($options as $option) {

        if ($option['id'] && $option['value'] && strlen($option['value']) > 0) {

            $Options = new Options();
            $Options->product_id = $product_id;
            $Options->feature_id = $option['id'];
            $Options->value = $option['value'];
            $Options->lang_id = 1;
            $Options->translit = Create_Slug($option['value']);

            $Options->save();

//            echo '<br> option translit is ' .  $Options->translit;
        }

    }


}

function save_image($url, $product_id, $img = null)
{
//    id	name	product_id	filename	position	http_original_name

    $img_url = 'https:' . $url;
    $trim_name = substr($img_url, strrpos($img_url, '/') + 1);
    $path = __DIR__ . '/results/img/tires_outer/' . $trim_name;
    $sitePath = 'design/avtogalant/images/' . $url;

    $image = new Images();
    $image->product_id = $product_id;
    $image->name = $trim_name;
    $image->filename = $trim_name;
    $image->http_original_name = $img_url;
    $image->save();

//    if (!file_exists($path)) {
//        file_put_contents($path, file_get_contents($img_url));
////        echo '<br> file saved ' . $path;
//    } else {
////        echo '<br> file exists ' . $path;
//    }
}

function save_images($images, $product_id)
{
    for ($i = 0; $i < count($images); $i++) {
        $img = $images[$i];
        $img_url = $images[$i]['src_full'];

        save_image($img_url, $product_id, $img);
    }
}

function get_first_word($string)
{
    return substr($string, 0, strpos(trim($string), ' '));
}


function trim_string_after_first($string, $symbol, $return_default = true)
{
    $trimmed = substr(strstr($string, $symbol), 1, strlen($string));

    if ($trimmed !== false) {
        return $trimmed;


    } else {
        if ($return_default === true) {
            return $string;
        } else {
            return null;
        }

    }
}

function get_speed_index($option_string)
{

    $speed_index = null;

    $dirty_string = mb_stristr($option_string, ' ');

    $speed_index = trim(substr($dirty_string, -4));

    return $speed_index;


//     replace_phrase_from(trim_string_after_first($option_string, '/'), $option_string);
}

$iteration_features = [];


function get_options($option_string, $add_options = null)
{
//    "195/65R15 91T" //  инд скор /выспрофиля Rпос диам

    $load_index_val = trim(substr($option_string, strrpos($option_string, ' ') + 1));


    $extended = mb_stristr(replace_phrase_from($load_index_val, $option_string), '/');

    $speed_index = get_speed_index(replace_phrase_from(mb_stristr($option_string, '/'), $option_string));


    $option_string = $extended;

    $start_height = strpos($option_string, '/');
    $start_radius = strpos($option_string, 'R');
    $last_space = strrpos($option_string, ' ');

    $options = [];

    $start_height_string = substr($option_string, $start_height);
    $start_radius_string = substr($option_string, $start_radius);
    $start_last_spaces_string = substr($option_string, $last_space);

    $height_length = strlen($start_height_string) - strlen($start_radius_string);
    $dia_length = strlen($start_radius_string) - strlen($start_last_spaces_string);

    $height_trim = substr($option_string, $start_height, $height_length);

    $prof_height_val = trim(substr($height_trim, strrpos($height_trim, '/') + 1));

    if (strlen(strlen($prof_height_val) < 3)) {

        $options['profile_height'] = ['id' => 7, 'name' => 'серия (высота профиля)', 'value' => trim($prof_height_val)];
    }

    $dia = substr($option_string, $start_radius, $dia_length);

    $options['load_index'] = ['id' => 16, 'name' => 'индекс нагрузки', 'value' => $load_index_val];

    if ($speed_index !== null) {
        $options['speed_index'] = ['id' => 17, 'name' => 'индекс скорости', 'value' => preg_replace('~[^0-9.]+~', '', $speed_index)];
    }


    $options['dia'] = ['id' => 8, 'name' => 'Посадочн диаметр', 'value' => $dia];


    if ($add_options != null) {
        foreach ($add_options as $option) {
            if ($option['value'] !== null)
                $options[$option['big_name']] = ['id' => $option['id'], 'name' => $option['name'], 'value' => $option['value']];
        }
    }

    echo '<table style="text-align: center; width : 100%">';
    echo '<tbody>';
    echo '<tr>';


    foreach ($options as $key => $option) {

        if ($option['id'] && $option['value'] && strlen($option['value']) > 0) {
            echo '<td>' . $key . '=' . $option['value'] . '</td>';

        }

    }
    echo '</tr>';
    echo '</tbody>';
    echo '</table>';

    return $options;


}

$vech_types = [

    'для автобусов и грузовых автомобилей',
    'для внедорожников/для автобусов и грузовых автомобилей',
    'для легковых автомобилей/для автобусов и грузовых автомобилей',
    'для легковых автомобилей/для внедорожников',
    'для легковых автомобилей/для внедорожников/для микроавтобусов и легкогрузовых автомобилей',
    'для микроавтобусов и легкогрузовых автомобилей',
    'для строительной и дорожной техники',
    'для легковых автомобилей',
    'для грузовых автомобилей',
    'для внедорожников',

];

$vech_types_trim = [
    ' Run-flat',
    ' run-flat',
    ' (run-flat)',
    ' шипы',
    ' Шипы'
];

function replace_phrase_from($what, $from)
{
    if ($what === $from) {
        return $what;

    } else {

//        dd(str_replace($what, '', $from));
        return str_replace($what, '', $from);
    }

}

function vechicle_type_trim($string)
{
    global $vech_types_trim;

    $cur_string = $string;

    foreach ($vech_types_trim as $trimmed) {

        if (strrpos($cur_string, $trimmed) !== false) {
            $cur_string = replace_phrase_from($trimmed, $string);

        }

    }
    return $cur_string;
}

;

function vechicle_type($string)
{
    global $vech_types;

    $vech_type_is = null;


    foreach ($vech_types as $vech_type) {

        $is_cur_type = strrpos($string, $vech_type);

        if ($is_cur_type !== false) {

            $vech_type_is = $vech_type;
        }

    }

    return $vech_type_is;

}

function return_season($string)
{

    if (strrpos($string, 'летние') !== false) {

        return 'летняя';

    } else {
        if (strrpos($string, 'зимние') !== false) {
            return 'зимняя';

        } else {
            if (strrpos($string, 'всесезонные') !== false) {
                return 'всесезонная';

            } else {
                return null;
            }
        }
    }

}

function check_season_vech_type($info)
{
    $add_options = [];

    $season = return_season($info);

    $add_options[0] = [
        'big_name' => 'season',
        'id' => 6,
        'name' => 'сезон',
        'value' => $season

    ];
    $add_options[1] = [
        'big_name' => 'naznachenie',
        'id' => 5,
        'name' => 'назначение',
        'value' => vechicle_type($info)
    ];


    return $add_options;
}

;

function replace_phrase_from_with($what, $from, $with){
    if ($what == $from) {
        return $what;
    } else {
        return str_replace($what, $with, $from);
    }
}


function setDBData($fetched_outer)
{
    global $counter_saved;

    $new_product_name = replace_phrase_from_with('&quot;',$fetched_outer['name'],'"');

    $name_to_parse = vechicle_type_trim($fetched_outer['name']);
    $descr_to_parse = vechicle_type_trim($fetched_outer['description']);

    $add_options = check_season_vech_type($descr_to_parse);

    echo '<br>';
    echo '<h1>' . $new_product_name . '</h1>';
//    get_options($name_to_parse, $add_options);


    $new_product_cat_id = 25;//Шины
    $new_product_url = $fetched_outer['key'];

    if (isset($fetched_outer['prices'])) {
        $new_product_price_min = $fetched_outer['prices']['price_min']['amount'];

        $new_product_price_max = $fetched_outer['prices']['price_max']['amount'];
    } else {
        $new_product_price_min = 0;

        $new_product_price_max = 0;
    }

    $new_product_currency_name = $fetched_outer['prices']['price_max']['currency'];
    $new_product_currency_id = 2;
    $brand_name_original = get_first_word($fetched_outer['full_name']);
    $brand_url = Create_Slug($brand_name_original);


    $Products = new Products();


    $Variants = new Variants();
    $Lang_Products = new Lang_Products();
    $product_cat = new Products_Categories();

    $Products->name = $new_product_name;
    $Products->url = $new_product_url;
    $Products->annotation = $new_product_name;
    $Products->meta_title = $new_product_name;
    $Products->meta_keywords = $new_product_name;


    if (Brands::where('name', $brand_name_original)->first()) {
        $brand = Brands::where('name', $brand_name_original)->first();
        $Products->brand_id = $brand['id'];


    } else {
        //        lang_id lang_label brand_id name meta_title meta_keywords meta_description annotation
        echo '<br> Создаем новый бренд: ' . $brand_name_original . '<br>';
        $le_brand = new Brands();
        $le_brand = new Brands();

        $le_brand->name = $brand_name_original;
        $le_brand->url = $brand_url;
        $le_brand->image = $brand_url;
        $le_brand->meta_title = $brand_name_original;
        $le_brand->meta_keywords = $brand_name_original;
        $le_brand->save();

        $le_brand_lang = new Lang_Brands();
        $le_brand_lang->brand_id = $le_brand->id;
        $le_brand_lang->lang_id = 1;
        $le_brand_lang->name = $brand_url;
        $le_brand_lang->meta_title = $brand_name_original;
        $le_brand_lang->meta_keywords = $brand_name_original;
        $le_brand_lang->save();

        $Products->brand_id = $le_brand['id'];

    }
    $Products->save();

    echo '<br> prod brand_id' . $Products['brand_id'];
    save_image($fetched_outer['images']['header'], $Products->id);

    set_options($Products->id, get_options($name_to_parse, $add_options));

    $product_cat->product_id = $Products->id;
    $product_cat->category_id = $new_product_cat_id;
    $product_cat->position = 2;


    $Variants->product_id = $Products->id;

    $Variants->price = $new_product_price_min;
    if ($new_product_price_min < $new_product_price_max) {
        $Variants->compare_price = $new_product_price_max;
    }

    $Variants->currency_id = $new_product_currency_id;

    $Lang_Products->product_id = $Products->id;
    $Lang_Products->meta_title = $new_product_name;
    $Lang_Products->meta_keywords = $new_product_name;
    $Lang_Products->lang_id = 1;//Русский
    $Lang_Products->name = $new_product_name;

    $Variants->save();
    $product_cat->save();
    $Lang_Products->save();

    file_put_contents(__DIR__ . '/db_tires_seeded_count.php', "<?php \n    return " . var_export([$counter_saved], true) . ";");
    $counter_saved++;

}


function loopAndSetDbData()
{

    global $fetched_outer;
    global $counter_saved;
    global $amount;

    for ($i = $counter_saved; $i < $amount; $i++) {

        setDBData($fetched_outer[$i]);
        echo ' ' . $counter_saved . ' ';
    }

    unset($fetched_products);
}

loopAndSetDbData();

//$AllProducts = Products::get();
//dd($AllProducts->toArray());


