<?php
/*
 * Get: Person links
 */
define('__HOMEDIR__', __DIR__);
require_once __DIR__ . '/../core.php';

$Setup = B_Iblock_Element::where('IBLOCK_ID', 2)->with('values')->get()->toArray();
//IBLOCK_ELEMENT_ID


$Setups = require_once __DIR__ . '/results/setups.php';
//print_r($Video);

foreach ($Setups as $item)
{
    // Save Object
    $object = new B_Iblock_Element();
    $object->IBLOCK_ID = 2;
    $object->IN_SECTIONS = 'N';
    $object->CODE = $item['alias'];
    $object->NAME = $item['pagetitle'];
    $object->PREVIEW_TEXT = $item['pagetitle'];
    $object->save();


    foreach ($item['videos'] as $video)
    {
        // Extra Field
        $prop = new B_Iblock_Element_Property();
        $prop->IBLOCK_PROPERTY_ID = 7;
        $prop->IBLOCK_ELEMENT_ID = $object->id;
        $prop->VALUE = $video['value'];
        $prop->VALUE_TYPE = 'text';
        $prop->save();
    }

    foreach ($item['images'] as $img)
    {
        $img_parts = explode('/', $img['value']);
        $img_name = $img_parts[count($img_parts) - 1];
        unset($img_parts[count($img_parts) - 1]);
        $img_path = implode('/', $img_parts);

        echo implode('/', $img_parts) . '<br>';

        // Save File
        $file = new B_File();
        $file->MODULE_ID = 'iblock';
        $file->SUBDIR = 'iblock/assets/' . $img_path;
        $file->CONTENT_TYPE = 'image/jpeg';
        $file->FILE_SIZE = 18136;
        $file->HEIGHT = 255;
        $file->WIDTH = 255;
        $file->FILE_NAME = $img_name;
        $file->ORIGINAL_NAME = $img_name;
        $file->save();

        // Extra Field
        $prop = new B_Iblock_Element_Property();
        $prop->IBLOCK_PROPERTY_ID = 9;
        $prop->IBLOCK_ELEMENT_ID = $object->id;
        $prop->VALUE = $file->id;
        $prop->VALUE_TYPE = 'file';
        $prop->save();
    }
}