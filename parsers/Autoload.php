<?php

class Autoload
{
    public function __construct()
    {
        spl_autoload_register([$this, 'Load_Models']);
    }

    protected function Load_Models($Class)
    {
        $Path = __HOMEDIR__ . '/models/' . $Class . '.php';

        if (file_exists($Path))
        {
            include $Path;
        }
    }
}

new Autoload();