<?php

define('__HOMEDIR__', __DIR__);

require_once __DIR__ . '/../core.php';

$fetched_outer = include __DIR__ . '/results/all_tires_outer.php';
$count = include __DIR__ . '/db_tires_seeded_count.php';
$counter_saved = $count[0];
//$counter_saved = 0;

//$fetched_inner = include __DIR__ . '/results/allInnerDom.php';

function set_options($product_id, $options)
{

//    product_id feature_id   lang_id  value   translit

    foreach ($options as $option) {
        if ($option['id'] && $option['value'] && strlen($option['value']) > 0) {

            $Options = new Options();
            $Options->product_id = $product_id;
            $Options->feature_id = $option['id'];
            $Options->value = $option['value'];
            $Options->lang_id = 1;
            $Options->translit = Create_Slug($option['name']);
            $Options->save();
        }

    }

}

function save_image($url, $product_id, $img = null)
{
//    id	name	product_id	filename	position	http_original_name

    $img_url = 'https:' . $url;
    $trim_name = substr($img_url, strrpos($img_url, '/') + 1);
    $path = __DIR__ . '/results/img/tires_outer/' . $trim_name;
    $sitePath = 'design/avtogalant/images/' . $url;

    $image = new Images();
    $image->product_id = $product_id;
    $image->name = $trim_name;
    $image->filename = $trim_name;
    $image->http_original_name = $img_url;
    $image->save();

    if (!file_exists($path)) {
        file_put_contents($path, file_get_contents($img_url));
        echo '<br> file saved ' . $path;
    } else {
        echo '<br> file exist ';
    }
}

function save_images($images, $product_id)
{
    for ($i = 0; $i < count($images); $i++) {
        $img = $images[$i];
        $img_url = $images[$i]['src_full'];

        save_image($img_url, $product_id, $img);
    }
}

function get_first_word($string)
{
    return substr($string, 0, strpos(trim($string), ' '));
}

$iteration_features = [];


function get_options($option_string, $add_options = null)
{
//    "195/65R15 91T" //  инд скор /выспрофиля Rпос диам

    $start_height = strpos($option_string, '/');
    $start_radius = strpos($option_string, 'R');
    $last_space = strrpos($option_string, ' ');

    $options = [];

    $start_height_string = substr($option_string, $start_height);
    $start_radius_string = substr($option_string, $start_radius);
    $start_last_spaces_string = substr($option_string, $last_space);

    $height_length = strlen($start_height_string) - strlen($start_radius_string);
    $dia_length = strlen($start_radius_string) - strlen($start_last_spaces_string);

    $height_trim = substr($option_string, $start_height, $height_length);
    $dia_trim = substr($option_string, $start_radius, $dia_length);

    $prof_height = trim(substr($height_trim, strrpos($height_trim, '/') + 1));
    $dia = preg_replace('~[^0-9]+~', '', $dia_trim);

    $options['speed_index'] = ['id' => 17, 'name' => 'индекс скорости', 'value' => preg_replace('~[^0-9]+~', '', substr($option_string, 0, 4))];
    $options['dia'] = ['id' => 8, 'name' => 'Посадочн диаметр', 'value' => $dia];
    $options['profile_height'] = ['id' => 7, 'name' => 'серия (высота профиля)', 'value' => $prof_height];
    $options['load_index'] = ['id' => 16, 'name' => 'индекс нагрузки', 'value' => trim(substr($option_string, strrpos($option_string, ' ') + 1))];

    if ($add_options != null) {
        foreach ($add_options as $option) {
            if ($option['value'] !== null)
                $options[$option['big_name']] = ['id' => $option['id'], 'name' => $option['name'], 'value' => $option['value']];
        }
    }

    if ($start_height && $start_radius && $last_space) {
        return $options;
    } else {
        return [];
    }

}


function vechicle_type($string)
{

    if (strrpos($string, 'для автобусов и грузовых автомобилей') !== false) {
        return 'для автобусов и грузовых автомобилей';

    } else {
        if (strrpos($string, 'для внедорожников') !== false) {
            return 'для внедорожников';

        } else {
            if (strrpos($string, 'для внедорожников/для автобусов и грузовых автомобилей') !== false) {
                return 'для внедорожников/для автобусов и грузовых автомобилей';

            } else {
                if (strrpos($string, 'для легковых автомобилей') !== false) {
                    return 'для легковых автомобилей';

                } else {
                    if (strrpos($string, 'для легковых автомобилей/для автобусов и грузовых автомобилей') !== false) {
                        return 'для легковых автомобилей/для автобусов и грузовых автомобилей';

                    } else {
                        if (strrpos($string, 'для легковых автомобилей/для внедорожников') !== false) {
                            return 'для легковых автомобилей/для внедорожников';

                        } else {
                            if (strrpos($string, 'для легковых автомобилей/для внедорожников/для микроавтобусов и легкогрузовых автомобилей') !== false) {
                                return 'для легковых автомобилей/для внедорожников/для микроавтобусов и легкогрузовых автомобилей';

                            } else {
                                if (strrpos($string, 'для микроавтобусов и легкогрузовых автомобилей') !== false) {
                                    return 'для микроавтобусов и легкогрузовых автомобилей';

                                } else {
                                    if (strrpos($string, 'для строительной и дорожной техники') !== false) {
                                        return 'для микроавтобусов и легкогрузовых автомобилей';

                                    } else {
                                        if (strrpos($string, 'Run-flat') == false) {

                                            if (strrpos($string, 'шипы') == false) {
                                                return $string;

                                            } else {
                                                return null;
                                            }

                                        } else {
                                            return null;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

function return_season($string)
{

    if (strrpos($string, 'летние') !== false) {

        return 'летние';
    } else {
        if (strrpos($string, 'зимние') !== false) {
            return 'зимние';

        } else {
            if (strrpos($string, 'всесезонные') !== false) {
                return 'всесезонные';

            } else {
                return null;
            }
        }
    }

}

function return_vechicle_type($info, $season)
{
    if ($season !== null && strlen($season) > 0) {
        $run_flat = 'Run-flat';
        $sh = 'шипы';

        $vech_type = substr($info, strlen($season));
        $vech_type = vechicle_type(trim(substr($vech_type, strrpos($vech_type, ',') + 1)));

        if ($vech_type !== null) {
            return $vech_type;

        } else {
            return null;
        }

    } else {
        return null;
    }

}

function check_season_vech_type($info)
{
    $add_options = null;


    if (isset($info) && strlen($info) > 0) {
        $add_options = [];

        $season = return_season($info);

        $add_options[0] = [
            'big_name' => 'season',
            'id' => 6,
            'name' => 'сезон',
            'value' => $season

        ];
        $add_options[1] = [
            'big_name' => 'naznachenie',
            'id' => 5,
            'name' => 'назначение',
            'value' => return_vechicle_type($info, $season)
        ];
    }

    return $add_options;
}

;


function setDBData($fetched_outer)
{
    global $counter_saved;
    $all_descriptions_add = substr($fetched_outer['name'], -13);

    $add_options = check_season_vech_type($fetched_outer['description']);

    $new_product_name = $fetched_outer['name'];
    $new_product_cat_id = 25;//Шины
    $new_product_url = $fetched_outer['key'];
    $new_product_price_min = $fetched_outer['prices']['price_min']['amount'];
    $new_product_price_max = $fetched_outer['prices']['price_max']['amount'];
    $new_product_currency_name = $fetched_outer['prices']['price_max']['currency'];
    $new_product_currency_id = 2;
    $brand_name_original = get_first_word($fetched_outer['full_name']);
    $brand_url = Create_Slug($brand_name_original);


    $Products = new Products();
    $le_brand = new Brands();

    $Variants = new Variants();
    $Lang_Products = new Lang_Products();
    $product_cat = new Products_Categories();

    $Products->name = $new_product_name;
    $Products->url = $new_product_url;
    $Products->annotation = $new_product_name;
    $Products->meta_title = $new_product_name;
    $Products->meta_keywords = $new_product_name;
    $Products->save();
    $new_product_images = save_image($fetched_outer['images']['header'], $Products->id);
    save_images($new_product_images, $Products->id);

    set_options($Products->id, get_options($all_descriptions_add, $add_options));

    $product_cat->product_id = $Products->id;
    $product_cat->category_id = $new_product_cat_id;
    $product_cat->position = 2;


    $Variants->product_id = $Products->id;
    $Variants->price = $new_product_price_max || 0;
    $Variants->compare_price = $new_product_price_min;
    $Variants->currency_id = $new_product_currency_id;

    $Lang_Products->product_id = $Products->id;
    $Lang_Products->meta_title = $new_product_name;
    $Lang_Products->meta_keywords = $new_product_name;
    $Lang_Products->lang_id = 1;//Русский
    $Lang_Products->name = $new_product_name;


    if (Brands::where('name', $brand_name_original)->first()) {
        $brand = Brands::where('name', $brand_name_original)->first();
        $Products->brand_id = $brand['id'];

    } else {
        echo 'Создаем новую категорию: ' . $brand_name_original;
        $le_brand->name = $brand_name_original;
        $le_brand->url = $brand_url;
        $le_brand->image = $brand_url;
        $le_brand->meta_title = $brand_name_original;
        $le_brand->meta_keywords = $brand_name_original;
        $le_brand->save();
    }

    $Variants->save();
    $product_cat->save();
    $Lang_Products->save();

    file_put_contents(__DIR__ . '/db_tires_seeded_count.php', "<?php \n    return " . var_export([$counter_saved], true) . ";");
    $counter_saved++;

}


function loopAndSetDbData()
{

    global $fetched_outer;
    global $counter_saved;

    //    for ($i = $counter_saved; $i < 1000; $i++) {
    for ($i = $counter_saved; $i < count($fetched_outer); $i++) {

        setDBData($fetched_outer[$i]);
    }

    unset($fetched_products);
}

loopAndSetDbData();

//$AllProducts = Products::get();
//dd($AllProducts->toArray());

