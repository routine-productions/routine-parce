<?php

define('__HOMEDIR__', __DIR__);

require_once __DIR__ . '/../core.php';

$Links = [];

// Get: All pages
$Dom->loadFromUrl('http://www.mos-nasos.ru/catalog/');

// Get: All links
foreach ($Dom->find('.sidebar-widget .sub-menu li a') as $key => $Link)
{
    $Links[$key]['href'] = $Link->href;
    $Links[$key]['title'] = $Link->text;
}
$Parent_href = [];

foreach ($Links as $key => $Link)
{
    if ($key < 2)
    {
        $Dom->loadFromUrl($Link['href']);
        foreach ($Dom->find('.sidebar-widget .sub-menu li ul li a') as $k => $link)
        {
            $Parent_href[$k]['href'] = $link->href;
            $Parent_href[$k]['title'] = $link->text;
        }
        $Links[$key]['parent_href']['link'] = $Parent_href;
    }
}
// Write: To file
file_put_contents(__DIR__ . '/results/01_links.php', "<?php \n    return " . var_export($Links, true) . ";");