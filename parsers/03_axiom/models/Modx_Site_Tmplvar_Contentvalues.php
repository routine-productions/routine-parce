<?php

class Modx_Site_Tmplvar_Contentvalues extends Illuminate\Database\Eloquent\Model
{
    public $timestamps = false;
    protected $table = "modx_site_tmplvar_contentvalues";
}