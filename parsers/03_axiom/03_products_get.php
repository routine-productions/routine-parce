<?php
define('__HOMEDIR__', __DIR__);
require_once __DIR__ . '/../core.php';


$products = Modx_Site_Content::where('parent', 140)->where('published', 1)->with('product', 'gallery', 'instruction', 'settings')->take(100)->get()->toArray();


//foreach ($products as $product) {
//    print_r($product['pagetitle']);
//}
//
//exit;

$products_state = [];
$products_universal = [];


foreach ($products as $product) {
    echo mb_strpos($product['pagetitle'], 'AXIOM');
    if (mb_strpos($product['pagetitle'], 'AXIOM') === 0 &&
        mb_strpos($product['pagetitle'], 'AXIOM REAR') !== 0 &&
        mb_strpos($product['pagetitle'], 'AXIOM CAR') !== 0
    ) {
        $products_state[] = $product;
    } else {
        $products_universal[] = $product;
    }
}

foreach ($products as $item) {
    if (isset($item['gallery'])) {
        foreach ($item['gallery'] as $img) {
            Save_File('/' . $img['url'], 'http://www.axiom-russia.ru');
        }
    }
}

file_put_contents(__DIR__ . '/results/products_state.php', '<?php return ' . var_export($products_state, true) . ';');
file_put_contents(__DIR__ . '/results/products_universal.php', '<?php return ' . var_export($products_universal, true) . ';');