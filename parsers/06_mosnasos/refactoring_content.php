<?php
define('__HOMEDIR__', __DIR__);
require_once __DIR__ . '/../core.php';


SetCat();


function SetCat()
{
    $Pumps = require __DIR__ . '/results/all_links.php';
//    print_r($Pumps);    exit;


    $Nasos = [];
    foreach ($Pumps as $k => $Pump)
    {

        $Image = array_filter(explode(' ', $Pump['img']));

        $Imag = [];
        foreach ($Image as $img)
        {
            $img = str_replace('http://www.mos-nasos.ru', '/wp-content/uploads/2016/09', $img);
            $img = explode('/', $img);
            $val = transliterator_transliterate("Any-Latin; NFD; [:Nonspacing Mark:] Remove; NFC; Lower();", $img[count($img) - 1]);
            $img[count($img) - 1] = $val;
            $img = implode('/', $img);
            $Imag[] = $img;
        }
        $Pumps[$k]['img'] = $Imag;

        if (!empty($Pump['description']))
        {

            $PDF = array_filter(explode(' ', $Pump['description']));
            $PD = [];
            foreach ($PDF as $pdf)
            {
                $pdf = str_replace('http://www.mos-nasos.ru', '/wp-content/uploads/2016/09', $pdf);
                $pdf = explode('/', $pdf);
                $val = transliterator_transliterate("Any-Latin; NFD; [:Nonspacing Mark:] Remove; NFC; Lower();", $pdf[count($pdf) - 1]);
                $pdf[count($pdf) - 1] = $val;
                $pdf = implode('/', $pdf);
                $PD[] = $pdf;
            }
            $Pumps[$k]['description'] = $PD;
        }
    }
    foreach ($Pumps as $k => $Pump)
    {
        if ($k < 4)
        {
            $Nasos['Насосы ГНОМ'][] = $Pump;
        } elseif ($k > 3 && $k < 7)
        {
            $Nasos['Насосы фекальные и канализационные ЦМФ, ЦМК, НПК'][] = $Pump;
        } else
        {
            $Nasos[$Pump['title']][] = $Pump;
        }
    }
//
    file_put_contents(__DIR__ . '/results/content-ready-to-load.php', "<?php \n    return " . var_export($Nasos, true) . ";");
}