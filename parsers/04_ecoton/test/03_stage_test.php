<?php
/*
 * Get: Items Links
 */

define('__HOMEDIR__', __DIR__);
require_once __DIR__ . '/../../core.php';

SetCat();
function SetCat()
{
    global $Dom;

    for ($index_test = 0; $index_test < 11; $index_test++)
    {

        $Links = require_once __DIR__ . '/../results/Items_links/' . $index_test . '_links.php';
        $Post = [];

        foreach ($Links as $link)
        {
            $Page = [];
            $Dom->loadFromURL('http://ekoton.com' . $link);
            $Page = $Dom->find('#primary');
            $Header = strip_tags($Page->find('.summary h1')->innerHtml);
            $Content = strip_tags($Page->find('.summary div')->innerHtml);
            $Charact = [];

            foreach ($Page->find('.dt-sc-tabs-frame li a') as $Item_id => $List)
            {
                if ($List->text == 'Описание')
                {
                    foreach ($Page->find('.dt-sc-tabs-frame-content img') as $Img)
                    {
                        $Img->setAttribute('src', '/wp-content/uploads/2016/09' . $Img->src);
                    }
                    $Description = $Page->find('.dt-sc-tabs-frame-content')[$Item_id]->innerHtml;
                    $Charact['Descripton'] = $Description;
                }
                if ($List->text == 'Принцип действия')
                {
                    $Operating_Principle = $Page->find('.dt-sc-tabs-frame-content')[$Item_id]->innerHtml;
                    $Charact['Operating_Principle'] = $Operating_Principle;
                }
                if ($List->text == 'Преимущества')
                {
                    $Advantage = $Page->find('.dt-sc-tabs-frame-content')[$Item_id]->innerHtml;
                    $Charact['Advantage'] = $Advantage;
                }
                if ($List->text == 'Характеристики')
                {
                    $Characteristics = $Page->find('.dt-sc-tabs-frame-content')[$Item_id]->innerHtml;
                    $Charact['Characteristics'] = $Characteristics;
                }
                if ($List->text == 'Автоматизация')
                {
                    $Automation = $Page->find('.dt-sc-tabs-frame-content')[$Item_id]->innerHtml;
                    $Charact['Automation'] = $Automation;
                }
                if ($List->text == 'Комплект поставки')
                {
                    $Contents_of_delivery = $Page->find('.dt-sc-tabs-frame-content')[$Item_id]->innerHtml;
                    $Charact['Contents_of_delivery'] = $Contents_of_delivery;
                }
            }
            $Images = [];
            $Images_alt = [];
            foreach ($Dom->find('#bx-pager a img') as $Image)
            {
                $Images[] = '/wp-content/uploads/2016/09' . $Image->src;
                $Images_alt[] = $Image->alt;
            }

            $Post[] = [
                'Content' => $Content,
                'Header' => $Header,
                'Images_src' => $Images,
                'Images_alt' => $Images_alt,
                'Characteristics' => $Charact,
            ];
        }
        file_put_contents(__DIR__ . '/../results/Items/' . $index_test . '_Item.php', "<?php \n    return " . var_export($Post, true) . ";");
    }
}
