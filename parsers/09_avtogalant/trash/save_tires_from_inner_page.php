<?php

define('__HOMEDIR__', __DIR__);

require_once __DIR__ . '/../core.php';

//Все страницы которые нужно спарсить
//$fetched_links = include 'results/links.php';
$fetched_links = include 'results/links.php';
$all_to_parse = count($fetched_links);


//Все спарсенные
$parsed_items = include __DIR__ . '/results/allInnerDom.php';
$counter = count($parsed_items);

$date = date('m/d/Y h:i:s a', time());
$time_start = microtime(true);

echo '<br> Last item parsed ';
print_r($counter);
echo '<br> Items to parse ';
print_r($all_to_parse);
echo '<br> Total items before parse ';
print_r($counter);
echo '<br>';
echo '<br> Parsing items...';
///////////////////////
//Execution part start
///////////////////////
for ($i = $counter; $i < $all_to_parse; $i++) {
    echo '' . $counter . ' ';
    domRetrieve($i, $fetched_links[$i], $Dom);
}
/////////////////////
//Execution part end
////////////////////
$date2 = date('m/d/Y h:i:s a', time());
$time_end = microtime(true);
$execution_time = ($time_end - $time_start) / 60;
echo '<br>  ';
echo '<br> Total items after parse ';
print_r($counter);

echo '<br>  ';
echo '<br> Time started  ';
echo $date;
echo '<br> Time ended  ';
echo $date2;
echo '<br> Parsing Execution time ';
echo $execution_time;

function domRetrieve($i, $url, $Dom)
{
    global $parsed_items;
    global $ItemsHolder;
    global $counter;

    $curDom = $Dom->loadFromUrl($url);

    $category_rus = '';
    $category_en = '';
    if ($Dom->find('.breadcrumbs__link')[2]) {
        $category_rus = $Dom->find('.breadcrumbs__link')[2]->find('span')->text;
        $de_la_href = $Dom->find('.breadcrumbs__link')[2]->href;
        $category_en = substr($de_la_href, strrpos($de_la_href, '/') + 1);
    }
    if (!empty($Dom->find('.offers-form__description_small')->text)) {
        $manufacturer = $Dom->find('.offers-form__description_small')->text;
    } else {
        $manufacturer = '';
    }


//    $descr_short = $Dom->find('.offers-description__specs')->firstChild()->text;
    $descr = [];

    foreach ($Dom->find('.product-tip__term') as $key => $link1) {
        $descr[$key]['title'] = $link1->text;
    }

    foreach ($Dom->find('.value__text') as $key => $link1) {
        $descr[$key]['description'] = $link1->text;
    }

    $Image = [];

    foreach ($Dom->find('.js-gallery-thumb') as $key => $Images) {
        $original_src = $Images->getAttribute('data-original');

        $Image[$key]['title'] = $Images->find('.product-gallery__thumb-img')->getAttribute('alt');
        $Image[$key]['src_full'] = $original_src;
        $Image[$key]['src_thumb'] = $Images->find('.product-gallery__thumb-img')->src;

        $url = $original_src;
        $path = __DIR__ . '/results/img/tires/' . substr($original_src, strrpos($original_src, '/') + 1);

        if (!file_exists($path)) {
            file_put_contents($path, file_get_contents($url));
        }
    }

    $ItemsHolder['images'] = $Image;
    $ItemsHolder['descriptions'] = $descr;
    $ItemsHolder['manufacturer'] = $manufacturer;
    $ItemsHolder['category_original'] = $category_rus;
    $ItemsHolder['category_en'] = $category_en;

    $parsed_items[$counter] = $ItemsHolder;

    file_put_contents(__DIR__ . '/results/allInnerDom.php', "<?php \n    return " . var_export($parsed_items, true) . ";");

    unset($curDom);
    unset($Images);
    unset($item);
    $counter++;
}