    <?php

class Posts extends Illuminate\Database\Eloquent\Model
{
    public $timestamps = false;
    protected $table = "wp_posts";

    public function PostMeta() {
        return $this->hasMany("wp_term_taxonomy" , "term_id", "term_taxonomy_id");
    }
}