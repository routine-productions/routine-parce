<?php
return array(
    'Однооборотные' =>
        array('0' =>
            array('href' => 'products/odnobortnie/kpmini/',
                'title' => 'KP MINI'
            ),
            '1' =>
                array('href' => 'products/odnobortnie/mok/',
                    'title' => 'MOK (Control)'
                ),
            '2' =>
                array('href' => 'products/odnobortnie/mokpeex/',
                    'title' => 'MOKP Eex'
                ),
            '3' =>
                array('href' => 'products/odnobortnie/moked/',
                    'title' => 'MOKED'
                ),
            '4' =>
                array('href' => 'products/odnobortnie/kpmidi/',
                    'title' => 'KP MIDI'
                ),
            '5' =>
                array('href' => 'products/odnobortnie/moka/',
                    'title' => 'MOKA'
                ),
        ),
    'Многооборотные' =>
        array('0' =>
            array('href' => 'products/mnogobortnie/moncontrol/',
                'title' => 'MON (Control)'
            ),

            '1' =>
                array('href' => 'products/mnogobortnie/mopcontol/',
                    'title' => 'MOP (Control)'
                ),

            '2' =>
                array('href' => 'products/mnogobortnie/moned/',
                    'title' => 'MONED'
                ),
            '3' =>
                array('href' => 'products/mnogobortnie/moped/',
                    'title' => 'MOPED'
                ),
            '4' =>
                array('href' => 'products/mnogobortnie/moeex/',
                    'title' => 'MO Eex'
                ),
            '5' =>
                array('href' => 'products/mnogobortnie/moa/',
                    'title' => 'MOA'
                ),
            '6' =>
                array('href' => 'products/mnogobortnie/moaoc/',
                    'title' => 'KMOA OC'
                ),
            '7' =>
                array('href' => 'products/mnogobortnie/monj/',
                    'title' => 'MONJ'
                ),
            '8' =>
                array('href' => 'products/mnogobortnie/monedj/',
                    'title' => 'MONEDJ'
                ),),
    'Рычажные' =>
        array('0' =>
            array('href' => 'products/richagnie/mpr/',
                'title' => 'MPR'
            ),
            '1' =>
                array('href' => 'products/richagnie/mpscontrol/',
                    'title' => 'MPS (Control)'
                ),
            '2' =>
                array('href' => 'products/richagnie/mpsed/',
                    'title' => 'MPSED'
                ),),
    'Прямоходные' =>
        array('0' =>
            array('href' => 'products/priamohodnie/mtncontrol/',
                'title' => 'MTN (Control)'
            ),
            '1' =>
                array('href' => 'products/priamohodnie/mtp/',
                    'title' => 'MTP'
                ),
            '2' =>
                array('href' => 'products/priamohodnie/mtned/',
                    'title' => 'MTNED'
                ),
            '3' =>
                array('href' => 'products/priamohodnie/mtped/',
                    'title' => 'MTPED'
                ),)
);