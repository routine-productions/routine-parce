<?php

define('__HOMEDIR__', __DIR__);

require_once __DIR__ . '/../core.php';

$Links = [];

// Get: All pages
$Dom->loadFromUrl('http://xylem.ru/brands/lowara/products/');

// Get: All links
foreach ($Dom->find('#brand-sidebar-categories ul li a') as $key => $Link)
{
    $Links[$key]['href'] = $Link->href;
    $Links[$key]['title'] = $Link->text;
}

foreach ($Links as $key => $Link)
{
    $Dom->loadFromUrl($Link['href']);
    foreach ($Dom->find('.productlist li') as $k => $link)
    {
        $Parent_href[$k]['href'] = $link->find('a')->href;
        $Parent_href[$k]['title'] = $link->find('h5 a')->text;
    }
        $Links[$key]['items'] = $Parent_href;
//    $Links[$key]['parent_href']['link']['title'] = $Parent_title;

}

//print_r($Links);exit;
// Write: To file
file_put_contents(__DIR__ . '/results/01_links.php', "<?php \n    return " . var_export($Links, true) . ";");