<?php
class Modx_Site_Content extends Illuminate\Database\Eloquent\Model
{
    public $timestamps = false;
    protected $table = "modx_site_content";
}