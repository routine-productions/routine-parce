<?php

define('__HOMEDIR__', __DIR__);

require_once __DIR__ . '/../core.php';
$newses = require __DIR__ . '/results/news.php';


foreach ($newses as $key => $news)
{

    $Dom->loadFromUrl($news['href']);
    $newses[$key] = $news;

    $photo = $Dom->find('.paralax-mobile')->src;
    $photo = explode('/', $photo);

    //remove domain from link
    unset($photo[0]);
    unset($photo[1]);
    unset($photo[2]);

    $photo = '/' . implode('/', $photo);

    Save_File($photo, 'http://top15moscow.ru');
    $newses[$key]['main_image'] = $photo;

    $newses[$key]['views'] = $Dom->find('.category-article-item-view span')->text;
    $newses[$key]['comments'] = $Dom->find('.category-article-item-comments span')->text;
    $newses[$key]['likes'] = $Dom->find('.category-article-item-like', 0)->find('span')->text;
    $newses[$key]['created_at'] = $Dom->find('.category-article-item-like', 1)->find('span')->text;
    $newses[$key]['description'] = $Dom->find('.single-text-wrap')->innerHtml();
    foreach ($Dom->find('meta') as $meta)
    {

        if ($meta->getAttribute('name') == 'description')
        {
            $newses[$key]['meta_description'] = $meta->getAttribute('content');
        }
    }

}
//exit;


// Write: To file

file_put_contents(__DIR__ . '/results/news.php', "<?php \n    return " . var_export($newses, true) . ";");
