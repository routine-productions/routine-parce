<?php 
    return array (
  'dekoratory' => 
  array (
    0 => 
    array (
      'href' => 'http://top15moscow.ru/dekoratory/mariya-german/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2017/03/IMG_d.jpg',
      'name' => 'Мария',
      'surname' => 'Герман',
      'alias' => 'mariya-german',
      'phone' => '
              +7 965 342-73-30',
      'meta_description' => ' Мария Герман — основатель творческой студии декора Maria German ',
      'email' => 'www.mariagerman.com',
      'profession' => 'Декоратор',
      'description' => '
              <p>Мария Герман превращает воображаемое в реальность, создавая  незабываемый декор. Финалист  Wedding Awards 2016</p>
              
              <div class=""> <p>Мария Герман давно хотела осуществить мечту  и заниматься тем, что по-настоящему любит – декором и флористикой. Поэтому в 2012 году появилась творческая студия Maria German.</p>
<p>К тому моменту у основательницы был уже большой опыт работы в сфере выставочного производства, который стал основой для реализации новых идей по оформлению мероприятий.</p>
<blockquote><p>«Глотком свежего воздуха» послужило обучения в одной из самых знаменитых западных школ по декору «Николь», а потом в Британской Школе Дизайна в Москве.</p></blockquote>
<p>Каждая работа студии уникальна, но при этом  выполнена в едином стиле и концепции.</p>
<p>Главе студии Maria German  под силу любой масштаб и самые креативные идеи, ведь у нее в арсенале собственное производство и огромный энтузиазм.</p>
<p>Так что, если для воплощения мечты заказчиков потребуется создать настоящий французский сад на берегу реки в Подмосковье, нет ничего невозможного!</p>
 </div>
            ',
      'short_about' => 'Кредо Марии как декоратора — это каждая деталь определяет настроение мероприятия, а все вместе они создают грандиозное событие, которое останется в сердце клиента навсегда.',
      'photos' => 
      array (
        0 => '/wp-content/uploads/2017/03/3.jpg',
        1 => '/wp-content/uploads/2017/03/IMG_de1-1024x683.jpg',
        2 => '/wp-content/uploads/2017/03/2.jpg',
        3 => '/wp-content/uploads/2017/03/16999234_406156759736322_7616601167565913254_n.jpg',
        4 => '/wp-content/uploads/2017/03/16998741_405247346493930_2218873348915645269_n.jpg',
        5 => '/wp-content/uploads/2017/03/16865023_404373476581317_1515258948456077750_n.jpg',
        6 => '/wp-content/uploads/2017/03/16996098_404373443247987_7539601720038527883_n.jpg',
        7 => '/wp-content/uploads/2017/03/16864421_403362830015715_4257637828811336029_n.jpg',
        8 => '/wp-content/uploads/2017/03/17103832_407902726228392_4910798871319262953_n.jpg',
        9 => '/wp-content/uploads/2017/03/16684002_404373419914656_731327667266916978_n.jpg',
        10 => '/wp-content/uploads/2017/03/1.jpg',
        11 => '/wp-content/uploads/2017/03/5.jpg',
      ),
    ),
    1 => 
    array (
      'href' => 'http://top15moscow.ru/dekoratory/dina-yakushina/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/08/16.07.27-top15Moscow-453.jpg',
      'name' => 'Дина',
      'surname' => 'Якушина',
      'alias' => 'dina-yakushina',
      'phone' => '
              +7 903 511-42-00',
      'meta_description' => ' Дина Якушина — декоратор, специализирующийся на свадьбах за рубежом ',
      'email' => 'www.dinayakushina.com',
      'profession' => 'Декоратор',
      'description' => '
              <p>Дина Якушина — профессиональный декоратор, она работает  с  командой талантливейших флористов и дизайнеров. Живёт в Подмосковье, работает в Москве, других городах России и зарубежных странах.</p>
              
              <div class=""> <blockquote><p>Я люблю создавать уникальные проекты, ведать жизненную историю молодожёнов и подчеркивать красоту того места, где проводится свадьба.</p></blockquote>
<p>За годы своей карьеры Дина оформила сотни свадеб не только в своей родной стране, но и в Италии, Франции, Монако, Черногории, Греции и Австралии.</p>
<p>В числе заказчиков декоратора Дины Якушиной крупнейшие и лучшие свадебные агентства: «Для двоих», «Tobelove Wedding» и «A2 Wedding».</p>
<p>Дина Якушина стремится к тому, чтобы  рынок декора стал стильным и интересным.</p>
<p>По этой причине она преподаёт в школе «Moscow Flower School», в которой ведёт занятия по свадебным церемониям на базовых и интенсивных курсах по свадебному декору.</p>
 </div>
            ',
      'short_about' => 'Проекты Дины Якушиной отличаются друг от друга по стилю. История каждой пары превращается в сказочную картину любви.',
      'photos' => 
      array (
        0 => '/wp-content/uploads/2016/08/Decorator-Dina-Yakushina30-1024x682.jpg',
        1 => '/wp-content/uploads/2016/08/Decorator-Dina-Yakushina1-1024x683.jpg',
        2 => '/wp-content/uploads/2016/08/Decorator-Dina-Yakushina26-683x1024.jpg',
        3 => '/wp-content/uploads/2016/08/Decorator-Dina-Yakushina27-1024x683.jpg',
        4 => '/wp-content/uploads/2016/08/Decorator-Dina-Yakushina28-1024x683.jpg',
        5 => '/wp-content/uploads/2016/08/Decorator-Dina-Yakushina29.jpg',
        6 => '/wp-content/uploads/2016/08/Decorator-Dina-Yakushina2-1024x683.jpg',
        7 => '/wp-content/uploads/2016/08/Decorator-Dina-Yakushina3-1024x683.jpg',
        8 => '/wp-content/uploads/2016/08/Decorator-Dina-Yakushina4-683x1024.jpg',
        9 => '/wp-content/uploads/2016/08/Decorator-Dina-Yakushina5-683x1024.jpg',
      ),
    ),
    2 => 
    array (
      'href' => 'http://top15moscow.ru/dekoratory/flowers-lovers/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/08/CZ1V2775.jpg',
      'name' => 'FlowersLovers',
      'alias' => 'flowers-lovers',
      'phone' => '
              +7 985 911-57-49',
      'meta_description' => ' FlowersLovers — мастерская цветов и декора, сохраняя стиль и индивидуальность клиента ',
      'email' => 'www.http://flowerslovers.ru/',
      'profession' => 'Декоратор',
      'description' => '
              <p>В приоритете мастерской максимальное внимание к деталям, природность и натуральность во флористике, тонкая работа с цветом и оттенками. Флористы стараются избегать театральных масштабных декораций и глобальных конструкций, вписывая декор в само пространство для праздника, а не переделывая его, раскрывая красоту цветов и подчеркивая душевность момента.</p>
              
              <div class=""> <blockquote><p>FlowersLovers не делают свадебные проекты, они создают иллюстрации к новой странице совместной истории пары. Для каждой истории любви — своя иллюстрация!</p></blockquote>
<p>FlowersLovers — это команда! Менеджеры проектов, декораторы, флористы — люди, которые любят свое дело и вместе с вами стремятся к лучшему результату в подготовке вашего дня. Эти профессионалы не только видят и находят красоту вокруг себя, но и создают ее, вдохновляясь историями и эмоциями.</p>
<p>Перфекционизм, душевность и немного юмора — основы основ в работе мастрерской. Они позволят с удовольствием прожить волнительный период подготовки к важному дню и получить праздник с тонким вкусом и безупречной флористикой.</p>
 </div>
            ',
      'short_about' => 'Перфекционизм, душевность и немного юмора — основы основ в работе FlowersLovers. Они позволят паре с удовольствием прожить волнительный период подготовки к их дню и получить праздник с тонким вкусом и безупречной флористикой!',
      'photos' => 
      array (
        0 => '/wp-content/uploads/2016/08/flowers-lovers23-683x1024.jpg',
        1 => '/wp-content/uploads/2016/08/flowers-lovers24-683x1024.jpg',
        2 => '/wp-content/uploads/2016/08/flowers-lovers25-1024x683.jpg',
        3 => '/wp-content/uploads/2016/08/flowers-lovers26-757x1024.jpg',
        4 => '/wp-content/uploads/2016/08/flowers-lovers27-683x1024.jpg',
        5 => '/wp-content/uploads/2016/08/flowers-lovers28-768x1024.jpg',
        6 => '/wp-content/uploads/2016/08/flowers-lovers30-682x1024.jpg',
        7 => '/wp-content/uploads/2016/08/flowers-lovers31-761x1024.jpg',
        8 => '/wp-content/uploads/2016/08/flowers-lovers32-687x1024.jpg',
        9 => '/wp-content/uploads/2016/08/flowers-lovers29-683x1024.jpg',
      ),
    ),
    3 => 
    array (
      'href' => 'http://top15moscow.ru/dekoratory/mezhdu-nami/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/08/UUS_1081.jpg',
      'name' => 'Студия',
      'surname' => '«Между',
      'alias' => 'mezhdu-nami',
      'phone' => '
              +7 964 539-78-49',
      'meta_description' => ' «Между Нами» — Лучшие флористы 2016 года по версии журнала Wedding Magazine Russia ',
      'email' => 'www.www.instagram.com/mezhdu_nami_',
      'profession' => '',
      'description' => '
              <p>Задача студии — отразить красоту отношений каждой пары в неповторимом декоре.</p>
<p>Между Нами — это студия с новым свежим взглядом на декор. Компания стремительно завоевала уважение коллег и любовь клиентов в Москве и других городах страны.</p>
              
              <div class=""> <p>Мы сотрудничаем с топовыми свадебными агентствами и лучшими площадками, создаём свадебные концепции для других декораторов по всей России.</p>
<blockquote><p>Между Нами — это то, что создается декораторами вместе c молодоженами</p></blockquote>
<p>Оформления студии — это Ваша уникальная история каждой пары, которую декораторы помогают сохранить в памяти, как самое красивое событие в жизни.</p>
 </div>
            ',
      'short_about' => '«Между Нами» — минимализм, элегантность и современность, наш узнаваемый стиль',
      'photos' => 
      array (
        0 => '/wp-content/uploads/2016/08/mezhdu-nami23-1024x792.jpg',
        1 => '/wp-content/uploads/2016/08/mezhdu-nami24-1024x691.jpg',
        2 => '/wp-content/uploads/2016/08/mezhdu-nami25-765x1024.jpg',
        3 => '/wp-content/uploads/2016/08/mezhdu-nami26-1024x805.jpg',
        4 => '/wp-content/uploads/2016/08/mezhdu-nami27-683x1024.jpg',
        5 => '/wp-content/uploads/2016/08/mezhdu-nami28-683x1024.jpg',
        6 => '/wp-content/uploads/2016/08/mezhdu-nami29-1024x683.jpg',
        7 => '/wp-content/uploads/2016/08/mezhdu-nami31-1024x631.jpg',
        8 => '/wp-content/uploads/2016/08/mezhdu-nami32-683x1024.jpg',
        9 => '/wp-content/uploads/2016/08/mezhdu-nami33-684x1024.jpg',
      ),
      'videos' => 
      array (
        0 => 'https://player.vimeo.com/video/181043442',
      ),
    ),
    4 => 
    array (
      'href' => 'http://top15moscow.ru/dekoratory/flowerbazar/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/08/321546546.jpg',
      'name' => 'Flowerbazar',
      'alias' => 'flowerbazar',
      'phone' => '
              +7 495 005-19-82',
      'meta_description' => ' FLOWERBAZAR создает стильные, запоминающиеся свадьбы, в которых масштаб гармонично сочетается с вниманием к деталям.  ',
      'email' => 'www.flowerbazar.ru',
      'profession' => 'Декораторы',
      'description' => '
              <p>Мастерская цветов и декора FLOWERBAZAR создана в 2011 году, после учебы у американского свадебного флориста Holly Heider Chapple. Этот необычный опыт очень пригодился им в начале пути и помог не потеряться среди множества молодых проектов.</p>
              
              <div class=""> <blockquote><p>В своей работе мы сохраняем разумный баланс между творческим полетом и ответственным отношением к каждой свадьбе.</p></blockquote>
<p>За 5 лет работы команда приобрела свой стиль, работала на самых известных площадках Москвы и области, а так же участвовала в выездных проектах в Сочи, Казани, Ялте и Испании. Сильные стороны Flowerbazar известны всем: это изысканная флористика, гармоничный декор пространства, профессиональная команда, подробные сметы и чёткая работа от эскиза до реализации. И успешное сотрудничество с весьма требовательными клиентами и агентствами служит тому подтверждением.</p>
<p>FLOWERBAZAR — победители самой престижной премии в свадебной индустрии России Wedding Awards 2015 в номинации «Лучшие свадебные флористы», а также IZWA 2016 (International Zankyou Wedding Awards) в номинации «Студия декора<br>
и флористики».</p>
 </div>
            ',
      'short_about' => 'В поисках профессиональных сотрудников руководитель студии Ольга Белецкая создала свой образовательный проект Moscow Flower School, благодаря которому смогла собрать вокруг себя надежную и умелую команду.',
      'photos' => 
      array (
        0 => '/wp-content/uploads/2016/08/flower-bazar2-1024x683.jpg',
        1 => '/wp-content/uploads/2016/08/flower-bazar1-683x1024.jpg',
        2 => '/wp-content/uploads/2016/08/flower-bazar3-1024x683.jpg',
        3 => '/wp-content/uploads/2016/08/flower-bazar24.jpg',
        4 => '/wp-content/uploads/2016/08/flower-bazar25-1024x682.jpg',
        5 => '/wp-content/uploads/2016/08/flower-bazar26-1024x684.jpg',
        6 => '/wp-content/uploads/2016/08/flower-bazar27-1024x683.jpg',
        7 => '/wp-content/uploads/2016/08/flower-bazar28-1024x683.jpg',
        8 => '/wp-content/uploads/2016/08/flower-bazar23-1024x682.jpg',
        9 => '/wp-content/uploads/2016/08/flower-bazar22-1024x683.jpg',
      ),
      'videos' => 
      array (
        0 => 'https://player.vimeo.com/video/179455261',
        1 => 'https://player.vimeo.com/video/179455325',
        2 => 'https://player.vimeo.com/video/179455184',
        3 => 'https://player.vimeo.com/video/179455398',
      ),
    ),
    5 => 
    array (
      'href' => 'http://top15moscow.ru/dekoratory/late-dekor/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/08/CZ1V2864.jpg',
      'name' => 'Latte',
      'surname' => 'Décor',
      'alias' => 'late-dekor',
      'phone' => '
              +7 926 524-21-18',
      'meta_description' => ' Студия Latte Décor — декораторы, превращающие работу в искусство ',
      'email' => 'www.lattedecor.ru',
      'profession' => 'Декораторы',
      'description' => '
              <p>Карина Донерова — профессиональный декоратор, любящий работать в различных направлениях. После того, как Карина получила художественное образование, она начала работать декоратором в области интерьеров.</p>
              
              <div class=""> <blockquote><p>Карина начала свой карьерный путь с небольших фотосессий, а уже год спустя стала заниматься масштабными свадебными проектами.</p></blockquote>
<p>Она всегда знала, что моя профессия так или иначе будет связана с дизайном. Карина смогла быстро добиться успеха и стать профи в области оформления декораций. Она основала компанию «Latte Décor», объединившую в себе лучших дизайнеров, декораторов и флористов.</p>
<p>Компания работает уже более 6 лет, стремясь создавать «картинку вне времени» на любом событии. По словам Карины, в своей работе «Latte Décor» использует индивидуальный подход, чтобы оформить мероприятие незабываемо и неповторимо.</p>
 </div>
            ',
      'short_about' => 'По мнению руководителя студии, декор — важнейший компонент в образе бракосочетания. И то, как его исполнить, это и есть настоящее искусство.',
      'photos' => 
      array (
        0 => '/wp-content/uploads/2016/08/latte-decor23-1024x683.jpg',
        1 => '/wp-content/uploads/2016/08/latte-decor24-683x1024.jpg',
        2 => '/wp-content/uploads/2016/08/latte-decor25-683x1024.jpg',
        3 => '/wp-content/uploads/2016/08/latte-decor26-1024x771.jpg',
        4 => '/wp-content/uploads/2016/08/latte-decor27-771x1024.jpg',
        5 => '/wp-content/uploads/2016/08/latte-decor28-1024x684.jpg',
        6 => '/wp-content/uploads/2016/08/latte-decor29-1024x684.jpg',
        7 => '/wp-content/uploads/2016/08/latte-decor30-1024x684.jpg',
        8 => '/wp-content/uploads/2016/08/latte-decor31-1024x681.jpg',
        9 => '/wp-content/uploads/2016/08/latte-decor32-680x1024.jpg',
      ),
    ),
    6 => 
    array (
      'href' => 'http://top15moscow.ru/dekoratory/mariya-kamenskaya/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/08/16.07.27-top15Moscow-725.jpg',
      'name' => 'Мария',
      'surname' => 'Каменская',
      'alias' => 'mariya-kamenskaya',
      'phone' => '
              +7 926 582-22-33',
      'meta_description' => ' Мария Каменская — Победитель премии Wedding Awards 2016 в номинации "Лучший декоратор" ',
      'email' => 'www.kamenskaya.ru',
      'profession' => 'Декоратор',
      'description' => '
              <p>Мария Каменская прошла нелегкий, тернистый путь по карьерной лестнице в области декора и дизайна. Когда-то она была никому не известной, а сегодня основала свою компанию «MK DÉCOR and DESIGN», в которой объединила лучшие творческие умы: профессионалов в среде флористики, декора, дизайна, архитектуры и производства декораций.</p>
              
              <div class=""> <blockquote><p>Про Марию часто говорят, что она сделала в очередной своей работе невозможное, но сама она всегда очень придирчива к результатам своего труда.</p></blockquote>
<p>Сегодня Мария вместе со своими сотрудниками создаёт ярчайшие декорации. На каждый проект она выделяет целую команду людей, которые влюблены в свою профессию. Она считает, что именно сплоченная команда профессионалов — залог успеха создания безупречных декораций и оформлений.</p>
<p>Портфолио Марии и её команды насчитывает десятки красивейших работ по декорации мероприятий, которые не оставят никого равнодушным.</p>
 </div>
            ',
      'short_about' => 'Мария пришла в сферу декора из флористики. Вдоволь назанимавшись протокольными мероприятиями она поняла, что душой тяготеет к оформлению свадеб: к их красоте и размаху.',
      'photos' => 
      array (
        0 => '/wp-content/uploads/2016/08/Decorator-Maria-Kamenskaya40-1024x1024.jpg',
        1 => '/wp-content/uploads/2016/08/Decorator-Maria-Kamenskaya41-1024x778.jpg',
        2 => '/wp-content/uploads/2016/08/Decorator-Maria-Kamenskaya42-683x1024.jpg',
        3 => '/wp-content/uploads/2016/08/Decorator-Maria-Kamenskaya43-1024x683.jpg',
        4 => '/wp-content/uploads/2016/08/Decorator-Maria-Kamenskaya39-1024x683.jpg',
        5 => '/wp-content/uploads/2016/08/Decorator-Maria-Kamenskaya38-1024x683.jpg',
        6 => '/wp-content/uploads/2016/08/Decorator-Maria-Kamenskaya37-683x1024.jpg',
        7 => '/wp-content/uploads/2016/08/Decorator-Maria-Kamenskaya36-1024x1024.jpg',
        8 => '/wp-content/uploads/2016/08/Decorator-Maria-Kamenskaya35-1024x684.jpg',
        9 => '/wp-content/uploads/2016/08/Decorator-Maria-Kamenskaya34-1024x684.jpg',
      ),
      'videos' => 
      array (
        0 => 'https://player.vimeo.com/video/174785595',
        1 => 'https://player.vimeo.com/video/179477150',
        2 => 'https://player.vimeo.com/video/172302557',
        3 => 'https://player.vimeo.com/video/163764594',
        4 => 'https://player.vimeo.com/video/161185044',
        5 => 'https://player.vimeo.com/video/159993879',
        6 => 'https://player.vimeo.com/video/151128001',
        7 => 'https://player.vimeo.com/video/171024523',
      ),
    ),
    7 => 
    array (
      'href' => 'http://top15moscow.ru/dekoratory/kovalishin/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/08/16.07.27-top15Moscow-525.jpg',
      'name' => 'Роман',
      'surname' => 'Ковалишин',
      'alias' => 'kovalishin',
      'phone' => '
              +7 985 784-42-80',
      'meta_description' => ' Kovalishin — победитель премий «Событие Года 2016» и Wedding Awards 2015 ',
      'email' => 'www.kovalishin.ru',
      'profession' => 'Декоратор',
      'description' => '
              <p>Компания «KOVALISHIN» — самая дружная команда. Креативные дизайнеры, флористы, декораторы и менеджеры во главе с Романом Ковалишиным способны выполнить декорацию любой сложности.</p>
              
              <div class=""> <blockquote><p>Декораторы любят проекты, имеющие концепцию и придумывают яркие образы через ассоциации, воплощая в жизнь смелые, неожиданные решения.</p></blockquote>
<p>Отличительной особенностью этой компании является особое трудолюбие — они работают, чтобы заказчики остались довольны результатом.</p>
<p>За время своего существования компания «KOVALISHIN» оформила более 300 мероприятий, объездив при этом 15 стран мира. Сотрудники компании работали и в Алжире, и на Аляске, но самым прекрасным местом считают Гималаи.</p>
<p>Любимые Клиенты и Партнеры:<br>
Dolce&amp;Gabbana, Four Seasons Hotel Moscow, BMW, ВТБ, Conde Nast Moscow (Conde Nast Traveller, Tatler, Vogue), Bulgary, Van Cleef, Meucci, Porsche, РАО ЕЭС, S7, Omega, La Mer, Infinity, Zara, INSTYLE, Moscow Fashion Week, Ginza Project, Master Card, Fomin Production, Louder, Wonderloft, RSVP, Departament, Event Cube и другие.</p>
<p>Компания «KOVALISHIN» также примечательна своей мобильностью, поэтому не отказывает и в срочных «горящих» проектах. Все сотрудники предварительно прошли обучение у ведущих мировых специалистов: Олафа Щрерса, Даниэля Оста, Грегора Лерша, Тора Гундерсена, а потому ни у кого не возникает вопросов относительно их компетентности.</p>
 </div>
            ',
      'short_about' => 'KOVALISHIN исполняет мечты, реализовывает фантазии и дарит настоящую сказку тем, кого вы любите.',
      'photos' => 
      array (
        0 => '/wp-content/uploads/2016/08/Decorator-Roman-Kovalishin39-1024x637.jpg',
        1 => '/wp-content/uploads/2016/08/Decorator-Roman-Kovalishin40-1024x752.jpg',
        2 => '/wp-content/uploads/2016/08/Decorator-Roman-Kovalishin41-683x1024.jpg',
        3 => '/wp-content/uploads/2016/08/Decorator-Roman-Kovalishin42-1024x684.jpg',
        4 => '/wp-content/uploads/2016/08/Decorator-Roman-Kovalishin43-1024x684.jpg',
        5 => '/wp-content/uploads/2016/08/Decorator-Roman-Kovalishin44-1024x684.jpg',
        6 => '/wp-content/uploads/2016/08/Decorator-Roman-Kovalishin45-1024x683.jpg',
        7 => '/wp-content/uploads/2016/08/Decorator-Roman-Kovalishin46-1024x684.jpg',
        8 => '/wp-content/uploads/2016/08/Decorator-Roman-Kovalishin47-1024x683.jpg',
        9 => '/wp-content/uploads/2016/08/Decorator-Roman-Kovalishin48-1024x683.jpg',
      ),
    ),
    8 => 
    array (
      'href' => 'http://top15moscow.ru/dekoratory/shakirova-julia/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/08/16.07.28-Top15-Moscow-130.jpg',
      'name' => 'Юлия',
      'surname' => 'Шакирова',
      'alias' => 'shakirova-julia',
      'phone' => '
              +7 495 943-33-82',
      'meta_description' => ' SHACKIROVA STUDIO — трендсеттер в оформлении масштабных частных торжеств ',
      'email' => 'www.shakirovajulia.ru',
      'profession' => 'Декоратор',
      'description' => '
              <p>Юлия Шакирова — Победитель премии Wedding Awards 2016 в номинации «Персона года».</p>
<p>Командой студии реализовано уже больше 2500 проектов, среди которых – оформление Константиновского дворца в Санкт-Петербурге, лучших площадок Москвы, французских замков, швейцарских шале, а также знаменитых винных погребов Эперне. Создатель бренда SHACKIROVA STUDIO – Юлия Шакирова — талантливый декоратор c более чем десятилетним опытом работы.</p>
              
              <div class=""> <blockquote><p>Все работы Юлии Шакировой – это произведения искусства, которые в любом пространстве способны создать впечатление другой реальности со своей особенной атмосферой.</p></blockquote>
<p>Юлия с детства интересовалась творчеством, в особенности прикладным искусством. Потом начала заниматься флористикой: прошла обучение в школе международного уровня «Николь», обучалась у флористов с мировым именем – Элли Лин, Валли Клетт, Даниэля Оста.</p>
<p>Полученные в области флористики знания определили путь дальнейшего развития уже в сфере декора. На сегодняшний день оформление «от Юлии Шакировой» – престижно, узнаваемо, оригинально.</p>
 </div>
            ',
      'short_about' => 'Несмотря на креативные, смелые и каждый раз нетривиальные решения, все проекты SHACKIROVA STUDIO имеют свой узнаваемый почерк и созданы с безупречным вкусом. Нестандартный подход к работе в совокупности с творческим мышлением и необычным стилем помогли Юлии стать одним из самых известных декораторов мира.',
      'photos' => 
      array (
        0 => '/wp-content/uploads/2016/08/Decorator-Julia-Shakirova39-1024x683.jpg',
        1 => '/wp-content/uploads/2016/08/Decorator-Julia-Shakirova41-1024x683.jpg',
        2 => '/wp-content/uploads/2016/08/Decorator-Julia-Shakirova43-1024x683.jpg',
        3 => '/wp-content/uploads/2016/08/Decorator-Julia-Shakirova45-1024x683.jpg',
        4 => '/wp-content/uploads/2016/08/Decorator-Julia-Shakirova47-1024x683.jpg',
        5 => '/wp-content/uploads/2016/08/Decorator-Julia-Shakirova49-683x1024.jpg',
        6 => '/wp-content/uploads/2016/08/Decorator-Julia-Shakirova51-1024x683.jpg',
        7 => '/wp-content/uploads/2016/08/Decorator-Julia-Shakirova53-1024x683.jpg',
        8 => '/wp-content/uploads/2016/08/Decorator-Julia-Shakirova55-1024x683.jpg',
        9 => '/wp-content/uploads/2016/08/Decorator-Julia-Shakirova57-1024x683.jpg',
      ),
      'videos' => 
      array (
        0 => 'https://player.vimeo.com/video/181231138',
        1 => 'https://player.vimeo.com/video/181231129',
        2 => 'https://player.vimeo.com/video/181231123',
        3 => 'https://player.vimeo.com/video/181231115',
      ),
    ),
    9 => 
    array (
      'href' => 'http://top15moscow.ru/dekoratory/tamara-valenkova/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/08/16.07.27-top15Moscow-108.jpg',
      'name' => 'Тамара',
      'surname' => 'Валенкова',
      'alias' => 'tamara-valenkova',
      'phone' => '
              +7 906 730-29-66',
      'meta_description' => ' Тамара Валенкова — декоратор, предлагающий только нетривиальные концепции ',
      'email' => '',
      'profession' => 'Декоратор',
      'description' => '
              <p>Тамара Валенкова — профессиональный декоратор, ценящий нестандартный, творческий подход к делу. В работе создаёт качественный и гармоничный декор в абсолютно разных стилях.</p>
              
              <div class=""> <blockquote><p>Тамара убеждена, что творческими людьми не становятся, а рождаются. Поэтому не она выбрала профессию, а профессия — ее.</p></blockquote>
<p>Тамара занимается оформлением бракосочетаний уже более 8 лет.</p>
<p>Время создания оформления зависит от объёма. По словам Тамары, небольшое событие можно оформить и за неделю, но для масштабного мероприятия нужно, как минимум, от двух недель до одного месяца.</p>
<p>Чтобы декорации выглядели лёгкими, красивыми и смотрелись к месту, разрабатывая концепцию, учитываются, прежде всего, площадку, в которой будет происходить свадьба.</p>
<p>В своей работе Тамара Валенкова ценит нестандартный подход и ищет нетривиальные решения, чтобы свадьбы, которые она оформляет, навсегда оставались запечатлёнными в памяти молодожёнов.</p>
 </div>
            ',
      'short_about' => 'Мечты у всех разные и пути их осуществления — тоже. Будь то уютная церемония в сказочном лесу или королевский прием, поражающий роскошью и величием, команда Тамары Валенковой сможет воплотить все ваши сокровенные мечты.',
      'photos' => 
      array (
        0 => '/wp-content/uploads/2016/08/Decorator-Tamara-Valentinova19-1024x683.jpg',
        1 => '/wp-content/uploads/2016/08/Decorator-Tamara-Valentinova20-683x1024.jpg',
        2 => '/wp-content/uploads/2016/08/Decorator-Tamara-Valentinova21-1024x789.jpg',
        3 => '/wp-content/uploads/2016/08/Decorator-Tamara-Valentinova22-1024x794.jpg',
        4 => '/wp-content/uploads/2016/08/Decorator-Tamara-Valentinova23-1024x641.jpg',
        5 => '/wp-content/uploads/2016/08/Decorator-Tamara-Valentinova24-1024x552.jpg',
        6 => '/wp-content/uploads/2016/08/Decorator-Tamara-Valentinova25-1007x1024.jpg',
        7 => '/wp-content/uploads/2016/08/Decorator-Tamara-Valentinova26-1024x683.jpg',
        8 => '/wp-content/uploads/2016/08/Decorator-Tamara-Valentinova27-1024x684.jpg',
        9 => '/wp-content/uploads/2016/08/Decorator-Tamara-Valentinova28-1024x763.jpg',
      ),
    ),
    10 => 
    array (
      'href' => 'http://top15moscow.ru/dekoratory/gennadij-samohin/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/08/16.07.28-Top15-Moscow-140.jpg',
      'name' => 'Геннадий',
      'surname' => 'Самохин',
      'alias' => 'gennadij-samohin',
      'phone' => '
              +7 903 204-22-33',
      'meta_description' => ' Геннадий Самохин — руководитель художественно-производственных мастерских с 16-летним опытом ',
      'email' => 'www.gensam.ru',
      'profession' => 'Декоратор',
      'description' => '
              <p>Геннадий Самохин родился и в настоящее время проживает в Москве. Обучался на архитектурном факультете государственного университета по землеустройству. В 1996 году начал работу арт-директором и художником в компании «Сцена». Геннадий тесно сотрудничал с известнейшим художником-сценографом России Борисом Красновым.</p>
              
              <div class=""> <blockquote><p>Для его компании не существует понятия “неинтересные клиенты”. Качественные услуги по декорированию предлагаются всем, кто в этом нуждается.</p></blockquote>
<p>В 2001 году Геннадий начал работать самостоятельно в качестве художника-сценографа в компании «Арт-Прожект», которая плотно заняла эту нишу и функционирует уже 16 лет, предлагая различные услуги по созданию декораций для концертов и шоу-программ, ТВ студий и выставочных стендов, корпоративных мероприятий/презентаций, свадеб и торжеств, интерьеров и др.</p>
<p>В числе его клиентов телеканал Россия, основатель и ведущий КВН Александр Масляков, корпорация SONY, телеканал «СТС», и другие акулы бизнеса.</p>
 </div>
            ',
      'short_about' => 'Компания создает потрясающие декорации как для небольших вечеринок, так и для крупнейших шоу-программ, при этом всегда находясь в поиске свежих, современных и нетривиальных решений.',
      'photos' => 
      array (
        0 => '/wp-content/uploads/2016/08/gennadiy-samikhin-21.jpg',
        1 => '/wp-content/uploads/2016/08/gennadiy-samikhin-22-1024x593.jpg',
        2 => '/wp-content/uploads/2016/08/gennadiy-samikhin-23-1024x768.jpg',
        3 => '/wp-content/uploads/2016/08/gennadiy-samikhin-24-1024x768.jpg',
        4 => '/wp-content/uploads/2016/08/gennadiy-samikhin-25-1024x768.jpg',
        5 => '/wp-content/uploads/2016/08/gennadiy-samikhin-26-1024x768.jpg',
        6 => '/wp-content/uploads/2016/08/gennadiy-samikhin-27-1024x684.jpg',
        7 => '/wp-content/uploads/2016/08/gennadiy-samikhin-28-1024x583.jpg',
        8 => '/wp-content/uploads/2016/08/gennadiy-samikhin-29-681x1024.jpg',
      ),
      'videos' => 
      array (
        0 => 'https://player.vimeo.com/video/179489904',
        1 => 'https://player.vimeo.com/video/179490109',
      ),
    ),
    11 => 
    array (
      'href' => 'http://top15moscow.ru/dekoratory/antizerskij/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/UUS_0656.jpg',
      'name' => 'Юрий',
      'surname' => 'Антизерский',
      'alias' => 'antizerskij',
      'phone' => '
              +7 903 724-38-50',
      'meta_description' => ' Декоратор Юрий Антизерский — архитектор, художник-постановщик с 30 летним опытом оформления мероприятий ',
      'email' => 'www.show-design.ru',
      'profession' => 'Декоратор',
      'description' => '
              <p>Юрий Антизерский, архитектор по образованию, начал свою карьеру в 1984 году с оформления  концертной программы Аллы Пугачевой «Пришла и говорю».  За более чем 30 лет работы было реализовано около 3000 проектов, среди них:  концерты, телевизионные программы, престижные конкурсы, фестивали, выставки,  свадебные торжества и корпоративные мероприятия.</p>
              
              <div class=""> <p><em>Опыт художника-постановщика многочисленных концертов и театральных спектаклей очень помогает в декоре свадеб и задает масштабный и неповторимый стиль Антизерского.</em></p>
<p>Команда Юрия состоит из настоящих профессионалов своего дела: дизайнеры, архитекторы, декораторы, флористы, бутафоры и монтажники, которые сдружились за время работы и понимают друг друга с полуслова, что очень важно при подготовке проекта.  Его мероприятия редко становятся достоянием общественности, фотографии жениха и невесты не принято показывать и лишь сам маэстро, его команда и почетные гости свадеб наслаждаются великолепием созданного им декора.</p>
 </div>
            ',
      'short_about' => '<em>Юрий Антизерский является признанным лидером свадебного дизайна и декора не только среди клиентов, но и среди профессионалов, являясь  членом жюри одной из самых престижных премий свадебной индустрии — WEDDING AWARDS</em>',
      'photos' => 
      array (
        0 => '/wp-content/uploads/2016/07/Yuriy-Antizerskiy-18-1024x683.jpg',
        1 => '/wp-content/uploads/2016/07/Yuriy-Antizerskiy-19-1024x683.jpg',
        2 => '/wp-content/uploads/2016/07/Yuriy-Antizerskiy-20-1024x683.jpg',
        3 => '/wp-content/uploads/2016/07/Yuriy-Antizerskiy-21-1024x683.jpg',
        4 => '/wp-content/uploads/2016/07/Yuriy-Antizerskiy-22-683x1024.jpg',
        5 => '/wp-content/uploads/2016/07/Yuriy-Antizerskiy-23-683x1024.jpg',
        6 => '/wp-content/uploads/2016/07/Yuriy-Antizerskiy-24-683x1024.jpg',
        7 => '/wp-content/uploads/2016/07/Yuriy-Antizerskiy-25-683x1024.jpg',
        8 => '/wp-content/uploads/2016/07/Yuriy-Antizerskiy-26-1024x683.jpg',
        9 => '/wp-content/uploads/2016/07/Yuriy-Antizerskiy-27-1024x683.jpg',
      ),
    ),
  ),
  'fotografy' => 
  array (
    0 => 
    array (
      'href' => 'http://top15moscow.ru/fotografy/katya-romanova/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/08/Fotograf-Katya-Romanova.jpg',
      'name' => 'Катя',
      'surname' => 'Романова',
      'alias' => 'katya-romanova',
      'phone' => '
              +7 926 102-96-51',
      'meta_description' => ' Фотограф Екатерина Романова — член ISPWP, WPS, победитель Mywed Award 2013 ',
      'email' => 'www.katiaromanova.com',
      'profession' => 'Фотограф',
      'description' => '
              <p>В каждой свадьбе Екатерина находит удивительные события, интересные личные истории и эмоции, которые и передаёт посредством своей камеры. Работает преимущественно в России, США и Европе, однако всегда готова познать и другие точки земного шара.</p>
              
              <div class=""> <blockquote><p>Больше всего Катя любит делать интересные снимки, общаться и познавать мир вокруг себя.</p></blockquote>
<p>То, что Екатерина — преданный мастер своего дела, говорят, прежде всего, её достижения: она стала призером международной ассоциации Fearless Photographers, является членом ISPWP, WPS, победила в Mywed Award 2013 в номинациях «Церемония» и «Герои дня», лучший свадебный фотограф в 2013 году по версии журнала Wedding.</p>
<p>Но, конечно, личность Екатерины также влияет на качество её фотографий. Она использует снимки, чтобы передать персональные переживания, интересные истории и эмоции молодожёнов.</p>
<p>По этой причине снимки получаются качественными и живыми, рассказывающими свои истории.</p>
 </div>
            ',
      'short_about' => 'В понимании фотографа свадебный день — это не день свадебной фотосессии, а настоящий яркий праздник с потрясающей энергетикой и колоссальным набором эмоций!',
      'photos' => 
      array (
        0 => '/wp-content/uploads/2016/08/Fotograf-Katya-Romanova_037-1024x683.jpg',
        1 => '/wp-content/uploads/2016/08/Fotograf-Katya-Romanova_038-683x1024.jpg',
        2 => '/wp-content/uploads/2016/08/Fotograf-Katya-Romanova_039-1024x683.jpg',
        3 => '/wp-content/uploads/2016/08/Fotograf-Katya-Romanova_040-1024x683.jpg',
        4 => '/wp-content/uploads/2016/08/Fotograf-Katya-Romanova_042-683x1024.jpg',
        5 => '/wp-content/uploads/2016/08/Fotograf-Katya-Romanova_043-1024x683.jpg',
        6 => '/wp-content/uploads/2016/08/Fotograf-Katya-Romanova_045-1024x683.jpg',
        7 => '/wp-content/uploads/2016/08/Fotograf-Katya-Romanova_046-683x1024.jpg',
        8 => '/wp-content/uploads/2016/08/Fotograf-Katya-Romanova_047-683x1024.jpg',
        9 => '/wp-content/uploads/2016/08/Fotograf-Katya-Romanova_041-1024x683.jpg',
      ),
    ),
    1 => 
    array (
      'href' => 'http://top15moscow.ru/fotografy/igor-bulgak/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/08/Fotograf-Igor-Bulgak.jpg',
      'name' => 'Игорь',
      'surname' => 'Булгак',
      'alias' => 'igor-bulgak',
      'phone' => '
              +7 928 900-00-40',
      'meta_description' => ' Фотограф Игорь Булгак — фотограф года 2015 по версии журнала Wedding ',
      'email' => 'www.bulgak.pro',
      'profession' => 'Фотограф',
      'description' => '
              <p>Игорь Булгак, сегодня — один из лучших фотографов страны. И это не просто красивые слова, ведь за ними стоят реальные достижения: фотограф года 2015 по версии журнала Wedding, ТОП 20 лучших фотографов мира по версии американской ассоциации fearless photographers, первое место в Mywed DesignBook Award 2014, финалист Wedlife Award 2013, финалист Mywed award 2012.</p>
              
              <div class=""> <p>Когда Игорю было пять, его отец работал свадебным фотографом при центральном ЗАГСе. Мальчик с интересом копался в огромной куче плёнок, наблюдал, как она проявляется и как фотография обретает свою осязаемую форму. Когда Игорю исполнилось 20 лет отец уговорил будущего фотографа попробовать свои силы. Игорю удалось достаточно быстро всё освоить и он начал зарабатывать свои первые деньги. Это и стало рывком для начала великой карьеры.</p>
<blockquote><p>Спустя какое-то время Игорю стало скучно от обыденных фотографий, которые он делал в ЗАГСе. Он понял, что может создавать что-то более интересное, воздушное, динамичное и эмоциональное!</p></blockquote>
<p>Сегодня Игорь Булгак делает по-настоящему выдающиеся снимки, выходящие за рамки обычных свадебных фотографий.  Его фото полны жизни, энергии и позитива, от чего картинка становится живой и реальной.</p>
 </div>
            ',
      'short_about' => 'Если определить, что повлияло на решение Игоря стать фотографом, то в первую очередь, это атмосфера любви, счастья и улыбок в его семье. Всё это помогает ему общаться с людьми и воспринимать мир, делиться этими чувствами с окружающими.',
      'photos' => 
      array (
        0 => '/wp-content/uploads/2016/08/Fotograph-Igor-Bulgak25-1024x1024.jpg',
        1 => '/wp-content/uploads/2016/08/Fotograph-Igor-Bulgak26-1024x1024.jpg',
        2 => '/wp-content/uploads/2016/08/Fotograph-Igor-Bulgak27-1024x1024.jpg',
        3 => '/wp-content/uploads/2016/08/Fotograph-Igor-Bulgak28-1024x1024.jpg',
        4 => '/wp-content/uploads/2016/08/Fotograph-Igor-Bulgak29-1024x1024.jpg',
        5 => '/wp-content/uploads/2016/08/Fotograph-Igor-Bulgak30-1024x1024.jpg',
        6 => '/wp-content/uploads/2016/08/Fotograph-Igor-Bulgak31-1024x1024.jpg',
        7 => '/wp-content/uploads/2016/08/Fotograph-Igor-Bulgak32-1024x1024.jpg',
        8 => '/wp-content/uploads/2016/08/Fotograph-Igor-Bulgak33-1024x620.jpg',
        9 => '/wp-content/uploads/2016/08/Fotograph-Igor-Bulgak34-1024x681.jpg',
      ),
    ),
    2 => 
    array (
      'href' => 'http://top15moscow.ru/fotografy/maksim-koliberdin/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/Fotograph-Maksim-Koliberdin.jpg',
      'name' => 'Максим',
      'surname' => 'Колибердин',
      'alias' => 'maksim-koliberdin',
      'phone' => '
              +7 926 580-53-24',
      'meta_description' => ' Максим Колибердин — Лучший Fine art фотограф 2016 года по версии журнала Wedding Magazine Russia ',
      'email' => 'www.maxkoliberdin.com',
      'profession' => 'Фотограф',
      'description' => '
              <p>Максим Колибердин — профессиональный свадебный фотограф, бесконечно преданный своему делу. Он любит мягкий натуральный свет, открытые просторы со старыми деревьями, а также импрессионизм.</p>
              
              <div class=""> <p>На счету Максима большое количество профессиональный достижений: вошёл в ТОП 15 фотографов по версии Wedding Vibes, попал в ТОП 10 лучших фотографов по версии Simple and Beyond, вошел в ТОП 10 по версии Veter Magazine, публиковался в таких журналах, как Weeding, Collezioni, Light, а также в зарубежных блогах на свадебную тематику: Once Wed, Style Me Pretty, Wedding Chicks, Magnolia Rouge, Weddywood, Wedding Vibes, Simple and Beyond, Veter Magazine.</p>
<blockquote><p>Максим передает в своих фотографиях красивую романтическую историю, неподвластную времени, которая отражает радость и любовь.</p></blockquote>
 </div>
            ',
      'short_about' => 'Максим живет в Москве, но готов приехать в любую страну мира, чтобы заснять ваш праздник на плёнку. Он предан своему делу, а потому гарантирует качественные фотографии, которые рассказывают свои истории и передают неподдельные эмоции.',
      'photos' => 
      array (
        0 => '/wp-content/uploads/2016/07/Fotograph-Maksim-Koliberdin19-768x1024.jpg',
        1 => '/wp-content/uploads/2016/07/Fotograph-Maksim-Koliberdin20-756x1024.jpg',
        2 => '/wp-content/uploads/2016/07/Fotograph-Maksim-Koliberdin21-768x1024.jpg',
        3 => '/wp-content/uploads/2016/07/Fotograph-Maksim-Koliberdin22-771x1024.jpg',
        4 => '/wp-content/uploads/2016/07/Fotograph-Maksim-Koliberdin24-755x1024.jpg',
        5 => '/wp-content/uploads/2016/07/Fotograph-Maksim-Koliberdin25-753x1024.jpg',
        6 => '/wp-content/uploads/2016/07/Fotograph-Maksim-Koliberdin26-761x1024.jpg',
        7 => '/wp-content/uploads/2016/07/Fotograph-Maksim-Koliberdin27-749x1024.jpg',
        8 => '/wp-content/uploads/2016/07/Fotograph-Maksim-Koliberdin28-753x1024.jpg',
        9 => '/wp-content/uploads/2016/07/Fotograph-Maksim-Koliberdin30-768x1024.jpg',
      ),
    ),
    3 => 
    array (
      'href' => 'http://top15moscow.ru/fotografy/katya-mukhina/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/Fotograph-Katya-Muhina.jpg',
      'name' => 'Катя',
      'surname' => 'Мухина',
      'alias' => 'katya-mukhina',
      'phone' => '
              +7 903 540-60-90',
      'meta_description' => ' Екатерина Мухина — многократный победитель фотоконкурсов WPPI, ISPWP, MYWED ',
      'email' => 'www.purlita.com/portfolio',
      'profession' => 'Фотограф',
      'description' => '
              <p>Катя Мухина запечатлит вас и ваших гостей красивыми. У нее к этому талант и большой опыт. Более 14 лет она снимает свадьбы в России и по всему миру.</p>
              
              <div class=""> <p>Свадьбы вдвоем, в семейном кругу, большие торжества на 200-300 гостей. В Москве, на экзотических островах и в роскошных особняках Европы — Катя мастерски и красиво снимет в любой ситуации. С теми же, кто хочет приключений, Катя готова отправиться хоть на край света — поэтому география ее съемок невероятно широка: от Исландии до Бора Бора, от Камчатки до Намибии, от Гавайев до Новой Зеландии.</p>
<blockquote><p>В своих снимках снимках Катя передает красоту и чувства, радость, искренность и любовь.</p></blockquote>
<p>Екатерина родилась и живёт в Москве, закончила ГУ-ВШЭ и Академию Фотографии, в совершенстве владеет английским и испанским языками.</p>
<p>Представитель Canon в России и Европе, дает мастер-классы в разных странах на международных фотоконференциях, входит в топ мировых свадебных фотографов, в топ-5 лучших свадебных фотографов России, многократный победитель фотоконкурсов WPPI, ISPWP, JUNEBUGWEDDINGS, MYWED, печатается на обложках, в блогах и журналах о свадьбах и путешествиях, автор колонки о свадьбах за границей в журнале “Счастливая Свадьба”</p>
<p>Как посол Canon, Катя Мухина использует самые надежные и новые камеры и объективы Canon. Результат гарантирован на высочайшем уровне. Отличается креативным индивидуальным подходом к каждой фотосъемке. Снимает красиво и при больших мероприятиях со своей командой.</p>
 </div>
            ',
      'short_about' => 'Катя создает удивительные фотографии. Каждая ее фотосъемка уникальна и передает красоту людей.',
      'photos' => 
      array (
        0 => '/wp-content/uploads/2016/07/Fotograph-Katya-Muhina21-1024x684.jpg',
        1 => '/wp-content/uploads/2016/07/Fotograph-Katya-Muhina22-1024x683.jpg',
        2 => '/wp-content/uploads/2016/07/Fotograph-Katya-Muhina23-1024x683.jpg',
        3 => '/wp-content/uploads/2016/07/Fotograph-Katya-Muhina24-1024x683.jpg',
        4 => '/wp-content/uploads/2016/07/Fotograph-Katya-Muhina25-1024x683.jpg',
        5 => '/wp-content/uploads/2016/07/Fotograph-Katya-Muhina26-1024x683.jpg',
        6 => '/wp-content/uploads/2016/07/Fotograph-Katya-Muhina27-679x1024.jpg',
        7 => '/wp-content/uploads/2016/07/Fotograph-Katya-Muhina28-1024x683.jpg',
        8 => '/wp-content/uploads/2016/07/Fotograph-Katya-Muhina29-1024x683.jpg',
        9 => '/wp-content/uploads/2016/07/Fotograph-Katya-Muhina30-1024x683.jpg',
      ),
    ),
    4 => 
    array (
      'href' => 'http://top15moscow.ru/fotografy/aleksej-kinyapin/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/fotograf-Aleksej-Kinyapin.jpg',
      'name' => 'Алексей',
      'surname' => 'Киняпин',
      'alias' => 'aleksej-kinyapin',
      'phone' => '
              +7 926 551-74-27',
      'meta_description' => ' Алексей Киняпин — фотограф  западного стиля ',
      'email' => 'www.akinyapin.com',
      'profession' => 'Фотограф',
      'description' => '
              <p>Алексей Киняпин — профессиональный фотограф, выработал свой уникальный стиль западной съёмки, который и сделал его известным. Он делает живые, качественные и непринуждённые снимки.</p>
              
              <div class=""> <blockquote><p>Алексей делает снимки в лёгком западном стиле, вдохновляясь людьми, которых фотографирует.</p></blockquote>
<p>Алексей не только фотограф, еще он путешественник и мечтатель. Он фотографирует свадьбы в России и далеко за её пределами. Его излюбленный стиль — чувственная западная фотография. Фотоснимки Алексея Киняпина сразу же бросаются в глаза, их нельзя спутать с чужими работами благодаря уникальному стилю.</p>
<p>Каждая свадьба для Алексея — своя история, красоту и моменты которой он и старается сохранить посредством своей работы. Фотографии — не просто бумажки, а ценные вещи, способные хранить память об определённом дне веками и тысячелетиями.</p>
<p>А в цифровом пространстве они остаются и вовсе навсегда.</p>
 </div>
            ',
      'short_about' => 'Что такое свадебное фото? Прежде всего — это лёгкость, воздушность, естественность и непринуждённость.',
      'photos' => 
      array (
        0 => '/wp-content/uploads/2016/09/Fotograph-Aleksey-Kinyapin14-1024x683.jpg',
        1 => '/wp-content/uploads/2016/09/Fotograph-Aleksey-Kinyapin13-1024x683.jpg',
        2 => '/wp-content/uploads/2016/09/Fotograph-Aleksey-Kinyapin12-1024x683.jpg',
        3 => '/wp-content/uploads/2016/09/Fotograph-Aleksey-Kinyapin10-1024x683.jpg',
        4 => '/wp-content/uploads/2016/09/Fotograph-Aleksey-Kinyapin9-1024x683.jpg',
        5 => '/wp-content/uploads/2016/09/Fotograph-Aleksey-Kinyapin8-1024x683.jpg',
        6 => '/wp-content/uploads/2016/09/Fotograph-Aleksey-Kinyapin7-1024x576.jpg',
        7 => '/wp-content/uploads/2016/09/Fotograph-Aleksey-Kinyapin6-1024x683.jpg',
        8 => '/wp-content/uploads/2016/09/Fotograph-Aleksey-Kinyapin5-1024x683.jpg',
        9 => '/wp-content/uploads/2016/09/Fotograph-Aleksey-Kinyapin4-1024x683.jpg',
      ),
    ),
    5 => 
    array (
      'href' => 'http://top15moscow.ru/fotografy/marina_fadeeva/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/Fotograph-Marina-Fadeeva.jpg',
      'name' => 'Марина',
      'surname' => 'Фадеева',
      'alias' => 'marina_fadeeva',
      'phone' => '
              +7 903 765-30-61',
      'meta_description' => ' Марина Фадеева — фотограф, который не боится экспериментировать ',
      'email' => 'www.marinafadeeva.com',
      'profession' => 'Фотограф',
      'description' => '
              <p>Марина называет себя с мужем Андреем — фотосемьей. От этого они оба обогащаются, становятся интереснее.</p>
              
              <div class=""> <blockquote><p>Добиваться успеха Марине помогает команда, а в первую очередь — муж Андрей.</p></blockquote>
<p>Марина убеждена, что когда работаешь с близким по духу человеком, это дает в 2 раза больше возможностей, дополняет чем-то новым и свежим. От этого, обогащается, становится интереснее.</p>
<p>На данный момент Марина с мужем снимают в России, Европе, Азии и работают с лучшими свадебными агентствами, журналами и площадками.</p>
<p>Марине сильно помогают знания и опыт, полученные во время преподавательской деятельности в ГУУ (где она вела маркетинг и управление проектами на кафедре автомобильного транспорта) и в интернет-компании (была специалистом по интернет-рекламе и web-дизайну).</p>
<p>Для Марины свадебная съемка — целый мир: не только необходимость показать историю дня, но и тонкая психология взаимоотношений с людьми, планирование времени, создание момента, творчество.</p>
<p>Она стремится увидеть и показать людей красивыми, даже в эмоциональные моменты, когда им кажется, что их лучше не снимать. Марина любит делать лаконичные и стильные снимки. В свадебной фотографии есть все, что ей интересно: элементы классического портрета, рекламная съемка, fashion, life-style, интерьерная и пейзажная съемка, репортаж. Кроме того, она часто становилась семейным фотографом и знала продолжение истории. Этот момент причастности, доверия — одна из причин, почему Марине хочется делать свои снимки еще более интересными и пронзительными.</p>
 </div>
            ',
      'short_about' => 'Марина Фадеева открыта к экспериментам со снимками. В своей работе она постоянно использует что-то новое, и 99.9% экспериментов проходят удачно!',
      'photos' => 
      array (
        0 => '/wp-content/uploads/2016/07/Fadeeva_Portfolio-021-1024x683.jpg',
        1 => '/wp-content/uploads/2016/07/Fadeeva_Portfolio-022-1024x683.jpg',
        2 => '/wp-content/uploads/2016/07/Fadeeva_Portfolio-023-683x1024.jpg',
        3 => '/wp-content/uploads/2016/07/Fadeeva_Portfolio-024-1024x683.jpg',
        4 => '/wp-content/uploads/2016/07/Fadeeva_Portfolio-025-1024x683.jpg',
        5 => '/wp-content/uploads/2016/07/Fadeeva_Portfolio-026-683x1024.jpg',
        6 => '/wp-content/uploads/2016/07/Fadeeva_Portfolio-027-1024x683.jpg',
        7 => '/wp-content/uploads/2016/07/Fadeeva_Portfolio-028-683x1024.jpg',
        8 => '/wp-content/uploads/2016/07/Fadeeva_Portfolio-029-1024x683.jpg',
        9 => '/wp-content/uploads/2016/07/Fadeeva_Portfolio-030-683x1024.jpg',
      ),
    ),
    6 => 
    array (
      'href' => 'http://top15moscow.ru/fotografy/dmitrij-markov/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/160711-top15photographers-1610.jpg',
      'name' => 'Дмитрий',
      'surname' => 'Марков',
      'alias' => 'dmitrij-markov',
      'phone' => '
              +7 981 192-55-70',
      'meta_description' => ' Дмитрий Марков — входит в ТОП 10 лучших фотографов по версии портала MyWed ',
      'email' => 'www.di-markov.ru',
      'profession' => 'Фотограф',
      'description' => '
              <p>Дмитрий Марков — профессиональный фотограф и просто хороший человек. Он занимается тем, что любит, а потому в своей профессии выходит далеко за рамки привычного.</p>
              
              <div class=""> <p>На счету Дмитрия много достижений, но он не считает их чем-то особенно важным.</p>
<p>Дмитрий входит в ТОП 10 лучших фотографов по версии портала MyWed;  в 2013 и 2014 годах стал финалистом и призёром премий MyWed Award и  финалистом в категории лучший фотограф премии журнала Wedding для профессионалов свадебного бизнеса Wedding Awards 2013; был участником и занял призовые места в международной ассоциации свадебных фотографов Fearless Photographers.</p>
<blockquote><p>Дмитрий делает фотографии без срока давности.</p></blockquote>
<p>Снимает Дмитрий преимущественно в Санкт-Петербурге и Москве, время от времени посещая другие города России. Он также любит ездить в Европу и снимать там. Фотограф открыт для съёмок в любой стране мира, в которую можно без проблем попасть, ведь он любит посещать новые места и узнавать что-то новое.</p>
 </div>
            ',
      'short_about' => 'Для Дмитрий фотография —гораздо больше, чем просто любимая работа.  Это страсть и призвание.<br>
Его завораживают живые искорки в глазах, улыбки, смех и слезы, радость и нежность, яркие эмоции и тонкие, едва уловимые моменты, все грани подлинного человеческого счастья.',
      'photos' => 
      array (
        0 => '/wp-content/uploads/2016/07/20_dimarkov_t15_v3-1024x683.jpg',
        1 => '/wp-content/uploads/2016/07/19_dimarkov_t15_v3-1024x683.jpg',
        2 => '/wp-content/uploads/2016/07/18_dimarkov_t15_v3-1024x683.jpg',
        3 => '/wp-content/uploads/2016/07/17_dimarkov_t15_v3-1024x683.jpg',
        4 => '/wp-content/uploads/2016/07/13_dimarkov_t15_v3-1024x683.jpg',
        5 => '/wp-content/uploads/2016/07/14_dimarkov_t15_v3-1024x683.jpg',
        6 => '/wp-content/uploads/2016/07/16_dimarkov_t15_v3-1024x683.jpg',
        7 => '/wp-content/uploads/2016/07/12_dimarkov_t15_v3-1024x683.jpg',
        8 => '/wp-content/uploads/2016/07/11_dimarkov_t15_v3-1024x683.jpg',
        9 => '/wp-content/uploads/2016/07/10_dimarkov_t15_v3-1024x683.jpg',
      ),
    ),
    7 => 
    array (
      'href' => 'http://top15moscow.ru/fotografy/natalya-legenda/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/08/160711-top15photographers-047.jpg',
      'name' => 'Наталья',
      'surname' => 'Легенда',
      'alias' => 'natalya-legenda',
      'phone' => '
              +7 985 156-97-99',
      'meta_description' => ' Наталья Легенда — фотограф, создающий нежные свадебные кадры. ',
      'email' => 'www.nataliya-legenda.ru',
      'profession' => 'Фотограф',
      'description' => '
              <p>Наталья Легенда — профессиональный фотограф, делающий «нежные» снимки. В настоящее время занимает ведущие позиции среди свадебных фотографов.</p>
              
              <div class=""> <p>Её работы регулярно публикуются в модных свадебных журналах различного формата: от строгой постановки «wedding» до свадебных проектов в стиле «old fashion», а так же в популярных свадебных блогах, как отечественных, так и иностранных.</p>
<p>Фотография также является частью романтики. Просматривая свои свадебные или семейные снимки, вы, словно на машине времени, переноситесь в прошлое.</p>
<p>Наталья никогда не останавливается на достигнутом и постоянно экспериментирует со стилями.</p>
<blockquote><p>Её снимки — целые истории живых людей, запечатлённые на плёнку.</p></blockquote>
<p>Благодаря уникальному таланту быть в нужное время рядом с фотоаппаратом Наталья на свадьбах успевает запечатлеть самые важные и искренние моменты.</p>
 </div>
            ',
      'short_about' => 'Наталья искренне любит каждую картинку, которую создает. А помогают ей в этом искренние эмоции людей и позитивная энергетика!',
      'photos' => 
      array (
        0 => '/wp-content/uploads/2016/07/Fotograph-Natalia-Legenda28-1024x683.jpg',
        1 => '/wp-content/uploads/2016/07/Fotograph-Natalia-Legenda27-1024x683.jpg',
        2 => '/wp-content/uploads/2016/07/Fotograph-Natalia-Legenda26-683x1024.jpg',
        3 => '/wp-content/uploads/2016/07/Fotograph-Natalia-Legenda25-682x1024.jpg',
        4 => '/wp-content/uploads/2016/07/Fotograph-Natalia-Legenda24-1024x1024.jpg',
        5 => '/wp-content/uploads/2016/07/Fotograph-Natalia-Legenda23-1024x683.jpg',
        6 => '/wp-content/uploads/2016/07/Fotograph-Natalia-Legenda22-1024x682.jpg',
        7 => '/wp-content/uploads/2016/07/Fotograph-Natalia-Legenda21-683x1024.jpg',
        8 => '/wp-content/uploads/2016/07/Fotograph-Natalia-Legenda20-1024x683.jpg',
        9 => '/wp-content/uploads/2016/07/Fotograph-Natalia-Legenda19-683x1024.jpg',
      ),
    ),
    8 => 
    array (
      'href' => 'http://top15moscow.ru/fotografy/sergej-zaporozhets/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/016-4.jpg',
      'name' => 'Сергей',
      'surname' => 'Запорожец',
      'alias' => 'sergej-zaporozhets',
      'phone' => '
              +7 926 239-62-69',
      'meta_description' => ' Сергей Запорожец — фотограф  живых эмоций ',
      'email' => 'www.sergeyzaporozhets.com',
      'profession' => 'Фотограф',
      'description' => '
              <p>Сергей Запорожец — профессиональный свадебный фотограф. Автор ярких и незабываемых снимков. Один из самых успешных свадебных фотографов России, несмотря на молодой возраст. Его фотосессии — эмоциональные приключения, которые Сергей проживает вместе с молодожёнами.</p>
              
              <div class=""> <blockquote><p>Сергей специализируется на сложносочиненных конструкциях благодаря множеству режиссерских свадеб, может дать экспертизу по свету, собрать команду под задачу, найти необычные ракурсы или сделать что-то реально новое.</p></blockquote>
<p>Но.</p>
<p>Важнее  всего  люди, а люди — это переживания, слезы, улыбки, удивление, грусть, гордость, любовь и еще и еще…</p>
<p>Все это не рождается просто так, оно копится, выращивается с каждой прожитой минутой, и свадьба — это не кульминация, а просто еще один виток, но довольно интересный.</p>
<p>В один день происходит столько событий, что вспоминать можно долго. Он сам вспоминает свой свадебный день с теплой улыбкой на лице. (Обязательно расскажет вам о нём при встрече)</p>
<p>Что в итоге?</p>
<p>Как бы странно ни звучало, но его стиль можно сравнить с одним животным — с хамелеоном. Сергей подстраивается под окружающую среду, ловит момент, бывает прикольно передвигается и любит интересные ракурсы.</p>
<p>А вот что никак не связано с этим забавным зверем, так это то, что ему важны люди. Особенно те, что обладают простыми качествами: искренностью, заботой, любовью, юмором, оптимизмом.</p>
<p>Сергей убеждён, что успех приходит лишь к тем, кто упорно трудится каждый день, осваивая интересные и новые технические приёмы. Он с техникой на «ты», что даёт фотографу безграничные возможности, ограничивающиеся лишь фантазией. Сергей советует новичкам понять простую истину: никакого секрета или чудесной формулы не существует. Привести вас к успеху может лишь тяжёлый труд каждый день и жажда новых знаний.</p>
<p>Сергей Запорожец даже изобрёл новый жанр в свадебной фотографии, который называется «Хамелеон». Его суть в том, что фотограф взаимодействует с природой вокруг него на все 100%.</p>
 </div>
            ',
      'short_about' => 'Портреты, букеты, декор и детали — Сергей фотографирует всё. Но! Когда есть живые эмоции, это всегда интересней!',
      'photos' => 
      array (
        0 => '/wp-content/uploads/2016/07/2016-08-08-sergeyzaporozhets-zprwdp-1-0030-1024x684.jpg',
        1 => '/wp-content/uploads/2016/07/2016-08-08-sergeyzaporozhets-zprwdp-1-0029-1024x684.jpg',
        2 => '/wp-content/uploads/2016/07/2016-08-08-sergeyzaporozhets-zprwdp-1-0028-1024x684.jpg',
        3 => '/wp-content/uploads/2016/07/2016-08-08-sergeyzaporozhets-zprwdp-1-0027-1024x684.jpg',
        4 => '/wp-content/uploads/2016/07/2016-08-08-sergeyzaporozhets-zprwdp-1-0026-1024x681.jpg',
        5 => '/wp-content/uploads/2016/07/2016-08-08-sergeyzaporozhets-zprwdp-1-0021-1024x684.jpg',
        6 => '/wp-content/uploads/2016/07/2016-08-08-sergeyzaporozhets-zprwdp-1-0022-1024x684.jpg',
        7 => '/wp-content/uploads/2016/07/2016-08-08-sergeyzaporozhets-zprwdp-1-0023-1024x684.jpg',
        8 => '/wp-content/uploads/2016/07/2016-08-08-sergeyzaporozhets-zprwdp-1-0024-1024x684.jpg',
        9 => '/wp-content/uploads/2016/07/2016-08-08-sergeyzaporozhets-zprwdp-1-0025-1024x684.jpg',
      ),
    ),
    9 => 
    array (
      'href' => 'http://top15moscow.ru/fotografy/andrej-nastasenko/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/16.07.27-top15Moscow-6161.jpg',
      'name' => 'Андрей',
      'surname' => 'Настасенко',
      'alias' => 'andrej-nastasenko',
      'phone' => '
              +7 916 999-34-43',
      'meta_description' => ' Фотограф Андрей Настасенко — входит в ТОП 50 лучших свадебных фотографов мира ',
      'email' => 'www.nastasenko.com',
      'profession' => 'Фотограф',
      'description' => '
              <p>Андрей Настасенко — профессиональный фотограф, его имя надежно вписано в ТОП-листы свадебных фотографов Москвы по версии MyWed. Рейтинг Fearless Photographers включил его в ТОП 50 лучших свадебных фотографов мира в 2014 году.</p>
              
              <div class=""> <blockquote><p>Андрей делает стильные и красивые фотографии. Чувства и эмоции, слезы и смех — вот, что его вдохновляет. Ну и свет, конечно же.</p></blockquote>
<p>Андрей живёт и работает в Москве. Нередко фотограф снимает и в Европе. Обожает путешествовать в те места, которые ещё ни разу не посещал, постоянно развививается и открывает новые горизонты.</p>
 </div>
            ',
      'short_about' => 'Всё, чего добился Андрей — это заслуженно. Его фотографии не имеют срока давности, в них видна жизнь и эмоции, которые переживала пара в конкретный момент времени.',
      'photos' => 
      array (
        0 => '/wp-content/uploads/2016/07/Fotograph-Andrey-Nastasenko21-1024x683.jpg',
        1 => '/wp-content/uploads/2016/07/Fotograph-Andrey-Nastasenko22-1024x683.jpg',
        2 => '/wp-content/uploads/2016/07/Fotograph-Andrey-Nastasenko23-1024x682.jpg',
        3 => '/wp-content/uploads/2016/07/Fotograph-Andrey-Nastasenko24-1024x682.jpg',
        4 => '/wp-content/uploads/2016/07/Fotograph-Andrey-Nastasenko25-1024x682.jpg',
        5 => '/wp-content/uploads/2016/07/Fotograph-Andrey-Nastasenko26-1024x683.jpg',
        6 => '/wp-content/uploads/2016/07/Fotograph-Andrey-Nastasenko27-1024x683.jpg',
        7 => '/wp-content/uploads/2016/07/Fotograph-Andrey-Nastasenko28-1024x683.jpg',
        8 => '/wp-content/uploads/2016/07/Fotograph-Andrey-Nastasenko29-1024x682.jpg',
        9 => '/wp-content/uploads/2016/07/Fotograph-Andrey-Nastasenko30-1024x683.jpg',
      ),
    ),
    10 => 
    array (
      'href' => 'http://top15moscow.ru/fotografy/liliya-gorlanova/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/160711-top15photographers-824.jpg',
      'name' => 'Лилия',
      'surname' => 'Горланова',
      'alias' => 'liliya-gorlanova',
      'phone' => '
              +7 926 264-95-79',
      'meta_description' => ' Лилия Горланова — призёр ассоциаций свадебных фотографов WPJA, AGWPJA, ISPWP ',
      'email' => 'www.liliyagorlanova.ru',
      'profession' => 'Фотограф',
      'description' => '
              <p>Лилия любит свою работу за положительные чувства, которые она ей дарит, обожает делиться собственными эмоциями, передавая их посредством снимков.</p>
              
              <div class=""> <p>В фотомир Лилия попала из мира моды. В университете она училась на художника-модельера, но вскоре поняла, что хочет заниматься фотографией. Главным в своей работе считает творческую составляющую.</p>
<p>Лилия может похвастаться следующими достижениями: она действительный член и призёр ассоциаций свадебных фотографов, таких как WPJA, AGWPJA, ISPWP, ТОП1 в рейтинге международной ассоциации свадебных фотожурналистов WPJA в 2011 году, победительница конкурса MyWed Award в 2011, а также обладательница титула Фотограф года.</p>
<blockquote><p>Фотограф со своей командой далеко не новички в свадебном деле. Их работа прекрасно отлажена и скоординирована. Они доверяют друг другу и знают, что в случае чего смогут положиться на любого человека из команды. Создавать идеальные свадьбы в столь доброй обстановке — это не только просто, но и приятно!</p></blockquote>
<p> </p>
 </div>
            ',
      'short_about' => 'Лилия очень любит свою работу, и те чувства, которые она ей дарит, отражаются в ее восхитительных снимках.',
      'photos' => 
      array (
        0 => '/wp-content/uploads/2016/07/Gorlanova_26-1024x683.jpg',
        1 => '/wp-content/uploads/2016/07/Gorlanova_27-1024x683.jpg',
        2 => '/wp-content/uploads/2016/07/Gorlanova_28-683x1024.jpg',
        3 => '/wp-content/uploads/2016/07/Gorlanova_29-1024x1024.jpg',
        4 => '/wp-content/uploads/2016/07/Gorlanova_30-1024x683.jpg',
        5 => '/wp-content/uploads/2016/07/Gorlanova_31-1024x647.jpg',
        6 => '/wp-content/uploads/2016/07/Gorlanova_32-1024x683.jpg',
        7 => '/wp-content/uploads/2016/07/Gorlanova_35-1024x683.jpg',
        8 => '/wp-content/uploads/2016/07/Gorlanova_36-1024x683.jpg',
        9 => '/wp-content/uploads/2016/07/Gorlanova_37-1024x683.jpg',
      ),
    ),
    11 => 
    array (
      'href' => 'http://top15moscow.ru/fotografy/mihail-harlamov/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/160711-top15photographers-1531.jpg',
      'name' => 'Михаил',
      'surname' => 'Харламов',
      'alias' => 'mihail-harlamov',
      'phone' => '
              +7 909 161-76-61',
      'meta_description' => ' Михаил Харламов — финалист конкурсов Mywed Award, спикер Mywed party ',
      'email' => 'www.happinesscorporation.net',
      'profession' => 'Фотограф',
      'description' => '
              <p>Cнимает с 2006 года, за свою карьеру успел поработать со всеми Российскими журналами, включая Wedding, для которого снял 8 обложек и фешен историй,  отснял более 200 свадеб.  </p>
<p>В том числе звездные свадьбы (Татьяны Навки и Дмитрия Пескова, Галины Юдашкиной и Петра Масхакова, Константина Крюкова и Алины Алексеевой, Николая Крутого, Татьяны Геворкян, Евгении Линович и Андрея Кузьмина). </p>
<p>География поездок и работ включает в себя Америку, (Лос Анжелес второй город Михаила) , Европу, ОАЭ.</p>
              
              <div class=""> <blockquote><p>Зимой и весной живет и работает в Лос Анжелесе, летом и осенью – в Москве. У Михаила всегда открыты визы — Америка, Лондон, Европа.</p></blockquote>
<p>Wpja, Fearless member, финалист конкурсов Mywed Award, спикер Mywed party, член жюри Wedding Magazine Award 13-16, преподаватель Wedding Academy.</p>
 </div>
            ',
      'short_about' => 'Магия момента, интересный свет и красиво срежиссированная постановка, фотография как Кино. Задача фотографа на свадьбе — передать самые важные моменты дня красиво и кинематографично, так, чтобы они остались в памяти на долгие годы.',
      'photos' => 
      array (
        0 => '/wp-content/uploads/2016/07/Top15Moscow-Kharlamov-Mikhail-Portfolio-2016-22-1024x683.jpg',
        1 => '/wp-content/uploads/2016/07/Top15Moscow-Kharlamov-Mikhail-Portfolio-2016-23-1024x683.jpg',
        2 => '/wp-content/uploads/2016/07/Top15Moscow-Kharlamov-Mikhail-Portfolio-2016-24-1024x683.jpg',
        3 => '/wp-content/uploads/2016/07/Top15Moscow-Kharlamov-Mikhail-Portfolio-2016-25-1024x683.jpg',
        4 => '/wp-content/uploads/2016/07/Top15Moscow-Kharlamov-Mikhail-Portfolio-2016-26-1024x683.jpg',
        5 => '/wp-content/uploads/2016/07/Top15Moscow-Kharlamov-Mikhail-Portfolio-2016-27-683x1024.jpg',
        6 => '/wp-content/uploads/2016/07/Top15Moscow-Kharlamov-Mikhail-Portfolio-2016-28-1024x683.jpg',
        7 => '/wp-content/uploads/2016/07/Top15Moscow-Kharlamov-Mikhail-Portfolio-2016-29-1024x683.jpg',
        8 => '/wp-content/uploads/2016/07/Top15Moscow-Kharlamov-Mikhail-Portfolio-2016-30-1024x678.jpg',
      ),
    ),
    12 => 
    array (
      'href' => 'http://top15moscow.ru/fotografy/galina-nabatnikova/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/16.07.10-top15photographers-579.jpg',
      'name' => 'Галина',
      'surname' => 'Набатникова',
      'alias' => 'galina-nabatnikova',
      'phone' => '
              +7 926 549-90-49',
      'meta_description' => ' Геннадий Гранин и Галина Набатникова — Фотографы года 2016 по версии журнала Wedding Magazine Russia ',
      'email' => 'www.galinanabatnikova.com',
      'profession' => 'Фотограф',
      'description' => '
              <p>Геннадий Гранин и Галина Набатникова — призеры Первой Национальной Премии «Лучший фотограф года»</p>
<p>Многочисленные призеры конкурсов Всемирной Ассоциации Профессиональных Свадебных Фотографов (ISPWP, Лос-Анджелес). Фотографы №1 в мировом рейтинге ISPWP зима-2014. Члены жюри международных конкурсов, в том числе Всемирной Ассоциации Профессиональных Свадебных Фотографов ISPWP. Серебряные призеры Кубка мира по фотографии (WPC) 2016. Финалисты Mywed Award 2014.</p>
<p>Свадебные фотографы №1 в России по рейтингу порталов mywed.com и wedlife.ru 2015, 2016.<br>
Победители Премии International Zankyou Wedding Awards 2016 в категории «Свадебный фотограф»;<br>
Официальные спикеры Nikon и единственные представители России в международной программе<br>
Nikon Wedding Advanced Campus.</p>
              
              <div class=""> <blockquote><p>Может быть, вы очень хорошо разбираетесь в фотографии, а может, впервые окунулись в этот удивительный мир. В любом случае у вас есть свое видение идеального снимка. Наша же задача — превратить это видение в изображение</p></blockquote>
<p>Мы описываем свое творчество как элегантную фотожурналистику в стиле кинематографа. Где одновременно совмещаются документальная и художественная фотография, где присутствуют красота и характер, эстетика и вкус. Где каждый из наших героев играет лучшую и главную роль. Роль самого себя.</p>
<p>Ничто в этой жизни не проходит бесследно, и мы видим свою задачу не только в фиксации какого либо момента. Мы хотим, чтобы рассматривая снимки спустя годы, зритель чувствовал настроение, атмосферу, эмоции и испытывал эстетический подъем.</p>
<p>Поэтому фотографии должны выдержать проверку временем и кроме ценности семейной, представлять ценность художественную. Задача не простая. Для этого мы тщательно готовимся и настраиваемся, и настраиваем вас, чтобы у вас остались действительно особенные и неповторимые фотографии.</p>
 </div>
            ',
      'short_about' => 'Наш творческий тандем существует более 15 лет и с тех пор как судьба свела нас вместе, появилось общее представление о том, как мы хотим снимать, дополняя друг друга. А главное, наше творчество очень тонко объединяет в себе женский взгляд на жизнь и мужское видение мира.',
      'photos' => 
      array (
        0 => '/wp-content/uploads/2016/07/graninphoto_121-1024x683.jpg',
        1 => '/wp-content/uploads/2016/07/graninphoto_111-1024x683.jpg',
        2 => '/wp-content/uploads/2016/07/graninphoto_101-1024x683.jpg',
        3 => '/wp-content/uploads/2016/07/graninphoto_091-1024x683.jpg',
        4 => '/wp-content/uploads/2016/07/graninphoto_081-1024x683.jpg',
        5 => '/wp-content/uploads/2016/07/graninphoto_061-1024x683.jpg',
        6 => '/wp-content/uploads/2016/07/graninphoto_051-1024x683.jpg',
        7 => '/wp-content/uploads/2016/07/graninphoto_041-1024x683.jpg',
        8 => '/wp-content/uploads/2016/07/graninphoto_031-1024x683.jpg',
        9 => '/wp-content/uploads/2016/07/graninphoto_021-683x1024.jpg',
      ),
    ),
    13 => 
    array (
      'href' => 'http://top15moscow.ru/fotografy/konstantin-semenihin/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/16.07.10-top15photographers-814.jpg',
      'name' => 'Константин',
      'surname' => 'Семенихин',
      'alias' => 'konstantin-semenihin',
      'phone' => '
              +7 903 129-94-90',
      'meta_description' => ' Константин Семенихин — один из самых востребованных свадебных фотографов Москвы ',
      'email' => 'www.ksemenikhin.com',
      'profession' => 'Фотограф',
      'description' => '
              <p>Эстет и перфекционист. Ему доверяют свои торжества топовые организаторы и самые взыскательные пары, а календарь расписан не только российскими, но и заграничными съемками.</p>
              
              <div class=""> <blockquote><p>Свадьбы, которые снимает Константин из года в год становятся лучшими свадьбами года, печатаются во всех свадебных журналах и собирают тысячи репостов в мировых блогах.</p></blockquote>
<p>Его работы очень универсальны и поэтому так популярны. Ведь в каждой его съемке есть место как трогательным живым эмоциям и репортажным моментам, так и красивым и стильным фэшн кадрам.</p>
<p>Внимание к эстетике торжества, характерам и стилю молодожёнов, мгновенная реакция и потрясающая работоспособность.</p>
<p>Плюс к этому лёгкое и дружественное общение с парой, умение видеть и чувствовать красоту в людях, помочь им раскрыться и проявить настоящие эмоции — в этом и есть стиль работы Константина.</p>
 </div>
            ',
      'short_about' => 'Его подход — это фотография вне времени, кадры которые не выйдут из моды и их захочется пересматривать через многие годы.',
      'photos' => 
      array (
        0 => '/wp-content/uploads/2016/07/Fotograph-Konstantin-Semenihin1-1024x683.jpg',
        1 => '/wp-content/uploads/2016/07/Fotograph-Konstantin-Semenihin2-1024x683.jpg',
        2 => '/wp-content/uploads/2016/07/Fotograph-Konstantin-Semenihin3-1024x683.jpg',
        3 => '/wp-content/uploads/2016/07/Fotograph-Konstantin-Semenihin4-683x1024.jpg',
        4 => '/wp-content/uploads/2016/07/Fotograph-Konstantin-Semenihin5-1024x683.jpg',
        5 => '/wp-content/uploads/2016/07/Fotograph-Konstantin-Semenihin6-1024x683.jpg',
        6 => '/wp-content/uploads/2016/07/Fotograph-Konstantin-Semenihin7-1024x683.jpg',
        7 => '/wp-content/uploads/2016/07/Fotograph-Konstantin-Semenihin8-1024x683.jpg',
        8 => '/wp-content/uploads/2016/07/Fotograph-Konstantin-Semenihin9-1024x683.jpg',
        9 => '/wp-content/uploads/2016/07/Fotograph-Konstantin-Semenihin10-683x1024.jpg',
      ),
    ),
    14 => 
    array (
      'href' => 'http://top15moscow.ru/fotografy/andrej-bajda/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/16.07.10-top15photographers-970.jpg',
      'name' => 'Андрей',
      'surname' => 'Байда',
      'alias' => 'andrej-bajda',
      'phone' => '
              +7 916 643-14-68',
      'meta_description' => ' Андрей Байда – фотограф модных глянцевых изданий ',
      'email' => 'www.andrewbayda.com/',
      'profession' => 'Фотограф',
      'description' => '
              <blockquote><p>Фотография для Андрея не просто работа — это его стиль жизни</p></blockquote>
<p>В столичном обществе Андрей Байда прославился несколько лет назад, проявив себя в качестве светского хроникера.</p>
<p>Андрей публикуется в крупных глянцевых изданиях, фотографирует знаменитостей, и на данный момент Андрей — один из самых востребованных фотографов России.</p>
              
              <div class=""> <blockquote><p>Андрей — финалист премии Wedding Awards 2016 в номинации «Лучший свадебный фотограф»</p></blockquote>
<p>Настоящая страсть Андрея — путешествия. Действительно, застать его в Москве очень трудно, а заметки, которые он делает в пути, рассказывают о его поездках не менее красочно, чем фотографии.</p>
<p>У фотографа есть собственный блог, он также регулярно пишет в колонку путешественника на одном из модных интернет-ресурсов.</p>
<p>Он посетил Америку, Австралию, Азию, Великобританию, Исландию и множество других стран, в которых снял десятки красивейших свадеб.</p>
 </div>
            ',
      'short_about' => 'Помимо активной работы в свадебной индустрии успешно работает в жанре Fashion и снимает для таких изданий как L’Officiel, Glamour, Wedding, Tatler, Allure, Harper’s Bazaar и других. ',
      'photos' => 
      array (
        0 => '/wp-content/uploads/2016/07/Top15_bayda_29-1024x683.jpg',
        1 => '/wp-content/uploads/2016/07/Top15_bayda_04-1024x683.jpg',
        2 => '/wp-content/uploads/2016/07/Top15_bayda_12-1024x683.jpg',
        3 => '/wp-content/uploads/2016/07/Top15_bayda_01-1024x683.jpg',
        4 => '/wp-content/uploads/2016/07/Top15_bayda_32-683x1024.jpg',
        5 => '/wp-content/uploads/2016/07/Top15_bayda_38-683x1024.jpg',
        6 => '/wp-content/uploads/2016/07/Top15_bayda_39-683x1024.jpg',
        7 => '/wp-content/uploads/2016/07/Top15_bayda_37-1024x683.jpg',
        8 => '/wp-content/uploads/2016/07/Top15_bayda_40-1024x683.jpg',
        9 => '/wp-content/uploads/2016/07/Top15_bayda_33-683x1024.jpg',
      ),
    ),
  ),
  'vedushie' => 
  array (
    0 => 
    array (
      'href' => 'http://top15moscow.ru/vedushie/leonid-margolin/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2017/04/Kopiya-Leonid-Margolin-2.jpg',
      'name' => 'Леонид',
      'surname' => 'Марголин',
      'alias' => 'leonid-margolin',
      'phone' => '
              +7 (926) 795-55-55',
      'meta_description' => ' Обладатель премии «РАДИОМАНИЯ» в номинации «Лучшее утреннее шоу» 2015 ',
      'email' => 'www.yaprazdnik.ru',
      'profession' => 'Ведущий',
      'description' => '
              <p>Леонид по образованию политолог. Окончил Российский государственный социальный университет, поступил в аспирантуру и сдал кандидатский минимум. Немного поработав на выборах, а также в сфере рекламы, перевёл умение общаться с людьми в другое сферу, став первоклассным ведущим свадеб и крупных мероприятий.</p>
<p>Благодаря глубокому знанию английского языка, зарекомендовал себя в качестве хорошего ведущего не только в Москве и других городах России, но и за рубежом: в Италии, Франции, Чехии, Прибалтике. В настоящее время Леонид каждую неделю проводит в среднем 2-3 свадьбы.</p>
              
              <div class=""> <p>Благодаря большому теле и радио опыту, Леонид Марголин прекрасно импровизирует.</p>
<p>В детстве Леонид увлекался литературой, играл в баскетбол. В настоящее время его большой страстью является кино. Леонид написал сценарий для 8 полнометражных фильмов, вышедших в прокат. В октябре 2016 года вышел дебютный фильм, где Леонид выступил в качестве режиссёра.</p>
<p>Сегодня Леонид Марголин – ведущий свадеб и крупных мероприятий. За его плечами проведение новогодних мероприятий СИБУРа, вечеринок Сбербанка во время олимпиады в Сочи, торжества по случаю открытия сочинской олимпиады «Горка город», открытие торговых центров «РИО», ведение Дня города на театральной площади в Москве, открытие Центрального детского магазина на Лубянке и многое другое.</p>
 </div>
            ',
      'short_about' => '«Моя миссия – показать людям самих себя с неожиданно приятной стороны»',
      'photos' => 
      array (
        0 => '/wp-content/uploads/2017/04/topshowmen-gallery-170417134501-1024x682.jpg',
        1 => '/wp-content/uploads/2017/04/1-386-1024x683.jpg',
        2 => '/wp-content/uploads/2017/04/1-515-1024x683.jpg',
        3 => '/wp-content/uploads/2017/04/2017-04-25-zprwdp-sergeyzaporozhets-22-24-31-21-1024x684.jpg',
        4 => '/wp-content/uploads/2017/04/1-378-1024x683.jpg',
        5 => '/wp-content/uploads/2017/04/topshowmen-gallery-170417134507-1024x768.jpg',
        6 => '/wp-content/uploads/2017/04/2017_04_25_top15_297-1024x683.jpg',
        7 => '/wp-content/uploads/2017/04/2017_04_25_top15_262-1024x683.jpg',
        8 => '/wp-content/uploads/2017/04/2017-04-25-zprwdp-sergeyzaporozhets-22-24-43-60-1024x684.jpg',
      ),
      'videos' => 
      array (
        0 => 'https://player.vimeo.com/video/201741209',
      ),
    ),
    1 => 
    array (
      'href' => 'http://top15moscow.ru/vedushie/aleksandr-belov/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2017/04/Kopiya-Aleksandr-Belov-2.jpg',
      'name' => 'Александр',
      'surname' => 'Белов',
      'alias' => 'aleksandr-belov',
      'phone' => '
              +7 (925) 508-71-58',
      'meta_description' => ' Ведущий всех звёздных вечеринок Москвы ',
      'email' => 'www.alexbelov.ru',
      'profession' => 'Ведущий',
      'description' => '
              <p>Шоумен Александр Белов, ведущий всех звёздных вечеринок Москвы, певец, бывший солист группы «ИКС-миссия» и бронзовый призер программы «Звёздный лед».</p>
<blockquote><p>Свадебный ведущий — не просто человек, который говорит правильные слова. Это тот, кто объединяет две большие семьи совершенно разных людей, зачастую даже не знающих друг друга. И на протяжении всей свадьбы ведущий аккуратно и незаметно слой за слоем снимает с гостей все лишнее: стеснение, напряжение, скованность, чтобы создать в итоге одну большую и дружную семью.</p></blockquote>
              
              <div class=""> <p>Портфолио Александра Белова говорит само за себя. В качестве ведущего проводил презентации и корпоративные мероприятия для таких крупнейших международных компаний как Wella, Volkswagen, BMW, Ernst&amp;Young, Ford, Jaguar, Korean Air, Lotte Hotel, Nike, Nutricia, Pfizer, Philips, Porshe Design, Procter and Gamble, Samsung, Shell, Sony, UniCredit, BMI и многих других.</p>
<p>И российские компании не остались обделёнными: Александр проводил презентации таких «монстров», как «Сбербанк», «Альфа-банк», «КАМАЗ», «Газпромбанк», «Дон-Строй», «Интеко», «Мегафон», «Планета Фитнес», «Снежная Королева», «ТНК», «Транскредит банк» и др.</p>
 </div>
            ',
      'short_about' => 'В идеале из компании в 100-200 человек надо сделать одно целое, как будто все сидят за одним большим столом и просто общаются. Это самое ценное, что может быть, — общение, потому что артистами никого уже не удивить',
      'photos' => 
      array (
        0 => '/wp-content/uploads/2017/04/xOTlnxrUixI-1024x1024.jpg',
        1 => '/wp-content/uploads/2017/04/VX0HgS2qixg-1024x904.jpg',
        2 => '/wp-content/uploads/2017/04/Viz3VzvOdRY-1024x683.jpg',
        3 => '/wp-content/uploads/2017/04/bkWY1UhkaP0-1024x744.jpg',
        4 => '/wp-content/uploads/2017/04/topshowmen-gallery-20160612101930-878x1024.jpg',
        5 => '/wp-content/uploads/2017/04/topshowmen-gallery-20160612101907.jpg',
        6 => '/wp-content/uploads/2017/04/topshowmen-gallery-20160612101915-1024x1024.jpg',
        7 => '/wp-content/uploads/2017/04/76sj1bdbQhY-1024x887.jpg',
        8 => '/wp-content/uploads/2017/04/topshowmen-gallery-20160612122318.jpg',
        9 => '/wp-content/uploads/2017/04/3yzW-AOzl1c-820x1024.jpg',
      ),
      'videos' => 
      array (
        0 => 'https://player.vimeo.com/video/215230764',
      ),
    ),
    2 => 
    array (
      'href' => 'http://top15moscow.ru/vedushie/oleg-savelyev/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2015/03/Kopiya-Oleg-Savelev.jpg',
      'name' => 'Олег',
      'surname' => 'Савельев',
      'alias' => 'oleg-savelyev',
      'phone' => '
              +7 925 589-84-01',
      'meta_description' => ' Олег Савельев — Лучший ведущий года по версии  Wedding Awards 2015 и обладатель премии ТЭФИ ',
      'email' => 'www.osavelev.ru',
      'profession' => 'Ведущий',
      'description' => '
              <p>В индустрии праздников Олег Савельев работает уже более 15 лет, что даёт ему полное право называться профессиональным ведущим.</p>
              
              <div class=""> <blockquote><p>Одно время Олег Савельев даже работал в качестве art-директора одного из казино Москвы.</p></blockquote>
<p>За время своей карьеры Олег стал лицом Пятого канала; работал ведущим программы «Утро на 5», ведущим проекта «Секретные материалы шоу-бизнеса» на телеканале «Пятница», ведущим шоу на радио «Маяк»; получил премию премию Wedding Awards 2015 в номинации «Лучший ведущий года» и премию ТЭФИ (российскую национальную телевизионную премию) за телепрограмму «Утро на 5».</p>
<p>Окончив университет, Олег начал жить в Москве. С 2000 года стартует его карьера на телевидении в качестве редактора всем известной «Большой стирки» на Первом канале и телевизионного ведущего проекта «Секретные материалы шоу-бизнеса» (стартовал на MTV Россия, перешел в эфир «Пятницы» и на «1+1»).</p>
<p>Небывалая популярность приходит к нему с работой в проекте «Наша Russia» (мировой гастрольный тур) от Comedy Club production.</p>
 </div>
            ',
      'short_about' => 'Номинирован на всероссийскую премию «Радиомания». Ведет юмористическое шоу «Ранеты на Маяке» и легендарное шоу «Ивановы!». Ныне на постоянной основе в компании Шорох Натальи и Ковалевского Максима участвует в шоу «Добрый вечер, профсоюзы».',
      'photos' => 
      array (
        0 => '/wp-content/uploads/2015/03/vedushiy-saveliev-oleg-1-1024x710.jpg',
        1 => '/wp-content/uploads/2015/03/vedushiy-saveliev-oleg-2-1024x682.jpg',
        2 => '/wp-content/uploads/2015/03/vedushiy-saveliev-oleg-6.jpg',
        3 => '/wp-content/uploads/2015/03/oleg-saveliev-5-725x1024.jpg',
        4 => '/wp-content/uploads/2015/03/oleg-saveliev-81-1024x683.jpg',
        5 => '/wp-content/uploads/2017/02/photofacefun_com_1494944648.jpg',
        6 => '/wp-content/uploads/2015/03/oleg-saveliev-9-1024x576.jpg',
        7 => '/wp-content/uploads/2015/03/vedushiy-saveliev-oleg-3-1024x682.jpg',
        8 => '/wp-content/uploads/2015/03/oleg-saveliev-71-804x1024.jpg',
        9 => '/wp-content/uploads/2015/03/FM392214_sm-3.jpg',
      ),
      'videos' => 
      array (
        0 => 'https://player.vimeo.com/video/156475388',
      ),
    ),
    3 => 
    array (
      'href' => 'http://top15moscow.ru/vedushie/oleg-beletskiy/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2015/03/Kopiya-Oleg-Beletskij.jpg',
      'name' => 'Олег',
      'surname' => 'Белецкий',
      'alias' => 'oleg-beletskiy',
      'phone' => '
              +7 985 411-24-00',
      'meta_description' => ' Олег Белецкий — востребованный шоумен столицы, основатель TOP15MOSCOW и TOPSHOWMEN ',
      'email' => 'www.olegbeletskiy.com',
      'profession' => 'Ведущий',
      'description' => '
              <p>Олег — один из самых востребованных ведущих Москвы. Именно за его плечами лежит создание самого популярного свадебного проекта #TOP15MOSCOW.</p>
              
              <div class=""> <p>Олег с лёгкостью проводит свадьбы, корпоративы, дни рождения, презентации и вечеринки не только в России, но и за рубежом: в Италии, Франции, Греции, Испании. </p>
<p>Ведение свадеб — это всегда искренние эмоции, каждая пара излучает свою энергетику и это чувствуют гости. Олег сумеет создать необходимую вам атмосферу и будет комфортным ведущим для вас и ваших гостей. </p>
<blockquote><p>За все время моей работы ведущим уже не осталось моментов, к которым я не был бы готов. Форс-мажорные ситуации случаются почти на каждой свадьбе и ведущему, предстоит оградить гостей от лишней суеты и хлопот. Бывает, нужно выиграть время для подготовки артистов или декораторов, подстраховать диджея или персонал ресторана, хорошего ведущего это не застанет врасплох.</p></blockquote>
 </div>
            ',
      'short_about' => 'Олег умело совмещает драйв с легким и ненавязчивым ведением вечера, на котором комфортно каждому из ваших гостей. И не удивляйтесь, если после завершения программы вашу вечеринку еще долго нельзя будет остановить:)',
      'photos' => 
      array (
        0 => '/wp-content/uploads/2015/03/vedushiy-oleg-beletskiy-2-1024x682.jpg',
        1 => '/wp-content/uploads/2015/03/vedushiy-oleg-beletskiy-3-1024x683.jpg',
        2 => '/wp-content/uploads/2015/03/vedushiy-oleg-beletskiy-4.jpg',
        3 => '/wp-content/uploads/2015/03/vedushiy-oleg-beletskiy-5-1024x683.jpg',
        4 => '/wp-content/uploads/2015/03/vedushiy-oleg-beletskiy-6-1024x684.jpg',
        5 => '/wp-content/uploads/2015/03/vedushiy-oleg-beletskiy-7-683x1024.jpg',
        6 => '/wp-content/uploads/2015/03/vedushiy-oleg-beletskiy-8-683x1024.jpg',
        7 => '/wp-content/uploads/2015/03/vedushiy-oleg-beletskiy-9-1024x684.jpg',
        8 => '/wp-content/uploads/2015/03/vedushiy-oleg-beletskiy-10-1024x683.jpg',
        9 => '/wp-content/uploads/2015/03/vedushiy-oleg-beletskiy-11-1024x683.jpg',
        10 => '/wp-content/uploads/2015/03/vedushiy-oleg-beletskiy-1-1024x683.jpg',
      ),
      'videos' => 
      array (
        0 => 'https://player.vimeo.com/video/143422673',
        1 => 'https://player.vimeo.com/video/158818326',
        2 => 'https://player.vimeo.com/video/143422858',
        3 => 'https://player.vimeo.com/video/143422158',
      ),
    ),
    4 => 
    array (
      'href' => 'http://top15moscow.ru/vedushie/mihail-belyanin/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2015/03/Kopiya-Mihail-Belyanin.jpg',
      'name' => 'Михаил',
      'surname' => 'Белянин',
      'alias' => 'mihail-belyanin',
      'phone' => '
              +7 985 136-91-53',
      'meta_description' => ' Михаил Белянин — ведущий Comedy Radio, обладатель премии «Радиомания 2015» ',
      'email' => 'www.belyanin.com',
      'profession' => 'Ведущий',
      'description' => '
              <p>Михаил Белянин — профессиональный ведущий, который в спокойной и ненавязчивой манере сделает ваше мероприятие очень веселым, интеллигентным и интересным для гостей высокого уровня.</p>
              
              <div class=""> <blockquote><p>Вы будете смеяться, но вы будете смеяться!</p></blockquote>
<p>Организаторам мероприятия, останется только получать удовольствие, также как это каждый будний день делают тысячи москвичей по дороге на работу под голос и юмор Михаила.</p>
<p>Популярность Михаила как ведущего на свадьбе постоянно растет. Он успешно сотрудничает с известными свадебными агентствами, проводит выездные торжества. Отличительной чертой его мероприятий являются яркость, интеллигентность, высокий градус эмоций.</p>
<p>На корпоративах ведущий Михаил Белянин — мастер работы с коллективом. Для него не составит труда организовать и увлечь как 5, так 1000 человек.</p>
<p>Между прочим, максимальной его аудиторией было 2000 зрителей! Свои корпоративные проекты Михаилу доверили Obi, BNP-Paribas, ATON и другие крупные компании и организации.</p>
 </div>
            ',
      'short_about' => 'На данном этапе Михаил  успешно совмещает ведение мероприятий и работу радиоведущим на ComedyRadio. На радио, в течение 4 часов, он непрерывно импровизирует, выдумывает различные шутки, заряжая слушателей позитивом на весь день.',
      'photos' => 
      array (
        0 => '/wp-content/uploads/2015/03/Mikhail-Belianin-7-1024x682.jpg',
        1 => '/wp-content/uploads/2015/03/Mikhail-Belianin-5.jpg',
        2 => '/wp-content/uploads/2015/03/Mikhail-Belianin-9-1024x681.jpg',
        3 => '/wp-content/uploads/2015/03/Mikhail-Belianin-10-e1501080273450.jpg',
        4 => '/wp-content/uploads/2015/03/Mikhail-Belianin-11-1024x684.jpg',
        5 => '/wp-content/uploads/2015/03/Mikhail-Belianin-12-1024x678.jpg',
        6 => '/wp-content/uploads/2015/03/Mikhail-Belianin-13-1024x683.jpg',
        7 => '/wp-content/uploads/2015/03/Mikhail-Belianin-14.jpg',
        8 => '/wp-content/uploads/2015/03/Mikhail-Belianin-15-1024x675.jpg',
      ),
      'videos' => 
      array (
        0 => 'https://player.vimeo.com/video/157196611',
      ),
    ),
    5 => 
    array (
      'href' => 'http://top15moscow.ru/vedushie/maksim-markevich/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2015/03/Kopiya-Maksim-Markevich.jpg',
      'name' => 'Максим',
      'surname' => 'Маркевич',
      'alias' => 'maksim-markevich',
      'phone' => '
              +7 916 321-91-41',
      'meta_description' => ' Ведущий Максим Маркевич — один из самых известных и ярких шоуменов России ',
      'email' => 'www.damaks.tv',
      'profession' => 'Ведущий',
      'description' => '
              <p>В свадьбах и других мероприятиях Максим больше всего ценит креативный подход, нетривиальные решения и преданность своему делу.</p>
              
              <div class=""> <blockquote><p>Как-то раз ведущий предложил молодожёнам снять фильм, в котором действие происходило в будущем. Ребята наняли гримера из «Мосфильма» и с его помощью визуально состарили молодожёнов, чтобы они в ролике рассказали гостям, кто кем стал в жизни. Этот фильм произвёл настоящий фурор!</p></blockquote>
<p>Максим убеждён, что каждый человек «должен быть на своём месте».</p>
<p>Креатив Маркевича заметен во всех проявлениях. Его нельзя назвать типичным ведущим, ведь каждое мероприятие он продумывает с особой тщательностью.</p>
 </div>
            ',
      'short_about' => 'Максим Маркевич — шоумен всея Руси. Снимает клипы для молодожёнов, которые набирают миллионы просмотров на ютуб.',
      'photos' => 
      array (
        0 => '/wp-content/uploads/2015/03/55d2e7ade8390-1024x683.jpeg',
        1 => '/wp-content/uploads/2015/03/55d2e7aea28dd-1024x517.jpeg',
        2 => '/wp-content/uploads/2015/03/markevich1-738x1024.jpg',
        3 => '/wp-content/uploads/2015/03/markevich2-1024x681.jpg',
        4 => '/wp-content/uploads/2015/03/markevich6-684x1024.jpg',
        5 => '/wp-content/uploads/2015/03/markevich8.jpg',
        6 => '/wp-content/uploads/2015/03/markevich9.jpg',
        7 => '/wp-content/uploads/2015/03/5903c94c3a01d-743x1024.jpeg',
      ),
      'videos' => 
      array (
        0 => 'https://player.vimeo.com/video/147004157',
      ),
    ),
    6 => 
    array (
      'href' => 'http://top15moscow.ru/vedushie/konstantin-anisimov/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2015/03/Kopiya-Konstantin-Anisimov.jpg',
      'name' => 'Константин',
      'surname' => 'Анисимов',
      'alias' => 'konstantin-anisimov',
      'phone' => '
              +7 (962) 911-28-28',
      'meta_description' => ' Ведущий Константин Анисимов — автор и актер телевизионного шоу Вечерний Ургант ',
      'email' => 'www.konstantinanisimov.ru',
      'profession' => 'Ведущий',
      'description' => '
              <p>Константин Анисимов — автор и актер телевизионного шоу Вечерний Ургант.</p>
              
              <div class=""> <blockquote><p>Константин — единственный из ведущих, кто одновременно с этим является чемпионом России по футболу</p></blockquote>
<p>Свой талант и душу Константин дарит не только на корпоративных мероприятиях известных компаний, но также и на различных церемониях, юбилеях, рекламных кампаниях и др.</p>
<p>Многочисленные восторженные отзывы лишний раз подтверждают наличие у Константина отменного чувства юмора, такта, способности ощущать все желания публики и неиссякаемого позитива.</p>
<p>Константин Анисимов востребован также и как ведущий на свадьбу. Его приоритетом является обязательный контакт со всеми присутствующими, он внимателен к самым маленьким деталям. Именно это и помогает ему создавать исключительно интересные и успешные события, наполненные позитивом.</p>
 </div>
            ',
      'short_about' => 'Константин — единственный ученик Ивана Урганта. Посмотрев на него в работе, Иван поклялся больше никого не обучать.',
      'photos' => 
      array (
        0 => '/wp-content/uploads/2015/03/anisimov2-e1480106362893.jpg',
        1 => '/wp-content/uploads/2015/03/anisimov6-683x1024.jpg',
        2 => '/wp-content/uploads/2015/03/anisimov8-683x1024.jpg',
        3 => '/wp-content/uploads/2015/03/anisimov5-683x1024.jpg',
        4 => '/wp-content/uploads/2015/03/anisimov7-683x1024.jpg',
        5 => '/wp-content/uploads/2015/03/anisimov9-683x1024.jpg',
        6 => '/wp-content/uploads/2015/03/anisimov3-682x1024.jpg',
        7 => '/wp-content/uploads/2015/03/anisimov1-1024x670.jpg',
      ),
      'videos' => 
      array (
        0 => 'https://player.vimeo.com/video/144962607',
      ),
    ),
    7 => 
    array (
      'href' => 'http://top15moscow.ru/vedushie/egor-pirogov/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2015/03/Kopiya-Egor-Pirogov.jpg',
      'name' => 'Егор',
      'surname' => 'Пирогов',
      'alias' => 'egor-pirogov',
      'phone' => '
              +7 929 522-25-55 Наталья',
      'meta_description' => ' Егор Пирогов — Лучший ведущий 2015 года по версии журнала Wedding Magazine Russia ',
      'email' => 'www.egorpirogov.ru',
      'profession' => 'Ведущий',
      'description' => '
              <p>Егор Пирогов — телевизионный ведущий крупных проектов на 5 канале, ТВЦ и РЕН ТВ.</p>
<p>Егор работает в этой сфере уже более 15 лет, накопив за это время достаточно опыта, чтобы удивлять и радовать гостей потрясающим перформансом.</p>
              
              <div class=""> <blockquote><p>Егор всегда мечтал стать диктором, а стал одним из лучших ведущих России. К счастью, не всем мечтам суждено сбываться.</p></blockquote>
<p>За его плечами проведение огромного количества мероприятий: от скромных семейных событий до корпоративов крупных российских компаний с многотысячной публикой. Служил в Малом Драматическом Театре THEATRE EUROPA. Был ведущим на радиостанциях «Радио РОКС», «Эльдорадио 101,4». В 2000 году дебютировал как телеведущий шоу «Телебукмекер» (5 канал, ТВЦ). В этом же году представил телеканалу ТВС проект «Однокашники» —  программа с самым высоким телевизионным рейтингом.</p>
<p>Ведет программу «Дата» (канал ТВЦ). А также тесно сотрудничает с каналом РЕН ТВ: телевизионный ведущий крупных проектов «Проверено на себе», «Актуальное чтиво», «Чистая работа», «Нарушители порядка», «Автохулиган» и «Лотерея-Мобилея».</p>
 </div>
            ',
      'short_about' => 'В Егоре Пирогове есть всё: профессионализм, наличие собственного стиля, обаяние, изящный, убедительный и просто красивый голос, съеденный наспех пирог по бабушкиному рецепту и многое другое.',
      'photos' => 
      array (
        0 => '/wp-content/uploads/2015/03/pirogov1-669x1024.jpg',
        1 => '/wp-content/uploads/2015/03/pirogov2-1024x682.jpg',
        2 => '/wp-content/uploads/2015/03/pirogov3.jpg',
        3 => '/wp-content/uploads/2015/03/pirogov4-1024x683.jpg',
        4 => '/wp-content/uploads/2015/03/pirogov5-708x1024.jpg',
        5 => '/wp-content/uploads/2015/03/pirogov6-1024x681.jpg',
        6 => '/wp-content/uploads/2015/03/pirogov7-1024x683.jpg',
        7 => '/wp-content/uploads/2015/03/pirogov8-683x1024.jpg',
        8 => '/wp-content/uploads/2015/03/pirogov9-1024x611.jpg',
        9 => '/wp-content/uploads/2015/03/pirogov-5-1024x568.jpg',
      ),
      'videos' => 
      array (
        0 => 'https://player.vimeo.com/video/145238987',
        1 => 'https://player.vimeo.com/video/145238008',
      ),
    ),
    8 => 
    array (
      'href' => 'http://top15moscow.ru/vedushie/dmitriy-babaev/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2015/03/Kopiya-Dmitrij-Babaev.jpg',
      'name' => 'Дмитрий',
      'surname' => 'Бабаев',
      'alias' => 'dmitriy-babaev',
      'phone' => '
              +7 985 410-46-12',
      'meta_description' => ' Шоумен Дмитрий Бабаев — постоянный ведущий радиостанции Megapolis FM ',
      'email' => 'www.dbabaev.ru',
      'profession' => 'Ведущий',
      'description' => '
              <p>Если  вы хотите, чтобы на Вашей свадьбе был тот же ведущий, что и у актера Константина Крюкова и Президента футбольной национальной лиги Игоря Ефремова, если готовы доверить свой юбилей так же как генеральный директор медиахолдинга <strong><span style="font-weight: 400;">«</span></strong>Румедия<strong><span style="font-weight: 400;">»</span></strong> Михаил Бергер или мэр Сергиева Посада Виктор Букин, то Дмитрий именно тот кто вам нужен.</p>
              
              <div class=""> <blockquote><p>Постоянный ведущий радиостанции Megapolis FM (89.5) и первый претендент на проведение свадьбы Путина и Кабаевой!</p></blockquote>
<p>Известные личности обращаются к Дмитрию  неслучайно! Ведь этот ведущий обладает уникальнейшим чувством юмора, высоким профессионализмом и потрясающей харизмой!</p>
<p>Сейчас за плечами ведущего Дмитрия Бабаева около тысячи проведенных в Москве мероприятий: незабываемые оригинальные свадебные торжества, огромное количество корпоративов («Business FM», «Сбербанк России», «Банк HSBC», «Mail.ru», «AUDI» и других), новогодних вечеринок, юбилеев и презентаций, а также интереснейших проектов в роли радиоведущего.</p>
 </div>
            ',
      'short_about' => 'Любой Ваш праздник с Дмитрием Бабаевым легко превратится в грандиозное шоу. Позитив, азарт и веселье вам обеспечены!',
      'photos' => 
      array (
        0 => '/wp-content/uploads/2015/03/_069-2-1024x682.jpg',
        1 => '/wp-content/uploads/2015/03/Dmitriy-Babaev-4-1024x682.jpg',
        2 => '/wp-content/uploads/2015/03/Dmitriy-Babaev-6-1024x682.jpg',
        3 => '/wp-content/uploads/2015/03/Dmitriy-Babaev-1024x682.jpg',
        4 => '/wp-content/uploads/2015/03/moon_wedding-elements_presswall_122-2-683x1024.jpg',
        5 => '/wp-content/uploads/2015/03/Dmitriy-Babaev-7-1024x682.jpg',
        6 => '/wp-content/uploads/2015/03/eventconf_022-2-1024x683.jpg',
        7 => '/wp-content/uploads/2015/03/Dmitriy-Babaev-5.jpg',
        8 => '/wp-content/uploads/2015/03/Tver-Colorfest-2-1024x683.jpg',
      ),
      'videos' => 
      array (
        0 => 'https://player.vimeo.com/video/144833776',
        1 => 'https://player.vimeo.com/video/181778761',
        2 => 'https://player.vimeo.com/video/144834078',
      ),
    ),
    9 => 
    array (
      'href' => 'http://top15moscow.ru/vedushie/anton-abuchkaev/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2015/03/Kopiya-Anton-Abuchkaev.jpg',
      'name' => 'Антон',
      'surname' => 'Абучкаев',
      'alias' => 'anton-abuchkaev',
      'phone' => '
              +7 910 428-40-27',
      'meta_description' => ' Ведущий Антон Абучкаев — шоумен с режиссерским образованием ',
      'email' => 'www.abuchkaev.ru',
      'profession' => 'Ведущий',
      'description' => '
              <p>Антон Абучкаев — профессиональный шоумен, обладающий харизмой и отличным чувством юмора. Свою карьеру Антон начал в далёком 2000 году и с того момента провел более 1000 мероприятий в России и за рубежом. Каждое торжество с ним — сказка, не похожая на другие.</p>
              
              <div class=""> <blockquote><p>Антон Абучкаев имеет высшее режиссерское образование, а потому знает, как сделать ваше мероприятие максимально эффектным.</p></blockquote>
<p>Многие известные компании доверяют свои мероприятия Антону Абучкаеву: Страховая Группа Энергогарант, ТД БелАЗ, ОАО «РЖД»,  Министерство Регионального Развития, Страховой Дом ВСК, MAJOR AUTO, HONDA MOTORS и многие другие.</p>
<p>Отдав предпочтение Антону, вы не ошибётесь!</p>
 </div>
            ',
      'short_about' => 'Антон обладает приятной внешностью, бархатным голосом, природной харизмой и обаянием. Он ведет  все праздники интеллигентно и ненавязчиво.',
      'photos' => 
      array (
        0 => '/wp-content/uploads/2015/03/anton-abuchkaev-3-1024x682.jpg',
        1 => '/wp-content/uploads/2015/03/anton-abuchkaev-4-1024x682.jpg',
        2 => '/wp-content/uploads/2015/03/anton-abuchkaev-5-1024x682.jpg',
        3 => '/wp-content/uploads/2015/03/anton-abuchkaev-2-1024x682.jpg',
        4 => '/wp-content/uploads/2015/03/abuchkaev2-1024x681.jpg',
        5 => '/wp-content/uploads/2015/03/anton-abuchkaev-21-2-1024x682.jpg',
      ),
    ),
    10 => 
    array (
      'href' => 'http://top15moscow.ru/vedushie/adis-mammo/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2015/03/Kopiya-Adis-Mammo.jpg',
      'name' => 'Адис',
      'surname' => 'Маммо',
      'alias' => 'adis-mammo',
      'phone' => '
              +7 926 568-57-15',
      'meta_description' => ' Шоумен Адис Маммо — ведущий на телеканалах MTV Russia, ТНТ и на радио Серебряный Дождь ',
      'email' => 'www.adismammo.com',
      'profession' => 'Ведущий',
      'description' => '
              <p>Здравствуйте. Меня зовут Адис.<br>
Ударение на букву «И»)</p>
<p>Я люблю юмор, океан, хорошие книги, бельгийское пиво, сериалы от HBO и NetFlix, итальянские костюмы, лигу чемпионов УЕФА, свою жену и дочку.</p>
<p>У меня отсутствует селфи-палка и я не блогер.</p>
<p>Если в нескольких словах описать мой формат ведения, то получится так:<br>
Современное и стильное ведение, основой которого является общение и юмор.</p>
              
              <div class=""> <blockquote><p>Агентства говорят про меня следущее: Легко, с тонким юмором и атмосферно.<br>
Моя жена про меня говорит так: С работы поедешь, купи воды домой.</p></blockquote>
<p>Своими сильными сторонами считаю интеллект, умение импровизировать и адекватность.<br>
Подготовка к мероприятию для имеет принципиальное значение.</p>
<p>Обязательно посмотрите видео. Из него вы поймете, как я говорю, как я выгляжу, как работают мои мимические мышцы и какой у меня тембр.</p>
<p>а еще: я создатель проекта #еслибыпеснябылаотомчтопроисходитвклипе</p>
 </div>
            ',
      'short_about' => 'Экс-ведущий утреннего шоу на радио Серебряный Дождь с Михаилом Шацем и Татьяной Лазаревой',
      'photos' => 
      array (
        0 => '/wp-content/uploads/2015/03/5.jpg',
        1 => '/wp-content/uploads/2015/03/FW-1106-WEB-360-1024x683.jpg',
        2 => '/wp-content/uploads/2015/03/DIM_5523-1024x683.jpg',
        3 => '/wp-content/uploads/2015/03/2.jpg',
        4 => '/wp-content/uploads/2015/03/mammo6-1024x683.jpg',
        5 => '/wp-content/uploads/2015/03/3.jpg',
        6 => '/wp-content/uploads/2015/03/SergeySasha_27-08-2016_0951-1024x683.jpg',
        7 => '/wp-content/uploads/2015/03/12345.jpg',
        8 => '/wp-content/uploads/2015/03/mammo-3-819x1024.jpg',
        9 => '/wp-content/uploads/2015/03/DIM_4616-1024x683.jpg',
        10 => '/wp-content/uploads/2015/03/mammo5.jpg',
      ),
      'videos' => 
      array (
        0 => 'https://player.vimeo.com/video/203853653',
        1 => 'https://player.vimeo.com/video/174353537',
        2 => 'https://player.vimeo.com/video/176480106',
        3 => 'https://player.vimeo.com/video/207934532',
      ),
    ),
    11 => 
    array (
      'href' => 'http://top15moscow.ru/vedushie/aleksandr-slesarev/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2015/03/Kopiya-Aleksandr-Slesarev.jpg',
      'name' => 'Александр',
      'surname' => 'Слесарев',
      'alias' => 'aleksandr-slesarev',
      'phone' => '
              +7 906 036-00-22',
      'meta_description' => ' Александр Слесарев — Лучший ведущий 2016 года по версии журнала Wedding Magazine Russia ',
      'email' => '',
      'profession' => 'Ведущий',
      'description' => '
              <p>Безукоризненный, тактичный, ответственный, стильный, а главное — искренний шоумен. Личная жизнь и работа для него — одно целое. По этой причине он одинаково добр ко всем: как к своим родным и близким, так и к гостям праздничного события.</p>
<p>Шоумен первым на практике доказал формулу: Александр Слесарев = ( Улыбка + Чувство юмора + Чувство такта + Искренность + Идеальный образ ведущего + Запоминающаяся внешность) * 130 мероприятий в год + сотрудничество с лучшими event агентствами + любимая супруга и двое очаровательных деток. Он искренен в отношении ко всему, а потому публика всегда воспринимает шоумена приветливо и тепло.</p>
              
              <div class=""> <blockquote><p>Александр называет себя счастливым человеком. Для него главное в жизни — семья и дети, а в людях ведущий ценит жизнелюбие и чувство юмора.</p></blockquote>
<p>Сценарии создаются ведущим индивидуально для каждого мероприятия с учетом всех требований и пожеланий. Слесарев может работать с любой аудиторией. Благодаря душевности и отменному чувству юмора он с легкостью может подбодрить, снять волнение и общее напряжение гостей, засыпать всех присутствующих забавными шутками и развлечь абсолютно любую публику.</p>
 </div>
            ',
      'short_about' => 'Александр Слесарев — любимый ведущий московских агентств, Чемпион Премьер-Лиги КВН',
      'photos' => 
      array (
        0 => '/wp-content/uploads/2015/03/slesarev-5-1024x683.jpg',
        1 => '/wp-content/uploads/2015/03/slesarev-4-1024x683.jpg',
        2 => '/wp-content/uploads/2015/03/slesarev-3-1024x683.jpg',
        3 => '/wp-content/uploads/2015/03/slesarev-8-1024x680.jpg',
        4 => '/wp-content/uploads/2015/03/slesarev-10-1024x683.jpg',
        5 => '/wp-content/uploads/2015/03/slesarev-2-961x1024.jpg',
      ),
      'videos' => 
      array (
        0 => 'https://player.vimeo.com/video/156479423',
      ),
    ),
    12 => 
    array (
      'href' => 'http://top15moscow.ru/vedushie/maksim-kritskiy/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2015/03/Kopiya-Maksim-Kritskij.jpg',
      'name' => 'Максим',
      'surname' => 'Крицкий',
      'alias' => 'maksim-kritskiy',
      'phone' => '
              +7 917 504-43-87',
      'meta_description' => ' Ведущий Максим Крицкий — тёплый, как плед и добрый, как объятия мамы ',
      'email' => 'www.maks-kritskiy.ru',
      'profession' => 'Ведущий',
      'description' => '
              <p>Максим Крицкий один из тех профессионалов, которые безупречно чувствуют, где именно находится золотая середина.</p>
              
              <div class=""> <blockquote><p>У шоумена итальянский костюм, швейцарские часы, немецкий автомобиль, крымская жена и точно такой же загар, а сын — коренной москвич.</p></blockquote>
<p>Ведущий обладает чувством такта и юмора, что делает его приятным собеседником в любой компании. Максим искренне считает, что каждое мероприятие – это маленькая жизнь, и эту жизнь надо прожить на полную! Макс истинный фанат своего дела. Каждый год ему доверяют более 100 престижных мероприятий.</p>
<p>В основном это свадебные приемы, масштабные корпоративные мероприятия, современные выставки, модные показы, эффектные презентации и даже научные конференции о развитии мировой акупунктуры.</p>
<p>Крицкий также любит благотворительные проекты и с удовольствием участвует в них.</p>
 </div>
            ',
      'short_about' => 'Шоумен  работает в сфере event-индустрии с 2006 года. За эти годы он с блеском провёл мероприятия, доказав публике свой профессионализм. Он полюбился тысячам людей, заработав себе безупречную репутацию. Там, где есть Максим, скучно не бывает.',
      'photos' => 
      array (
        0 => '/wp-content/uploads/2015/03/ivanushki-1024x683.jpg',
        1 => '/wp-content/uploads/2015/03/gYE7JYR5aq8-1024x683.jpg',
        2 => '/wp-content/uploads/2015/03/BIO_7752-1024x681.jpg',
        3 => '/wp-content/uploads/2015/03/Ot-YUrchenko-Nasti-1024x683.jpg',
        4 => '/wp-content/uploads/2015/03/DG4_3174-1024x681.jpg',
        5 => '/wp-content/uploads/2015/03/Intervyu-1024x683.jpg',
        6 => '/wp-content/uploads/2015/03/ot-Zlobina-Koli-15-aprelya-1024x683.jpg',
        7 => '/wp-content/uploads/2015/03/BIO_7684-1024x681.jpg',
      ),
      'videos' => 
      array (
        0 => 'https://player.vimeo.com/video/224437163',
      ),
    ),
    13 => 
    array (
      'href' => 'http://top15moscow.ru/vedushie/roman-klyachkin/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2015/03/Kopiya-Roman-Klyachkin.jpg',
      'name' => 'Роман',
      'surname' => 'Клячкин',
      'alias' => 'roman-klyachkin',
      'phone' => '
              +7 916 285-91-69',
      'meta_description' => ' Роман Клячкин — ведущий с уникальным чувством юмора, участник «Убойной лиги» и «Comedy Club» ',
      'email' => 'www.romanklyachkin.ru',
      'profession' => 'Ведущий',
      'description' => '
              <p>Свой путь будущий шоумен начал, как и многие: с КВН. Первый раз побывал на сцене, будучи ещё маленьким школьником.</p>
              
              <div class=""> <blockquote><p>Во всех уголках России, где побывал Клячкин, остались нотки позитива, которыми он навсегда запомнился своим поклонникам.</p></blockquote>
<p>Далее праздники следовали один за другим: Новый Год, 8 марта… И юный КВНщик делал заметные успехи.</p>
<p>Роман был одним из участников дуэта «Красивые», в котором и победил в шоу «Смех без правил» на ТНТ, участвовал в проекте «Убойная лига», «Comedy Club» и многих других.</p>
<p>Теперь же Роман — один из самых известных шоуменов Москвы. Он весёлый, энергичный, обладает прекрасным чувством юмора и способен подарить хорошее настроение всем присутствующим.</p>
 </div>
            ',
      'short_about' => 'Самый сексуальный мужчина улицы Щорса города Красноярск. Лучший вратарь Красноярского края конца 90х. Единственный ведущий Москвы, который ездил на праворукой Ниссан Премьере цвета мокрого асфальта, на колпаках.',
      'photos' => 
      array (
        0 => '/wp-content/uploads/2015/03/klyachkin7-1024x683.jpg',
        1 => '/wp-content/uploads/2015/03/klyachkin9-1024x683.jpg',
      ),
      'videos' => 
      array (
        0 => 'https://player.vimeo.com/video/190700407',
        1 => 'https://player.vimeo.com/video/201741209',
      ),
    ),
    14 => 
    array (
      'href' => 'http://top15moscow.ru/vedushie/andrey-predelin/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2015/03/Kopiya-Andrej-Predelin.jpg',
      'name' => 'Андрей',
      'surname' => 'Пределин',
      'alias' => 'andrey-predelin',
      'phone' => '
              +7 915 352-50-00',
      'meta_description' => ' Обаятельный ведущий Андрей Пределин — генерирует пять шуток в минуту. За деньги – семь. ',
      'email' => 'www.predelin.ru',
      'profession' => 'Ведущий',
      'description' => '
              <h2>Ведущий Андрей Пределин — именно тот шоумен, когда нужно элегантно, но не скучно.</h2>
<p>Весело, но без пошлости. Активно, но в меру.  Интересно, но ненавязчиво. Ведет телевизионные развлекательные и туристические шоу. Отличительная особенность – импровизация и непринужденность.</p>
              
              <div class=""> <blockquote><p>Математический лицей, два высших образования — все это было по плечу, а теперь уже за плечами у Андрея, однако, благодаря КВНу, в какой-то момент ведущий Андрей Пределин понял, что шутки в его голове рождаются намного быстрее, чем сложные формулы. Так был сделан выбор в пользу профессии шоумена.</p></blockquote>
<p>Сейчас Андрей Пределин может похвастаться особым, легким стилем проведения мероприятий. Он тонко чувствует грань между тем, о чем можно и нельзя говорить. Поможет составить такой сценарий, который придется по душе гостям любого возраста и статуса. Быстро соображает, импровизирует и выдает целое облако позитивных эмоций!</p>
<p>Будучи ведущим, в том числе, и свадебных мероприятий, Андрей Пределин четко помнит, что такое событие бывает раз в жизни и этот день нужно провести так, чтобы спустя годы воспоминания были не менее красочными и захватывающими, чем сам праздник. Глубоко индивидуальный подход, обсуждение мелочей и нюансов, уместные шутки — вот далеко не полный арсенал, с которым Андрей подходит к вашему торжеству.</p>
 </div>
            ',
      'short_about' => 'Андрей умеет быстро и неглупо импровизировать, выдавать нескончаемые потоки смешных шуток и дарить хорошее настроение всем: как виновникам события, так и его гостям!',
      'photos' => 
      array (
        0 => '/wp-content/uploads/2015/03/Andrey_Predelin_078-e1495453981970-1024x614.jpg',
        1 => '/wp-content/uploads/2015/03/FG7A5361-1024x683.jpg',
        2 => '/wp-content/uploads/2015/03/predelin5-1024x683.jpg',
        3 => '/wp-content/uploads/2015/03/IMG_9221-1024x683.jpg',
        4 => '/wp-content/uploads/2015/03/Kopiya-IMG_8378-683x1024.jpg',
        5 => '/wp-content/uploads/2015/03/Wedding_NB_713-1024x683.jpg',
        6 => '/wp-content/uploads/2015/03/fIhuHB94uic-1024x682.jpg',
        7 => '/wp-content/uploads/2016/05/gal-53061-1024x682.jpg',
      ),
      'videos' => 
      array (
        0 => 'https://player.vimeo.com/video/176480887',
        1 => 'https://player.vimeo.com/video/176480868',
      ),
    ),
  ),
  'videografy' => 
  array (
    0 => 
    array (
      'href' => 'http://top15moscow.ru/videografy/miras-film/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2017/08/portretGENA2680-1-1.jpg',
      'name' => 'Miras',
      'surname' => 'Film',
      'alias' => 'miras-film',
      'phone' => '
              +7 925 146-77-53',
      'meta_description' => ' MIRAS FILM: when films come true! ',
      'email' => 'www.mirasfilm.me',
      'profession' => 'Видеографы',
      'description' => '
              <p>Компания MIRAS FILM за почти 5 лет существования выросла из 2-х человек в полноценное предприятие: просторная студия, собственное оборудование, полный комплект кино-света, микроавтобус, хромакей, налаженная работа с регионами и талантливый пул авторов — в общем,  всё готово к тому, если «идея нужна через час», а «съемка уже завтра».</p>
              
              <div class=""> <p>Сегодня MIRAS FILM — это сразу несколько направлений: свадьбы, реклама, имиджевые ролики, съемки клипов и концертов. Вот лишь несколько клиентов, с которыми мы работали: «Альфа-Банк», «Доминант», «Мясницкий ряд», «Международная зерновая компания», «Лаборатория Касперского», «Семейный доктор», «Уралкалий», МГИМО, РЖД, Металлоинвест, Imperial Tobacco, Sandoz Novartis, Johnson &amp; Johnson, «Word Class», Nestle, Playboy, Loreal, Coca-Cola.</p>
 </div>
            ',
      'short_about' => 'Свои праздники нам доверяли звезды шоу бизнеса, бизнесмены из списка Forbes и политическая элита страны. Мы привыкли каждый раз удивлять клиента и сделаем картинку вашей мечты реальной!',
      'videos' => 
      array (
        0 => 'https://player.vimeo.com/video/229836122',
        1 => 'https://player.vimeo.com/video/ 229833601',
        2 => 'https://player.vimeo.com/video/229835981',
      ),
    ),
    1 => 
    array (
      'href' => 'http://top15moscow.ru/videografy/jenevastudio/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2017/04/Vildan-H-1600-2.jpg',
      'name' => 'Jenevastudio',
      'alias' => 'jenevastudio',
      'phone' => '
              +7 (921) 999-71-81',
      'meta_description' => ' Кино, у которого есть сердце ',
      'email' => 'www.jenevastudio.com',
      'profession' => 'Видеографы',
      'description' => '
              <p class="black-light-text">Jenevastudio — видеопродакшн, основанный в 2014 году. С самого начала своей деятельности они поставили себе задачу создавать истории, вдохновляющие на самые искренние чувства и эмоции. Безграничная любовь к своему делу и невероятная самоотдача — это то, что делает кино от Jenevastudio по-настоящему живым, честным и душевным.</p>
<p>Действительно, каждый результат совместной работы профессионалов Jenevastudio и клиента — это настоящее маленькое кино о самых трогательных и главных моментах в жизни людей. Они стараются запечатлеть неуловимое. Каждый кадр — любовь.</p>
<p>— Ты хочешь взять с собой что-нибудь особенное?<br>
— Тебя… </p>
<p>Любовь — странное и сложное чувство, не признающее мнение окружающих. Любовь — это недуг, поражающий души и сердца людей независимо от того, кто они. Любовь — это всепоглощающая страсть без остатка, о которой зачастую так сложно произнести хотя бы одно слово, хотя хочется показать целую историю… (с) Jenevastudio</p>
              
              <div class=""> <p>На счету Jenevastudio серьезные победы и признания на высочайшем уровне.<br>
EEVA wedding video contest 2015<br>
The best  Cameramen (Лучший свадебный оператор)<br>
The best Promo (Лучший промо-ролик)<br>
2nd Exotic&amp;Erotic</p>
 </div>
            ',
      'short_about' => 'Jenevastudio — это особенное сочетание личностей, абсолютно вовлеченных в свое творчество профессионалов.<br>
Jenevastudio -истории без границ, рождающиеся в любой точке Земли.',
      'videos' => 
      array (
        0 => 'https://player.vimeo.com/video/151516861',
        1 => 'https://player.vimeo.com/video/215255715',
        2 => 'https://player.vimeo.com/video/215255875',
        3 => 'https://player.vimeo.com/video/215256309',
        4 => 'https://player.vimeo.com/video/215257812',
        5 => 'https://player.vimeo.com/video/215259373',
        6 => 'https://player.vimeo.com/video/215260110',
        7 => 'https://player.vimeo.com/video/215261358',
      ),
    ),
    2 => 
    array (
      'href' => 'http://top15moscow.ru/videografy/studiya-art-island/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2017/04/160711_top15photographers-1505.jpg',
      'name' => 'Студия',
      'surname' => '«Art',
      'alias' => 'studiya-art-island',
      'phone' => '
              +7 495 776-72-73',
      'meta_description' => ' Art Island — Свадебное видео Премиум класса ',
      'email' => 'www.artisland.com',
      'profession' => 'Видеографы',
      'description' => '
              <p>Ролики, которые снимают видеографы Art Island – больше, чем просто видео. На протяжении многих лет студия считается лучшей в России и странах СНГ.</p>
              
              <div class=""> <blockquote><p>Видеографы работают как в России так и за рубежом: в Италии, Таиланде, ОАЭ, Малайзии, Индонезии и других странах.</p></blockquote>
<p>Art Island — профессиональная студия, занимающаяся съёмкой свадебных видеороликов премиум класса. Основана Артуром Инамовым и Андреем Меликовым.</p>
<p>Помимо съёмок свадеб, команда также занимается проведением семинаров и мастер-классов, в которых учит новичков и будущих видеографов основам профессии. Art Island также снимает семейные пары, «Love story» и коммерческие клипы.</p>
 </div>
            ',
      'short_about' => 'КогдаArt Island посредством видео рассказывают чьи-либо истории, они наделяют её любовью и жизнью, именно поэтому ваше событие останется с вами навсегда и войдет в историю.',
      'videos' => 
      array (
        0 => 'https://player.vimeo.com/video/174092306',
        1 => 'https://player.vimeo.com/video/148921764',
        2 => 'https://player.vimeo.com/video/126677151',
        3 => 'https://player.vimeo.com/video/125457850',
        4 => 'https://player.vimeo.com/video/170939608',
        5 => 'https://player.vimeo.com/video/29784965',
        6 => 'https://player.vimeo.com/video/193106509',
        7 => 'https://player.vimeo.com/video/44723248',
        8 => 'https://player.vimeo.com/video/107827576',
        9 => 'https://player.vimeo.com/video/40065562',
        10 => 'https://player.vimeo.com/video/66410476',
        11 => 'https://player.vimeo.com/video/135140021',
      ),
    ),
    3 => 
    array (
      'href' => 'http://top15moscow.ru/videografy/roman_khlustov',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2017/02/523A5348-Edit-Edit-2.jpg',
      'name' => 'Роман',
      'surname' => 'Хлюстов',
      'alias' => 'roman_khlustov',
      'phone' => '
              +7 964 470-28-66',
      'meta_description' => ' Роман Хлюстов - Победитель премии Wedding Awards 2016 в номинации "Лучший свадебный видеограф" ',
      'email' => 'www.khlyustov.com',
      'profession' => 'Видеограф',
      'description' => '
              <p>Мы очень любим работать на свадьбах. Нас окружают красивые, нарядные, счастливые люди, которые не скупятся на эмоции, которые чутки к окружающим, трогательны в своей радости, прекрасны в выражении чувств. Мы заряжаемся от них, мы хотим, чтобы они смогли раз за разом видеть и переживать то же, просматривая видео. Чтобы каждый миг этого важного события остался не только в памяти, но и в чём-то материально ценном.</p>
              
              <div class=""> <blockquote><p>Естественность, правда и никаких постановок.</p></blockquote>
<p>Пожалуй, иногда страшно — потому что то, что мы снимаем, — будут смотреть несколько поколений. Это большая ответственность, которая заставляет раз за разом становиться лучше: так мы ушли от погони за красивой картинкой и пришли к ловле эмоций. Выражения лиц, слова, жесты, движения для нас важнее удачных пролётов со стедикамом.</p>
 </div>
            ',
      'short_about' => 'Роман Хлюстов и его команда также увлекается съемкой короткометражного кино. Так, в 2016 году история любви «Главное, что не овца», снятая для одной нашей пары, стала победителем фестиваля короткометражного кино «Арткино» (город Москва) в номинации лучший фильм.',
      'videos' => 
      array (
        0 => 'https://player.vimeo.com/video/203966258',
        1 => 'https://player.vimeo.com/video/168743806',
        2 => 'https://player.vimeo.com/video/198529596',
        3 => 'https://player.vimeo.com/video/183638106',
        4 => 'https://player.vimeo.com/video/153499444',
        5 => 'https://player.vimeo.com/video/177186559',
      ),
    ),
    4 => 
    array (
      'href' => 'http://top15moscow.ru/videografy/vitalij-sidorenko/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/16.07.10-top15photographers-7152.jpg',
      'name' => 'Виталий',
      'surname' => 'Сидоренко',
      'alias' => 'vitalij-sidorenko',
      'phone' => '
              +7 925 303-50-04',
      'meta_description' => ' Видеограф Виталий Сидоренко — победитель премии  International Zankyou Wedding Awards 2017 ',
      'email' => 'www.vsidorenko.com',
      'profession' => 'Видеограф',
      'description' => '
              <p>Виталий входит в число лучших видеографов СНГ по версиям крупнейших свадебных порталов.<br>
Работает с командой профессионалов, в России, а также и за рубежом: в Италии, Франции, Англии, Черногории, Греции и других странах.</p>
              
              <div class=""> <blockquote><p>Им под силам организовать видеосъёмку любой сложности и для любого праздника. Будь это небольшая свадьба или масштабный проект, торжество звёздной пары или съёмка за границей. Техническое оборудование для съемок, которое имеется позволяет получать качественный материал сопоставим по уровню музыкальных и рекламных роликов. Также они большое внимание уделяют качественной аэросъёмке.</p></blockquote>
<p>Виталий успешно сотрудничает со многими популярными event-агентствами и упорно идет к своей цели — быть признанным не только у соотечественников, но и на уровне мирового масштаба.</p>
<p>Достижения:<br>
Победитель Премии IZWA 2017 — International Zankyou Wedding Awards.<br>
Победитель и призёр свадебных конкурсов организованными European Event Videographers Association EEVA, Annual Wedding Video Contest 2010 в номинации «Лучший свадебный клип».<br>
Приглашался в качестве члена жюри и эксперта на этот же конкурс.<br>
Успешно проводит авторские мастер-классы по видеографии.<br>
Входит в число лучших видеографов СНГ по версиям крупнейших свадебных порталов.<br>
Его работы публикуются в отчетах известных event-агентств в свадебных интернет изданиях.<br>
Его требования заставляют добиваться высокого уровня и быть среди лучших.<br>
Помимо всех личностных достижений, Виталий очень увлечен своим делом и видео для него стало неотъемлемой частью жизни.</p>
 </div>
            ',
      'short_about' => 'Если Вы ищите лучшее качество и хотите быть уверенными в красоте Вашего видео, то Вы на правильном пути.',
      'videos' => 
      array (
        0 => 'https://player.vimeo.com/video/226214668',
        1 => 'https://player.vimeo.com/video/222504105',
        2 => 'https://player.vimeo.com/video/152164360',
        3 => 'https://player.vimeo.com/video/193275734',
        4 => 'https://player.vimeo.com/video/117292818',
      ),
    ),
    5 => 
    array (
      'href' => 'http://top15moscow.ru/videografy/sergej-novikov/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/160711-top15photographers-563.jpg',
      'name' => 'Сергей',
      'surname' => 'Новиков',
      'alias' => 'sergej-novikov',
      'phone' => '
              +7 963 316-58-08',
      'meta_description' => ' Видеограф Сергей Новиков — финалист премии Wedlife 2015 в номинации «Лучший свадебный видеограф» ',
      'email' => 'www.vimeo.com/snovikov',
      'profession' => 'Видеограф',
      'description' => '
              <p>Видеограф Сергей Новиков занимается своей деятельностью на протяжении 11 лет. В свадебной и семейной съемке ценит возможность зафиксировать на камеру моменты реальной жизни.</p>
              
              <div class=""> <blockquote><p>К счастью невозможно привыкнуть и относится как к чему-то обыденному. Счастье — оно всегда до мурашек… даже, когда ты просто ему свидетель с камерой в руках.</p></blockquote>
<p>Сергей убежден, что создание хорошего фильма зависит не только от мастерства оператора, но в большей степени от настроения главных героев торжества. Каждый раз, снимая главные моменты жизни людей, видеографу передается эмоциональное настроение  и волнение своих героев.</p>
<p>За свои работы, наполненные искренними чувствами, Сергей Новиков стал победителем Wedding Awards 2015 в номинации «Лучшее свадебный видеограф» и финалистом Wedding Awards 2014 в номинации «Лучшее свадебное видео». Также он попал в топ Veter Magazine, Wedding Magazine и ведущих свадебных порталов России и мира.</p>
<p>На этих достижениях Сергей останавливаться не собирается. Впереди он планирует массу интересных проектов и создания новых шедевров.</p>
 </div>
            ',
      'short_about' => 'Его творческая фантазия, широкий размах и реализация задуманного не знает границ.',
      'videos' => 
      array (
        0 => 'https://player.vimeo.com/video/117407696',
        1 => 'https://player.vimeo.com/video/153605195',
        2 => 'https://player.vimeo.com/video/145237030',
        3 => 'https://player.vimeo.com/video/141983474',
        4 => 'https://player.vimeo.com/video/113895530',
      ),
    ),
    6 => 
    array (
      'href' => 'http://top15moscow.ru/videografy/artem-korzhavin/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/160711-top15photographers-379.jpg',
      'name' => 'Артем',
      'surname' => 'Коржавин',
      'alias' => 'artem-korzhavin',
      'phone' => '
              +7 931 340-74-52',
      'meta_description' => ' Видеограф Артем Коржавин: видеосъёмка и никаких постановок ',
      'email' => 'www.akfilms.ru',
      'profession' => 'Видеограф',
      'description' => '
              <p>Артем Коржавин – профессиональный свадебный видеограф Москвы. Специализируется на съёмке свадеб, love story, семейных видеороликов.</p>
              
              <div class=""> <blockquote><p>Рассказывая о ком-то историю в видео, Артем наполняет ее жизнью и любовью, и именно поэтому ваш день останется с вами навсегда таким, каким он был на самом деле.</p></blockquote>
<p>Видеограф снимает свои работы не только в России, но и по всему миру.</p>
<p>В процессе съемки Артём пытается быть совершенно незаметным. В одном из отзывов его поклонница пишет следующее: «Артем, как никто другой, знает свою работу и во время мероприятия сделал все, чтобы его не было не слышно и не видно, что очень важно, ведь ничто не должно отвлекать гостей и виновников мероприятия от самого счастливого дня в жизни – свадьбы».</p>
 </div>
            ',
      'short_about' => 'Снимая свадьбы и другие тожества, видеограф старается не использовать никаких запланированных постановок, чтобы получить искренний живой фильм.',
      'videos' => 
      array (
        0 => 'https://player.vimeo.com/video/121435631',
        1 => 'https://player.vimeo.com/video/154439565',
        2 => 'https://player.vimeo.com/video/126061637',
        3 => 'https://player.vimeo.com/video/118162116',
      ),
    ),
    7 => 
    array (
      'href' => 'http://top15moscow.ru/videografy/yulya-vopilova/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/160711-top15photographers-880.jpg',
      'name' => 'Юля',
      'surname' => 'Вопилова',
      'alias' => 'yulya-vopilova',
      'phone' => '
              +7 922 605-82-44',
      'meta_description' => ' Видеографы Юлия Вопилова и Никита Засыпкин: Победители EUROPEAN WEDDING VIDEOCONTEST 2012 ',
      'email' => 'www.freshfstudio.com',
      'profession' => 'Видеограф',
      'description' => '
              <p>Количество видов деятельности Никиты Засыпкина и Юлии Вопиловой можно перечислять очень долго. Они относятся к числу профессиональных фотографов, талантливых видеографов, звукооператоров, путешественников и имеют массу других увлечений.</p>
              
              <div class=""> <blockquote><p>За 9 лет работы Никита и Юлия ни разу не присутствовали на двух одинаковых свадьбах. Свадьба — это не концепция и не декор, не ведущий, не фото и не видео. Это маленькая история двух влюблённых людей в окружении близких. Эти истории трогательные, нежные, весёлые, зажигательные, и всегда  очень разные. Задача свадебного видео — пересказать их в мельчайших деталях.</p></blockquote>
<p>Их профессиональная деятельность началась в 2008 году. С момента основания, они были участниками одной из самых узнаваемых студий Екатеринбурга, а в конце 2011 года решили расширить свои горизонты и открыли фото-видео студию в самом центре Европы: в красивом и вдохновляющем городе Прага.</p>
<p>С тех пор Юлия и Никита постоянно путешествуют и работают в разных уголках мира, доставляя людям истинное удовольствие от своих работ.</p>
<p>Юля Вопилова и Никита Засыпкин постоянно участвуют и побеждают в различных конкурсах. Их самые яркие достижения: Звание лучшего оператора и звукорежиссера в EEVA — Top 15, (2014-15) 1 место, в конкурсе Wedlife — Top 10 2016. 5 место, 1 место за лучший свадебный клип  в  EUROPEAN WEDDING VIDEOCONTEST 2012.</p>
 </div>
            ',
      'short_about' => 'Динамика, живость съёмки и умение передать насыщенность и атмосферу праздника —отличительная черта роликов Юли и Никиты.',
      'videos' => 
      array (
        0 => 'https://player.vimeo.com/video/157149955',
        1 => 'https://player.vimeo.com/video/171979252',
        2 => 'https://player.vimeo.com/video/137123644',
        3 => 'https://player.vimeo.com/video/129961337',
      ),
    ),
    8 => 
    array (
      'href' => 'http://top15moscow.ru/videografy/family-frames/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/160711-top15photographers-1387.jpg',
      'name' => 'Павел',
      'surname' => 'Трепов',
      'alias' => 'family-frames',
      'phone' => '
              +7 905 713-88-11',
      'meta_description' => ' Студия «Family Frames» — история только начинается! ',
      'email' => 'www.familyframes.ru',
      'profession' => 'Видеограф',
      'description' => '
              <p>Студия FamilyFrames занимается видеосъемкой свадебных торжеств и основана в Москве, а сам Павел является автором лучших свадебных клипов, качественных фильмов с интересными семейными историями и трогательными лавстори.</p>
              
              <div class=""> <blockquote><p>Если ваша история попадает в мастерские руки Павла, она оживает, приобретает неповторимый смысл и насыщается яркими красками.</p></blockquote>
<p>В студии работают самые талантливые фотографы и видеографы, которые со своей командой колесят по всему миру и снимают чрезвычайно необычные свадебные церемонии разных народов. В итоге получаются захватывающие интересные истории с удивительным магнетизмом, шармом и творческой вкусностью.</p>
<p>Наряду с этой деятельностью, студия занимается проведением уникальных мастер классов, где на практике раскрываются все секреты создания прекрасных шедевров.</p>
<p>Одним из талантливых операторов компании является Павел Трепов. Здесь он успешно трудится начиная с 2011 года и имеет большой опыт оператора, видеографа и монтажера фильмов.</p>
 </div>
            ',
      'short_about' => 'Талантливые работы студии familyframes постоянно попадают в топ лучших по результатам EEVA и Wedlife. Далеко идущие грандиозные планы направлены на покорение новых высот и охватывают самые дальние уголки земного шара.',
      'videos' => 
      array (
        0 => 'https://player.vimeo.com/video/200593188',
        1 => 'https://player.vimeo.com/video/207564045',
        2 => 'https://player.vimeo.com/video/180908832',
        3 => 'https://player.vimeo.com/video/206795220',
        4 => 'https://player.vimeo.com/video/187384812',
      ),
    ),
    9 => 
    array (
      'href' => 'http://top15moscow.ru/videografy/ivan-zalevskij/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/020-3.jpg',
      'name' => 'Иван',
      'surname' => 'Залевский',
      'alias' => 'ivan-zalevskij',
      'phone' => '
              +7 916 123-93-14',
      'meta_description' => ' Видеограф Иван Залевский — мероприятие в хронологическом порядке ',
      'email' => 'www.zalevichfilms.com',
      'profession' => 'Видеограф',
      'description' => '
              <p>Иван Залевский — известный видеограф Москвы. Основная специфика его деятельности заключается в съемке свадебных торжеств. В своих работах главный акцент он делает на эмоциональных чувства влюбленной пары, четко улавливая моменты прикосновений рук, искренних улыбок и слез радости.</p>
              
              <div class=""> <blockquote><p>Если вы посмотрите все свадебные видео этого профессионала, то не найдёте среди них даже двух похожих.</p></blockquote>
<p>В 2013 году, Залевский был приглашен в состав членов жюри известного конкурса Wed life Awards. Все свадебные видео Ивана уникальны и не похожи друг на друга.</p>
<p>Для работы, он подключает не менее 3-х операторов, чтобы ничего не упустить и наиболее полно отобразить важный день в жизни молодых. 90% времени отдается под репортажную съемку и при ее монтаже соблюдается хронологическая последовательность праздника и учитываются пожелания пары.</p>
<p>Отразить индивидуальность, показать атмосферу свадьбы, ее неповторимость — это, пожалуй, главная задача видеографа. В его фильмах мы стараемся делать акцент сделан не столько на оформлении (пусть даже и прекрасном), сколько на эмоциях пары. Потому что слезы радости, искренние улыбки, прикосновения рук никогда не выйдут из моды и сохранят воспоминания об этом дне навсегда.</p>
<p>Любите друг друга, а мы поможем вам разглядеть, какие вы особенные.</p>
 </div>
            ',
      'short_about' => 'Настоящие и искренние эмоции никогда не выйдут из моды, поэтому талантливые работы Ивана Залевского собирают все большее количество почитателей. Он умеет сделать каждую пару особенной и неповторимой и наиболее ярко запечатлеть торжественное событие.',
      'videos' => 
      array (
        0 => 'https://player.vimeo.com/video/154504018',
        1 => 'https://player.vimeo.com/video/83020277',
        2 => 'https://player.vimeo.com/video/124976602',
        3 => 'https://player.vimeo.com/video/74921237',
        4 => 'https://player.vimeo.com/video/105061911',
        5 => 'https://player.vimeo.com/video/73729785',
      ),
    ),
    10 => 
    array (
      'href' => 'http://top15moscow.ru/videografy/artem-korchagin/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/16.07.10-top15photographers-308.jpg',
      'name' => 'Артем',
      'surname' => 'Корчагин',
      'alias' => 'artem-korchagin',
      'phone' => '
              +7 920 020-03-52',
      'meta_description' => ' Видеограф Артем Корчагин — видеограф года по версии EEVA и Wedlife в 2014 ',
      'email' => 'www.artemkorchagin.com',
      'profession' => 'Видеограф',
      'description' => '
              <p>Артем Корчагин начинал свою карьеру на музыкальном поприще и был самым рьяным рок-туссовщиком в Нижнем Новгороде.</p>
              
              <div class=""> <blockquote><p>Умение профессионально подбирать к своим роликам самые подходящие трогательные саундтреки способствует еще большему эмоциональному восприятию его работ и доводит зрителей до состояния мурашек по коже.</p></blockquote>
<p>В настоящее время он талантливейший видеограф красивых семейных торжеств, неоднократно признанный коллегами лучшим не только в России, но и в Европе.</p>
<p>За свои талантливые шедевры, Артем одержал победу в журнале Wedding Awards 2013 и признан лучшим видеографом года по версии EEVA и Wedlife в 2014. Завоевать всеобщее признание, видеографу помогло и его музыкальное прошлое.</p>
<p>Друзья и коллеги считают, что добиться успеха Артему помогли талант и любовь. Главным цензором его творчества является любимая жена Юля. Сам видеограф скромно утверждает, что главный его секрет — это перфекционизм. И кофе. Много кофе.</p>
 </div>
            ',
      'short_about' => 'С каждым годом увеличивается число благодарных поклонников Артема Корчагина, который мастерски может отобразить самый светлый праздник создания новой семьи и передать бушующие эмоции торжественного события в своей работе.',
      'videos' => 
      array (
        0 => 'https://player.vimeo.com/video/140853504',
        1 => 'https://player.vimeo.com/video/144100793',
        2 => 'https://player.vimeo.com/video/122426276',
        3 => 'https://player.vimeo.com/video/145320004',
        4 => 'https://player.vimeo.com/video/139161978',
      ),
    ),
    11 => 
    array (
      'href' => 'http://top15moscow.ru/videografy/sergej-andreev/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/160711-top15photographers-288.jpg',
      'name' => 'Сергей',
      'surname' => 'Андреев',
      'alias' => 'sergej-andreev',
      'phone' => '
              +7 903 695-43-11',
      'meta_description' => ' Видеограф Сергей Андреев — победитель WeddingFairyTale 2015 и финалист Wedding Awards 2015 ',
      'email' => 'www.andreevmedia.com',
      'profession' => 'Видеограф',
      'description' => '
              <p>Свадебный видеооператор Сергей Андреев прославился на своем поприще не только в кругу российских поклонников, но и в других европейских государствах. Обмен опытом с такими же талантливыми операторами за границей отточило мастерство и профессионализм видеографа.</p>
              
              <div class=""> <blockquote><p>Сергей считает, что в его профессии важно быть незаметным, но в тоже время быть везде и всюду!</p></blockquote>
<p>За свои прекрасные работы Андреев стал победителем международного конкурса WeddingFairyTale 2015 и финалистом Wedding Awards 2015.</p>
<p>Талантливого видеографа заметили и на телевидении и приглашали на съемки передачи «Все о самых горячих свадьбах 2016 год» канала Россия 1.</p>
<p>Являясь руководителем собственной студии Аndreevmedia Broskostudio, Сергей постоянно перемещается по миру, завоевывая все новые просторы. Видеограф со своей командой появляется в самых неожиданных местах земного шара для съемки интересных проектов, чтобы порадовать своих почитателей свежими работами.</p>
 </div>
            ',
      'short_about' => 'Сергей Андреев стал видеооператором на свадьбах таких звездных пар, как  телеведущей Ксении Бородиной и Курбана Омарова, а также звезды сериала “Универ” Анны Хилькевич и ее избранника Артура Волкова.',
      'videos' => 
      array (
        0 => 'https://player.vimeo.com/video/190983936',
        1 => 'https://player.vimeo.com/video/179892863',
        2 => 'https://player.vimeo.com/video/179892860',
        3 => 'https://player.vimeo.com/video/179892854',
      ),
    ),
    12 => 
    array (
      'href' => 'http://top15moscow.ru/videografy/maksim-molchanov/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/022-2.jpg',
      'name' => 'Максим',
      'surname' => 'Молчанов',
      'alias' => 'maksim-molchanov',
      'phone' => '
              +7 916 366-24-55',
      'meta_description' => ' Видеограф Максим Молчанов — влюблённый в свою профессию ',
      'email' => 'www.maximmolchanov.com',
      'profession' => 'Видеограф',
      'description' => '
              <p>Известный видеограф Максим Молчанов многие годы работает с лучшими свадебными агентствами Москвы. Его свадьбы из года в год публикуются в ведущих печатных и Интернет изданиях, являясь образцами свадебных трендов в России.</p>
              
              <div class=""> <blockquote><p>Наши глаза — это единственный критерий качества. Хороший фильм пересказать нельзя, его нужно просто смотреть и пересматривать.</p></blockquote>
<p>Максим, несомненно, имеет свой собственный, узнаваемый стиль. Его работы пронизаны вечными ценностями, классикой (во всей полноте этого слова), искренностью и нежностью — тем, что никогда не выйдет из моды. </p>
<p>Колоссальный опыт работы во множестве стран, сотни свадебных фильмов и запоминающихся клипов говорят о настоящей любви Максима к своему делу. </p>
 </div>
            ',
      'short_about' => 'Он мало любит говорить о себе и своих достижениях, считая что только сама работа может характеризовать автора. ',
      'videos' => 
      array (
        0 => 'https://player.vimeo.com/video/179914619',
        1 => 'https://player.vimeo.com/video/179741223',
        2 => 'https://player.vimeo.com/video/179741228',
      ),
    ),
    13 => 
    array (
      'href' => 'http://top15moscow.ru/videografy/mihail-levchuk/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/160711-top15photographers-1122.jpg',
      'name' => 'Михаил',
      'surname' => 'Левчук',
      'alias' => 'mihail-levchuk',
      'phone' => '
              +7 926 292-85-35',
      'meta_description' => ' Видеограф Михаил Левчук — лучший свадебный видеограф по версии Wedding Awards 2015 ',
      'email' => 'www.mlevchuk.ru',
      'profession' => 'Видеограф',
      'description' => '
              <p>Михаил Левчук — развивающийся режиссер индустрии музыкальных клипов, «Лучший свадебный видеограф» Wedding Awards 2015, Финалист Wedding Awards 2014 в номинации «Лучшее свадебное видео». Топ Veter Magazine, Wedding Magazine и ведущих свадебных порталов России и мира.</p>
              
              <div class=""> <blockquote><p>Задача Михаила: быть частью праздника, быть незаметными, а после удивить шикарным результатом. Он «родственник» с видеокамерой, которого невозможно стесняться, — такова его цель.</p></blockquote>
<p>Говоря о большом кино, Михаил Левчук является фанатом «рваной», «живой» камеры: кадры, снятые с рук, подглядывание, и таким образом, полное погружение зрителя в событие.</p>
<p>При этом Михаил не может отвести глаз от классической киношной плавности, длинных проездов, захватывающих дух. Стиль Михаила Левчука — это объединение этих подходов.</p>
<p>Команда Михаила Левчука — лучшие специалисты свадебной и музыкальной индустрии.</p>
 </div>
            ',
      'short_about' => 'Михаил превращает зрителя в человека, который «находится» рядом с главными действующими героями, чувствует и даже сам испытывает их эмоции.',
      'videos' => 
      array (
        0 => 'https://player.vimeo.com/video/142628707',
        1 => 'https://player.vimeo.com/video/143412041',
        2 => 'https://player.vimeo.com/video/149399264',
        3 => 'https://player.vimeo.com/video/147131334',
        4 => 'https://player.vimeo.com/video/159462626',
      ),
    ),
    14 => 
    array (
      'href' => 'http://top15moscow.ru/videografy/artisland/',
      'img' => 'http://top15moscow.ru/wp-content/uploads/2016/07/160711-top15photographers-1466.jpg',
      'name' => 'Студия',
      'surname' => '«Art',
      'alias' => 'artisland',
      'phone' => '
              +7 495 776-72-73',
      'meta_description' => ' Art Island — Свадебное видео Премиум класса ',
      'email' => 'www.artisland.com',
      'profession' => 'Видеографы',
      'description' => '
              <p>Ролики, которые снимают видеографы Art Island – больше, чем просто видео. На протяжении многих лет студия считается лучшей в России и странах СНГ.</p>
              
              <div class=""> <blockquote><p>Видеографы работают как в России так и за рубежом: в Италии, Таиланде, ОАЭ, Малайзии, Индонезии и других странах.</p></blockquote>
<p>Art Island — профессиональная студия, занимающаяся съёмкой свадебных видеороликов премиум класса. Основана Артуром Инамовым и Андреем Меликовым.</p>
<p>Помимо съёмок свадеб, команда также занимается проведением семинаров и мастер-классов, в которых учит новичков и будущих видеографов основам профессии. Art Island также снимает семейные пары, «Love story» и коммерческие клипы.</p>
 </div>
            ',
      'short_about' => 'КогдаArt Island посредством видео рассказывают чьи-либо истории, они наделяют её любовью и жизнью, именно поэтому ваше событие останется с вами навсегда и войдет в историю.',
      'videos' => 
      array (
        0 => 'https://player.vimeo.com/video/174092306',
        1 => 'https://player.vimeo.com/video/148921764',
        2 => 'https://player.vimeo.com/video/126677151',
        3 => 'https://player.vimeo.com/video/125457850',
        4 => 'https://player.vimeo.com/video/170939608',
        5 => 'https://player.vimeo.com/video/29784965',
        6 => 'https://player.vimeo.com/video/193106509',
        7 => 'https://player.vimeo.com/video/44723248',
        8 => 'https://player.vimeo.com/video/107827576',
        9 => 'https://player.vimeo.com/video/40065562',
        10 => 'https://player.vimeo.com/video/66410476',
        11 => 'https://player.vimeo.com/video/135140021',
      ),
    ),
  ),
);