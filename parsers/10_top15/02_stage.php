<?php
/*
 * Get: Items Links
 */

define('__HOMEDIR__', __DIR__);
require_once __DIR__ . '/../core.php';
require __HOMEDIR__ . '/phpQuery-onefile.php';

$subscribers_professions = require __DIR__ . '/results/01_subscribers.php';
$Items = [];
foreach ($subscribers_professions as $profession_key => $subscribers_profession)
{
    foreach ($subscribers_profession as $subscriber_key => $subscriber)
    {
        print_r($subscriber['href']);
        echo '<br>';

        $hbr = file_get_contents($subscriber['href']);
        $Dom = phpQuery::newDocument($hbr);

//        $Dom->loadFromURL($subscriber['href']);

        $subscribers_professions[$profession_key][$subscriber_key] = $subscriber;

        print_r($Dom->find('.all-person-bt-phone')->text());
        echo '<br>';


        $subscribers_professions[$profession_key][$subscriber_key]['phone'] = $Dom->find('.social-single-phone')->text();
        $subscribers_professions[$profession_key][$subscriber_key]['meta_description'] = $Dom->find('.person-text-header .single-paralax-header')->text();
        $subscribers_professions[$profession_key][$subscriber_key]['email'] = $Dom->find('.social-single-website')->text();
        $subscribers_professions[$profession_key][$subscriber_key]['profession'] = $Dom->find('.person-position')->text();


        $Dom->find('.single-text-wrap a')->remove();
        $Dom->find('.single-text-wrap div')->attr('class', '');

        $subscribers_professions[$profession_key][$subscriber_key]['description'] = $Dom->find('.single-text-wrap')->html();
        $subscribers_professions[$profession_key][$subscriber_key]['short_about'] = $Dom->find('.single-text-sidebar p')->html();

        //get photo gallery
        foreach ($Dom->find('.photo-gallery a') as $photo)
        {
            $photo = pq($photo);
            $photo = explode('/', $photo->find('img')->attr('src'));

            //remove domain from link
            unset($photo[0]);
            unset($photo[1]);
            unset($photo[2]);

            $photo = '/' . implode('/', $photo);

            Save_File($photo, 'http://top15moscow.ru');
            $subscribers_professions[$profession_key][$subscriber_key]['photos'][] = $photo;
        }

        //get photo gallery
        foreach ($Dom->find('.video-slider-item') as $video)
        {
            $video = pq($video);
            $subscribers_professions[$profession_key][$subscriber_key]['videos'][] = $video->find('iframe')->attr('src');
        }

    }
}


file_put_contents(__DIR__ . '/results/02_subscribers.php', "<?php \n    return " . var_export($subscribers_professions, true) . ";");

