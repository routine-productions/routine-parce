<?php
/*
 * Get:  links and Titles of Category
 */

define('__HOMEDIR__', __DIR__);

require_once __DIR__ . '/../core.php';

$Links = [];

// Get: All pages
$Dom->loadFromUrl('http://ekoton.com/');

// Get: All links
foreach ($Dom->find('.menu-item-megamenu-parent .megamenu-child-container ul  li > a') as $Link)
{
    $Links[] = $Link->href;
    $Titles[] = $Link->text;
}

// Unset void elements from links and Titles
foreach ($Links as $i => $v)
{
    if ($v == '') unset($Links[$i]);
}
foreach ($Titles as $i => $v)
{
    if ($v == '+') unset($Titles[$i]);
}
$Titles1 = array_values($Titles);
$Titles = [];

for ($i = 0; $i < 11; $i++)
{
    $Titles[] = $Titles1[$i];
}
$Links1 = array_values($Links);
$Links = [];
for ($i = 0; $i < 11; $i++)
{
    $Links[] = $Links1[$i];
}
$ids = [];

foreach ($Titles as $title)
{
    $Category = new Category();
    $Category->name = $title;
    $Category->slug = Create_Slug($title);
    $Category->save();
    $ids[] = $Category->id;

    $Relation = new Taxonomy();
    $Relation->term_taxonomy_id = $Category->id;
    $Relation->term_id = $Category->id;
    $Relation->taxonomy = 'category';
    $Relation->parent = 5;
    $Relation->count = 0;
    $Relation->save();
}

$i = 0;
$Link = [];
foreach ($Links as $link)
{
    $Link[$ids[$i]] = $link;
    $i++;
}
print_r($Link);
// Write: To file
file_put_contents(__DIR__ . '/results/01_links.php', "<?php \n    return " . var_export($Link, true) . ";");