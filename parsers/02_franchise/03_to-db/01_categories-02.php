<?php
/*
 * Create: Categories 01
 */
define('__HOMEDIR__', __DIR__);
require_once __DIR__ . '/../../core.php';

$Model = new Modx_Site_Content();

$Categories = require_once __DIR__ . '/../02_franchise-02/results/01_categories.php';

foreach ($Categories as $Category_Key => $Category)
{
    $Model = new Modx_Site_Content();

    $Model->id = 2000 + $Category_Key;
    $Model->parent = 2;
    $Model->pagetitle = $Category['title'];
    $Model->alias = Create_Slug($Category['title']);
    $Model->published = 1;
    $Model->isfolder = 1;
    $Model->template = 3;
    $Model->menuindex = 3;
    $Model->uri = Create_Slug($Category['title']) . '/';
    $Model->save();
}