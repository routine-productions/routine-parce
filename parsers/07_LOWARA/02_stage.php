<?php
/*
 * Get: Items Links
 */

define('__HOMEDIR__', __DIR__);
require_once __DIR__ . '/../core.php';
$Items = require __DIR__ . '/results/01_links.php';
//
//print_r($Items);
//exit;
foreach ($Items as $keyy => $Category_link)
{

    foreach ($Category_link['items'] as $keys => $Item_link)
    {
//            if (isset($Item_link['href']))
//            {
        $Dom->loadFromURL($Item_link['href']);
        /*
         *Item content
         */
        $Header = $Dom->find('.productdata h2')->text;

        if (isset($Dom->find('#tabs-0 h3 a')->text))
        {
            $Content = $Dom->find('#tabs-0 h3 a')->text;
        } else
        {
            $Content = '';
            $Characteristics = '';
            foreach ($Dom->find('#tabs-0 *') as $k => $text)
            {

                if (isset($text->find('img')->src))
                {
                    $img = str_replace('http://xylem.ru', '', $Images->find('img')->src);
                    $Image[]['src'] = '/wp-content/uploads/2016/09' . $img;
                    Save_File($img, 'http://xylem.ru');
                }
                $Content .= $text->outerHtml;
            }

            print_r('<br>' . $Header . '</br>');

        }
        foreach ($Dom->find('#tabs-1 ul li') as $pdf)
        {
            $href = $pdf->find('a')->href;
            $href = str_replace('http://documentlibrary.xylemappliedwater.com', '', $href);
            Save_File($href, 'http://xylem.ru');
        }

        foreach ($Dom->find('#tabs-1 ul li a') as $href)
        {
            $href->setAttribute('href', str_replace('http://documentlibrary.xylemappliedwater.com', '', $href->href));
        }
        $Description = $Dom->find('#tabs-1')->outerHtml;

        /*
        *Item images
        */

        $Image = [];
        foreach ($Dom->find('.productfeatureimage') as $Images)
        {
            $img = str_replace('http://xylem.ru', '', $Images->find('img')->src);
            $Image[]['src'] = '/wp-content/uploads/2016/09' . $img;
            Save_File($img, 'http://xylem.ru');
        }

        $Items[$keyy]['items'][$keys] = [
            'images' => $Image,
            'Header' => $Header,
            'Content' => $Content,
            'description' => $Description,
            'href' => $Item_link['href']
        ];

    }
}
//print_r($Items);
file_put_contents(__DIR__ . '/results/Items_01.php', "<?php \n    return " . var_export($Items, true) . ";");

