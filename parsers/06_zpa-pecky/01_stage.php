<?php
/*
 * Get:  links and Titles of Category
 */

define('__HOMEDIR__', __DIR__);

require_once __DIR__ . '/../core.php';
$Links = require_once __DIR__ . '/results/01_links.php';

foreach ($Links as $key => $link)
{
    foreach ($link as $k => $item)
    {
        $Dom->loadFromUrl('http://www.zpa-pecky.ru/' . $item['href']);
        /*
         *
         * Characteristics
         *
         *
         * */
        $upper_content = $Dom->find('td center table', 3)->find('tbody tr td', 3);
        $Characteristic = $upper_content->find('tbody');
        foreach ($Characteristic->find('img') as $img)
        {
            Save_File($img->src, 'http://www.zpa-pecky.ru/');
            $img->setAttribute('src', '/wp-content/uploads/2016/09' . $img->src);
        }

        /*
         *
         *
         * Description
         *
         *
         * */

        $Content = '';
        $midle_content = $Dom->find('td center table', 3)->find('tbody tr td', 0);
        $Content = $midle_content->find('table tbody tr td', 2)->find('table tbody tr', 2);
        $Description = '';
        foreach ($Content->find('p') as $i => $something)
        {
            if ($i > 2)
            {
                Save_File($something->find('a')->href, 'http://www.zpa-pecky.ru/');
                $something->find('a')->setAttribute('href', '/wp-content/uploads/2016/09/files' . $something->find('a')->href);
                $Description .= $something;
            }

        }
        /*
         *
         *
         *
         * Save Images
         *
         *
         * */

        $Image = $Dom->find('td center table', 3)->find('tbody tr td', 0)->find('img');
        Save_File($Image->src, 'http://www.zpa-pecky.ru/');
        $Image->setAttribute('src', '/wp-content/uploads/2016/09' . $Image->src);
        $Image = $Image->src;

        /*
         *
         *
         * Main content
         *
         *
         * */
        $Content = $Dom->find('td center table', 3)->find('tbody tr td', 3)->find('p', 0);

        if (!empty($Content->find('img')->src))
        {
            Save_File($Content->find('img')->src, 'http://www.zpa-pecky.ru/');
            $Content->find('img')->setAttribute('src', '/wp-content/uploads/2016/09' . $Content->find('img')->src);
        }

        $Main_Content = '';
        foreach ($Content->find('*') as $king_key => $cont)
        {
            if ($king_key > 3)
            {
                $Main_Content .= iconv("windows-1251", "UTF-8", $cont);

            }
        }

        /*
         *
         *
         *Convert to UTF-8
         *
         *
         * */

        $Characteristic = iconv("windows-1251", "UTF-8", $Characteristic);
        $Description = iconv("windows-1251", "UTF-8", $Description);
        $Short_description = $Main_Content;
        $Content = $Main_Content;

        /*
         *
         *
         *Write to file
         *
         *
         * */

        $Links[$key][$k] = [
            'Characteristics' => $Characteristic,
            'title' => $item['title'],
            'Description' => $Description,
            'Content' => $Content,
            'Short_description' => $Short_description,
            'href' => $item['href'],
            'Image' => $Image
        ];

    }
}
file_put_contents(__DIR__ . '/results/Items_01.php', "<?php \n    return " . var_export($Links, true) . ";");