<?php
/*
 * Get: Items Links
 */

define('__HOMEDIR__', __DIR__);
require_once __DIR__ . '/../core.php';
$Links = require __DIR__ . '/results/01_links.php';

foreach ($Links as $keys => $link)
{
    foreach ($link['parent_href']['link'] as $k => $links)
    {
        $Items = [];
        $Dom->loadFromURL('http://www.analitpribor-smolensk.ru/' . $links['href']);

        foreach ($Dom->find('.object .textblk p a') as $link1)
        {
            $Items[]['href'] = $link1->href;
        }

        $Links[$keys]['parent_href']['link'][$k]['items'] = $Items;
        foreach ($Dom->find('.object .textblk') as $key => $desc)
        {
            $Descriptions = $desc->find('p', 1)->text;
            $Links[$keys]['parent_href']['link'][$k]['items'][$key]['description'] = $Descriptions;
        }
    }
//    print_r($Links);
}
file_put_contents(__DIR__ . '/results/Items.php', "<?php \n    return " . var_export($Links, true) . ";");