-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Июл 03 2016 г., 12:06
-- Версия сервера: 10.1.13-MariaDB
-- Версия PHP: 7.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `martialholding_franchise`
--

-- --------------------------------------------------------

--
-- Структура таблицы `modx_access_actiondom`
--

CREATE TABLE `modx_access_actiondom` (
  `id` int(10) UNSIGNED NOT NULL,
  `target` varchar(100) NOT NULL DEFAULT '',
  `principal_class` varchar(100) NOT NULL DEFAULT 'modPrincipal',
  `principal` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `authority` int(10) UNSIGNED NOT NULL DEFAULT '9999',
  `policy` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `modx_access_actions`
--

CREATE TABLE `modx_access_actions` (
  `id` int(10) UNSIGNED NOT NULL,
  `target` varchar(100) NOT NULL DEFAULT '',
  `principal_class` varchar(100) NOT NULL DEFAULT 'modPrincipal',
  `principal` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `authority` int(10) UNSIGNED NOT NULL DEFAULT '9999',
  `policy` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `modx_access_category`
--

CREATE TABLE `modx_access_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `target` varchar(100) NOT NULL DEFAULT '',
  `principal_class` varchar(100) NOT NULL DEFAULT 'modPrincipal',
  `principal` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `authority` int(10) UNSIGNED NOT NULL DEFAULT '9999',
  `policy` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `context_key` varchar(100) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `modx_access_context`
--

CREATE TABLE `modx_access_context` (
  `id` int(10) UNSIGNED NOT NULL,
  `target` varchar(100) NOT NULL DEFAULT '',
  `principal_class` varchar(100) NOT NULL DEFAULT 'modPrincipal',
  `principal` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `authority` int(10) UNSIGNED NOT NULL DEFAULT '9999',
  `policy` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_access_context`
--

INSERT INTO `modx_access_context` (`id`, `target`, `principal_class`, `principal`, `authority`, `policy`) VALUES
(1, 'web', 'modUserGroup', 0, 9999, 3),
(2, 'mgr', 'modUserGroup', 1, 0, 2),
(3, 'web', 'modUserGroup', 1, 0, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `modx_access_elements`
--

CREATE TABLE `modx_access_elements` (
  `id` int(10) UNSIGNED NOT NULL,
  `target` varchar(100) NOT NULL DEFAULT '',
  `principal_class` varchar(100) NOT NULL DEFAULT 'modPrincipal',
  `principal` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `authority` int(10) UNSIGNED NOT NULL DEFAULT '9999',
  `policy` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `context_key` varchar(100) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `modx_access_media_source`
--

CREATE TABLE `modx_access_media_source` (
  `id` int(10) UNSIGNED NOT NULL,
  `target` varchar(100) NOT NULL DEFAULT '',
  `principal_class` varchar(100) NOT NULL DEFAULT 'modPrincipal',
  `principal` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `authority` int(10) UNSIGNED NOT NULL DEFAULT '9999',
  `policy` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `context_key` varchar(100) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `modx_access_menus`
--

CREATE TABLE `modx_access_menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `target` varchar(100) NOT NULL DEFAULT '',
  `principal_class` varchar(100) NOT NULL DEFAULT 'modPrincipal',
  `principal` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `authority` int(10) UNSIGNED NOT NULL DEFAULT '9999',
  `policy` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `modx_access_namespace`
--

CREATE TABLE `modx_access_namespace` (
  `id` int(10) UNSIGNED NOT NULL,
  `target` varchar(100) NOT NULL DEFAULT '',
  `principal_class` varchar(100) NOT NULL DEFAULT 'modPrincipal',
  `principal` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `authority` int(10) UNSIGNED NOT NULL DEFAULT '9999',
  `policy` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `context_key` varchar(100) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `modx_access_permissions`
--

CREATE TABLE `modx_access_permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `template` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `value` tinyint(1) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_access_permissions`
--

INSERT INTO `modx_access_permissions` (`id`, `template`, `name`, `description`, `value`) VALUES
(1, 1, 'about', 'perm.about_desc', 1),
(2, 1, 'access_permissions', 'perm.access_permissions_desc', 1),
(3, 1, 'actions', 'perm.actions_desc', 1),
(4, 1, 'change_password', 'perm.change_password_desc', 1),
(5, 1, 'change_profile', 'perm.change_profile_desc', 1),
(6, 1, 'charsets', 'perm.charsets_desc', 1),
(7, 1, 'class_map', 'perm.class_map_desc', 1),
(8, 1, 'components', 'perm.components_desc', 1),
(9, 1, 'content_types', 'perm.content_types_desc', 1),
(10, 1, 'countries', 'perm.countries_desc', 1),
(11, 1, 'create', 'perm.create_desc', 1),
(12, 1, 'credits', 'perm.credits_desc', 1),
(13, 1, 'customize_forms', 'perm.customize_forms_desc', 1),
(14, 1, 'dashboards', 'perm.dashboards_desc', 1),
(15, 1, 'database', 'perm.database_desc', 1),
(16, 1, 'database_truncate', 'perm.database_truncate_desc', 1),
(17, 1, 'delete_category', 'perm.delete_category_desc', 1),
(18, 1, 'delete_chunk', 'perm.delete_chunk_desc', 1),
(19, 1, 'delete_context', 'perm.delete_context_desc', 1),
(20, 1, 'delete_document', 'perm.delete_document_desc', 1),
(21, 1, 'delete_eventlog', 'perm.delete_eventlog_desc', 1),
(22, 1, 'delete_plugin', 'perm.delete_plugin_desc', 1),
(23, 1, 'delete_propertyset', 'perm.delete_propertyset_desc', 1),
(24, 1, 'delete_snippet', 'perm.delete_snippet_desc', 1),
(25, 1, 'delete_template', 'perm.delete_template_desc', 1),
(26, 1, 'delete_tv', 'perm.delete_tv_desc', 1),
(27, 1, 'delete_role', 'perm.delete_role_desc', 1),
(28, 1, 'delete_user', 'perm.delete_user_desc', 1),
(29, 1, 'directory_chmod', 'perm.directory_chmod_desc', 1),
(30, 1, 'directory_create', 'perm.directory_create_desc', 1),
(31, 1, 'directory_list', 'perm.directory_list_desc', 1),
(32, 1, 'directory_remove', 'perm.directory_remove_desc', 1),
(33, 1, 'directory_update', 'perm.directory_update_desc', 1),
(34, 1, 'edit_category', 'perm.edit_category_desc', 1),
(35, 1, 'edit_chunk', 'perm.edit_chunk_desc', 1),
(36, 1, 'edit_context', 'perm.edit_context_desc', 1),
(37, 1, 'edit_document', 'perm.edit_document_desc', 1),
(38, 1, 'edit_locked', 'perm.edit_locked_desc', 1),
(39, 1, 'edit_plugin', 'perm.edit_plugin_desc', 1),
(40, 1, 'edit_propertyset', 'perm.edit_propertyset_desc', 1),
(41, 1, 'edit_role', 'perm.edit_role_desc', 1),
(42, 1, 'edit_snippet', 'perm.edit_snippet_desc', 1),
(43, 1, 'edit_template', 'perm.edit_template_desc', 1),
(44, 1, 'edit_tv', 'perm.edit_tv_desc', 1),
(45, 1, 'edit_user', 'perm.edit_user_desc', 1),
(46, 1, 'element_tree', 'perm.element_tree_desc', 1),
(47, 1, 'empty_cache', 'perm.empty_cache_desc', 1),
(48, 1, 'error_log_erase', 'perm.error_log_erase_desc', 1),
(49, 1, 'error_log_view', 'perm.error_log_view_desc', 1),
(50, 1, 'export_static', 'perm.export_static_desc', 1),
(51, 1, 'file_create', 'perm.file_create_desc', 1),
(52, 1, 'file_list', 'perm.file_list_desc', 1),
(53, 1, 'file_manager', 'perm.file_manager_desc', 1),
(54, 1, 'file_remove', 'perm.file_remove_desc', 1),
(55, 1, 'file_tree', 'perm.file_tree_desc', 1),
(56, 1, 'file_update', 'perm.file_update_desc', 1),
(57, 1, 'file_upload', 'perm.file_upload_desc', 1),
(58, 1, 'file_unpack', 'perm.file_unpack_desc', 1),
(59, 1, 'file_view', 'perm.file_view_desc', 1),
(60, 1, 'flush_sessions', 'perm.flush_sessions_desc', 1),
(61, 1, 'frames', 'perm.frames_desc', 1),
(62, 1, 'help', 'perm.help_desc', 1),
(63, 1, 'home', 'perm.home_desc', 1),
(64, 1, 'import_static', 'perm.import_static_desc', 1),
(65, 1, 'languages', 'perm.languages_desc', 1),
(66, 1, 'lexicons', 'perm.lexicons_desc', 1),
(67, 1, 'list', 'perm.list_desc', 1),
(68, 1, 'load', 'perm.load_desc', 1),
(69, 1, 'logout', 'perm.logout_desc', 1),
(70, 1, 'logs', 'perm.logs_desc', 1),
(71, 1, 'menu_reports', 'perm.menu_reports_desc', 1),
(72, 1, 'menu_security', 'perm.menu_security_desc', 1),
(73, 1, 'menu_site', 'perm.menu_site_desc', 1),
(74, 1, 'menu_support', 'perm.menu_support_desc', 1),
(75, 1, 'menu_system', 'perm.menu_system_desc', 1),
(76, 1, 'menu_tools', 'perm.menu_tools_desc', 1),
(77, 1, 'menu_user', 'perm.menu_user_desc', 1),
(78, 1, 'menus', 'perm.menus_desc', 1),
(79, 1, 'messages', 'perm.messages_desc', 1),
(80, 1, 'namespaces', 'perm.namespaces_desc', 1),
(81, 1, 'new_category', 'perm.new_category_desc', 1),
(82, 1, 'new_chunk', 'perm.new_chunk_desc', 1),
(83, 1, 'new_context', 'perm.new_context_desc', 1),
(84, 1, 'new_document', 'perm.new_document_desc', 1),
(85, 1, 'new_static_resource', 'perm.new_static_resource_desc', 1),
(86, 1, 'new_symlink', 'perm.new_symlink_desc', 1),
(87, 1, 'new_weblink', 'perm.new_weblink_desc', 1),
(88, 1, 'new_document_in_root', 'perm.new_document_in_root_desc', 1),
(89, 1, 'new_plugin', 'perm.new_plugin_desc', 1),
(90, 1, 'new_propertyset', 'perm.new_propertyset_desc', 1),
(91, 1, 'new_role', 'perm.new_role_desc', 1),
(92, 1, 'new_snippet', 'perm.new_snippet_desc', 1),
(93, 1, 'new_template', 'perm.new_template_desc', 1),
(94, 1, 'new_tv', 'perm.new_tv_desc', 1),
(95, 1, 'new_user', 'perm.new_user_desc', 1),
(96, 1, 'packages', 'perm.packages_desc', 1),
(97, 1, 'policy_delete', 'perm.policy_delete_desc', 1),
(98, 1, 'policy_edit', 'perm.policy_edit_desc', 1),
(99, 1, 'policy_new', 'perm.policy_new_desc', 1),
(100, 1, 'policy_save', 'perm.policy_save_desc', 1),
(101, 1, 'policy_view', 'perm.policy_view_desc', 1),
(102, 1, 'policy_template_delete', 'perm.policy_template_delete_desc', 1),
(103, 1, 'policy_template_edit', 'perm.policy_template_edit_desc', 1),
(104, 1, 'policy_template_new', 'perm.policy_template_new_desc', 1),
(105, 1, 'policy_template_save', 'perm.policy_template_save_desc', 1),
(106, 1, 'policy_template_view', 'perm.policy_template_view_desc', 1),
(107, 1, 'property_sets', 'perm.property_sets_desc', 1),
(108, 1, 'providers', 'perm.providers_desc', 1),
(109, 1, 'publish_document', 'perm.publish_document_desc', 1),
(110, 1, 'purge_deleted', 'perm.purge_deleted_desc', 1),
(111, 1, 'remove', 'perm.remove_desc', 1),
(112, 1, 'remove_locks', 'perm.remove_locks_desc', 1),
(113, 1, 'resource_duplicate', 'perm.resource_duplicate_desc', 1),
(114, 1, 'resourcegroup_delete', 'perm.resourcegroup_delete_desc', 1),
(115, 1, 'resourcegroup_edit', 'perm.resourcegroup_edit_desc', 1),
(116, 1, 'resourcegroup_new', 'perm.resourcegroup_new_desc', 1),
(117, 1, 'resourcegroup_resource_edit', 'perm.resourcegroup_resource_edit_desc', 1),
(118, 1, 'resourcegroup_resource_list', 'perm.resourcegroup_resource_list_desc', 1),
(119, 1, 'resourcegroup_save', 'perm.resourcegroup_save_desc', 1),
(120, 1, 'resourcegroup_view', 'perm.resourcegroup_view_desc', 1),
(121, 1, 'resource_quick_create', 'perm.resource_quick_create_desc', 1),
(122, 1, 'resource_quick_update', 'perm.resource_quick_update_desc', 1),
(123, 1, 'resource_tree', 'perm.resource_tree_desc', 1),
(124, 1, 'save', 'perm.save_desc', 1),
(125, 1, 'save_category', 'perm.save_category_desc', 1),
(126, 1, 'save_chunk', 'perm.save_chunk_desc', 1),
(127, 1, 'save_context', 'perm.save_context_desc', 1),
(128, 1, 'save_document', 'perm.save_document_desc', 1),
(129, 1, 'save_plugin', 'perm.save_plugin_desc', 1),
(130, 1, 'save_propertyset', 'perm.save_propertyset_desc', 1),
(131, 1, 'save_role', 'perm.save_role_desc', 1),
(132, 1, 'save_snippet', 'perm.save_snippet_desc', 1),
(133, 1, 'save_template', 'perm.save_template_desc', 1),
(134, 1, 'save_tv', 'perm.save_tv_desc', 1),
(135, 1, 'save_user', 'perm.save_user_desc', 1),
(136, 1, 'search', 'perm.search_desc', 1),
(137, 1, 'settings', 'perm.settings_desc', 1),
(138, 1, 'events', 'perm.events_desc', 1),
(139, 1, 'source_save', 'perm.source_save_desc', 1),
(140, 1, 'source_delete', 'perm.source_delete_desc', 1),
(141, 1, 'source_edit', 'perm.source_edit_desc', 1),
(142, 1, 'source_view', 'perm.source_view_desc', 1),
(143, 1, 'sources', 'perm.sources_desc', 1),
(144, 1, 'steal_locks', 'perm.steal_locks_desc', 1),
(145, 1, 'tree_show_element_ids', 'perm.tree_show_element_ids_desc', 1),
(146, 1, 'tree_show_resource_ids', 'perm.tree_show_resource_ids_desc', 1),
(147, 1, 'undelete_document', 'perm.undelete_document_desc', 1),
(148, 1, 'unpublish_document', 'perm.unpublish_document_desc', 1),
(149, 1, 'unlock_element_properties', 'perm.unlock_element_properties_desc', 1),
(150, 1, 'usergroup_delete', 'perm.usergroup_delete_desc', 1),
(151, 1, 'usergroup_edit', 'perm.usergroup_edit_desc', 1),
(152, 1, 'usergroup_new', 'perm.usergroup_new_desc', 1),
(153, 1, 'usergroup_save', 'perm.usergroup_save_desc', 1),
(154, 1, 'usergroup_user_edit', 'perm.usergroup_user_edit_desc', 1),
(155, 1, 'usergroup_user_list', 'perm.usergroup_user_list_desc', 1),
(156, 1, 'usergroup_view', 'perm.usergroup_view_desc', 1),
(157, 1, 'view', 'perm.view_desc', 1),
(158, 1, 'view_category', 'perm.view_category_desc', 1),
(159, 1, 'view_chunk', 'perm.view_chunk_desc', 1),
(160, 1, 'view_context', 'perm.view_context_desc', 1),
(161, 1, 'view_document', 'perm.view_document_desc', 1),
(162, 1, 'view_element', 'perm.view_element_desc', 1),
(163, 1, 'view_eventlog', 'perm.view_eventlog_desc', 1),
(164, 1, 'view_offline', 'perm.view_offline_desc', 1),
(165, 1, 'view_plugin', 'perm.view_plugin_desc', 1),
(166, 1, 'view_propertyset', 'perm.view_propertyset_desc', 1),
(167, 1, 'view_role', 'perm.view_role_desc', 1),
(168, 1, 'view_snippet', 'perm.view_snippet_desc', 1),
(169, 1, 'view_sysinfo', 'perm.view_sysinfo_desc', 1),
(170, 1, 'view_template', 'perm.view_template_desc', 1),
(171, 1, 'view_tv', 'perm.view_tv_desc', 1),
(172, 1, 'view_user', 'perm.view_user_desc', 1),
(173, 1, 'view_unpublished', 'perm.view_unpublished_desc', 1),
(174, 1, 'workspaces', 'perm.workspaces_desc', 1),
(175, 2, 'add_children', 'perm.add_children_desc', 1),
(176, 2, 'copy', 'perm.copy_desc', 1),
(177, 2, 'create', 'perm.create_desc', 1),
(178, 2, 'delete', 'perm.delete_desc', 1),
(179, 2, 'list', 'perm.list_desc', 1),
(180, 2, 'load', 'perm.load_desc', 1),
(181, 2, 'move', 'perm.move_desc', 1),
(182, 2, 'publish', 'perm.publish_desc', 1),
(183, 2, 'remove', 'perm.remove_desc', 1),
(184, 2, 'save', 'perm.save_desc', 1),
(185, 2, 'steal_lock', 'perm.steal_lock_desc', 1),
(186, 2, 'undelete', 'perm.undelete_desc', 1),
(187, 2, 'unpublish', 'perm.unpublish_desc', 1),
(188, 2, 'view', 'perm.view_desc', 1),
(189, 3, 'load', 'perm.load_desc', 1),
(190, 3, 'list', 'perm.list_desc', 1),
(191, 3, 'view', 'perm.view_desc', 1),
(192, 3, 'save', 'perm.save_desc', 1),
(193, 3, 'remove', 'perm.remove_desc', 1),
(194, 4, 'add_children', 'perm.add_children_desc', 1),
(195, 4, 'create', 'perm.create_desc', 1),
(196, 4, 'copy', 'perm.copy_desc', 1),
(197, 4, 'delete', 'perm.delete_desc', 1),
(198, 4, 'list', 'perm.list_desc', 1),
(199, 4, 'load', 'perm.load_desc', 1),
(200, 4, 'remove', 'perm.remove_desc', 1),
(201, 4, 'save', 'perm.save_desc', 1),
(202, 4, 'view', 'perm.view_desc', 1),
(203, 5, 'create', 'perm.create_desc', 1),
(204, 5, 'copy', 'perm.copy_desc', 1),
(205, 5, 'list', 'perm.list_desc', 1),
(206, 5, 'load', 'perm.load_desc', 1),
(207, 5, 'remove', 'perm.remove_desc', 1),
(208, 5, 'save', 'perm.save_desc', 1),
(209, 5, 'view', 'perm.view_desc', 1),
(210, 6, 'load', 'perm.load_desc', 1),
(211, 6, 'list', 'perm.list_desc', 1),
(212, 6, 'view', 'perm.view_desc', 1),
(213, 6, 'save', 'perm.save_desc', 1),
(214, 6, 'remove', 'perm.remove_desc', 1),
(215, 6, 'view_unpublished', 'perm.view_unpublished_desc', 1),
(216, 6, 'copy', 'perm.copy_desc', 1),
(217, 7, 'list', 'perm.list_desc', 1),
(218, 7, 'load', 'perm.load_desc', 1),
(219, 7, 'view', 'perm.view_desc', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `modx_access_policies`
--

CREATE TABLE `modx_access_policies` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` mediumtext,
  `parent` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `template` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `class` varchar(255) NOT NULL DEFAULT '',
  `data` text,
  `lexicon` varchar(255) NOT NULL DEFAULT 'permissions'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_access_policies`
--

INSERT INTO `modx_access_policies` (`id`, `name`, `description`, `parent`, `template`, `class`, `data`, `lexicon`) VALUES
(1, 'Resource', 'MODX Resource Policy with all attributes.', 0, 2, '', '{"add_children":true,"create":true,"copy":true,"delete":true,"list":true,"load":true,"move":true,"publish":true,"remove":true,"save":true,"steal_lock":true,"undelete":true,"unpublish":true,"view":true}', 'permissions'),
(2, 'Administrator', 'Context administration policy with all permissions.', 0, 1, '', '{"about":true,"access_permissions":true,"actions":true,"change_password":true,"change_profile":true,"charsets":true,"class_map":true,"components":true,"content_types":true,"countries":true,"create":true,"credits":true,"customize_forms":true,"dashboards":true,"database":true,"database_truncate":true,"delete_category":true,"delete_chunk":true,"delete_context":true,"delete_document":true,"delete_eventlog":true,"delete_plugin":true,"delete_propertyset":true,"delete_role":true,"delete_snippet":true,"delete_template":true,"delete_tv":true,"delete_user":true,"directory_chmod":true,"directory_create":true,"directory_list":true,"directory_remove":true,"directory_update":true,"edit_category":true,"edit_chunk":true,"edit_context":true,"edit_document":true,"edit_locked":true,"edit_plugin":true,"edit_propertyset":true,"edit_role":true,"edit_snippet":true,"edit_template":true,"edit_tv":true,"edit_user":true,"element_tree":true,"empty_cache":true,"error_log_erase":true,"error_log_view":true,"export_static":true,"file_create":true,"file_list":true,"file_manager":true,"file_remove":true,"file_tree":true,"file_update":true,"file_upload":true,"file_unpack":true,"file_view":true,"flush_sessions":true,"frames":true,"help":true,"home":true,"import_static":true,"languages":true,"lexicons":true,"list":true,"load":true,"logout":true,"logs":true,"menus":true,"menu_reports":true,"menu_security":true,"menu_site":true,"menu_support":true,"menu_system":true,"menu_tools":true,"menu_user":true,"messages":true,"namespaces":true,"new_category":true,"new_chunk":true,"new_context":true,"new_document":true,"new_document_in_root":true,"new_plugin":true,"new_propertyset":true,"new_role":true,"new_snippet":true,"new_static_resource":true,"new_symlink":true,"new_template":true,"new_tv":true,"new_user":true,"new_weblink":true,"packages":true,"policy_delete":true,"policy_edit":true,"policy_new":true,"policy_save":true,"policy_template_delete":true,"policy_template_edit":true,"policy_template_new":true,"policy_template_save":true,"policy_template_view":true,"policy_view":true,"property_sets":true,"providers":true,"publish_document":true,"purge_deleted":true,"remove":true,"remove_locks":true,"resource_duplicate":true,"resourcegroup_delete":true,"resourcegroup_edit":true,"resourcegroup_new":true,"resourcegroup_resource_edit":true,"resourcegroup_resource_list":true,"resourcegroup_save":true,"resourcegroup_view":true,"resource_quick_create":true,"resource_quick_update":true,"resource_tree":true,"save":true,"save_category":true,"save_chunk":true,"save_context":true,"save_document":true,"save_plugin":true,"save_propertyset":true,"save_role":true,"save_snippet":true,"save_template":true,"save_tv":true,"save_user":true,"search":true,"settings":true,"sources":true,"source_delete":true,"source_edit":true,"source_save":true,"source_view":true,"steal_locks":true,"tree_show_element_ids":true,"tree_show_resource_ids":true,"undelete_document":true,"unlock_element_properties":true,"unpublish_document":true,"usergroup_delete":true,"usergroup_edit":true,"usergroup_new":true,"usergroup_save":true,"usergroup_user_edit":true,"usergroup_user_list":true,"usergroup_view":true,"view":true,"view_category":true,"view_chunk":true,"view_context":true,"view_document":true,"view_element":true,"view_eventlog":true,"view_offline":true,"view_plugin":true,"view_propertyset":true,"view_role":true,"view_snippet":true,"view_sysinfo":true,"view_template":true,"view_tv":true,"view_unpublished":true,"view_user":true,"workspaces":true}', 'permissions'),
(3, 'Load Only', 'A minimal policy with permission to load an object.', 0, 3, '', '{"load":true}', 'permissions'),
(4, 'Load, List and View', 'Provides load, list and view permissions only.', 0, 3, '', '{"load":true,"list":true,"view":true}', 'permissions'),
(5, 'Object', 'An Object policy with all permissions.', 0, 3, '', '{"load":true,"list":true,"view":true,"save":true,"remove":true}', 'permissions'),
(6, 'Element', 'MODX Element policy with all attributes.', 0, 4, '', '{"add_children":true,"create":true,"delete":true,"list":true,"load":true,"remove":true,"save":true,"view":true,"copy":true}', 'permissions'),
(7, 'Content Editor', 'Context administration policy with limited, content-editing related Permissions, but no publishing.', 0, 1, '', '{"change_profile":true,"class_map":true,"countries":true,"edit_document":true,"frames":true,"help":true,"home":true,"load":true,"list":true,"logout":true,"menu_reports":true,"menu_site":true,"menu_support":true,"menu_tools":true,"menu_user":true,"resource_duplicate":true,"resource_tree":true,"save_document":true,"source_view":true,"tree_show_resource_ids":true,"view":true,"view_document":true,"new_document":true,"delete_document":true}', 'permissions'),
(8, 'Media Source Admin', 'Media Source administration policy.', 0, 5, '', '{"create":true,"copy":true,"load":true,"list":true,"save":true,"remove":true,"view":true}', 'permissions'),
(9, 'Media Source User', 'Media Source user policy, with basic viewing and using - but no editing - of Media Sources.', 0, 5, '', '{"load":true,"list":true,"view":true}', 'permissions'),
(10, 'Developer', 'Context administration policy with most Permissions except Administrator and Security functions.', 0, 0, '', '{"about":true,"change_password":true,"change_profile":true,"charsets":true,"class_map":true,"components":true,"content_types":true,"countries":true,"create":true,"credits":true,"customize_forms":true,"dashboards":true,"database":true,"delete_category":true,"delete_chunk":true,"delete_context":true,"delete_document":true,"delete_eventlog":true,"delete_plugin":true,"delete_propertyset":true,"delete_snippet":true,"delete_template":true,"delete_tv":true,"delete_role":true,"delete_user":true,"directory_chmod":true,"directory_create":true,"directory_list":true,"directory_remove":true,"directory_update":true,"edit_category":true,"edit_chunk":true,"edit_context":true,"edit_document":true,"edit_locked":true,"edit_plugin":true,"edit_propertyset":true,"edit_role":true,"edit_snippet":true,"edit_template":true,"edit_tv":true,"edit_user":true,"element_tree":true,"empty_cache":true,"error_log_erase":true,"error_log_view":true,"export_static":true,"file_create":true,"file_list":true,"file_manager":true,"file_remove":true,"file_tree":true,"file_update":true,"file_upload":true,"file_unpack":true,"file_view":true,"frames":true,"help":true,"home":true,"import_static":true,"languages":true,"lexicons":true,"list":true,"load":true,"logout":true,"logs":true,"menu_reports":true,"menu_site":true,"menu_support":true,"menu_system":true,"menu_tools":true,"menu_user":true,"menus":true,"messages":true,"namespaces":true,"new_category":true,"new_chunk":true,"new_context":true,"new_document":true,"new_static_resource":true,"new_symlink":true,"new_weblink":true,"new_document_in_root":true,"new_plugin":true,"new_propertyset":true,"new_role":true,"new_snippet":true,"new_template":true,"new_tv":true,"new_user":true,"packages":true,"property_sets":true,"providers":true,"publish_document":true,"purge_deleted":true,"remove":true,"resource_duplicate":true,"resource_quick_create":true,"resource_quick_update":true,"resource_tree":true,"save":true,"save_category":true,"save_chunk":true,"save_context":true,"save_document":true,"save_plugin":true,"save_propertyset":true,"save_snippet":true,"save_template":true,"save_tv":true,"save_user":true,"search":true,"settings":true,"source_delete":true,"source_edit":true,"source_save":true,"source_view":true,"sources":true,"tree_show_element_ids":true,"tree_show_resource_ids":true,"undelete_document":true,"unpublish_document":true,"unlock_element_properties":true,"view":true,"view_category":true,"view_chunk":true,"view_context":true,"view_document":true,"view_element":true,"view_eventlog":true,"view_offline":true,"view_plugin":true,"view_propertyset":true,"view_role":true,"view_snippet":true,"view_sysinfo":true,"view_template":true,"view_tv":true,"view_user":true,"view_unpublished":true,"workspaces":true}', 'permissions'),
(11, 'Context', 'A standard Context policy that you can apply when creating Context ACLs for basic read/write and view_unpublished access within a Context.', 0, 6, '', '{"load":true,"list":true,"view":true,"save":true,"remove":true,"copy":true,"view_unpublished":true}', 'permissions'),
(12, 'Hidden Namespace', 'Hidden Namespace policy, will not show Namespace in lists.', 0, 7, '', '{"load":false,"list":false,"view":true}', 'permissions');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_access_policy_templates`
--

CREATE TABLE `modx_access_policy_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `template_group` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext,
  `lexicon` varchar(255) NOT NULL DEFAULT 'permissions'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_access_policy_templates`
--

INSERT INTO `modx_access_policy_templates` (`id`, `template_group`, `name`, `description`, `lexicon`) VALUES
(1, 1, 'AdministratorTemplate', 'Context administration policy template with all permissions.', 'permissions'),
(2, 3, 'ResourceTemplate', 'Resource Policy Template with all attributes.', 'permissions'),
(3, 2, 'ObjectTemplate', 'Object Policy Template with all attributes.', 'permissions'),
(4, 4, 'ElementTemplate', 'Element Policy Template with all attributes.', 'permissions'),
(5, 5, 'MediaSourceTemplate', 'Media Source Policy Template with all attributes.', 'permissions'),
(6, 2, 'ContextTemplate', 'Context Policy Template with all attributes.', 'permissions'),
(7, 6, 'NamespaceTemplate', 'Namespace Policy Template with all attributes.', 'permissions');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_access_policy_template_groups`
--

CREATE TABLE `modx_access_policy_template_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_access_policy_template_groups`
--

INSERT INTO `modx_access_policy_template_groups` (`id`, `name`, `description`) VALUES
(1, 'Admin', 'All admin policy templates.'),
(2, 'Object', 'All Object-based policy templates.'),
(3, 'Resource', 'All Resource-based policy templates.'),
(4, 'Element', 'All Element-based policy templates.'),
(5, 'MediaSource', 'All Media Source-based policy templates.'),
(6, 'Namespace', 'All Namespace based policy templates.');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_access_resources`
--

CREATE TABLE `modx_access_resources` (
  `id` int(10) UNSIGNED NOT NULL,
  `target` varchar(100) NOT NULL DEFAULT '',
  `principal_class` varchar(100) NOT NULL DEFAULT 'modPrincipal',
  `principal` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `authority` int(10) UNSIGNED NOT NULL DEFAULT '9999',
  `policy` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `context_key` varchar(100) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `modx_access_resource_groups`
--

CREATE TABLE `modx_access_resource_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `target` varchar(100) NOT NULL DEFAULT '',
  `principal_class` varchar(100) NOT NULL DEFAULT 'modPrincipal',
  `principal` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `authority` int(10) UNSIGNED NOT NULL DEFAULT '9999',
  `policy` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `context_key` varchar(100) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `modx_access_templatevars`
--

CREATE TABLE `modx_access_templatevars` (
  `id` int(10) UNSIGNED NOT NULL,
  `target` varchar(100) NOT NULL DEFAULT '',
  `principal_class` varchar(100) NOT NULL DEFAULT 'modPrincipal',
  `principal` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `authority` int(10) UNSIGNED NOT NULL DEFAULT '9999',
  `policy` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `context_key` varchar(100) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `modx_actiondom`
--

CREATE TABLE `modx_actiondom` (
  `id` int(10) UNSIGNED NOT NULL,
  `set` int(11) NOT NULL DEFAULT '0',
  `action` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text,
  `xtype` varchar(100) NOT NULL DEFAULT '',
  `container` varchar(255) NOT NULL DEFAULT '',
  `rule` varchar(100) NOT NULL DEFAULT '',
  `value` text NOT NULL,
  `constraint` varchar(255) NOT NULL DEFAULT '',
  `constraint_field` varchar(100) NOT NULL DEFAULT '',
  `constraint_class` varchar(100) NOT NULL DEFAULT '',
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `for_parent` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `rank` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `modx_actions`
--

CREATE TABLE `modx_actions` (
  `id` int(10) UNSIGNED NOT NULL,
  `namespace` varchar(100) NOT NULL DEFAULT 'core',
  `controller` varchar(255) NOT NULL,
  `haslayout` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `lang_topics` text NOT NULL,
  `assets` text NOT NULL,
  `help_url` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_actions`
--

INSERT INTO `modx_actions` (`id`, `namespace`, `controller`, `haslayout`, `lang_topics`, `assets`, `help_url`) VALUES
(1, 'tagger', 'index', 1, 'tagger:default', '', ''),
(2, 'gallery', 'index', 1, 'gallery:default', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_actions_fields`
--

CREATE TABLE `modx_actions_fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `action` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `type` varchar(100) NOT NULL DEFAULT 'field',
  `tab` varchar(255) NOT NULL DEFAULT '',
  `form` varchar(255) NOT NULL DEFAULT '',
  `other` varchar(255) NOT NULL DEFAULT '',
  `rank` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_actions_fields`
--

INSERT INTO `modx_actions_fields` (`id`, `action`, `name`, `type`, `tab`, `form`, `other`, `rank`) VALUES
(1, 'resource/update', 'modx-resource-settings', 'tab', '', 'modx-panel-resource', '', 0),
(2, 'resource/update', 'modx-resource-main-left', 'tab', '', 'modx-panel-resource', '', 1),
(3, 'resource/update', 'id', 'field', 'modx-resource-main-left', 'modx-panel-resource', '', 0),
(4, 'resource/update', 'pagetitle', 'field', 'modx-resource-main-left', 'modx-panel-resource', '', 1),
(5, 'resource/update', 'longtitle', 'field', 'modx-resource-main-left', 'modx-panel-resource', '', 2),
(6, 'resource/update', 'description', 'field', 'modx-resource-main-left', 'modx-panel-resource', '', 3),
(7, 'resource/update', 'introtext', 'field', 'modx-resource-main-left', 'modx-panel-resource', '', 4),
(8, 'resource/update', 'modx-resource-main-right', 'tab', '', 'modx-panel-resource', '', 2),
(9, 'resource/update', 'template', 'field', 'modx-resource-main-right', 'modx-panel-resource', '', 0),
(10, 'resource/update', 'alias', 'field', 'modx-resource-main-right', 'modx-panel-resource', '', 1),
(11, 'resource/update', 'menutitle', 'field', 'modx-resource-main-right', 'modx-panel-resource', '', 2),
(12, 'resource/update', 'link_attributes', 'field', 'modx-resource-main-right', 'modx-panel-resource', '', 3),
(13, 'resource/update', 'hidemenu', 'field', 'modx-resource-main-right', 'modx-panel-resource', '', 4),
(14, 'resource/update', 'published', 'field', 'modx-resource-main-right', 'modx-panel-resource', '', 5),
(15, 'resource/update', 'modx-page-settings', 'tab', '', 'modx-panel-resource', '', 3),
(16, 'resource/update', 'modx-page-settings-left', 'tab', '', 'modx-panel-resource', '', 4),
(17, 'resource/update', 'parent-cmb', 'field', 'modx-page-settings-left', 'modx-panel-resource', '', 0),
(18, 'resource/update', 'class_key', 'field', 'modx-page-settings-left', 'modx-panel-resource', '', 1),
(19, 'resource/update', 'content_type', 'field', 'modx-page-settings-left', 'modx-panel-resource', '', 2),
(20, 'resource/update', 'content_dispo', 'field', 'modx-page-settings-left', 'modx-panel-resource', '', 3),
(21, 'resource/update', 'menuindex', 'field', 'modx-page-settings-left', 'modx-panel-resource', '', 4),
(22, 'resource/update', 'modx-page-settings-right', 'tab', '', 'modx-panel-resource', '', 5),
(23, 'resource/update', 'publishedon', 'field', 'modx-page-settings-right', 'modx-panel-resource', '', 0),
(24, 'resource/update', 'pub_date', 'field', 'modx-page-settings-right', 'modx-panel-resource', '', 1),
(25, 'resource/update', 'unpub_date', 'field', 'modx-page-settings-right', 'modx-panel-resource', '', 2),
(26, 'resource/update', 'modx-page-settings-right-box-left', 'tab', '', 'modx-panel-resource', '', 6),
(27, 'resource/update', 'isfolder', 'field', 'modx-page-settings-right-box-left', 'modx-panel-resource', '', 0),
(28, 'resource/update', 'searchable', 'field', 'modx-page-settings-right-box-left', 'modx-panel-resource', '', 1),
(29, 'resource/update', 'richtext', 'field', 'modx-page-settings-right-box-left', 'modx-panel-resource', '', 2),
(30, 'resource/update', 'uri_override', 'field', 'modx-page-settings-right-box-left', 'modx-panel-resource', '', 3),
(31, 'resource/update', 'uri', 'field', 'modx-page-settings-right-box-left', 'modx-panel-resource', '', 4),
(32, 'resource/update', 'modx-page-settings-right-box-right', 'tab', '', 'modx-panel-resource', '', 7),
(33, 'resource/update', 'cacheable', 'field', 'modx-page-settings-right-box-right', 'modx-panel-resource', '', 0),
(34, 'resource/update', 'syncsite', 'field', 'modx-page-settings-right-box-right', 'modx-panel-resource', '', 1),
(35, 'resource/update', 'deleted', 'field', 'modx-page-settings-right-box-right', 'modx-panel-resource', '', 2),
(36, 'resource/update', 'modx-panel-resource-tv', 'tab', '', 'modx-panel-resource', 'tv', 8),
(37, 'resource/update', 'modx-resource-access-permissions', 'tab', '', 'modx-panel-resource', '', 9),
(38, 'resource/update', 'modx-resource-content', 'field', 'modx-resource-content', 'modx-panel-resource', '', 0),
(39, 'resource/create', 'modx-resource-settings', 'tab', '', 'modx-panel-resource', '', 0),
(40, 'resource/create', 'modx-resource-main-left', 'tab', '', 'modx-panel-resource', '', 1),
(41, 'resource/create', 'id', 'field', 'modx-resource-main-left', 'modx-panel-resource', '', 0),
(42, 'resource/create', 'pagetitle', 'field', 'modx-resource-main-left', 'modx-panel-resource', '', 1),
(43, 'resource/create', 'longtitle', 'field', 'modx-resource-main-left', 'modx-panel-resource', '', 2),
(44, 'resource/create', 'description', 'field', 'modx-resource-main-left', 'modx-panel-resource', '', 3),
(45, 'resource/create', 'introtext', 'field', 'modx-resource-main-left', 'modx-panel-resource', '', 4),
(46, 'resource/create', 'modx-resource-main-right', 'tab', '', 'modx-panel-resource', '', 2),
(47, 'resource/create', 'template', 'field', 'modx-resource-main-right', 'modx-panel-resource', '', 0),
(48, 'resource/create', 'alias', 'field', 'modx-resource-main-right', 'modx-panel-resource', '', 1),
(49, 'resource/create', 'menutitle', 'field', 'modx-resource-main-right', 'modx-panel-resource', '', 2),
(50, 'resource/create', 'link_attributes', 'field', 'modx-resource-main-right', 'modx-panel-resource', '', 3),
(51, 'resource/create', 'hidemenu', 'field', 'modx-resource-main-right', 'modx-panel-resource', '', 4),
(52, 'resource/create', 'published', 'field', 'modx-resource-main-right', 'modx-panel-resource', '', 5),
(53, 'resource/create', 'modx-page-settings', 'tab', '', 'modx-panel-resource', '', 3),
(54, 'resource/create', 'modx-page-settings-left', 'tab', '', 'modx-panel-resource', '', 4),
(55, 'resource/create', 'parent-cmb', 'field', 'modx-page-settings-left', 'modx-panel-resource', '', 0),
(56, 'resource/create', 'class_key', 'field', 'modx-page-settings-left', 'modx-panel-resource', '', 1),
(57, 'resource/create', 'content_type', 'field', 'modx-page-settings-left', 'modx-panel-resource', '', 2),
(58, 'resource/create', 'content_dispo', 'field', 'modx-page-settings-left', 'modx-panel-resource', '', 3),
(59, 'resource/create', 'menuindex', 'field', 'modx-page-settings-left', 'modx-panel-resource', '', 4),
(60, 'resource/create', 'modx-page-settings-right', 'tab', '', 'modx-panel-resource', '', 5),
(61, 'resource/create', 'publishedon', 'field', 'modx-page-settings-right', 'modx-panel-resource', '', 0),
(62, 'resource/create', 'pub_date', 'field', 'modx-page-settings-right', 'modx-panel-resource', '', 1),
(63, 'resource/create', 'unpub_date', 'field', 'modx-page-settings-right', 'modx-panel-resource', '', 2),
(64, 'resource/create', 'modx-page-settings-right-box-left', 'tab', '', 'modx-panel-resource', '', 6),
(65, 'resource/create', 'isfolder', 'field', 'modx-page-settings-right-box-left', 'modx-panel-resource', '', 0),
(66, 'resource/create', 'searchable', 'field', 'modx-page-settings-right-box-left', 'modx-panel-resource', '', 1),
(67, 'resource/create', 'richtext', 'field', 'modx-page-settings-right-box-left', 'modx-panel-resource', '', 2),
(68, 'resource/create', 'uri_override', 'field', 'modx-page-settings-right-box-left', 'modx-panel-resource', '', 3),
(69, 'resource/create', 'uri', 'field', 'modx-page-settings-right-box-left', 'modx-panel-resource', '', 4),
(70, 'resource/create', 'modx-page-settings-right-box-right', 'tab', '', 'modx-panel-resource', '', 7),
(71, 'resource/create', 'cacheable', 'field', 'modx-page-settings-right-box-right', 'modx-panel-resource', '', 0),
(72, 'resource/create', 'syncsite', 'field', 'modx-page-settings-right-box-right', 'modx-panel-resource', '', 1),
(73, 'resource/create', 'deleted', 'field', 'modx-page-settings-right-box-right', 'modx-panel-resource', '', 2),
(74, 'resource/create', 'modx-panel-resource-tv', 'tab', '', 'modx-panel-resource', 'tv', 8),
(75, 'resource/create', 'modx-resource-access-permissions', 'tab', '', 'modx-panel-resource', '', 9),
(76, 'resource/create', 'modx-resource-content', 'field', 'modx-resource-content', 'modx-panel-resource', '', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `modx_active_users`
--

CREATE TABLE `modx_active_users` (
  `internalKey` int(9) NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL DEFAULT '',
  `lasthit` int(20) NOT NULL DEFAULT '0',
  `id` int(10) DEFAULT NULL,
  `action` varchar(255) NOT NULL DEFAULT '',
  `ip` varchar(20) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `modx_categories`
--

CREATE TABLE `modx_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent` int(10) UNSIGNED DEFAULT '0',
  `category` varchar(45) NOT NULL DEFAULT '',
  `rank` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_categories`
--

INSERT INTO `modx_categories` (`id`, `parent`, `category`, `rank`) VALUES
(1, 0, 'Breadcrumbs', 0),
(2, 0, 'Tagger', 0),
(4, 0, 'Франшиза', 0),
(5, 0, 'getResourceField', 0),
(6, 0, 'Элементы шаблона', 0),
(7, 0, 'Описание франшизы', 0),
(8, 0, 'Фотографии', 0),
(9, 0, 'Gallery', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `modx_categories_closure`
--

CREATE TABLE `modx_categories_closure` (
  `ancestor` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `descendant` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `depth` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_categories_closure`
--

INSERT INTO `modx_categories_closure` (`ancestor`, `descendant`, `depth`) VALUES
(1, 1, 0),
(0, 1, 0),
(2, 2, 0),
(0, 2, 0),
(4, 4, 0),
(0, 4, 0),
(5, 5, 0),
(0, 5, 0),
(6, 6, 0),
(0, 6, 0),
(7, 7, 0),
(0, 7, 0),
(8, 8, 0),
(0, 8, 0),
(9, 9, 0),
(0, 9, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `modx_class_map`
--

CREATE TABLE `modx_class_map` (
  `id` int(10) UNSIGNED NOT NULL,
  `class` varchar(120) NOT NULL DEFAULT '',
  `parent_class` varchar(120) NOT NULL DEFAULT '',
  `name_field` varchar(255) NOT NULL DEFAULT 'name',
  `path` tinytext,
  `lexicon` varchar(255) NOT NULL DEFAULT 'core:resource'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_class_map`
--

INSERT INTO `modx_class_map` (`id`, `class`, `parent_class`, `name_field`, `path`, `lexicon`) VALUES
(1, 'modDocument', 'modResource', 'pagetitle', '', 'core:resource'),
(2, 'modWebLink', 'modResource', 'pagetitle', '', 'core:resource'),
(3, 'modSymLink', 'modResource', 'pagetitle', '', 'core:resource'),
(4, 'modStaticResource', 'modResource', 'pagetitle', '', 'core:resource'),
(5, 'modTemplate', 'modElement', 'templatename', '', 'core:resource'),
(6, 'modTemplateVar', 'modElement', 'name', '', 'core:resource'),
(7, 'modChunk', 'modElement', 'name', '', 'core:resource'),
(8, 'modSnippet', 'modElement', 'name', '', 'core:resource'),
(9, 'modPlugin', 'modElement', 'name', '', 'core:resource');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_content_type`
--

CREATE TABLE `modx_content_type` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` tinytext,
  `mime_type` tinytext,
  `file_extensions` tinytext,
  `headers` mediumtext,
  `binary` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_content_type`
--

INSERT INTO `modx_content_type` (`id`, `name`, `description`, `mime_type`, `file_extensions`, `headers`, `binary`) VALUES
(1, 'HTML', 'HTML content', 'text/html', '.html', NULL, 0),
(2, 'XML', 'XML content', 'text/xml', '.xml', NULL, 0),
(3, 'text', 'plain text content', 'text/plain', '.txt', NULL, 0),
(4, 'CSS', 'CSS content', 'text/css', '.css', NULL, 0),
(5, 'javascript', 'javascript content', 'text/javascript', '.js', NULL, 0),
(6, 'RSS', 'For RSS feeds', 'application/rss+xml', '.rss', NULL, 0),
(7, 'JSON', 'JSON', 'application/json', '.json', NULL, 0),
(8, 'PDF', 'PDF Files', 'application/pdf', '.pdf', NULL, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `modx_context`
--

CREATE TABLE `modx_context` (
  `key` varchar(100) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` tinytext,
  `rank` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_context`
--

INSERT INTO `modx_context` (`key`, `name`, `description`, `rank`) VALUES
('web', 'Website', 'The default front-end context for your web site.', 0),
('mgr', 'Manager', 'The default manager or administration context for content management activity.', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `modx_context_resource`
--

CREATE TABLE `modx_context_resource` (
  `context_key` varchar(255) NOT NULL,
  `resource` int(11) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `modx_context_setting`
--

CREATE TABLE `modx_context_setting` (
  `context_key` varchar(255) NOT NULL,
  `key` varchar(50) NOT NULL,
  `value` mediumtext,
  `xtype` varchar(75) NOT NULL DEFAULT 'textfield',
  `namespace` varchar(40) NOT NULL DEFAULT 'core',
  `area` varchar(255) NOT NULL DEFAULT '',
  `editedon` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_context_setting`
--

INSERT INTO `modx_context_setting` (`context_key`, `key`, `value`, `xtype`, `namespace`, `area`, `editedon`) VALUES
('mgr', 'allow_tags_in_post', '1', 'combo-boolean', 'core', 'system', '0000-00-00 00:00:00'),
('mgr', 'modRequest.class', 'modManagerRequest', 'textfield', 'core', 'system', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_dashboard`
--

CREATE TABLE `modx_dashboard` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text,
  `hide_trees` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_dashboard`
--

INSERT INTO `modx_dashboard` (`id`, `name`, `description`, `hide_trees`) VALUES
(1, 'Default', '', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `modx_dashboard_widget`
--

CREATE TABLE `modx_dashboard_widget` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text,
  `type` varchar(100) NOT NULL,
  `content` mediumtext,
  `namespace` varchar(255) NOT NULL DEFAULT '',
  `lexicon` varchar(255) NOT NULL DEFAULT 'core:dashboards',
  `size` varchar(255) NOT NULL DEFAULT 'half'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_dashboard_widget`
--

INSERT INTO `modx_dashboard_widget` (`id`, `name`, `description`, `type`, `content`, `namespace`, `lexicon`, `size`) VALUES
(1, 'w_newsfeed', 'w_newsfeed_desc', 'file', '[[++manager_path]]controllers/default/dashboard/widget.modx-news.php', 'core', 'core:dashboards', 'half'),
(2, 'w_securityfeed', 'w_securityfeed_desc', 'file', '[[++manager_path]]controllers/default/dashboard/widget.modx-security.php', 'core', 'core:dashboards', 'half'),
(3, 'w_whosonline', 'w_whosonline_desc', 'file', '[[++manager_path]]controllers/default/dashboard/widget.grid-online.php', 'core', 'core:dashboards', 'half'),
(4, 'w_recentlyeditedresources', 'w_recentlyeditedresources_desc', 'file', '[[++manager_path]]controllers/default/dashboard/widget.grid-rer.php', 'core', 'core:dashboards', 'half'),
(5, 'w_configcheck', 'w_configcheck_desc', 'file', '[[++manager_path]]controllers/default/dashboard/widget.configcheck.php', 'core', 'core:dashboards', 'full');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_dashboard_widget_placement`
--

CREATE TABLE `modx_dashboard_widget_placement` (
  `dashboard` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `widget` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `rank` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_dashboard_widget_placement`
--

INSERT INTO `modx_dashboard_widget_placement` (`dashboard`, `widget`, `rank`) VALUES
(1, 5, 0),
(1, 1, 1),
(1, 2, 2),
(1, 3, 3),
(1, 4, 4);

-- --------------------------------------------------------

--
-- Структура таблицы `modx_documentgroup_names`
--

CREATE TABLE `modx_documentgroup_names` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `private_memgroup` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `private_webgroup` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `modx_document_groups`
--

CREATE TABLE `modx_document_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `document_group` int(10) NOT NULL DEFAULT '0',
  `document` int(10) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `modx_element_property_sets`
--

CREATE TABLE `modx_element_property_sets` (
  `element` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `element_class` varchar(100) NOT NULL DEFAULT '',
  `property_set` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `modx_extension_packages`
--

CREATE TABLE `modx_extension_packages` (
  `id` int(10) UNSIGNED NOT NULL,
  `namespace` varchar(40) NOT NULL DEFAULT 'core',
  `name` varchar(100) NOT NULL DEFAULT 'core',
  `path` text,
  `table_prefix` varchar(255) NOT NULL DEFAULT '',
  `service_class` varchar(255) NOT NULL DEFAULT '',
  `service_name` varchar(255) NOT NULL DEFAULT '',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `modx_fc_profiles`
--

CREATE TABLE `modx_fc_profiles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `rank` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `modx_fc_profiles_usergroups`
--

CREATE TABLE `modx_fc_profiles_usergroups` (
  `usergroup` int(11) NOT NULL DEFAULT '0',
  `profile` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `modx_fc_sets`
--

CREATE TABLE `modx_fc_sets` (
  `id` int(10) UNSIGNED NOT NULL,
  `profile` int(11) NOT NULL DEFAULT '0',
  `action` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `template` int(11) NOT NULL DEFAULT '0',
  `constraint` varchar(255) NOT NULL DEFAULT '',
  `constraint_field` varchar(100) NOT NULL DEFAULT '',
  `constraint_class` varchar(100) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `modx_gallery_albums`
--

CREATE TABLE `modx_gallery_albums` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `year` varchar(100) DEFAULT NULL,
  `description` text,
  `createdon` datetime DEFAULT NULL,
  `createdby` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `rank` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `prominent` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `watermark` varchar(255) NOT NULL DEFAULT '',
  `cover_filename` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_gallery_albums`
--

INSERT INTO `modx_gallery_albums` (`id`, `parent`, `name`, `year`, `description`, `createdon`, `createdby`, `rank`, `active`, `prominent`, `watermark`, `cover_filename`) VALUES
(1, 0, 'Photos', '2016', '', '2016-06-24 19:16:52', 1, 0, 1, 1, '', ''),
(2, 1, 'Чайбург', '', '', '2016-06-24 19:17:57', 1, 1, 1, 1, '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_gallery_album_contexts`
--

CREATE TABLE `modx_gallery_album_contexts` (
  `id` int(10) UNSIGNED NOT NULL,
  `album` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `context_key` varchar(100) NOT NULL DEFAULT 'web'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `modx_gallery_album_items`
--

CREATE TABLE `modx_gallery_album_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `item` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `album` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `rank` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_gallery_album_items`
--

INSERT INTO `modx_gallery_album_items` (`id`, `item`, `album`, `rank`) VALUES
(1, 1, 2, 0),
(2, 2, 2, 1),
(3, 3, 2, 2),
(4, 4, 2, 3),
(5, 5, 2, 4);

-- --------------------------------------------------------

--
-- Структура таблицы `modx_gallery_items`
--

CREATE TABLE `modx_gallery_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL DEFAULT '',
  `filename` varchar(255) NOT NULL DEFAULT '',
  `description` text,
  `mediatype` varchar(40) NOT NULL DEFAULT 'image',
  `url` text,
  `createdon` datetime DEFAULT NULL,
  `createdby` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `duration` varchar(40) NOT NULL DEFAULT '',
  `streamer` text,
  `watermark_pos` varchar(10) NOT NULL DEFAULT 'tl'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_gallery_items`
--

INSERT INTO `modx_gallery_items` (`id`, `name`, `filename`, `description`, `mediatype`, `url`, `createdon`, `createdby`, `active`, `duration`, `streamer`, `watermark_pos`) VALUES
(1, 'pic_1.jpg', '2/1.jpg', NULL, 'image', NULL, '2016-06-24 19:19:18', 1, 1, '', NULL, 'tl'),
(2, 'pic_2.jpg', '2/2.jpg', NULL, 'image', NULL, '2016-06-24 19:19:18', 1, 1, '', NULL, 'tl'),
(3, 'pic_3.jpg', '2/3.jpg', NULL, 'image', NULL, '2016-06-24 19:19:18', 1, 1, '', NULL, 'tl'),
(4, 'pic_4.jpg', '2/4.jpg', NULL, 'image', NULL, '2016-06-24 19:19:18', 1, 1, '', NULL, 'tl'),
(5, 'pic_5.jpg', '2/5.jpg', NULL, 'image', NULL, '2016-06-24 19:19:18', 1, 1, '', NULL, 'tl');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_gallery_tags`
--

CREATE TABLE `modx_gallery_tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `item` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `tag` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_gallery_tags`
--

INSERT INTO `modx_gallery_tags` (`id`, `item`, `tag`) VALUES
(1, 1, 'img_'),
(2, 2, 'img_'),
(3, 3, 'img_'),
(4, 4, 'img_'),
(5, 5, 'img_');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_lexicon_entries`
--

CREATE TABLE `modx_lexicon_entries` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `value` text NOT NULL,
  `topic` varchar(255) NOT NULL DEFAULT 'default',
  `namespace` varchar(40) NOT NULL DEFAULT 'core',
  `language` varchar(20) NOT NULL DEFAULT 'en',
  `createdon` datetime DEFAULT NULL,
  `editedon` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `modx_manager_log`
--

CREATE TABLE `modx_manager_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `user` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `occurred` datetime DEFAULT '0000-00-00 00:00:00',
  `action` varchar(100) NOT NULL DEFAULT '',
  `classKey` varchar(100) NOT NULL DEFAULT '',
  `item` varchar(255) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_manager_log`
--

INSERT INTO `modx_manager_log` (`id`, `user`, `occurred`, `action`, `classKey`, `item`) VALUES
(1, 1, '2016-06-08 13:27:09', 'login', 'modContext', 'mgr'),
(2, 1, '2016-06-08 13:33:33', 'template_update', 'modTemplate', '1'),
(3, 1, '2016-06-08 13:33:34', 'propertyset_update_from_element', 'modTemplate', '1'),
(4, 1, '2016-06-08 13:33:39', 'template_update', 'modTemplate', '1'),
(5, 1, '2016-06-08 13:33:39', 'propertyset_update_from_element', 'modTemplate', '1'),
(6, 1, '2016-06-08 13:33:53', 'template_update', 'modTemplate', '1'),
(7, 1, '2016-06-08 13:33:54', 'propertyset_update_from_element', 'modTemplate', '1'),
(8, 1, '2016-06-08 13:35:35', 'template_update', 'modTemplate', '1'),
(9, 1, '2016-06-08 13:35:35', 'propertyset_update_from_element', 'modTemplate', '1'),
(10, 1, '2016-06-08 13:57:52', 'setting_update', 'modSystemSetting', 'friendly_alias_realtime'),
(11, 1, '2016-06-08 13:58:09', 'setting_update', 'modSystemSetting', 'friendly_alias_translit'),
(12, 1, '2016-06-08 13:58:18', 'setting_update', 'modSystemSetting', 'friendly_urls'),
(13, 1, '2016-06-08 13:58:22', 'setting_update', 'modSystemSetting', 'global_duplicate_uri_check'),
(14, 1, '2016-06-08 13:58:31', 'setting_update', 'modSystemSetting', 'use_alias_path'),
(15, 1, '2016-06-08 13:58:33', 'setting_update', 'modSystemSetting', 'use_frozen_parent_uris'),
(16, 1, '2016-06-08 13:59:06', 'setting_update', 'modSystemSetting', 'friendly_alias_max_length'),
(17, 1, '2016-06-08 14:01:52', 'resource_create', 'modDocument', '2'),
(18, 1, '2016-06-08 14:02:01', 'resource_update', 'modResource', '2'),
(19, 1, '2016-06-08 14:10:49', 'template_create', 'modTemplate', '2'),
(20, 1, '2016-06-08 14:11:21', 'category_create', 'modCategory', '3'),
(21, 1, '2016-06-08 14:13:34', 'category_create', 'modCategory', '4'),
(22, 1, '2016-06-08 14:16:54', 'tv_create', 'modTemplateVar', '1'),
(23, 1, '2016-06-08 14:17:21', 'tv_update', 'modTemplateVar', '1'),
(24, 1, '2016-06-08 14:17:22', 'propertyset_update_from_element', 'modTemplateVar', '1'),
(25, 1, '2016-06-08 14:21:06', 'tv_create', 'modTemplateVar', '2'),
(26, 1, '2016-06-08 14:24:35', 'tv_create', 'modTemplateVar', '3'),
(27, 1, '2016-06-08 14:25:55', 'tv_update', 'modTemplateVar', '3'),
(28, 1, '2016-06-08 14:25:55', 'propertyset_update_from_element', 'modTemplateVar', '3'),
(29, 1, '2016-06-08 14:26:17', 'tv_update', 'modTemplateVar', '3'),
(30, 1, '2016-06-08 14:26:17', 'propertyset_update_from_element', 'modTemplateVar', '3'),
(31, 1, '2016-06-08 14:29:43', 'tagger.group_create', 'TaggerGroup', '1'),
(32, 1, '2016-06-08 14:33:41', 'resource_update', 'modResource', '2'),
(33, 1, '2016-06-08 14:35:40', 'resource_update', 'modResource', '2'),
(34, 1, '2016-06-08 14:35:47', 'resource_update', 'modResource', '2'),
(35, 1, '2016-06-08 14:37:19', 'tagger.group_update', 'TaggerGroup', '1'),
(36, 1, '2016-06-08 14:38:02', 'tagger.group_create', 'TaggerGroup', '2'),
(37, 1, '2016-06-08 14:38:58', 'tagger.group_update', 'TaggerGroup', '2'),
(38, 1, '2016-06-08 14:39:27', 'tagger.tag_create', 'TaggerTag', '1'),
(39, 1, '2016-06-08 14:40:20', 'tagger.tag_create', 'TaggerTag', '2'),
(40, 1, '2016-06-08 14:45:28', 'tagger.tag_create', 'TaggerTag', '3'),
(41, 1, '2016-06-08 14:46:02', 'tagger.tag_create', 'TaggerTag', '4'),
(42, 1, '2016-06-08 14:46:54', 'tagger.tag_create', 'TaggerTag', '5'),
(43, 1, '2016-06-08 14:47:43', 'tagger.group_update', 'TaggerGroup', '2'),
(44, 1, '2016-06-08 14:48:46', 'tagger.tag_create', 'TaggerTag', '6'),
(45, 1, '2016-06-08 14:49:44', 'resource_update', 'modResource', '2'),
(46, 1, '2016-06-08 14:51:43', 'directory_create', '', '/Applications/MAMP/htdocs/franchise_cmf/images'),
(47, 1, '2016-06-08 14:52:07', 'directory_create', '', '/Applications/MAMP/htdocs/franchise_cmf/images/catalog'),
(48, 1, '2016-06-08 14:52:25', 'file_upload', '', '/Applications/MAMP/htdocs/franchise_cmf/images/catalog/'),
(49, 1, '2016-06-08 14:54:28', 'tagger.group_update', 'TaggerGroup', '2'),
(50, 1, '2016-06-08 14:54:37', 'tagger.group_update', 'TaggerGroup', '1'),
(51, 1, '2016-06-08 14:55:08', 'tagger.group_update', 'TaggerGroup', '1'),
(52, 1, '2016-06-08 14:56:52', 'resource_create', 'modDocument', '3'),
(53, 1, '2016-06-08 14:57:07', 'resource_update', 'modResource', '3'),
(54, 1, '2016-06-08 14:58:20', 'duplicate_resource', 'modDocument', '4'),
(55, 1, '2016-06-08 14:58:46', 'duplicate_resource', 'modDocument', '5'),
(56, 1, '2016-06-08 14:59:00', 'duplicate_resource', 'modDocument', '6'),
(57, 1, '2016-06-08 14:59:49', 'duplicate_resource', 'modDocument', '7'),
(58, 1, '2016-06-08 15:00:19', 'resource_update', 'modResource', '7'),
(59, 1, '2016-06-08 15:00:47', 'duplicate_resource', 'modDocument', '8'),
(60, 1, '2016-06-08 15:01:20', 'duplicate_resource', 'modDocument', '9'),
(61, 1, '2016-06-08 15:05:34', 'chunk_create', 'modChunk', '1'),
(62, 1, '2016-06-08 15:05:54', 'template_update', 'modTemplate', '1'),
(63, 1, '2016-06-08 15:05:55', 'propertyset_update_from_element', 'modTemplate', '1'),
(64, 1, '2016-06-08 15:08:51', 'template_update', 'modTemplate', '1'),
(65, 1, '2016-06-08 15:08:52', 'propertyset_update_from_element', 'modTemplate', '1'),
(66, 1, '2016-06-08 15:09:29', 'template_update', 'modTemplate', '1'),
(67, 1, '2016-06-08 15:09:29', 'propertyset_update_from_element', 'modTemplate', '1'),
(68, 1, '2016-06-08 15:09:58', 'template_update', 'modTemplate', '1'),
(69, 1, '2016-06-08 15:09:58', 'propertyset_update_from_element', 'modTemplate', '1'),
(70, 1, '2016-06-08 15:10:16', 'template_update', 'modTemplate', '1'),
(71, 1, '2016-06-08 15:10:16', 'propertyset_update_from_element', 'modTemplate', '1'),
(72, 1, '2016-06-08 15:13:06', 'resource_update', 'modResource', '3'),
(73, 1, '2016-06-08 15:13:46', 'resource_update', 'modResource', '4'),
(74, 1, '2016-06-08 15:13:52', 'resource_update', 'modResource', '5'),
(75, 1, '2016-06-08 15:13:57', 'resource_update', 'modResource', '6'),
(76, 1, '2016-06-08 15:14:02', 'resource_update', 'modResource', '8'),
(77, 1, '2016-06-08 15:14:07', 'resource_update', 'modResource', '7'),
(78, 1, '2016-06-08 15:14:12', 'resource_update', 'modResource', '9'),
(79, 1, '2016-06-08 15:15:41', 'duplicate_resource', 'modDocument', '10'),
(80, 1, '2016-06-08 15:17:12', 'chunk_update', 'modChunk', '1'),
(81, 1, '2016-06-08 15:17:12', 'propertyset_update_from_element', 'modChunk', '1'),
(82, 1, '2016-06-08 15:22:21', 'resource_create', 'modDocument', '11'),
(83, 1, '2016-06-08 15:22:58', 'resource_create', 'modDocument', '12'),
(84, 1, '2016-06-08 15:23:06', 'resource_sort', '', 'unknown'),
(85, 1, '2016-06-08 15:23:13', 'resource_sort', '', 'unknown'),
(86, 1, '2016-06-08 15:23:18', 'resource_sort', '', 'unknown'),
(87, 1, '2016-06-08 15:23:23', 'resource_sort', '', 'unknown'),
(88, 1, '2016-06-08 15:23:54', 'resource_create', 'modDocument', '13'),
(89, 1, '2016-06-08 15:24:06', 'resource_sort', '', 'unknown'),
(90, 1, '2016-06-08 15:24:08', 'resource_sort', '', 'unknown'),
(91, 1, '2016-06-08 15:24:10', 'resource_sort', '', 'unknown'),
(92, 1, '2016-06-08 15:24:12', 'resource_sort', '', 'unknown'),
(93, 1, '2016-06-08 15:24:15', 'resource_sort', '', 'unknown'),
(94, 1, '2016-06-08 15:24:40', 'resource_update', 'modResource', '12'),
(95, 1, '2016-06-08 15:24:45', 'resource_update', 'modResource', '13'),
(96, 1, '2016-06-08 15:34:26', 'template_update', 'modTemplate', '1'),
(97, 1, '2016-06-08 15:34:26', 'propertyset_update_from_element', 'modTemplate', '1'),
(98, 1, '2016-06-08 15:37:14', 'template_update', 'modTemplate', '1'),
(99, 1, '2016-06-08 15:37:14', 'propertyset_update_from_element', 'modTemplate', '1'),
(100, 1, '2016-06-08 15:38:14', 'template_update', 'modTemplate', '1'),
(101, 1, '2016-06-08 15:38:14', 'propertyset_update_from_element', 'modTemplate', '1'),
(102, 1, '2016-06-08 15:41:30', 'chunk_update', 'modChunk', '1'),
(103, 1, '2016-06-08 15:41:30', 'propertyset_update_from_element', 'modChunk', '1'),
(104, 1, '2016-06-08 15:41:52', 'chunk_update', 'modChunk', '1'),
(105, 1, '2016-06-08 15:41:53', 'propertyset_update_from_element', 'modChunk', '1'),
(106, 1, '2016-06-08 15:42:05', 'chunk_update', 'modChunk', '1'),
(107, 1, '2016-06-08 15:42:05', 'propertyset_update_from_element', 'modChunk', '1'),
(108, 1, '2016-06-08 15:42:17', 'chunk_update', 'modChunk', '1'),
(109, 1, '2016-06-08 15:42:17', 'propertyset_update_from_element', 'modChunk', '1'),
(110, 1, '2016-06-08 15:42:35', 'chunk_update', 'modChunk', '1'),
(111, 1, '2016-06-08 15:42:35', 'propertyset_update_from_element', 'modChunk', '1'),
(112, 1, '2016-06-08 15:42:54', 'chunk_update', 'modChunk', '1'),
(113, 1, '2016-06-08 15:42:54', 'propertyset_update_from_element', 'modChunk', '1'),
(114, 1, '2016-06-08 15:43:08', 'chunk_update', 'modChunk', '1'),
(115, 1, '2016-06-08 15:43:08', 'propertyset_update_from_element', 'modChunk', '1'),
(116, 1, '2016-06-08 15:44:31', 'chunk_update', 'modChunk', '1'),
(117, 1, '2016-06-08 15:44:31', 'propertyset_update_from_element', 'modChunk', '1'),
(118, 1, '2016-06-08 15:48:18', 'chunk_update', 'modChunk', '1'),
(119, 1, '2016-06-08 15:48:18', 'propertyset_update_from_element', 'modChunk', '1'),
(120, 1, '2016-06-08 15:48:34', 'chunk_update', 'modChunk', '1'),
(121, 1, '2016-06-08 15:48:34', 'propertyset_update_from_element', 'modChunk', '1'),
(122, 1, '2016-06-08 16:05:28', 'chunk_update', 'modChunk', '1'),
(123, 1, '2016-06-08 16:05:29', 'propertyset_update_from_element', 'modChunk', '1'),
(124, 1, '2016-06-08 16:11:05', 'chunk_update', 'modChunk', '1'),
(125, 1, '2016-06-08 16:11:05', 'propertyset_update_from_element', 'modChunk', '1'),
(126, 1, '2016-06-08 16:13:33', 'chunk_update', 'modChunk', '1'),
(127, 1, '2016-06-08 16:13:33', 'propertyset_update_from_element', 'modChunk', '1'),
(128, 1, '2016-06-08 16:14:14', 'chunk_update', 'modChunk', '1'),
(129, 1, '2016-06-08 16:14:14', 'propertyset_update_from_element', 'modChunk', '1'),
(130, 1, '2016-06-08 16:18:56', 'chunk_update', 'modChunk', '1'),
(131, 1, '2016-06-08 16:18:56', 'propertyset_update_from_element', 'modChunk', '1'),
(132, 1, '2016-06-08 16:22:48', 'chunk_update', 'modChunk', '1'),
(133, 1, '2016-06-08 16:22:48', 'propertyset_update_from_element', 'modChunk', '1'),
(134, 1, '2016-06-08 16:30:58', 'chunk_update', 'modChunk', '1'),
(135, 1, '2016-06-08 16:30:59', 'propertyset_update_from_element', 'modChunk', '1'),
(136, 1, '2016-06-08 16:34:14', 'snippet_create', 'modSnippet', '7'),
(137, 1, '2016-06-08 16:34:57', 'chunk_update', 'modChunk', '1'),
(138, 1, '2016-06-08 16:34:57', 'propertyset_update_from_element', 'modChunk', '1'),
(139, 1, '2016-06-08 16:35:58', 'snippet_update', 'modSnippet', '7'),
(140, 1, '2016-06-08 16:35:59', 'propertyset_update_from_element', 'modSnippet', '7'),
(141, 1, '2016-06-08 16:38:50', 'chunk_update', 'modChunk', '1'),
(142, 1, '2016-06-08 16:38:50', 'propertyset_update_from_element', 'modChunk', '1'),
(143, 1, '2016-06-08 17:04:07', 'snippet_update', 'modSnippet', '7'),
(144, 1, '2016-06-08 17:04:07', 'propertyset_update_from_element', 'modSnippet', '7'),
(145, 1, '2016-06-08 17:04:20', 'chunk_update', 'modChunk', '1'),
(146, 1, '2016-06-08 17:04:20', 'propertyset_update_from_element', 'modChunk', '1'),
(147, 1, '2016-06-08 17:04:48', 'chunk_update', 'modChunk', '1'),
(148, 1, '2016-06-08 17:04:48', 'propertyset_update_from_element', 'modChunk', '1'),
(149, 1, '2016-06-08 17:05:07', 'chunk_update', 'modChunk', '1'),
(150, 1, '2016-06-08 17:05:07', 'propertyset_update_from_element', 'modChunk', '1'),
(151, 1, '2016-06-08 17:05:30', 'chunk_update', 'modChunk', '1'),
(152, 1, '2016-06-08 17:05:30', 'propertyset_update_from_element', 'modChunk', '1'),
(153, 1, '2016-06-08 17:06:48', 'chunk_update', 'modChunk', '1'),
(154, 1, '2016-06-08 17:06:49', 'propertyset_update_from_element', 'modChunk', '1'),
(155, 1, '2016-06-08 17:07:20', 'chunk_update', 'modChunk', '1'),
(156, 1, '2016-06-08 17:07:20', 'propertyset_update_from_element', 'modChunk', '1'),
(157, 1, '2016-06-08 17:09:16', 'snippet_delete', 'modSnippet', '7'),
(158, 1, '2016-06-08 17:10:14', 'template_create', 'modTemplate', '3'),
(159, 1, '2016-06-08 17:10:40', 'template_update', 'modTemplate', '3'),
(160, 1, '2016-06-08 17:10:40', 'propertyset_update_from_element', 'modTemplate', '3'),
(161, 1, '2016-06-08 17:10:58', 'resource_update', 'modResource', '2'),
(162, 1, '2016-06-08 17:12:32', 'category_create', 'modCategory', '6'),
(163, 1, '2016-06-08 17:13:17', 'chunk_create', 'modChunk', '2'),
(164, 1, '2016-06-08 17:13:49', 'template_update', 'modTemplate', '1'),
(165, 1, '2016-06-08 17:13:49', 'propertyset_update_from_element', 'modTemplate', '1'),
(166, 1, '2016-06-08 17:14:39', 'chunk_create', 'modChunk', '3'),
(167, 1, '2016-06-08 17:14:59', 'template_update', 'modTemplate', '1'),
(168, 1, '2016-06-08 17:14:59', 'propertyset_update_from_element', 'modTemplate', '1'),
(169, 1, '2016-06-08 17:15:19', 'template_update', 'modTemplate', '1'),
(170, 1, '2016-06-08 17:15:19', 'propertyset_update_from_element', 'modTemplate', '1'),
(171, 1, '2016-06-08 17:16:31', 'template_update', 'modTemplate', '1'),
(172, 1, '2016-06-08 17:16:31', 'propertyset_update_from_element', 'modTemplate', '1'),
(173, 1, '2016-06-08 17:17:08', 'chunk_create', 'modChunk', '4'),
(174, 1, '2016-06-08 17:17:30', 'template_update', 'modTemplate', '1'),
(175, 1, '2016-06-08 17:17:30', 'propertyset_update_from_element', 'modTemplate', '1'),
(176, 1, '2016-06-08 17:17:57', 'template_update', 'modTemplate', '3'),
(177, 1, '2016-06-08 17:17:57', 'propertyset_update_from_element', 'modTemplate', '3'),
(178, 1, '2016-06-08 17:18:02', 'template_update', 'modTemplate', '3'),
(179, 1, '2016-06-08 17:18:03', 'propertyset_update_from_element', 'modTemplate', '3'),
(180, 1, '2016-06-08 17:18:21', 'template_update', 'modTemplate', '3'),
(181, 1, '2016-06-08 17:18:21', 'propertyset_update_from_element', 'modTemplate', '3'),
(182, 1, '2016-06-08 17:18:38', 'template_update', 'modTemplate', '3'),
(183, 1, '2016-06-08 17:18:38', 'propertyset_update_from_element', 'modTemplate', '3'),
(184, 1, '2016-06-08 17:18:57', 'template_update', 'modTemplate', '3'),
(185, 1, '2016-06-08 17:18:57', 'propertyset_update_from_element', 'modTemplate', '3'),
(186, 1, '2016-06-08 17:19:36', 'template_update', 'modTemplate', '1'),
(187, 1, '2016-06-08 17:19:36', 'propertyset_update_from_element', 'modTemplate', '1'),
(188, 1, '2016-06-08 17:22:05', 'chunk_update', 'modChunk', '4'),
(189, 1, '2016-06-08 17:22:05', 'propertyset_update_from_element', 'modChunk', '4'),
(190, 1, '2016-06-08 17:22:31', 'chunk_update', 'modChunk', '4'),
(191, 1, '2016-06-08 17:22:31', 'propertyset_update_from_element', 'modChunk', '4'),
(192, 1, '2016-06-08 17:26:23', 'template_update', 'modTemplate', '1'),
(193, 1, '2016-06-08 17:26:23', 'propertyset_update_from_element', 'modTemplate', '1'),
(194, 1, '2016-06-08 17:26:40', 'template_update', 'modTemplate', '1'),
(195, 1, '2016-06-08 17:26:40', 'propertyset_update_from_element', 'modTemplate', '1'),
(196, 1, '2016-06-08 17:27:30', 'template_update', 'modTemplate', '3'),
(197, 1, '2016-06-08 17:27:30', 'propertyset_update_from_element', 'modTemplate', '3'),
(198, 1, '2016-06-08 17:28:49', 'template_update', 'modTemplate', '3'),
(199, 1, '2016-06-08 17:28:49', 'propertyset_update_from_element', 'modTemplate', '3'),
(200, 1, '2016-06-08 17:29:04', 'template_update', 'modTemplate', '3'),
(201, 1, '2016-06-08 17:29:04', 'propertyset_update_from_element', 'modTemplate', '3'),
(202, 1, '2016-06-08 17:30:49', 'template_update', 'modTemplate', '3'),
(203, 1, '2016-06-08 17:30:49', 'propertyset_update_from_element', 'modTemplate', '3'),
(204, 1, '2016-06-08 17:31:21', 'template_update', 'modTemplate', '3'),
(205, 1, '2016-06-08 17:31:21', 'propertyset_update_from_element', 'modTemplate', '3'),
(206, 1, '2016-06-08 17:31:28', 'template_update', 'modTemplate', '3'),
(207, 1, '2016-06-08 17:31:28', 'propertyset_update_from_element', 'modTemplate', '3'),
(208, 1, '2016-06-08 17:32:36', 'chunk_duplicate', 'modChunk', '5'),
(209, 1, '2016-06-08 17:33:07', 'template_update', 'modTemplate', '1'),
(210, 1, '2016-06-08 17:33:07', 'propertyset_update_from_element', 'modTemplate', '1'),
(211, 1, '2016-06-08 17:40:56', 'template_update', 'modTemplate', '3'),
(212, 1, '2016-06-08 17:40:56', 'propertyset_update_from_element', 'modTemplate', '3'),
(213, 1, '2016-06-08 17:44:10', 'tagger.tag_delete', 'TaggerTag', '6'),
(214, 1, '2016-06-08 17:44:13', 'tagger.tag_delete', 'TaggerTag', '5'),
(215, 1, '2016-06-08 17:44:23', 'tagger.group_delete', 'TaggerGroup', '2'),
(216, 1, '2016-06-08 17:47:01', 'resource_create', 'modDocument', '14'),
(217, 1, '2016-06-08 17:47:35', 'resource_create', 'modDocument', '15'),
(218, 1, '2016-06-08 17:47:47', 'resource_update', 'modResource', '15'),
(219, 1, '2016-06-08 17:48:24', 'duplicate_resource', 'modDocument', '16'),
(220, 1, '2016-06-08 17:49:08', 'duplicate_resource', 'modDocument', '17'),
(221, 1, '2016-06-08 17:49:46', 'resource_update', 'modResource', '17'),
(222, 1, '2016-06-08 17:50:10', 'duplicate_resource', 'modDocument', '18'),
(223, 1, '2016-06-08 17:51:06', 'duplicate_resource', 'modDocument', '19'),
(224, 1, '2016-06-08 17:52:49', 'template_update', 'modTemplate', '1'),
(225, 1, '2016-06-08 17:52:49', 'propertyset_update_from_element', 'modTemplate', '1'),
(226, 1, '2016-06-08 17:53:13', 'template_update', 'modTemplate', '1'),
(227, 1, '2016-06-08 17:53:13', 'propertyset_update_from_element', 'modTemplate', '1'),
(228, 1, '2016-06-08 17:59:04', 'template_update', 'modTemplate', '1'),
(229, 1, '2016-06-08 17:59:05', 'propertyset_update_from_element', 'modTemplate', '1'),
(230, 1, '2016-06-08 18:00:13', 'chunk_create', 'modChunk', '6'),
(231, 1, '2016-06-08 18:00:17', 'template_update', 'modTemplate', '1'),
(232, 1, '2016-06-08 18:00:17', 'propertyset_update_from_element', 'modTemplate', '1'),
(233, 1, '2016-06-08 18:04:01', 'template_update', 'modTemplate', '1'),
(234, 1, '2016-06-08 18:04:01', 'propertyset_update_from_element', 'modTemplate', '1'),
(235, 1, '2016-06-08 18:05:54', 'template_update', 'modTemplate', '1'),
(236, 1, '2016-06-08 18:05:54', 'propertyset_update_from_element', 'modTemplate', '1'),
(237, 1, '2016-06-08 18:06:10', 'template_update', 'modTemplate', '1'),
(238, 1, '2016-06-08 18:06:10', 'propertyset_update_from_element', 'modTemplate', '1'),
(239, 1, '2016-06-08 18:08:01', 'template_update', 'modTemplate', '1'),
(240, 1, '2016-06-08 18:08:02', 'propertyset_update_from_element', 'modTemplate', '1'),
(241, 1, '2016-06-08 18:08:24', 'template_update', 'modTemplate', '1'),
(242, 1, '2016-06-08 18:08:24', 'propertyset_update_from_element', 'modTemplate', '1'),
(243, 1, '2016-06-08 18:08:45', 'chunk_update', 'modChunk', '6'),
(244, 1, '2016-06-08 18:08:45', 'propertyset_update_from_element', 'modChunk', '6'),
(245, 1, '2016-06-08 18:09:14', 'chunk_update', 'modChunk', '6'),
(246, 1, '2016-06-08 18:09:14', 'propertyset_update_from_element', 'modChunk', '6'),
(247, 1, '2016-06-08 18:11:11', 'chunk_update', 'modChunk', '6'),
(248, 1, '2016-06-08 18:11:11', 'propertyset_update_from_element', 'modChunk', '6'),
(249, 1, '2016-06-08 18:20:55', 'template_update', 'modTemplate', '1'),
(250, 1, '2016-06-08 18:20:56', 'propertyset_update_from_element', 'modTemplate', '1'),
(251, 1, '2016-06-08 18:21:42', 'chunk_create', 'modChunk', '7'),
(252, 1, '2016-06-08 18:21:44', 'template_update', 'modTemplate', '1'),
(253, 1, '2016-06-08 18:21:44', 'propertyset_update_from_element', 'modTemplate', '1'),
(254, 1, '2016-06-08 18:23:46', 'template_update', 'modTemplate', '1'),
(255, 1, '2016-06-08 18:23:46', 'propertyset_update_from_element', 'modTemplate', '1'),
(256, 1, '2016-06-08 18:23:54', 'template_update', 'modTemplate', '1'),
(257, 1, '2016-06-08 18:23:54', 'propertyset_update_from_element', 'modTemplate', '1'),
(258, 1, '2016-06-08 18:28:36', 'template_update', 'modTemplate', '1'),
(259, 1, '2016-06-08 18:28:36', 'propertyset_update_from_element', 'modTemplate', '1'),
(260, 1, '2016-06-08 18:29:44', 'chunk_duplicate', 'modChunk', '8'),
(261, 1, '2016-06-08 18:29:50', 'template_update', 'modTemplate', '1'),
(262, 1, '2016-06-08 18:29:50', 'propertyset_update_from_element', 'modTemplate', '1'),
(263, 1, '2016-06-08 18:30:07', 'chunk_update', 'modChunk', '8'),
(264, 1, '2016-06-08 18:30:07', 'propertyset_update_from_element', 'modChunk', '8'),
(265, 1, '2016-06-08 18:30:40', 'template_update', 'modTemplate', '1'),
(266, 1, '2016-06-08 18:30:40', 'propertyset_update_from_element', 'modTemplate', '1'),
(267, 1, '2016-06-08 18:40:55', 'chunk_update', 'modChunk', '6'),
(268, 1, '2016-06-08 18:40:55', 'propertyset_update_from_element', 'modChunk', '6'),
(269, 1, '2016-06-08 18:43:25', 'template_update', 'modTemplate', '1'),
(270, 1, '2016-06-08 18:43:25', 'propertyset_update_from_element', 'modTemplate', '1'),
(271, 1, '2016-06-08 18:43:42', 'resource_update', 'modResource', '1'),
(272, 1, '2016-06-08 19:06:17', 'chunk_update', 'modChunk', '6'),
(273, 1, '2016-06-08 19:06:17', 'propertyset_update_from_element', 'modChunk', '6'),
(274, 1, '2016-06-08 19:09:48', 'resource_update', 'modResource', '1'),
(275, 1, '2016-06-08 19:21:19', 'resource_update', 'modResource', '1'),
(276, 1, '2016-06-08 19:24:11', 'resource_update', 'modResource', '1'),
(277, 1, '2016-06-08 19:25:40', 'resource_update', 'modResource', '1'),
(278, 1, '2016-06-08 19:26:20', 'resource_update', 'modResource', '1'),
(279, 1, '2016-06-08 19:26:50', 'resource_update', 'modResource', '1'),
(280, 1, '2016-06-08 19:29:02', 'resource_update', 'modResource', '1'),
(281, 1, '2016-06-08 19:29:18', 'resource_update', 'modResource', '1'),
(282, 1, '2016-06-08 19:33:26', 'resource_update', 'modResource', '1'),
(283, 1, '2016-06-08 19:34:22', 'resource_update', 'modResource', '1'),
(284, 1, '2016-06-08 19:35:13', 'template_update', 'modTemplate', '1'),
(285, 1, '2016-06-08 19:35:13', 'propertyset_update_from_element', 'modTemplate', '1'),
(286, 1, '2016-06-08 19:35:39', 'template_update', 'modTemplate', '1'),
(287, 1, '2016-06-08 19:35:39', 'propertyset_update_from_element', 'modTemplate', '1'),
(288, 1, '2016-06-08 19:39:52', 'template_update', 'modTemplate', '1'),
(289, 1, '2016-06-08 19:39:52', 'propertyset_update_from_element', 'modTemplate', '1'),
(290, 1, '2016-06-08 19:55:54', 'template_update', 'modTemplate', '1'),
(291, 1, '2016-06-08 19:55:54', 'propertyset_update_from_element', 'modTemplate', '1'),
(292, 1, '2016-06-08 20:14:59', 'template_update', 'modTemplate', '1'),
(293, 1, '2016-06-08 20:14:59', 'propertyset_update_from_element', 'modTemplate', '1'),
(294, 1, '2016-06-08 20:15:06', 'template_update', 'modTemplate', '1'),
(295, 1, '2016-06-08 20:15:06', 'propertyset_update_from_element', 'modTemplate', '1'),
(296, 1, '2016-06-08 20:15:47', 'template_update', 'modTemplate', '1'),
(297, 1, '2016-06-08 20:15:47', 'propertyset_update_from_element', 'modTemplate', '1'),
(298, 1, '2016-06-08 20:19:55', 'template_update', 'modTemplate', '1'),
(299, 1, '2016-06-08 20:19:55', 'propertyset_update_from_element', 'modTemplate', '1'),
(300, 1, '2016-06-08 20:26:27', 'template_update', 'modTemplate', '1'),
(301, 1, '2016-06-08 20:26:27', 'propertyset_update_from_element', 'modTemplate', '1'),
(302, 1, '2016-06-08 20:27:45', 'template_update', 'modTemplate', '1'),
(303, 1, '2016-06-08 20:27:45', 'propertyset_update_from_element', 'modTemplate', '1'),
(304, 1, '2016-06-08 20:28:06', 'template_update', 'modTemplate', '1'),
(305, 1, '2016-06-08 20:28:06', 'propertyset_update_from_element', 'modTemplate', '1'),
(306, 1, '2016-06-08 20:28:51', 'template_update', 'modTemplate', '1'),
(307, 1, '2016-06-08 20:28:51', 'propertyset_update_from_element', 'modTemplate', '1'),
(308, 1, '2016-06-08 20:29:17', 'template_update', 'modTemplate', '1'),
(309, 1, '2016-06-08 20:29:17', 'propertyset_update_from_element', 'modTemplate', '1'),
(310, 1, '2016-06-08 20:30:44', 'template_update', 'modTemplate', '1'),
(311, 1, '2016-06-08 20:30:44', 'propertyset_update_from_element', 'modTemplate', '1'),
(312, 1, '2016-06-08 20:30:49', 'template_update', 'modTemplate', '1'),
(313, 1, '2016-06-08 20:30:49', 'propertyset_update_from_element', 'modTemplate', '1'),
(314, 1, '2016-06-08 20:31:06', 'template_update', 'modTemplate', '1'),
(315, 1, '2016-06-08 20:31:06', 'propertyset_update_from_element', 'modTemplate', '1'),
(316, 1, '2016-06-08 20:36:51', 'template_update', 'modTemplate', '1'),
(317, 1, '2016-06-08 20:36:51', 'propertyset_update_from_element', 'modTemplate', '1'),
(318, 1, '2016-06-08 20:38:23', 'resource_update', 'modResource', '3'),
(319, 1, '2016-06-08 20:38:48', 'resource_update', 'modResource', '3'),
(320, 1, '2016-06-08 20:39:45', 'template_update', 'modTemplate', '1'),
(321, 1, '2016-06-08 20:39:45', 'propertyset_update_from_element', 'modTemplate', '1'),
(322, 1, '2016-06-08 20:39:53', 'template_update', 'modTemplate', '1'),
(323, 1, '2016-06-08 20:39:53', 'propertyset_update_from_element', 'modTemplate', '1'),
(324, 1, '2016-06-08 20:40:10', 'resource_update', 'modResource', '1'),
(325, 1, '2016-06-09 10:50:07', 'resource_update', 'modResource', '15'),
(326, 1, '2016-06-09 10:51:25', 'template_update', 'modTemplate', '3'),
(327, 1, '2016-06-09 10:51:25', 'propertyset_update_from_element', 'modTemplate', '3'),
(328, 1, '2016-06-09 10:52:43', 'template_update', 'modTemplate', '3'),
(329, 1, '2016-06-09 10:52:44', 'propertyset_update_from_element', 'modTemplate', '3'),
(330, 1, '2016-06-09 10:53:05', 'template_update', 'modTemplate', '3'),
(331, 1, '2016-06-09 10:53:05', 'propertyset_update_from_element', 'modTemplate', '3'),
(332, 1, '2016-06-09 10:53:20', 'template_update', 'modTemplate', '3'),
(333, 1, '2016-06-09 10:53:20', 'propertyset_update_from_element', 'modTemplate', '3'),
(334, 1, '2016-06-09 10:54:06', 'template_update', 'modTemplate', '3'),
(335, 1, '2016-06-09 10:54:07', 'propertyset_update_from_element', 'modTemplate', '3'),
(336, 1, '2016-06-09 13:39:28', 'login', 'modContext', 'mgr'),
(337, 1, '2016-06-09 13:40:46', 'file_upload', '', '/Applications/MAMP/htdocs/franchise_cmf/images/catalog/'),
(338, 1, '2016-06-09 13:40:52', 'resource_update', 'modResource', '6'),
(339, 1, '2016-06-09 13:41:49', 'file_upload', '', '/Applications/MAMP/htdocs/franchise_cmf/images/catalog/'),
(340, 1, '2016-06-09 13:41:56', 'resource_update', 'modResource', '5'),
(341, 1, '2016-06-09 13:51:38', 'resource_update', 'modResource', '5'),
(342, 1, '2016-06-09 13:53:40', 'file_upload', '', '/Applications/MAMP/htdocs/franchise_cmf/images/catalog/'),
(343, 1, '2016-06-09 13:53:46', 'resource_update', 'modResource', '4'),
(344, 1, '2016-06-09 13:53:59', 'file_remove', '', '/Applications/MAMP/htdocs/franchise_cmf/images/catalog/pirat.jpg'),
(345, 1, '2016-06-09 13:54:21', 'file_upload', '', '/Applications/MAMP/htdocs/franchise_cmf/images/catalog/'),
(346, 1, '2016-06-09 13:54:27', 'resource_update', 'modResource', '4'),
(347, 1, '2016-06-09 13:56:26', 'file_upload', '', '/Applications/MAMP/htdocs/franchise_cmf/images/catalog/'),
(348, 1, '2016-06-09 13:56:32', 'resource_update', 'modResource', '7'),
(349, 1, '2016-06-09 13:57:26', 'file_upload', '', '/Applications/MAMP/htdocs/franchise_cmf/images/catalog/'),
(350, 1, '2016-06-09 13:57:35', 'resource_update', 'modResource', '8'),
(351, 1, '2016-06-09 14:00:06', 'file_upload', '', '/Applications/MAMP/htdocs/franchise_cmf/images/catalog/'),
(352, 1, '2016-06-09 14:00:11', 'resource_update', 'modResource', '9'),
(353, 1, '2016-06-09 14:01:31', 'file_upload', '', '/Applications/MAMP/htdocs/franchise_cmf/images/catalog/'),
(354, 1, '2016-06-09 14:01:41', 'resource_update', 'modResource', '10'),
(355, 1, '2016-06-09 14:03:35', 'chunk_update', 'modChunk', '5'),
(356, 1, '2016-06-09 14:03:35', 'propertyset_update_from_element', 'modChunk', '5'),
(357, 1, '2016-06-09 14:04:10', 'chunk_update', 'modChunk', '5'),
(358, 1, '2016-06-09 14:04:10', 'propertyset_update_from_element', 'modChunk', '5'),
(359, 1, '2016-06-09 14:04:33', 'resource_update', 'modResource', '10'),
(360, 1, '2016-06-09 14:05:09', 'chunk_update', 'modChunk', '5'),
(361, 1, '2016-06-09 14:05:10', 'propertyset_update_from_element', 'modChunk', '5'),
(362, 1, '2016-06-09 14:20:06', 'tv_create', 'modTemplateVar', '4'),
(363, 1, '2016-06-09 14:22:24', 'tv_update', 'modTemplateVar', '4'),
(364, 1, '2016-06-09 14:22:24', 'propertyset_update_from_element', 'modTemplateVar', '4'),
(365, 1, '2016-06-09 14:22:54', 'tv_update', 'modTemplateVar', '4'),
(366, 1, '2016-06-09 14:22:54', 'propertyset_update_from_element', 'modTemplateVar', '4'),
(367, 1, '2016-06-09 14:25:35', 'tv_update', 'modTemplateVar', '4'),
(368, 1, '2016-06-09 14:25:35', 'propertyset_update_from_element', 'modTemplateVar', '4'),
(369, 1, '2016-06-09 14:26:05', 'tv_update', 'modTemplateVar', '4'),
(370, 1, '2016-06-09 14:26:05', 'propertyset_update_from_element', 'modTemplateVar', '4'),
(371, 1, '2016-06-09 14:26:10', 'tv_update', 'modTemplateVar', '4'),
(372, 1, '2016-06-09 14:26:11', 'propertyset_update_from_element', 'modTemplateVar', '4'),
(373, 1, '2016-06-09 14:27:37', 'tv_update', 'modTemplateVar', '4'),
(374, 1, '2016-06-09 14:27:37', 'propertyset_update_from_element', 'modTemplateVar', '4'),
(375, 1, '2016-06-09 14:27:58', 'resource_update', 'modResource', '7'),
(376, 1, '2016-06-09 14:33:30', 'chunk_update', 'modChunk', '1'),
(377, 1, '2016-06-09 14:33:30', 'propertyset_update_from_element', 'modChunk', '1'),
(378, 1, '2016-06-09 14:41:22', 'chunk_update', 'modChunk', '1'),
(379, 1, '2016-06-09 14:41:22', 'propertyset_update_from_element', 'modChunk', '1'),
(380, 1, '2016-06-09 14:41:41', 'chunk_update', 'modChunk', '1'),
(381, 1, '2016-06-09 14:41:41', 'propertyset_update_from_element', 'modChunk', '1'),
(382, 1, '2016-06-09 14:46:28', 'chunk_update', 'modChunk', '1'),
(383, 1, '2016-06-09 14:46:28', 'propertyset_update_from_element', 'modChunk', '1'),
(384, 1, '2016-06-09 14:46:55', 'chunk_update', 'modChunk', '1'),
(385, 1, '2016-06-09 14:46:56', 'propertyset_update_from_element', 'modChunk', '1'),
(386, 1, '2016-06-09 14:53:48', 'chunk_update', 'modChunk', '5'),
(387, 1, '2016-06-09 14:53:49', 'propertyset_update_from_element', 'modChunk', '5'),
(388, 1, '2016-06-09 14:58:38', 'chunk_update', 'modChunk', '5'),
(389, 1, '2016-06-09 14:58:38', 'propertyset_update_from_element', 'modChunk', '5'),
(390, 1, '2016-06-09 14:59:08', 'chunk_update', 'modChunk', '5'),
(391, 1, '2016-06-09 14:59:08', 'propertyset_update_from_element', 'modChunk', '5'),
(392, 1, '2016-06-09 15:03:09', 'chunk_update', 'modChunk', '1'),
(393, 1, '2016-06-09 15:03:09', 'propertyset_update_from_element', 'modChunk', '1'),
(394, 1, '2016-06-09 15:06:13', 'tv_create', 'modTemplateVar', '5'),
(395, 1, '2016-06-09 15:06:22', 'tv_update', 'modTemplateVar', '5'),
(396, 1, '2016-06-09 15:06:23', 'propertyset_update_from_element', 'modTemplateVar', '5'),
(397, 1, '2016-06-09 15:07:59', 'chunk_update', 'modChunk', '1'),
(398, 1, '2016-06-09 15:07:59', 'propertyset_update_from_element', 'modChunk', '1'),
(399, 1, '2016-06-09 15:09:22', 'chunk_update', 'modChunk', '1'),
(400, 1, '2016-06-09 15:09:22', 'propertyset_update_from_element', 'modChunk', '1'),
(401, 1, '2016-06-09 15:09:31', 'chunk_update', 'modChunk', '1'),
(402, 1, '2016-06-09 15:09:32', 'propertyset_update_from_element', 'modChunk', '1'),
(403, 1, '2016-06-09 15:10:32', 'chunk_update', 'modChunk', '1'),
(404, 1, '2016-06-09 15:10:33', 'propertyset_update_from_element', 'modChunk', '1'),
(405, 1, '2016-06-09 15:13:29', 'template_update', 'modTemplate', '1'),
(406, 1, '2016-06-09 15:13:30', 'propertyset_update_from_element', 'modTemplate', '1'),
(407, 1, '2016-06-09 15:14:02', 'resource_update', 'modResource', '15'),
(408, 1, '2016-06-09 15:14:57', 'chunk_update', 'modChunk', '1'),
(409, 1, '2016-06-09 15:14:58', 'propertyset_update_from_element', 'modChunk', '1'),
(410, 1, '2016-06-09 15:15:14', 'chunk_update', 'modChunk', '1'),
(411, 1, '2016-06-09 15:15:15', 'propertyset_update_from_element', 'modChunk', '1'),
(412, 1, '2016-06-09 15:43:57', 'template_update', 'modTemplate', '3'),
(413, 1, '2016-06-09 15:43:57', 'propertyset_update_from_element', 'modTemplate', '3'),
(414, 1, '2016-06-09 15:45:33', 'template_update', 'modTemplate', '3'),
(415, 1, '2016-06-09 15:45:33', 'propertyset_update_from_element', 'modTemplate', '3'),
(416, 1, '2016-06-09 15:45:46', 'template_update', 'modTemplate', '3'),
(417, 1, '2016-06-09 15:45:46', 'propertyset_update_from_element', 'modTemplate', '3'),
(418, 1, '2016-06-09 15:46:40', 'template_update', 'modTemplate', '3'),
(419, 1, '2016-06-09 15:46:40', 'propertyset_update_from_element', 'modTemplate', '3'),
(420, 1, '2016-06-09 16:06:28', 'snippet_create', 'modSnippet', '9'),
(421, 1, '2016-06-09 16:16:10', 'template_update', 'modTemplate', '3'),
(422, 1, '2016-06-09 16:16:10', 'propertyset_update_from_element', 'modTemplate', '3'),
(423, 1, '2016-06-09 16:33:39', 'template_update', 'modTemplate', '3'),
(424, 1, '2016-06-09 16:33:39', 'propertyset_update_from_element', 'modTemplate', '3'),
(425, 1, '2016-06-09 16:34:02', 'template_update', 'modTemplate', '3'),
(426, 1, '2016-06-09 16:34:02', 'propertyset_update_from_element', 'modTemplate', '3'),
(427, 1, '2016-06-09 16:34:33', 'template_update', 'modTemplate', '3'),
(428, 1, '2016-06-09 16:34:33', 'propertyset_update_from_element', 'modTemplate', '3'),
(429, 1, '2016-06-09 16:48:26', 'template_update', 'modTemplate', '3'),
(430, 1, '2016-06-09 16:48:26', 'propertyset_update_from_element', 'modTemplate', '3'),
(431, 1, '2016-06-09 16:48:58', 'template_update', 'modTemplate', '3'),
(432, 1, '2016-06-09 16:48:58', 'propertyset_update_from_element', 'modTemplate', '3'),
(433, 1, '2016-06-09 17:09:52', 'template_update', 'modTemplate', '3'),
(434, 1, '2016-06-09 17:09:52', 'propertyset_update_from_element', 'modTemplate', '3'),
(435, 1, '2016-06-09 17:12:18', 'chunk_create', 'modChunk', '9'),
(436, 1, '2016-06-09 17:16:41', 'chunk_update', 'modChunk', '9'),
(437, 1, '2016-06-09 17:16:41', 'propertyset_update_from_element', 'modChunk', '9'),
(438, 1, '2016-06-09 17:22:55', 'chunk_update', 'modChunk', '9'),
(439, 1, '2016-06-09 17:22:56', 'propertyset_update_from_element', 'modChunk', '9'),
(440, 1, '2016-06-09 17:23:00', 'chunk_update', 'modChunk', '9'),
(441, 1, '2016-06-09 17:23:00', 'propertyset_update_from_element', 'modChunk', '9'),
(442, 1, '2016-06-09 17:23:06', 'chunk_update', 'modChunk', '9'),
(443, 1, '2016-06-09 17:23:06', 'propertyset_update_from_element', 'modChunk', '9'),
(444, 1, '2016-06-09 17:23:41', 'resource_update', 'modResource', '15'),
(445, 1, '2016-06-09 17:25:07', 'chunk_update', 'modChunk', '9'),
(446, 1, '2016-06-09 17:25:07', 'propertyset_update_from_element', 'modChunk', '9'),
(447, 1, '2016-06-09 17:44:04', 'template_update', 'modTemplate', '3'),
(448, 1, '2016-06-09 17:44:04', 'propertyset_update_from_element', 'modTemplate', '3'),
(449, 1, '2016-06-09 17:44:23', 'template_update', 'modTemplate', '3'),
(450, 1, '2016-06-09 17:44:23', 'propertyset_update_from_element', 'modTemplate', '3'),
(451, 1, '2016-06-09 18:01:00', 'chunk_update', 'modChunk', '9'),
(452, 1, '2016-06-09 18:01:01', 'propertyset_update_from_element', 'modChunk', '9'),
(453, 1, '2016-06-09 18:04:14', 'chunk_update', 'modChunk', '9'),
(454, 1, '2016-06-09 18:04:14', 'propertyset_update_from_element', 'modChunk', '9'),
(455, 1, '2016-06-09 18:05:15', 'chunk_update', 'modChunk', '9'),
(456, 1, '2016-06-09 18:05:15', 'propertyset_update_from_element', 'modChunk', '9'),
(457, 1, '2016-06-09 18:12:10', 'chunk_update', 'modChunk', '9'),
(458, 1, '2016-06-09 18:12:10', 'propertyset_update_from_element', 'modChunk', '9'),
(459, 1, '2016-06-09 18:12:52', 'chunk_update', 'modChunk', '9'),
(460, 1, '2016-06-09 18:12:52', 'propertyset_update_from_element', 'modChunk', '9'),
(461, 1, '2016-06-09 18:19:57', 'chunk_update', 'modChunk', '9'),
(462, 1, '2016-06-09 18:19:57', 'propertyset_update_from_element', 'modChunk', '9'),
(463, 1, '2016-06-09 18:20:05', 'chunk_update', 'modChunk', '9'),
(464, 1, '2016-06-09 18:20:05', 'propertyset_update_from_element', 'modChunk', '9'),
(465, 1, '2016-06-09 18:20:28', 'resource_update', 'modResource', '15'),
(466, 1, '2016-06-09 18:29:31', 'chunk_update', 'modChunk', '9'),
(467, 1, '2016-06-09 18:29:31', 'propertyset_update_from_element', 'modChunk', '9'),
(468, 1, '2016-06-09 18:30:09', 'chunk_update', 'modChunk', '9'),
(469, 1, '2016-06-09 18:30:09', 'propertyset_update_from_element', 'modChunk', '9'),
(470, 1, '2016-06-15 10:53:14', 'login', 'modContext', 'mgr'),
(471, 1, '2016-06-15 14:47:21', 'template_update', 'modTemplate', '1'),
(472, 1, '2016-06-15 14:47:22', 'propertyset_update_from_element', 'modTemplate', '1'),
(473, 1, '2016-06-15 15:04:17', 'template_update', 'modTemplate', '1'),
(474, 1, '2016-06-15 15:04:17', 'propertyset_update_from_element', 'modTemplate', '1'),
(475, 1, '2016-06-15 15:16:06', 'template_update', 'modTemplate', '1'),
(476, 1, '2016-06-15 15:16:07', 'propertyset_update_from_element', 'modTemplate', '1'),
(477, 1, '2016-06-15 15:16:41', 'template_update', 'modTemplate', '1'),
(478, 1, '2016-06-15 15:16:41', 'propertyset_update_from_element', 'modTemplate', '1'),
(479, 1, '2016-06-15 15:17:12', 'template_update', 'modTemplate', '1'),
(480, 1, '2016-06-15 15:17:12', 'propertyset_update_from_element', 'modTemplate', '1'),
(481, 1, '2016-06-15 15:28:24', 'template_update', 'modTemplate', '1'),
(482, 1, '2016-06-15 15:28:24', 'propertyset_update_from_element', 'modTemplate', '1'),
(483, 1, '2016-06-15 15:29:59', 'template_update', 'modTemplate', '1'),
(484, 1, '2016-06-15 15:29:59', 'propertyset_update_from_element', 'modTemplate', '1'),
(485, 1, '2016-06-15 15:30:41', 'template_update', 'modTemplate', '1'),
(486, 1, '2016-06-15 15:30:41', 'propertyset_update_from_element', 'modTemplate', '1'),
(487, 1, '2016-06-15 15:31:06', 'template_update', 'modTemplate', '1'),
(488, 1, '2016-06-15 15:31:06', 'propertyset_update_from_element', 'modTemplate', '1'),
(489, 1, '2016-06-15 15:32:18', 'template_update', 'modTemplate', '1'),
(490, 1, '2016-06-15 15:32:18', 'propertyset_update_from_element', 'modTemplate', '1'),
(491, 1, '2016-06-15 15:34:12', 'template_update', 'modTemplate', '1'),
(492, 1, '2016-06-15 15:34:12', 'propertyset_update_from_element', 'modTemplate', '1'),
(493, 1, '2016-06-15 15:35:37', 'template_update', 'modTemplate', '1'),
(494, 1, '2016-06-15 15:35:37', 'propertyset_update_from_element', 'modTemplate', '1'),
(495, 1, '2016-06-15 15:36:56', 'template_update', 'modTemplate', '1'),
(496, 1, '2016-06-15 15:36:56', 'propertyset_update_from_element', 'modTemplate', '1'),
(497, 1, '2016-06-15 15:37:09', 'template_update', 'modTemplate', '1'),
(498, 1, '2016-06-15 15:37:09', 'propertyset_update_from_element', 'modTemplate', '1'),
(499, 1, '2016-06-15 17:10:57', 'template_update', 'modTemplate', '1'),
(500, 1, '2016-06-15 17:10:57', 'propertyset_update_from_element', 'modTemplate', '1'),
(501, 1, '2016-06-15 17:12:39', 'template_update', 'modTemplate', '1'),
(502, 1, '2016-06-15 17:12:39', 'propertyset_update_from_element', 'modTemplate', '1'),
(503, 1, '2016-06-15 17:14:05', 'template_update', 'modTemplate', '1'),
(504, 1, '2016-06-15 17:14:05', 'propertyset_update_from_element', 'modTemplate', '1'),
(505, 1, '2016-06-24 12:54:11', 'login', 'modContext', 'mgr'),
(506, 1, '2016-06-24 17:32:49', 'template_update', 'modTemplate', '2'),
(507, 1, '2016-06-24 17:32:49', 'propertyset_update_from_element', 'modTemplate', '2'),
(508, 1, '2016-06-24 17:33:24', 'template_update', 'modTemplate', '2'),
(509, 1, '2016-06-24 17:33:24', 'propertyset_update_from_element', 'modTemplate', '2'),
(510, 1, '2016-06-24 17:33:42', 'template_update', 'modTemplate', '2'),
(511, 1, '2016-06-24 17:33:42', 'propertyset_update_from_element', 'modTemplate', '2'),
(512, 1, '2016-06-24 17:33:46', 'template_update', 'modTemplate', '2'),
(513, 1, '2016-06-24 17:33:46', 'propertyset_update_from_element', 'modTemplate', '2'),
(514, 1, '2016-06-24 17:37:06', 'template_update', 'modTemplate', '1'),
(515, 1, '2016-06-24 17:37:06', 'propertyset_update_from_element', 'modTemplate', '1'),
(516, 1, '2016-06-24 17:37:30', 'template_update', 'modTemplate', '1'),
(517, 1, '2016-06-24 17:37:30', 'propertyset_update_from_element', 'modTemplate', '1'),
(518, 1, '2016-06-24 17:37:46', 'template_update', 'modTemplate', '1'),
(519, 1, '2016-06-24 17:37:46', 'propertyset_update_from_element', 'modTemplate', '1'),
(520, 1, '2016-06-24 17:44:03', 'template_update', 'modTemplate', '2'),
(521, 1, '2016-06-24 17:44:03', 'propertyset_update_from_element', 'modTemplate', '2'),
(522, 1, '2016-06-24 17:57:06', 'template_update', 'modTemplate', '1'),
(523, 1, '2016-06-24 17:57:06', 'propertyset_update_from_element', 'modTemplate', '1'),
(524, 1, '2016-06-24 18:03:19', 'file_update', '', '/Applications/MAMP/htdocs/franchise_cmf/api/catalog.php'),
(525, 1, '2016-06-24 18:04:21', 'file_update', '', '/Applications/MAMP/htdocs/franchise_cmf/templates/chunks/catalog_item.html'),
(526, 1, '2016-06-24 18:04:42', 'file_update', '', '/Applications/MAMP/htdocs/franchise_cmf/templates/chunks/catalog_item.html'),
(527, 1, '2016-06-24 18:04:49', 'file_update', '', '/Applications/MAMP/htdocs/franchise_cmf/templates/chunks/catalog_item.html'),
(528, 1, '2016-06-24 18:16:50', 'file_update', '', '/Applications/MAMP/htdocs/franchise_cmf/templates/chunks/catalog_item.html'),
(529, 1, '2016-06-24 18:44:21', 'category_create', 'modCategory', '7'),
(530, 1, '2016-06-24 18:45:04', 'tv_create', 'modTemplateVar', '6'),
(531, 1, '2016-06-24 18:45:24', 'tv_update', 'modTemplateVar', '6'),
(532, 1, '2016-06-24 18:45:24', 'propertyset_update_from_element', 'modTemplateVar', '6'),
(533, 1, '2016-06-24 18:45:47', 'tv_update', 'modTemplateVar', '6'),
(534, 1, '2016-06-24 18:45:47', 'propertyset_update_from_element', 'modTemplateVar', '6'),
(535, 1, '2016-06-24 18:52:25', 'tv_create', 'modTemplateVar', '7'),
(536, 1, '2016-06-24 18:52:36', 'tv_update', 'modTemplateVar', '7'),
(537, 1, '2016-06-24 18:52:36', 'propertyset_update_from_element', 'modTemplateVar', '7'),
(538, 1, '2016-06-24 18:52:59', 'tv_update', 'modTemplateVar', '7'),
(539, 1, '2016-06-24 18:52:59', 'propertyset_update_from_element', 'modTemplateVar', '7'),
(540, 1, '2016-06-24 18:53:59', 'tv_duplicate', 'modTemplateVar', '8'),
(541, 1, '2016-06-24 18:54:25', 'tv_update', 'modTemplateVar', '8'),
(542, 1, '2016-06-24 18:54:25', 'propertyset_update_from_element', 'modTemplateVar', '8'),
(543, 1, '2016-06-24 18:55:15', 'tv_update', 'modTemplateVar', '8'),
(544, 1, '2016-06-24 18:55:15', 'propertyset_update_from_element', 'modTemplateVar', '8'),
(545, 1, '2016-06-24 18:56:22', 'tv_duplicate', 'modTemplateVar', '9'),
(546, 1, '2016-06-24 18:56:38', 'tv_update', 'modTemplateVar', '9'),
(547, 1, '2016-06-24 18:56:38', 'propertyset_update_from_element', 'modTemplateVar', '9'),
(548, 1, '2016-06-24 18:57:36', 'tv_update', 'modTemplateVar', '9'),
(549, 1, '2016-06-24 18:57:36', 'propertyset_update_from_element', 'modTemplateVar', '9'),
(550, 1, '2016-06-24 18:57:59', 'tv_duplicate', 'modTemplateVar', '10'),
(551, 1, '2016-06-24 18:58:24', 'tv_update', 'modTemplateVar', '10'),
(552, 1, '2016-06-24 18:58:25', 'propertyset_update_from_element', 'modTemplateVar', '10'),
(553, 1, '2016-06-24 19:03:02', 'category_create', 'modCategory', '8'),
(554, 1, '2016-06-24 19:16:52', 'gallery.album_create', 'galAlbum', '1'),
(555, 1, '2016-06-24 19:17:57', 'gallery.album_create', 'galAlbum', '2'),
(556, 1, '2016-06-24 19:19:18', 'directory_create', '', '/Applications/MAMP/htdocs/franchise_cmf/assets/gallery/2/'),
(557, 1, '2016-06-24 19:24:46', 'tv_create', 'modTemplateVar', '11'),
(558, 1, '2016-06-24 19:25:38', 'resource_update', 'modResource', '9'),
(559, 1, '2016-06-24 19:26:38', 'file_update', '', '/Applications/MAMP/htdocs/franchise_cmf/api/catalog.php'),
(560, 1, '2016-06-24 19:29:32', 'file_update', '', '/Applications/MAMP/htdocs/franchise_cmf/api/catalog.php'),
(561, 1, '2016-06-24 19:32:25', 'template_update', 'modTemplate', '2'),
(562, 1, '2016-06-24 19:32:25', 'propertyset_update_from_element', 'modTemplate', '2'),
(563, 1, '2016-06-24 19:36:57', 'template_update', 'modTemplate', '2'),
(564, 1, '2016-06-24 19:36:57', 'propertyset_update_from_element', 'modTemplate', '2'),
(565, 1, '2016-06-24 19:37:47', 'template_update', 'modTemplate', '2'),
(566, 1, '2016-06-24 19:37:48', 'propertyset_update_from_element', 'modTemplate', '2'),
(567, 1, '2016-06-24 19:38:30', 'template_update', 'modTemplate', '2'),
(568, 1, '2016-06-24 19:38:30', 'propertyset_update_from_element', 'modTemplate', '2'),
(569, 1, '2016-06-24 19:40:43', 'chunk_duplicate', 'modChunk', '12'),
(570, 1, '2016-06-24 19:41:35', 'chunk_update', 'modChunk', '12'),
(571, 1, '2016-06-24 19:41:36', 'propertyset_update_from_element', 'modChunk', '12'),
(572, 1, '2016-06-24 19:41:48', 'chunk_update', 'modChunk', '12'),
(573, 1, '2016-06-24 19:41:48', 'propertyset_update_from_element', 'modChunk', '12'),
(574, 1, '2016-06-24 19:42:42', 'template_update', 'modTemplate', '2'),
(575, 1, '2016-06-24 19:42:42', 'propertyset_update_from_element', 'modTemplate', '2'),
(576, 1, '2016-06-24 19:44:11', 'chunk_update', 'modChunk', '12'),
(577, 1, '2016-06-24 19:44:11', 'propertyset_update_from_element', 'modChunk', '12'),
(578, 1, '2016-06-24 19:44:52', 'template_update', 'modTemplate', '2'),
(579, 1, '2016-06-24 19:44:52', 'propertyset_update_from_element', 'modTemplate', '2'),
(580, 1, '2016-07-02 12:25:51', 'login', 'modContext', 'mgr'),
(581, 1, '2016-07-02 20:51:00', 'tv_create', 'modTemplateVar', '12'),
(582, 1, '2016-07-02 20:51:17', 'tv_update', 'modTemplateVar', '12'),
(583, 1, '2016-07-02 20:51:17', 'propertyset_update_from_element', 'modTemplateVar', '12'),
(584, 1, '2016-07-02 20:51:42', 'category_delete', 'modCategory', '3'),
(585, 1, '2016-07-02 20:53:19', 'tv_create', 'modTemplateVar', '13'),
(586, 1, '2016-07-02 20:53:39', 'tv_update', 'modTemplateVar', '13'),
(587, 1, '2016-07-02 20:53:39', 'propertyset_update_from_element', 'modTemplateVar', '13'),
(588, 1, '2016-07-02 20:53:49', 'tv_update', 'modTemplateVar', '12'),
(589, 1, '2016-07-02 20:53:49', 'propertyset_update_from_element', 'modTemplateVar', '12'),
(590, 1, '2016-07-02 20:54:47', 'tv_create', 'modTemplateVar', '14'),
(591, 1, '2016-07-02 20:57:30', 'tv_create', 'modTemplateVar', '15'),
(592, 1, '2016-07-02 20:58:39', 'tv_create', 'modTemplateVar', '16'),
(593, 1, '2016-07-02 21:00:25', 'tv_create', 'modTemplateVar', '17'),
(594, 1, '2016-07-02 21:00:36', 'tv_update', 'modTemplateVar', '14'),
(595, 1, '2016-07-02 21:00:37', 'propertyset_update_from_element', 'modTemplateVar', '14'),
(596, 1, '2016-07-02 21:00:50', 'tv_update', 'modTemplateVar', '15'),
(597, 1, '2016-07-02 21:00:50', 'propertyset_update_from_element', 'modTemplateVar', '15'),
(598, 1, '2016-07-02 21:00:56', 'tv_update', 'modTemplateVar', '16'),
(599, 1, '2016-07-02 21:00:57', 'propertyset_update_from_element', 'modTemplateVar', '16'),
(600, 1, '2016-07-02 21:01:18', 'tv_update', 'modTemplateVar', '13'),
(601, 1, '2016-07-02 21:01:18', 'propertyset_update_from_element', 'modTemplateVar', '13'),
(602, 1, '2016-07-02 21:03:09', 'tv_update', 'modTemplateVar', '17'),
(603, 1, '2016-07-02 21:03:09', 'propertyset_update_from_element', 'modTemplateVar', '17'),
(604, 1, '2016-07-02 21:08:19', 'tv_create', 'modTemplateVar', '18'),
(605, 1, '2016-07-02 21:11:00', 'tv_update', 'modTemplateVar', '18'),
(606, 1, '2016-07-02 21:11:01', 'propertyset_update_from_element', 'modTemplateVar', '18'),
(607, 1, '2016-07-02 21:11:41', 'tv_create', 'modTemplateVar', '19'),
(608, 1, '2016-07-02 21:12:10', 'tv_create', 'modTemplateVar', '20'),
(609, 1, '2016-07-02 21:12:31', 'tv_update', 'modTemplateVar', '20'),
(610, 1, '2016-07-02 21:12:31', 'propertyset_update_from_element', 'modTemplateVar', '20'),
(611, 1, '2016-07-02 21:12:45', 'tv_update', 'modTemplateVar', '20'),
(612, 1, '2016-07-02 21:12:45', 'propertyset_update_from_element', 'modTemplateVar', '20'),
(613, 1, '2016-07-02 21:13:23', 'tv_create', 'modTemplateVar', '21'),
(614, 1, '2016-07-02 21:15:33', 'tv_create', 'modTemplateVar', '22'),
(615, 1, '2016-07-02 21:16:15', 'tv_create', 'modTemplateVar', '23'),
(616, 1, '2016-07-02 21:16:40', 'tv_update', 'modTemplateVar', '23'),
(617, 1, '2016-07-02 21:16:40', 'propertyset_update_from_element', 'modTemplateVar', '23'),
(618, 1, '2016-07-02 21:21:24', 'tv_create', 'modTemplateVar', '24'),
(619, 1, '2016-07-02 21:21:55', 'tv_update', 'modTemplateVar', '24'),
(620, 1, '2016-07-02 21:21:55', 'propertyset_update_from_element', 'modTemplateVar', '24'),
(621, 1, '2016-07-02 21:23:27', 'tv_create', 'modTemplateVar', '25'),
(622, 1, '2016-07-02 21:24:47', 'tv_create', 'modTemplateVar', '26'),
(623, 1, '2016-07-02 21:25:09', 'tv_create', 'modTemplateVar', '27'),
(624, 1, '2016-07-02 21:25:37', 'tv_create', 'modTemplateVar', '28'),
(625, 1, '2016-07-02 21:26:06', 'tv_create', 'modTemplateVar', '29'),
(626, 1, '2016-07-02 21:26:41', 'tv_create', 'modTemplateVar', '30'),
(627, 1, '2016-07-02 21:27:32', 'tv_create', 'modTemplateVar', '31'),
(628, 1, '2016-07-02 21:27:43', 'tv_update', 'modTemplateVar', '31'),
(629, 1, '2016-07-02 21:27:43', 'propertyset_update_from_element', 'modTemplateVar', '31'),
(630, 1, '2016-07-02 21:29:44', 'tv_create', 'modTemplateVar', '32');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_media_sources`
--

CREATE TABLE `modx_media_sources` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text,
  `class_key` varchar(100) NOT NULL DEFAULT 'sources.modFileMediaSource',
  `properties` mediumtext,
  `is_stream` tinyint(1) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_media_sources`
--

INSERT INTO `modx_media_sources` (`id`, `name`, `description`, `class_key`, `properties`, `is_stream`) VALUES
(1, 'Filesystem', '', 'sources.modFileMediaSource', 'a:0:{}', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `modx_media_sources_contexts`
--

CREATE TABLE `modx_media_sources_contexts` (
  `source` int(11) NOT NULL DEFAULT '0',
  `context_key` varchar(100) NOT NULL DEFAULT 'web'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `modx_media_sources_elements`
--

CREATE TABLE `modx_media_sources_elements` (
  `source` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `object_class` varchar(100) NOT NULL DEFAULT 'modTemplateVar',
  `object` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `context_key` varchar(100) NOT NULL DEFAULT 'web'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_media_sources_elements`
--

INSERT INTO `modx_media_sources_elements` (`source`, `object_class`, `object`, `context_key`) VALUES
(1, 'modTemplateVar', 1, 'web'),
(1, 'modTemplateVar', 2, 'web'),
(1, 'modTemplateVar', 3, 'web'),
(1, 'modTemplateVar', 4, 'web'),
(1, 'modTemplateVar', 5, 'web'),
(1, 'modTemplateVar', 6, 'web'),
(1, 'modTemplateVar', 7, 'web'),
(1, 'modTemplateVar', 8, 'web'),
(1, 'modTemplateVar', 9, 'web'),
(1, 'modTemplateVar', 10, 'web'),
(1, 'modTemplateVar', 11, 'web'),
(1, 'modTemplateVar', 12, 'web'),
(1, 'modTemplateVar', 13, 'web'),
(1, 'modTemplateVar', 14, 'web'),
(1, 'modTemplateVar', 15, 'web'),
(1, 'modTemplateVar', 16, 'web'),
(1, 'modTemplateVar', 17, 'web'),
(1, 'modTemplateVar', 18, 'web'),
(1, 'modTemplateVar', 19, 'web'),
(1, 'modTemplateVar', 20, 'web'),
(1, 'modTemplateVar', 21, 'web'),
(1, 'modTemplateVar', 22, 'web'),
(1, 'modTemplateVar', 23, 'web'),
(1, 'modTemplateVar', 24, 'web'),
(1, 'modTemplateVar', 25, 'web'),
(1, 'modTemplateVar', 26, 'web'),
(1, 'modTemplateVar', 27, 'web'),
(1, 'modTemplateVar', 28, 'web'),
(1, 'modTemplateVar', 29, 'web'),
(1, 'modTemplateVar', 30, 'web'),
(1, 'modTemplateVar', 31, 'web'),
(1, 'modTemplateVar', 32, 'web');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_membergroup_names`
--

CREATE TABLE `modx_membergroup_names` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text,
  `parent` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `rank` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `dashboard` int(10) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_membergroup_names`
--

INSERT INTO `modx_membergroup_names` (`id`, `name`, `description`, `parent`, `rank`, `dashboard`) VALUES
(1, 'Administrator', NULL, 0, 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `modx_member_groups`
--

CREATE TABLE `modx_member_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_group` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `member` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `role` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `rank` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_member_groups`
--

INSERT INTO `modx_member_groups` (`id`, `user_group`, `member`, `role`, `rank`) VALUES
(1, 1, 1, 2, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `modx_menus`
--

CREATE TABLE `modx_menus` (
  `text` varchar(255) NOT NULL DEFAULT '',
  `parent` varchar(255) NOT NULL DEFAULT '',
  `action` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `icon` varchar(255) NOT NULL DEFAULT '',
  `menuindex` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `handler` text NOT NULL,
  `permissions` text NOT NULL,
  `namespace` varchar(100) NOT NULL DEFAULT 'core'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_menus`
--

INSERT INTO `modx_menus` (`text`, `parent`, `action`, `description`, `icon`, `menuindex`, `params`, `handler`, `permissions`, `namespace`) VALUES
('topnav', '', '', 'topnav_desc', '', 0, '', '', '', 'core'),
('site', 'topnav', '', '', '', 0, '', '', 'menu_site', 'core'),
('new_resource', 'site', 'resource/create', 'new_resource_desc', '', 0, '', '', 'new_document', 'core'),
('preview', 'site', '', 'preview_desc', '', 4, '', 'MODx.preview(); return false;', '', 'core'),
('import_site', 'site', 'system/import/html', 'import_site_desc', '', 5, '', '', 'import_static', 'core'),
('import_resources', 'site', 'system/import', 'import_resources_desc', '', 6, '', '', 'import_static', 'core'),
('resource_groups', 'site', 'security/resourcegroup', 'resource_groups_desc', '', 7, '', '', 'access_permissions', 'core'),
('content_types', 'site', 'system/contenttype', 'content_types_desc', '', 8, '', '', 'content_types', 'core'),
('media', 'topnav', '', 'media_desc', '', 1, '', '', 'file_manager', 'core'),
('file_browser', 'media', 'media/browser', 'file_browser_desc', '', 0, '', '', 'file_manager', 'core'),
('sources', 'media', 'source', 'sources_desc', '', 1, '', '', 'sources', 'core'),
('components', 'topnav', '', '', '', 2, '', '', 'components', 'core'),
('installer', 'components', 'workspaces', 'installer_desc', '', 0, '', '', 'packages', 'core'),
('manage', 'topnav', '', '', '', 3, '', '', 'menu_tools', 'core'),
('users', 'manage', 'security/user', 'user_management_desc', '', 0, '', '', 'view_user', 'core'),
('refresh_site', 'manage', '', 'refresh_site_desc', '', 1, '', 'MODx.clearCache(); return false;', 'empty_cache', 'core'),
('refreshuris', 'refresh_site', '', 'refreshuris_desc', '', 0, '', 'MODx.refreshURIs(); return false;', 'empty_cache', 'core'),
('remove_locks', 'manage', '', 'remove_locks_desc', '', 2, '', '\nMODx.msg.confirm({\n    title: _(''remove_locks'')\n    ,text: _(''confirm_remove_locks'')\n    ,url: MODx.config.connectors_url\n    ,params: {\n        action: ''system/remove_locks''\n    }\n    ,listeners: {\n        ''success'': {fn:function() {\n            var tree = Ext.getCmp("modx-resource-tree");\n            if (tree && tree.rendered) {\n                tree.refresh();\n            }\n         },scope:this}\n    }\n});', 'remove_locks', 'core'),
('flush_access', 'manage', '', 'flush_access_desc', '', 3, '', 'MODx.msg.confirm({\n    title: _(''flush_access'')\n    ,text: _(''flush_access_confirm'')\n    ,url: MODx.config.connector_url\n    ,params: {\n        action: ''security/access/flush''\n    }\n    ,listeners: {\n        ''success'': {fn:function() { location.href = ''./''; },scope:this}\n    }\n});', 'access_permissions', 'core'),
('flush_sessions', 'manage', '', 'flush_sessions_desc', '', 4, '', 'MODx.msg.confirm({\n    title: _(''flush_sessions'')\n    ,text: _(''flush_sessions_confirm'')\n    ,url: MODx.config.connector_url\n    ,params: {\n        action: ''security/flush''\n    }\n    ,listeners: {\n        ''success'': {fn:function() { location.href = ''./''; },scope:this}\n    }\n});', 'flush_sessions', 'core'),
('reports', 'manage', '', 'reports_desc', '', 5, '', '', 'menu_reports', 'core'),
('site_schedule', 'reports', 'resource/site_schedule', 'site_schedule_desc', '', 0, '', '', 'view_document', 'core'),
('view_logging', 'reports', 'system/logs', 'view_logging_desc', '', 1, '', '', 'logs', 'core'),
('eventlog_viewer', 'reports', 'system/event', 'eventlog_viewer_desc', '', 2, '', '', 'view_eventlog', 'core'),
('view_sysinfo', 'reports', 'system/info', 'view_sysinfo_desc', '', 3, '', '', 'view_sysinfo', 'core'),
('usernav', '', '', 'usernav_desc', '', 0, '', '', '', 'core'),
('user', 'usernav', '', '', '<span id="user-avatar">{$userImage}</span> <span id="user-username">{$username}</span>', 5, '', '', 'menu_user', 'core'),
('profile', 'user', 'security/profile', 'profile_desc', '', 0, '', '', 'change_profile', 'core'),
('messages', 'user', 'security/message', 'messages_desc', '', 1, '', '', 'messages', 'core'),
('logout', 'user', 'security/logout', 'logout_desc', '', 2, '', 'MODx.logout(); return false;', 'logout', 'core'),
('admin', 'usernav', '', '', '<i class="icon-gear icon icon-large"></i>', 6, '', '', 'settings', 'core'),
('system_settings', 'admin', 'system/settings', 'system_settings_desc', '', 0, '', '', 'settings', 'core'),
('bespoke_manager', 'admin', 'security/forms', 'bespoke_manager_desc', '', 1, '', '', 'customize_forms', 'core'),
('dashboards', 'admin', 'system/dashboards', 'dashboards_desc', '', 2, '', '', 'dashboards', 'core'),
('contexts', 'admin', 'context', 'contexts_desc', '', 3, '', '', 'view_context', 'core'),
('edit_menu', 'admin', 'system/action', 'edit_menu_desc', '', 4, '', '', 'actions', 'core'),
('acls', 'admin', 'security/permission', 'acls_desc', '', 5, '', '', 'access_permissions', 'core'),
('propertysets', 'admin', 'element/propertyset', 'propertysets_desc', '', 6, '', '', 'property_sets', 'core'),
('lexicon_management', 'admin', 'workspaces/lexicon', 'lexicon_management_desc', '', 7, '', '', 'lexicons', 'core'),
('namespaces', 'admin', 'workspaces/namespace', 'namespaces_desc', '', 8, '', '', 'namespaces', 'core'),
('about', 'usernav', 'help', '', '<i class="icon-question-circle icon icon-large"></i>', 7, '', '', 'help', 'core'),
('tagger.menu.tagger', 'components', '1', 'tagger.menu.tagger_desc', '', 0, '', '', '', 'core'),
('gallery', 'components', '2', 'gallery.menu_desc', 'images/icons/plugin.gif', 0, '', '', '', 'core');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_namespaces`
--

CREATE TABLE `modx_namespaces` (
  `name` varchar(40) NOT NULL DEFAULT '',
  `path` text,
  `assets_path` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_namespaces`
--

INSERT INTO `modx_namespaces` (`name`, `path`, `assets_path`) VALUES
('core', '{core_path}', '{assets_path}'),
('ace', '{core_path}components/ace/', ''),
('breadcrumbs', '{core_path}components/breadcrumbs/', ''),
('ckeditor', '{core_path}components/ckeditor/', ''),
('filetranslit', '{core_path}components/filetranslit/', ''),
('tagger', '{core_path}components/tagger/', '{assets_path}components/tagger/'),
('translit', '{core_path}components/translit/', ''),
('getresourcefield', '{core_path}components/getresourcefield/', ''),
('wayfinder', '{core_path}components/wayfinder/', ''),
('gallery', '{core_path}components/gallery/', '/Users/theboxer/www/modx/pkgs/gallery/assets/components/gallery/');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_property_set`
--

CREATE TABLE `modx_property_set` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT '',
  `category` int(10) NOT NULL DEFAULT '0',
  `description` varchar(255) NOT NULL DEFAULT '',
  `properties` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `modx_register_messages`
--

CREATE TABLE `modx_register_messages` (
  `topic` int(10) UNSIGNED NOT NULL,
  `id` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `valid` datetime NOT NULL,
  `accessed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `accesses` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `expires` int(20) NOT NULL DEFAULT '0',
  `payload` mediumtext NOT NULL,
  `kill` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `modx_register_queues`
--

CREATE TABLE `modx_register_queues` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `options` mediumtext
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_register_queues`
--

INSERT INTO `modx_register_queues` (`id`, `name`, `options`) VALUES
(1, 'locks', 'a:1:{s:9:"directory";s:5:"locks";}'),
(2, 'resource_reload', 'a:1:{s:9:"directory";s:15:"resource_reload";}');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_register_topics`
--

CREATE TABLE `modx_register_topics` (
  `id` int(10) UNSIGNED NOT NULL,
  `queue` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `options` mediumtext
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_register_topics`
--

INSERT INTO `modx_register_topics` (`id`, `queue`, `name`, `created`, `updated`, `options`) VALUES
(1, 1, '/resource/', '2016-06-08 13:32:52', NULL, NULL),
(2, 2, '/resourcereload/', '2016-06-08 14:49:05', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `modx_session`
--

CREATE TABLE `modx_session` (
  `id` varchar(255) NOT NULL DEFAULT '',
  `access` int(20) UNSIGNED NOT NULL,
  `data` mediumtext
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_session`
--

INSERT INTO `modx_session` (`id`, `access`, `data`) VALUES
('56548e94ff6ec35a96acaf7abcfca705', 1466785515, 'modx.user.0.resourceGroups|a:1:{s:3:"web";a:0:{}}modx.user.0.attributes|a:1:{s:3:"web";a:5:{s:16:"modAccessContext";a:1:{s:3:"web";a:1:{i:0;a:3:{s:9:"principal";i:0;s:9:"authority";s:1:"0";s:6:"policy";a:1:{s:4:"load";b:1;}}}}s:22:"modAccessResourceGroup";a:0:{}s:17:"modAccessCategory";a:0:{}s:28:"sources.modAccessMediaSource";a:0:{}s:18:"modAccessNamespace";a:0:{}}}modx.user.contextTokens|a:1:{s:3:"mgr";i:1;}modx.mgr.user.token|s:52:"modx5757f2c76ab4f3.63592797_1576d0343516681.09607884";modx.mgr.session.cookie.lifetime|i:0;modx.mgr.user.config|a:0:{}newResourceTokens|a:13:{i:0;s:23:"576d0352844770.42880990";i:1;s:23:"576d035b217322.27494498";i:2;s:23:"576d447888e1a0.47999764";i:3;s:23:"576d44d33efee9.81711726";i:4;s:23:"576d44d787b420.97866746";i:5;s:23:"576d55a0d0fe28.08885671";i:6;s:23:"576d55b37006e4.81831248";i:7;s:23:"576d5629738a40.32598406";i:8;s:23:"576d574d3a51b7.80694611";i:9;s:23:"576d575edd5220.86777869";i:10;s:23:"576d57b6946e01.66833444";i:11;s:23:"576d5af0df5f84.57392618";i:12;s:23:"576d5eebb47629.48662642";}'),
('np3g01kq9562lkereeik400u77', 1467045290, 'modx.user.contextTokens|a:0:{}modx.user.0.resourceGroups|a:1:{s:3:"web";a:0:{}}modx.user.0.attributes|a:1:{s:3:"web";a:5:{s:16:"modAccessContext";a:1:{s:3:"web";a:1:{i:0;a:3:{s:9:"principal";i:0;s:9:"authority";s:1:"0";s:6:"policy";a:1:{s:4:"load";b:1;}}}}s:22:"modAccessResourceGroup";a:0:{}s:17:"modAccessCategory";a:0:{}s:28:"sources.modAccessMediaSource";a:0:{}s:18:"modAccessNamespace";a:0:{}}}'),
('i1et04d9ijphob608qe7hj0196', 1467048177, 'modx.user.contextTokens|a:0:{}modx.user.0.resourceGroups|a:1:{s:3:"web";a:0:{}}modx.user.0.attributes|a:1:{s:3:"web";a:5:{s:16:"modAccessContext";a:1:{s:3:"web";a:1:{i:0;a:3:{s:9:"principal";i:0;s:9:"authority";s:1:"0";s:6:"policy";a:1:{s:4:"load";b:1;}}}}s:22:"modAccessResourceGroup";a:0:{}s:17:"modAccessCategory";a:0:{}s:28:"sources.modAccessMediaSource";a:0:{}s:18:"modAccessNamespace";a:0:{}}}'),
('hhim2agfe3gvoer0m6133jblq4', 1467054710, 'modx.user.0.resourceGroups|a:1:{s:3:"web";a:0:{}}modx.user.0.attributes|a:1:{s:3:"web";a:5:{s:16:"modAccessContext";a:1:{s:3:"web";a:1:{i:0;a:3:{s:9:"principal";i:0;s:9:"authority";s:1:"0";s:6:"policy";a:1:{s:4:"load";b:1;}}}}s:22:"modAccessResourceGroup";a:0:{}s:17:"modAccessCategory";a:0:{}s:28:"sources.modAccessMediaSource";a:0:{}s:18:"modAccessNamespace";a:0:{}}}modx.user.contextTokens|a:0:{}'),
('kgc7n6tillffnh7aj893ljs2n7', 1467054711, 'modx.user.0.resourceGroups|a:1:{s:3:"web";a:0:{}}modx.user.0.attributes|a:1:{s:3:"web";a:5:{s:16:"modAccessContext";a:1:{s:3:"web";a:1:{i:0;a:3:{s:9:"principal";i:0;s:9:"authority";s:1:"0";s:6:"policy";a:1:{s:4:"load";b:1;}}}}s:22:"modAccessResourceGroup";a:0:{}s:17:"modAccessCategory";a:0:{}s:28:"sources.modAccessMediaSource";a:0:{}s:18:"modAccessNamespace";a:0:{}}}modx.user.contextTokens|a:0:{}'),
('i4lo5ut8rovghvolee4ah79pa3', 1467130791, 'modx.user.0.resourceGroups|a:1:{s:3:"web";a:0:{}}modx.user.0.attributes|a:1:{s:3:"web";a:5:{s:16:"modAccessContext";a:1:{s:3:"web";a:1:{i:0;a:3:{s:9:"principal";i:0;s:9:"authority";s:1:"0";s:6:"policy";a:1:{s:4:"load";b:1;}}}}s:22:"modAccessResourceGroup";a:0:{}s:17:"modAccessCategory";a:0:{}s:28:"sources.modAccessMediaSource";a:0:{}s:18:"modAccessNamespace";a:0:{}}}modx.user.contextTokens|a:0:{}'),
('orf31kgu5ava4f15d1s4p2q3j1', 1467482509, 'modx.user.0.resourceGroups|a:1:{s:3:"web";a:0:{}}modx.user.0.attributes|a:1:{s:3:"web";a:5:{s:16:"modAccessContext";a:1:{s:3:"web";a:1:{i:0;a:3:{s:9:"principal";i:0;s:9:"authority";s:1:"0";s:6:"policy";a:1:{s:4:"load";b:1;}}}}s:22:"modAccessResourceGroup";a:0:{}s:17:"modAccessCategory";a:0:{}s:28:"sources.modAccessMediaSource";a:0:{}s:18:"modAccessNamespace";a:0:{}}}modx.user.contextTokens|a:1:{s:3:"mgr";i:1;}modx.mgr.user.token|s:52:"modx5757f2c76ab4f3.63592797_15777889f23a1a5.06608628";modx.mgr.session.cookie.lifetime|i:604800;modx.mgr.user.config|a:0:{}newResourceTokens|a:16:{i:0;s:23:"577788bd666534.21569295";i:1;s:23:"5777966cb1e1f2.42735153";i:2;s:23:"577796755f0495.69920114";i:3;s:23:"5777967c4bfdc8.38462467";i:4;s:23:"57779682c2b298.27775193";i:5;s:23:"5777adf258b756.52427399";i:6;s:23:"5777b1eae7bdf7.37011888";i:7;s:23:"5777f9f82a1a49.34577501";i:8;s:23:"5777fe70742c48.82808733";i:9;s:23:"5777ff320fe152.67846975";i:10;s:23:"5777ff3279d333.21492758";i:11;s:23:"5777ff32e0df35.56000479";i:12;s:23:"5777ff334eb5c5.43822648";i:13;s:23:"57780110b37c13.08273202";i:14;s:23:"57780124801d64.33141943";i:15;s:23:"5778018de3ad74.46996269";}');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_site_content`
--

CREATE TABLE `modx_site_content` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(20) NOT NULL DEFAULT 'document',
  `contentType` varchar(50) NOT NULL DEFAULT 'text/html',
  `pagetitle` varchar(255) NOT NULL DEFAULT '',
  `longtitle` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) DEFAULT '',
  `link_attributes` varchar(255) NOT NULL DEFAULT '',
  `published` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `pub_date` int(20) NOT NULL DEFAULT '0',
  `unpub_date` int(20) NOT NULL DEFAULT '0',
  `parent` int(10) NOT NULL DEFAULT '0',
  `isfolder` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `introtext` text,
  `content` mediumtext,
  `richtext` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `template` int(10) NOT NULL DEFAULT '0',
  `menuindex` int(10) NOT NULL DEFAULT '0',
  `searchable` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `cacheable` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `createdby` int(10) NOT NULL DEFAULT '0',
  `createdon` int(20) NOT NULL DEFAULT '0',
  `editedby` int(10) NOT NULL DEFAULT '0',
  `editedon` int(20) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `deletedon` int(20) NOT NULL DEFAULT '0',
  `deletedby` int(10) NOT NULL DEFAULT '0',
  `publishedon` int(20) NOT NULL DEFAULT '0',
  `publishedby` int(10) NOT NULL DEFAULT '0',
  `menutitle` varchar(255) NOT NULL DEFAULT '',
  `donthit` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `privateweb` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `privatemgr` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `content_dispo` tinyint(1) NOT NULL DEFAULT '0',
  `hidemenu` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `class_key` varchar(100) NOT NULL DEFAULT 'modDocument',
  `context_key` varchar(100) NOT NULL DEFAULT 'web',
  `content_type` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `uri` text,
  `uri_override` tinyint(1) NOT NULL DEFAULT '0',
  `hide_children_in_tree` tinyint(1) NOT NULL DEFAULT '0',
  `show_in_tree` tinyint(1) NOT NULL DEFAULT '1',
  `properties` mediumtext
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_site_content`
--

INSERT INTO `modx_site_content` (`id`, `type`, `contentType`, `pagetitle`, `longtitle`, `description`, `alias`, `link_attributes`, `published`, `pub_date`, `unpub_date`, `parent`, `isfolder`, `introtext`, `content`, `richtext`, `template`, `menuindex`, `searchable`, `cacheable`, `createdby`, `createdon`, `editedby`, `editedon`, `deleted`, `deletedon`, `deletedby`, `publishedon`, `publishedby`, `menutitle`, `donthit`, `privateweb`, `privatemgr`, `content_dispo`, `hidemenu`, `class_key`, `context_key`, `content_type`, `uri`, `uri_override`, `hide_children_in_tree`, `show_in_tree`, `properties`) VALUES
(1, 'document', 'text/html', 'Главная', 'Поздравляем!', '', 'index', '', 1, 0, 0, 0, 0, '', '[[!getResources? &tpl=`home.catalog_item` &limit=`8` &depth=`3` &parents=`2` &includeTVs=`1` &processTVs=`1` &where=`[[!TaggerGetResourcesWhere? &where=`{"template":2}` &tags=`2016`]]`]]', 1, 1, 0, 1, 1, 1, 1465381589, 1, 1465407610, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'index.html', 0, 0, 1, NULL),
(2, 'document', 'text/html', 'Каталог', '', '', 'catalog', '', 1, 0, 0, 0, 1, '', '', 1, 3, 1, 1, 1, 1, 1465383712, 1, 1465395058, 0, 0, 0, 1465383660, 1, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'catalog/', 0, 0, 1, NULL),
(3, 'document', 'text/html', 'Папа Джонс', '', '', 'papa-dzhons', '', 1, 0, 0, 13, 0, '', '', 1, 2, 4, 1, 1, 1, 1465387012, 1, 1465407527, 0, 0, 0, 1465386960, 1, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'catalog/obshhestvennoe-pitanie/piczczeriya/papa-dzhons.html', 0, 0, 1, NULL),
(4, 'document', 'text/html', 'Пират пицца', '', '', 'pirat-piczcza', '', 1, 0, 0, 13, 0, '', '', 1, 2, 3, 1, 1, 1, 1465387099, 1, 1465469667, 0, 0, 0, 1465386960, 1, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'catalog/obshhestvennoe-pitanie/piczczeriya/pirat-piczcza.html', 0, 0, 1, NULL),
(5, 'document', 'text/html', 'Сити Пицца', '', '', 'siti-piczcza', '', 1, 0, 0, 13, 0, '', '', 1, 2, 2, 1, 1, 1, 1465387126, 1, 1465469498, 0, 0, 0, 1465386960, 1, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'catalog/obshhestvennoe-pitanie/piczczeriya/siti-piczcza.html', 0, 0, 1, NULL),
(6, 'document', 'text/html', 'Додо Пицца', '', '', 'dodo-piczcza', '', 1, 0, 0, 13, 0, '', '', 1, 2, 1, 1, 1, 1, 1465387140, 1, 1465468851, 0, 0, 0, 1465386960, 1, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'catalog/obshhestvennoe-pitanie/piczczeriya/dodo-piczcza.html', 0, 0, 1, NULL),
(7, 'document', 'text/html', 'Jeffrey''s Coffe', '', '', 'jeffreys-coffe', '', 1, 0, 0, 12, 0, '', '', 1, 2, 2, 1, 1, 1, 1465387189, 1, 1465471678, 0, 0, 0, 1465386960, 1, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'catalog/obshhestvennoe-pitanie/kofejni/jeffreys-coffe.html', 0, 0, 1, NULL),
(8, 'document', 'text/html', 'Take and Wake Coffe', '', '', 'take-and-wake-coffe', '', 1, 0, 0, 12, 0, '', '', 1, 2, 1, 1, 1, 1, 1465387247, 1, 1465469855, 0, 0, 0, 1465386960, 1, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'catalog/obshhestvennoe-pitanie/kofejni/take-and-wake-coffe.html', 0, 0, 1, NULL),
(9, 'document', 'text/html', 'Чайбург', '', '', 'chajburg', '', 1, 0, 0, 12, 0, '', '', 1, 2, 0, 1, 1, 1, 1465387280, 1, 1466785538, 0, 0, 0, 1465386960, 1, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'catalog/obshhestvennoe-pitanie/kofejni/chajburg.html', 0, 0, 1, NULL),
(10, 'document', 'text/html', 'Пиццерия То-То', '', '', 'piczczeriya-to-to', '', 1, 0, 0, 13, 0, '', '', 1, 2, 0, 1, 1, 1, 1465388141, 1, 1465470273, 0, 0, 0, 1465386960, 1, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'catalog/obshhestvennoe-pitanie/piczczeriya/piczczeriya-to-to.html', 0, 0, 1, NULL),
(11, 'document', 'text/html', 'Общественное питание', '', '', 'obshhestvennoe-pitanie', '', 1, 0, 0, 2, 1, '', '', 1, 0, 8, 1, 1, 1, 1465388541, 0, 0, 0, 0, 0, 1465388541, 1, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'catalog/obshhestvennoe-pitanie/', 0, 0, 1, NULL),
(12, 'document', 'text/html', 'Кофейни', '', '', 'kofejni', '', 1, 0, 0, 11, 0, '', '', 1, 0, 0, 1, 1, 1, 1465388578, 1, 1465388680, 0, 0, 0, 1465388520, 1, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'catalog/obshhestvennoe-pitanie/kofejni.html', 0, 0, 1, NULL),
(13, 'document', 'text/html', 'Пиццерия', '', '', 'piczczeriya', '', 1, 0, 0, 11, 0, '', '', 1, 0, 1, 1, 1, 1, 1465388634, 1, 1465388685, 0, 0, 0, 1465388580, 1, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'catalog/obshhestvennoe-pitanie/piczczeriya.html', 0, 0, 1, NULL),
(14, 'document', 'text/html', 'Подборки', '', '', 'podborki', '', 1, 0, 0, 0, 1, '', '', 1, 0, 2, 1, 1, 1, 1465397221, 0, 0, 0, 0, 0, 1465397221, 1, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'podborki/', 0, 0, 1, NULL),
(15, 'document', 'text/html', 'Франшизы 2016', '', '', 'franshizyi-2016', '', 1, 0, 0, 14, 0, '', '[[!$contentCatalog?]]', 1, 3, 0, 1, 1, 1, 1465397255, 1, 1465485628, 0, 0, 0, 1465397220, 1, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'podborki/franshizyi-2016.html', 0, 0, 1, NULL),
(16, 'document', 'text/html', 'Топ-10 франшиз', '', '', 'top-10-franshiz', '', 1, 0, 0, 14, 0, '', '', 1, 3, 0, 1, 1, 1, 1465397304, 0, 0, 0, 0, 0, 1465397267, 1, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'podborki/top-10-franshiz.html', 0, 0, 1, NULL),
(17, 'document', 'text/html', 'Для малого бизнеса', '', '', 'dlya-malogo-biznesa', '', 1, 0, 0, 14, 0, '', '', 1, 3, 0, 1, 1, 1, 1465397348, 1, 1465397386, 0, 0, 0, 1465397220, 1, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'podborki/dlya-malogo-biznesa.html', 0, 0, 1, NULL),
(18, 'document', 'text/html', 'Франшиза до 100', '', '', 'franshiza-do-100', '', 1, 0, 0, 14, 0, '', '', 1, 3, 0, 1, 1, 1, 1465397410, 0, 0, 0, 0, 0, 1465397220, 1, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'podborki/franshiza-do-100.html', 0, 0, 1, NULL),
(19, 'document', 'text/html', 'Для женщин', '', '', 'dlya-zhenshhin', '', 1, 0, 0, 14, 0, '', '', 1, 3, 0, 1, 1, 1, 1465397466, 0, 0, 0, 0, 0, 1465397220, 1, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'podborki/dlya-zhenshhin.html', 0, 0, 1, NULL),
(20, 'document', 'text/html', '', '', '', '', '', 0, 0, 0, 0, 0, NULL, NULL, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, NULL, 0, 0, 1, NULL),
(1001, 'document', 'text/html', 'Франшиза в торговле', '', '', 'fransiza-v-torgovle', '', 1, 0, 0, 2, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'fransiza-v-torgovle/', 0, 0, 1, NULL),
(1100, 'document', 'text/html', 'Одежда', '', '', 'odezda', '', 1, 0, 0, 1001, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'odezda/', 0, 0, 1, NULL),
(1101, 'document', 'text/html', 'Кондитерские изделия', '', '', 'konditerskie-izdelia', '', 1, 0, 0, 1001, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'konditerskie-izdelia/', 0, 0, 1, NULL),
(1102, 'document', 'text/html', 'Подарки', '', '', 'podarki', '', 1, 0, 0, 1001, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'podarki/', 0, 0, 1, NULL),
(1103, 'document', 'text/html', 'Автоаксессуары', '', '', 'avtoaksessuary', '', 1, 0, 0, 1001, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'avtoaksessuary/', 0, 0, 1, NULL),
(1104, 'document', 'text/html', 'Сумки и чемоданы', '', '', 'sumki-i-cemodany', '', 1, 0, 0, 1001, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'sumki-i-cemodany/', 0, 0, 1, NULL),
(1105, 'document', 'text/html', 'Нижнее белье', '', '', 'niznee-belʹe', '', 1, 0, 0, 1001, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'niznee-belʹe/', 0, 0, 1, NULL),
(1106, 'document', 'text/html', 'Оптика', '', '', 'optika', '', 1, 0, 0, 1001, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'optika/', 0, 0, 1, NULL),
(1107, 'document', 'text/html', 'Товары для детей', '', '', 'tovary-dla-detej', '', 1, 0, 0, 1001, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'tovary-dla-detej/', 0, 0, 1, NULL),
(1108, 'document', 'text/html', 'Канцтовары', '', '', 'kanctovary', '', 1, 0, 0, 1001, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'kanctovary/', 0, 0, 1, NULL),
(1109, 'document', 'text/html', 'Продукты питания', '', '', 'produkty-pitania', '', 1, 0, 0, 1001, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'produkty-pitania/', 0, 0, 1, NULL),
(1110, 'document', 'text/html', 'Строительные материалы', '', '', 'stroitelʹnye-materialy', '', 1, 0, 0, 1001, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'stroitelʹnye-materialy/', 0, 0, 1, NULL),
(1111, 'document', 'text/html', 'Обувь', '', '', 'obuvʹ', '', 1, 0, 0, 1001, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'obuvʹ/', 0, 0, 1, NULL),
(1112, 'document', 'text/html', 'Ювелирные изделия', '', '', 'uvelirnye-izdelia', '', 1, 0, 0, 1001, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'uvelirnye-izdelia/', 0, 0, 1, NULL),
(1113, 'document', 'text/html', 'Автозапчасти', '', '', 'avtozapcasti', '', 1, 0, 0, 1001, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'avtozapcasti/', 0, 0, 1, NULL),
(1114, 'document', 'text/html', 'Косметика и парфюмерия', '', '', 'kosmetika-i-parfumeria', '', 1, 0, 0, 1001, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'kosmetika-i-parfumeria/', 0, 0, 1, NULL),
(1115, 'document', 'text/html', 'Животные', '', '', 'zivotnye', '', 1, 0, 0, 1001, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'zivotnye/', 0, 0, 1, NULL),
(1116, 'document', 'text/html', 'Мебель', '', '', 'mebelʹ', '', 1, 0, 0, 1001, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'mebelʹ/', 0, 0, 1, NULL),
(1117, 'document', 'text/html', 'Оборудование и инструменты', '', '', 'oborudovanie-i-instrumenty', '', 1, 0, 0, 1001, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'oborudovanie-i-instrumenty/', 0, 0, 1, NULL),
(1002, 'document', 'text/html', 'Франшиза в сфере услуг', '', '', 'fransiza-v-sfere-uslug', '', 1, 0, 0, 2, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'fransiza-v-sfere-uslug/', 0, 0, 1, NULL),
(1200, 'document', 'text/html', 'Обучение и образование', '', '', 'obucenie-i-obrazovanie', '', 1, 0, 0, 1002, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'obucenie-i-obrazovanie/', 0, 0, 1, NULL),
(1201, 'document', 'text/html', 'Салоны красоты', '', '', 'salony-krasoty', '', 1, 0, 0, 1002, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'salony-krasoty/', 0, 0, 1, NULL),
(1202, 'document', 'text/html', 'Гостиничный бизнес', '', '', 'gostinicnyj-biznes', '', 1, 0, 0, 1002, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'gostinicnyj-biznes/', 0, 0, 1, NULL),
(1203, 'document', 'text/html', 'Медицина и здоровье', '', '', 'medicina-i-zdorovʹe', '', 1, 0, 0, 1002, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'medicina-i-zdorovʹe/', 0, 0, 1, NULL),
(1204, 'document', 'text/html', 'Фитнес-клуб', '', '', 'fitnes-klub', '', 1, 0, 0, 1002, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'fitnes-klub/', 0, 0, 1, NULL),
(1205, 'document', 'text/html', 'Детский центр', '', '', 'detskij-centr', '', 1, 0, 0, 1002, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'detskij-centr/', 0, 0, 1, NULL),
(1206, 'document', 'text/html', 'Развлечения и досуг', '', '', 'razvlecenia-i-dosug', '', 1, 0, 0, 1002, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'razvlecenia-i-dosug/', 0, 0, 1, NULL),
(1207, 'document', 'text/html', 'Реклама', '', '', 'reklama', '', 1, 0, 0, 1002, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'reklama/', 0, 0, 1, NULL),
(1208, 'document', 'text/html', 'Автосервисы, СТО', '', '', 'avtoservisy-sto', '', 1, 0, 0, 1002, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'avtoservisy-sto/', 0, 0, 1, NULL),
(1209, 'document', 'text/html', 'Такси, грузоперевозки', '', '', 'taksi-gruzoperevozki', '', 1, 0, 0, 1002, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'taksi-gruzoperevozki/', 0, 0, 1, NULL),
(1210, 'document', 'text/html', 'Шоу', '', '', 'sou', '', 1, 0, 0, 1002, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'sou/', 0, 0, 1, NULL),
(1211, 'document', 'text/html', 'Квесты', '', '', 'kvesty', '', 1, 0, 0, 1002, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'kvesty/', 0, 0, 1, NULL),
(1212, 'document', 'text/html', 'Спорт', '', '', 'sport', '', 1, 0, 0, 1002, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'sport/', 0, 0, 1, NULL),
(1213, 'document', 'text/html', 'Тиры', '', '', 'tiry', '', 1, 0, 0, 1002, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'tiry/', 0, 0, 1, NULL),
(1214, 'document', 'text/html', 'Доставка еды', '', '', 'dostavka-edy', '', 1, 0, 0, 1002, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'dostavka-edy/', 0, 0, 1, NULL),
(1215, 'document', 'text/html', 'Стоматология', '', '', 'stomatologia', '', 1, 0, 0, 1002, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'stomatologia/', 0, 0, 1, NULL),
(1216, 'document', 'text/html', 'Автомойки', '', '', 'avtomojki', '', 1, 0, 0, 1002, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'avtomojki/', 0, 0, 1, NULL),
(1217, 'document', 'text/html', 'Интернет, IT', '', '', 'internet-it', '', 1, 0, 0, 1002, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'internet-it/', 0, 0, 1, NULL),
(1218, 'document', 'text/html', 'Недвижимость', '', '', 'nedvizimostʹ', '', 1, 0, 0, 1002, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'nedvizimostʹ/', 0, 0, 1, NULL),
(1219, 'document', 'text/html', 'Юридические услуги', '', '', 'uridiceskie-uslugi', '', 1, 0, 0, 1002, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'uridiceskie-uslugi/', 0, 0, 1, NULL),
(1220, 'document', 'text/html', 'Финансы / кредитование', '', '', 'finansy-kreditovanie', '', 1, 0, 0, 1002, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'finansy-kreditovanie/', 0, 0, 1, NULL),
(1221, 'document', 'text/html', 'Строительство и ремонт', '', '', 'stroitelʹstvo-i-remont', '', 1, 0, 0, 1002, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'stroitelʹstvo-i-remont/', 0, 0, 1, NULL),
(1222, 'document', 'text/html', 'Курьерские службы доставки', '', '', 'kurʹerskie-sluzby-dostavki', '', 1, 0, 0, 1002, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'kurʹerskie-sluzby-dostavki/', 0, 0, 1, NULL),
(1003, 'document', 'text/html', 'Франшиза общественного питания', '', '', 'fransiza-obsestvennogo-pitania', '', 1, 0, 0, 2, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'fransiza-obsestvennogo-pitania/', 0, 0, 1, NULL),
(1300, 'document', 'text/html', 'Кафе быстрого питания', '', '', 'kafe-bystrogo-pitania', '', 1, 0, 0, 1003, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'kafe-bystrogo-pitania/', 0, 0, 1, NULL),
(1301, 'document', 'text/html', 'Кофейни', '', '', 'kofejni', '', 1, 0, 0, 1003, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'kofejni/', 0, 0, 1, NULL),
(1302, 'document', 'text/html', 'Рестораны', '', '', 'restorany', '', 1, 0, 0, 1003, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'restorany/', 0, 0, 1, NULL),
(1303, 'document', 'text/html', 'Пиццерии', '', '', 'piccerii', '', 1, 0, 0, 1003, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'piccerii/', 0, 0, 1, NULL),
(1304, 'document', 'text/html', 'Пивные', '', '', 'pivnye', '', 1, 0, 0, 1003, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'pivnye/', 0, 0, 1, NULL),
(1004, 'document', 'text/html', 'Франшиза в производстве', '', '', 'fransiza-v-proizvodstve', '', 1, 0, 0, 2, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'fransiza-v-proizvodstve/', 0, 0, 1, NULL),
(2000, 'document', 'text/html', 'Автобизнес', '', '', 'avtobiznes', '', 1, 0, 0, 2, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'avtobiznes/', 0, 0, 1, NULL),
(2001, 'document', 'text/html', 'Товары и услуги для детей', '', '', 'tovary-i-uslugi-dla-detej', '', 1, 0, 0, 2, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'tovary-i-uslugi-dla-detej/', 0, 0, 1, NULL),
(2002, 'document', 'text/html', 'Издательский бизнес и СМИ', '', '', 'izdatelʹskij-biznes-i-smi', '', 1, 0, 0, 2, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'izdatelʹskij-biznes-i-smi/', 0, 0, 1, NULL),
(2003, 'document', 'text/html', 'Инновационные товары, технологии и Производство', '', '', 'innovacionnye-tovary-tehnologii-i-proizvodstvo', '', 1, 0, 0, 2, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'innovacionnye-tovary-tehnologii-i-proizvodstvo/', 0, 0, 1, NULL),
(2004, 'document', 'text/html', 'Рекламные услуги', '', '', 'reklamnye-uslugi', '', 1, 0, 0, 2, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'reklamnye-uslugi/', 0, 0, 1, NULL),
(2005, 'document', 'text/html', 'Красота и Здоровье', '', '', 'krasota-i-zdorovʹe', '', 1, 0, 0, 2, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'krasota-i-zdorovʹe/', 0, 0, 1, NULL),
(2006, 'document', 'text/html', 'Недвижимость', '', '', 'nedvizimostʹ', '', 1, 0, 0, 2, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'nedvizimostʹ/', 0, 0, 1, NULL),
(2007, 'document', 'text/html', 'Одежда и Обувь', '', '', 'odezda-i-obuvʹ', '', 1, 0, 0, 2, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'odezda-i-obuvʹ/', 0, 0, 1, NULL),
(2008, 'document', 'text/html', 'Общественное питание и продукты', '', '', 'obsestvennoe-pitanie-i-produkty', '', 1, 0, 0, 2, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'obsestvennoe-pitanie-i-produkty/', 0, 0, 1, NULL),
(2009, 'document', 'text/html', 'Образование', '', '', 'obrazovanie', '', 1, 0, 0, 2, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'obrazovanie/', 0, 0, 1, NULL),
(2010, 'document', 'text/html', 'Развлечения', '', '', 'razvlecenia', '', 1, 0, 0, 2, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'razvlecenia/', 0, 0, 1, NULL),
(2011, 'document', 'text/html', 'Ритейл', '', '', 'ritejl', '', 1, 0, 0, 2, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'ritejl/', 0, 0, 1, NULL),
(2012, 'document', 'text/html', 'Строительные материалы', '', '', 'stroitelʹnye-materialy', '', 1, 0, 0, 2, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'stroitelʹnye-materialy/', 0, 0, 1, NULL),
(2013, 'document', 'text/html', 'Сфера услуг', '', '', 'sfera-uslug', '', 1, 0, 0, 2, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'sfera-uslug/', 0, 0, 1, NULL),
(2014, 'document', 'text/html', 'Финансы', '', '', 'finansy', '', 1, 0, 0, 2, 1, NULL, NULL, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 'modDocument', 'web', 1, 'finansy/', 0, 0, 1, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `modx_site_htmlsnippets`
--

CREATE TABLE `modx_site_htmlsnippets` (
  `id` int(10) UNSIGNED NOT NULL,
  `source` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `property_preprocess` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT 'Chunk',
  `editor_type` int(11) NOT NULL DEFAULT '0',
  `category` int(11) NOT NULL DEFAULT '0',
  `cache_type` tinyint(1) NOT NULL DEFAULT '0',
  `snippet` mediumtext,
  `locked` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `properties` text,
  `static` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `static_file` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_site_htmlsnippets`
--

INSERT INTO `modx_site_htmlsnippets` (`id`, `source`, `property_preprocess`, `name`, `description`, `editor_type`, `category`, `cache_type`, `snippet`, `locked`, `properties`, `static`, `static_file`) VALUES
(1, 0, 0, 'catalog.item', '', 0, 0, 0, '\n<div class="item">\n    [[+tv.label:is=`NEW`:then=`<div class="cat_item_label"></div>`:else=``]]\n                <div class="rate">\n                    [[+tv.rate:is=`1`:then=`\n                        <div class="star star_active"></div>\n                        <div class="star"></div>\n                        <div class="star"></div>\n                        <div class="star"></div>\n                        <div class="star"></div>\n                    `:else=``]]\n                    [[+tv.rate:is=`2`:then=`\n                        <div class="star star_active"></div>\n                        <div class="star star_active"></div>\n                        <div class="star"></div>\n                        <div class="star"></div>\n                        <div class="star"></div>\n                    `:else=``]]\n                    [[+tv.rate:is=`3`:then=`\n                        <div class="star star_active"></div>\n                        <div class="star star_active"></div>\n                        <div class="star star_active"></div>\n                        <div class="star"></div>\n                        <div class="star"></div>\n                    `:else=``]]\n                    [[+tv.rate:is=`4`:then=`\n                        <div class="star star_active"></div>\n                        <div class="star star_active"></div>\n                        <div class="star star_active"></div>\n                        <div class="star star_active"></div>\n                        <div class="star"></div>\n                    `:else=``]]\n                    [[+tv.rate:is=`5`:then=`\n                        <div class="star star_active"></div>\n                        <div class="star star_active"></div>\n                        <div class="star star_active"></div>\n                        <div class="star star_active"></div>\n                        <div class="star star_active"></div>\n                    `:else=``]]\n                </div>\n                <a href="" class="fr_name">[[+pagetitle]]</a>\n                <div class="cover">\n                    <img src="[[+tv.anonce_img]]" alt="">\n                </div>\n                <a href="" class="link cat-link">[[!getResourceField? &field=`pagetitle` &id=[[+parent]] ]]</a>\n\n                <span class="opt">Окупаемость: <b>[[+tv.cashback_time]] мес.</b></span>\n                <span class="opt">Инвестиции: <b>[[+tv.min_invest]] руб.</b></span>\n\n                <div class="actions">\n                    <a href="" class="btn">К сравнению</a>\n                    <a href="" class="btn btn-more">Подробнее</a>\n                </div>\n            </div>', 0, 'a:0:{}', 0, ''),
(2, 1, 0, 'header', 'Шапка сайта', 0, 6, 0, '<header>\n    <div class="container">\n        <a href="/" class="logo">\n            <img src="img/logo.png" alt="Logo">\n        </a>\n\n        <div class="navigation">\n            <ul>\n                <li><a href="/" class="active">Каталог франшиз</a></li>\n                <li><a href="/">Торговля</a></li>\n                <li><a href="/">Услуги</a></li>\n                <li><a href="/">Общественное питание</a></li>\n                <li><a href="/">Производство</a></li>\n            </ul>\n        </div>\n\n        <div class="actions">\n        </div>\n\n        <div class="header_contacts pull-right">\n            <a href="tel:80000000000" class="phone">8(800)333-33-33</a>\n            <a href="" class="callback_btn">Обратный звонок</a>\n        </div>\n    </div>\n</header>\n', 0, 'a:0:{}', 1, 'chunks/header.html'),
(3, 1, 0, 'footer', 'Футер сайта', 0, 6, 0, '<footer>\n    <div class="container">\n        <div class="footer_logo">\n            <a href="/" class="logo"></a>\n        </div>\n        <div class="copy">\n            © 2016 ВСЕ ПРАВА ЗАЩИЩЕНЫ\n            <a href="" class="policy">Политика конфиденциальности</a>\n        </div>\n        <div class="search">\n            <form action="/" class="search_form">\n                <div class="input" tabindex="-1">\n                    <input type="email" id="search_input_bottom" name="email">\n                    <label for="search_input_bottom">Поиск по сайту:</label>\n                </div>\n                <button type="submit" class="btn btn-outline">Искать</button>\n            </form>\n        </div>\n    </div>\n</footer>', 0, 'a:0:{}', 1, 'chunks/footer.html'),
(4, 1, 0, 'meta', 'Мета теги шаблона', 0, 6, 0, '<meta name="description" content="">\n<meta name="viewport" content="width=1200, initial-scale=1">\n<base href="[[++site_url]]">', 0, 'a:0:{}', 0, ''),
(5, 0, 0, 'home.catalog_item', '', 0, 0, 0, '<div class="item">\n    [[+tv.label:is=`NEW`:then=`<div class="cat_item_label"></div>`:else=``]]\n                <div class="rate">\n                    <div class="star star_active"></div>\n                    <div class="star star_active"></div>\n                    <div class="star star_active"></div>\n                    <div class="star"></div>\n                    <div class="star"></div>\n                </div>\n                <a href="" class="fr_name">[[+pagetitle]]</a>\n                <div class="cover">\n                    <img src="[[+tv.anonce_img]]" alt="">\n                </div>\n                <a href="" class="link cat-link">[[!getResourceField? &field=`pagetitle` &id=[[+parent]] ]]</a>\n\n                <span class="opt">Окупаемость: <b>[[+tv.cashback_time]] мес.</b></span>\n                <span class="opt">Инвестиции: <b>[[+tv.min_invest]] руб.</b></span>\n\n                <div class="actions">\n                    <a href="" class="btn">К сравнению</a>\n                    <a href="" class="btn btn-more">Подробнее</a>\n                </div>\n            </div>', 0, 'a:0:{}', 0, ''),
(6, 0, 0, 'home.package_link', '', 0, 0, 0, '<a href="[[~[[+id]]]]" [[+wf.classes]]>[[+pagetitle]]</a>', 0, 'a:0:{}', 0, ''),
(7, 0, 0, 'home.package_nav_container', '', 0, 0, 0, '[[+wf.wrapper]]', 0, NULL, 0, ''),
(8, 0, 0, 'home.hamburger_package_link', '', 0, 0, 0, '<li><a href="[[~[[+id]]]]" class="">[[+pagetitle]]</a></li>', 0, 'a:0:{}', 0, ''),
(9, 0, 0, 'contentCatalog', 'Вывод подборки', 0, 0, 0, '\n\n[[!getPage?\n    &pagePrevTpl=`<li class="control"><a[[+classes]][[+title]] href="[[+href]]"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>`\n    &pageLastTpl=`<li class="control"><a[[+classes]][[+title]] href="[[+href]]"><i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>`\n	&pageFirstTpl=`<li class="control"><a[[+classes]][[+title]] href="[[+href]]"><i class="fa fa-angle-double-left" aria-hidden="true"></i></a></li>`\n	&pageNextTpl=`<li class="control"><a[[+classes]][[+title]] href="[[+href]]"><i class="fa fa-angle-right" aria-hidden="true"></i>`\n	&limit=`6`\n	&element=`getResources`\n	&tpl=`catalog.item` \n    &depth=`3` \n    &parents=`2` \n    &includeTVs=`1` \n    &processTVs=`1` \n    &where=`[[!TaggerGetResourcesWhere? &where=`{"template":2}` &tags=`2016`]]`\n]]\n', 0, 'a:0:{}', 0, ''),
(10, 0, 0, 'galAlbumRowTpl', '', 0, 9, 0, '<li[[+cls:notempty=` class="[[+cls]]"`]]><a href="[[~[[*id]]? &[[+albumRequestVar]]=`[[+id]]`]]">[[+showName:notempty=`[[+name]]`]]</a></li>', 0, '', 0, ''),
(11, 0, 0, 'galItemThumb', '', 0, 9, 0, '<div class="[[+cls]]">\n    <a href="[[+linkToImage:if=`[[+linkToImage]]`:is=`1`:then=`[[+image_absolute]]`:else=`[[~[[*id]]?\n            &[[+imageGetParam]]=`[[+id]]`\n            &[[+albumRequestVar]]=`[[+album]]`\n            &[[+tagRequestVar]]=`[[+tag]]` ]]`]]" title="[[+name]]" [[+link_attributes]]>\n\n        <img class="[[+imgCls]]" src="[[+thumbnail]]" alt="[[+name]]" [[+image_attributes]] />\n    </a>\n</div>', 0, '', 0, ''),
(12, 0, 0, 'gallery.fullImage', '', 0, 9, 0, '<!--\n<div class="[[+cls]]">\n    <a href="[[+linkToImage:if=`[[+linkToImage]]`:is=`1`:then=`[[+image_absolute]]`:else=`[[~[[*id]]?\n            &[[+imageGetParam]]=`[[+id]]`\n            &[[+albumRequestVar]]=`[[+album]]`\n            &[[+tagRequestVar]]=`[[+tag]]` ]]`]]" title="[[+name]]" [[+link_attributes]]>\n\n        <img class="[[+imgCls]]" src="[[+thumbnail]]" alt="[[+name]]" [[+image_attributes]] />\n    </a>\n</div>\n-->\n<div><img class="[[+imgCls]]" src="[[+image_absolute]]" alt="[[+name]]" [[+image_attributes]] /></div>', 0, 'a:0:{}', 0, '');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_site_plugins`
--

CREATE TABLE `modx_site_plugins` (
  `id` int(10) UNSIGNED NOT NULL,
  `source` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `property_preprocess` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `editor_type` int(11) NOT NULL DEFAULT '0',
  `category` int(11) NOT NULL DEFAULT '0',
  `cache_type` tinyint(1) NOT NULL DEFAULT '0',
  `plugincode` mediumtext NOT NULL,
  `locked` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `properties` text,
  `disabled` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `moduleguid` varchar(32) NOT NULL DEFAULT '',
  `static` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `static_file` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_site_plugins`
--

INSERT INTO `modx_site_plugins` (`id`, `source`, `property_preprocess`, `name`, `description`, `editor_type`, `category`, `cache_type`, `plugincode`, `locked`, `properties`, `disabled`, `moduleguid`, `static`, `static_file`) VALUES
(1, 0, 0, 'Ace', 'Ace code editor plugin for MODx Revolution', 0, 0, 0, '/**\n * Ace Source Editor Plugin\n *\n * Events: OnManagerPageBeforeRender, OnRichTextEditorRegister, OnSnipFormPrerender,\n * OnTempFormPrerender, OnChunkFormPrerender, OnPluginFormPrerender,\n * OnFileCreateFormPrerender, OnFileEditFormPrerender, OnDocFormPrerender\n *\n * @author Danil Kostin <danya.postfactum(at)gmail.com>\n *\n * @package ace\n *\n * @var array $scriptProperties\n * @var Ace $ace\n */\nif ($modx->event->name == ''OnRichTextEditorRegister'') {\n    $modx->event->output(''Ace'');\n    return;\n}\n\nif ($modx->getOption(''which_element_editor'', null, ''Ace'') !== ''Ace'') {\n    return;\n}\n\n$ace = $modx->getService(''ace'', ''Ace'', $modx->getOption(''ace.core_path'', null, $modx->getOption(''core_path'').''components/ace/'').''model/ace/'');\n$ace->initialize();\n\n$extensionMap = array(\n    ''tpl''   => ''text/x-smarty'',\n    ''htm''   => ''text/html'',\n    ''html''  => ''text/html'',\n    ''css''   => ''text/css'',\n    ''scss''  => ''text/x-scss'',\n    ''less''  => ''text/x-less'',\n    ''svg''   => ''image/svg+xml'',\n    ''xml''   => ''application/xml'',\n    ''xsl''   => ''application/xml'',\n    ''js''    => ''application/javascript'',\n    ''json''  => ''application/json'',\n    ''php''   => ''application/x-php'',\n    ''sql''   => ''text/x-sql'',\n    ''md''    => ''text/x-markdown'',\n    ''txt''   => ''text/plain'',\n    ''twig''  => ''text/x-twig''\n);\n\n// Defines wether we should highlight modx tags\n$modxTags = false;\nswitch ($modx->event->name) {\n    case ''OnSnipFormPrerender'':\n        $field = ''modx-snippet-snippet'';\n        $mimeType = ''application/x-php'';\n        break;\n    case ''OnTempFormPrerender'':\n        $field = ''modx-template-content'';\n        $modxTags = true;\n\n        switch (true) {\n            case $modx->getOption(''twiggy_class''):\n                $mimeType = ''text/x-twig'';\n                break;\n            case $modx->getOption(''pdotools_fenom_parser''):\n                $mimeType = ''text/x-smarty'';\n                break;\n            default:\n                $mimeType = ''text/html'';\n                break;\n        }\n\n        break;\n    case ''OnChunkFormPrerender'':\n        $field = ''modx-chunk-snippet'';\n        if ($modx->controller->chunk && $modx->controller->chunk->isStatic()) {\n            $extension = pathinfo($modx->controller->chunk->getSourceFile(), PATHINFO_EXTENSION);\n            $mimeType = isset($extensionMap[$extension]) ? $extensionMap[$extension] : ''text/plain'';\n        } else {\n            $mimeType = ''text/html'';\n        }\n        $modxTags = true;\n\n        switch (true) {\n            case $modx->getOption(''twiggy_class''):\n                $mimeType = ''text/x-twig'';\n                break;\n            case $modx->getOption(''pdotools_fenom_default''):\n                $mimeType = ''text/x-smarty'';\n                break;\n            default:\n                $mimeType = ''text/html'';\n                break;\n        }\n\n        break;\n    case ''OnPluginFormPrerender'':\n        $field = ''modx-plugin-plugincode'';\n        $mimeType = ''application/x-php'';\n        break;\n    case ''OnFileCreateFormPrerender'':\n        $field = ''modx-file-content'';\n        $mimeType = ''text/plain'';\n        break;\n    case ''OnFileEditFormPrerender'':\n        $field = ''modx-file-content'';\n        $extension = pathinfo($scriptProperties[''file''], PATHINFO_EXTENSION);\n        $mimeType = isset($extensionMap[$extension])\n            ? $extensionMap[$extension]\n            : ''text/plain'';\n        $modxTags = $extension == ''tpl'';\n        break;\n    case ''OnDocFormPrerender'':\n        if (!$modx->controller->resourceArray) {\n            return;\n        }\n        $field = ''ta'';\n        $mimeType = $modx->getObject(''modContentType'', $modx->controller->resourceArray[''content_type''])->get(''mime_type'');\n\n        switch (true) {\n            case $mimeType == ''text/html'' && $modx->getOption(''twiggy_class''):\n                $mimeType = ''text/x-twig'';\n                break;\n            case $mimeType == ''text/html'' && $modx->getOption(''pdotools_fenom_parser''):\n                $mimeType = ''text/x-smarty'';\n                break;\n        }\n\n        if ($modx->getOption(''use_editor'')){\n            $richText = $modx->controller->resourceArray[''richtext''];\n            $classKey = $modx->controller->resourceArray[''class_key''];\n            if ($richText || in_array($classKey, array(''modStaticResource'',''modSymLink'',''modWebLink'',''modXMLRPCResource''))) {\n                $field = false;\n            }\n        }\n        $modxTags = true;\n        break;\n    default:\n        return;\n}\n\n$modxTags = (int) $modxTags;\n$script = '''';\nif ($field) {\n    $script .= "MODx.ux.Ace.replaceComponent(''$field'', ''$mimeType'', $modxTags);";\n}\n\nif ($modx->event->name == ''OnDocFormPrerender'' && !$modx->getOption(''use_editor'')) {\n    $script .= "MODx.ux.Ace.replaceTextAreas(Ext.query(''.modx-richtext''));";\n}\n\nif ($script) {\n    $modx->controller->addHtml(''<script>Ext.onReady(function() {'' . $script . ''});</script>'');\n}', 0, NULL, 0, '', 0, 'ace/elements/plugins/ace.plugin.php'),
(2, 0, 0, 'CKEditor', 'CKEditor WYSIWYG editor plugin for MODx Revolution', 0, 0, 0, '', 0, NULL, 0, '', 1, 'ckeditor/elements/plugins/ckeditor.plugin.php'),
(3, 0, 0, 'fileTranslit', 'This plugin transliterates file names automatically on upload.', 0, 0, 0, '/**\r\n * @author Anton Andersen <anton.a.andersen@gmail.com>\r\n *\r\n * This plugin transliterates filenames on upload via MODX filemanager.\r\n * It should be bent to the OnFileManagerUpload event.\r\n * Project page: https://github.com/TriAnMan/filetranslit\r\n */\r\n$currentdoc = $modx->newObject(''modResource'');\r\nforeach ($files as &$file) {\r\n	if ($file[''error''] == 0) {\r\n		$newName = $currentdoc->cleanAlias($file[''name'']);\r\n\r\n		//file rename logic\r\n		if ($file[''name''] !== $newName) {\r\n			$arDirFiles = $source->getObjectsInContainer($directory);\r\n			foreach ($arDirFiles as &$dirFile){\r\n				if($dirFile[''name'']===$newName){\r\n					//delete file if there is one with new name\r\n					$source->removeObject($directory . $newName);\r\n				}\r\n			}\r\n			//transliterate uploaded file\r\n			$source->renameObject($directory . $file[''name''], $newName);\r\n		}\r\n	}\r\n}', 0, NULL, 0, '', 0, ''),
(4, 0, 0, 'Tagger', 'This plugin inject Tagger tab into Resource panel and handles saving of tags.', 0, 2, 0, '/**\n * Tagger\n *\n * DESCRIPTION\n *\n * This plugin inject JS to add Tab with tag groups into Resource panel\n */\n\n$corePath = $modx->getOption(''tagger.core_path'', null, $modx->getOption(''core_path'', null, MODX_CORE_PATH) . ''components/tagger/'');\n/** @var Tagger $tagger */\n$tagger = $modx->getService(\n    ''tagger'',\n    ''Tagger'',\n    $corePath . ''model/tagger/'',\n    array(\n        ''core_path'' => $corePath\n    )\n);\n\n$className = ''Tagger'' . $modx->event->name;\n$modx->loadClass(''TaggerPlugin'', $tagger->getOption(''modelPath'') . ''tagger/events/'', true, true);\n$modx->loadClass($className, $tagger->getOption(''modelPath'') . ''tagger/events/'', true, true);\n\nif (class_exists($className)) {\n    /** @var TaggerPlugin $handler */\n    $handler = new $className($modx, $scriptProperties);\n    $handler->run();\n}\n\nreturn;', 0, NULL, 0, '', 0, ''),
(5, 0, 0, 'GalleryCustomTV', '', 0, 0, 0, '/**\n * Handles plugin events for Gallery''s Custom TV\n * \n * @package gallery\n */\n$corePath = $modx->getOption(''gallery.core_path'',null,$modx->getOption(''core_path'').''components/gallery/'');\nswitch ($modx->event->name) {\n    case ''OnTVInputRenderList'':\n        $modx->event->output($corePath.''elements/tv/input/'');\n        break;\n    case ''OnTVOutputRenderList'':\n        $modx->event->output($corePath.''elements/tv/output/'');\n        break;\n    case ''OnTVInputPropertiesList'':\n        $modx->event->output($corePath.''elements/tv/inputoptions/'');\n        break;\n    case ''OnTVOutputRenderPropertiesList'':\n        $modx->event->output($corePath.''elements/tv/properties/'');\n        break;\n    case ''OnManagerPageBeforeRender'':\n        $gallery = $modx->getService(''gallery'',''Gallery'',$modx->getOption(''gallery.core_path'',null,$modx->getOption(''core_path'').''components/gallery/'').''model/gallery/'',$scriptProperties);\n        if (!($gallery instanceof Gallery)) return '''';\n\n        $snippetIds = '''';\n        $gallerySnippet = $modx->getObject(''modSnippet'',array(''name'' => ''Gallery''));\n        if ($gallerySnippet) {\n            $snippetIds .= ''GAL.snippetGallery = "''.$gallerySnippet->get(''id'').''";''."\\n";\n        }\n        $galleryItemSnippet = $modx->getObject(''modSnippet'',array(''name'' => ''GalleryItem''));\n        if ($galleryItemSnippet) {\n            $snippetIds .= ''GAL.snippetGalleryItem = "''.$galleryItemSnippet->get(''id'').''";''."\\n";\n        }\n\n        $jsDir = $modx->getOption(''gallery.assets_url'',null,$modx->getOption(''assets_url'').''components/gallery/'').''js/mgr/'';\n        $modx->controller->addLexiconTopic(''gallery:default'');\n        $modx->controller->addJavascript($jsDir.''gallery.js'');\n        $modx->controller->addJavascript($jsDir.''tree.js'');\n        $modx->controller->addHtml(''<script type="text/javascript">\n        Ext.onReady(function() {\n            GAL.config.connector_url = "''.$gallery->config[''connectorUrl''].''";\n            ''.$snippetIds.''\n        });\n        </script>'');\n        break;\n    case ''OnDocFormPrerender'':\n        $gallery = $modx->getService(''gallery'',''Gallery'',$modx->getOption(''gallery.core_path'',null,$modx->getOption(''core_path'').''components/gallery/'').''model/gallery/'',$scriptProperties);\n        if (!($gallery instanceof Gallery)) return '''';\n\n        /* assign gallery lang to JS */\n        $modx->controller->addLexiconTopic(''gallery:tv'');\n\n        /* @var modAction $action */\n        $action = $modx->getObject(''modAction'',array(\n            ''namespace'' => ''gallery'',\n            ''controller'' => ''index'',\n        ));\n        $modx->controller->addHtml(''<script type="text/javascript">\n        Ext.onReady(function() {\n            GAL.config = {};\n            GAL.config.connector_url = "''.$gallery->config[''connectorUrl''].''";\n            GAL.action = "''.($action ? $action->get(''id'') : 0).''";\n        });\n        </script>'');\n        $modx->controller->addJavascript($gallery->config[''assetsUrl''].''js/mgr/tv/Spotlight.js'');\n        $modx->controller->addJavascript($gallery->config[''assetsUrl''].''js/mgr/gallery.js'');\n        $modx->controller->addJavascript($gallery->config[''assetsUrl''].''js/mgr/widgets/album/album.items.view.js'');\n        $modx->controller->addJavascript($gallery->config[''assetsUrl''].''js/mgr/widgets/album/album.tree.js'');\n        $modx->controller->addJavascript($gallery->config[''assetsUrl''].''js/mgr/tv/gal.browser.js'');\n        $modx->controller->addJavascript($gallery->config[''assetsUrl''].''js/mgr/tv/galtv.js'');\n        $modx->controller->addCss($gallery->config[''cssUrl''].''mgr.css'');\n        break;\n}\nreturn;', 0, NULL, 0, '', 0, '');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_site_plugin_events`
--

CREATE TABLE `modx_site_plugin_events` (
  `pluginid` int(10) NOT NULL DEFAULT '0',
  `event` varchar(255) NOT NULL DEFAULT '',
  `priority` int(10) NOT NULL DEFAULT '0',
  `propertyset` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_site_plugin_events`
--

INSERT INTO `modx_site_plugin_events` (`pluginid`, `event`, `priority`, `propertyset`) VALUES
(1, 'OnChunkFormPrerender', 0, 0),
(1, 'OnPluginFormPrerender', 0, 0),
(1, 'OnSnipFormPrerender', 0, 0),
(1, 'OnTempFormPrerender', 0, 0),
(1, 'OnFileEditFormPrerender', 0, 0),
(1, 'OnFileCreateFormPrerender', 0, 0),
(1, 'OnDocFormPrerender', 0, 0),
(1, 'OnRichTextEditorRegister', 0, 0),
(1, 'OnManagerPageBeforeRender', 0, 0),
(2, 'OnManagerPageBeforeRender', 0, 0),
(2, 'OnRichTextEditorRegister', 0, 0),
(2, 'OnRichTextBrowserInit', 0, 0),
(2, 'OnRichTextEditorInit', 0, 0),
(3, 'OnFileManagerUpload', 0, 0),
(4, 'OnDocFormSave', 0, 0),
(4, 'OnDocFormPrerender', 0, 0),
(4, 'OnHandleRequest', 0, 0),
(4, 'OnResourceDuplicate', 0, 0),
(5, 'OnTVInputRenderList', 0, 0),
(5, 'OnTVInputPropertiesList', 0, 0),
(5, 'OnTVOutputRenderList', 0, 0),
(5, 'OnTVOutputRenderPropertiesList', 0, 0),
(5, 'OnDocFormPrerender', 0, 0),
(5, 'OnManagerPageBeforeRender', 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `modx_site_snippets`
--

CREATE TABLE `modx_site_snippets` (
  `id` int(10) UNSIGNED NOT NULL,
  `source` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `property_preprocess` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `editor_type` int(11) NOT NULL DEFAULT '0',
  `category` int(11) NOT NULL DEFAULT '0',
  `cache_type` tinyint(1) NOT NULL DEFAULT '0',
  `snippet` mediumtext,
  `locked` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `properties` text,
  `moduleguid` varchar(32) NOT NULL DEFAULT '',
  `static` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `static_file` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_site_snippets`
--

INSERT INTO `modx_site_snippets` (`id`, `source`, `property_preprocess`, `name`, `description`, `editor_type`, `category`, `cache_type`, `snippet`, `locked`, `properties`, `moduleguid`, `static`, `static_file`) VALUES
(1, 0, 0, 'Breadcrumbs', '', 0, 1, 0, '/**\n * BreadCrumbs\n *\n * Copyright 2009-2011 by Shaun McCormick <shaun+bc@modx.com>\n *\n * BreadCrumbs is free software; you can redistribute it and/or modify it\n * under the terms of the GNU General Public License as published by the Free\n * Software Foundation; either version 2 of the License, or (at your option) any\n * later version.\n *\n * BreadCrumbs is distributed in the hope that it will be useful, but WITHOUT ANY\n * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR\n * A PARTICULAR PURPOSE. See the GNU General Public License for more details.\n *\n * You should have received a copy of the GNU General Public License along with\n * BreadCrumbs; if not, write to the Free Software Foundation, Inc., 59 Temple Place,\n * Suite 330, Boston, MA 02111-1307 USA\n *\n * @package breadcrumbs\n */\n/**\n * @name BreadCrumbs\n * @version 0.9f\n * @created 2006-06-12\n * @since 2009-05-11\n * @author Jared <jaredc@honeydewdesign.com>\n * @editor Bill Wilson\n * @editor wendy@djamoer.net\n * @editor grad\n * @editor Shaun McCormick <shaun@collabpad.com>\n * @editor Shawn Wilkerson <shawn@shawnwilkerson.com>\n * @editor Wieger Sloot, Sterc.nl <wieger@sterc.nl>\n * @tester Bob Ray\n * @package breadcrumbs\n *\n * This snippet was designed to show the path through the various levels of site\n * structure back to the root. It is NOT necessarily the path the user took to\n * arrive at a given page.\n *\n * @see breadcrumbs.class.php for config settings\n *\n * Included classes\n * .B_crumbBox Span that surrounds all crumb output\n * .B_hideCrumb Span surrounding the "..." if there are more crumbs than will be shown\n * .B_currentCrumb Span or A tag surrounding the current crumb\n * .B_firstCrumb Span that always surrounds the first crumb, whether it is "home" or not\n * .B_lastCrumb Span surrounding last crumb, whether it is the current page or not .\n * .B_crumb Class given to each A tag surrounding the intermediate crumbs (not home, or\n * hide)\n * .B_homeCrumb Class given to the home crumb\n */\nrequire_once $modx->getOption(''breadcrumbs.core_path'',null,$modx->getOption(''core_path'').''components/breadcrumbs/'').''model/breadcrumbs/breadcrumbs.class.php'';\n$bc = new BreadCrumbs($modx,$scriptProperties);\nreturn $bc->run();', 0, '', '', 0, ''),
(2, 0, 0, 'getResources', '<strong>1.6.1-pl</strong> A general purpose Resource listing and summarization snippet for MODX Revolution', 0, 0, 0, '/**\n * getResources\n *\n * A general purpose Resource listing and summarization snippet for MODX 2.x.\n *\n * @author Jason Coward\n * @copyright Copyright 2010-2013, Jason Coward\n *\n * TEMPLATES\n *\n * tpl - Name of a chunk serving as a resource template\n * [NOTE: if not provided, properties are dumped to output for each resource]\n *\n * tplOdd - (Opt) Name of a chunk serving as resource template for resources with an odd idx value\n * (see idx property)\n * tplFirst - (Opt) Name of a chunk serving as resource template for the first resource (see first\n * property)\n * tplLast - (Opt) Name of a chunk serving as resource template for the last resource (see last\n * property)\n * tpl_{n} - (Opt) Name of a chunk serving as resource template for the nth resource\n *\n * tplCondition - (Opt) Defines a field of the resource to evaluate against keys defined in the\n * conditionalTpls property. Must be a resource field; does not work with Template Variables.\n * conditionalTpls - (Opt) A JSON object defining a map of field values and the associated tpl to\n * use when the field defined by tplCondition matches the value. [NOTE: tplOdd, tplFirst, tplLast,\n * and tpl_{n} will take precedence over any defined conditionalTpls]\n *\n * tplWrapper - (Opt) Name of a chunk serving as a wrapper template for the output\n * [NOTE: Does not work with toSeparatePlaceholders]\n *\n * SELECTION\n *\n * parents - Comma-delimited list of ids serving as parents\n *\n * context - (Opt) Comma-delimited list of context keys to limit results by; if empty, contexts for all specified\n * parents will be used (all contexts if 0 is specified) [default=]\n *\n * depth - (Opt) Integer value indicating depth to search for resources from each parent [default=10]\n *\n * tvFilters - (Opt) Delimited-list of TemplateVar values to filter resources by. Supports two\n * delimiters and two value search formats. The first delimiter || represents a logical OR and the\n * primary grouping mechanism.  Within each group you can provide a comma-delimited list of values.\n * These values can be either tied to a specific TemplateVar by name, e.g. myTV==value, or just the\n * value, indicating you are searching for the value in any TemplateVar tied to the Resource. An\n * example would be &tvFilters=`filter2==one,filter1==bar%||filter1==foo`\n * [NOTE: filtering by values uses a LIKE query and % is considered a wildcard.]\n * [NOTE: this only looks at the raw value set for specific Resource, i. e. there must be a value\n * specifically set for the Resource and it is not evaluated.]\n *\n * tvFiltersAndDelimiter - (Opt) Custom delimiter for logical AND, default '','', in case you want to\n * match a literal comma in the tvFilters. E.g. &tvFiltersAndDelimiter=`&&`\n * &tvFilters=`filter1==foo,bar&&filter2==baz` [default=,]\n *\n * tvFiltersOrDelimiter - (Opt) Custom delimiter for logical OR, default ''||'', in case you want to\n * match a literal ''||'' in the tvFilters. E.g. &tvFiltersOrDelimiter=`|OR|`\n * &tvFilters=`filter1==foo||bar|OR|filter2==baz` [default=||]\n *\n * where - (Opt) A JSON expression of criteria to build any additional where clauses from. An example would be\n * &where=`{{"alias:LIKE":"foo%", "OR:alias:LIKE":"%bar"},{"OR:pagetitle:=":"foobar", "AND:description:=":"raboof"}}`\n *\n * sortby - (Opt) Field to sort by or a JSON array, e.g. {"publishedon":"ASC","createdon":"DESC"} [default=publishedon]\n * sortbyTV - (opt) A Template Variable name to sort by (if supplied, this precedes the sortby value) [default=]\n * sortbyTVType - (Opt) A data type to CAST a TV Value to in order to sort on it properly [default=string]\n * sortbyAlias - (Opt) Query alias for sortby field [default=]\n * sortbyEscaped - (Opt) Escapes the field name(s) specified in sortby [default=0]\n * sortdir - (Opt) Order which to sort by [default=DESC]\n * sortdirTV - (Opt) Order which to sort by a TV [default=DESC]\n * limit - (Opt) Limits the number of resources returned [default=5]\n * offset - (Opt) An offset of resources returned by the criteria to skip [default=0]\n * dbCacheFlag - (Opt) Controls caching of db queries; 0|false = do not cache result set; 1 = cache result set\n * according to cache settings, any other integer value = number of seconds to cache result set [default=0]\n *\n * OPTIONS\n *\n * includeContent - (Opt) Indicates if the content of each resource should be returned in the\n * results [default=0]\n * includeTVs - (Opt) Indicates if TemplateVar values should be included in the properties available\n * to each resource template [default=0]\n * includeTVList - (Opt) Limits the TemplateVars that are included if includeTVs is true to those specified\n * by name in a comma-delimited list [default=]\n * prepareTVs - (Opt) Prepares media-source dependent TemplateVar values [default=1]\n * prepareTVList - (Opt) Limits the TVs that are prepared to those specified by name in a comma-delimited\n * list [default=]\n * processTVs - (Opt) Indicates if TemplateVar values should be rendered as they would on the\n * resource being summarized [default=0]\n * processTVList - (opt) Limits the TemplateVars that are processed if included to those specified\n * by name in a comma-delimited list [default=]\n * tvPrefix - (Opt) The prefix for TemplateVar properties [default=tv.]\n * idx - (Opt) You can define the starting idx of the resources, which is an property that is\n * incremented as each resource is rendered [default=1]\n * first - (Opt) Define the idx which represents the first resource (see tplFirst) [default=1]\n * last - (Opt) Define the idx which represents the last resource (see tplLast) [default=# of\n * resources being summarized + first - 1]\n * outputSeparator - (Opt) An optional string to separate each tpl instance [default="\\n"]\n * wrapIfEmpty - (Opt) Indicates if the tplWrapper should be applied if the output is empty [default=0]\n *\n */\n$output = array();\n$outputSeparator = isset($outputSeparator) ? $outputSeparator : "\\n";\n\n/* set default properties */\n$tpl = !empty($tpl) ? $tpl : '''';\n$includeContent = !empty($includeContent) ? true : false;\n$includeTVs = !empty($includeTVs) ? true : false;\n$includeTVList = !empty($includeTVList) ? explode('','', $includeTVList) : array();\n$processTVs = !empty($processTVs) ? true : false;\n$processTVList = !empty($processTVList) ? explode('','', $processTVList) : array();\n$prepareTVs = !empty($prepareTVs) ? true : false;\n$prepareTVList = !empty($prepareTVList) ? explode('','', $prepareTVList) : array();\n$tvPrefix = isset($tvPrefix) ? $tvPrefix : ''tv.'';\n$parents = (!empty($parents) || $parents === ''0'') ? explode('','', $parents) : array($modx->resource->get(''id''));\narray_walk($parents, ''trim'');\n$parents = array_unique($parents);\n$depth = isset($depth) ? (integer) $depth : 10;\n\n$tvFiltersOrDelimiter = isset($tvFiltersOrDelimiter) ? $tvFiltersOrDelimiter : ''||'';\n$tvFiltersAndDelimiter = isset($tvFiltersAndDelimiter) ? $tvFiltersAndDelimiter : '','';\n$tvFilters = !empty($tvFilters) ? explode($tvFiltersOrDelimiter, $tvFilters) : array();\n\n$where = !empty($where) ? $modx->fromJSON($where) : array();\n$showUnpublished = !empty($showUnpublished) ? true : false;\n$showDeleted = !empty($showDeleted) ? true : false;\n\n$sortby = isset($sortby) ? $sortby : ''publishedon'';\n$sortbyTV = isset($sortbyTV) ? $sortbyTV : '''';\n$sortbyAlias = isset($sortbyAlias) ? $sortbyAlias : ''modResource'';\n$sortbyEscaped = !empty($sortbyEscaped) ? true : false;\n$sortdir = isset($sortdir) ? $sortdir : ''DESC'';\n$sortdirTV = isset($sortdirTV) ? $sortdirTV : ''DESC'';\n$limit = isset($limit) ? (integer) $limit : 5;\n$offset = isset($offset) ? (integer) $offset : 0;\n$totalVar = !empty($totalVar) ? $totalVar : ''total'';\n\n$dbCacheFlag = !isset($dbCacheFlag) ? false : $dbCacheFlag;\nif (is_string($dbCacheFlag) || is_numeric($dbCacheFlag)) {\n    if ($dbCacheFlag == ''0'') {\n        $dbCacheFlag = false;\n    } elseif ($dbCacheFlag == ''1'') {\n        $dbCacheFlag = true;\n    } else {\n        $dbCacheFlag = (integer) $dbCacheFlag;\n    }\n}\n\n/* multiple context support */\n$contextArray = array();\n$contextSpecified = false;\nif (!empty($context)) {\n    $contextArray = explode('','',$context);\n    array_walk($contextArray, ''trim'');\n    $contexts = array();\n    foreach ($contextArray as $ctx) {\n        $contexts[] = $modx->quote($ctx);\n    }\n    $context = implode('','',$contexts);\n    $contextSpecified = true;\n    unset($contexts,$ctx);\n} else {\n    $context = $modx->quote($modx->context->get(''key''));\n}\n\n$pcMap = array();\n$pcQuery = $modx->newQuery(''modResource'', array(''id:IN'' => $parents), $dbCacheFlag);\n$pcQuery->select(array(''id'', ''context_key''));\nif ($pcQuery->prepare() && $pcQuery->stmt->execute()) {\n    foreach ($pcQuery->stmt->fetchAll(PDO::FETCH_ASSOC) as $pcRow) {\n        $pcMap[(integer) $pcRow[''id'']] = $pcRow[''context_key''];\n    }\n}\n\n$children = array();\n$parentArray = array();\nforeach ($parents as $parent) {\n    $parent = (integer) $parent;\n    if ($parent === 0) {\n        $pchildren = array();\n        if ($contextSpecified) {\n            foreach ($contextArray as $pCtx) {\n                if (!in_array($pCtx, $contextArray)) {\n                    continue;\n                }\n                $options = $pCtx !== $modx->context->get(''key'') ? array(''context'' => $pCtx) : array();\n                $pcchildren = $modx->getChildIds($parent, $depth, $options);\n                if (!empty($pcchildren)) $pchildren = array_merge($pchildren, $pcchildren);\n            }\n        } else {\n            $cQuery = $modx->newQuery(''modContext'', array(''key:!='' => ''mgr''));\n            $cQuery->select(array(''key''));\n            if ($cQuery->prepare() && $cQuery->stmt->execute()) {\n                foreach ($cQuery->stmt->fetchAll(PDO::FETCH_COLUMN) as $pCtx) {\n                    $options = $pCtx !== $modx->context->get(''key'') ? array(''context'' => $pCtx) : array();\n                    $pcchildren = $modx->getChildIds($parent, $depth, $options);\n                    if (!empty($pcchildren)) $pchildren = array_merge($pchildren, $pcchildren);\n                }\n            }\n        }\n        $parentArray[] = $parent;\n    } else {\n        $pContext = array_key_exists($parent, $pcMap) ? $pcMap[$parent] : false;\n        if ($debug) $modx->log(modX::LOG_LEVEL_ERROR, "context for {$parent} is {$pContext}");\n        if ($pContext && $contextSpecified && !in_array($pContext, $contextArray, true)) {\n            $parent = next($parents);\n            continue;\n        }\n        $parentArray[] = $parent;\n        $options = !empty($pContext) && $pContext !== $modx->context->get(''key'') ? array(''context'' => $pContext) : array();\n        $pchildren = $modx->getChildIds($parent, $depth, $options);\n    }\n    if (!empty($pchildren)) $children = array_merge($children, $pchildren);\n    $parent = next($parents);\n}\n$parents = array_merge($parentArray, $children);\n\n/* build query */\n$criteria = array("modResource.parent IN (" . implode('','', $parents) . ")");\nif ($contextSpecified) {\n    $contextResourceTbl = $modx->getTableName(''modContextResource'');\n    $criteria[] = "(modResource.context_key IN ({$context}) OR EXISTS(SELECT 1 FROM {$contextResourceTbl} ctx WHERE ctx.resource = modResource.id AND ctx.context_key IN ({$context})))";\n}\nif (empty($showDeleted)) {\n    $criteria[''deleted''] = ''0'';\n}\nif (empty($showUnpublished)) {\n    $criteria[''published''] = ''1'';\n}\nif (empty($showHidden)) {\n    $criteria[''hidemenu''] = ''0'';\n}\nif (!empty($hideContainers)) {\n    $criteria[''isfolder''] = ''0'';\n}\n$criteria = $modx->newQuery(''modResource'', $criteria);\nif (!empty($tvFilters)) {\n    $tmplVarTbl = $modx->getTableName(''modTemplateVar'');\n    $tmplVarResourceTbl = $modx->getTableName(''modTemplateVarResource'');\n    $conditions = array();\n    $operators = array(\n        ''<=>'' => ''<=>'',\n        ''==='' => ''='',\n        ''!=='' => ''!='',\n        ''<>'' => ''<>'',\n        ''=='' => ''LIKE'',\n        ''!='' => ''NOT LIKE'',\n        ''<<'' => ''<'',\n        ''<='' => ''<='',\n        ''=<'' => ''=<'',\n        ''>>'' => ''>'',\n        ''>='' => ''>='',\n        ''=>'' => ''=>''\n    );\n    foreach ($tvFilters as $fGroup => $tvFilter) {\n        $filterGroup = array();\n        $filters = explode($tvFiltersAndDelimiter, $tvFilter);\n        $multiple = count($filters) > 0;\n        foreach ($filters as $filter) {\n            $operator = ''=='';\n            $sqlOperator = ''LIKE'';\n            foreach ($operators as $op => $opSymbol) {\n                if (strpos($filter, $op, 1) !== false) {\n                    $operator = $op;\n                    $sqlOperator = $opSymbol;\n                    break;\n                }\n            }\n            $tvValueField = ''tvr.value'';\n            $tvDefaultField = ''tv.default_text'';\n            $f = explode($operator, $filter);\n            if (count($f) >= 2) {\n                if (count($f) > 2) {\n                    $k = array_shift($f);\n                    $b = join($operator, $f);\n                    $f = array($k, $b);\n                }\n                $tvName = $modx->quote($f[0]);\n                if (is_numeric($f[1]) && !in_array($sqlOperator, array(''LIKE'', ''NOT LIKE''))) {\n                    $tvValue = $f[1];\n                    if ($f[1] == (integer)$f[1]) {\n                        $tvValueField = "CAST({$tvValueField} AS SIGNED INTEGER)";\n                        $tvDefaultField = "CAST({$tvDefaultField} AS SIGNED INTEGER)";\n                    } else {\n                        $tvValueField = "CAST({$tvValueField} AS DECIMAL)";\n                        $tvDefaultField = "CAST({$tvDefaultField} AS DECIMAL)";\n                    }\n                } else {\n                    $tvValue = $modx->quote($f[1]);\n                }\n                if ($multiple) {\n                    $filterGroup[] =\n                        "(EXISTS (SELECT 1 FROM {$tmplVarResourceTbl} tvr JOIN {$tmplVarTbl} tv ON {$tvValueField} {$sqlOperator} {$tvValue} AND tv.name = {$tvName} AND tv.id = tvr.tmplvarid WHERE tvr.contentid = modResource.id) " .\n                        "OR EXISTS (SELECT 1 FROM {$tmplVarTbl} tv WHERE tv.name = {$tvName} AND {$tvDefaultField} {$sqlOperator} {$tvValue} AND tv.id NOT IN (SELECT tmplvarid FROM {$tmplVarResourceTbl} WHERE contentid = modResource.id)) " .\n                        ")";\n                } else {\n                    $filterGroup =\n                        "(EXISTS (SELECT 1 FROM {$tmplVarResourceTbl} tvr JOIN {$tmplVarTbl} tv ON {$tvValueField} {$sqlOperator} {$tvValue} AND tv.name = {$tvName} AND tv.id = tvr.tmplvarid WHERE tvr.contentid = modResource.id) " .\n                        "OR EXISTS (SELECT 1 FROM {$tmplVarTbl} tv WHERE tv.name = {$tvName} AND {$tvDefaultField} {$sqlOperator} {$tvValue} AND tv.id NOT IN (SELECT tmplvarid FROM {$tmplVarResourceTbl} WHERE contentid = modResource.id)) " .\n                        ")";\n                }\n            } elseif (count($f) == 1) {\n                $tvValue = $modx->quote($f[0]);\n                if ($multiple) {\n                    $filterGroup[] = "EXISTS (SELECT 1 FROM {$tmplVarResourceTbl} tvr JOIN {$tmplVarTbl} tv ON {$tvValueField} {$sqlOperator} {$tvValue} AND tv.id = tvr.tmplvarid WHERE tvr.contentid = modResource.id)";\n                } else {\n                    $filterGroup = "EXISTS (SELECT 1 FROM {$tmplVarResourceTbl} tvr JOIN {$tmplVarTbl} tv ON {$tvValueField} {$sqlOperator} {$tvValue} AND tv.id = tvr.tmplvarid WHERE tvr.contentid = modResource.id)";\n                }\n            }\n        }\n        $conditions[] = $filterGroup;\n    }\n    if (!empty($conditions)) {\n        $firstGroup = true;\n        foreach ($conditions as $cGroup => $c) {\n            if (is_array($c)) {\n                $first = true;\n                foreach ($c as $cond) {\n                    if ($first && !$firstGroup) {\n                        $criteria->condition($criteria->query[''where''][0][1], $cond, xPDOQuery::SQL_OR, null, $cGroup);\n                    } else {\n                        $criteria->condition($criteria->query[''where''][0][1], $cond, xPDOQuery::SQL_AND, null, $cGroup);\n                    }\n                    $first = false;\n                }\n            } else {\n                $criteria->condition($criteria->query[''where''][0][1], $c, $firstGroup ? xPDOQuery::SQL_AND : xPDOQuery::SQL_OR, null, $cGroup);\n            }\n            $firstGroup = false;\n        }\n    }\n}\n/* include/exclude resources, via &resources=`123,-456` prop */\nif (!empty($resources)) {\n    $resourceConditions = array();\n    $resources = explode('','',$resources);\n    $include = array();\n    $exclude = array();\n    foreach ($resources as $resource) {\n        $resource = (int)$resource;\n        if ($resource == 0) continue;\n        if ($resource < 0) {\n            $exclude[] = abs($resource);\n        } else {\n            $include[] = $resource;\n        }\n    }\n    if (!empty($include)) {\n        $criteria->where(array(''OR:modResource.id:IN'' => $include), xPDOQuery::SQL_OR);\n    }\n    if (!empty($exclude)) {\n        $criteria->where(array(''modResource.id:NOT IN'' => $exclude), xPDOQuery::SQL_AND, null, 1);\n    }\n}\nif (!empty($where)) {\n    $criteria->where($where);\n}\n\n$total = $modx->getCount(''modResource'', $criteria);\n$modx->setPlaceholder($totalVar, $total);\n\n$fields = array_keys($modx->getFields(''modResource''));\nif (empty($includeContent)) {\n    $fields = array_diff($fields, array(''content''));\n}\n$columns = $includeContent ? $modx->getSelectColumns(''modResource'', ''modResource'') : $modx->getSelectColumns(''modResource'', ''modResource'', '''', array(''content''), true);\n$criteria->select($columns);\nif (!empty($sortbyTV)) {\n    $criteria->leftJoin(''modTemplateVar'', ''tvDefault'', array(\n        "tvDefault.name" => $sortbyTV\n    ));\n    $criteria->leftJoin(''modTemplateVarResource'', ''tvSort'', array(\n        "tvSort.contentid = modResource.id",\n        "tvSort.tmplvarid = tvDefault.id"\n    ));\n    if (empty($sortbyTVType)) $sortbyTVType = ''string'';\n    if ($modx->getOption(''dbtype'') === ''mysql'') {\n        switch ($sortbyTVType) {\n            case ''integer'':\n                $criteria->select("CAST(IFNULL(tvSort.value, tvDefault.default_text) AS SIGNED INTEGER) AS sortTV");\n                break;\n            case ''decimal'':\n                $criteria->select("CAST(IFNULL(tvSort.value, tvDefault.default_text) AS DECIMAL) AS sortTV");\n                break;\n            case ''datetime'':\n                $criteria->select("CAST(IFNULL(tvSort.value, tvDefault.default_text) AS DATETIME) AS sortTV");\n                break;\n            case ''string'':\n            default:\n                $criteria->select("IFNULL(tvSort.value, tvDefault.default_text) AS sortTV");\n                break;\n        }\n    } elseif ($modx->getOption(''dbtype'') === ''sqlsrv'') {\n        switch ($sortbyTVType) {\n            case ''integer'':\n                $criteria->select("CAST(ISNULL(tvSort.value, tvDefault.default_text) AS BIGINT) AS sortTV");\n                break;\n            case ''decimal'':\n                $criteria->select("CAST(ISNULL(tvSort.value, tvDefault.default_text) AS DECIMAL) AS sortTV");\n                break;\n            case ''datetime'':\n                $criteria->select("CAST(ISNULL(tvSort.value, tvDefault.default_text) AS DATETIME) AS sortTV");\n                break;\n            case ''string'':\n            default:\n                $criteria->select("ISNULL(tvSort.value, tvDefault.default_text) AS sortTV");\n                break;\n        }\n    }\n    $criteria->sortby("sortTV", $sortdirTV);\n}\nif (!empty($sortby)) {\n    if (strpos($sortby, ''{'') === 0) {\n        $sorts = $modx->fromJSON($sortby);\n    } else {\n        $sorts = array($sortby => $sortdir);\n    }\n    if (is_array($sorts)) {\n        while (list($sort, $dir) = each($sorts)) {\n            if ($sortbyEscaped) $sort = $modx->escape($sort);\n            if (!empty($sortbyAlias)) $sort = $modx->escape($sortbyAlias) . ".{$sort}";\n            $criteria->sortby($sort, $dir);\n        }\n    }\n}\nif (!empty($limit)) $criteria->limit($limit, $offset);\n\nif (!empty($debug)) {\n    $criteria->prepare();\n    $modx->log(modX::LOG_LEVEL_ERROR, $criteria->toSQL());\n}\n$collection = $modx->getCollection(''modResource'', $criteria, $dbCacheFlag);\n\n$idx = !empty($idx) || $idx === ''0'' ? (integer) $idx : 1;\n$first = empty($first) && $first !== ''0'' ? 1 : (integer) $first;\n$last = empty($last) ? (count($collection) + $idx - 1) : (integer) $last;\n\n/* include parseTpl */\ninclude_once $modx->getOption(''getresources.core_path'',null,$modx->getOption(''core_path'').''components/getresources/'').''include.parsetpl.php'';\n\n$templateVars = array();\nif (!empty($includeTVs) && !empty($includeTVList)) {\n    $templateVars = $modx->getCollection(''modTemplateVar'', array(''name:IN'' => $includeTVList));\n}\n/** @var modResource $resource */\nforeach ($collection as $resourceId => $resource) {\n    $tvs = array();\n    if (!empty($includeTVs)) {\n        if (empty($includeTVList)) {\n            $templateVars = $resource->getMany(''TemplateVars'');\n        }\n        /** @var modTemplateVar $templateVar */\n        foreach ($templateVars as $tvId => $templateVar) {\n            if (!empty($includeTVList) && !in_array($templateVar->get(''name''), $includeTVList)) continue;\n            if ($processTVs && (empty($processTVList) || in_array($templateVar->get(''name''), $processTVList))) {\n                $tvs[$tvPrefix . $templateVar->get(''name'')] = $templateVar->renderOutput($resource->get(''id''));\n            } else {\n                $value = $templateVar->getValue($resource->get(''id''));\n                if ($prepareTVs && method_exists($templateVar, ''prepareOutput'') && (empty($prepareTVList) || in_array($templateVar->get(''name''), $prepareTVList))) {\n                    $value = $templateVar->prepareOutput($value);\n                }\n                $tvs[$tvPrefix . $templateVar->get(''name'')] = $value;\n            }\n        }\n    }\n    $odd = ($idx & 1);\n    $properties = array_merge(\n        $scriptProperties\n        ,array(\n            ''idx'' => $idx\n            ,''first'' => $first\n            ,''last'' => $last\n            ,''odd'' => $odd\n        )\n        ,$includeContent ? $resource->toArray() : $resource->get($fields)\n        ,$tvs\n    );\n    $resourceTpl = false;\n    if ($idx == $first && !empty($tplFirst)) {\n        $resourceTpl = parseTpl($tplFirst, $properties);\n    }\n    if ($idx == $last && empty($resourceTpl) && !empty($tplLast)) {\n        $resourceTpl = parseTpl($tplLast, $properties);\n    }\n    $tplidx = ''tpl_'' . $idx;\n    if (empty($resourceTpl) && !empty($$tplidx)) {\n        $resourceTpl = parseTpl($$tplidx, $properties);\n    }\n    if ($idx > 1 && empty($resourceTpl)) {\n        $divisors = getDivisors($idx);\n        if (!empty($divisors)) {\n            foreach ($divisors as $divisor) {\n                $tplnth = ''tpl_n'' . $divisor;\n                if (!empty($$tplnth)) {\n                    $resourceTpl = parseTpl($$tplnth, $properties);\n                    if (!empty($resourceTpl)) {\n                        break;\n                    }\n                }\n            }\n        }\n    }\n    if ($odd && empty($resourceTpl) && !empty($tplOdd)) {\n        $resourceTpl = parseTpl($tplOdd, $properties);\n    }\n    if (!empty($tplCondition) && !empty($conditionalTpls) && empty($resourceTpl)) {\n        $conTpls = $modx->fromJSON($conditionalTpls);\n        $subject = $properties[$tplCondition];\n        $tplOperator = !empty($tplOperator) ? $tplOperator : ''='';\n        $tplOperator = strtolower($tplOperator);\n        $tplCon = '''';\n        foreach ($conTpls as $operand => $conditionalTpl) {\n            switch ($tplOperator) {\n                case ''!='':\n                case ''neq'':\n                case ''not'':\n                case ''isnot'':\n                case ''isnt'':\n                case ''unequal'':\n                case ''notequal'':\n                    $tplCon = (($subject != $operand) ? $conditionalTpl : $tplCon);\n                    break;\n                case ''<'':\n                case ''lt'':\n                case ''less'':\n                case ''lessthan'':\n                    $tplCon = (($subject < $operand) ? $conditionalTpl : $tplCon);\n                    break;\n                case ''>'':\n                case ''gt'':\n                case ''greater'':\n                case ''greaterthan'':\n                    $tplCon = (($subject > $operand) ? $conditionalTpl : $tplCon);\n                    break;\n                case ''<='':\n                case ''lte'':\n                case ''lessthanequals'':\n                case ''lessthanorequalto'':\n                    $tplCon = (($subject <= $operand) ? $conditionalTpl : $tplCon);\n                    break;\n                case ''>='':\n                case ''gte'':\n                case ''greaterthanequals'':\n                case ''greaterthanequalto'':\n                    $tplCon = (($subject >= $operand) ? $conditionalTpl : $tplCon);\n                    break;\n                case ''isempty'':\n                case ''empty'':\n                    $tplCon = empty($subject) ? $conditionalTpl : $tplCon;\n                    break;\n                case ''!empty'':\n                case ''notempty'':\n                case ''isnotempty'':\n                    $tplCon = !empty($subject) && $subject != '''' ? $conditionalTpl : $tplCon;\n                    break;\n                case ''isnull'':\n                case ''null'':\n                    $tplCon = $subject == null || strtolower($subject) == ''null'' ? $conditionalTpl : $tplCon;\n                    break;\n                case ''inarray'':\n                case ''in_array'':\n                case ''ia'':\n                    $operand = explode('','', $operand);\n                    $tplCon = in_array($subject, $operand) ? $conditionalTpl : $tplCon;\n                    break;\n                case ''between'':\n                case ''range'':\n                case ''>=<'':\n                case ''><'':\n                    $operand = explode('','', $operand);\n                    $tplCon = ($subject >= min($operand) && $subject <= max($operand)) ? $conditionalTpl : $tplCon;\n                    break;\n                case ''=='':\n                case ''='':\n                case ''eq'':\n                case ''is'':\n                case ''equal'':\n                case ''equals'':\n                case ''equalto'':\n                default:\n                    $tplCon = (($subject == $operand) ? $conditionalTpl : $tplCon);\n                    break;\n            }\n        }\n        if (!empty($tplCon)) {\n            $resourceTpl = parseTpl($tplCon, $properties);\n        }\n    }\n    if (!empty($tpl) && empty($resourceTpl)) {\n        $resourceTpl = parseTpl($tpl, $properties);\n    }\n    if ($resourceTpl === false && !empty($debug)) {\n        $chunk = $modx->newObject(''modChunk'');\n        $chunk->setCacheable(false);\n        $output[]= $chunk->process(array(), ''<pre>'' . print_r($properties, true) .''</pre>'');\n    } else {\n        $output[]= $resourceTpl;\n    }\n    $idx++;\n}\n\n/* output */\n$toSeparatePlaceholders = $modx->getOption(''toSeparatePlaceholders'', $scriptProperties, false);\nif (!empty($toSeparatePlaceholders)) {\n    $modx->setPlaceholders($output, $toSeparatePlaceholders);\n    return '''';\n}\n\n$output = implode($outputSeparator, $output);\n\n$tplWrapper = $modx->getOption(''tplWrapper'', $scriptProperties, false);\n$wrapIfEmpty = $modx->getOption(''wrapIfEmpty'', $scriptProperties, false);\nif (!empty($tplWrapper) && ($wrapIfEmpty || !empty($output))) {\n    $output = parseTpl($tplWrapper, array_merge($scriptProperties, array(''output'' => $output)));\n}\n\n$toPlaceholder = $modx->getOption(''toPlaceholder'', $scriptProperties, false);\nif (!empty($toPlaceholder)) {\n    $modx->setPlaceholder($toPlaceholder, $output);\n    return '''';\n}\nreturn $output;', 0, 'a:44:{s:3:"tpl";a:7:{s:4:"name";s:3:"tpl";s:4:"desc";s:121:"Name of a chunk serving as a resource template. NOTE: if not provided, properties are dumped to output for each resource.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:6:"tplOdd";a:7:{s:4:"name";s:6:"tplOdd";s:4:"desc";s:100:"Name of a chunk serving as resource template for resources with an odd idx value (see idx property).";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:8:"tplFirst";a:7:{s:4:"name";s:8:"tplFirst";s:4:"desc";s:89:"Name of a chunk serving as resource template for the first resource (see first property).";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:7:"tplLast";a:7:{s:4:"name";s:7:"tplLast";s:4:"desc";s:87:"Name of a chunk serving as resource template for the last resource (see last property).";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:10:"tplWrapper";a:7:{s:4:"name";s:10:"tplWrapper";s:4:"desc";s:115:"Name of a chunk serving as wrapper template for the Snippet output. This does not work with toSeparatePlaceholders.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:11:"wrapIfEmpty";a:7:{s:4:"name";s:11:"wrapIfEmpty";s:4:"desc";s:95:"Indicates if empty output should be wrapped by the tplWrapper, if specified. Defaults to false.";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:0;s:7:"lexicon";N;s:4:"area";s:0:"";}s:6:"sortby";a:7:{s:4:"name";s:6:"sortby";s:4:"desc";s:153:"A field name to sort by or JSON object of field names and sortdir for each field, e.g. {"publishedon":"ASC","createdon":"DESC"}. Defaults to publishedon.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:11:"publishedon";s:7:"lexicon";N;s:4:"area";s:0:"";}s:8:"sortbyTV";a:7:{s:4:"name";s:8:"sortbyTV";s:4:"desc";s:65:"Name of a Template Variable to sort by. Defaults to empty string.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:12:"sortbyTVType";a:7:{s:4:"name";s:12:"sortbyTVType";s:4:"desc";s:72:"An optional type to indicate how to sort on the Template Variable value.";s:4:"type";s:4:"list";s:7:"options";a:4:{i:0;a:2:{s:4:"text";s:6:"string";s:5:"value";s:6:"string";}i:1;a:2:{s:4:"text";s:7:"integer";s:5:"value";s:7:"integer";}i:2;a:2:{s:4:"text";s:7:"decimal";s:5:"value";s:7:"decimal";}i:3;a:2:{s:4:"text";s:8:"datetime";s:5:"value";s:8:"datetime";}}s:5:"value";s:6:"string";s:7:"lexicon";N;s:4:"area";s:0:"";}s:11:"sortbyAlias";a:7:{s:4:"name";s:11:"sortbyAlias";s:4:"desc";s:58:"Query alias for sortby field. Defaults to an empty string.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:13:"sortbyEscaped";a:7:{s:4:"name";s:13:"sortbyEscaped";s:4:"desc";s:82:"Determines if the field name specified in sortby should be escaped. Defaults to 0.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:1:"0";s:7:"lexicon";N;s:4:"area";s:0:"";}s:7:"sortdir";a:7:{s:4:"name";s:7:"sortdir";s:4:"desc";s:41:"Order which to sort by. Defaults to DESC.";s:4:"type";s:4:"list";s:7:"options";a:2:{i:0;a:2:{s:4:"text";s:3:"ASC";s:5:"value";s:3:"ASC";}i:1;a:2:{s:4:"text";s:4:"DESC";s:5:"value";s:4:"DESC";}}s:5:"value";s:4:"DESC";s:7:"lexicon";N;s:4:"area";s:0:"";}s:9:"sortdirTV";a:7:{s:4:"name";s:9:"sortdirTV";s:4:"desc";s:61:"Order which to sort a Template Variable by. Defaults to DESC.";s:4:"type";s:4:"list";s:7:"options";a:2:{i:0;a:2:{s:4:"text";s:3:"ASC";s:5:"value";s:3:"ASC";}i:1;a:2:{s:4:"text";s:4:"DESC";s:5:"value";s:4:"DESC";}}s:5:"value";s:4:"DESC";s:7:"lexicon";N;s:4:"area";s:0:"";}s:5:"limit";a:7:{s:4:"name";s:5:"limit";s:4:"desc";s:55:"Limits the number of resources returned. Defaults to 5.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:1:"5";s:7:"lexicon";N;s:4:"area";s:0:"";}s:6:"offset";a:7:{s:4:"name";s:6:"offset";s:4:"desc";s:56:"An offset of resources returned by the criteria to skip.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:1:"0";s:7:"lexicon";N;s:4:"area";s:0:"";}s:9:"tvFilters";a:7:{s:4:"name";s:9:"tvFilters";s:4:"desc";s:778:"Delimited-list of TemplateVar values to filter resources by. Supports two delimiters and two value search formats. THe first delimiter || represents a logical OR and the primary grouping mechanism.  Within each group you can provide a comma-delimited list of values. These values can be either tied to a specific TemplateVar by name, e.g. myTV==value, or just the value, indicating you are searching for the value in any TemplateVar tied to the Resource. An example would be &tvFilters=`filter2==one,filter1==bar%||filter1==foo`. <br />NOTE: filtering by values uses a LIKE query and % is considered a wildcard. <br />ANOTHER NOTE: This only looks at the raw value set for specific Resource, i. e. there must be a value specifically set for the Resource and it is not evaluated.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:21:"tvFiltersAndDelimiter";a:7:{s:4:"name";s:21:"tvFiltersAndDelimiter";s:4:"desc";s:83:"The delimiter to use to separate logical AND expressions in tvFilters. Default is ,";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:1:",";s:7:"lexicon";N;s:4:"area";s:0:"";}s:20:"tvFiltersOrDelimiter";a:7:{s:4:"name";s:20:"tvFiltersOrDelimiter";s:4:"desc";s:83:"The delimiter to use to separate logical OR expressions in tvFilters. Default is ||";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:2:"||";s:7:"lexicon";N;s:4:"area";s:0:"";}s:5:"depth";a:7:{s:4:"name";s:5:"depth";s:4:"desc";s:88:"Integer value indicating depth to search for resources from each parent. Defaults to 10.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:2:"10";s:7:"lexicon";N;s:4:"area";s:0:"";}s:7:"parents";a:7:{s:4:"name";s:7:"parents";s:4:"desc";s:57:"Optional. Comma-delimited list of ids serving as parents.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:14:"includeContent";a:7:{s:4:"name";s:14:"includeContent";s:4:"desc";s:95:"Indicates if the content of each resource should be returned in the results. Defaults to false.";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:0;s:7:"lexicon";N;s:4:"area";s:0:"";}s:10:"includeTVs";a:7:{s:4:"name";s:10:"includeTVs";s:4:"desc";s:124:"Indicates if TemplateVar values should be included in the properties available to each resource template. Defaults to false.";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:0;s:7:"lexicon";N;s:4:"area";s:0:"";}s:13:"includeTVList";a:7:{s:4:"name";s:13:"includeTVList";s:4:"desc";s:96:"Limits included TVs to those specified as a comma-delimited list of TV names. Defaults to empty.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:10:"showHidden";a:7:{s:4:"name";s:10:"showHidden";s:4:"desc";s:85:"Indicates if Resources that are hidden from menus should be shown. Defaults to false.";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:0;s:7:"lexicon";N;s:4:"area";s:0:"";}s:15:"showUnpublished";a:7:{s:4:"name";s:15:"showUnpublished";s:4:"desc";s:79:"Indicates if Resources that are unpublished should be shown. Defaults to false.";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:0;s:7:"lexicon";N;s:4:"area";s:0:"";}s:11:"showDeleted";a:7:{s:4:"name";s:11:"showDeleted";s:4:"desc";s:75:"Indicates if Resources that are deleted should be shown. Defaults to false.";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:0;s:7:"lexicon";N;s:4:"area";s:0:"";}s:9:"resources";a:7:{s:4:"name";s:9:"resources";s:4:"desc";s:177:"A comma-separated list of resource IDs to exclude or include. IDs with a - in front mean to exclude. Ex: 123,-456 means to include Resource 123, but always exclude Resource 456.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:10:"processTVs";a:7:{s:4:"name";s:10:"processTVs";s:4:"desc";s:117:"Indicates if TemplateVar values should be rendered as they would on the resource being summarized. Defaults to false.";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:0;s:7:"lexicon";N;s:4:"area";s:0:"";}s:13:"processTVList";a:7:{s:4:"name";s:13:"processTVList";s:4:"desc";s:166:"Limits processed TVs to those specified as a comma-delimited list of TV names; note only includedTVs will be available for processing if specified. Defaults to empty.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:10:"prepareTVs";a:7:{s:4:"name";s:10:"prepareTVs";s:4:"desc";s:120:"Indicates if TemplateVar values that are not processed fully should be prepared before being returned. Defaults to true.";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:1;s:7:"lexicon";N;s:4:"area";s:0:"";}s:13:"prepareTVList";a:7:{s:4:"name";s:13:"prepareTVList";s:4:"desc";s:164:"Limits prepared TVs to those specified as a comma-delimited list of TV names; note only includedTVs will be available for preparing if specified. Defaults to empty.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:8:"tvPrefix";a:7:{s:4:"name";s:8:"tvPrefix";s:4:"desc";s:55:"The prefix for TemplateVar properties. Defaults to: tv.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:3:"tv.";s:7:"lexicon";N;s:4:"area";s:0:"";}s:3:"idx";a:7:{s:4:"name";s:3:"idx";s:4:"desc";s:120:"You can define the starting idx of the resources, which is an property that is incremented as each resource is rendered.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:5:"first";a:7:{s:4:"name";s:5:"first";s:4:"desc";s:81:"Define the idx which represents the first resource (see tplFirst). Defaults to 1.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:4:"last";a:7:{s:4:"name";s:4:"last";s:4:"desc";s:129:"Define the idx which represents the last resource (see tplLast). Defaults to the number of resources being summarized + first - 1";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:13:"toPlaceholder";a:7:{s:4:"name";s:13:"toPlaceholder";s:4:"desc";s:85:"If set, will assign the result to this placeholder instead of outputting it directly.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:22:"toSeparatePlaceholders";a:7:{s:4:"name";s:22:"toSeparatePlaceholders";s:4:"desc";s:130:"If set, will assign EACH result to a separate placeholder named by this param suffixed with a sequential number (starting from 0).";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:5:"debug";a:7:{s:4:"name";s:5:"debug";s:4:"desc";s:68:"If true, will send the SQL query to the MODX log. Defaults to false.";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:0;s:7:"lexicon";N;s:4:"area";s:0:"";}s:5:"where";a:7:{s:4:"name";s:5:"where";s:4:"desc";s:193:"A JSON expression of criteria to build any additional where clauses from, e.g. &where=`{{"alias:LIKE":"foo%", "OR:alias:LIKE":"%bar"},{"OR:pagetitle:=":"foobar", "AND:description:=":"raboof"}}`";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:11:"dbCacheFlag";a:7:{s:4:"name";s:11:"dbCacheFlag";s:4:"desc";s:218:"Determines how result sets are cached if cache_db is enabled in MODX. 0|false = do not cache result set; 1 = cache result set according to cache settings, any other integer value = number of seconds to cache result set";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:1:"0";s:7:"lexicon";N;s:4:"area";s:0:"";}s:7:"context";a:7:{s:4:"name";s:7:"context";s:4:"desc";s:116:"A comma-delimited list of context keys for limiting results. Default is empty, i.e. do not limit results by context.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:12:"tplCondition";a:7:{s:4:"name";s:12:"tplCondition";s:4:"desc";s:129:"A condition to compare against the conditionalTpls property to map Resources to different tpls based on custom conditional logic.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:11:"tplOperator";a:7:{s:4:"name";s:11:"tplOperator";s:4:"desc";s:125:"An optional operator to use for the tplCondition when comparing against the conditionalTpls operands. Default is == (equals).";s:4:"type";s:4:"list";s:7:"options";a:10:{i:0;a:2:{s:4:"text";s:11:"is equal to";s:5:"value";s:2:"==";}i:1;a:2:{s:4:"text";s:15:"is not equal to";s:5:"value";s:2:"!=";}i:2;a:2:{s:4:"text";s:9:"less than";s:5:"value";s:1:"<";}i:3;a:2:{s:4:"text";s:21:"less than or equal to";s:5:"value";s:2:"<=";}i:4;a:2:{s:4:"text";s:24:"greater than or equal to";s:5:"value";s:2:">=";}i:5;a:2:{s:4:"text";s:8:"is empty";s:5:"value";s:5:"empty";}i:6;a:2:{s:4:"text";s:12:"is not empty";s:5:"value";s:6:"!empty";}i:7;a:2:{s:4:"text";s:7:"is null";s:5:"value";s:4:"null";}i:8;a:2:{s:4:"text";s:11:"is in array";s:5:"value";s:7:"inarray";}i:9;a:2:{s:4:"text";s:10:"is between";s:5:"value";s:7:"between";}}s:5:"value";s:2:"==";s:7:"lexicon";N;s:4:"area";s:0:"";}s:15:"conditionalTpls";a:7:{s:4:"name";s:15:"conditionalTpls";s:4:"desc";s:121:"A JSON map of conditional operands and tpls to compare against the tplCondition property using the specified tplOperator.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}}', '', 0, '');
INSERT INTO `modx_site_snippets` (`id`, `source`, `property_preprocess`, `name`, `description`, `editor_type`, `category`, `cache_type`, `snippet`, `locked`, `properties`, `moduleguid`, `static`, `static_file`) VALUES
(3, 0, 0, 'TaggerGetTags', 'This Snippet allows you to list tags for resource(s), group(s) and all tags', 0, 2, 0, '/**\n * TaggerGetTags\n *\n * DESCRIPTION\n *\n * This Snippet allows you to list tags for resource(s), group(s) and all tags\n *\n * PROPERTIES:\n *\n * &resources       string  optional    Comma separated list of resources for which will be listed Tags\n * &groups          string  optional    Comma separated list of Tagger Groups for which will be listed Tags\n * &rowTpl          string  optional    Name of a chunk that will be used for each Tag. If no chunk is given, array with available placeholders will be rendered\n * &outTpl          string  optional    Name of a chunk that will be used for wrapping all tags. If no chunk is given, tags will be rendered without a wrapper\n * &separator       string  optional    String separator, that will be used for separating Tags\n * &limit           int     optional    Limit number of returned tag Tags\n * &offset          int     optional    Offset the output by this number of Tags\n * &totalPh         string  optional    Placeholder to output the total number of Tags regardless of &limit and &offset\n * &target          int     optional    An ID of a resource that will be used for generating URI for a Tag. If no ID is given, current Resource ID will be used\n * &showUnused      int     optional    If 1 is set, Tags that are not assigned to any Resource will be included to the output as well\n * &showUnpublished int     optional    If 1 is set, Tags that are assigned only to unpublished Resources will be included to the output as well\n * &showDeleted     int     optional    If 1 is set, Tags that are assigned only to deleted Resources will be included to the output as well\n * &contexts        string  optional    If set, will display only tags for resources in given contexts. Contexts can be separated by a comma\n * &toPlaceholder   string  optional    If set, output will return in placeholder with given name\n * &sort            string  optional    Sort options in JSON. Example {"tag": "ASC"} or multiple sort options {"group_id": "ASC", "tag": "ASC"}\n * &friendlyURL     int     optional    If set, will be used instead of friendly_urls system setting to generate URL\n *\n * USAGE:\n *\n * [[!TaggerGetTags? &showUnused=`1`]]\n *\n *\n * @package tagger\n */\n\n/** @var Tagger $tagger */\n$tagger = $modx->getService(''tagger'',''Tagger'',$modx->getOption(''tagger.core_path'',null,$modx->getOption(''core_path'').''components/tagger/'').''model/tagger/'',$scriptProperties);\nif (!($tagger instanceof Tagger)) return '''';\n\n$resources = $modx->getOption(''resources'', $scriptProperties, '''');\n$groups = $modx->getOption(''groups'', $scriptProperties, '''');\n$target = (int) $modx->getOption(''target'', $scriptProperties, $modx->resource->id, true);\n$showUnused = (int) $modx->getOption(''showUnused'', $scriptProperties, ''0'');\n$showUnpublished = (int) $modx->getOption(''showUnpublished'', $scriptProperties, ''0'');\n$showDeleted = (int) $modx->getOption(''showDeleted'', $scriptProperties, ''0'');\n$contexts = $modx->getOption(''contexts'', $scriptProperties, '''');\n$translate = (int) $modx->getOption(''translate'', $scriptProperties, ''0'');\n\n$defaultRowTpl = $modx->getOption(''rowTpl'', $scriptProperties, '''');\n$outTpl = $modx->getOption(''outTpl'', $scriptProperties, '''');\n$wrapIfEmpty = $modx->getOption(''wrapIfEmpty'', $scriptProperties, 1);\n$separator = $modx->getOption(''separator'', $scriptProperties, '''');\n$limit = intval($modx->getOption(''limit'', $scriptProperties, 0));\n$offset = intval($modx->getOption(''offset'', $scriptProperties, 0));\n$totalPh = $modx->getOption(''totalPh'', $scriptProperties, ''tags_total'');\n\n$weight = (int) $modx->getOption(''weight'', $scriptProperties, ''0'');\n\n$friendlyURL = $modx->getOption(''friendlyURL'', $scriptProperties, $modx->getOption(''friendly_urls'', null, 0));\n\n$sort = $modx->getOption(''sort'', $scriptProperties, ''{}'');\n$sort = $modx->fromJSON($sort);\nif ($sort === null || $sort == '''' || count($sort) == 0) {\n    $sort = array(\n        ''tag'' => ''ASC''\n    );\n}\n\n$resources = $tagger->explodeAndClean($resources);\n$groups = $tagger->explodeAndClean($groups);\n$contexts = $tagger->explodeAndClean($contexts);\n$toPlaceholder = $modx->getOption(''toPlaceholder'', $scriptProperties, '''');\n\n$c = $modx->newQuery(''TaggerTag'');\n\n$c->leftJoin(''TaggerTagResource'', ''Resources'');\n$c->leftJoin(''TaggerGroup'', ''Group'');\n$c->leftJoin(''modResource'', ''Resource'', array(''Resources.resource = Resource.id''));\n\n\nif (!empty($contexts)) {\n    $c->where(array(\n        ''Resource.context_key:IN'' => $contexts,\n    ));\n}\n\nif ($showUnpublished == 0) {\n    $c->where(array(\n        ''Resource.published'' => 1,\n        ''OR:Resource.published:IN'' => null,\n    ));\n}\n\nif ($showDeleted == 0) {\n    $c->where(array(\n        ''Resource.deleted'' => 0,\n        ''OR:Resource.deleted:IS'' => null,\n    ));\n}\n\nif ($showUnused == 0) {\n    $c->having(array(\n        ''cnt > 0'',\n    ));\n}\n\nif ($resources) {\n    $c->where(array(\n        ''Resources.resource:IN'' => $resources\n    ));\n}\n\nif ($groups) {\n    $c->where(array(\n        ''Group.id:IN'' => $groups,\n        ''OR:Group.name:IN'' => $groups,\n        ''OR:Group.alias:IN'' => $groups,\n    ));\n}\n$c->select($modx->getSelectColumns(''TaggerTag'', ''TaggerTag''));\n$c->select($modx->getSelectColumns(''TaggerGroup'', ''Group'', ''group_''));\n$c->select(array(''cnt'' => ''COUNT(Resources.tag)''));\n$c->groupby($modx->getSelectColumns(''TaggerTag'', ''TaggerTag'') . '','' . $modx->getSelectColumns(''TaggerGroup'', ''Group''));\n\n$c->prepare();\n\n$countQuery = new xPDOCriteria($modx, "SELECT COUNT(*) as total, MAX(cnt) as max_cnt FROM ({$c->toSQL(false)}) cq", $c->bindings, $c->cacheFlag);\n$stmt = $countQuery->prepare();\n\nif ($stmt && $stmt->execute()) {\n    $fetchedData = $stmt->fetch(PDO::FETCH_ASSOC);\n    $total = intval($fetchedData[''total'']);\n    $maxCnt = intval($fetchedData[''max_cnt'']);\n} else {\n    $total = 0;\n    $maxCnt = 0;\n}\n\n$modx->setPlaceholder($totalPh, $total);\n\nforeach ($sort as $field => $dir) {\n    $dir = (strtolower($dir) == ''asc'') ? ''asc'' : ''desc'';\n    $c->sortby($field, $dir);\n}\n\n$c->limit($limit, $offset);\n\n$tags = $modx->getIterator(''TaggerTag'', $c);\n\n$out = array();\n\n// prep for &tpl_N\n$keys = array_keys($scriptProperties);\n$nthTpls = array();\nforeach($keys as $key) {\n    $keyBits = $tagger->explodeAndClean($key, ''_'');\n    if (isset($keyBits[0]) && $keyBits[0] === ''tpl'') {\n        if ($i = (int) $keyBits[1]) $nthTpls[$i] = $scriptProperties[$key];\n    }\n}\nksort($nthTpls);\n\n$idx = 1;\nforeach ($tags as $tag) {\n    /** @var TaggerTag $tag */\n    $phs = $tag->toArray();\n\n    $group = $tag->Group;\n\n    if ($friendlyURL == 1) {\n        $uri = rtrim($modx->makeUrl($target, '''', ''''), ''/'') . ''/'' . $group->alias . ''/'' . $tag->alias . ''/'';\n    } else {\n        $uri = $modx->makeUrl($target, '''', $group->alias . ''='' . $tag->alias);\n    }\n\n    $phs[''uri''] = $uri;\n    $phs[''idx''] = $idx;\n    $phs[''target''] = $target;\n    $phs[''max_cnt''] = $maxCnt;\n\n    if ($weight > 0) {\n        $phs[''weight''] = intval(ceil($phs[''cnt''] / ($maxCnt / $weight)));\n    }\n\n    if ($translate == 1) {\n        $groupNameTranslated = $modx->lexicon(''tagger.custom.'' . $phs[''group_alias'']);\n        $groupDescriptionTranslated = $modx->lexicon(''tagger.custom.'' . $phs[''group_alias''] . ''_desc'');\n\n        $phs[''group_name_translated''] = ($groupNameTranslated == ''tagger.custom.'' . $phs[''group_alias'']) ? $phs[''group_name''] : $groupNameTranslated;\n        $phs[''group_description_translated''] = ($groupDescriptionTranslated == ''tagger.custom.'' . $phs[''group_alias''] . ''_desc'') ? $phs[''group_description''] : $groupDescriptionTranslated;\n    }\n\n    $rowTpl = $defaultRowTpl;\n    if ($rowTpl == '''') {\n        $out[] = ''<pre>'' . print_r($phs, true) . ''</pre>'';\n    } else {\n        if (isset($nthTpls[$idx])) {\n            $rowTpl = $nthTpls[$idx];\n        } else {\n            foreach ($nthTpls as $int => $tpl) {\n                if ( ($idx % $int) === 0 ) $rowTpl = $tpl;\n            }\n        }\n\n        $out[] = $tagger->getChunk($rowTpl, $phs);\n    }\n\n    $idx++;\n}\n\n$out = implode($separator, $out);\n\nif ($outTpl != '''') {\n    if (!empty($out) || $wrapIfEmpty) {\n        $out = $tagger->getChunk($outTpl, array(''tags'' => $out));\n    }\n}\n\nif (!empty($toPlaceholder)) {\n    $modx->setPlaceholder($toPlaceholder, $out);\n    return '''';\n}\n\nreturn $out;', 0, 'a:15:{s:9:"resources";a:7:{s:4:"name";s:9:"resources";s:4:"desc";s:29:"tagger.gettags.resources_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:17:"tagger:properties";s:4:"area";s:0:"";}s:6:"groups";a:7:{s:4:"name";s:6:"groups";s:4:"desc";s:26:"tagger.gettags.groups_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:17:"tagger:properties";s:4:"area";s:0:"";}s:6:"rowTpl";a:7:{s:4:"name";s:6:"rowTpl";s:4:"desc";s:26:"tagger.gettags.rowTpl_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:17:"tagger:properties";s:4:"area";s:0:"";}s:6:"outTpl";a:7:{s:4:"name";s:6:"outTpl";s:4:"desc";s:26:"tagger.gettags.outTpl_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:17:"tagger:properties";s:4:"area";s:0:"";}s:9:"separator";a:7:{s:4:"name";s:9:"separator";s:4:"desc";s:29:"tagger.gettags.separator_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:17:"tagger:properties";s:4:"area";s:0:"";}s:6:"target";a:7:{s:4:"name";s:6:"target";s:4:"desc";s:26:"tagger.gettags.target_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:17:"tagger:properties";s:4:"area";s:0:"";}s:10:"showUnused";a:7:{s:4:"name";s:10:"showUnused";s:4:"desc";s:30:"tagger.gettags.showUnused_desc";s:4:"type";s:11:"numberfield";s:7:"options";s:0:"";s:5:"value";s:1:"0";s:7:"lexicon";s:17:"tagger:properties";s:4:"area";s:0:"";}s:15:"showUnpublished";a:7:{s:4:"name";s:15:"showUnpublished";s:4:"desc";s:35:"tagger.gettags.showUnpublished_desc";s:4:"type";s:11:"numberfield";s:7:"options";s:0:"";s:5:"value";s:1:"0";s:7:"lexicon";s:17:"tagger:properties";s:4:"area";s:0:"";}s:11:"showDeleted";a:7:{s:4:"name";s:11:"showDeleted";s:4:"desc";s:31:"tagger.gettags.showDeleted_desc";s:4:"type";s:11:"numberfield";s:7:"options";s:0:"";s:5:"value";s:1:"0";s:7:"lexicon";s:17:"tagger:properties";s:4:"area";s:0:"";}s:8:"contexts";a:7:{s:4:"name";s:8:"contexts";s:4:"desc";s:28:"tagger.gettags.contexts_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:17:"tagger:properties";s:4:"area";s:0:"";}s:13:"toPlaceholder";a:7:{s:4:"name";s:13:"toPlaceholder";s:4:"desc";s:33:"tagger.gettags.toPlaceholder_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:17:"tagger:properties";s:4:"area";s:0:"";}s:5:"limit";a:7:{s:4:"name";s:5:"limit";s:4:"desc";s:25:"tagger.gettags.limit_desc";s:4:"type";s:11:"numberfield";s:7:"options";s:0:"";s:5:"value";s:1:"0";s:7:"lexicon";s:17:"tagger:properties";s:4:"area";s:0:"";}s:6:"offset";a:7:{s:4:"name";s:6:"offset";s:4:"desc";s:26:"tagger.gettags.offset_desc";s:4:"type";s:11:"numberfield";s:7:"options";s:0:"";s:5:"value";s:1:"0";s:7:"lexicon";s:17:"tagger:properties";s:4:"area";s:0:"";}s:7:"totalPh";a:7:{s:4:"name";s:7:"totalPh";s:4:"desc";s:27:"tagger.gettags.totalPh_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:10:"tags_total";s:7:"lexicon";s:17:"tagger:properties";s:4:"area";s:0:"";}s:4:"sort";a:7:{s:4:"name";s:4:"sort";s:4:"desc";s:24:"tagger.gettags.sort_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:14:"{"tag": "asc"}";s:7:"lexicon";s:17:"tagger:properties";s:4:"area";s:0:"";}}', '', 0, ''),
(4, 0, 0, 'TaggerGetResourcesWhere', 'This snippet generate SQL Query that can be used in WHERE condition in getResources snippet', 0, 2, 0, '/**\n * TaggerGetResourcesWhere\n *\n * DESCRIPTION\n *\n * This snippet generate SQL Query that can be used in WHERE condition in getResources snippet\n *\n * PROPERTIES:\n *\n * &tags            string  optional    Comma separated list of Tags for which will be generated a Resource query. By default Tags from GET param will be loaded\n * &groups          string  optional    Comma separated list of Tagger Groups. Only from those groups will Tags be allowed\n * &where           string  optional    Original getResources where property. If you used where property in your current getResources call, move it here\n * &likeComparison  int     optional    If set to 1, tags will compare using LIKE\n * &tagField        string  optional    Field that will be used to compare with given tags. Default: alias\n * &matchAll        int     optional    If set to 1, resource must have all specified tags. Default: 0\n * &field           string  optional    modResource field that will be used to compare with assigned resource ID\n *\n * USAGE:\n *\n * [[!getResources? &where=`[[!TaggerGetResourcesWhere? &tags=`Books,Vehicles` &where=`{"isfolder": 0}`]]`]]\n *\n */\n\n$tagger = $modx->getService(''tagger'',''Tagger'',$modx->getOption(''tagger.core_path'',null,$modx->getOption(''core_path'').''components/tagger/'').''model/tagger/'',$scriptProperties);\nif (!($tagger instanceof Tagger)) return '''';\n\n$tags = $modx->getOption(''tags'', $scriptProperties, '''');\n$where = $modx->getOption(''where'', $scriptProperties, '''');\n$tagField = $modx->getOption(''tagField'', $scriptProperties, ''alias'');\n$likeComparison = (int) $modx->getOption(''likeComparison'', $scriptProperties, 0);\n$matchAll = (int) $modx->getOption(''matchAll'', $scriptProperties, 0);\n$field = $modx->getOption(''field'', $scriptProperties, ''id'');\n$where = $modx->fromJSON($where);\nif ($where == false) {\n    $where = array();\n}\n\n$tagsCount = 0;\n\nif ($tags == '''') {\n    $gc = $modx->newQuery(''TaggerGroup'');\n    $gc->select($modx->getSelectColumns(''TaggerGroup'', '''', '''', array(''alias'')));\n\n    $groups = $modx->getOption(''groups'', $scriptProperties, '''');\n    $groups = $tagger->explodeAndClean($groups);\n    if (!empty($groups)) {\n        $gc->where(array(\n            ''name:IN'' => $groups,\n            ''OR:alias:IN'' => $groups,\n            ''OR:id:IN'' => $groups,\n        ));\n    }\n\n    $gc->prepare();\n    $gc->stmt->execute();\n    $groups = $gc->stmt->fetchAll(PDO::FETCH_COLUMN, 0);\n\n    $conditions = array();\n    foreach ($groups as $group) {\n        if (isset($_GET[$group])) {\n            $groupTags = $tagger->explodeAndClean($_GET[$group]);\n            if (!empty($groupTags)) {\n                $like = array(''AND:alias:IN'' => $groupTags);\n\n                if ($likeComparison == 1) {\n                    foreach ($groupTags as $tag) {\n                        $like[] = array(''OR:alias:LIKE'' => ''%'' . $tag . ''%'');\n                    }\n                }\n\n                $conditions[] = array(\n                    ''OR:Group.alias:='' => $group,\n                    $like\n                );\n                $tagsCount += count($groupTags);\n            }\n        }\n    }\n\n    if (count($conditions) == 0) {\n        return $modx->toJSON($where);\n    }\n\n    $c = $modx->newQuery(''TaggerTag'');\n    $c->leftJoin(''TaggerGroup'', ''Group'');\n\n    $c->where($conditions);\n} else {\n    $tags = $tagger->explodeAndClean($tags);\n\n    if (empty($tags)) {\n        return $modx->toJSON($where);\n    }\n\n    $tagsCount = count($tags);\n\n    $groups = $modx->getOption(''groups'', $scriptProperties, '''');\n\n    $groups = $tagger->explodeAndClean($groups);\n\n    $c = $modx->newQuery(''TaggerTag'');\n    $c->select($modx->getSelectColumns(''TaggerTag'', ''TaggerTag'', '''', array(''id'')));\n\n    $compare = array(\n        $tagField . '':IN'' => $tags\n    );\n\n    if ($likeComparison == 1) {\n        foreach ($tags as $tag) {\n            $compare[] = array(''OR:'' . $tagField . '':LIKE'' => ''%'' . $tag . ''%'');\n        }\n    }\n\n    $c->where($compare);\n\n    if (!empty($groups)) {\n        $c->leftJoin(''TaggerGroup'', ''Group'');\n        $c->where(array(\n            ''Group.id:IN'' => $groups,\n            ''OR:Group.name:IN'' => $groups,\n            ''OR:Group.alias:IN'' => $groups,\n        ));\n    }\n}\n\n$c->prepare();\n$c->stmt->execute();\n$tagIDs = $c->stmt->fetchAll(PDO::FETCH_COLUMN, 0);\n\nif (count($tagIDs) == 0) {\n    $tagIDs[] = 0;\n}\n\nif ($matchAll == 0) {\n    $where[] = "EXISTS (SELECT 1 FROM {$modx->getTableName(''TaggerTagResource'')} r WHERE r.tag IN (" . implode('','', $tagIDs) . ") AND r.resource = modResource." . $field . ")";\n} else {\n    $where[] = "EXISTS (SELECT 1 as found FROM {$modx->getTableName(''TaggerTagResource'')} r WHERE r.tag IN (" . implode('','', $tagIDs) . ") AND r.resource = modResource." . $field . " GROUP BY found HAVING count(found) = " . $tagsCount . ")";\n}\n\nreturn $modx->toJSON($where);', 0, 'a:6:{s:4:"tags";a:7:{s:4:"name";s:4:"tags";s:4:"desc";s:34:"tagger.getresourceswhere.tags_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:17:"tagger:properties";s:4:"area";s:0:"";}s:6:"groups";a:7:{s:4:"name";s:6:"groups";s:4:"desc";s:36:"tagger.getresourceswhere.groups_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:17:"tagger:properties";s:4:"area";s:0:"";}s:5:"where";a:7:{s:4:"name";s:5:"where";s:4:"desc";s:35:"tagger.getresourceswhere.where_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:17:"tagger:properties";s:4:"area";s:0:"";}s:14:"likeComparison";a:7:{s:4:"name";s:14:"likeComparison";s:4:"desc";s:44:"tagger.getresourceswhere.likeComparison_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:1:"0";s:7:"lexicon";s:17:"tagger:properties";s:4:"area";s:0:"";}s:8:"tagField";a:7:{s:4:"name";s:8:"tagField";s:4:"desc";s:38:"tagger.getresourceswhere.tagField_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:5:"alias";s:7:"lexicon";s:17:"tagger:properties";s:4:"area";s:0:"";}s:8:"matchAll";a:7:{s:4:"name";s:8:"matchAll";s:4:"desc";s:38:"tagger.getresourceswhere.matchAll_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:1:"0";s:7:"lexicon";s:17:"tagger:properties";s:4:"area";s:0:"";}}', '', 0, ''),
(5, 0, 0, 'TaggerGetRelatedWhere', 'This Snippet allows you to list related resources', 0, 2, 0, '/**\n * TaggerGetRelatedWhere\n *\n * DESCRIPTION\n *\n * This snippet generates a SQL Query that can be used in WHERE condition in getResources snippet\n * to get related resources, which have the same tags \n *\n * PROPERTIES:\n *\n * &resources       string  optional    Comma separated list of resources for which will be listed Tags. Default: current resource\n * &groups          string  optional    Comma separated list of Tagger Groups for which will be listed Tags\n * &showUnused      int     optional    If 1 is set, Tags that are not assigned to any Resource will be included to the output as well\n * &showUnpublished int     optional    If 1 is set, Tags that are assigned only to unpublished Resources will be included to the output as well\n * &showDeleted     int     optional    If 1 is set, Tags that are assigned only to deleted Resources will be included to the output as well\n * &contexts        string  optional    If set, will display only tags for resources in given contexts. Contexts can be separated by a comma\n * \n * TaggerGetResourcesWhere - PROPERTIES:\n * \n * &tags            string  optional    Comma separated list of Tags for which will be generated a Resource query. By default Tags from GET param will be loaded\n * &groups          string  optional    Comma separated list of Tagger Groups. Only from those groups will Tags be allowed\n * &where           string  optional    Original getResources where property. If you used where property in your current getResources call, move it here\n * &likeComparison  int     optional    If set to 1, tags will compare using LIKE\n * &tagField        string  optional    Field that will be used to compare with given tags. Default: alias\n * &matchAll        int     optional    If set to 1, resource must have all specified tags. Default: 0 \n *\n * USAGE:\n *\n * [[!TaggerGetRelatedWhere? &groups=`1` ]]\n *\n *\n * @package tagger\n */\n\n/**\n @var Tagger $tagger */\n$tagger = $modx->getService(''tagger'', ''Tagger'', $modx->getOption(''tagger.core_path'', null, $modx->getOption(''core_path'') . ''components/tagger/'') . ''model/tagger/'', $scriptProperties);\nif (!($tagger instanceof Tagger))\n    return '''';\n\n$resources = $modx->getOption(''resources'', $scriptProperties, is_object($modx->resource) ? $modx->resource->get(''id'') : '''');\n$groups = $modx->getOption(''groups'', $scriptProperties, '''');\n$showUnused = (int)$modx->getOption(''showUnused'', $scriptProperties, ''0'');\n$showUnpublished = (int)$modx->getOption(''showUnpublished'', $scriptProperties, ''0'');\n$showDeleted = (int)$modx->getOption(''showDeleted'', $scriptProperties, ''0'');\n$contexts = $modx->getOption(''contexts'', $scriptProperties, '''');\n\n$totalPh = $modx->getOption(''totalPh'', $scriptProperties, ''tags_total'');\n\n$resources = $tagger->explodeAndClean($resources);\n$groups = $tagger->explodeAndClean($groups);\n$contexts = $tagger->explodeAndClean($contexts);\n\n$c = $modx->newQuery(''TaggerTag'');\n\n$c->leftJoin(''TaggerTagResource'', ''Resources'');\n$c->leftJoin(''TaggerGroup'', ''Group'');\n$c->leftJoin(''modResource'', ''Resource'', array(''Resources.resource = Resource.id''));\n\n\nif (!empty($contexts)) {\n    $c->where(array(''Resource.context_key:IN'' => $contexts, ));\n}\n\nif ($showUnpublished == 0) {\n    $c->where(array(\n        ''Resource.published'' => 1,\n        ''OR:Resource.published:IN'' => null,\n        ));\n}\n\nif ($showDeleted == 0) {\n    $c->where(array(\n        ''Resource.deleted'' => 0,\n        ''OR:Resource.deleted:IS'' => null,\n        ));\n}\n\nif ($showUnused == 0) {\n    $c->having(array(''cnt > 0'', ));\n}\n\nif ($resources) {\n    $c->where(array(''Resources.resource:IN'' => $resources));\n}\n\nif ($groups) {\n    $c->where(array(\n        ''Group.id:IN'' => $groups,\n        ''OR:Group.name:IN'' => $groups,\n        ''OR:Group.alias:IN'' => $groups,\n        ));\n}\n$c->select($modx->getSelectColumns(''TaggerTag'', ''TaggerTag''));\n//$c->select($modx->getSelectColumns(''TaggerGroup'', ''Group'', ''group_''));\n$c->select(array(''cnt'' => ''COUNT(Resources.tag)''));\n$c->groupby($modx->getSelectColumns(''TaggerTag'', ''TaggerTag'') . '','' . $modx->getSelectColumns(''TaggerGroup'', ''Group''));\n\n$c->prepare();\n$countQuery = new xPDOCriteria($modx, "SELECT COUNT(*) FROM ({$c->toSQL(false)}) cq", $c->bindings, $c->cacheFlag);\n$stmt = $countQuery->prepare();\nif ($stmt && $stmt->execute()) {\n    $total = intval($stmt->fetchColumn());\n} else {\n    $total = 0;\n}\n\n$modx->setPlaceholder($totalPh, $total);\n\n$tags = array();\n\nif ($collection = $modx->getIterator(''TaggerTag'', $c)) {\n    foreach ($collection as $tag) {\n        $tags[] = $tag->get(''tag'');\n    }\n}\n\n$wherecondition = array(''id:not IN'' => $resources);\n\n$scriptProperties[''where''] = $modx->toJson($wherecondition);\n\n$output = ''{"template":"99999999"}'';\n\nif (count($tags)) {\n    $scriptProperties[''tags''] = implode('','', $tags);\n    $output = $modx->runSnippet(''TaggerGetResourcesWhere'', $scriptProperties);\n}\n\nreturn $output;', 0, NULL, '', 0, ''),
(6, 0, 0, 'getResourceField', 'Grab a value from a resource field or a TV', 0, 5, 0, '/**\n *\n * getResourceField\n *\n * A snippet to grab a value from a resource field or a TV\n *\n * @ author Paul Merchant\n * @ author Shaun McCormick\n * @ copyright 2011 Paul Merchant\n * @ version 1.0.3 - August 8, 2011\n * @ MIT License\n *\n * OPTIONS\n * id - The resource ID\n * field - (Opt) The field or template variable name, defaults to pagetitle\n * isTV - (Opt) Set as true to return a raw template variable\n * processTV - (Opt) Set as true to return a rendered template variable\n * default - (Opt) The value returned if no field is found\n *\n * Exmaple1: [[getResourceField? &id=`123`]]\n * Example2: [[getResourceField? &id=`[[UltimateParent?]]` &field=`myTV` &isTV=`1`]]\n * Example3: [[getResourceField? &id=`[[*parent]]` &field=`myTV` &processTV=`1`]]\n *\n**/\n\n// set defaults\n$id = $modx->getOption(''id'',$scriptProperties,$modx->resource->get(''id''));\n$field = $modx->getOption(''field'',$scriptProperties,''pagetitle'');\n$isTV = $modx->getOption(''isTV'',$scriptProperties,false);\n$processTV = $modx->getOption(''processTV'',$scriptProperties,false);\n$output = $modx->getOption(''default'',$scriptProperties,'''');\n\nif ($isTV || $processTV) {\n    // get the tv object\n    $tv = $modx->getObject(''modTemplateVar'',array(''name''=>$field));\n    if (empty($tv)) return $output;\n    if ($processTV) {\n        // get rendered tv value\n        $tvValue = $tv->renderOutput($id);\n    } else {\n        // get raw tv value\n        $tvValue = $tv->getValue($id);\n    }\n    if ($tvValue !== null) {\n        $output = $tvValue;\n    }\n} else {\n    if ($id == $modx->resource->get(''id'')) {\n        // use the current resource\n        $resource =& $modx->resource;\n        // current resource can infinite loop if pulling content field into itself\n        if ($field == ''content'') {\n            return $output;\n        }\n    } else {\n        // grab only the columns we need\n        $criteria = $modx->newQuery(''modResource'');\n        $criteria->select($modx->getSelectColumns(''modResource'',''modResource'','''',array(''id'',$field)));\n        $criteria->where(array(''id''=>$id,));\n        $resource = $modx->getObject(''modResource'',$criteria);\n        if (empty($resource)) return $output;\n    }\n    $fieldValue = $resource->get($field);\n    if ($fieldValue !== null) {\n        $output = $fieldValue;\n    }\n}\n\nreturn $output;', 0, 'a:0:{}', '', 0, ''),
(8, 0, 0, 'Wayfinder', 'Wayfinder for MODx Revolution 2.0.0-beta-5 and later.', 0, 0, 0, '/**\n * Wayfinder Snippet to build site navigation menus\n *\n * Totally refactored from original DropMenu nav builder to make it easier to\n * create custom navigation by using chunks as output templates. By using\n * templates, many of the paramaters are no longer needed for flexible output\n * including tables, unordered- or ordered-lists (ULs or OLs), definition lists\n * (DLs) or in any other format you desire.\n *\n * @version 2.1.1-beta5\n * @author Garry Nutting (collabpad.com)\n * @author Kyle Jaebker (muddydogpaws.com)\n * @author Ryan Thrash (modx.com)\n * @author Shaun McCormick (modx.com)\n * @author Jason Coward (modx.com)\n *\n * @example [[Wayfinder? &startId=`0`]]\n *\n * @var modX $modx\n * @var array $scriptProperties\n * \n * @package wayfinder\n */\n$wayfinder_base = $modx->getOption(''wayfinder.core_path'',$scriptProperties,$modx->getOption(''core_path'').''components/wayfinder/'');\n\n/* include a custom config file if specified */\nif (isset($scriptProperties[''config''])) {\n    $scriptProperties[''config''] = str_replace(''../'','''',$scriptProperties[''config'']);\n    $scriptProperties[''config''] = $wayfinder_base.''configs/''.$scriptProperties[''config''].''.config.php'';\n} else {\n    $scriptProperties[''config''] = $wayfinder_base.''configs/default.config.php'';\n}\nif (file_exists($scriptProperties[''config''])) {\n    include $scriptProperties[''config''];\n}\n\n/* include wayfinder class */\ninclude_once $wayfinder_base.''wayfinder.class.php'';\nif (!$modx->loadClass(''Wayfinder'',$wayfinder_base,true,true)) {\n    return ''error: Wayfinder class not found'';\n}\n$wf = new Wayfinder($modx,$scriptProperties);\n\n/* get user class definitions\n * TODO: eventually move these into config parameters */\n$wf->_css = array(\n    ''first'' => isset($firstClass) ? $firstClass : '''',\n    ''last'' => isset($lastClass) ? $lastClass : ''last'',\n    ''here'' => isset($hereClass) ? $hereClass : ''active'',\n    ''parent'' => isset($parentClass) ? $parentClass : '''',\n    ''row'' => isset($rowClass) ? $rowClass : '''',\n    ''outer'' => isset($outerClass) ? $outerClass : '''',\n    ''inner'' => isset($innerClass) ? $innerClass : '''',\n    ''level'' => isset($levelClass) ? $levelClass: '''',\n    ''self'' => isset($selfClass) ? $selfClass : '''',\n    ''weblink'' => isset($webLinkClass) ? $webLinkClass : ''''\n);\n\n/* get user templates\n * TODO: eventually move these into config parameters */\n$wf->_templates = array(\n    ''outerTpl'' => isset($outerTpl) ? $outerTpl : '''',\n    ''rowTpl'' => isset($rowTpl) ? $rowTpl : '''',\n    ''parentRowTpl'' => isset($parentRowTpl) ? $parentRowTpl : '''',\n    ''parentRowHereTpl'' => isset($parentRowHereTpl) ? $parentRowHereTpl : '''',\n    ''hereTpl'' => isset($hereTpl) ? $hereTpl : '''',\n    ''innerTpl'' => isset($innerTpl) ? $innerTpl : '''',\n    ''innerRowTpl'' => isset($innerRowTpl) ? $innerRowTpl : '''',\n    ''innerHereTpl'' => isset($innerHereTpl) ? $innerHereTpl : '''',\n    ''activeParentRowTpl'' => isset($activeParentRowTpl) ? $activeParentRowTpl : '''',\n    ''categoryFoldersTpl'' => isset($categoryFoldersTpl) ? $categoryFoldersTpl : '''',\n    ''startItemTpl'' => isset($startItemTpl) ? $startItemTpl : ''''\n);\n\n/* process Wayfinder */\n$output = $wf->run();\nif ($wf->_config[''debug'']) {\n    $output .= $wf->renderDebugOutput();\n}\n\n/* output results */\nif ($wf->_config[''ph'']) {\n    $modx->setPlaceholder($wf->_config[''ph''],$output);\n} else {\n    return $output;\n}', 0, 'a:48:{s:5:"level";a:6:{s:4:"name";s:5:"level";s:4:"desc";s:25:"prop_wayfinder.level_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:1:"0";s:7:"lexicon";s:20:"wayfinder:properties";}s:11:"includeDocs";a:6:{s:4:"name";s:11:"includeDocs";s:4:"desc";s:31:"prop_wayfinder.includeDocs_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:1:"0";s:7:"lexicon";s:20:"wayfinder:properties";}s:11:"excludeDocs";a:6:{s:4:"name";s:11:"excludeDocs";s:4:"desc";s:31:"prop_wayfinder.excludeDocs_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:1:"0";s:7:"lexicon";s:20:"wayfinder:properties";}s:8:"contexts";a:6:{s:4:"name";s:8:"contexts";s:4:"desc";s:28:"prop_wayfinder.contexts_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:20:"wayfinder:properties";}s:12:"cacheResults";a:6:{s:4:"name";s:12:"cacheResults";s:4:"desc";s:32:"prop_wayfinder.cacheResults_desc";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:1;s:7:"lexicon";s:20:"wayfinder:properties";}s:9:"cacheTime";a:6:{s:4:"name";s:9:"cacheTime";s:4:"desc";s:29:"prop_wayfinder.cacheTime_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";i:3600;s:7:"lexicon";s:20:"wayfinder:properties";}s:2:"ph";a:6:{s:4:"name";s:2:"ph";s:4:"desc";s:22:"prop_wayfinder.ph_desc";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:0;s:7:"lexicon";s:20:"wayfinder:properties";}s:5:"debug";a:6:{s:4:"name";s:5:"debug";s:4:"desc";s:25:"prop_wayfinder.debug_desc";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:0;s:7:"lexicon";s:20:"wayfinder:properties";}s:12:"ignoreHidden";a:6:{s:4:"name";s:12:"ignoreHidden";s:4:"desc";s:32:"prop_wayfinder.ignoreHidden_desc";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:0;s:7:"lexicon";s:20:"wayfinder:properties";}s:12:"hideSubMenus";a:6:{s:4:"name";s:12:"hideSubMenus";s:4:"desc";s:32:"prop_wayfinder.hideSubMenus_desc";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:0;s:7:"lexicon";s:20:"wayfinder:properties";}s:13:"useWeblinkUrl";a:6:{s:4:"name";s:13:"useWeblinkUrl";s:4:"desc";s:33:"prop_wayfinder.useWeblinkUrl_desc";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:1;s:7:"lexicon";s:20:"wayfinder:properties";}s:8:"fullLink";a:6:{s:4:"name";s:8:"fullLink";s:4:"desc";s:28:"prop_wayfinder.fullLink_desc";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:0;s:7:"lexicon";s:20:"wayfinder:properties";}s:6:"scheme";a:6:{s:4:"name";s:6:"scheme";s:4:"desc";s:26:"prop_wayfinder.scheme_desc";s:4:"type";s:4:"list";s:7:"options";a:3:{i:0;a:2:{s:4:"text";s:23:"prop_wayfinder.relative";s:5:"value";s:0:"";}i:1;a:2:{s:4:"text";s:23:"prop_wayfinder.absolute";s:5:"value";s:3:"abs";}i:2;a:2:{s:4:"text";s:19:"prop_wayfinder.full";s:5:"value";s:4:"full";}}s:5:"value";s:0:"";s:7:"lexicon";s:20:"wayfinder:properties";}s:9:"sortOrder";a:6:{s:4:"name";s:9:"sortOrder";s:4:"desc";s:29:"prop_wayfinder.sortOrder_desc";s:4:"type";s:4:"list";s:7:"options";a:2:{i:0;a:2:{s:4:"text";s:24:"prop_wayfinder.ascending";s:5:"value";s:3:"ASC";}i:1;a:2:{s:4:"text";s:25:"prop_wayfinder.descending";s:5:"value";s:4:"DESC";}}s:5:"value";s:3:"ASC";s:7:"lexicon";s:20:"wayfinder:properties";}s:6:"sortBy";a:6:{s:4:"name";s:6:"sortBy";s:4:"desc";s:26:"prop_wayfinder.sortBy_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:9:"menuindex";s:7:"lexicon";s:20:"wayfinder:properties";}s:5:"limit";a:6:{s:4:"name";s:5:"limit";s:4:"desc";s:25:"prop_wayfinder.limit_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:1:"0";s:7:"lexicon";s:20:"wayfinder:properties";}s:6:"cssTpl";a:6:{s:4:"name";s:6:"cssTpl";s:4:"desc";s:26:"prop_wayfinder.cssTpl_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";b:0;s:7:"lexicon";s:20:"wayfinder:properties";}s:5:"jsTpl";a:6:{s:4:"name";s:5:"jsTpl";s:4:"desc";s:25:"prop_wayfinder.jsTpl_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";b:0;s:7:"lexicon";s:20:"wayfinder:properties";}s:11:"rowIdPrefix";a:6:{s:4:"name";s:11:"rowIdPrefix";s:4:"desc";s:31:"prop_wayfinder.rowIdPrefix_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";b:0;s:7:"lexicon";s:20:"wayfinder:properties";}s:11:"textOfLinks";a:6:{s:4:"name";s:11:"textOfLinks";s:4:"desc";s:31:"prop_wayfinder.textOfLinks_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:9:"menutitle";s:7:"lexicon";s:20:"wayfinder:properties";}s:12:"titleOfLinks";a:6:{s:4:"name";s:12:"titleOfLinks";s:4:"desc";s:32:"prop_wayfinder.titleOfLinks_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:9:"pagetitle";s:7:"lexicon";s:20:"wayfinder:properties";}s:12:"displayStart";a:6:{s:4:"name";s:12:"displayStart";s:4:"desc";s:32:"prop_wayfinder.displayStart_desc";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:0;s:7:"lexicon";s:20:"wayfinder:properties";}s:10:"firstClass";a:6:{s:4:"name";s:10:"firstClass";s:4:"desc";s:30:"prop_wayfinder.firstClass_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:5:"first";s:7:"lexicon";s:20:"wayfinder:properties";}s:9:"lastClass";a:6:{s:4:"name";s:9:"lastClass";s:4:"desc";s:29:"prop_wayfinder.lastClass_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:4:"last";s:7:"lexicon";s:20:"wayfinder:properties";}s:9:"hereClass";a:6:{s:4:"name";s:9:"hereClass";s:4:"desc";s:29:"prop_wayfinder.hereClass_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:6:"active";s:7:"lexicon";s:20:"wayfinder:properties";}s:11:"parentClass";a:6:{s:4:"name";s:11:"parentClass";s:4:"desc";s:31:"prop_wayfinder.parentClass_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:20:"wayfinder:properties";}s:8:"rowClass";a:6:{s:4:"name";s:8:"rowClass";s:4:"desc";s:28:"prop_wayfinder.rowClass_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:20:"wayfinder:properties";}s:10:"outerClass";a:6:{s:4:"name";s:10:"outerClass";s:4:"desc";s:30:"prop_wayfinder.outerClass_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:20:"wayfinder:properties";}s:10:"innerClass";a:6:{s:4:"name";s:10:"innerClass";s:4:"desc";s:30:"prop_wayfinder.innerClass_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:20:"wayfinder:properties";}s:10:"levelClass";a:6:{s:4:"name";s:10:"levelClass";s:4:"desc";s:30:"prop_wayfinder.levelClass_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:20:"wayfinder:properties";}s:9:"selfClass";a:6:{s:4:"name";s:9:"selfClass";s:4:"desc";s:29:"prop_wayfinder.selfClass_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:20:"wayfinder:properties";}s:12:"webLinkClass";a:6:{s:4:"name";s:12:"webLinkClass";s:4:"desc";s:32:"prop_wayfinder.webLinkClass_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:20:"wayfinder:properties";}s:8:"outerTpl";a:6:{s:4:"name";s:8:"outerTpl";s:4:"desc";s:28:"prop_wayfinder.outerTpl_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:20:"wayfinder:properties";}s:6:"rowTpl";a:6:{s:4:"name";s:6:"rowTpl";s:4:"desc";s:26:"prop_wayfinder.rowTpl_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:20:"wayfinder:properties";}s:12:"parentRowTpl";a:6:{s:4:"name";s:12:"parentRowTpl";s:4:"desc";s:32:"prop_wayfinder.parentRowTpl_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:20:"wayfinder:properties";}s:16:"parentRowHereTpl";a:6:{s:4:"name";s:16:"parentRowHereTpl";s:4:"desc";s:36:"prop_wayfinder.parentRowHereTpl_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:20:"wayfinder:properties";}s:7:"hereTpl";a:6:{s:4:"name";s:7:"hereTpl";s:4:"desc";s:27:"prop_wayfinder.hereTpl_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:20:"wayfinder:properties";}s:8:"innerTpl";a:6:{s:4:"name";s:8:"innerTpl";s:4:"desc";s:28:"prop_wayfinder.innerTpl_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:20:"wayfinder:properties";}s:11:"innerRowTpl";a:6:{s:4:"name";s:11:"innerRowTpl";s:4:"desc";s:31:"prop_wayfinder.innerRowTpl_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:20:"wayfinder:properties";}s:12:"innerHereTpl";a:6:{s:4:"name";s:12:"innerHereTpl";s:4:"desc";s:32:"prop_wayfinder.innerHereTpl_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:20:"wayfinder:properties";}s:18:"activeParentRowTpl";a:6:{s:4:"name";s:18:"activeParentRowTpl";s:4:"desc";s:38:"prop_wayfinder.activeParentRowTpl_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:20:"wayfinder:properties";}s:18:"categoryFoldersTpl";a:6:{s:4:"name";s:18:"categoryFoldersTpl";s:4:"desc";s:38:"prop_wayfinder.categoryFoldersTpl_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:20:"wayfinder:properties";}s:12:"startItemTpl";a:6:{s:4:"name";s:12:"startItemTpl";s:4:"desc";s:32:"prop_wayfinder.startItemTpl_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:20:"wayfinder:properties";}s:11:"permissions";a:6:{s:4:"name";s:11:"permissions";s:4:"desc";s:31:"prop_wayfinder.permissions_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:4:"list";s:7:"lexicon";s:20:"wayfinder:properties";}s:6:"hereId";a:6:{s:4:"name";s:6:"hereId";s:4:"desc";s:26:"prop_wayfinder.hereId_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:20:"wayfinder:properties";}s:5:"where";a:6:{s:4:"name";s:5:"where";s:4:"desc";s:25:"prop_wayfinder.where_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:20:"wayfinder:properties";}s:9:"templates";a:6:{s:4:"name";s:9:"templates";s:4:"desc";s:29:"prop_wayfinder.templates_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:20:"wayfinder:properties";}s:18:"previewUnpublished";a:6:{s:4:"name";s:18:"previewUnpublished";s:4:"desc";s:38:"prop_wayfinder.previewunpublished_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:20:"wayfinder:properties";}}', '', 0, ''),
(9, 0, 0, 'getItemCount', 'Выводит количество ресурсов в контейнере', 0, 0, 0, '$ids = $modx->getChildIds($input);\n$count = 0;\nif (!empty($ids)) {\n    $count = $modx->getCount(''modResource'', array(\n        ''id:IN'' => $ids\n        ,''published'' => 1\n        ,''deleted'' => 0\n        ,''hidemenu'' => 0\n        ,''isfolder'' => 0\n    ));\n}\nreturn $count;', 0, NULL, '', 0, '');
INSERT INTO `modx_site_snippets` (`id`, `source`, `property_preprocess`, `name`, `description`, `editor_type`, `category`, `cache_type`, `snippet`, `locked`, `properties`, `moduleguid`, `static`, `static_file`) VALUES
(10, 0, 0, 'getPage', '<b>1.2.4-pl</b> A generic wrapper snippet for returning paged results and navigation from snippets that return limitable collections.', 0, 0, 0, '/**\n * @package getpage\n */\n$output = '''';\n\n$properties =& $scriptProperties;\n$properties[''page''] = (isset($_GET[$properties[''pageVarKey'']]) && ($page = intval($_GET[$properties[''pageVarKey'']]))) ? $page : null;\nif ($properties[''page''] === null) {\n    $properties[''page''] = (isset($_REQUEST[$properties[''pageVarKey'']]) && ($page = intval($_REQUEST[$properties[''pageVarKey'']]))) ? $page : 1;\n}\n$properties[''limit''] = (isset($_GET[''limit''])) ? intval($_GET[''limit'']) : null;\nif ($properties[''limit''] === null) {\n    $properties[''limit''] = (isset($_REQUEST[''limit''])) ? intval($_REQUEST[''limit'']) : intval($limit);\n}\n$properties[''offset''] = (!empty($properties[''limit'']) && !empty($properties[''page''])) ? ($properties[''limit''] * ($properties[''page''] - 1)) : 0;\n$properties[''totalVar''] = empty($totalVar) ? "total" : $totalVar;\n$properties[$properties[''totalVar'']] = !empty($properties[$properties[''totalVar'']]) && $total = intval($properties[$properties[''totalVar'']]) ? $total : 0;\n$properties[''pageOneLimit''] = (!empty($pageOneLimit) && $pageOneLimit = intval($pageOneLimit)) ? $pageOneLimit : $properties[''limit''];\n$properties[''actualLimit''] = $properties[''limit''];\n$properties[''pageLimit''] = isset($pageLimit) && is_numeric($pageLimit) ? intval($pageLimit) : 5;\n$properties[''element''] = empty($element) ? '''' : $element;\n$properties[''elementClass''] = empty($elementClass) ? ''modChunk'' : $elementClass;\n$properties[''pageNavVar''] = empty($pageNavVar) ? ''page.nav'' : $pageNavVar;\n$properties[''pageNavTpl''] = !isset($pageNavTpl) ? "<li[[+classes]]><a[[+classes]][[+title]] href=\\"[[+href]]\\">[[+pageNo]]</a></li>" : $pageNavTpl;\n$properties[''pageNavOuterTpl''] = !isset($pageNavOuterTpl) ? "[[+first]][[+prev]][[+pages]][[+next]][[+last]]" : $pageNavOuterTpl;\n$properties[''pageActiveTpl''] = !isset($pageActiveTpl) ? "<li[[+activeClasses:default=` class=\\"active\\"`]]><a[[+activeClasses:default=` class=\\"active\\"`]][[+title]] href=\\"[[+href]]\\">[[+pageNo]]</a></li>" : $pageActiveTpl;\n$properties[''pageFirstTpl''] = !isset($pageFirstTpl) ? "<li class=\\"control\\"><a[[+title]] href=\\"[[+href]]\\">First</a></li>" : $pageFirstTpl;\n$properties[''pageLastTpl''] = !isset($pageLastTpl) ? "<li class=\\"control\\"><a[[+title]] href=\\"[[+href]]\\">Last</a></li>" : $pageLastTpl;\n$properties[''pagePrevTpl''] = !isset($pagePrevTpl) ? "<li class=\\"control\\"><a[[+title]] href=\\"[[+href]]\\">&lt;&lt;</a></li>" : $pagePrevTpl;\n$properties[''pageNextTpl''] = !isset($pageNextTpl) ? "<li class=\\"control\\"><a[[+title]] href=\\"[[+href]]\\">&gt;&gt;</a></li>" : $pageNextTpl;\n$properties[''toPlaceholder''] = !empty($toPlaceholder) ? $toPlaceholder : '''';\n$properties[''cache''] = isset($cache) ? (boolean) $cache : (boolean) $modx->getOption(''cache_resource'', null, false);\nif (empty($cache_key)) $properties[xPDO::OPT_CACHE_KEY] = $modx->getOption(''cache_resource_key'', null, ''resource'');\nif (empty($cache_handler)) $properties[xPDO::OPT_CACHE_HANDLER] = $modx->getOption(''cache_resource_handler'', null, ''xPDOFileCache'');\nif (empty($cache_expires)) $properties[xPDO::OPT_CACHE_EXPIRES] = (integer) $modx->getOption(''cache_resource_expires'', null, 0);\n\nif ($properties[''page''] == 1 && $properties[''pageOneLimit''] !== $properties[''actualLimit'']) {\n    $properties[''limit''] = $properties[''pageOneLimit''];\n}\n\nif ($properties[''cache'']) {\n    $properties[''cachePageKey''] = $modx->resource->getCacheKey() . ''/'' . $properties[''page''] . ''/'' . md5(http_build_query($modx->request->getParameters()) . http_build_query($scriptProperties));\n    $properties[''cacheOptions''] = array(\n        xPDO::OPT_CACHE_KEY => $properties[xPDO::OPT_CACHE_KEY],\n        xPDO::OPT_CACHE_HANDLER => $properties[xPDO::OPT_CACHE_HANDLER],\n        xPDO::OPT_CACHE_EXPIRES => $properties[xPDO::OPT_CACHE_EXPIRES],\n    );\n}\n$cached = false;\nif ($properties[''cache'']) {\n    if ($modx->getCacheManager()) {\n        $cached = $modx->cacheManager->get($properties[''cachePageKey''], $properties[''cacheOptions'']);\n    }\n}\nif (empty($cached) || !isset($cached[''properties'']) || !isset($cached[''output''])) {\n    $elementObj = $modx->getObject($properties[''elementClass''], array(''name'' => $properties[''element'']));\n    if ($elementObj) {\n        $elementObj->setCacheable(false);\n        if (!empty($properties[''toPlaceholder''])) {\n            $elementObj->process($properties);\n            $output = $modx->getPlaceholder($properties[''toPlaceholder'']);\n        } else {\n            $output = $elementObj->process($properties);\n        }\n    }\n\n    include_once $modx->getOption(''getpage.core_path'',$properties,$modx->getOption(''core_path'', $properties, MODX_CORE_PATH) . ''components/getpage/'').''include.getpage.php'';\n\n    $qs = $modx->request->getParameters();\n    $properties[''qs''] =& $qs;\n\n    $totalSet = $modx->getPlaceholder($properties[''totalVar'']);\n    $properties[$properties[''totalVar'']] = (($totalSet = intval($totalSet)) ? $totalSet : $properties[$properties[''totalVar'']]);\n    if (!empty($properties[$properties[''totalVar'']]) && !empty($properties[''actualLimit''])) {\n        if ($properties[''pageOneLimit''] !== $properties[''actualLimit'']) {\n            $adjustedTotal = $properties[$properties[''totalVar'']] - $properties[''pageOneLimit''];\n            $properties[''pageCount''] = $adjustedTotal > 0 ? ceil($adjustedTotal / $properties[''actualLimit'']) + 1 : 1;\n        } else {\n            $properties[''pageCount''] = ceil($properties[$properties[''totalVar'']] / $properties[''actualLimit'']);\n        }\n    } else {\n        $properties[''pageCount''] = 1;\n    }\n    if (empty($properties[$properties[''totalVar'']]) || empty($properties[''actualLimit'']) || $properties[$properties[''totalVar'']] <= $properties[''actualLimit''] || ($properties[''page''] == 1 && $properties[$properties[''totalVar'']] <= $properties[''pageOneLimit''])) {\n        $properties[''page''] = 1;\n    } else {\n        $pageNav = getpage_buildControls($modx, $properties);\n        $properties[$properties[''pageNavVar'']] = $modx->newObject(''modChunk'')->process(array_merge($properties, $pageNav), $properties[''pageNavOuterTpl'']);\n        if ($properties[''page''] > 1) {\n            $qs[$properties[''pageVarKey'']] = $properties[''page''];\n        }\n    }\n\n    $properties[''firstItem''] = $properties[''offset''] + 1;\n    $properties[''lastItem''] = ($properties[''offset''] + $properties[''limit'']) < $totalSet ? ($properties[''offset''] + $properties[''limit'']) : $totalSet;\n\n    $properties[''pageUrl''] = $modx->makeUrl($modx->resource->get(''id''), '''', $qs);\n    if ($properties[''cache''] && $modx->getCacheManager()) {\n        $cached = array(''properties'' => $properties, ''output'' => $output);\n        $modx->cacheManager->set($properties[''cachePageKey''], $cached, $properties[xPDO::OPT_CACHE_EXPIRES], $properties[''cacheOptions'']);\n    }\n} else {\n    $properties = $cached[''properties''];\n    $output = $cached[''output''];\n}\n$modx->setPlaceholders($properties, $properties[''namespace'']);\nif (!empty($properties[''toPlaceholder''])) {\n    $modx->setPlaceholder($properties[''toPlaceholder''], $output);\n    $output = '''';\n}\n\nreturn $output;', 0, 'a:21:{s:9:"namespace";a:7:{s:4:"name";s:9:"namespace";s:4:"desc";s:114:"An execution namespace that serves as a prefix for placeholders set by a specific instance of the getPage snippet.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:5:"limit";a:7:{s:4:"name";s:5:"limit";s:4:"desc";s:62:"The result limit per page; can be overridden in the $_REQUEST.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:2:"10";s:7:"lexicon";N;s:4:"area";s:0:"";}s:6:"offset";a:7:{s:4:"name";s:6:"offset";s:4:"desc";s:171:"The offset, or record position to start at within the collection for rendering results for the current page; should be calculated based on page variable set in pageVarKey.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:1:"0";s:7:"lexicon";N;s:4:"area";s:0:"";}s:4:"page";a:7:{s:4:"name";s:4:"page";s:4:"desc";s:136:"The page to display; this is determined based on the value indicated by the page variable set in pageVarKey, typically in the $_REQUEST.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:1:"0";s:7:"lexicon";N;s:4:"area";s:0:"";}s:10:"pageVarKey";a:7:{s:4:"name";s:10:"pageVarKey";s:4:"desc";s:54:"The key of a property that indicates the current page.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:4:"page";s:7:"lexicon";N;s:4:"area";s:0:"";}s:8:"totalVar";a:7:{s:4:"name";s:8:"totalVar";s:4:"desc";s:101:"The key of a placeholder that must contain the total records in the limitable collection being paged.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:5:"total";s:7:"lexicon";N;s:4:"area";s:0:"";}s:9:"pageLimit";a:7:{s:4:"name";s:9:"pageLimit";s:4:"desc";s:69:"The maximum number of pages to display when rendering paging controls";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:1:"5";s:7:"lexicon";N;s:4:"area";s:0:"";}s:12:"elementClass";a:7:{s:4:"name";s:12:"elementClass";s:4:"desc";s:73:"The class of element that will be called by the getPage snippet instance.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:10:"modSnippet";s:7:"lexicon";N;s:4:"area";s:0:"";}s:10:"pageNavVar";a:7:{s:4:"name";s:10:"pageNavVar";s:4:"desc";s:71:"The key of a placeholder to be set with the paging navigation controls.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:8:"page.nav";s:7:"lexicon";N;s:4:"area";s:0:"";}s:10:"pageNavTpl";a:7:{s:4:"name";s:10:"pageNavTpl";s:4:"desc";s:54:"Content representing a single page navigation control.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:78:"<li[[+classes]]><a[[+classes]][[+title]] href="[[+href]]">[[+pageNo]]</a></li>";s:7:"lexicon";N;s:4:"area";s:0:"";}s:15:"pageNavOuterTpl";a:7:{s:4:"name";s:15:"pageNavOuterTpl";s:4:"desc";s:64:"Content representing the layout of the page navigation controls.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:47:"[[+first]][[+prev]][[+pages]][[+next]][[+last]]";s:7:"lexicon";N;s:4:"area";s:0:"";}s:13:"pageActiveTpl";a:7:{s:4:"name";s:13:"pageActiveTpl";s:4:"desc";s:57:"Content representing the current page navigation control.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:116:"<li[[+activeClasses]]><a[[+activeClasses:default=` class="active"`]][[+title]] href="[[+href]]">[[+pageNo]]</a></li>";s:7:"lexicon";N;s:4:"area";s:0:"";}s:12:"pageFirstTpl";a:7:{s:4:"name";s:12:"pageFirstTpl";s:4:"desc";s:55:"Content representing the first page navigation control.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:76:"<li class="control"><a[[+classes]][[+title]] href="[[+href]]">First</a></li>";s:7:"lexicon";N;s:4:"area";s:0:"";}s:11:"pageLastTpl";a:7:{s:4:"name";s:11:"pageLastTpl";s:4:"desc";s:54:"Content representing the last page navigation control.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:75:"<li class="control"><a[[+classes]][[+title]] href="[[+href]]">Last</a></li>";s:7:"lexicon";N;s:4:"area";s:0:"";}s:11:"pagePrevTpl";a:7:{s:4:"name";s:11:"pagePrevTpl";s:4:"desc";s:58:"Content representing the previous page navigation control.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:79:"<li class="control"><a[[+classes]][[+title]] href="[[+href]]">&lt;&lt;</a></li>";s:7:"lexicon";N;s:4:"area";s:0:"";}s:11:"pageNextTpl";a:7:{s:4:"name";s:11:"pageNextTpl";s:4:"desc";s:54:"Content representing the next page navigation control.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:79:"<li class="control"><a[[+classes]][[+title]] href="[[+href]]">&gt;&gt;</a></li>";s:7:"lexicon";N;s:4:"area";s:0:"";}s:5:"cache";a:7:{s:4:"name";s:5:"cache";s:4:"desc";s:84:"If true, unique page requests will be cached according to addition cache properties.";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:0;s:7:"lexicon";N;s:4:"area";s:0:"";}s:9:"cache_key";a:7:{s:4:"name";s:9:"cache_key";s:4:"desc";s:120:"The key of the cache provider to use; leave empty to use the cache_resource_key cache partition (default is "resource").";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";b:0;s:7:"lexicon";N;s:4:"area";s:0:"";}s:13:"cache_handler";a:7:{s:4:"name";s:13:"cache_handler";s:4:"desc";s:99:"The cache provider implementation to use; leave empty unless you are caching to a custom cache_key.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";b:0;s:7:"lexicon";N;s:4:"area";s:0:"";}s:13:"cache_expires";a:7:{s:4:"name";s:13:"cache_expires";s:4:"desc";s:141:"The number of seconds before the cached pages expire and must be regenerated; leave empty to use the cache provider option for cache_expires.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";b:0;s:7:"lexicon";N;s:4:"area";s:0:"";}s:13:"pageNavScheme";a:7:{s:4:"name";s:13:"pageNavScheme";s:4:"desc";s:145:"Optionally specify a scheme for use when generating page navigation links; will use link_tag_scheme if empty or not specified (default is empty).";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}}', '', 0, ''),
(11, 0, 0, 'Gallery', '', 0, 9, 0, '/**\n * Gallery\n *\n * Copyright 2010-2012 by Shaun McCormick <shaun@modx.com>\n *\n * Gallery is free software; you can redistribute it and/or modify it under the\n * terms of the GNU General Public License as published by the Free Software\n * Foundation; either version 2 of the License, or (at your option) any later\n * version.\n *\n * Gallery is distributed in the hope that it will be useful, but WITHOUT ANY\n * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR\n * A PARTICULAR PURPOSE. See the GNU General Public License for more details.\n *\n * You should have received a copy of the GNU General Public License along with\n * Gallery; if not, write to the Free Software Foundation, Inc., 59 Temple\n * Place, Suite 330, Boston, MA 02111-1307 USA\n *\n * @package gallery\n */\n/**\n * The main Gallery snippet.\n *\n * @var modX $modx\n * @var Gallery $gallery\n * \n * @package gallery\n */\n$gallery = $modx->getService(''gallery'',''Gallery'',$modx->getOption(''gallery.core_path'',null,$modx->getOption(''core_path'').''components/gallery/'').''model/gallery/'',$scriptProperties);\nif (!($gallery instanceof Gallery)) return '''';\n$modx->lexicon->load(''gallery:web'');\n\n/* check for REQUEST vars if property set */\n$imageGetParam = $modx->getOption(''imageGetParam'',$scriptProperties,''galItem'');\n$albumRequestVar = $modx->getOption(''albumRequestVar'',$scriptProperties,''galAlbum'');\n$tagRequestVar = $modx->getOption(''tagRequestVar'',$scriptProperties,''galTag'');\nif ($modx->getOption(''checkForRequestAlbumVar'',$scriptProperties,true)) {\n    if (!empty($_REQUEST[$albumRequestVar])) $scriptProperties[''album''] = $_REQUEST[$albumRequestVar];\n}\nif ($modx->getOption(''checkForRequestTagVar'',$scriptProperties,true)) {\n    if (!empty($_REQUEST[$tagRequestVar])) $scriptProperties[''tag''] = $_REQUEST[$tagRequestVar];\n}\nif (empty($scriptProperties[''album'']) && empty($scriptProperties[''tag''])) return '''';\n\n$data = $modx->call(''galItem'',''getList'',array(&$modx,$scriptProperties));\n$totalVar = $modx->getOption(''totalVar'', $scriptProperties, ''total'');\n$modx->setPlaceholder($totalVar,$data[''total'']);\n\n/* load plugins */\n$plugin = $modx->getOption(''plugin'',$scriptProperties,'''');\nif (!empty($plugin)) {\n    $pluginPath = $modx->getOption(''pluginPath'',$scriptProperties,'''');\n    if (empty($pluginPath)) {\n        $pluginPath = $gallery->config[''modelPath''].''gallery/plugins/'';\n    }\n    /** @var GalleryPlugin $plugin */\n    if (($className = $modx->loadClass($plugin,$pluginPath,true,true))) {\n        $plugin = new $className($gallery,$scriptProperties);\n        $plugin->load();\n        $scriptProperties = $plugin->adjustSettings($scriptProperties);\n    } else {\n        return $modx->lexicon(''gallery.plugin_err_load'',array(''name'' => $plugin,''path'' => $pluginPath));\n    }\n} else {\n    if ($modx->getOption(''useCss'',$scriptProperties,true)) {\n        $modx->regClientCSS($gallery->config[''cssUrl''].''web.css'');\n    }\n}\n\n/* iterate */\n$imageProperties = $modx->getOption(''imageProperties'',$scriptProperties,'''');\n$imageProperties = !empty($imageProperties) ? $modx->fromJSON($imageProperties) : array();\n$imageProperties = array_merge(array(\n    ''w'' => (int)$modx->getOption(''imageWidth'',$scriptProperties,500),\n    ''h'' => (int)$modx->getOption(''imageHeight'',$scriptProperties,500),\n    ''zc'' => (boolean)$modx->getOption(''imageZoomCrop'',$scriptProperties,0),\n    ''far'' => (string)$modx->getOption(''imageFar'',$scriptProperties,false),\n    ''q'' => (int)$modx->getOption(''imageQuality'',$scriptProperties,90),\n),$imageProperties);\n\n$thumbProperties = $modx->getOption(''thumbProperties'',$scriptProperties,'''');\n$thumbProperties = !empty($thumbProperties) ? $modx->fromJSON($thumbProperties) : array();\n$thumbProperties = array_merge(array(\n    ''w'' => (int)$modx->getOption(''thumbWidth'',$scriptProperties,100),\n    ''h'' => (int)$modx->getOption(''thumbHeight'',$scriptProperties,100),\n    ''zc'' => (boolean)$modx->getOption(''thumbZoomCrop'',$scriptProperties,1),\n    ''far'' => (string)$modx->getOption(''thumbFar'',$scriptProperties,''C''),\n    ''q'' => (int)$modx->getOption(''thumbQuality'',$scriptProperties,90),\n),$thumbProperties);\n\n$idx = 0;\n$output = array();\n$filesUrl = $modx->call(''galAlbum'',''getFilesUrl'',array(&$modx));\n$filesPath = $modx->call(''galAlbum'',''getFilesPath'',array(&$modx));\n$itemCls = $modx->getOption(''itemCls'',$scriptProperties,''gal-item'');\n$imageAttributes = $modx->getOption(''imageAttributes'',$scriptProperties,'''');\n$linkAttributes = $modx->getOption(''linkAttributes'',$scriptProperties,'''');\n$linkToImage = $modx->getOption(''linkToImage'',$scriptProperties,false);\n$activeCls = $modx->getOption(''activeCls'',$scriptProperties,''gal-item-active'');\n$highlightItem = $modx->getOption($imageGetParam,$_REQUEST,false);\n$defaultThumbTpl = $modx->getOption(''thumbTpl'',$scriptProperties,''galItemThumb'');\n\n/** @var galItem $item */\n\nif (!is_array($data)) return '''';\n\n// prep for &thumbTpl_N\n$keys = array_keys($scriptProperties);\n$nthTpls = array();\nforeach($keys as $key) {\n    $keyBits = $gallery->explodeAndClean($key, ''_'');\n    if (isset($keyBits[0]) && $keyBits[0] === ''thumbTpl'') {\n        if ($i = (int) $keyBits[1]) $nthTpls[$i] = $scriptProperties[$key];\n    }\n}\nksort($nthTpls);\n\nforeach ($data[''items''] as $item) {\n    $itemArray = $item->toArray();\n    $itemArray[''idx''] = $idx;\n    $itemArray[''cls''] = $itemCls;\n    if ($itemArray[''id''] == $highlightItem) {\n        $itemArray[''cls''] .= '' ''.$activeCls;\n    }\n    $itemArray[''filename''] = basename($item->get(''filename''));\n    $itemArray[''image_absolute''] = $item->get(''base_url'').$filesUrl.$item->get(''filename'');\n    $itemArray[''fileurl''] = $itemArray[''image_absolute''];\n    $itemArray[''filepath''] = $filesPath.$item->get(''filename'');\n    $itemArray[''filesize''] = $item->get(''filesize'');\n    $itemArray[''thumbnail''] = $item->get(''thumbnail'',$thumbProperties);\n    $itemArray[''image''] = $item->get(''thumbnail'',$imageProperties);\n    $itemArray[''image_attributes''] = $imageAttributes;\n    $itemArray[''link_attributes''] = $linkAttributes;\n    if (!empty($scriptProperties[''album''])) $itemArray[''album''] = $scriptProperties[''album''];\n    if (!empty($scriptProperties[''tag''])) $itemArray[''tag''] = $scriptProperties[''tag''];\n    $itemArray[''linkToImage''] = $linkToImage;\n    $itemArray[''url''] = $item->get(''url'');\n    $itemArray[''imageGetParam''] = $imageGetParam;\n    $itemArray[''albumRequestVar''] = $albumRequestVar;\n    $itemArray[''tagRequestVar''] = $tagRequestVar;\n    $itemArray[''tag''] = '''';\n    if ($plugin) {\n        $plugin->renderItem($itemArray);\n    }\n\n    $thumbTpl = $defaultThumbTpl;\n    if (isset($nthTpls[$idx])) {\n        $thumbTpl = $nthTpls[$idx];\n    } else {\n        foreach ($nthTpls as $int => $tpl) {\n            if ( ($idx % $int) === 0 ) $thumbTpl = $tpl;\n        }\n    }\n\n    $output[] = $gallery->getChunk($thumbTpl,$itemArray);\n\n    $idx++;\n}\n$output = implode("\\n",$output);\n\n/* if set, place in a container tpl */\n$containerTpl = $modx->getOption(''containerTpl'',$scriptProperties,false);\nif (!empty($containerTpl)) {\n    $ct = $gallery->getChunk($containerTpl,array(\n        ''thumbnails'' => $output,\n        ''album_name'' => $data[''album''][''name''],\n        ''album_description'' => $data[''album''][''description''],\n        ''album_year'' => isset($data[''album''][''year'']) ? $data[''album''][''year''] : '''',\n        ''albumRequestVar'' => $albumRequestVar,\n        ''albumId'' => $data[''album''][''id''],\n    ));\n    if (!empty($ct)) $output = $ct;\n}\n\n/* set to placeholders or output directly */\n$toPlaceholder = $modx->getOption(''toPlaceholder'',$scriptProperties,false);\nif (!empty($toPlaceholder)) {\n    $modx->toPlaceholders(array(\n        $toPlaceholder => $output,\n        $toPlaceholder.''.id'' => $data[''album''][''id''],\n        $toPlaceholder.''.name'' => $data[''album''][''name''],\n        $toPlaceholder.''.year'' => isset($data[''album''][''year'']) ? $data[''album''][''year''] : '''',\n        $toPlaceholder.''.description'' => $data[''album''][''description''],\n        $toPlaceholder.''.total'' => $data[''total''],\n        $toPlaceholder.''.next'' => $data[''album''][''id''] + 1,\n        $toPlaceholder.''.prev'' => $data[''album''][''id''] - 1,\n    ));\n} else {\n    $placeholderPrefix = $modx->getOption(''placeholderPrefix'',$scriptProperties,''gallery.'');\n    $modx->toPlaceholders(array(\n        $placeholderPrefix.''id'' => $data[''album''][''id''],\n        $placeholderPrefix.''name'' => $data[''album''][''name''],\n        $placeholderPrefix.''year'' => isset($data[''album''][''year'']) ? $data[''album''][''year''] : '''',\n        $placeholderPrefix.''description'' => $data[''album''][''description''],\n        $placeholderPrefix.''total'' => $data[''total''],\n        $placeholderPrefix.''next'' => $data[''album''][''id''] + 1,\n        $placeholderPrefix.''prev'' => $data[''album''][''id''] - 1,\n    ));\n    return $output;\n}\nreturn '''';', 0, 'a:33:{s:5:"album";a:7:{s:4:"name";s:5:"album";s:4:"desc";s:18:"gallery.album_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:3:"tag";a:7:{s:4:"name";s:3:"tag";s:4:"desc";s:16:"gallery.tag_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:6:"plugin";a:7:{s:4:"name";s:6:"plugin";s:4:"desc";s:19:"gallery.plugin_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:10:"pluginPath";a:7:{s:4:"name";s:10:"pluginPath";s:4:"desc";s:23:"gallery.pluginpath_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:8:"thumbTpl";a:7:{s:4:"name";s:8:"thumbTpl";s:4:"desc";s:21:"gallery.thumbtpl_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:12:"galItemThumb";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:12:"containerTpl";a:7:{s:4:"name";s:12:"containerTpl";s:4:"desc";s:25:"gallery.containertpl_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:7:"itemCls";a:7:{s:4:"name";s:7:"itemCls";s:4:"desc";s:20:"gallery.itemcls_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:8:"gal-item";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:9:"activeCls";a:7:{s:4:"name";s:9:"activeCls";s:4:"desc";s:22:"gallery.activecls_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:15:"gal-item-active";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:13:"toPlaceholder";a:7:{s:4:"name";s:13:"toPlaceholder";s:4:"desc";s:26:"gallery.toplaceholder_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:10:"thumbWidth";a:7:{s:4:"name";s:10:"thumbWidth";s:4:"desc";s:23:"gallery.thumbwidth_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:3:"100";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:11:"thumbHeight";a:7:{s:4:"name";s:11:"thumbHeight";s:4:"desc";s:24:"gallery.thumbheight_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:3:"100";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:13:"thumbZoomCrop";a:7:{s:4:"name";s:13:"thumbZoomCrop";s:4:"desc";s:26:"gallery.thumbzoomcrop_desc";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:1;s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:8:"thumbFar";a:7:{s:4:"name";s:8:"thumbFar";s:4:"desc";s:21:"gallery.thumbfar_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:1:"C";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:12:"thumbQuality";a:7:{s:4:"name";s:12:"thumbQuality";s:4:"desc";s:25:"gallery.thumbquality_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";i:90;s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:15:"thumbProperties";a:7:{s:4:"name";s:15:"thumbProperties";s:4:"desc";s:28:"gallery.thumbproperties_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:11:"linkToImage";a:7:{s:4:"name";s:11:"linkToImage";s:4:"desc";s:24:"gallery.linktoimage_desc";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:13:"imageGetParam";a:7:{s:4:"name";s:13:"imageGetParam";s:4:"desc";s:26:"gallery.imagegetparam_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:7:"galItem";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:10:"imageWidth";a:7:{s:4:"name";s:10:"imageWidth";s:4:"desc";s:23:"gallery.imagewidth_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";i:500;s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:11:"imageHeight";a:7:{s:4:"name";s:11:"imageHeight";s:4:"desc";s:24:"gallery.imageheight_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";i:500;s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:13:"imageZoomCrop";a:7:{s:4:"name";s:13:"imageZoomCrop";s:4:"desc";s:26:"gallery.imagezoomcrop_desc";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:0;s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:8:"imageFar";a:7:{s:4:"name";s:8:"imageFar";s:4:"desc";s:21:"gallery.imagefar_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:12:"imageQuality";a:7:{s:4:"name";s:12:"imageQuality";s:4:"desc";s:25:"gallery.imagequality_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";i:90;s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:15:"imageProperties";a:7:{s:4:"name";s:15:"imageProperties";s:4:"desc";s:28:"gallery.imageproperties_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:4:"sort";a:7:{s:4:"name";s:4:"sort";s:4:"desc";s:17:"gallery.sort_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:4:"rank";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:3:"dir";a:7:{s:4:"name";s:3:"dir";s:4:"desc";s:16:"gallery.dir_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:3:"ASC";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:5:"limit";a:7:{s:4:"name";s:5:"limit";s:4:"desc";s:18:"gallery.limit_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";i:0;s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:5:"start";a:7:{s:4:"name";s:5:"start";s:4:"desc";s:18:"gallery.start_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";i:0;s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:12:"showInactive";a:7:{s:4:"name";s:12:"showInactive";s:4:"desc";s:25:"gallery.showinactive_desc";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:0;s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:23:"checkForRequestAlbumVar";a:7:{s:4:"name";s:23:"checkForRequestAlbumVar";s:4:"desc";s:36:"gallery.checkforrequestalbumvar_desc";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:1;s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:15:"albumRequestVar";a:7:{s:4:"name";s:15:"albumRequestVar";s:4:"desc";s:28:"gallery.albumrequestvar_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:8:"galAlbum";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:21:"checkForRequestTagVar";a:7:{s:4:"name";s:21:"checkForRequestTagVar";s:4:"desc";s:34:"gallery.checkforrequesttagvar_desc";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:1;s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:13:"tagRequestVar";a:7:{s:4:"name";s:13:"tagRequestVar";s:4:"desc";s:26:"gallery.tagrequestvar_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:6:"galTag";s:7:"lexicon";N;s:4:"area";s:0:"";}s:6:"useCss";a:7:{s:4:"name";s:6:"useCss";s:4:"desc";s:19:"gallery.usecss_desc";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:1;s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}}', '', 0, ''),
(12, 0, 0, 'GalleryAlbums', '', 0, 9, 0, '/**\n * Gallery\n *\n * Copyright 2010-2012 by Shaun McCormick <shaun@modx.com>\n *\n * Gallery is free software; you can redistribute it and/or modify it under the\n * terms of the GNU General Public License as published by the Free Software\n * Foundation; either version 2 of the License, or (at your option) any later\n * version.\n *\n * Gallery is distributed in the hope that it will be useful, but WITHOUT ANY\n * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR\n * A PARTICULAR PURPOSE. See the GNU General Public License for more details.\n *\n * You should have received a copy of the GNU General Public License along with\n * Gallery; if not, write to the Free Software Foundation, Inc., 59 Temple\n * Place, Suite 330, Boston, MA 02111-1307 USA\n *\n * @package gallery\n */\n/**\n * Loads a list of Albums\n *\n * @var modX $modx\n * @var Gallery $gallery\n * @package gallery\n */\n$gallery = $modx->getService(''gallery'',''Gallery'',$modx->getOption(''gallery.core_path'',null,$modx->getOption(''core_path'').''components/gallery/'').''model/gallery/'',$scriptProperties);\nif (!($gallery instanceof Gallery)) return '''';\n\n/* setup default properties */\n$rowTpl = $modx->getOption(''rowTpl'',$scriptProperties,''galAlbumRowTpl'');\n$rowCls = $modx->getOption(''rowCls'',$scriptProperties,'''');\n$toPlaceholder = $modx->getOption(''toPlaceholder'',$scriptProperties,false);\n$showAll = $modx->getOption(''showAll'',$scriptProperties,false);\n$albumRequestVar = $modx->getOption(''albumRequestVar'',$scriptProperties,''galAlbum'');\n$albumCoverSort = $modx->getOption(''albumCoverSort'',$scriptProperties,''rank'');\n$albumCoverSortDir = $modx->getOption(''albumCoverSortDir'',$scriptProperties,''ASC'');\n$showName = $modx->getOption(''showName'',$scriptProperties,true);\n\n$totalProperties = $scriptProperties;\n$totalProperties[''limit''] = ''0'';\n$totalProperties[''start''] = ''0'';\n$totalAlbums = $modx->call(''galAlbum'', ''getList'', array(&$modx, $totalProperties));\n$totalVar = $modx->getOption(''totalVar'', $scriptProperties, ''total'');\n$modx->setPlaceholder($totalVar, count($totalAlbums));\n\n/* build query */\n$albums = $modx->call(''galAlbum'',''getList'',array(&$modx,$scriptProperties));\n\n/* handle sorting for album cover */\nif ($albumCoverSort == ''rank'') {\n    $albumCoverSort = ''AlbumItems.rank'';\n}\nif (in_array(strtolower($albumCoverSort),array(''random'',''rand()'',''rand''))) {\n    $albumCoverSort = ''RAND()'';\n    $albumCoverSortDir = '''';\n}\n\n/* get thumb properties for album cover */\n$thumbProperties = $modx->getOption(''thumbProperties'',$scriptProperties,'''');\n$thumbProperties = !empty($thumbProperties) ? $modx->fromJSON($thumbProperties) : array();\n$thumbProperties = array_merge(array(\n    ''w'' => (int)$modx->getOption(''thumbWidth'',$scriptProperties,100),\n    ''h'' => (int)$modx->getOption(''thumbHeight'',$scriptProperties,100),\n    ''zc'' => (string)$modx->getOption(''thumbZoomCrop'',$scriptProperties,1),\n    ''far'' => (string)$modx->getOption(''thumbFar'',$scriptProperties,''C''),\n    ''q'' => (int)$modx->getOption(''thumbQuality'',$scriptProperties,90),\n),$thumbProperties);\n\n/* iterate */\n$output = array();\n$idx = 0;\n$filesUrl = $modx->call(''galAlbum'',''getFilesUrl'',array(&$modx));\n$nav = array();\n/** @var galAlbum $album */\nforeach ($albums as $album) {\n    $albumArray = $album->toArray();\n    $classes = array($rowCls);\n\n    if (!isset($nav[''first''])) {\n        $nav[''first''] = $albumArray[''id''];\n    }\n    if (!isset($nav[''next'']) && isset($nav[''current''])) {\n        $nav[''next''] = $albumArray[''id''];\n    }\n    if ($_GET[$albumRequestVar] == $albumArray[''id'']) {\n        $nav[''current''] = $albumArray[''id''];\n        $nav[''curIdx''] = $idx + 1;\n        $classes[] = ''current'';\n    }\n    if (!isset($nav[''current''])) {\n        $nav[''prev''] = $albumArray[''id''];\n    }\n    $nav[''last''] = $albumArray[''id''];\n\n    $albumArray[''cls''] = implode('' '', $classes);\n    $albumArray[''idx''] = $idx;\n    $albumArray[''showName''] = $showName;\n    $albumArray[''albumRequestVar''] = $albumRequestVar;\n    $coverItem = $album->getCoverItem($albumCoverSort,$albumCoverSortDir);\n    if ($coverItem) {\n        $albumArray[''image''] = $coverItem->get(''thumbnail'',$thumbProperties);\n        $albumArray[''image_absolute''] = $filesUrl.$coverItem->get(''filename'');\n        $albumArray[''total''] = $coverItem->get(''total'');\n    }\n\n    $albumArray[''cls''] = implode('' '', $classes);\n    $albumArray[''idx''] = $idx;\n    $albumArray[''showName''] = $showName;\n    $albumArray[''albumRequestVar''] = $albumRequestVar;\n    $output[] = $gallery->getChunk($rowTpl,$albumArray);\n    $idx++;\n}\nif (!isset($nav[''current''])) {\n    unset($nav[''prev'']);\n}\n$nav[''count''] = $idx;\n\n/* set output to placeholder or return */\n$outputSeparator = $modx->getOption(''outputSeparator'',$scriptProperties,"\\n");\n$output = implode($outputSeparator,$output);\n\n/* if set, place in a container tpl */\n$containerTpl = $modx->getOption(''containerTpl'',$scriptProperties,false);\nif (!empty($containerTpl)) {\n    $ct = $gallery->getChunk($containerTpl,array(\n        ''albums'' => $output,\n        ''nav'' => $nav,\n        ''albumRequestVar'' => $albumRequestVar\n    ));\n    if (!empty($ct)) $output = $ct;\n}\n\nif ($toPlaceholder) {\n    $modx->setPlaceholder($toPlaceholder,$output);\n    return '''';\n}\nreturn $output;', 0, 'a:21:{s:6:"rowTpl";a:7:{s:4:"name";s:6:"rowTpl";s:4:"desc";s:25:"galleryalbums.rowtpl_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:14:"galAlbumRowTpl";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:6:"rowCls";a:7:{s:4:"name";s:6:"rowCls";s:4:"desc";s:25:"galleryalbums.rowcls_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:4:"sort";a:7:{s:4:"name";s:4:"sort";s:4:"desc";s:23:"galleryalbums.sort_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:4:"rank";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:3:"dir";a:7:{s:4:"name";s:3:"dir";s:4:"desc";s:22:"galleryalbums.dir_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:4:"DESC";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:5:"limit";a:7:{s:4:"name";s:5:"limit";s:4:"desc";s:24:"galleryalbums.limit_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:2:"10";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:5:"start";a:7:{s:4:"name";s:5:"start";s:4:"desc";s:24:"galleryalbums.start_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:1:"0";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:13:"toPlaceholder";a:7:{s:4:"name";s:13:"toPlaceholder";s:4:"desc";s:32:"galleryalbums.toplaceholder_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:12:"showInactive";a:7:{s:4:"name";s:12:"showInactive";s:4:"desc";s:31:"galleryalbums.showinactive_desc";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:0;s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:13:"prominentOnly";a:7:{s:4:"name";s:13:"prominentOnly";s:4:"desc";s:32:"galleryalbums.prominentonly_desc";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:1;s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:6:"parent";a:7:{s:4:"name";s:6:"parent";s:4:"desc";s:25:"galleryalbums.parent_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:1:"0";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:7:"showAll";a:7:{s:4:"name";s:7:"showAll";s:4:"desc";s:26:"galleryalbums.showall_desc";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:1;s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:8:"showName";a:7:{s:4:"name";s:8:"showName";s:4:"desc";s:27:"galleryalbums.showname_desc";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:1;s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:15:"albumRequestVar";a:7:{s:4:"name";s:15:"albumRequestVar";s:4:"desc";s:34:"galleryalbums.albumrequestvar_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:8:"galAlbum";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:14:"albumCoverSort";a:7:{s:4:"name";s:14:"albumCoverSort";s:4:"desc";s:33:"galleryalbums.albumcoversort_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:4:"rank";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:17:"albumCoverSortDir";a:7:{s:4:"name";s:17:"albumCoverSortDir";s:4:"desc";s:36:"galleryalbums.albumcoversortdir_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:3:"ASC";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:10:"thumbWidth";a:7:{s:4:"name";s:10:"thumbWidth";s:4:"desc";s:29:"galleryalbums.thumbwidth_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:3:"100";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:11:"thumbHeight";a:7:{s:4:"name";s:11:"thumbHeight";s:4:"desc";s:30:"galleryalbums.thumbheight_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:3:"100";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:13:"thumbZoomCrop";a:7:{s:4:"name";s:13:"thumbZoomCrop";s:4:"desc";s:32:"galleryalbums.thumbzoomcrop_desc";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:1;s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:8:"thumbFar";a:7:{s:4:"name";s:8:"thumbFar";s:4:"desc";s:27:"galleryalbums.thumbfar_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:1:"C";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:12:"thumbQuality";a:7:{s:4:"name";s:12:"thumbQuality";s:4:"desc";s:31:"galleryalbums.thumbquality_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";i:90;s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:15:"thumbProperties";a:7:{s:4:"name";s:15:"thumbProperties";s:4:"desc";s:34:"galleryalbums.thumbproperties_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}}', '', 0, '');
INSERT INTO `modx_site_snippets` (`id`, `source`, `property_preprocess`, `name`, `description`, `editor_type`, `category`, `cache_type`, `snippet`, `locked`, `properties`, `moduleguid`, `static`, `static_file`) VALUES
(13, 0, 0, 'GalleryItem', '', 0, 9, 0, '/**\n * Gallery\n *\n * Copyright 2010-2012 by Shaun McCormick <shaun@modx.com>\n *\n * Gallery is free software; you can redistribute it and/or modify it under the\n * terms of the GNU General Public License as published by the Free Software\n * Foundation; either version 2 of the License, or (at your option) any later\n * version.\n *\n * Gallery is distributed in the hope that it will be useful, but WITHOUT ANY\n * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR\n * A PARTICULAR PURPOSE. See the GNU General Public License for more details.\n *\n * You should have received a copy of the GNU General Public License along with\n * Gallery; if not, write to the Free Software Foundation, Inc., 59 Temple\n * Place, Suite 330, Boston, MA 02111-1307 USA\n *\n * @package gallery\n */\n/**\n * Display a single Gallery Item\n *\n * @package gallery\n */\n$gallery = $modx->getService(''gallery'',''Gallery'',$modx->getOption(''gallery.core_path'',null,$modx->getOption(''core_path'').''components/gallery/'').''model/gallery/'',$scriptProperties);\nif (!($gallery instanceof Gallery)) return '''';\n\n/* get ID of item */\n$id = (int)$modx->getOption(''id'',$scriptProperties,false);\nif ($modx->getOption(''checkForRequestVar'',$scriptProperties,true)) {\n    $getParam = $modx->getOption(''getParam'',$scriptProperties,''galItem'');\n    if (!empty($_REQUEST[$getParam])) { $id = (int)$_REQUEST[$getParam]; }\n}\nif (empty($id)) return '''';\n\n/* setup default properties */\n$tpl = $modx->getOption(''tpl'',$scriptProperties,''galItem'');\n$toPlaceholders = $modx->getOption(''toPlaceholders'',$scriptProperties,true);\n$toPlaceholdersPrefix = $modx->getOption(''toPlaceholdersPrefix'',$scriptProperties,''galitem'');\n$albumTpl = $modx->getOption(''albumTpl'',$scriptProperties,''galItemAlbum'');\n$albumSeparator = $modx->getOption(''albumSeparator'',$scriptProperties,'',&nbsp;'');\n$albumRequestVar = $modx->getOption(''albumRequestVar'',$scriptProperties,''galAlbum'');\n$tagTpl = $modx->getOption(''tagTpl'',$scriptProperties,''galItemTag'');\n$tagSeparator = $modx->getOption(''tagSeparator'',$scriptProperties,'',&nbsp;'');\n$tagSortDir = $modx->getOption(''tagSortDir'',$scriptProperties,''DESC'');\n$tagRequestVar = $modx->getOption(''tagRequestVar'',$scriptProperties,''galTag'');\n/* get item */\n$c = $modx->newQuery(''galItem'');\n$c->select($modx->getSelectColumns(''galItem'',''galItem''));\n$c->where(array(\n    ''id'' => $id,\n));\n$item = $modx->getObject(''galItem'',$c);\nif (empty($item)) return '''';\n\n/* setup placeholders */\n$itemArray = $item->toArray();\n$itemArray[''filename''] = basename($item->get(''filename''));\n$itemArray[''filesize''] = $item->get(''filesize'');\n\n/* get image+thumbnail */\n$thumbProperties = $modx->getOption(''thumbProperties'',$scriptProperties,'''');\n$thumbProperties = !empty($thumbProperties) ? $modx->fromJSON($thumbProperties) : array();\n$thumbProperties = array_merge(array(\n    ''w'' => (int)$modx->getOption(''thumbWidth'',$scriptProperties,100),\n    ''h'' => (int)$modx->getOption(''thumbHeight'',$scriptProperties,100),\n    ''zc'' => (boolean)$modx->getOption(''thumbZoomCrop'',$scriptProperties,0),\n    ''far'' => (string)$modx->getOption(''thumbFar'',$scriptProperties,''C''),\n    ''q'' => (int)$modx->getOption(''thumbQuality'',$scriptProperties,90),\n),$thumbProperties);\n$itemArray[''thumbnail''] = $item->get(''thumbnail'',$thumbProperties);\n\n$imageProperties = $modx->getOption(''imageProperties'',$scriptProperties,'''');\n$imageProperties = !empty($imageProperties) ? $modx->fromJSON($imageProperties) : array();\n$imageProperties = array_merge(array(\n    ''w'' => (int)$modx->getOption(''imageWidth'',$scriptProperties,500),\n    ''h'' => (int)$modx->getOption(''imageHeight'',$scriptProperties,500),\n    ''zc'' => (boolean)$modx->getOption(''imageZoomCrop'',$scriptProperties,0),\n    ''far'' => (string)$modx->getOption(''imageFar'',$scriptProperties,false),\n    ''q'' => (int)$modx->getOption(''imageQuality'',$scriptProperties,90),\n),$imageProperties);\n$itemArray[''image''] = $item->get(''thumbnail'',$imageProperties);\n\n/* get albums */\n$c = $modx->newQuery(''galAlbum'');\n$c->innerJoin(''galAlbumItem'',''AlbumItems'',$modx->getSelectColumns(''galAlbumItem'',''AlbumItems'','''',array(''album'')).'' = ''.$modx->getSelectColumns(''galAlbum'',''galAlbum'','''',array(''id''))\n    .'' AND ''.$modx->getSelectColumns(''galAlbumItem'',''AlbumItems'','''',array(''item'')).'' = ''.$item->get(''id''));\n$c->sortby(''AlbumItems.rank'',''ASC'');\n$albums = $modx->getCollection(''galAlbum'',$c);\n$itemArray[''albums''] = array();\n$i = 0;\nforeach ($albums as $album) {\n    $albumArray = $album->toArray('''',true,true);\n    $albumArray[''idx''] = $i;\n    $albumArray[''albumRequestVar''] = $albumRequestVar;\n    $itemArray[''albums''][] = $gallery->getChunk($albumTpl,$albumArray);\n    $i++;\n}\n$itemArray[''albums''] = implode($albumSeparator,$itemArray[''albums'']);\n\n/* get tags */\n$c = $modx->newQuery(''galTag'');\n$c->where(array(\n    ''item'' => $item->get(''id''),\n));\n$c->sortby(''tag'',$tagSortDir);\n$tags = $modx->getCollection(''galTag'',$c);\n$i = 0;\n$itemArray[''tags''] = array();\nforeach ($tags as $tag) {\n    $tagArray = $tag->toArray();\n    $tagArray[''idx''] = $i;\n    $tagArray[''tagRequestVar''] = $tagRequestVar;\n    $itemArray[''tags''][] = $gallery->getChunk($tagTpl,$tagArray);\n    $i++;\n}\n$itemArray[''tags''] = implode($tagSeparator,$itemArray[''tags'']);\n\n/* if outputting to placeholders, use this, otherwise, use tpl */\nif ($toPlaceholders) {\n    $modx->toPlaceholders($itemArray,$toPlaceholdersPrefix);\n    return '''';\n}\n\nif (empty($tpl)) return '''';\n$output .= $gallery->getChunk($tpl,$itemArray);\nreturn $output;', 0, 'a:23:{s:2:"id";a:7:{s:4:"name";s:2:"id";s:4:"desc";s:19:"galleryitem.id_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:14:"toPlaceholders";a:7:{s:4:"name";s:14:"toPlaceholders";s:4:"desc";s:31:"galleryitem.toplaceholders_desc";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:1;s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:20:"toPlaceholdersPrefix";a:7:{s:4:"name";s:20:"toPlaceholdersPrefix";s:4:"desc";s:37:"galleryitem.toplaceholdersprefix_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:7:"galitem";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:3:"tpl";a:7:{s:4:"name";s:3:"tpl";s:4:"desc";s:20:"galleryitem.tpl_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:7:"galItem";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:8:"albumTpl";a:7:{s:4:"name";s:8:"albumTpl";s:4:"desc";s:25:"galleryitem.albumtpl_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:12:"galItemAlbum";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:14:"albumSeparator";a:7:{s:4:"name";s:14:"albumSeparator";s:4:"desc";s:31:"galleryitem.albumseparator_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:7:",&nbsp;";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:15:"albumRequestVar";a:7:{s:4:"name";s:15:"albumRequestVar";s:4:"desc";s:32:"galleryitem.albumrequestvar_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:8:"galAlbum";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:6:"tagTpl";a:7:{s:4:"name";s:6:"tagTpl";s:4:"desc";s:23:"galleryitem.tagtpl_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:10:"galItemTag";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:12:"tagSeparator";a:7:{s:4:"name";s:12:"tagSeparator";s:4:"desc";s:29:"galleryitem.tagseparator_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:7:",&nbsp;";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:10:"tagSortDir";a:7:{s:4:"name";s:10:"tagSortDir";s:4:"desc";s:27:"galleryitem.tagsortdir_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:4:"DESC";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:13:"tagRequestVar";a:7:{s:4:"name";s:13:"tagRequestVar";s:4:"desc";s:30:"galleryitem.tagrequestvar_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:6:"galTag";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:10:"thumbWidth";a:7:{s:4:"name";s:10:"thumbWidth";s:4:"desc";s:27:"galleryitem.thumbwidth_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:3:"100";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:11:"thumbHeight";a:7:{s:4:"name";s:11:"thumbHeight";s:4:"desc";s:28:"galleryitem.thumbheight_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:3:"100";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:13:"thumbZoomCrop";a:7:{s:4:"name";s:13:"thumbZoomCrop";s:4:"desc";s:30:"galleryitem.thumbzoomcrop_desc";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:1;s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:8:"thumbFar";a:7:{s:4:"name";s:8:"thumbFar";s:4:"desc";s:25:"galleryitem.thumbfar_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:1:"C";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:12:"thumbQuality";a:7:{s:4:"name";s:12:"thumbQuality";s:4:"desc";s:29:"galleryitem.thumbquality_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";i:90;s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:15:"thumbProperties";a:7:{s:4:"name";s:15:"thumbProperties";s:4:"desc";s:32:"galleryitem.thumbproperties_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:10:"imageWidth";a:7:{s:4:"name";s:10:"imageWidth";s:4:"desc";s:27:"galleryitem.imagewidth_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:11:"imageHeight";a:7:{s:4:"name";s:11:"imageHeight";s:4:"desc";s:28:"galleryitem.imageheight_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:13:"imageZoomCrop";a:7:{s:4:"name";s:13:"imageZoomCrop";s:4:"desc";s:30:"galleryitem.imagezoomcrop_desc";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:0;s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:8:"imageFar";a:7:{s:4:"name";s:8:"imageFar";s:4:"desc";s:25:"galleryitem.imagefar_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:12:"imageQuality";a:7:{s:4:"name";s:12:"imageQuality";s:4:"desc";s:29:"galleryitem.imagequality_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";i:90;s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}s:15:"imageProperties";a:7:{s:4:"name";s:15:"imageProperties";s:4:"desc";s:32:"galleryitem.imageproperties_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:18:"gallery:properties";s:4:"area";s:0:"";}}', '', 0, '');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_site_templates`
--

CREATE TABLE `modx_site_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `source` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `property_preprocess` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `templatename` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT 'Template',
  `editor_type` int(11) NOT NULL DEFAULT '0',
  `category` int(11) NOT NULL DEFAULT '0',
  `icon` varchar(255) NOT NULL DEFAULT '',
  `template_type` int(11) NOT NULL DEFAULT '0',
  `content` mediumtext NOT NULL,
  `locked` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `properties` text,
  `static` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `static_file` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_site_templates`
--

INSERT INTO `modx_site_templates` (`id`, `source`, `property_preprocess`, `templatename`, `description`, `editor_type`, `category`, `icon`, `template_type`, `content`, `locked`, `properties`, `static`, `static_file`) VALUES
(1, 1, 0, 'Главная страница', 'Шаблон для главной страницы', 0, 0, '', 0, '<!doctype html>\n<html>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge">\n    <title>[[*pagetitle]]</title>\n    \n    [[!$meta?]]\n\n\n    <link rel="stylesheet" href="css/main.css">\n    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">\n    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css">\n\n    <script src="https://yastatic.net/jquery/2.2.3/jquery.min.js"></script>\n    <script src="js/mustache.min.js" type="text/javascript"></script>\n\n\n    \n\n</head>\n<body>\n\n[[!$header?]]\n\n\n<section class="first-screen">\n\n    <div class="container">\n\n        <div id="line_0_container" data-0="height:25px; width:0px;" data-100="height:25px; width: 980px;" data-150="height:80px; width: 980px;">\n            <div class="line"></div>\n        </div>\n\n        <h1 class="title">Лучший сервис по быстрому подбору франшиз</h1>\n        <h2 class="subtitle">Более 1000 безопасных и проверенных франшиз на сервисе</h2>\n    </div>\n\n    <div class="wrapper">\n\n        <div class="container">\n            <div class="filter">\n                <h4>Быстрый поиск франшиз</h4>\n\n                <form action="/" method="post">\n                    <div class="param">\n                        <label for="invest_range">Инвестиции, тыс. руб.</label>\n                        <input type="text" id="invest_range" name="invest_range" class="range_slider" value="" />\n                    </div>\n\n                    <div class="param">\n                        <label for="cashback_range">Срок окупаемости, мес.</label>\n                        <input type="text" id="cashback_range" name="cashback_range" class="range_slider" value="" />\n                    </div>\n\n                    <div class="param param_selector">\n                        <a href="#" class="select_business">Вид бизнеса</a>\n                        <div class="cat_accordion">\n                            <div class="cat_title">Категория 1</div>\n                            <div class="cat_container" style="display: block;">\n                                <label for="elem_1">\n                                    <input id="elem_1" type="checkbox" value="cat_1"> Элемент 1\n                                </label>\n\n                                <label for="elem_2">\n                                    <input id="elem_2" type="checkbox" value="cat_1"> Элемент 2\n                                </label>\n\n                                <label for="elem_3">\n                                    <input id="elem_3" type="checkbox" value="cat_1"> Элемент 3\n                                </label>\n                            </div>\n\n\n                            <div class="cat_title">Категория 2</div>\n                            <div class="cat_container">\n                                <label for="elem_4">\n                                    <input id="elem_4" type="checkbox" value="cat_1"> Элемент 1\n                                </label>\n\n                                <label for="elem_5">\n                                    <input id="elem_5" type="checkbox" value="cat_1"> Элемент 2\n                                </label>\n\n                                <label for="elem_6">\n                                    <input id="elem_6" type="checkbox" value="cat_1"> Элемент 3\n                                </label>\n                            </div>\n                        </div>\n                    </div>\n\n                    <button type="submit" class="btn btn-outline">\n                        <span class="text">Подобрать</span>\n                        <span class="hover"></span>\n                    </button>\n                    <a href="/" class="link">Расширенный поиск</a>\n\n                </form>\n            </div> <!-- filter end -->\n            <div class="slider">\n                <div class="slide">\n                    <img src="img/slide1.jpg" alt="">\n                </div>\n                <div class="slide">\n                    <img src="img/slide1.jpg" alt="">\n                </div>\n                <div class="slide">\n                    <img src="img/slide1.jpg" alt="">\n                </div>\n            </div>\n\n            <div class="clearfix"></div>\n        </div>\n\n    </div><!-- wrapper end -->\n    \n    <div class="container">\n\n        <div id="line_1_container" data-250="height:0px; width:25px;" data-350="height:75px; width: 25px;" data-400="height:75px; width: 600px;">\n            <div class="line"></div>\n        </div>\n\n        <img src="img/instruction.png" alt="" class="instruct">\n\n        <div class="get-checklist">\n            Получить инструкцию и чек-лист: <br>\n            <span class="bold">«Как не купить не прибыльную <br>\n            франшизу и не потерять свои деньги»</span>\n        </div>\n\n        <a href="/" class="btn btn-outline">\n            <span class="text">Получить инструкцию и чек-лист</span>\n            <span class="hover"></span>\n        </a>\n    </div>\n</section>\n\n\n<hr class="hr-line">\n\n\n<section class="bullets">\n    <div class="container">\n        <div class="item">\n            <div class="ico">\n                <span class="ico1"></span>\n            </div>\n            <div class="descript">\n                <span class="bold">5 лет</span> на рынке франчайзинга\n            </div>\n        </div>\n\n        <div class="item">\n            <div class="ico">\n                <span class="ico2"></span>\n            </div>\n            <div class="descript">\n                <span class="bold">17 специалистов-</span>\n                консультантов\n                по подбору\n            </div>\n        </div>\n        <div class="item">\n            <div class="ico">\n                <span class="ico3"></span>\n            </div>\n            <div class="descript">\n                <span class="bold">70 направлений</span> бизнеса\n            </div>\n        </div>\n        <div class="item">\n            <div class="ico">\n                <span class="ico4"></span>\n            </div>\n            <div class="descript">\n                <span class="bold">100</span> уникальных <span class="bold">предложений</span>\n            </div>\n        </div>\n        <div class="item">\n            <div class="ico">\n                <span class="ico5"></span>\n            </div>\n            <div class="descript">\n                <span class="bold">7000 франшиз</span>\n                в каталоге\n            </div>\n        </div>\n    </div>\n</section>\n\n\n<section class="catalog">\n\n    <div id="line_2_container" data-500="height:0px; width:0px;" data-1150="height:600px; width: 530px;">\n        <div class="line"></div>\n    </div>\n\n    <div class="container">\n        <h1 class="title">Каталог франшиз</h1>\n        <div class="cat_filter_mini">\n            \n            [[!Wayfinder?\n                &startId=`14`\n            	&level=`1`\n            	&cacheResults=`0`\n            	&limit=`3`\n            	&sortOrder=`ASC`\n            	&rowTpl=`home.package_link`\n            	&outerTpl=`home.package_nav_container`\n            ]]\n            \n            <div class="hamburger_nav">\n                <button class="hamburger_btn" id="catalog_hamburger_btn">\n                    <i class="fa fa-bars" aria-hidden="true"></i>\n                </button>\n                <div class="hamburger_nav_container">\n                    <ul>\n                        [[getResources?\n                        	&tpl=`home.hamburger_package_link`\n                        	&limit=`0`\n                        	&offset=`3`\n                        	&sortby=`menuindex`\n                        	&sortdir=`ASC`\n                        	&sortdirTV=`ASC`\n                        	&depth=`1`\n                        	&parents=`14`\n                        	&includeTVs=`1`\n                        	&processTVs=`1`\n                        ]]\n                    </ul>\n                </div>\n            </div>\n        </div>\n\n        \n        <div class="catalog_wrapper">\n        \n            \n        </div><!-- cat wrapper end -->\n\n        <a href="[[~2]]" class="btn btn-outline more_fr">\n            Посмотреть больше франшиз\n        </a>\n        \n    </div>\n</section>\n\n\n\n<section class="about">\n    <div class="container">\n        <h1 class="title">\n            franchising.ru – эффективный сервис <br>\n            для франчайзеров\n        </h1>\n\n        <div class="descripts">\n            <div class="elem">\n                <h3 class="elem_title">\n                    vel illum qui dolorem eum <br>\n                    fugiat quo voluptas\n                </h3>\n                <p class="text">\n                    Porro quisquam est, qui dolorem ipsum, quia dolor sit, amet, consectetur, adipisci velit, sed quia\n                    non numquam eius modi tempora incidunt, ut labore et dolore magnam aliquam quaerat voluptatem.\n                </p>\n            </div>\n            <div class="elem">\n                <h3 class="elem_title">\n                    llum qui dolorem eum\n                </h3>\n                <p class="text">\n                    Porro quisquam est, qui dolorem ipsum, quia dolor sit, amet, consectetur, adipisci velit, sed quia\n                    non numquam eius modi tempora incidunt, ut labore et dolore magnam aliquam quaerat voluptatem.\n                </p>\n            </div>\n            <div class="elem">\n                <h3 class="elem_title">\n                    vel illum qui dolorem eum <br>\n                    fugiat quo voluptas\n                </h3>\n                <p class="text">\n                    Porro quisquam est, qui dolorem ipsum, quia dolor sit, amet, consectetur, adipisci velit, sed quia\n                    non numquam eius modi tempora incidunt, ut labore et dolore magnam aliquam quaerat voluptatem.\n                </p>\n            </div>\n\n            <a class="btn btn-outline">Подать заявку на публикацию в каталоге</a>\n\n        </div>\n        \n        <div class="sphere">\n            <div class="logo">\n                <div class="particles">\n                    <div class="particle_1"><img class="part" src="img/boll_elem_1.svg" alt=""></div>\n                    <div class="particle_2"><img class="part" src="img/boll_elem_2.svg" alt=""></div>\n                    <div class="particle_3"><img class="part" src="img/boll_elem_3.svg" alt=""></div>\n                </div>\n                <img src="img/logo.svg" class="logo_img" alt="">\n            </div>\n        </div>\n\n        <div class="clearfix"></div>\n        \n    </div>\n</section>\n\n\n\n<section class="other_services">\n    <div class="container">\n        <h1 class="title">оказываем доп. услуги</h1>\n        <div class="services_container">\n\n            <div class="service">\n                <a href="">\n                    <div class="cover">\n                        <img src="img/serv_1.png" alt="">\n                    </div>\n                </a>\n                <div class="text">\n                    <a href="">\n                        <h4 class="service_name">Упаковка франшизы</h4>\n                    </a>\n                    <p class="service_descript">\n                        As part of an ongoing relationship with Google’s retail team, we were tasked with helping them\n                        better\n                        understand a customer''s path to purchase and brand relationship.\n                    </p>\n                    <a href="" class="more">Подробнее</a>\n                </div>\n            </div>\n\n            <div class="service">\n                <div class="cover">\n                    <img src="img/serv2.png" alt="">\n                </div>\n                <div class="text">\n                    <h4 class="service_name">Упаковка франшизы</h4>\n                    <p class="service_descript">\n                        As part of an ongoing relationship with Google’s retail team, we were tasked with helping them\n                        better\n                        understand a customer''s path to purchase and brand relationship.\n                    </p>\n                    <a href="" class="more">Подробнее</a>\n                </div>\n            </div>\n\n            <div class="service">\n                <div class="cover">\n                    <img src="img/serv3.png" alt="">\n                </div>\n                <div class="text">\n                    <h4 class="service_name">Упаковка франшизы</h4>\n                    <p class="service_descript">\n                        As part of an ongoing relationship with Google’s retail team, we were tasked with helping them\n                        better\n                        understand a customer''s path to purchase and brand relationship.\n                    </p>\n                    <a href="" class="more">Подробнее</a>\n                </div>\n            </div>\n\n            <div class="service">\n                <a href="">\n                    <div class="cover">\n                        <img src="img/serv4.png" alt="">\n                    </div>\n                </a>\n                <div class="text">\n                    <h4 class="service_name">Упаковка франшизы</h4>\n                    <p class="service_descript">\n                        As part of an ongoing relationship with Google’s retail team, we were tasked with helping them better\n                        understand a customer''s path to purchase and brand relationship.\n                    </p>\n                    <a href="" class="more">Подробнее</a>\n                </div>\n            </div>\n        </div>\n    </div>\n</section>\n\n\n\n<section class="reviews">\n    <div class="container">\n        <h2 class="title">Отзывы наших клиентов</h2>\n\n        <div class="reviews_slider">\n            <div class="slide">\n                <div class="client">\n                    <img src="img/client.jpg" alt="">\n                </div>\n                <div class="text">\n                    <div class="name">Степаненко Антон Геннадьевич,</div>\n                    <span class="fr_type">франшиза Фитнес-клубы FitSpot</span>\n                    <p>\n                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod\n                        tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis\n                        nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.\n                        Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel\n                        illum dolore eu feugiat nulla facilisis at vero eros et accumsan.\n                    </p>\n                </div>\n            </div>\n            <div class="slide">\n                <div class="client">\n                </div>\n                <div class="text">\n                    <div class="name">Степаненко Антон Геннадьевич,</div>\n                    <span>франшиза Фитнес-клубы FitSpot</span>\n                    <p>\n                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod\n                        tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis\n                        nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.\n                        Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel\n                        illum dolore eu feugiat nulla facilisis at vero eros et accumsan.\n                    </p>\n                </div>\n            </div>\n            <div class="slide"></div>\n            <div class="slide"></div>\n            <div class="slide"></div>\n        </div>\n\n    </div>\n</section>\n\n\n\n\n\n\n\n<section class="contacts">\n    <div class="container">\n\n        <div id="contacts_line_1"  data-200-end="height:0px; width: 25px;" data-100-end="height:70px; width: 25px;" data-end="height:70px; width: 250px;">\n            <div class="line"></div>\n        </div>\n\n\n        <div id="contacts_line_2"  data-200-end="height:70px; width: 0px;" data-100-end="height:70px; width: 600px;" data-end="height:270px; width: 600px;">\n            <div class="line"></div>\n        </div>\n\n\n        <div class="phones">\n            <b class="phones_title">Телефон в Москве:</b>\n\n            <div class="phones_wrapper">\n                <div class="phone-ico">\n                </div>\n                <div class="phone_item">+7 (495) 374-70-77</div>\n                <div class="phone_item">+7 (495) 374-70-77</div>\n                <div class="clearfix"></div>\n            </div>\n            <div class="clearfix"></div>\n        </div>\n\n        <div class="subscribe">\n            <div class="email-icon"></div>\n            <div class="subscribe-text">\n                <p>\n                    Получить инструкцию и чек-лист:<span class="bold">«Как не купить <br>\n                    не прибыльную франшизу и не потерять свои деньги»</span>\n                </p>\n\n                <form action="" class="subscribe_form">\n                    <div class="input" tabindex="-1">\n                        <input type="email" id="subscribe_input" name="email">\n                        <label for="subscribe_input">E-mail:</label>\n                    </div>\n                    <button type="submit" class="btn btn-outline">Получить инструкцию и чек-лист</button>\n                </form>\n\n            </div>\n\n\n        </div>\n\n        <div class="clearfix"></div>\n\n    </div>\n</section>\n<section class="bottom_nav">\n    <div class="container">\n        <div class="col">\n            <b class="cat_title">О проекте</b>\n            <ul>\n                <li><a href="">О нас</a></li>\n                <li><a href="">Условия публикации</a></li>\n                <li><a href="">FAQ</a></li>\n            </ul>\n        </div>\n        <div class="col">\n            <b class="cat_title">О проекте</b>\n            <ul>\n                <li><a href="">О нас</a></li>\n                <li><a href="">Условия публикации</a></li>\n                <li><a href="">FAQ</a></li>\n            </ul>\n        </div>\n        <div class="col">\n            <b class="cat_title">О проекте</b>\n            <ul>\n                <li><a href="">О нас</a></li>\n                <li><a href="">Условия публикации</a></li>\n                <li><a href="">FAQ</a></li>\n            </ul>\n        </div>\n        <div class="col">\n            <b class="cat_title">О проекте</b>\n            <ul>\n                <li><a href="">О нас</a></li>\n                <li><a href="">Условия публикации</a></li>\n                <li><a href="">FAQ</a></li>\n            </ul>\n        </div>\n    </div>\n</section>\n\n[[!$footer?]]\n\n\n<img src="img/parallax/circle.svg" class="left depth-2" data-0="top:40%; transform:scale(2);" data-end="top:0%;">\n<img src="img/parallax/square.svg" class="left depth-1" data-0="top:300%; transform:scale(0.8);" data-end="top:0%;">\n<img src="img/parallax/triange.svg" class="left depth-3" data-0="top:100%;" data-end="top:20%;">\n<img src="img/parallax/circle.svg" class="left depth-2" data-0="top:200%;" data-end="top:0%;">\n<img src="img/parallax/square.svg" class="left depth-3" data-0="top:900%; transform:scale(3);" data-end="top:0%;">\n<img src="img/parallax/triange.svg" class="left depth-3" data-0="top:500%; left:-3%; transform:scale(2) rotate(0deg);" data-end="top:20%; transform: scale(2) rotate(660deg);">\n<img src="img/parallax/circle.svg" class="left depth-2" data-0="top:600%;" data-end="top:150%;">\n<img src="img/parallax/square.svg" class="left depth-3" data-0="top:700%;" data-end="top:200%;">\n<img src="img/parallax/triange.svg" class="left depth-1" data-0="left:8%; top:800%; transform: rotate(0deg);" data-end="top:200%; transform: rotate(660deg);">\n\n\n<img src="img/parallax/triange.svg" class="right depth-2" data-0="top:40%; right:-10%; transform:scale(2);" data-end="top:0%;">\n<img src="img/parallax/square.svg" class="right depth-1" data-0="top:100%; transform:scale(0.8);" data-end="top:0%;">\n<img src="img/parallax/square.svg" class="right depth-3" data-0="top:400%;" data-end="top:20%;">\n<img src="img/parallax/triange.svg" class="right depth-2" data-0="top:800%;" data-end="top:0%;">\n<img src="img/parallax/circle.svg" class="right depth-3" data-0="top:120%; right:-3%; transform:scale(2) rotate(0deg);" data-end="top:20%; transform: scale(2) rotate(660deg);">\n\n\n\n<script src="js/slick.min.js" type="text/javascript"></script>\n<script src="js/ion.rangeSlider.min.js" type="text/javascript"></script>\n<script src="js/skrollr.min.js" type="text/javascript"></script>\n\n<script src="js/main.js"></script>\n\n<script>\n    $(document).ready(function(){\n        \n        // Получаем каталог в формате JSON и \n        // вставляем их в .catalog_wrapper через mustache\n        $.getJSON("/api/catalog.php", function(result){\n            var data = { items: result}\n            console.log(result);\n            $.get(''/templates/chunks/catalog_item.html'', function(template) {\n                var rendered = Mustache.render(template, data);\n                $(''.catalog_wrapper'').html(rendered);\n            });\n        });\n    \n    });\n</script>\n\n\n</body>\n</html>', 0, 'a:0:{}', 1, 'templates/index.html'),
(3, 1, 0, 'Каталог', '', 0, 0, '', 0, '<!doctype html>\n<html>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge">\n    <title>[[*pagetitle]]</title>\n    \n    [[!$meta?]]\n\n    <link rel="stylesheet" href="css/catalog.css">\n    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">\n    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css">\n\n    <script src="https://yastatic.net/jquery/2.2.3/jquery.min.js"></script>\n\n\n</head>\n<body>\n\n[[!$header?]]\n\n\n<section class="breadcrumbs">\n    <div class="container">\n        <ul class="crumbs">\n            <li><a href=""><i class="fa fa-home" aria-hidden="true"></i></a></li>\n            <li><a href="">Каталог франшиз</a></li>\n            <li><a href="">Услуги</a></li>\n        </ul>\n    </div>\n</section>\n\n\n<section class="heading_section">\n    <div class="container">\n\n        <div id="catalog_line_1" data-0="height:70px; width:0px;" data-150="height:70px; width: 980px;" data-200="height:130px; width: 980px;">\n            <div class="line"></div>\n        </div>\n\n        <h1 class="title">[[*pagetitle]]</h1>\n        <h5 class="subtitle">Найдено [[*id:getItemCount]] франшиз</h5>\n    </div>\n</section>\n\n\n\n<section class="top_catalog">\n\n    <!--<div id="line_2_container" data-500="height:0px; width:0px;" data-1150="height:600px; width: 530px;">-->\n        <!--<div class="line"></div>-->\n    <!--</div>-->\n\n    <div class="container">\n        <h1 class="title">Топ 10 франшиз</h1>\n\n\n        <div class="catalog_wrapper top_catalog_slider">\n            \n            [[!getResources?\n            	&tpl=`catalog.item`\n            	&limit=`10`\n            	&includeContent=`1`\n            	&includeTVs=`1`\n            	&processTVs=`1`\n            	&parents=`2`\n            	&where=`{"template":2}`\n            ]]\n\n        </div><!-- top cat wrapper end -->\n\n    </div>\n</section>\n\n<section class="get_checklist">\n    <div class="container">\n        <div class="block">\n\n            <div class="line" data-230="width:0px;" data-550="width:660px;"></div>\n\n            <img src="img/instruction.png" alt="" class="instruct">\n            <div class="get-checklist">\n                Получить инструкцию и чек-лист: <br>\n            <span class="bold">«Как не купить не прибыльную </span> <br>\n            франшизу и не потерять свои деньги»\n            </div>\n            <a href="/" class="btn btn-outline">\n                Получить инструкцию и чек-лист\n            </a>\n        </div>\n    </div>\n</section>\n\n\n\n<section class="content">\n    <div class="container">\n        <div class="left_sidebar">\n\n            <div class="filter">\n                <h4>Быстрый поиск франшиз</h4>\n\n                <form action="/" method="post">\n                    <div class="param">\n                        <label for="invest_range">Инвестиции, тыс. руб.</label>\n                        <input type="text" id="invest_range" name="invest_range" class="range_slider" value="" />\n                    </div>\n\n                    <div class="param">\n                        <label for="cashback_range">Срок окупаемости, мес.</label>\n                        <input type="text" id="cashback_range" name="cashback_range" class="range_slider" value="" />\n                    </div>\n\n                    <div class="param param_selector">\n                        <a href="#" class="select_business">Вид бизнеса</a>\n                        <div class="cat_accordion">\n                            <div class="cat_title">Категория 1</div>\n                            <div class="cat_container" style="display: block;">\n                                <label for="elem_1">\n                                    <input id="elem_1" type="checkbox" value="cat_1"> Элемент 1\n                                </label>\n\n                                <label for="elem_2">\n                                    <input id="elem_2" type="checkbox" value="cat_1"> Элемент 2\n                                </label>\n\n                                <label for="elem_3">\n                                    <input id="elem_3" type="checkbox" value="cat_1"> Элемент 3\n                                </label>\n                            </div>\n\n\n                            <div class="cat_title">Категория 2</div>\n                            <div class="cat_container">\n                                <label for="elem_4">\n                                    <input id="elem_4" type="checkbox" value="cat_1"> Элемент 1\n                                </label>\n\n                                <label for="elem_5">\n                                    <input id="elem_5" type="checkbox" value="cat_1"> Элемент 2\n                                </label>\n\n                                <label for="elem_6">\n                                    <input id="elem_6" type="checkbox" value="cat_1"> Элемент 3\n                                </label>\n                            </div>\n                        </div>\n                    </div>\n\n                    <div id="hidden_options" style="display: none;">\n                        <div class="param param_selector">\n                            <a href="#" class="select_business">Роялти</a>\n                            <div class="cat_accordion">\n                                <div class="cat_container" style="display: block;">\n                                    <label for="param_1">\n                                        <input id="param_1" type="checkbox" value="cat_1"> Элемент 1\n                                    </label>\n\n                                    <label for="param_2">\n                                        <input id="param_2" type="checkbox" value="cat_1"> Элемент 2\n                                    </label>\n\n                                    <label for="param_3">\n                                        <input id="param_3" type="checkbox" value="cat_1"> Элемент 3\n                                    </label>\n                                </div>\n\n                            </div>\n                        </div>\n\n                        <div class="param">\n                            <label for="square_range">Мин. площадь, кв.м</label>\n                            <input type="text" id="square_range" name="cashback_range" class="range_slider" value="" />\n                        </div>\n\n                        <div class="param">\n                            <label for="top_ten">\n                                <input id="top_ten" type="checkbox" value="cat_1"> Входит в ТОП-10 франшиз\n                            </label>\n                        </div>\n                    </div>\n\n\n\n\n                    <button type="submit" class="btn btn-outline">Подобрать</button>\n                    <a href="/" class="link" id="detail_search_btn">Расширенный поиск</a>\n\n                </form>\n            </div> <!-- filter end -->\n\n\n            <button class="btn btn-outline">\n                Отправить зявку на публикацию в каталоге\n            </button>\n\n            <h3 class="subtitle">Новые отзывы</h3>\n\n\n\n            <div class="reviews">\n\n                <div id="catalog_line_2" data-950="height:0px; width:25px;" data-1250="height:70px; width: 25px;" data-1450="height:70px; width: 250px;" >\n                    <div class="line"></div>\n                </div>\n\n                <div class="item">\n                    <div class="item_head">\n                        <div class="quote-icon">«</div>\n                        <div class="photo">\n                            <img src="img/client.jpg" alt="">\n                        </div>\n                        <div class="franchise">\n                            Франшиза\n                            <a href="" class="link">Фитнес-клубы FitSport</a>\n                            <div class="date">26.06.2016</div>\n                        </div>\n                        <div class="clearfix"></div>\n                    </div>\n                    <div class="name">\n                        Степаненко Антон\n                        Геннадьевич\n                    </div>\n                    <div class="text">\n                        <p>\n                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.\n                            Asperiores atque, aut dolorem eum <a href="" class="more">Подробнее >></a>\n                        </p>\n                    </div>\n                </div>\n\n                <div class="item">\n                    <div class="item_head">\n                        <div class="quote-icon">«</div>\n                        <div class="photo">\n                            <img src="img/client.jpg" alt="">\n                        </div>\n                        <div class="franchise">\n                            Франшиза\n                            <a href="" class="link">Фитнес-клубы FitSport</a>\n                            <div class="date">26.06.2016</div>\n                        </div>\n                        <div class="clearfix"></div>\n                    </div>\n                    <div class="name">\n                        Степаненко Антон\n                        Геннадьевич\n                    </div>\n                    <div class="text">\n                        <p>\n                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.\n                            Asperiores atque, aut dolorem eum <a href="" class="more">Подробнее >></a>\n                        </p>\n                    </div>\n                </div>\n            </div>\n\n            <div class="about">\n                <h3 class="subtitle">О категории</h3>\n\n                <div id="catalog_line_3" data-1750="height:0px; width:25px;" data-1850="height:70px; width: 25px;" data-1950="height:70px; width: 350px;" >\n                    <div class="line"></div>\n                </div>\n\n                <p class="cat_descript">\n                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid amet aut cupiditate earum eos et\n                    molestias obcaecati, odio voluptate. Cumque delectus ex laboriosam minima odio quia rem reprehenderit\n                    velit voluptate.\n                </p>\n            </div>\n\n\n        </div>\n        <div class="right_side">\n\n            <div class="view_actions">\n                <b>Сортировка:</b>\n\n                <a href="#" class="active">Инвестиции <i class="fa fa-sort-desc" aria-hidden="true"></i></a>\n                <a href="#">Новизна <i class="fa fa-sort" aria-hidden="true"></i></a>\n                <a href="#">Рейтинг <i class="fa fa-sort" aria-hidden="true"></i></a>\n                <a href="#">Вид бизнеса <i class="fa fa-sort" aria-hidden="true"></i></a>\n\n                <div class="view_type">\n                    <b>Вид списка:</b>\n                    <button class="set_view_btn active"><i class="fa fa-th-large" aria-hidden="true"></i></button>\n                    <button class="set_view_btn"><i class="fa fa-list" aria-hidden="true"></i></button>\n                </div>\n\n            </div>\n\n            <hr class="hr-line">\n\n            <h4 class="subtitle">Франшизы общественного питания</h4>\n\n\n            <div class="catalog">\n                \n                [[*content]]\n                \n                <!--<div class="item">\n\n                    <div class="label">\n                    </div>\n\n                    <div class="rate">\n                        <div class="star star_active"></div>\n                        <div class="star star_active"></div>\n                        <div class="star star_active"></div>\n                        <div class="star"></div>\n                        <div class="star"></div>\n                    </div>\n                    <a href="" class="fr_name">Персональное решение</a>\n                    <div class="cover">\n                        <img src="img/fr_logo5.jpg" alt="">\n                    </div>\n                    <a href="" class="link cat-link">Услуги для бизнеса</a>\n\n                    <span class="opt">Окупаемость: <b>3 мес.</b></span>\n                    <span class="opt">Инвестиции: <b>194 000 руб.</b></span>\n\n                    <div class="actions">\n                        <a href="" class="btn">К сравнению</a>\n                        <a href="" class="btn btn-more">Подробнее</a>\n                    </div>\n                </div>-->\n\n            </div>\n            \n            <div class="catalog_pagination">\n                <ul>\n                    [[+page.nav]]\n                    <!--\n                    <li><a href="" class="active">1</a></li>\n                    <li><a href="">2</a></li>\n                    <li><a href="">3</a></li>\n                    <li><a href="">4</a></li>\n                    <li><a href="">5</a></li>\n                    -->\n                </ul>\n            </div>\n            \n            \n            \n\n\n        </div>\n\n        <div class="clearfix"></div>\n    </div>\n</section>\n\n\n\n<section class="faq">\n    <div class="container">\n        <div class="accordion">\n            <div class="elem">\n                <dt>Просто заголовок</dt>\n                <dd>\n                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab aliquam autem consectetur cum\n                        deserunt, dolore eos excepturi fugit maxime nam odio perspiciatis quaerat quo reiciendis sunt\n                        suscipit tenetur ut velit?\n                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab asperiores autem culpa, cum dolor\n                        dolores error labore minima nam, obcaecati perferendis porro, quas rerum saepe sed soluta\n                        temporibus unde vero.</p>\n                </dd>\n            </div>\n            <div class="elem">\n                <dt>Неожиданный заголовок</dt>\n                <dd>\n                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab aliquam autem consectetur cum\n                        deserunt, dolore eos excepturi fugit maxime nam odio perspiciatis quaerat quo reiciendis sunt\n                        suscipit tenetur ut velit?</p>\n                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab asperiores autem culpa, cum dolor\n                        dolores error labore minima nam, obcaecati perferendis porro, quas rerum saepe sed soluta\n                        temporibus unde vero.</p>\n                </dd>\n            </div>\n            <div class="elem">\n                <dt>Странный заголовок</dt>\n                <dd>\n                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab aliquam autem consectetur cum\n                        deserunt, dolore eos excepturi fugit maxime nam odio perspiciatis quaerat quo reiciendis sunt\n                        suscipit tenetur ut velit?</p>\n                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab asperiores autem culpa, cum dolor\n                        dolores error labore minima nam, obcaecati perferendis porro, quas rerum saepe sed soluta\n                        temporibus unde vero.</p>\n                </dd>\n            </div>\n        </div>\n    </div>\n</section>\n\n\n<section class="contacts">\n    <div class="container">\n\n        <div id="contacts_line_1"  data-200-end="height:0px; width: 25px;" data-100-end="height:70px; width: 25px;" data-end="height:70px; width: 250px;">\n            <div class="line"></div>\n        </div>\n\n\n        <div id="contacts_line_2"  data-200-end="height:70px; width: 0px;" data-100-end="height:70px; width: 600px;" data-end="height:270px; width: 600px;">\n            <div class="line"></div>\n        </div>\n\n\n        <div class="phones">\n            <b class="phones_title">Телефон в Москве:</b>\n\n            <div class="phones_wrapper">\n                <div class="phone-ico">\n                </div>\n                <div class="phone_item">+7 (495) 374-70-77</div>\n                <div class="phone_item">+7 (495) 374-70-77</div>\n                <div class="clearfix"></div>\n            </div>\n            <div class="clearfix"></div>\n        </div>\n\n        <div class="subscribe">\n            <div class="email-icon"></div>\n            <div class="subscribe-text">\n                <p>\n                    Получить инструкцию и чек-лист:<span class="bold">«Как не купить <br>\n                    не прибыльную франшизу и не потерять свои деньги»</span>\n                </p>\n\n                <form action="" class="subscribe_form">\n                    <div class="input" tabindex="-1">\n                        <input type="email" id="subscribe_input" name="email">\n                        <label for="subscribe_input">E-mail:</label>\n                    </div>\n                    <button type="submit" class="btn btn-outline">Получить инструкцию и чек-лист</button>\n                </form>\n\n            </div>\n\n\n        </div>\n\n        <div class="clearfix"></div>\n\n    </div>\n</section>\n<section class="bottom_nav">\n    <div class="container">\n        <div class="col">\n            <b class="cat_title">О проекте</b>\n            <ul>\n                <li><a href="">О нас</a></li>\n                <li><a href="">Условия публикации</a></li>\n                <li><a href="">FAQ</a></li>\n            </ul>\n        </div>\n        <div class="col">\n            <b class="cat_title">О проекте</b>\n            <ul>\n                <li><a href="">О нас</a></li>\n                <li><a href="">Условия публикации</a></li>\n                <li><a href="">FAQ</a></li>\n            </ul>\n        </div>\n        <div class="col">\n            <b class="cat_title">О проекте</b>\n            <ul>\n                <li><a href="">О нас</a></li>\n                <li><a href="">Условия публикации</a></li>\n                <li><a href="">FAQ</a></li>\n            </ul>\n        </div>\n        <div class="col">\n            <b class="cat_title">О проекте</b>\n            <ul>\n                <li><a href="">О нас</a></li>\n                <li><a href="">Условия публикации</a></li>\n                <li><a href="">FAQ</a></li>\n            </ul>\n        </div>\n    </div>\n</section>\n\n[[!$footer?]]\n\n\n\n<script src="js/slick.min.js" type="text/javascript"></script>\n<script src="js/ion.rangeSlider.min.js" type="text/javascript"></script>\n<script src="js/skrollr.min.js" type="text/javascript"></script>\n<script src="js/main.js"></script>\n\n\n</body>\n</html>', 0, 'a:0:{}', 1, 'templates/catalog.html');
INSERT INTO `modx_site_templates` (`id`, `source`, `property_preprocess`, `templatename`, `description`, `editor_type`, `category`, `icon`, `template_type`, `content`, `locked`, `properties`, `static`, `static_file`) VALUES
(2, 1, 0, 'Детальный просмотр', 'Шаблон для страницы детального просмотра страницы', 0, 0, '', 0, '<!doctype html>\n<html>\n<head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge">\n    <title>[[*pagetitle]]</title>\n    \n    [[!$meta?]]\n\n    <link rel="stylesheet" href="css/detail.css">\n    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">\n    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css">\n    <script src="https://yastatic.net/jquery/2.2.3/jquery.min.js"></script>\n\n\n</head>\n<body>\n\n<header>\n    <div class="container">\n        <a href="/" class="logo">\n            <img src="img/logo.png" alt="Logo">\n        </a>\n\n        <div class="navigation">\n            <ul>\n                <li><a href="/" class="active">Каталог франшиз</a></li>\n                <li><a href="/">Торговля</a></li>\n                <li><a href="/">Услуги</a></li>\n                <li><a href="/">Общественное питание</a></li>\n                <li><a href="/">Производство</a></li>\n            </ul>\n        </div>\n\n        <div class="actions">\n            <button class="act-btn" onclick="start_search();"><i class="fa fa-eye" aria-hidden="true"></i></button>\n            <a class="act-btn" onclick="start_search();">\n                <span class="counter-label"><span class="num">7</span></span>\n                <i class="fa fa-exchange" aria-hidden="true"></i>\n            </a>\n            <button class="act-btn" onclick="start_search();"><i class="fa fa-search" aria-hidden="true"></i></button>\n        </div>\n\n        <div class="header_contacts pull-right">\n            <a href="tel:80000000000" class="phone">8(800)333-33-33</a>\n            <a href="" class="callback_btn" onclick="show_modal(); return false;">Обратный звонок</a>\n        </div>\n    </div>\n\n\n    <div class="search-overflow" id="search-form">\n        <div class="form-search-container">\n            <form action="">\n                <input type="text" name="search-query" placeholder="Введите ключевое слово для поиска...">\n                <button type="submit" class="search-btn"><i class="fa fa-search" aria-hidden="true"></i></button>\n            </form>\n            <button class="cancel-search-btn" onclick="cancel_search();">Отмена</button>\n        </div>\n\n    </div>\n\n</header>\n\n\n<section class="breadcrumbs">\n    <div class="container">\n        <ul class="crumbs">\n            <li><a href=""><i class="fa fa-home" aria-hidden="true"></i></a></li>\n            <li><a href="">Каталог франшиз</a></li>\n            <li><a href="">Услуги</a></li>\n        </ul>\n    </div>\n</section>\n\n\n<section class="heading_section">\n    <div class="container">\n\n        <div id="catalog_line_1" data-0="height:70px; width:0px;" data-150="height:70px; width: 980px;" data-200="height:130px; width: 980px;">\n            <div class="line"></div>\n        </div>\n\n        <h1 class="title">Франшиза COFFEE CAKE</h1>\n        <h5 class="subtitle"><br></h5>\n    </div>\n</section>\n\n\n\n<section class="detail_overview">\n    <div class="container">\n\n        <div class="detail_card">\n            <div class="head">Минимальные инвестиции <br> 250 000 руб.</div>\n            <div class="subhead">Краткая информация о франшизе</div>\n            <div class="table-container">\n                <table>\n                    <tr>\n                        <td>Первоначальный взнос:</td>\n                        <td>отсутствует</td>\n                    </tr>\n                    <tr>\n                        <td>Инвестиции от:</td>\n                        <td>250 000 руб</td>\n                    </tr>\n                    <tr>\n                        <td>Инвестиции до:</td>\n                        <td>10 000 000 руб.</td>\n                    </tr>\n                    <tr>\n                        <td>Роялти:</td>\n                        <td>5% от оборота</td>\n                    </tr>\n                    <tr>\n                        <td>Срок окупаемости:</td>\n                        <td>1 год</td>\n                    </tr>\n                    <tr>\n                        <td>Минимальная площадь:</td>\n                        <td>9 м<span class="sqrt">2</span></td>\n                    </tr>\n                    <tr>\n                        <td>Год запуска франчайзинга:</td>\n                        <td>2015</td>\n                    </tr>\n                    <tr>\n                        <td>Франшизных точек:</td>\n                        <td>5</td>\n                    </tr>\n                </table>\n                <div class="alert-stick">\n                    <i class="fa fa-exclamation-circle" aria-hidden="true"></i>\n                    <p>\n                        Показатели окупаемости являются предположительными и не являются обещанием успешности бизнеса\n                    </p>\n                    <div class="clearfix"></div>\n                </div>\n            </div>\n        </div><!-- detail card end-->\n\n\n        <div class="right_block">\n            <div class="actions">\n                <div class="action"><i class="fa fa-eye" aria-hidden="true"></i> : 2</div>\n                <div class="action"><i class="fa fa-commenting-o" aria-hidden="true"></i> : 2</div>\n                <div class="action">\n                    <i class="fa fa-star" aria-hidden="true"></i>\n                    <i class="fa fa-star" aria-hidden="true"></i>\n                    <i class="fa fa-star" aria-hidden="true"></i>\n                    <i class="fa fa-star-o" aria-hidden="true"></i>\n                    <i class="fa fa-star-o" aria-hidden="true"></i>\n                </div>\n                <div class="action">\n                    <a href=""><i class="fa fa-exchange" aria-hidden="true"></i>&nbsp; К сравнению</a>\n                </div>\n            </div>\n\n            <div class="fr-slider">\n                <!-- Slider here-->\n                <div class="big-pic-slider">\n                    [[!Gallery? &album=`2` &thumbTpl=`gallery.fullImage`]]\n                </div>\n                <div class="mini-pics-slider">\n                    [[!Gallery? &album=`2` &thumbTpl=`gallery.fullImage`]]\n                </div>\n\n            </div>\n\n            <div class="presentation">\n                <span class="ico"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></span>\n                <a href="">Презентация Coffe Cake</a>\n                <span class="format">PDF | 7.15Мб</span>\n            </div>\n        </div>\n\n\n        <div class="clearfix"></div>\n\n\n    </div>\n</section>\n\n\n\n<section class="fr-description">\n    <div class="container">\n\n        <button class="btn-outline" id="pws_btn_consult">Бесплатная консультация</button>\n\n        <div class="detail-tabs">\n            <div data-pws-tab="anynameyouwant1" data-pws-tab-name="Описание">\n\n                <div class="section">\n                    <h2>О франчайзере</h2>\n                    <p class="about-fr">\n                        Франшиза Coffee Cake - идеальное решение для предпринимателей из регионов, бизнес опробован и\n                        доказал свою эффективность. Привлека-тельный бизнес с ежегодной тенденцией прироста рынка на 15%\n                    </p>\n                    <button class="btn-outline" id="ask_fr_btn">Задать вопрос франчайзеру</button>\n                    <div class="clearfix"></div>\n                </div>\n\n                <div class="section">\n                    <h2>Преимущества франшизы</h2>\n                    <p>\n                    <ul>\n                        <li>Система обучения и стажировки персонала. Проведение тренингов.</li>\n                        <li>Сбалансированность инвестиций, за счет возможности выбора одного из трёх форматов кофейни\n                            Coffee  Cake, востребованных целевой аудиторией.\n                        </li>\n                        <li>Снижение рисков, бизнес по франшизе Coffee Cake поможет избежать возможных ошибок.</li>\n                        <li>Минимум затрат на рекламную и маркетинговую деятельность - рекламные и маркетинговые\n                            кампании\n                            уже  опробованы в сети Coffee Cake, затраты на реализацию сводятся к минимуму.\n                        </li>\n                        <li>Продвижение в интернете. Сайт Coffee Cake разработан, и каждый день привлекает новых\n                            посетителей.  Технологии SMM продвижения опробованы и эффективно работают.\n                        </li>\n                        <li>Приобретая франшизу Coffee Cake, наши партнеры получают отлаженный механизм, с описанием\n                            всех \n                            процессов.\n                        </li>\n                        <li>Прозрачная система работы, ERP-система. Программа удаленного контроля работы кофейни.</li>\n                    </ul>\n                    </p>\n                </div>\n                <div class="section">\n                    <h2>Бизнес-модель</h2>\n                    <p>\n                        Кофейни Coffee Cake представляет сеть ресторанов LFR Family - стабильная, развивающаяся на\n                        рынке услуг общественного питания. В компании реализуются несколько проектов: японские суши-бары\n                        Сушимин, американские рестораны New York, ночной бар MIXTURA BAR, итальянский ресторан IL TEMPO,\n                        кафе грузинской кухни Хачапури, чайхана КишМиш, кафе восточной кухни Барашек, итальянское кафе\n                        Trattoria Pepperoni и ночной клуб THE TOP CLUB. Миссия компании LFR Family: всегда приятно\n                        удивлять и превосходить ожидания гостей за счет отличного сервиса и профессионализма сотрудников.\n                    </p>\n                </div>\n\n                <div class="section">\n                    <h2>Финансовые условия</h2>\n                    <p>\n                        <b>Вступительный взнос:</b> Отсутствует <br>\n                        <b>Роялти 5% от оборота,</b> для формата to go и остров в ТЦ - фиксированный платеж.\n                    </p>\n                </div>\n\n                <div class="section">\n                    <h2>что входит во франчайзинговый пакет</h2>\n                    <p>\n                        <ul>\n                            <li> Консультации по вопросам подбора помещений для кофейни Сoffee Cake;</li>\n                            <li> Право использования товарного знака и логотипа Coffee Cake;</li>\n                            <li> Консультации по техническим вопросам строительства и оформления;</li>\n                            <li> Технологии приготовления блюд и раскладке меню;</li>\n                            <li> Помощь в приобретении оборудования, необходимого для кофейни</li>\n                            <li> Обучение и стажировка персонала;</li>\n                            <li> Маркетинговая поддержка:</li>\n                            <li>\n                                <ol>\n                                    <li> разработка ассортимента</li>\n                                    <li> ценообразование</li>\n                                    <li> анализ продаж</li>\n                                    <li> реклама</li>\n                                </ol>\n                            </li>\n                            <li> Выезд команды специалистов на запуск кофейни;</li>\n                            <li> Постоянная поддержка по управлению и продвижению кофейни после открытия</li>\n                        </ul>\n                    </p>\n                </div>\n\n\n            </div>\n            <div data-pws-tab="anynameyouwant2" data-pws-tab-name="Обучение">Our second tab</div>\n            <div data-pws-tab="anynameyouwant3" data-pws-tab-name="Требования">Our third tab</div>\n            <div data-pws-tab="anynameyouwant4" data-pws-tab-name="География франчайзинга">Our third tab</div>\n            <div data-pws-tab="anynameyouwant5" data-pws-tab-name="Отзывы">Our third tab</div>\n        </div>\n\n    </div>\n</section>\n\n\n\n\n<section class="video_form">\n    <div class="container">\n        <div class="video">\n            <iframe src="https://player.vimeo.com/video/36911719?loop=1&title=0&byline=0&portrait=0" width="555" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>\n        </div>\n\n        <div class="form">\n            <h3>Получить бесплатную <br> консультацию по франшизе</h3>\n            <form action="">\n                <input type="text" placeholder="Введите ваше имя:" name="name">\n                <input type="tel" placeholder="Введите ваш телефон:" name="phone">\n                <input type="email" placeholder="Введите ваш e-mail" name="email">\n\n                <button type="submit" class="btn-outline"> Получить бесплатную консультацию </button>\n            </form>\n        </div>\n\n        <div class="clearfix"></div>\n\n    </div>\n</section>\n\n\n\n\n\n\n<section class="contacts">\n    <div class="container">\n\n        <div id="contacts_line_1"  data-200-end="height:0px; width: 25px;" data-100-end="height:70px; width: 25px;" data-end="height:70px; width: 250px;">\n            <div class="line"></div>\n        </div>\n\n\n        <div id="contacts_line_2"  data-200-end="height:70px; width: 0px;" data-100-end="height:70px; width: 600px;" data-end="height:270px; width: 600px;">\n            <div class="line"></div>\n        </div>\n\n\n        <div class="phones">\n            <b class="phones_title">Телефон в Москве:</b>\n\n            <div class="phones_wrapper">\n                <div class="phone-ico">\n                </div>\n                <div class="phone_item">+7 (495) 374-70-77</div>\n                <div class="phone_item">+7 (495) 374-70-77</div>\n                <div class="clearfix"></div>\n            </div>\n            <div class="clearfix"></div>\n        </div>\n\n        <div class="subscribe">\n            <div class="email-icon"></div>\n            <div class="subscribe-text">\n                <p>\n                    Получить инструкцию и чек-лист:<span class="bold">«Как не купить <br>\n                    не прибыльную франшизу и не потерять свои деньги»</span>\n                </p>\n\n                <form action="" class="subscribe_form">\n                    <div class="input" tabindex="-1">\n                        <input type="email" id="subscribe_input" name="email">\n                        <label for="subscribe_input">E-mail:</label>\n                    </div>\n                    <button type="submit" class="btn btn-outline">Получить инструкцию и чек-лист</button>\n                </form>\n\n            </div>\n\n\n        </div>\n\n        <div class="clearfix"></div>\n\n    </div>\n</section>\n<section class="bottom_nav">\n    <div class="container">\n        <div class="col">\n            <b class="cat_title">О проекте</b>\n            <ul>\n                <li><a href="">О нас</a></li>\n                <li><a href="">Условия публикации</a></li>\n                <li><a href="">FAQ</a></li>\n            </ul>\n        </div>\n        <div class="col">\n            <b class="cat_title">О проекте</b>\n            <ul>\n                <li><a href="">О нас</a></li>\n                <li><a href="">Условия публикации</a></li>\n                <li><a href="">FAQ</a></li>\n            </ul>\n        </div>\n        <div class="col">\n            <b class="cat_title">О проекте</b>\n            <ul>\n                <li><a href="">О нас</a></li>\n                <li><a href="">Условия публикации</a></li>\n                <li><a href="">FAQ</a></li>\n            </ul>\n        </div>\n        <div class="col">\n            <b class="cat_title">О проекте</b>\n            <ul>\n                <li><a href="">О нас</a></li>\n                <li><a href="">Условия публикации</a></li>\n                <li><a href="">FAQ</a></li>\n            </ul>\n        </div>\n    </div>\n</section>\n<footer>\n    <div class="container">\n        <div class="footer_logo">\n            <a href="/" class="logo"></a>\n        </div>\n        <div class="copy">\n            © 2016 ВСЕ ПРАВА ЗАЩИЩЕНЫ\n            <a href="" class="policy">Политика конфиденциальности</a>\n        </div>\n        <div class="search">\n            <form action="/" class="search_form">\n                <div class="input" tabindex="-1">\n                    <input type="email" id="search_input_bottom" name="email">\n                    <label for="search_input_bottom">Поиск по сайту:</label>\n                </div>\n                <button type="submit" class="btn btn-outline">Искать</button>\n            </form>\n        </div>\n    </div>\n</footer>\n\n\n\n\n<script src="js/slick.min.js" type="text/javascript"></script>\n<script src="js/ion.rangeSlider.min.js" type="text/javascript"></script>\n<script src="js/skrollr.min.js" type="text/javascript"></script>\n<script src="js/jquery.pwstabs.min.js" type="text/javascript"></script>\n\n<script src="js/main.js"></script>\n\n<script>\n    // Tabs for Detail Page\n    $(''.detail-tabs'').pwstabs();\n</script>\n\n\n</body>\n</html>', 0, 'a:0:{}', 1, 'templates/detail.html');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_site_tmplvars`
--

CREATE TABLE `modx_site_tmplvars` (
  `id` int(10) UNSIGNED NOT NULL,
  `source` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `property_preprocess` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `type` varchar(20) NOT NULL DEFAULT '',
  `name` varchar(50) NOT NULL DEFAULT '',
  `caption` varchar(80) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `editor_type` int(11) NOT NULL DEFAULT '0',
  `category` int(11) NOT NULL DEFAULT '0',
  `locked` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `elements` text,
  `rank` int(11) NOT NULL DEFAULT '0',
  `display` varchar(20) NOT NULL DEFAULT '',
  `default_text` mediumtext,
  `properties` text,
  `input_properties` text,
  `output_properties` text,
  `static` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `static_file` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_site_tmplvars`
--

INSERT INTO `modx_site_tmplvars` (`id`, `source`, `property_preprocess`, `type`, `name`, `caption`, `description`, `editor_type`, `category`, `locked`, `elements`, `rank`, `display`, `default_text`, `properties`, `input_properties`, `output_properties`, `static`, `static_file`) VALUES
(1, 1, 0, 'number', 'min_invest', 'Минимальные инвестиции', '', 0, 4, 0, '', 0, 'default', '', 'a:0:{}', 'a:7:{s:10:"allowBlank";s:5:"false";s:13:"allowDecimals";s:6:"Нет";s:13:"allowNegative";s:6:"Нет";s:16:"decimalPrecision";s:1:"2";s:16:"decimalSeparator";s:1:".";s:8:"maxValue";s:0:"";s:8:"minValue";s:1:"0";}', 'a:0:{}', 0, ''),
(2, 1, 0, 'image', 'anonce_img', 'Лого или изображение для каталога', '', 0, 4, 0, '', 0, 'default', '', 'a:0:{}', 'a:0:{}', 'a:0:{}', 0, ''),
(3, 1, 0, 'number', 'cashback_time', 'Срок окупаемости (месяцев)', '', 0, 4, 0, '', 0, 'default', '1', 'a:0:{}', 'a:7:{s:10:"allowBlank";s:5:"false";s:13:"allowDecimals";s:6:"Нет";s:13:"allowNegative";s:6:"Нет";s:16:"decimalPrecision";s:1:"2";s:16:"decimalSeparator";s:1:".";s:8:"maxValue";s:0:"";s:8:"minValue";s:1:"1";}', 'a:0:{}', 0, ''),
(4, 1, 0, 'listbox', 'label', 'Лычка для товара', 'Возможность задать лычку для товара "NEW", "BEST" и другое ', 0, 4, 0, 'NEW||\nBEST', 0, 'default', 'Нет', 'a:0:{}', 'a:7:{s:10:"allowBlank";s:4:"true";s:9:"listWidth";s:0:"";s:5:"title";s:0:"";s:9:"typeAhead";s:5:"false";s:14:"typeAheadDelay";s:3:"250";s:14:"forceSelection";s:4:"true";s:13:"listEmptyText";s:0:"";}', 'a:0:{}', 0, ''),
(5, 1, 0, 'listbox', 'rate', 'Оценка', 'Оценка (рейтинг) франшизы от 1 до 5', 0, 4, 0, '1||\n2||\n3||\n4||\n5', 0, 'default', '4', 'a:0:{}', 'a:7:{s:10:"allowBlank";s:4:"true";s:9:"listWidth";s:0:"";s:5:"title";s:0:"";s:9:"typeAhead";s:5:"false";s:14:"typeAheadDelay";s:3:"250";s:14:"forceSelection";s:5:"false";s:13:"listEmptyText";s:0:"";}', 'a:0:{}', 0, ''),
(6, 1, 0, 'richtext', 'about_fr', 'О ФРАНЧАЙЗЕРЕ', '', 0, 7, 0, '', 0, 'default', '', 'a:0:{}', 'a:5:{s:10:"allowBlank";s:4:"true";s:9:"maxLength";s:0:"";s:9:"minLength";s:0:"";s:5:"regex";s:0:"";s:9:"regexText";s:0:"";}', 'a:0:{}', 0, ''),
(7, 1, 0, 'richtext', 'adv_fr', 'Преимущества франшизы', '', 0, 7, 0, '', 0, 'default', '', 'a:0:{}', 'a:5:{s:10:"allowBlank";s:4:"true";s:9:"maxLength";s:0:"";s:9:"minLength";s:0:"";s:5:"regex";s:0:"";s:9:"regexText";s:0:"";}', 'a:0:{}', 0, ''),
(8, 1, 0, 'richtext', 'business_model', 'Бизнес-модель', '', 0, 7, 0, '', 0, 'default', '', 'a:0:{}', 'a:5:{s:10:"allowBlank";s:4:"true";s:9:"maxLength";s:0:"";s:9:"minLength";s:0:"";s:5:"regex";s:0:"";s:9:"regexText";s:0:"";}', 'a:0:{}', 0, ''),
(9, 1, 0, 'richtext', 'fin_condition', 'Финансовые условия', '', 0, 7, 0, '', 0, 'default', '', 'a:0:{}', 'a:5:{s:10:"allowBlank";s:4:"true";s:9:"maxLength";s:0:"";s:9:"minLength";s:0:"";s:5:"regex";s:0:"";s:9:"regexText";s:0:"";}', 'a:0:{}', 0, ''),
(10, 1, 0, 'richtext', 'what_include', 'ЧТО ВХОДИТ ВО ФРАНЧАЙЗИНГОВЫЙ ПАКЕТ', '', 0, 7, 0, '', 0, 'default', '', 'a:0:{}', 'a:5:{s:10:"allowBlank";s:4:"true";s:9:"maxLength";s:0:"";s:9:"minLength";s:0:"";s:5:"regex";s:0:"";s:9:"regexText";s:0:"";}', 'a:0:{}', 0, ''),
(11, 1, 0, 'galleryalbumlist', 'gallery', 'Галлерея', '', 0, 4, 0, '', 0, 'default', '', 'a:0:{}', 'a:15:{s:10:"allowBlank";s:4:"true";s:4:"sort";s:4:"rank";s:3:"dir";s:4:"DESC";s:5:"limit";s:1:"0";s:5:"start";s:1:"0";s:8:"showNone";s:4:"true";s:9:"showCover";s:4:"true";s:6:"parent";s:0:"";s:9:"subchilds";s:6:"Нет";s:5:"width";s:3:"400";s:9:"listWidth";s:0:"";s:9:"typeAhead";s:5:"false";s:14:"typeAheadDelay";s:3:"250";s:14:"forceSelection";s:5:"false";s:13:"listEmptyText";s:0:"";}', 'a:0:{}', 0, ''),
(12, 1, 0, 'textarea', 'requirements', 'Требования', '', 0, 7, 0, '', 0, 'default', '', 'a:0:{}', 'a:1:{s:10:"allowBlank";s:4:"true";}', 'a:0:{}', 0, ''),
(13, 1, 0, 'image', 'otherone_image', 'Дополнительное изображение', '', 0, 4, 0, '', 0, 'default', '', 'a:0:{}', 'a:5:{s:10:"allowBlank";s:4:"true";s:9:"maxLength";s:0:"";s:9:"minLength";s:0:"";s:5:"regex";s:0:"";s:9:"regexText";s:0:"";}', 'a:0:{}', 0, ''),
(14, 1, 0, 'text', 'business', 'Бизнес', '', 0, 4, 0, '', 0, 'default', '', 'a:0:{}', 'a:5:{s:10:"allowBlank";s:4:"true";s:9:"maxLength";s:0:"";s:9:"minLength";s:0:"";s:5:"regex";s:0:"";s:9:"regexText";s:0:"";}', 'a:0:{}', 0, ''),
(15, 1, 0, 'text', 'category_business', 'Категория бизнеса', '', 0, 4, 0, '', 0, 'default', '', 'a:0:{}', 'a:5:{s:10:"allowBlank";s:4:"true";s:9:"maxLength";s:0:"";s:9:"minLength";s:0:"";s:5:"regex";s:0:"";s:9:"regexText";s:0:"";}', 'a:0:{}', 0, ''),
(16, 1, 0, 'text', 'enterprise', 'Предприятие', '', 0, 4, 0, '', 0, 'default', '', 'a:0:{}', 'a:5:{s:10:"allowBlank";s:4:"true";s:9:"maxLength";s:0:"";s:9:"minLength";s:0:"";s:5:"regex";s:0:"";s:9:"regexText";s:0:"";}', 'a:0:{}', 0, ''),
(17, 1, 0, 'text', 'turnover_cash', 'Денежный оборот', '', 0, 4, 0, '', 0, 'default', '', 'a:0:{}', 'a:5:{s:10:"allowBlank";s:4:"true";s:9:"maxLength";s:0:"";s:9:"minLength";s:0:"";s:5:"regex";s:0:"";s:9:"regexText";s:0:"";}', 'a:0:{}', 0, ''),
(18, 1, 0, 'text', 'foundation', 'Год основания', '', 0, 4, 0, '', 0, 'default', '', 'a:0:{}', 'a:5:{s:10:"allowBlank";s:4:"true";s:9:"maxLength";s:0:"";s:9:"minLength";s:0:"";s:5:"regex";s:0:"";s:9:"regexText";s:0:"";}', 'a:0:{}', 0, ''),
(19, 1, 0, 'text', 'start_year', 'Год запуска франшизы в РОССИИ', '', 0, 4, 0, '', 0, 'default', '', 'a:0:{}', 'a:5:{s:10:"allowBlank";s:4:"true";s:9:"maxLength";s:0:"";s:9:"minLength";s:0:"";s:5:"regex";s:0:"";s:9:"regexText";s:0:"";}', 'a:0:{}', 0, ''),
(20, 1, 0, 'text', 'franchise_enterprises', 'Франшизных предприятий', '', 0, 4, 0, '', 0, 'default', '', 'a:0:{}', 'a:5:{s:10:"allowBlank";s:4:"true";s:9:"maxLength";s:0:"";s:9:"minLength";s:0:"";s:5:"regex";s:0:"";s:9:"regexText";s:0:"";}', 'a:0:{}', 0, ''),
(21, 1, 0, 'text', 'own_enterprises', 'Собственных предприятий ', '', 0, 4, 0, '', 0, 'default', '', 'a:0:{}', 'a:5:{s:10:"allowBlank";s:4:"true";s:9:"maxLength";s:0:"";s:9:"minLength";s:0:"";s:5:"regex";s:0:"";s:9:"regexText";s:0:"";}', 'a:0:{}', 0, ''),
(22, 1, 0, 'text', 'royalty', 'Роялти', '', 0, 4, 0, '', 0, 'default', '', 'a:0:{}', 'a:5:{s:10:"allowBlank";s:4:"true";s:9:"maxLength";s:0:"";s:9:"minLength";s:0:"";s:5:"regex";s:0:"";s:9:"regexText";s:0:"";}', 'a:0:{}', 0, ''),
(23, 1, 0, 'text', 'advertising', 'Рекламный сбор', '', 0, 4, 0, '', 0, 'default', '', 'a:0:{}', 'a:5:{s:10:"allowBlank";s:4:"true";s:9:"maxLength";s:0:"";s:9:"minLength";s:0:"";s:5:"regex";s:0:"";s:9:"regexText";s:0:"";}', 'a:0:{}', 0, ''),
(24, 1, 0, 'text', 'profitability', 'Срок окупаемости', '', 0, 4, 0, '', 0, 'default', '', 'a:0:{}', 'a:5:{s:10:"allowBlank";s:4:"true";s:9:"maxLength";s:0:"";s:9:"minLength";s:0:"";s:5:"regex";s:0:"";s:9:"regexText";s:0:"";}', 'a:0:{}', 0, ''),
(25, 1, 0, 'text', 'franchise_price', 'Стоимость франшизы ', '', 0, 4, 0, '', 0, 'default', '', 'a:0:{}', 'a:5:{s:10:"allowBlank";s:4:"true";s:9:"maxLength";s:0:"";s:9:"minLength";s:0:"";s:5:"regex";s:0:"";s:9:"regexText";s:0:"";}', 'a:0:{}', 0, ''),
(26, 1, 0, 'text', 'company_title', 'Наименование юр. лица', '', 0, 4, 0, '', 0, 'default', '', 'a:0:{}', 'a:5:{s:10:"allowBlank";s:4:"true";s:9:"maxLength";s:0:"";s:9:"minLength";s:0:"";s:5:"regex";s:0:"";s:9:"regexText";s:0:"";}', 'a:0:{}', 0, ''),
(27, 1, 0, 'text', 'address', 'Адрес', '', 0, 4, 0, '', 0, 'default', '', 'a:0:{}', 'a:5:{s:10:"allowBlank";s:4:"true";s:9:"maxLength";s:0:"";s:9:"minLength";s:0:"";s:5:"regex";s:0:"";s:9:"regexText";s:0:"";}', 'a:0:{}', 0, ''),
(28, 1, 0, 'text', 'email', 'email', '', 0, 4, 0, '', 0, 'default', '', 'a:0:{}', 'a:5:{s:10:"allowBlank";s:4:"true";s:9:"maxLength";s:0:"";s:9:"minLength";s:0:"";s:5:"regex";s:0:"";s:9:"regexText";s:0:"";}', 'a:0:{}', 0, ''),
(29, 1, 0, 'text', 'phone', 'Телефон', '', 0, 4, 0, '', 0, 'default', '', 'a:0:{}', 'a:5:{s:10:"allowBlank";s:4:"true";s:9:"maxLength";s:0:"";s:9:"minLength";s:0:"";s:5:"regex";s:0:"";s:9:"regexText";s:0:"";}', 'a:0:{}', 0, ''),
(30, 1, 0, 'text', 'website', 'Веб-Сайт', '', 0, 4, 0, '', 0, 'default', '', 'a:0:{}', 'a:5:{s:10:"allowBlank";s:4:"true";s:9:"maxLength";s:0:"";s:9:"minLength";s:0:"";s:5:"regex";s:0:"";s:9:"regexText";s:0:"";}', 'a:0:{}', 0, ''),
(31, 1, 0, 'text', 'contact', 'Контактное лицо', '', 0, 4, 0, '', 0, 'default', '', 'a:0:{}', 'a:5:{s:10:"allowBlank";s:4:"true";s:9:"maxLength";s:0:"";s:9:"minLength";s:0:"";s:5:"regex";s:0:"";s:9:"regexText";s:0:"";}', 'a:0:{}', 0, ''),
(32, 1, 0, 'text', 'motherland', 'Страна происхождения', '', 0, 4, 0, '', 0, 'default', '', 'a:0:{}', 'a:5:{s:10:"allowBlank";s:4:"true";s:9:"maxLength";s:0:"";s:9:"minLength";s:0:"";s:5:"regex";s:0:"";s:9:"regexText";s:0:"";}', 'a:0:{}', 0, '');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_site_tmplvar_access`
--

CREATE TABLE `modx_site_tmplvar_access` (
  `id` int(10) UNSIGNED NOT NULL,
  `tmplvarid` int(10) NOT NULL DEFAULT '0',
  `documentgroup` int(10) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `modx_site_tmplvar_contentvalues`
--

CREATE TABLE `modx_site_tmplvar_contentvalues` (
  `id` int(10) UNSIGNED NOT NULL,
  `tmplvarid` int(10) NOT NULL DEFAULT '0',
  `contentid` int(10) NOT NULL DEFAULT '0',
  `value` mediumtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_site_tmplvar_contentvalues`
--

INSERT INTO `modx_site_tmplvar_contentvalues` (`id`, `tmplvarid`, `contentid`, `value`) VALUES
(1, 1, 3, '100000'),
(2, 2, 3, 'images/catalog/187886.png'),
(3, 3, 3, '10'),
(4, 1, 4, '100000'),
(5, 2, 4, 'images/catalog/pira.jpg'),
(6, 3, 4, '10'),
(7, 1, 5, '100000'),
(8, 2, 5, 'images/catalog/city.png'),
(9, 3, 5, '10'),
(10, 1, 6, '100000'),
(11, 2, 6, 'images/catalog/dodo.jpg'),
(12, 3, 6, '10'),
(13, 1, 7, '100000'),
(14, 2, 7, 'images/catalog/jf.png'),
(15, 3, 7, '10'),
(16, 1, 8, '100000'),
(17, 2, 8, 'images/catalog/imgres.jpg'),
(18, 3, 8, '10'),
(19, 1, 9, '100000'),
(20, 2, 9, 'images/catalog/imgres.png'),
(21, 3, 9, '10'),
(22, 1, 10, '100000'),
(23, 2, 10, 'images/catalog/toto.jpg'),
(24, 3, 10, '10'),
(25, 4, 7, 'NEW'),
(26, 11, 9, '2');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_site_tmplvar_templates`
--

CREATE TABLE `modx_site_tmplvar_templates` (
  `tmplvarid` int(10) NOT NULL DEFAULT '0',
  `templateid` int(11) NOT NULL DEFAULT '0',
  `rank` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_site_tmplvar_templates`
--

INSERT INTO `modx_site_tmplvar_templates` (`tmplvarid`, `templateid`, `rank`) VALUES
(1, 2, 0),
(2, 2, 0),
(3, 2, 0),
(4, 2, 0),
(5, 2, 0),
(6, 2, 0),
(7, 2, 0),
(8, 2, 0),
(9, 2, 0),
(10, 2, 0),
(11, 2, 0),
(12, 2, 0),
(17, 2, 0),
(14, 2, 0),
(15, 2, 0),
(16, 2, 0),
(13, 2, 0),
(18, 2, 0),
(19, 2, 0),
(20, 2, 0),
(21, 2, 0),
(22, 2, 0),
(23, 2, 0),
(24, 2, 0),
(25, 2, 0),
(26, 2, 0),
(27, 2, 0),
(28, 2, 0),
(29, 2, 0),
(30, 2, 0),
(31, 2, 0),
(32, 2, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `modx_system_eventnames`
--

CREATE TABLE `modx_system_eventnames` (
  `name` varchar(50) NOT NULL,
  `service` tinyint(4) UNSIGNED NOT NULL DEFAULT '0',
  `groupname` varchar(20) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_system_eventnames`
--

INSERT INTO `modx_system_eventnames` (`name`, `service`, `groupname`) VALUES
('OnPluginEventBeforeSave', 1, 'Plugin Events'),
('OnPluginEventSave', 1, 'Plugin Events'),
('OnPluginEventBeforeRemove', 1, 'Plugin Events'),
('OnPluginEventRemove', 1, 'Plugin Events'),
('OnResourceGroupSave', 1, 'Security'),
('OnResourceGroupBeforeSave', 1, 'Security'),
('OnResourceGroupRemove', 1, 'Security'),
('OnResourceGroupBeforeRemove', 1, 'Security'),
('OnSnippetBeforeSave', 1, 'Snippets'),
('OnSnippetSave', 1, 'Snippets'),
('OnSnippetBeforeRemove', 1, 'Snippets'),
('OnSnippetRemove', 1, 'Snippets'),
('OnSnipFormPrerender', 1, 'Snippets'),
('OnSnipFormRender', 1, 'Snippets'),
('OnBeforeSnipFormSave', 1, 'Snippets'),
('OnSnipFormSave', 1, 'Snippets'),
('OnBeforeSnipFormDelete', 1, 'Snippets'),
('OnSnipFormDelete', 1, 'Snippets'),
('OnTemplateBeforeSave', 1, 'Templates'),
('OnTemplateSave', 1, 'Templates'),
('OnTemplateBeforeRemove', 1, 'Templates'),
('OnTemplateRemove', 1, 'Templates'),
('OnTempFormPrerender', 1, 'Templates'),
('OnTempFormRender', 1, 'Templates'),
('OnBeforeTempFormSave', 1, 'Templates'),
('OnTempFormSave', 1, 'Templates'),
('OnBeforeTempFormDelete', 1, 'Templates'),
('OnTempFormDelete', 1, 'Templates'),
('OnTemplateVarBeforeSave', 1, 'Template Variables'),
('OnTemplateVarSave', 1, 'Template Variables'),
('OnTemplateVarBeforeRemove', 1, 'Template Variables'),
('OnTemplateVarRemove', 1, 'Template Variables'),
('OnTVFormPrerender', 1, 'Template Variables'),
('OnTVFormRender', 1, 'Template Variables'),
('OnBeforeTVFormSave', 1, 'Template Variables'),
('OnTVFormSave', 1, 'Template Variables'),
('OnBeforeTVFormDelete', 1, 'Template Variables'),
('OnTVFormDelete', 1, 'Template Variables'),
('OnTVInputRenderList', 1, 'Template Variables'),
('OnTVInputPropertiesList', 1, 'Template Variables'),
('OnTVOutputRenderList', 1, 'Template Variables'),
('OnTVOutputRenderPropertiesList', 1, 'Template Variables'),
('OnUserGroupBeforeSave', 1, 'User Groups'),
('OnUserGroupSave', 1, 'User Groups'),
('OnUserGroupBeforeRemove', 1, 'User Groups'),
('OnUserGroupRemove', 1, 'User Groups'),
('OnBeforeUserGroupFormSave', 1, 'User Groups'),
('OnUserGroupFormSave', 1, 'User Groups'),
('OnBeforeUserGroupFormRemove', 1, 'User Groups'),
('OnDocFormPrerender', 1, 'Resources'),
('OnDocFormRender', 1, 'Resources'),
('OnBeforeDocFormSave', 1, 'Resources'),
('OnDocFormSave', 1, 'Resources'),
('OnBeforeDocFormDelete', 1, 'Resources'),
('OnDocFormDelete', 1, 'Resources'),
('OnDocPublished', 5, 'Resources'),
('OnDocUnPublished', 5, 'Resources'),
('OnBeforeEmptyTrash', 1, 'Resources'),
('OnEmptyTrash', 1, 'Resources'),
('OnResourceTVFormPrerender', 1, 'Resources'),
('OnResourceTVFormRender', 1, 'Resources'),
('OnResourceAutoPublish', 1, 'Resources'),
('OnResourceDelete', 1, 'Resources'),
('OnResourceUndelete', 1, 'Resources'),
('OnResourceBeforeSort', 1, 'Resources'),
('OnResourceSort', 1, 'Resources'),
('OnResourceDuplicate', 1, 'Resources'),
('OnResourceToolbarLoad', 1, 'Resources'),
('OnResourceRemoveFromResourceGroup', 1, 'Resources'),
('OnResourceAddToResourceGroup', 1, 'Resources'),
('OnRichTextEditorRegister', 1, 'RichText Editor'),
('OnRichTextEditorInit', 1, 'RichText Editor'),
('OnRichTextBrowserInit', 1, 'RichText Editor'),
('OnWebLogin', 3, 'Security'),
('OnBeforeWebLogout', 3, 'Security'),
('OnWebLogout', 3, 'Security'),
('OnManagerLogin', 2, 'Security'),
('OnBeforeManagerLogout', 2, 'Security'),
('OnManagerLogout', 2, 'Security'),
('OnBeforeWebLogin', 3, 'Security'),
('OnWebAuthentication', 3, 'Security'),
('OnBeforeManagerLogin', 2, 'Security'),
('OnManagerAuthentication', 2, 'Security'),
('OnManagerLoginFormRender', 2, 'Security'),
('OnManagerLoginFormPrerender', 2, 'Security'),
('OnPageUnauthorized', 1, 'Security'),
('OnUserFormPrerender', 1, 'Users'),
('OnUserFormRender', 1, 'Users'),
('OnBeforeUserFormSave', 1, 'Users'),
('OnUserFormSave', 1, 'Users'),
('OnBeforeUserFormDelete', 1, 'Users'),
('OnUserFormDelete', 1, 'Users'),
('OnUserNotFound', 1, 'Users'),
('OnBeforeUserActivate', 1, 'Users'),
('OnUserActivate', 1, 'Users'),
('OnBeforeUserDeactivate', 1, 'Users'),
('OnUserDeactivate', 1, 'Users'),
('OnBeforeUserDuplicate', 1, 'Users'),
('OnUserDuplicate', 1, 'Users'),
('OnUserChangePassword', 1, 'Users'),
('OnUserBeforeRemove', 1, 'Users'),
('OnUserBeforeSave', 1, 'Users'),
('OnUserSave', 1, 'Users'),
('OnUserRemove', 1, 'Users'),
('OnUserBeforeAddToGroup', 1, 'User Groups'),
('OnUserAddToGroup', 1, 'User Groups'),
('OnUserBeforeRemoveFromGroup', 1, 'User Groups'),
('OnUserRemoveFromGroup', 1, 'User Groups'),
('OnWebPagePrerender', 5, 'System'),
('OnBeforeCacheUpdate', 4, 'System'),
('OnCacheUpdate', 4, 'System'),
('OnLoadWebPageCache', 4, 'System'),
('OnBeforeSaveWebPageCache', 4, 'System'),
('OnSiteRefresh', 1, 'System'),
('OnFileManagerDirCreate', 1, 'System'),
('OnFileManagerDirRemove', 1, 'System'),
('OnFileManagerDirRename', 1, 'System'),
('OnFileManagerFileRename', 1, 'System'),
('OnFileManagerFileRemove', 1, 'System'),
('OnFileManagerFileUpdate', 1, 'System'),
('OnFileManagerFileCreate', 1, 'System'),
('OnFileManagerBeforeUpload', 1, 'System'),
('OnFileManagerUpload', 1, 'System'),
('OnFileManagerMoveObject', 1, 'System'),
('OnFileCreateFormPrerender', 1, 'System'),
('OnFileEditFormPrerender', 1, 'System'),
('OnManagerPageInit', 2, 'System'),
('OnManagerPageBeforeRender', 2, 'System'),
('OnManagerPageAfterRender', 2, 'System'),
('OnWebPageInit', 5, 'System'),
('OnLoadWebDocument', 5, 'System'),
('OnParseDocument', 5, 'System'),
('OnWebPageComplete', 5, 'System'),
('OnBeforeManagerPageInit', 2, 'System'),
('OnPageNotFound', 1, 'System'),
('OnHandleRequest', 5, 'System'),
('OnMODXInit', 5, 'System'),
('OnElementNotFound', 1, 'System'),
('OnSiteSettingsRender', 1, 'Settings'),
('OnInitCulture', 1, 'Internationalization'),
('OnCategorySave', 1, 'Categories'),
('OnCategoryBeforeSave', 1, 'Categories'),
('OnCategoryRemove', 1, 'Categories'),
('OnCategoryBeforeRemove', 1, 'Categories'),
('OnChunkSave', 1, 'Chunks'),
('OnChunkBeforeSave', 1, 'Chunks'),
('OnChunkRemove', 1, 'Chunks'),
('OnChunkBeforeRemove', 1, 'Chunks'),
('OnChunkFormPrerender', 1, 'Chunks'),
('OnChunkFormRender', 1, 'Chunks'),
('OnBeforeChunkFormSave', 1, 'Chunks'),
('OnChunkFormSave', 1, 'Chunks'),
('OnBeforeChunkFormDelete', 1, 'Chunks'),
('OnChunkFormDelete', 1, 'Chunks'),
('OnContextSave', 1, 'Contexts'),
('OnContextBeforeSave', 1, 'Contexts'),
('OnContextRemove', 1, 'Contexts'),
('OnContextBeforeRemove', 1, 'Contexts'),
('OnContextFormPrerender', 2, 'Contexts'),
('OnContextFormRender', 2, 'Contexts'),
('OnPluginSave', 1, 'Plugins'),
('OnPluginBeforeSave', 1, 'Plugins'),
('OnPluginRemove', 1, 'Plugins'),
('OnPluginBeforeRemove', 1, 'Plugins'),
('OnPluginFormPrerender', 1, 'Plugins'),
('OnPluginFormRender', 1, 'Plugins'),
('OnBeforePluginFormSave', 1, 'Plugins'),
('OnPluginFormSave', 1, 'Plugins'),
('OnBeforePluginFormDelete', 1, 'Plugins'),
('OnPluginFormDelete', 1, 'Plugins'),
('OnPropertySetSave', 1, 'Property Sets'),
('OnPropertySetBeforeSave', 1, 'Property Sets'),
('OnPropertySetRemove', 1, 'Property Sets'),
('OnPropertySetBeforeRemove', 1, 'Property Sets'),
('OnMediaSourceBeforeFormDelete', 1, 'Media Sources'),
('OnMediaSourceBeforeFormSave', 1, 'Media Sources'),
('OnMediaSourceGetProperties', 1, 'Media Sources'),
('OnMediaSourceFormDelete', 1, 'Media Sources'),
('OnMediaSourceFormSave', 1, 'Media Sources'),
('OnMediaSourceDuplicate', 1, 'Media Sources');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_system_settings`
--

CREATE TABLE `modx_system_settings` (
  `key` varchar(50) NOT NULL DEFAULT '',
  `value` text NOT NULL,
  `xtype` varchar(75) NOT NULL DEFAULT 'textfield',
  `namespace` varchar(40) NOT NULL DEFAULT 'core',
  `area` varchar(255) NOT NULL DEFAULT '',
  `editedon` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_system_settings`
--

INSERT INTO `modx_system_settings` (`key`, `value`, `xtype`, `namespace`, `area`, `editedon`) VALUES
('access_category_enabled', '1', 'combo-boolean', 'core', 'authentication', '0000-00-00 00:00:00'),
('access_context_enabled', '1', 'combo-boolean', 'core', 'authentication', '0000-00-00 00:00:00'),
('access_resource_group_enabled', '1', 'combo-boolean', 'core', 'authentication', '0000-00-00 00:00:00'),
('allow_forward_across_contexts', '', 'combo-boolean', 'core', 'system', '0000-00-00 00:00:00'),
('allow_manager_login_forgot_password', '1', 'combo-boolean', 'core', 'authentication', '0000-00-00 00:00:00'),
('allow_multiple_emails', '1', 'combo-boolean', 'core', 'authentication', '0000-00-00 00:00:00'),
('allow_tags_in_post', '', 'combo-boolean', 'core', 'system', '0000-00-00 00:00:00'),
('archive_with', '', 'combo-boolean', 'core', 'system', '0000-00-00 00:00:00'),
('auto_menuindex', '1', 'combo-boolean', 'core', 'site', '0000-00-00 00:00:00'),
('auto_check_pkg_updates', '1', 'combo-boolean', 'core', 'system', '0000-00-00 00:00:00'),
('auto_check_pkg_updates_cache_expire', '15', 'textfield', 'core', 'system', '0000-00-00 00:00:00'),
('automatic_alias', '1', 'combo-boolean', 'core', 'furls', '0000-00-00 00:00:00'),
('base_help_url', '//rtfm.modx.com/display/revolution20/', 'textfield', 'core', 'manager', '0000-00-00 00:00:00'),
('blocked_minutes', '60', 'textfield', 'core', 'authentication', '0000-00-00 00:00:00'),
('cache_action_map', '1', 'combo-boolean', 'core', 'caching', '0000-00-00 00:00:00'),
('cache_alias_map', '1', 'combo-boolean', 'core', 'caching', '0000-00-00 00:00:00'),
('cache_context_settings', '1', 'combo-boolean', 'core', 'caching', '0000-00-00 00:00:00'),
('cache_db', '0', 'combo-boolean', 'core', 'caching', '0000-00-00 00:00:00'),
('cache_db_expires', '0', 'textfield', 'core', 'caching', '0000-00-00 00:00:00'),
('cache_db_session', '0', 'combo-boolean', 'core', 'caching', '0000-00-00 00:00:00'),
('cache_db_session_lifetime', '', 'textfield', 'core', 'caching', '0000-00-00 00:00:00'),
('cache_default', '1', 'combo-boolean', 'core', 'caching', '0000-00-00 00:00:00'),
('cache_disabled', '0', 'combo-boolean', 'core', 'caching', '0000-00-00 00:00:00'),
('cache_expires', '0', 'textfield', 'core', 'caching', '0000-00-00 00:00:00'),
('cache_format', '0', 'textfield', 'core', 'caching', '0000-00-00 00:00:00'),
('cache_handler', 'xPDOFileCache', 'textfield', 'core', 'caching', '0000-00-00 00:00:00'),
('cache_lang_js', '1', 'combo-boolean', 'core', 'caching', '0000-00-00 00:00:00'),
('cache_lexicon_topics', '1', 'combo-boolean', 'core', 'caching', '0000-00-00 00:00:00'),
('cache_noncore_lexicon_topics', '1', 'combo-boolean', 'core', 'caching', '0000-00-00 00:00:00'),
('cache_resource', '1', 'combo-boolean', 'core', 'caching', '0000-00-00 00:00:00'),
('cache_resource_expires', '0', 'textfield', 'core', 'caching', '0000-00-00 00:00:00'),
('cache_scripts', '1', 'combo-boolean', 'core', 'caching', '0000-00-00 00:00:00'),
('cache_system_settings', '1', 'combo-boolean', 'core', 'caching', '0000-00-00 00:00:00'),
('clear_cache_refresh_trees', '0', 'combo-boolean', 'core', 'caching', '0000-00-00 00:00:00'),
('compress_css', '1', 'combo-boolean', 'core', 'manager', '0000-00-00 00:00:00'),
('compress_js', '1', 'combo-boolean', 'core', 'manager', '0000-00-00 00:00:00'),
('compress_js_max_files', '10', 'textfield', 'core', 'manager', '0000-00-00 00:00:00'),
('confirm_navigation', '1', 'combo-boolean', 'core', 'manager', '0000-00-00 00:00:00'),
('container_suffix', '/', 'textfield', 'core', 'furls', '0000-00-00 00:00:00'),
('context_tree_sort', '', 'combo-boolean', 'core', 'manager', '0000-00-00 00:00:00'),
('context_tree_sortby', 'rank', 'textfield', 'core', 'manager', '0000-00-00 00:00:00'),
('context_tree_sortdir', 'ASC', 'textfield', 'core', 'manager', '0000-00-00 00:00:00'),
('cultureKey', 'ru', 'modx-combo-language', 'core', 'language', '2016-06-08 10:26:29'),
('date_timezone', '', 'textfield', 'core', 'system', '0000-00-00 00:00:00'),
('debug', '', 'textfield', 'core', 'system', '0000-00-00 00:00:00'),
('default_duplicate_publish_option', 'preserve', 'textfield', 'core', 'manager', '0000-00-00 00:00:00'),
('default_media_source', '1', 'modx-combo-source', 'core', 'manager', '0000-00-00 00:00:00'),
('default_per_page', '20', 'textfield', 'core', 'manager', '0000-00-00 00:00:00'),
('default_context', 'web', 'modx-combo-context', 'core', 'site', '0000-00-00 00:00:00'),
('default_template', '1', 'modx-combo-template', 'core', 'site', '0000-00-00 00:00:00'),
('default_content_type', '1', 'modx-combo-content-type', 'core', 'site', '0000-00-00 00:00:00'),
('editor_css_path', '', 'textfield', 'core', 'editor', '0000-00-00 00:00:00'),
('editor_css_selectors', '', 'textfield', 'core', 'editor', '0000-00-00 00:00:00'),
('emailsender', 'samodurov.ivan@gmail.com', 'textfield', 'core', 'authentication', '2016-06-08 10:26:29'),
('emailsubject', 'Your login details', 'textfield', 'core', 'authentication', '0000-00-00 00:00:00'),
('enable_dragdrop', '1', 'combo-boolean', 'core', 'manager', '0000-00-00 00:00:00'),
('error_page', '1', 'textfield', 'core', 'site', '0000-00-00 00:00:00'),
('failed_login_attempts', '5', 'textfield', 'core', 'authentication', '0000-00-00 00:00:00'),
('fe_editor_lang', 'en', 'modx-combo-language', 'core', 'language', '0000-00-00 00:00:00'),
('feed_modx_news', 'http://feeds.feedburner.com/modx-announce', 'textfield', 'core', 'system', '0000-00-00 00:00:00'),
('feed_modx_news_enabled', '1', 'combo-boolean', 'core', 'system', '0000-00-00 00:00:00'),
('feed_modx_security', 'http://forums.modx.com/board.xml?board=294', 'textfield', 'core', 'system', '0000-00-00 00:00:00'),
('feed_modx_security_enabled', '1', 'combo-boolean', 'core', 'system', '0000-00-00 00:00:00'),
('filemanager_path', '', 'textfield', 'core', 'file', '0000-00-00 00:00:00'),
('filemanager_path_relative', '1', 'combo-boolean', 'core', 'file', '0000-00-00 00:00:00'),
('filemanager_url', '', 'textfield', 'core', 'file', '0000-00-00 00:00:00'),
('filemanager_url_relative', '1', 'combo-boolean', 'core', 'file', '0000-00-00 00:00:00'),
('forgot_login_email', '<p>Hello [[+username]],</p>\n<p>A request for a password reset has been issued for your MODX user. If you sent this, you may follow this link and use this password to login. If you did not send this request, please ignore this email.</p>\n\n<p>\n    <strong>Activation Link:</strong> [[+url_scheme]][[+http_host]][[+manager_url]]?modahsh=[[+hash]]<br />\n    <strong>Username:</strong> [[+username]]<br />\n    <strong>Password:</strong> [[+password]]<br />\n</p>\n\n<p>After you log into the MODX Manager, you can change your password again, if you wish.</p>\n\n<p>Regards,<br />Site Administrator</p>', 'textarea', 'core', 'authentication', '0000-00-00 00:00:00'),
('form_customization_use_all_groups', '', 'combo-boolean', 'core', 'manager', '0000-00-00 00:00:00'),
('forward_merge_excludes', 'type,published,class_key', 'textfield', 'core', 'system', '0000-00-00 00:00:00'),
('friendly_alias_lowercase_only', '1', 'combo-boolean', 'core', 'furls', '0000-00-00 00:00:00'),
('friendly_alias_max_length', '30', 'textfield', 'core', 'furls', '2016-06-08 10:59:06'),
('friendly_alias_realtime', '1', 'combo-boolean', 'core', 'furls', '2016-06-08 10:57:52'),
('friendly_alias_restrict_chars', 'pattern', 'textfield', 'core', 'furls', '0000-00-00 00:00:00'),
('friendly_alias_restrict_chars_pattern', '/[\\0\\x0B\\t\\n\\r\\f\\a&=+%#<>"~:`@\\?\\[\\]\\{\\}\\|\\^''\\\\]/', 'textfield', 'core', 'furls', '0000-00-00 00:00:00'),
('friendly_alias_strip_element_tags', '1', 'combo-boolean', 'core', 'furls', '0000-00-00 00:00:00'),
('friendly_alias_translit', 'russian', 'textfield', 'core', 'furls', '2016-06-08 10:58:09'),
('friendly_alias_translit_class', 'modx.translit.modTransliterate', 'textfield', 'core', 'furls', '2016-06-08 10:54:28'),
('friendly_alias_translit_class_path', '{core_path}components/translit/model/', 'textfield', 'core', 'furls', '2016-06-08 10:54:28'),
('friendly_alias_trim_chars', '/.-_', 'textfield', 'core', 'furls', '0000-00-00 00:00:00'),
('friendly_alias_word_delimiter', '-', 'textfield', 'core', 'furls', '0000-00-00 00:00:00'),
('friendly_alias_word_delimiters', '-_', 'textfield', 'core', 'furls', '0000-00-00 00:00:00'),
('friendly_urls', '1', 'combo-boolean', 'core', 'furls', '2016-06-08 10:58:18'),
('friendly_urls_strict', '0', 'combo-boolean', 'core', 'furls', '0000-00-00 00:00:00'),
('use_frozen_parent_uris', '1', 'combo-boolean', 'core', 'furls', '2016-06-08 10:58:33'),
('global_duplicate_uri_check', '1', 'combo-boolean', 'core', 'furls', '2016-06-08 10:58:22'),
('hidemenu_default', '0', 'combo-boolean', 'core', 'site', '0000-00-00 00:00:00'),
('inline_help', '1', 'combo-boolean', 'core', 'manager', '0000-00-00 00:00:00'),
('locale', '', 'textfield', 'core', 'language', '0000-00-00 00:00:00'),
('log_level', '1', 'textfield', 'core', 'system', '0000-00-00 00:00:00'),
('log_target', 'FILE', 'textfield', 'core', 'system', '0000-00-00 00:00:00'),
('link_tag_scheme', '-1', 'textfield', 'core', 'site', '0000-00-00 00:00:00'),
('lock_ttl', '360', 'textfield', 'core', 'system', '0000-00-00 00:00:00'),
('mail_charset', 'UTF-8', 'modx-combo-charset', 'core', 'mail', '0000-00-00 00:00:00'),
('mail_encoding', '8bit', 'textfield', 'core', 'mail', '0000-00-00 00:00:00'),
('mail_use_smtp', '', 'combo-boolean', 'core', 'mail', '0000-00-00 00:00:00'),
('mail_smtp_auth', '', 'combo-boolean', 'core', 'mail', '0000-00-00 00:00:00'),
('mail_smtp_helo', '', 'textfield', 'core', 'mail', '0000-00-00 00:00:00'),
('mail_smtp_hosts', 'localhost', 'textfield', 'core', 'mail', '0000-00-00 00:00:00'),
('mail_smtp_keepalive', '', 'combo-boolean', 'core', 'mail', '0000-00-00 00:00:00'),
('mail_smtp_pass', '', 'text-password', 'core', 'mail', '0000-00-00 00:00:00'),
('mail_smtp_port', '587', 'textfield', 'core', 'mail', '0000-00-00 00:00:00'),
('mail_smtp_prefix', '', 'textfield', 'core', 'mail', '0000-00-00 00:00:00'),
('mail_smtp_single_to', '', 'combo-boolean', 'core', 'mail', '0000-00-00 00:00:00'),
('mail_smtp_timeout', '10', 'textfield', 'core', 'mail', '0000-00-00 00:00:00'),
('mail_smtp_user', '', 'textfield', 'core', 'mail', '0000-00-00 00:00:00'),
('manager_date_format', 'Y-m-d', 'textfield', 'core', 'manager', '0000-00-00 00:00:00'),
('manager_favicon_url', '', 'textfield', 'core', 'manager', '0000-00-00 00:00:00'),
('manager_html5_cache', '0', 'combo-boolean', 'core', 'manager', '0000-00-00 00:00:00'),
('manager_js_cache_file_locking', '1', 'combo-boolean', 'core', 'manager', '0000-00-00 00:00:00'),
('manager_js_cache_max_age', '3600', 'textfield', 'core', 'manager', '0000-00-00 00:00:00'),
('manager_js_document_root', '', 'textfield', 'core', 'manager', '0000-00-00 00:00:00'),
('manager_js_zlib_output_compression', '0', 'combo-boolean', 'core', 'manager', '0000-00-00 00:00:00'),
('manager_time_format', 'g:i a', 'textfield', 'core', 'manager', '0000-00-00 00:00:00'),
('manager_direction', 'ltr', 'textfield', 'core', 'language', '0000-00-00 00:00:00'),
('manager_lang_attribute', 'ru', 'textfield', 'core', 'language', '2016-06-08 10:26:29'),
('manager_language', 'ru', 'modx-combo-language', 'core', 'language', '2016-06-08 10:26:29'),
('manager_login_url_alternate', '', 'textfield', 'core', 'authentication', '0000-00-00 00:00:00'),
('manager_theme', 'default', 'modx-combo-manager-theme', 'core', 'manager', '0000-00-00 00:00:00'),
('manager_week_start', '0', 'textfield', 'core', 'manager', '0000-00-00 00:00:00'),
('modx_browser_tree_hide_files', '', 'combo-boolean', 'core', 'manager', '0000-00-00 00:00:00'),
('modx_browser_tree_hide_tooltips', '1', 'combo-boolean', 'core', 'manager', '0000-00-00 00:00:00'),
('modx_browser_default_sort', 'name', 'textfield', 'core', 'manager', '0000-00-00 00:00:00'),
('modx_browser_default_viewmode', 'grid', 'textfield', 'core', 'manager', '0000-00-00 00:00:00'),
('modx_charset', 'UTF-8', 'modx-combo-charset', 'core', 'language', '0000-00-00 00:00:00'),
('principal_targets', 'modAccessContext,modAccessResourceGroup,modAccessCategory,sources.modAccessMediaSource,modAccessNamespace', 'textfield', 'core', 'authentication', '0000-00-00 00:00:00'),
('proxy_auth_type', 'BASIC', 'textfield', 'core', 'proxy', '0000-00-00 00:00:00'),
('proxy_host', '', 'textfield', 'core', 'proxy', '0000-00-00 00:00:00'),
('proxy_password', '', 'text-password', 'core', 'proxy', '0000-00-00 00:00:00'),
('proxy_port', '', 'textfield', 'core', 'proxy', '0000-00-00 00:00:00'),
('proxy_username', '', 'textfield', 'core', 'proxy', '0000-00-00 00:00:00'),
('password_generated_length', '8', 'textfield', 'core', 'authentication', '0000-00-00 00:00:00'),
('password_min_length', '8', 'textfield', 'core', 'authentication', '0000-00-00 00:00:00'),
('phpthumb_allow_src_above_docroot', '', 'combo-boolean', 'core', 'phpthumb', '0000-00-00 00:00:00'),
('phpthumb_cache_maxage', '30', 'textfield', 'core', 'phpthumb', '0000-00-00 00:00:00'),
('phpthumb_cache_maxsize', '100', 'textfield', 'core', 'phpthumb', '0000-00-00 00:00:00'),
('phpthumb_cache_maxfiles', '10000', 'textfield', 'core', 'phpthumb', '0000-00-00 00:00:00'),
('phpthumb_cache_source_enabled', '', 'combo-boolean', 'core', 'phpthumb', '0000-00-00 00:00:00'),
('phpthumb_document_root', '', 'textfield', 'core', 'phpthumb', '0000-00-00 00:00:00'),
('phpthumb_error_bgcolor', 'CCCCFF', 'textfield', 'core', 'phpthumb', '0000-00-00 00:00:00'),
('phpthumb_error_textcolor', 'FF0000', 'textfield', 'core', 'phpthumb', '0000-00-00 00:00:00'),
('phpthumb_error_fontsize', '1', 'textfield', 'core', 'phpthumb', '0000-00-00 00:00:00'),
('phpthumb_far', 'C', 'textfield', 'core', 'phpthumb', '0000-00-00 00:00:00'),
('phpthumb_imagemagick_path', '', 'textfield', 'core', 'phpthumb', '0000-00-00 00:00:00'),
('phpthumb_nohotlink_enabled', '1', 'combo-boolean', 'core', 'phpthumb', '0000-00-00 00:00:00'),
('phpthumb_nohotlink_erase_image', '1', 'combo-boolean', 'core', 'phpthumb', '0000-00-00 00:00:00'),
('phpthumb_nohotlink_valid_domains', '{http_host}', 'textfield', 'core', 'phpthumb', '0000-00-00 00:00:00'),
('phpthumb_nohotlink_text_message', 'Off-server thumbnailing is not allowed', 'textfield', 'core', 'phpthumb', '0000-00-00 00:00:00'),
('phpthumb_nooffsitelink_enabled', '', 'combo-boolean', 'core', 'phpthumb', '0000-00-00 00:00:00'),
('phpthumb_nooffsitelink_erase_image', '1', 'combo-boolean', 'core', 'phpthumb', '0000-00-00 00:00:00'),
('phpthumb_nooffsitelink_require_refer', '', 'combo-boolean', 'core', 'phpthumb', '0000-00-00 00:00:00'),
('phpthumb_nooffsitelink_text_message', 'Off-server linking is not allowed', 'textfield', 'core', 'phpthumb', '0000-00-00 00:00:00'),
('phpthumb_nooffsitelink_valid_domains', '{http_host}', 'textfield', 'core', 'phpthumb', '0000-00-00 00:00:00'),
('phpthumb_nooffsitelink_watermark_src', '', 'textfield', 'core', 'phpthumb', '0000-00-00 00:00:00'),
('phpthumb_zoomcrop', '0', 'textfield', 'core', 'phpthumb', '0000-00-00 00:00:00'),
('publish_default', '', 'combo-boolean', 'core', 'site', '0000-00-00 00:00:00'),
('rb_base_dir', '', 'textfield', 'core', 'file', '0000-00-00 00:00:00'),
('rb_base_url', '', 'textfield', 'core', 'file', '0000-00-00 00:00:00'),
('request_controller', 'index.php', 'textfield', 'core', 'gateway', '0000-00-00 00:00:00'),
('request_method_strict', '0', 'combo-boolean', 'core', 'gateway', '0000-00-00 00:00:00'),
('request_param_alias', 'q', 'textfield', 'core', 'gateway', '0000-00-00 00:00:00'),
('request_param_id', 'id', 'textfield', 'core', 'gateway', '0000-00-00 00:00:00'),
('resolve_hostnames', '0', 'combo-boolean', 'core', 'system', '0000-00-00 00:00:00'),
('resource_tree_node_name', 'pagetitle', 'textfield', 'core', 'manager', '0000-00-00 00:00:00'),
('resource_tree_node_name_fallback', 'pagetitle', 'textfield', 'core', 'manager', '0000-00-00 00:00:00'),
('resource_tree_node_tooltip', '', 'textfield', 'core', 'manager', '0000-00-00 00:00:00'),
('richtext_default', '1', 'combo-boolean', 'core', 'manager', '0000-00-00 00:00:00'),
('search_default', '1', 'combo-boolean', 'core', 'site', '0000-00-00 00:00:00'),
('server_offset_time', '0', 'textfield', 'core', 'system', '0000-00-00 00:00:00'),
('server_protocol', 'http', 'textfield', 'core', 'system', '0000-00-00 00:00:00'),
('session_cookie_domain', '', 'textfield', 'core', 'session', '0000-00-00 00:00:00'),
('default_username', '(anonymous)', 'textfield', 'core', 'session', '0000-00-00 00:00:00'),
('anonymous_sessions', '1', 'combo-boolean', 'core', 'session', '0000-00-00 00:00:00'),
('session_cookie_lifetime', '604800', 'textfield', 'core', 'session', '0000-00-00 00:00:00'),
('session_cookie_path', '', 'textfield', 'core', 'session', '0000-00-00 00:00:00'),
('session_cookie_secure', '', 'combo-boolean', 'core', 'session', '0000-00-00 00:00:00'),
('session_cookie_httponly', '1', 'combo-boolean', 'core', 'session', '0000-00-00 00:00:00'),
('session_gc_maxlifetime', '604800', 'textfield', 'core', 'session', '0000-00-00 00:00:00'),
('session_handler_class', 'modSessionHandler', 'textfield', 'core', 'session', '0000-00-00 00:00:00'),
('session_name', '', 'textfield', 'core', 'session', '0000-00-00 00:00:00'),
('set_header', '1', 'combo-boolean', 'core', 'system', '0000-00-00 00:00:00'),
('send_poweredby_header', '0', 'combo-boolean', 'core', 'system', '0000-00-00 00:00:00'),
('show_tv_categories_header', '1', 'combo-boolean', 'core', 'manager', '0000-00-00 00:00:00'),
('signupemail_message', '<p>Hello [[+uid]],</p>\n    <p>Here are your login details for the [[+sname]] MODX Manager:</p>\n\n    <p>\n        <strong>Username:</strong> [[+uid]]<br />\n        <strong>Password:</strong> [[+pwd]]<br />\n    </p>\n\n    <p>Once you log into the MODX Manager at [[+surl]], you can change your password.</p>\n\n    <p>Regards,<br />Site Administrator</p>', 'textarea', 'core', 'authentication', '0000-00-00 00:00:00'),
('site_name', 'MODX Revolution', 'textfield', 'core', 'site', '0000-00-00 00:00:00'),
('site_start', '1', 'textfield', 'core', 'site', '0000-00-00 00:00:00'),
('site_status', '1', 'combo-boolean', 'core', 'site', '0000-00-00 00:00:00'),
('site_unavailable_message', 'The site is currently unavailable', 'textfield', 'core', 'site', '0000-00-00 00:00:00'),
('site_unavailable_page', '0', 'textfield', 'core', 'site', '0000-00-00 00:00:00'),
('strip_image_paths', '1', 'combo-boolean', 'core', 'file', '0000-00-00 00:00:00'),
('symlink_merge_fields', '1', 'combo-boolean', 'core', 'site', '0000-00-00 00:00:00'),
('syncsite_default', '1', 'combo-boolean', 'core', 'caching', '0000-00-00 00:00:00'),
('topmenu_show_descriptions', '1', 'combo-boolean', 'core', 'manager', '0000-00-00 00:00:00'),
('tree_default_sort', 'menuindex', 'textfield', 'core', 'manager', '0000-00-00 00:00:00'),
('tree_root_id', '0', 'numberfield', 'core', 'manager', '0000-00-00 00:00:00'),
('tvs_below_content', '0', 'combo-boolean', 'core', 'manager', '0000-00-00 00:00:00'),
('udperms_allowroot', '', 'combo-boolean', 'core', 'authentication', '0000-00-00 00:00:00'),
('unauthorized_page', '1', 'textfield', 'core', 'site', '0000-00-00 00:00:00'),
('upload_files', 'txt,html,htm,xml,js,css,zip,gz,rar,z,tgz,tar,htaccess,mp3,mp4,aac,wav,au,wmv,avi,mpg,mpeg,pdf,doc,docx,xls,xlsx,ppt,pptx,jpg,jpeg,png,tiff,svg,svgz,gif,psd,ico,bmp,odt,ods,odp,odb,odg,odf,md,ttf,woff,eot', 'textfield', 'core', 'file', '0000-00-00 00:00:00'),
('upload_flash', 'swf,fla', 'textfield', 'core', 'file', '0000-00-00 00:00:00'),
('upload_images', 'jpg,jpeg,png,gif,psd,ico,bmp,tiff,svg,svgz', 'textfield', 'core', 'file', '0000-00-00 00:00:00'),
('upload_maxsize', '33554432', 'textfield', 'core', 'file', '2016-06-08 10:26:29'),
('upload_media', 'mp3,wav,au,wmv,avi,mpg,mpeg', 'textfield', 'core', 'file', '0000-00-00 00:00:00'),
('use_alias_path', '1', 'combo-boolean', 'core', 'furls', '2016-06-08 10:58:31'),
('use_browser', '1', 'combo-boolean', 'core', 'file', '0000-00-00 00:00:00'),
('use_editor', '1', 'combo-boolean', 'core', 'editor', '0000-00-00 00:00:00'),
('use_multibyte', '1', 'combo-boolean', 'core', 'language', '2016-06-08 10:26:29'),
('use_weblink_target', '', 'combo-boolean', 'core', 'site', '0000-00-00 00:00:00'),
('webpwdreminder_message', '<p>Hello [[+uid]],</p>\n\n    <p>To activate your new password click the following link:</p>\n\n    <p>[[+surl]]</p>\n\n    <p>If successful you can use the following password to login:</p>\n\n    <p><strong>Password:</strong> [[+pwd]]</p>\n\n    <p>If you did not request this email then please ignore it.</p>\n\n    <p>Regards,<br />\n    Site Administrator</p>', 'textarea', 'core', 'authentication', '0000-00-00 00:00:00'),
('websignupemail_message', '<p>Hello [[+uid]],</p>\n\n    <p>Here are your login details for [[+sname]]:</p>\n\n    <p><strong>Username:</strong> [[+uid]]<br />\n    <strong>Password:</strong> [[+pwd]]</p>\n\n    <p>Once you log into [[+sname]] at [[+surl]], you can change your password.</p>\n\n    <p>Regards,<br />\n    Site Administrator</p>', 'textarea', 'core', 'authentication', '0000-00-00 00:00:00'),
('welcome_screen', '', 'combo-boolean', 'core', 'manager', '2016-06-08 10:27:09'),
('welcome_screen_url', '//misc.modx.com/revolution/welcome.25.html ', 'textfield', 'core', 'manager', '0000-00-00 00:00:00'),
('welcome_action', 'welcome', 'textfield', 'core', 'manager', '0000-00-00 00:00:00'),
('welcome_namespace', 'core', 'textfield', 'core', 'manager', '0000-00-00 00:00:00'),
('which_editor', 'CKEditor', 'modx-combo-rte', 'core', 'editor', '2016-06-08 10:53:59'),
('which_element_editor', 'Ace', 'modx-combo-rte', 'core', 'editor', '2016-06-08 10:53:45'),
('xhtml_urls', '1', 'combo-boolean', 'core', 'site', '0000-00-00 00:00:00'),
('enable_gravatar', '1', 'combo-boolean', 'core', 'manager', '0000-00-00 00:00:00'),
('mgr_tree_icon_context', 'tree-context', 'textfield', 'core', 'manager', '0000-00-00 00:00:00'),
('mgr_source_icon', 'icon-folder-open-o', 'textfield', 'core', 'manager', '0000-00-00 00:00:00'),
('main_nav_parent', 'topnav', 'textfield', 'core', 'manager', '0000-00-00 00:00:00'),
('user_nav_parent', 'usernav', 'textfield', 'core', 'manager', '0000-00-00 00:00:00'),
('auto_isfolder', '1', 'combo-boolean', 'core', 'site', '0000-00-00 00:00:00'),
('manager_use_fullname', '', 'combo-boolean', 'core', 'manager', '0000-00-00 00:00:00'),
('parser_recurse_uncacheable', '1', 'combo-boolean', 'core', 'system', '0000-00-00 00:00:00'),
('preserve_menuindex', '1', 'combo-boolean', 'core', 'manager', '0000-00-00 00:00:00'),
('settings_version', '2.5.0-pl', 'textfield', 'core', 'system', '0000-00-00 00:00:00'),
('settings_distro', 'traditional', 'textfield', 'core', 'system', '0000-00-00 00:00:00'),
('ace.theme', 'chrome', 'textfield', 'ace', 'general', '0000-00-00 00:00:00'),
('ace.font_size', '13px', 'textfield', 'ace', 'general', '0000-00-00 00:00:00'),
('ace.word_wrap', '', 'combo-boolean', 'ace', 'general', '0000-00-00 00:00:00'),
('ace.soft_tabs', '1', 'combo-boolean', 'ace', 'general', '0000-00-00 00:00:00'),
('ace.tab_size', '4', 'textfield', 'ace', 'general', '0000-00-00 00:00:00'),
('ace.fold_widgets', '1', 'combo-boolean', 'ace', 'general', '0000-00-00 00:00:00'),
('ace.show_invisibles', '0', 'combo-boolean', 'ace', 'general', '0000-00-00 00:00:00'),
('ace.snippets', '', 'textarea', 'ace', 'general', '0000-00-00 00:00:00'),
('ace.height', '', 'textfield', 'ace', 'general', '0000-00-00 00:00:00'),
('ckeditor.ui_color', '#DDDDDD', 'textfield', 'ckeditor', 'general', '0000-00-00 00:00:00'),
('ckeditor.toolbar', '', 'textarea', 'ckeditor', 'general', '0000-00-00 00:00:00'),
('ckeditor.toolbar_groups', '[{"name":"document","groups":["mode","document","doctools"]},{"name":"clipboard","groups":["clipboard","undo"]},{"name":"editing","groups":["find","selection","spellchecker"]},{"name":"links"},{"name":"insert"},{"name":"forms"},"/",{"name":"basicstyles","groups":["basicstyles","cleanup"]},{"name":"paragraph","groups":["list","indent","blocks","align","bidi"]},{"name":"styles"},{"name":"colors"},{"name":"tools"},{"name":"others"},{"name":"about"}]', 'textarea', 'ckeditor', 'general', '0000-00-00 00:00:00'),
('ckeditor.format_tags', 'p;h1;h2;h3;h4;h5;h6;pre;address;div', 'textfield', 'ckeditor', 'general', '0000-00-00 00:00:00'),
('ckeditor.skin', 'moono', 'textfield', 'ckeditor', 'general', '0000-00-00 00:00:00'),
('ckeditor.extra_plugins', '', 'textfield', 'ckeditor', 'general', '0000-00-00 00:00:00'),
('ckeditor.object_resizing', '0', 'combo-boolean', 'ckeditor', 'general', '0000-00-00 00:00:00'),
('ckeditor.autocorrect_dash', '—', 'textfield', 'ckeditor', 'general', '0000-00-00 00:00:00'),
('ckeditor.autocorrect_double_quotes', '«»', 'textfield', 'ckeditor', 'general', '0000-00-00 00:00:00'),
('ckeditor.autocorrect_single_quotes', '„“', 'textfield', 'ckeditor', 'general', '0000-00-00 00:00:00'),
('ckeditor.styles_set', 'default', 'textarea', 'ckeditor', 'general', '0000-00-00 00:00:00'),
('ckeditor.remove_plugins', 'forms,smiley,autogrow,liststyle,justify,pagebreak,colorbutton,indentblock,font,newpage,print,save,language,bidi,selectall,preview', 'textfield', 'ckeditor', 'general', '0000-00-00 00:00:00'),
('ckeditor.native_spellchecker', '1', 'combo-boolean', 'ckeditor', 'general', '0000-00-00 00:00:00'),
('tagger.place_above_content_header', '1', 'combo-boolean', 'tagger', 'places', '0000-00-00 00:00:00'),
('tagger.place_below_content_header', '1', 'combo-boolean', 'tagger', 'places', '0000-00-00 00:00:00'),
('tagger.place_bottom_page_header', '1', 'combo-boolean', 'tagger', 'places', '0000-00-00 00:00:00'),
('tagger.place_in_tab_label', 'tagger.tab.label', 'textfield', 'tagger', 'places', '0000-00-00 00:00:00'),
('tagger.place_tvs_tab_label', 'tagger.tab.label', 'textfield', 'tagger', 'places', '0000-00-00 00:00:00'),
('tagger.place_above_content_label', 'tagger.tab.label', 'textfield', 'tagger', 'places', '0000-00-00 00:00:00'),
('tagger.place_below_content_label', 'tagger.tab.label', 'textfield', 'tagger', 'places', '0000-00-00 00:00:00'),
('tagger.place_bottom_page_label', 'tagger.tab.label', 'textfield', 'tagger', 'places', '0000-00-00 00:00:00'),
('gallery.backend_thumb_far', 'C', 'textfield', 'gallery', 'backend', '0000-00-00 00:00:00'),
('gallery.backend_thumb_height', '80', 'textfield', 'gallery', 'backend', '0000-00-00 00:00:00'),
('gallery.backend_thumb_width', '100', 'textfield', 'gallery', 'backend', '0000-00-00 00:00:00'),
('gallery.backend_thumb_zoomcrop', '1', 'combo-boolean', 'gallery', 'backend', '0000-00-00 00:00:00'),
('gallery.default_batch_upload_path', '{assets_path}images/', 'textfield', 'gallery', 'backend', '0000-00-00 00:00:00'),
('gallery.thumbs_prepend_site_url', '', 'combo-boolean', 'gallery', '', '0000-00-00 00:00:00'),
('gallery.mediaSource', '1', 'modx-combo-source', 'gallery', '', '0000-00-00 00:00:00'),
('gallery.use_richtext', '', 'combo-boolean', 'gallery', 'TinyMCE', '0000-00-00 00:00:00'),
('gallery.tiny.width', '95%', 'textfield', 'gallery', 'TinyMCE', '0000-00-00 00:00:00'),
('gallery.tiny.height', '200', 'textfield', 'gallery', 'TinyMCE', '0000-00-00 00:00:00'),
('gallery.tiny.buttons1', 'undo,redo,selectall,pastetext,pasteword,charmap,separator,image,modxlink,unlink,media,separator,code,help', 'textfield', 'gallery', 'TinyMCE', '0000-00-00 00:00:00'),
('gallery.tiny.buttons2', 'bold,italic,underline,strikethrough,sub,sup,separator,bullist,numlist,outdent,indent,separator,justifyleft,justifycenter,justifyright,justifyfull', 'textfield', 'gallery', 'TinyMCE', '0000-00-00 00:00:00'),
('gallery.tiny.buttons3', 'styleselect,formatselect,separator,styleprops', 'textfield', 'gallery', 'TinyMCE', '0000-00-00 00:00:00'),
('gallery.tiny.buttons4', '', 'textfield', 'gallery', 'TinyMCE', '0000-00-00 00:00:00'),
('gallery.tiny.buttons5', '', 'textfield', 'gallery', 'TinyMCE', '0000-00-00 00:00:00'),
('gallery.tiny.custom_plugins', '', 'textfield', 'gallery', 'TinyMCE', '0000-00-00 00:00:00'),
('gallery.tiny.theme', '', 'textfield', 'gallery', 'TinyMCE', '0000-00-00 00:00:00'),
('gallery.tiny.theme_advanced_blockformats', '', 'textfield', 'gallery', 'TinyMCE', '0000-00-00 00:00:00'),
('gallery.tiny.theme_advanced_css_selectors', '', 'textfield', 'gallery', 'TinyMCE', '0000-00-00 00:00:00'),
('gallery.files_path', '[[++assets_path]]gallery/', 'textfield', 'gallery', 'Paths', '0000-00-00 00:00:00'),
('gallery.files_url', '[[++assets_url]]gallery/', 'textfield', 'gallery', 'Paths', '0000-00-00 00:00:00'),
('gallery.file_structure_version', '1.0', 'textfield', 'gallery', 'system', '0000-00-00 00:00:00'),
('extension_packages', '[{"gallery":{"path":"[[++core_path]]components/gallery/model/"}}]', 'textfield', 'core', 'system', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_tagger_groups`
--

CREATE TABLE `modx_tagger_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `alias` varchar(100) NOT NULL,
  `field_type` varchar(100) NOT NULL,
  `allow_new` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `remove_unused` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `allow_blank` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `allow_type` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `show_autotag` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `hide_input` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `tag_limit` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `show_for_templates` text NOT NULL,
  `place` varchar(100) NOT NULL DEFAULT 'in-tab',
  `position` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `in_tvs_position` int(10) UNSIGNED NOT NULL DEFAULT '9999'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_tagger_groups`
--

INSERT INTO `modx_tagger_groups` (`id`, `name`, `alias`, `field_type`, `allow_new`, `remove_unused`, `allow_blank`, `allow_type`, `show_autotag`, `hide_input`, `tag_limit`, `show_for_templates`, `place`, `position`, `description`, `in_tvs_position`) VALUES
(1, 'Каталог франшиз', 'catalog-fr', 'tagger-field-tags', 1, 0, 1, 0, 1, 0, 0, '2', 'bottom-page', 0, 'Каталог', 9999);

-- --------------------------------------------------------

--
-- Структура таблицы `modx_tagger_tags`
--

CREATE TABLE `modx_tagger_tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `tag` varchar(100) NOT NULL,
  `alias` varchar(100) NOT NULL,
  `group` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_tagger_tags`
--

INSERT INTO `modx_tagger_tags` (`id`, `tag`, `alias`, `group`) VALUES
(1, 'Франшизы 2016', '2016', 1),
(2, 'Недорогие франшизы', 'lowprice', 1),
(3, 'Франшизы до 300 тыс. руб.', 'less300', 1),
(4, 'Франшизы до 100 тыс. руб.', 'less100', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `modx_tagger_tag_resources`
--

CREATE TABLE `modx_tagger_tag_resources` (
  `tag` int(10) UNSIGNED NOT NULL,
  `resource` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_tagger_tag_resources`
--

INSERT INTO `modx_tagger_tag_resources` (`tag`, `resource`) VALUES
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10);

-- --------------------------------------------------------

--
-- Структура таблицы `modx_transport_packages`
--

CREATE TABLE `modx_transport_packages` (
  `signature` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `installed` datetime DEFAULT NULL,
  `state` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `workspace` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `provider` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `disabled` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `source` tinytext,
  `manifest` text,
  `attributes` mediumtext,
  `package_name` varchar(255) NOT NULL,
  `metadata` text,
  `version_major` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `version_minor` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `version_patch` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `release` varchar(100) NOT NULL DEFAULT '',
  `release_index` smallint(5) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_transport_packages`
--

INSERT INTO `modx_transport_packages` (`signature`, `created`, `updated`, `installed`, `state`, `workspace`, `provider`, `disabled`, `source`, `manifest`, `attributes`, `package_name`, `metadata`, `version_major`, `version_minor`, `version_patch`, `release`, `release_index`) VALUES
('getresources-1.6.1-pl', '2016-06-08 01:35:52', '2016-06-08 10:54:12', '2016-06-08 13:54:12', 0, 1, 1, 0, 'getresources-1.6.1-pl.transport.zip', NULL, 'a:34:{s:7:"license";s:15218:"GNU GENERAL PUBLIC LICENSE\n   Version 2, June 1991\n--------------------------\n\nCopyright (C) 1989, 1991 Free Software Foundation, Inc.\n59 Temple Place, Suite 330, Boston, MA  02111-1307  USA\n\nEveryone is permitted to copy and distribute verbatim copies\nof this license document, but changing it is not allowed.\n\nPreamble\n--------\n\n  The licenses for most software are designed to take away your\nfreedom to share and change it.  By contrast, the GNU General Public\nLicense is intended to guarantee your freedom to share and change free\nsoftware--to make sure the software is free for all its users.  This\nGeneral Public License applies to most of the Free Software\nFoundation''s software and to any other program whose authors commit to\nusing it.  (Some other Free Software Foundation software is covered by\nthe GNU Library General Public License instead.)  You can apply it to\nyour programs, too.\n\n  When we speak of free software, we are referring to freedom, not\nprice.  Our General Public Licenses are designed to make sure that you\nhave the freedom to distribute copies of free software (and charge for\nthis service if you wish), that you receive source code or can get it\nif you want it, that you can change the software or use pieces of it\nin new free programs; and that you know you can do these things.\n\n  To protect your rights, we need to make restrictions that forbid\nanyone to deny you these rights or to ask you to surrender the rights.\nThese restrictions translate to certain responsibilities for you if you\ndistribute copies of the software, or if you modify it.\n\n  For example, if you distribute copies of such a program, whether\ngratis or for a fee, you must give the recipients all the rights that\nyou have.  You must make sure that they, too, receive or can get the\nsource code.  And you must show them these terms so they know their\nrights.\n\n  We protect your rights with two steps: (1) copyright the software, and\n(2) offer you this license which gives you legal permission to copy,\ndistribute and/or modify the software.\n\n  Also, for each author''s protection and ours, we want to make certain\nthat everyone understands that there is no warranty for this free\nsoftware.  If the software is modified by someone else and passed on, we\nwant its recipients to know that what they have is not the original, so\nthat any problems introduced by others will not reflect on the original\nauthors'' reputations.\n\n  Finally, any free program is threatened constantly by software\npatents.  We wish to avoid the danger that redistributors of a free\nprogram will individually obtain patent licenses, in effect making the\nprogram proprietary.  To prevent this, we have made it clear that any\npatent must be licensed for everyone''s free use or not licensed at all.\n\n  The precise terms and conditions for copying, distribution and\nmodification follow.\n\n\nGNU GENERAL PUBLIC LICENSE\nTERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION\n---------------------------------------------------------------\n\n  0. This License applies to any program or other work which contains\na notice placed by the copyright holder saying it may be distributed\nunder the terms of this General Public License.  The "Program", below,\nrefers to any such program or work, and a "work based on the Program"\nmeans either the Program or any derivative work under copyright law:\nthat is to say, a work containing the Program or a portion of it,\neither verbatim or with modifications and/or translated into another\nlanguage.  (Hereinafter, translation is included without limitation in\nthe term "modification".)  Each licensee is addressed as "you".\n\nActivities other than copying, distribution and modification are not\ncovered by this License; they are outside its scope.  The act of\nrunning the Program is not restricted, and the output from the Program\nis covered only if its contents constitute a work based on the\nProgram (independent of having been made by running the Program).\nWhether that is true depends on what the Program does.\n\n  1. You may copy and distribute verbatim copies of the Program''s\nsource code as you receive it, in any medium, provided that you\nconspicuously and appropriately publish on each copy an appropriate\ncopyright notice and disclaimer of warranty; keep intact all the\nnotices that refer to this License and to the absence of any warranty;\nand give any other recipients of the Program a copy of this License\nalong with the Program.\n\nYou may charge a fee for the physical act of transferring a copy, and\nyou may at your option offer warranty protection in exchange for a fee.\n\n  2. You may modify your copy or copies of the Program or any portion\nof it, thus forming a work based on the Program, and copy and\ndistribute such modifications or work under the terms of Section 1\nabove, provided that you also meet all of these conditions:\n\n    a) You must cause the modified files to carry prominent notices\n    stating that you changed the files and the date of any change.\n\n    b) You must cause any work that you distribute or publish, that in\n    whole or in part contains or is derived from the Program or any\n    part thereof, to be licensed as a whole at no charge to all third\n    parties under the terms of this License.\n\n    c) If the modified program normally reads commands interactively\n    when run, you must cause it, when started running for such\n    interactive use in the most ordinary way, to print or display an\n    announcement including an appropriate copyright notice and a\n    notice that there is no warranty (or else, saying that you provide\n    a warranty) and that users may redistribute the program under\n    these conditions, and telling the user how to view a copy of this\n    License.  (Exception: if the Program itself is interactive but\n    does not normally print such an announcement, your work based on\n    the Program is not required to print an announcement.)\n\nThese requirements apply to the modified work as a whole.  If\nidentifiable sections of that work are not derived from the Program,\nand can be reasonably considered independent and separate works in\nthemselves, then this License, and its terms, do not apply to those\nsections when you distribute them as separate works.  But when you\ndistribute the same sections as part of a whole which is a work based\non the Program, the distribution of the whole must be on the terms of\nthis License, whose permissions for other licensees extend to the\nentire whole, and thus to each and every part regardless of who wrote it.\n\nThus, it is not the intent of this section to claim rights or contest\nyour rights to work written entirely by you; rather, the intent is to\nexercise the right to control the distribution of derivative or\ncollective works based on the Program.\n\nIn addition, mere aggregation of another work not based on the Program\nwith the Program (or with a work based on the Program) on a volume of\na storage or distribution medium does not bring the other work under\nthe scope of this License.\n\n  3. You may copy and distribute the Program (or a work based on it,\nunder Section 2) in object code or executable form under the terms of\nSections 1 and 2 above provided that you also do one of the following:\n\n    a) Accompany it with the complete corresponding machine-readable\n    source code, which must be distributed under the terms of Sections\n    1 and 2 above on a medium customarily used for software interchange; or,\n\n    b) Accompany it with a written offer, valid for at least three\n    years, to give any third party, for a charge no more than your\n    cost of physically performing source distribution, a complete\n    machine-readable copy of the corresponding source code, to be\n    distributed under the terms of Sections 1 and 2 above on a medium\n    customarily used for software interchange; or,\n\n    c) Accompany it with the information you received as to the offer\n    to distribute corresponding source code.  (This alternative is\n    allowed only for noncommercial distribution and only if you\n    received the program in object code or executable form with such\n    an offer, in accord with Subsection b above.)\n\nThe source code for a work means the preferred form of the work for\nmaking modifications to it.  For an executable work, complete source\ncode means all the source code for all modules it contains, plus any\nassociated interface definition files, plus the scripts used to\ncontrol compilation and installation of the executable.  However, as a\nspecial exception, the source code distributed need not include\nanything that is normally distributed (in either source or binary\nform) with the major components (compiler, kernel, and so on) of the\noperating system on which the executable runs, unless that component\nitself accompanies the executable.\n\nIf distribution of executable or object code is made by offering\naccess to copy from a designated place, then offering equivalent\naccess to copy the source code from the same place counts as\ndistribution of the source code, even though third parties are not\ncompelled to copy the source along with the object code.\n\n  4. You may not copy, modify, sublicense, or distribute the Program\nexcept as expressly provided under this License.  Any attempt\notherwise to copy, modify, sublicense or distribute the Program is\nvoid, and will automatically terminate your rights under this License.\nHowever, parties who have received copies, or rights, from you under\nthis License will not have their licenses terminated so long as such\nparties remain in full compliance.\n\n  5. You are not required to accept this License, since you have not\nsigned it.  However, nothing else grants you permission to modify or\ndistribute the Program or its derivative works.  These actions are\nprohibited by law if you do not accept this License.  Therefore, by\nmodifying or distributing the Program (or any work based on the\nProgram), you indicate your acceptance of this License to do so, and\nall its terms and conditions for copying, distributing or modifying\nthe Program or works based on it.\n\n  6. Each time you redistribute the Program (or any work based on the\nProgram), the recipient automatically receives a license from the\noriginal licensor to copy, distribute or modify the Program subject to\nthese terms and conditions.  You may not impose any further\nrestrictions on the recipients'' exercise of the rights granted herein.\nYou are not responsible for enforcing compliance by third parties to\nthis License.\n\n  7. If, as a consequence of a court judgment or allegation of patent\ninfringement or for any other reason (not limited to patent issues),\nconditions are imposed on you (whether by court order, agreement or\notherwise) that contradict the conditions of this License, they do not\nexcuse you from the conditions of this License.  If you cannot\ndistribute so as to satisfy simultaneously your obligations under this\nLicense and any other pertinent obligations, then as a consequence you\nmay not distribute the Program at all.  For example, if a patent\nlicense would not permit royalty-free redistribution of the Program by\nall those who receive copies directly or indirectly through you, then\nthe only way you could satisfy both it and this License would be to\nrefrain entirely from distribution of the Program.\n\nIf any portion of this section is held invalid or unenforceable under\nany particular circumstance, the balance of the section is intended to\napply and the section as a whole is intended to apply in other\ncircumstances.\n\nIt is not the purpose of this section to induce you to infringe any\npatents or other property right claims or to contest validity of any\nsuch claims; this section has the sole purpose of protecting the\nintegrity of the free software distribution system, which is\nimplemented by public license practices.  Many people have made\ngenerous contributions to the wide range of software distributed\nthrough that system in reliance on consistent application of that\nsystem; it is up to the author/donor to decide if he or she is willing\nto distribute software through any other system and a licensee cannot\nimpose that choice.\n\nThis section is intended to make thoroughly clear what is believed to\nbe a consequence of the rest of this License.\n\n  8. If the distribution and/or use of the Program is restricted in\ncertain countries either by patents or by copyrighted interfaces, the\noriginal copyright holder who places the Program under this License\nmay add an explicit geographical distribution limitation excluding\nthose countries, so that distribution is permitted only in or among\ncountries not thus excluded.  In such case, this License incorporates\nthe limitation as if written in the body of this License.\n\n  9. The Free Software Foundation may publish revised and/or new versions\nof the General Public License from time to time.  Such new versions will\nbe similar in spirit to the present version, but may differ in detail to\naddress new problems or concerns.\n\nEach version is given a distinguishing version number.  If the Program\nspecifies a version number of this License which applies to it and "any\nlater version", you have the option of following the terms and conditions\neither of that version or of any later version published by the Free\nSoftware Foundation.  If the Program does not specify a version number of\nthis License, you may choose any version ever published by the Free Software\nFoundation.\n\n  10. If you wish to incorporate parts of the Program into other free\nprograms whose distribution conditions are different, write to the author\nto ask for permission.  For software which is copyrighted by the Free\nSoftware Foundation, write to the Free Software Foundation; we sometimes\nmake exceptions for this.  Our decision will be guided by the two goals\nof preserving the free status of all derivatives of our free software and\nof promoting the sharing and reuse of software generally.\n\nNO WARRANTY\n-----------\n\n  11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY\nFOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN\nOTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES\nPROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED\nOR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF\nMERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS\nTO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE\nPROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,\nREPAIR OR CORRECTION.\n\n  12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING\nWILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR\nREDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,\nINCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING\nOUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED\nTO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY\nYOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER\nPROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE\nPOSSIBILITY OF SUCH DAMAGES.\n\n---------------------------\nEND OF TERMS AND CONDITIONS";s:6:"readme";s:336:"--------------------\nSnippet: getResources\n--------------------\nVersion: 1.6.0-pl\nReleased: December 30, 2013\nSince: December 28, 2009\nAuthor: Jason Coward <jason@opengeek.com>\n\nA general purpose Resource listing and summarization snippet for MODX Revolution.\n\nOfficial Documentation:\nhttp://docs.modxcms.com/display/ADDON/getResources\n";s:9:"changelog";s:3492:"Changelog for getResources.\n\ngetResources 1.6.1-pl (December 30, 2013)\n====================================\n- Allow tvFilter values to contain filter operators\n- Allow 0-based idx\n- Pass scriptProperties to wrapperTpl\n- [#30][#80] Only dump properties for invalid tpl when debug enabled\n\ngetResources 1.6.0-pl (February 19, 2013)\n====================================\n- Add tplWrapper for specifying a wrapper template\n\ngetResources 1.5.1-pl (August 23, 2012)\n====================================\n- Add tplOperator property to default properties\n- [#73] Add between tplOperator to conditionalTpls\n\ngetResources 1.5.0-pl (June 15, 2012)\n====================================\n- [#58] Add tplCondition/conditionalTpls support\n- [#67] Add odd property\n- [#60] Allow custom delimiters for tvFilters\n- [#63] Give tplFirst/tplLast precedence over tpl_X/tpl_nX\n- Automatically prepare TV values for media-source dependent TVs\n\ngetResources 1.4.2-pl (December 9, 2011)\n====================================\n- [#25] Add new operators to tvFilters\n- [#37] Consider default values with tvFilters\n- [#57] Fix tpl overrides and improve order\n\ngetResources 1.4.1-pl (December 8, 2011)\n====================================\n- [#57] Add support for factor-based tpls\n- [#54], [#55] Fix processTVList feature\n\ngetResources 1.4.0-pl (September 21, 2011)\n====================================\n- [#50] Use children of parents from other contexts\n- [#45] Add dbCacheFlag to control db caching of getCollection, default to false\n- [#49] Allow comma-delimited list of TV names as includeTVList or processTVList\n\ngetResources 1.3.1-pl (July 14, 2011)\n====================================\n- [#43] Allow 0 as idx property\n- [#9] Fix tvFilters grouping\n- [#46] Fix criteria issue with &resources property\n\ngetResources 1.3.0-pl (March 28, 2011)\n====================================\n- [#33] sortbyTVType: Allow numeric and datetime TV sorting via SQL CAST()\n- [#24] Fix typos in list property options\n- [#4] Support multiple sortby fields via JSON object\n- Use get() instead to toArray() if includeContent is false\n- [#22] Add &toSeparatePlaceholders property for splitting output\n\ngetResources 1.2.2-pl (October 18, 2010)\n====================================\n- [#19] Fix sortbyTV returning duplicate rows\n\ngetResources 1.2.1-pl (October 11, 2010)\n====================================\n- Remove inadvertent call to modX::setLogTarget(''ECHO'')\n\ngetResources 1.2.0-pl (September 25, 2010)\n====================================\n- Fix error when &parents is not set\n- Allow empty &sortby\n- Add ability to sort by a single Template Variable value (or default value)\n\ngetResources 1.1.0-pl (July 30, 2010)\n====================================\n- Added &toPlaceholder property for assigning results to a placeholder\n- Added &resources property for including/excluding specific resources\n- Added &showDeleted property\n- Allow multiple contexts to be passed into &context\n- Added &showUnpublish property\n- Added getresources.core_path reference for easier development\n- [#ADDON-135] Make output separator configurable via outputSeparator property\n- Add where property to allow ad hoc criteria in JSON format\n\ngetResources 1.0.0-ga (December 29, 2009)\n====================================\n- [#ADDON-81] Allow empty tvPrefix property.\n- [#ADDON-89] Allow parents property to have a value of 0.\n- Changed default value of sortbyAlias to empty string and added sortbyEscaped property with default of 0.\n- Added changelog, license, and readme.\n";s:9:"signature";s:21:"getresources-1.6.1-pl";s:13:"initialConfig";s:15:"[object Object]";s:4:"text";s:20:"Продолжить";s:2:"id";s:19:"package-install-btn";s:6:"hidden";s:5:"false";s:7:"handler";s:1749:"function (va){\n        var r;\n        var g = Ext.getCmp(''modx-package-grid'');\n        if (!g) return false;\n        \n        if (va.signature != undefined && va.signature != '''') {\n            r = {signature: va.signature};\n        } else {\n            r = g.menu.record.data ? g.menu.record.data : g.menu.record;\n        }\n		var topic = ''/workspace/package/install/''+r.signature+''/'';\n        g.loadConsole(Ext.getBody(),topic);\n\n		va = va || {};\n        Ext.apply(va,{\n            action: ''workspace/packages/install''\n            ,signature: r.signature\n            ,register: ''mgr''\n            ,topic: topic\n        });\n\n        MODx.Ajax.request({\n            url: MODx.config.connector_url\n            ,params: va\n            ,listeners: {\n                ''success'': {fn:function() {\n                    var bc = Ext.getCmp(''packages-breadcrumbs'');\n                    var trail= bc.data.trail;\n                    trail.pop();\n\n                    if (trail.length > 1) {\n                        last = trail[trail.length - 1];\n\n                        if (last != undefined && last.rec != undefined) {\n                            bc.data.trail.pop();\n                            bc.data.trail.shift();\n                            bc.updateDetail(bc.data);\n\n                            var grid = Ext.getCmp(''modx-package-grid'');\n                            grid.install(last.rec);\n                            return;\n                        }\n                    }\n\n                    this.activate();\n					Ext.getCmp(''modx-package-grid'').refresh();\n                },scope:this}\n                ,''failure'': {fn:function() {\n                    this.activate();\n                },scope:this}\n            }\n        });\n\n        return true;\n	}";s:8:"disabled";s:5:"false";s:5:"scope";s:15:"[object Object]";s:8:"minWidth";s:2:"75";s:10:"removeMode";s:9:"container";s:10:"hideParent";s:4:"true";s:6:"events";s:15:"[object Object]";s:7:"ownerCt";s:15:"[object Object]";s:9:"container";s:15:"[object Object]";s:8:"rendered";s:4:"true";s:8:"template";s:15:"[object Object]";s:5:"btnEl";s:15:"[object Object]";s:4:"mons";s:15:"[object Object]";s:2:"el";s:15:"[object Object]";s:4:"icon";s:0:"";s:7:"iconCls";s:0:"";s:8:"boxReady";s:4:"true";s:8:"lastSize";s:15:"[object Object]";s:11:"useSetClass";s:4:"true";s:6:"oldCls";s:12:"x-btn-noicon";s:3:"doc";s:15:"[object Object]";s:19:"monitoringMouseOver";s:4:"true";s:6:"action";s:26:"workspace/packages/install";s:8:"register";s:3:"mgr";s:5:"topic";s:49:"/workspace/package/install/getresources-1.6.1-pl/";s:14:"package_action";i:0;}', 'getResources', 'a:38:{s:2:"id";s:24:"52c184b462cf240b35006e31";s:7:"package";s:24:"4d556c3db2b083396d000abe";s:12:"display_name";s:21:"getresources-1.6.1-pl";s:4:"name";s:12:"getResources";s:7:"version";s:5:"1.6.1";s:13:"version_major";s:1:"1";s:13:"version_minor";s:1:"6";s:13:"version_patch";s:1:"1";s:7:"release";s:2:"pl";s:8:"vrelease";s:2:"pl";s:14:"vrelease_index";s:1:"0";s:6:"author";s:8:"opengeek";s:11:"description";s:157:"<p>This patch release fixes several bugs, including the dumping of properties to array if the output of a tpl Chunk is empty.</p><p></p><p></p><p></p><p></p>";s:12:"instructions";s:37:"<p>Install via Package Management</p>";s:9:"changelog";s:1742:"<p></p><p>getResources 1.6.1-pl (December 30, 2013)</p><ul><li>Allow tvFilter values to contain filter operators</li><li><li>Allow 0-based idx</li><li>Pass scriptProperties to wrapperTpl</li><li>Only dump properties for invalid tpl when debug enabled</li></li></ul><p>getResources 1.6.0-pl (February 19, 2013)</p><p></p><ul><li>Add tplWrapper for specifying a wrapper template</li></ul><p></p><p>getResources 1.5.1-pl (August 23, 2012)</p><p></p><ul><li>Add tplOperator property to default properties</li><li>&#91;#73&#93; Add between tplOperator to conditionalTpls</li></ul><p></p><p>getResources 1.5.0-pl (June 15, 2012)</p><p></p><ul><li>&#91;#58&#93; Add tplCondition/conditionalTpls support</li><li>&#91;#67&#93; Add odd property</li><li>&#91;#60&#93; Allow custom delimiters for tvFilters</li><li>&#91;#63&#93; Give tplFirst/tplLast precedence over tpl_X/tpl_nX</li><li>Automatically prepare TV values for media-source dependent TVs</li></ul><p></p><p></p><p>getResources 1.4.2-pl (December 9, 2011)</p><p></p><ul><li>&#91;#25&#93; Add new operators to tvFilters</li><li>&#91;#37&#93; Consider default values with tvFilters</li><li>&#91;#57&#93; Fix tpl overrides and improve order</li></ul><p></p><p></p><p>getResources 1.4.1-pl (December 8, 2011)</p><p></p><ul><li>&#91;#57&#93; Add support for factor-based tpls</li><li>&#91;#54&#93;, &#91;#55&#93; Fix processTVList feature</li></ul><p></p><p></p><p>getResources 1.4.0-pl (September 21, 2011)</p><p></p><ul><li>&#91;#50&#93; Use children of parents from other contexts</li><li>&#91;#45&#93; Add dbCacheFlag to control db caching of getCollection, default to false</li><li>&#91;#49&#93; Allow comma-delimited list of TV names as includeTVList or processTVList</li></ul><p></p><p></p>";s:9:"createdon";s:24:"2013-12-30T14:35:32+0000";s:9:"createdby";s:8:"opengeek";s:8:"editedon";s:24:"2016-06-08T10:30:25+0000";s:10:"releasedon";s:24:"2013-12-30T14:35:32+0000";s:9:"downloads";s:6:"204846";s:8:"approved";s:4:"true";s:7:"audited";s:4:"true";s:8:"featured";s:4:"true";s:10:"deprecated";s:5:"false";s:7:"license";s:5:"GPLv2";s:7:"smf_url";s:0:"";s:10:"repository";s:24:"4d4c3fa6b2b0830da9000001";s:8:"supports";s:1:"2";s:8:"location";s:60:"http://modx.com/extras/download/?id=52c184b562cf240b35006e33";s:9:"signature";s:21:"getresources-1.6.1-pl";s:11:"supports_db";s:12:"mysql,sqlsrv";s:16:"minimum_supports";s:1:"2";s:9:"breaks_at";s:8:"10000000";s:10:"screenshot";s:0:"";s:4:"file";a:7:{s:2:"id";s:24:"52c184b562cf240b35006e33";s:7:"version";s:24:"52c184b462cf240b35006e31";s:8:"filename";s:35:"getresources-1.6.1-pl.transport.zip";s:9:"downloads";s:5:"78544";s:6:"lastip";s:12:"185.98.7.102";s:9:"transport";s:4:"true";s:8:"location";s:60:"http://modx.com/extras/download/?id=52c184b562cf240b35006e33";}s:17:"package-signature";s:21:"getresources-1.6.1-pl";s:10:"categories";s:32:"blogging,content,navigation,news";s:4:"tags";s:57:"blog,blogging,resources,getr,getresource,resource,listing";}', 1, 6, 1, 'pl', 0);
INSERT INTO `modx_transport_packages` (`signature`, `created`, `updated`, `installed`, `state`, `workspace`, `provider`, `disabled`, `source`, `manifest`, `attributes`, `package_name`, `metadata`, `version_major`, `version_minor`, `version_patch`, `release`, `release_index`) VALUES
('tagger-1.7.0-pl', '2016-06-08 01:35:58', '2016-06-08 10:54:19', '2016-06-08 13:54:19', 0, 1, 1, 0, 'tagger-1.7.0-pl.transport.zip', NULL, 'a:34:{s:7:"license";s:15218:"GNU GENERAL PUBLIC LICENSE\n   Version 2, June 1991\n--------------------------\n\nCopyright (C) 1989, 1991 Free Software Foundation, Inc.\n59 Temple Place, Suite 330, Boston, MA  02111-1307  USA\n\nEveryone is permitted to copy and distribute verbatim copies\nof this license document, but changing it is not allowed.\n\nPreamble\n--------\n\n  The licenses for most software are designed to take away your\nfreedom to share and change it.  By contrast, the GNU General Public\nLicense is intended to guarantee your freedom to share and change free\nsoftware--to make sure the software is free for all its users.  This\nGeneral Public License applies to most of the Free Software\nFoundation''s software and to any other program whose authors commit to\nusing it.  (Some other Free Software Foundation software is covered by\nthe GNU Library General Public License instead.)  You can apply it to\nyour programs, too.\n\n  When we speak of free software, we are referring to freedom, not\nprice.  Our General Public Licenses are designed to make sure that you\nhave the freedom to distribute copies of free software (and charge for\nthis service if you wish), that you receive source code or can get it\nif you want it, that you can change the software or use pieces of it\nin new free programs; and that you know you can do these things.\n\n  To protect your rights, we need to make restrictions that forbid\nanyone to deny you these rights or to ask you to surrender the rights.\nThese restrictions translate to certain responsibilities for you if you\ndistribute copies of the software, or if you modify it.\n\n  For example, if you distribute copies of such a program, whether\ngratis or for a fee, you must give the recipients all the rights that\nyou have.  You must make sure that they, too, receive or can get the\nsource code.  And you must show them these terms so they know their\nrights.\n\n  We protect your rights with two steps: (1) copyright the software, and\n(2) offer you this license which gives you legal permission to copy,\ndistribute and/or modify the software.\n\n  Also, for each author''s protection and ours, we want to make certain\nthat everyone understands that there is no warranty for this free\nsoftware.  If the software is modified by someone else and passed on, we\nwant its recipients to know that what they have is not the original, so\nthat any problems introduced by others will not reflect on the original\nauthors'' reputations.\n\n  Finally, any free program is threatened constantly by software\npatents.  We wish to avoid the danger that redistributors of a free\nprogram will individually obtain patent licenses, in effect making the\nprogram proprietary.  To prevent this, we have made it clear that any\npatent must be licensed for everyone''s free use or not licensed at all.\n\n  The precise terms and conditions for copying, distribution and\nmodification follow.\n\n\nGNU GENERAL PUBLIC LICENSE\nTERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION\n---------------------------------------------------------------\n\n  0. This License applies to any program or other work which contains\na notice placed by the copyright holder saying it may be distributed\nunder the terms of this General Public License.  The "Program", below,\nrefers to any such program or work, and a "work based on the Program"\nmeans either the Program or any derivative work under copyright law:\nthat is to say, a work containing the Program or a portion of it,\neither verbatim or with modifications and/or translated into another\nlanguage.  (Hereinafter, translation is included without limitation in\nthe term "modification".)  Each licensee is addressed as "you".\n\nActivities other than copying, distribution and modification are not\ncovered by this License; they are outside its scope.  The act of\nrunning the Program is not restricted, and the output from the Program\nis covered only if its contents constitute a work based on the\nProgram (independent of having been made by running the Program).\nWhether that is true depends on what the Program does.\n\n  1. You may copy and distribute verbatim copies of the Program''s\nsource code as you receive it, in any medium, provided that you\nconspicuously and appropriately publish on each copy an appropriate\ncopyright notice and disclaimer of warranty; keep intact all the\nnotices that refer to this License and to the absence of any warranty;\nand give any other recipients of the Program a copy of this License\nalong with the Program.\n\nYou may charge a fee for the physical act of transferring a copy, and\nyou may at your option offer warranty protection in exchange for a fee.\n\n  2. You may modify your copy or copies of the Program or any portion\nof it, thus forming a work based on the Program, and copy and\ndistribute such modifications or work under the terms of Section 1\nabove, provided that you also meet all of these conditions:\n\n    a) You must cause the modified files to carry prominent notices\n    stating that you changed the files and the date of any change.\n\n    b) You must cause any work that you distribute or publish, that in\n    whole or in part contains or is derived from the Program or any\n    part thereof, to be licensed as a whole at no charge to all third\n    parties under the terms of this License.\n\n    c) If the modified program normally reads commands interactively\n    when run, you must cause it, when started running for such\n    interactive use in the most ordinary way, to print or display an\n    announcement including an appropriate copyright notice and a\n    notice that there is no warranty (or else, saying that you provide\n    a warranty) and that users may redistribute the program under\n    these conditions, and telling the user how to view a copy of this\n    License.  (Exception: if the Program itself is interactive but\n    does not normally print such an announcement, your work based on\n    the Program is not required to print an announcement.)\n\nThese requirements apply to the modified work as a whole.  If\nidentifiable sections of that work are not derived from the Program,\nand can be reasonably considered independent and separate works in\nthemselves, then this License, and its terms, do not apply to those\nsections when you distribute them as separate works.  But when you\ndistribute the same sections as part of a whole which is a work based\non the Program, the distribution of the whole must be on the terms of\nthis License, whose permissions for other licensees extend to the\nentire whole, and thus to each and every part regardless of who wrote it.\n\nThus, it is not the intent of this section to claim rights or contest\nyour rights to work written entirely by you; rather, the intent is to\nexercise the right to control the distribution of derivative or\ncollective works based on the Program.\n\nIn addition, mere aggregation of another work not based on the Program\nwith the Program (or with a work based on the Program) on a volume of\na storage or distribution medium does not bring the other work under\nthe scope of this License.\n\n  3. You may copy and distribute the Program (or a work based on it,\nunder Section 2) in object code or executable form under the terms of\nSections 1 and 2 above provided that you also do one of the following:\n\n    a) Accompany it with the complete corresponding machine-readable\n    source code, which must be distributed under the terms of Sections\n    1 and 2 above on a medium customarily used for software interchange; or,\n\n    b) Accompany it with a written offer, valid for at least three\n    years, to give any third party, for a charge no more than your\n    cost of physically performing source distribution, a complete\n    machine-readable copy of the corresponding source code, to be\n    distributed under the terms of Sections 1 and 2 above on a medium\n    customarily used for software interchange; or,\n\n    c) Accompany it with the information you received as to the offer\n    to distribute corresponding source code.  (This alternative is\n    allowed only for noncommercial distribution and only if you\n    received the program in object code or executable form with such\n    an offer, in accord with Subsection b above.)\n\nThe source code for a work means the preferred form of the work for\nmaking modifications to it.  For an executable work, complete source\ncode means all the source code for all modules it contains, plus any\nassociated interface definition files, plus the scripts used to\ncontrol compilation and installation of the executable.  However, as a\nspecial exception, the source code distributed need not include\nanything that is normally distributed (in either source or binary\nform) with the major components (compiler, kernel, and so on) of the\noperating system on which the executable runs, unless that component\nitself accompanies the executable.\n\nIf distribution of executable or object code is made by offering\naccess to copy from a designated place, then offering equivalent\naccess to copy the source code from the same place counts as\ndistribution of the source code, even though third parties are not\ncompelled to copy the source along with the object code.\n\n  4. You may not copy, modify, sublicense, or distribute the Program\nexcept as expressly provided under this License.  Any attempt\notherwise to copy, modify, sublicense or distribute the Program is\nvoid, and will automatically terminate your rights under this License.\nHowever, parties who have received copies, or rights, from you under\nthis License will not have their licenses terminated so long as such\nparties remain in full compliance.\n\n  5. You are not required to accept this License, since you have not\nsigned it.  However, nothing else grants you permission to modify or\ndistribute the Program or its derivative works.  These actions are\nprohibited by law if you do not accept this License.  Therefore, by\nmodifying or distributing the Program (or any work based on the\nProgram), you indicate your acceptance of this License to do so, and\nall its terms and conditions for copying, distributing or modifying\nthe Program or works based on it.\n\n  6. Each time you redistribute the Program (or any work based on the\nProgram), the recipient automatically receives a license from the\noriginal licensor to copy, distribute or modify the Program subject to\nthese terms and conditions.  You may not impose any further\nrestrictions on the recipients'' exercise of the rights granted herein.\nYou are not responsible for enforcing compliance by third parties to\nthis License.\n\n  7. If, as a consequence of a court judgment or allegation of patent\ninfringement or for any other reason (not limited to patent issues),\nconditions are imposed on you (whether by court order, agreement or\notherwise) that contradict the conditions of this License, they do not\nexcuse you from the conditions of this License.  If you cannot\ndistribute so as to satisfy simultaneously your obligations under this\nLicense and any other pertinent obligations, then as a consequence you\nmay not distribute the Program at all.  For example, if a patent\nlicense would not permit royalty-free redistribution of the Program by\nall those who receive copies directly or indirectly through you, then\nthe only way you could satisfy both it and this License would be to\nrefrain entirely from distribution of the Program.\n\nIf any portion of this section is held invalid or unenforceable under\nany particular circumstance, the balance of the section is intended to\napply and the section as a whole is intended to apply in other\ncircumstances.\n\nIt is not the purpose of this section to induce you to infringe any\npatents or other property right claims or to contest validity of any\nsuch claims; this section has the sole purpose of protecting the\nintegrity of the free software distribution system, which is\nimplemented by public license practices.  Many people have made\ngenerous contributions to the wide range of software distributed\nthrough that system in reliance on consistent application of that\nsystem; it is up to the author/donor to decide if he or she is willing\nto distribute software through any other system and a licensee cannot\nimpose that choice.\n\nThis section is intended to make thoroughly clear what is believed to\nbe a consequence of the rest of this License.\n\n  8. If the distribution and/or use of the Program is restricted in\ncertain countries either by patents or by copyrighted interfaces, the\noriginal copyright holder who places the Program under this License\nmay add an explicit geographical distribution limitation excluding\nthose countries, so that distribution is permitted only in or among\ncountries not thus excluded.  In such case, this License incorporates\nthe limitation as if written in the body of this License.\n\n  9. The Free Software Foundation may publish revised and/or new versions\nof the General Public License from time to time.  Such new versions will\nbe similar in spirit to the present version, but may differ in detail to\naddress new problems or concerns.\n\nEach version is given a distinguishing version number.  If the Program\nspecifies a version number of this License which applies to it and "any\nlater version", you have the option of following the terms and conditions\neither of that version or of any later version published by the Free\nSoftware Foundation.  If the Program does not specify a version number of\nthis License, you may choose any version ever published by the Free Software\nFoundation.\n\n  10. If you wish to incorporate parts of the Program into other free\nprograms whose distribution conditions are different, write to the author\nto ask for permission.  For software which is copyrighted by the Free\nSoftware Foundation, write to the Free Software Foundation; we sometimes\nmake exceptions for this.  Our decision will be guided by the two goals\nof preserving the free status of all derivatives of our free software and\nof promoting the sharing and reuse of software generally.\n\nNO WARRANTY\n-----------\n\n  11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY\nFOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN\nOTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES\nPROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED\nOR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF\nMERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS\nTO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE\nPROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,\nREPAIR OR CORRECTION.\n\n  12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING\nWILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR\nREDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,\nINCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING\nOUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED\nTO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY\nYOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER\nPROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE\nPOSSIBILITY OF SUCH DAMAGES.\n\n---------------------------\nEND OF TERMS AND CONDITIONS";s:6:"readme";s:175:"---------------------------------------\nTagger\n---------------------------------------\nVersion: 1.7.0\nAuthor: John Peca <john@modx.com>\n---------------------------------------";s:9:"changelog";s:4541:"Changelog for Tagger.\n\nTagger 1.7.0\n==============\n- Match group by alias or name in TaggerGetResourcesWhere\n- Add TaggerGetRelatedWhere snippet\n- Class for each plugin event\n- Hide tag limit field for combo box input\n- Fix gateway for index page\n- Load values for Tagger fields under TVs properly\n- Add field option to TaggerGetResourcesWhere\n- New styles for Revolution 2.3\n- Added @INLINE support in TaggerGetTags tpls params\n- Added &weight parameter to TaggerGetTags\n- Fixed matchAll parameter in TaggerGetResourcesWhere\n\nTagger 1.6.0\n==============\n- Added friendlyURL option to TaggerGetTags\n- Added matchAll option to TaggerGetResourcesWhere\n\nTagger 1.5.1\n==============\n- Fixed compatibility with PHP 5.2\n\nTagger 1.5.0\n==============\n- Added option to translate Tagger groups\n- Added system settings for changing place''s labels\n- Added an option to place a group as a tab into TVs section\n- Added option &wrapIfEmpty\n\nTagger 1.4.0\n==============\n- Updated french translations\n- Fixed gateway to support all file extensions\n- Made combobox for tags 100% width\n- Put form labels to top\n- Added lexicon string for Tagger''s tab\n- Updated styles to look better in 2.3\n- Added tagField to GetResourcesWhere snippet\n- Added sort param to GetTags snippet\n- Added Group description\n- Changed labelSeparator\n- Fixed notice in GetTags snippet\n- Fixed logging messages from plugin\n- Updated GetResourcesWhere snippet and removed tag_key init\n\nTagger 1.3.1\n==============\n- Fixed autotag styles in Revolution 2.3\n- Fixed tag''s styles inside tag field in Revolution 2.3\n\nTagger 1.3.0\n==============\n- Fixed bug that removed all assigned tags on second resource''s save\n- Added alias field to groups\n- Added alias field to tags\n- Added option to limit number of selected tags\n- Added option to use LIKE in query in TaggerGetResourcesWhere snippet\n- Fixed tag count when using showDeleted = 0 or showUnpublished = 0\n- Added an option to hide input field when auto tag is showed\n- Added limit, offset, totalPh, tpl_N properties to TaggerGetTags snippet\n- Fixed gateway redirecting to URL ended with a comma\n\nTagger 1.2.0\n==============\n- Added merge selected Tags\n- Added remove selected Tags\n- Fixed IndexManagerController\n- Added options showDeleted and showUnpublished to snippet getTags\n- Fixed getResourcesWhere to not return null when no tags are provided\n\nTagger 1.1.0\n==============\n- Added toPlaceholder property to getTags snippet\n- Duplicate Tags also for children\n- Improved gateway\n- Added handler for onResourceDuplicate event, that copies Tags (only for the Resource, not for children)\n- Added contexts property to TaggerGetTags snippet\n\nTagger 1.0.2\n==============\n- Replaced calling shorthand PHP array [] with default array()\n\nTagger 1.0.1\n==============\n- Fixed default value for &target in TaggerGetTags\n- Added descriptions to add/update group window\n- Fixed &group property in TaggerGetTags snippet\n\nTagger 1.0.0\n==============\n- Added ability to show assigned resources for selected tag from Tag grid\n- Added system settings to show/hide header of group places\n- Added option to select place where to render Tagger Group\n- Added import button to Tagger Group grid\n- Added drag & drop groups reorder\n- Added position to Tagger group\n\nTagger 0.4.1\n==============\n- Fixed group add dialog\n\nTagger 0.4.0\n==============\n- Added snippets properties\n- Translated system settings\n- Improved Snippets documentation\n- Added autotag option for groups that have field type set to Tag field\n- Fixed updating tags from manager\n- Added PHP documentation to xPDO classes\n- Added an option to show only used tags in TaggerGetTags\n- Added Allow type option to groups\n- Added trigger button to the tagger field\n\nTagger 0.3.0\n==============\n- Added url placeholder into TaggerGetTags row tpl\n- Added gateway for handling friendly URL with tags\n- Added snippet for creating WHERE query that can be used in getResources\n- Removed snippet for listing resources by tags\n\nTagger 0.2.0\n==============\n- Fixed removing tags after quick save\n- Fixed removing old tag - resource relations\n- Fixed removing unused tags\n- Added snippet for listing resources by tags\n- Added snippet for listing tags\n\nTagger 0.1.0\n==============\n- Updated schema - added indexes, removed ID from TaggerTagResource\n- Speed improvements in retrieving tags\n- Added groups options: field_type, allow_new, remove_unused and allow_blank\n- Added combobox for tags\n- Added tag field\n- Added sorting option to tag and group grids\n- Tag management\n- Group management\n- Initial release.\n";s:9:"signature";s:15:"tagger-1.7.0-pl";s:13:"initialConfig";s:15:"[object Object]";s:4:"text";s:20:"Продолжить";s:2:"id";s:19:"package-install-btn";s:6:"hidden";s:5:"false";s:7:"handler";s:1749:"function (va){\n        var r;\n        var g = Ext.getCmp(''modx-package-grid'');\n        if (!g) return false;\n        \n        if (va.signature != undefined && va.signature != '''') {\n            r = {signature: va.signature};\n        } else {\n            r = g.menu.record.data ? g.menu.record.data : g.menu.record;\n        }\n		var topic = ''/workspace/package/install/''+r.signature+''/'';\n        g.loadConsole(Ext.getBody(),topic);\n\n		va = va || {};\n        Ext.apply(va,{\n            action: ''workspace/packages/install''\n            ,signature: r.signature\n            ,register: ''mgr''\n            ,topic: topic\n        });\n\n        MODx.Ajax.request({\n            url: MODx.config.connector_url\n            ,params: va\n            ,listeners: {\n                ''success'': {fn:function() {\n                    var bc = Ext.getCmp(''packages-breadcrumbs'');\n                    var trail= bc.data.trail;\n                    trail.pop();\n\n                    if (trail.length > 1) {\n                        last = trail[trail.length - 1];\n\n                        if (last != undefined && last.rec != undefined) {\n                            bc.data.trail.pop();\n                            bc.data.trail.shift();\n                            bc.updateDetail(bc.data);\n\n                            var grid = Ext.getCmp(''modx-package-grid'');\n                            grid.install(last.rec);\n                            return;\n                        }\n                    }\n\n                    this.activate();\n					Ext.getCmp(''modx-package-grid'').refresh();\n                },scope:this}\n                ,''failure'': {fn:function() {\n                    this.activate();\n                },scope:this}\n            }\n        });\n\n        return true;\n	}";s:8:"disabled";s:5:"false";s:5:"scope";s:15:"[object Object]";s:8:"minWidth";s:2:"75";s:10:"removeMode";s:9:"container";s:10:"hideParent";s:4:"true";s:6:"events";s:15:"[object Object]";s:7:"ownerCt";s:15:"[object Object]";s:9:"container";s:15:"[object Object]";s:8:"rendered";s:4:"true";s:8:"template";s:15:"[object Object]";s:5:"btnEl";s:15:"[object Object]";s:4:"mons";s:15:"[object Object]";s:2:"el";s:15:"[object Object]";s:4:"icon";s:0:"";s:7:"iconCls";s:0:"";s:8:"boxReady";s:4:"true";s:8:"lastSize";s:15:"[object Object]";s:11:"useSetClass";s:4:"true";s:6:"oldCls";s:12:"x-btn-noicon";s:3:"doc";s:15:"[object Object]";s:19:"monitoringMouseOver";s:4:"true";s:6:"action";s:26:"workspace/packages/install";s:8:"register";s:3:"mgr";s:5:"topic";s:43:"/workspace/package/install/tagger-1.7.0-pl/";s:14:"package_action";i:0;}', 'Tagger', 'a:38:{s:2:"id";s:24:"5512e476dc532f210c01280d";s:7:"package";s:24:"533de2d262cf2468ab018351";s:12:"display_name";s:15:"tagger-1.7.0-pl";s:4:"name";s:6:"Tagger";s:7:"version";s:5:"1.7.0";s:13:"version_major";s:1:"1";s:13:"version_minor";s:1:"7";s:13:"version_patch";s:1:"0";s:7:"release";s:2:"pl";s:8:"vrelease";s:2:"pl";s:14:"vrelease_index";s:0:"";s:6:"author";s:8:"theboxer";s:11:"description";s:502:"<p></p><p></p><p>A robust and performant tag management system. Summary of the many, many features:</p><p></p><ul><li>Tested with up to a million tags</li><li>Paginated drop-down and type-ahead for easy tag input</li><li>Combo-box or tag-field input types</li><li>Optionally remove unused tags from the database automatically</li><li>Optionally restrict tag creation to the CMP, versus on input</li><li>Optionally use Auto-Tag cloud for input</li><li>Alias for each group</li></ul><p></p><p></p><p></p>";s:12:"instructions";s:114:"<p>Follow the <a href="http://rtfm.modx.com/extras/revo/tagger" title="" target="">official documentation</a>.</p>";s:9:"changelog";s:5427:"<p><b>Tagger 1.7.0</b></p><p><ul><li>Match group by alias or name in TaggerGetResourcesWhere</li><li>Add TaggerGetRelatedWhere snippet</li><li>Class for each plugin event</li><li>Hide tag limit field for combo box input</li><li>Fix gateway for index page</li><li>Load values for Tagger fields under TVs properly</li><li>Add field option to TaggerGetResourcesWhere</li><li>New styles for Revolution 2.3</li><li>Added @INLINE support in TaggerGetTags tpls params</li><li>Added &amp;weight parameter to TaggerGetTags</li><li>Fixed matchAll parameter in TaggerGetResourcesWhere</li></ul></p><p></p><p><b>Tagger 1.6.0</b></p><p></p><ul><li>Added friendlyURL option to TaggerGetTags</li><li>Added matchAll option to TaggerGetResourcesWhere</li></ul><p></p><p><b>Tagger 1.5.1</b></p><p></p><ul><li>Fixed compatibility with PHP 5.2</li></ul><p></p><p><b>Tagger 1.5.0</b></p><p></p><ul><li>Added option to translate Tagger groups</li><li>Added system settings for changing place''s labels</li><li>Added an option to place a group as a tab into TVs section</li><li>Added option &amp;wrapIfEmpty</li></ul><p></p><p><b>Tagger 1.4.0</b></p><p></p><ul><li>Updated french translations</li><li>Fixed gateway to support all file extensions</li><li>Made combobox for tags 100% width</li><li>Put form labels to top</li><li>Added lexicon string for Tagger''s tab</li><li>Updated styles to look better in 2.3</li><li>Added tagField to GetResourcesWhere snippet</li><li>Added sort param to GetTags snippet</li><li>Added Group description</li><li>Changed labelSeparator</li><li>Fixed notice in GetTags snippet</li><li>Fixed logging messages from plugin</li><li>Updated GetResourcesWhere snippet and removed tag_key init</li></ul><p></p><p><b>Tagger 1.3.1</b></p><p></p><ul><li>Fixed autotag styles in Revolution 2.3</li><li>Fixed tag''s styles inside tag field in Revolution 2.3</li></ul><p></p><p><b>Tagger 1.3.0</b></p><p></p><ul><li>Fixed bug that removed all assigned tags on second resource''s save</li><li>Added alias field to groups</li><li>Added alias field to tags</li><li>Added option to limit number of selected tags</li><li>Added option to use LIKE in query in TaggerGetResourcesWhere snippet</li><li>Fixed tag count when using showDeleted = 0 or showUnpublished = 0</li><li>Added an option to hide input field when auto tag is showed</li><li>Added limit, offset, totalPh, tpl_N properties to TaggerGetTags snippet</li><li>Fixed gateway redirecting to URL ended with a comma</li></ul><p></p><p><b>Tagger 1.2.0</b></p><p></p><ul><li>Added merge selected Tags</li><li>Added remove selected Tags</li><li>Fixed IndexManagerController</li><li>Added options showDeleted and showUnpublished to snippet getTags</li><li>Fixed getResourcesWhere to not return null when no tags are provided</li></ul><p></p><p><b>Tagger 1.1.0</b></p><p></p><ul><li>Added toPlaceholder property to getTags snippet</li><li>Duplicate Tags also for children</li><li>Improved gateway</li><li>Added handler for onResourceDuplicate event, that copies Tags (only for the Resource, not for children)</li><li>Added contexts property to TaggerGetTags snippet</li></ul><p></p><p><b>Tagger 1.0.2</b></p><p></p><ul><li>Replaced calling shorthand PHP array &#91;&#93; with default array()</li></ul><p></p><p><b>Tagger 1.0.1</b></p><p></p><ul><li>Fixed default value for &amp;target in TaggerGetTags</li><li>Added descriptions to add/update group window</li><li>Fixed &amp;group property in TaggerGetTags snippet</li></ul><p></p><p><b>Tagger 1.0.0</b></p><p></p><ul><li>Added ability to show assigned resources for selected tag from Tag grid</li><li>Added system settings to show/hide header of group places</li><li>Added option to select place where to render Tagger Group</li><li>Added import button to Tagger Group grid</li><li>Added drag &amp; drop groups reorder</li><li>Added position to Tagger group</li></ul><p></p><p><b>Tagger 0.4.1</b></p><p></p><ul><li>Fixed group add dialog</li></ul><p></p><p><b>Tagger 0.4.0</b></p><p></p><ul><li>Added snippets properties</li><li>Translated system settings</li><li>Improved Snippets documentation</li><li>Added autotag option for groups that have field type set to Tag field</li><li>Fixed updating tags from manager</li><li>Added PHP documentation to xPDO classes</li><li>Added an option to show only used tags in TaggerGetTags</li><li>Added Allow type option to groups</li><li>Added trigger button to the tagger field</li></ul><p></p><p><b>Tagger 0.3.0</b></p><p></p><ul><li>Added url placeholder into TaggerGetTags row tpl</li><li>Added gateway for handling friendly URL with tags</li><li>Added snippet for creating WHERE query that can be used in getResources</li><li>Removed snippet for listing resources by tags</li></ul><p></p><p><b>Tagger 0.2.0</b></p><p></p><ul><li>Fixed removing tags after quick save</li><li>Fixed removing old tag - resource relations</li><li>Fixed removing unused tags</li><li>Added snippet for listing resources by tags</li><li>Added snippet for listing tags</li></ul><p></p><p><b>Tagger 0.1.0</b></p><p></p><ul><li>Updated schema - added indexes, removed ID from TaggerTagResource</li><li>Speed improvements in retrieving tags</li><li>Added groups options: field_type, allow_new, remove_unused and allow_blank</li><li>Added combobox for tags</li><li>Added tag field</li><li>Added sorting option to tag and group grids</li><li>Tag management</li><li>Group management</li><li>Initial release.</li></ul><p></p>";s:9:"createdon";s:24:"2015-03-25T16:38:14+0000";s:9:"createdby";s:8:"theboxer";s:8:"editedon";s:24:"2016-06-08T08:11:03+0000";s:10:"releasedon";s:24:"2015-03-25T16:38:14+0000";s:9:"downloads";s:4:"6267";s:8:"approved";s:4:"true";s:7:"audited";s:4:"true";s:8:"featured";s:4:"true";s:10:"deprecated";s:5:"false";s:7:"license";s:5:"GPLv2";s:7:"smf_url";s:0:"";s:10:"repository";s:24:"4d4c3fa6b2b0830da9000001";s:8:"supports";s:3:"2.2";s:8:"location";s:60:"http://modx.com/extras/download/?id=5512e476dc532f210c01280f";s:9:"signature";s:15:"tagger-1.7.0-pl";s:11:"supports_db";s:5:"mysql";s:16:"minimum_supports";s:3:"2.2";s:9:"breaks_at";s:8:"10000000";s:10:"screenshot";s:111:"http://modx.s3.amazonaws.com/extras%2F533de2d262cf2468ab018351%2FScreen%20Shot%202014-04-04%20at%2000.32.48.png";s:4:"file";a:7:{s:2:"id";s:24:"5512e476dc532f210c01280f";s:7:"version";s:24:"5512e476dc532f210c01280d";s:8:"filename";s:29:"tagger-1.7.0-pl.transport.zip";s:9:"downloads";s:4:"3118";s:6:"lastip";s:13:"68.180.228.50";s:9:"transport";s:4:"true";s:8:"location";s:60:"http://modx.com/extras/download/?id=5512e476dc532f210c01280f";}s:17:"package-signature";s:15:"tagger-1.7.0-pl";s:10:"categories";s:16:"blogging,content";s:4:"tags";s:0:"";}', 1, 7, 0, 'pl', 0);
INSERT INTO `modx_transport_packages` (`signature`, `created`, `updated`, `installed`, `state`, `workspace`, `provider`, `disabled`, `source`, `manifest`, `attributes`, `package_name`, `metadata`, `version_major`, `version_minor`, `version_patch`, `release`, `release_index`) VALUES
('ace-1.6.5-pl', '2016-06-08 01:36:32', '2016-06-08 10:53:45', '2016-06-08 13:53:45', 0, 1, 1, 0, 'ace-1.6.5-pl.transport.zip', NULL, 'a:34:{s:7:"license";s:15218:"GNU GENERAL PUBLIC LICENSE\n   Version 2, June 1991\n--------------------------\n\nCopyright (C) 1989, 1991 Free Software Foundation, Inc.\n59 Temple Place, Suite 330, Boston, MA  02111-1307  USA\n\nEveryone is permitted to copy and distribute verbatim copies\nof this license document, but changing it is not allowed.\n\nPreamble\n--------\n\n  The licenses for most software are designed to take away your\nfreedom to share and change it.  By contrast, the GNU General Public\nLicense is intended to guarantee your freedom to share and change free\nsoftware--to make sure the software is free for all its users.  This\nGeneral Public License applies to most of the Free Software\nFoundation''s software and to any other program whose authors commit to\nusing it.  (Some other Free Software Foundation software is covered by\nthe GNU Library General Public License instead.)  You can apply it to\nyour programs, too.\n\n  When we speak of free software, we are referring to freedom, not\nprice.  Our General Public Licenses are designed to make sure that you\nhave the freedom to distribute copies of free software (and charge for\nthis service if you wish), that you receive source code or can get it\nif you want it, that you can change the software or use pieces of it\nin new free programs; and that you know you can do these things.\n\n  To protect your rights, we need to make restrictions that forbid\nanyone to deny you these rights or to ask you to surrender the rights.\nThese restrictions translate to certain responsibilities for you if you\ndistribute copies of the software, or if you modify it.\n\n  For example, if you distribute copies of such a program, whether\ngratis or for a fee, you must give the recipients all the rights that\nyou have.  You must make sure that they, too, receive or can get the\nsource code.  And you must show them these terms so they know their\nrights.\n\n  We protect your rights with two steps: (1) copyright the software, and\n(2) offer you this license which gives you legal permission to copy,\ndistribute and/or modify the software.\n\n  Also, for each author''s protection and ours, we want to make certain\nthat everyone understands that there is no warranty for this free\nsoftware.  If the software is modified by someone else and passed on, we\nwant its recipients to know that what they have is not the original, so\nthat any problems introduced by others will not reflect on the original\nauthors'' reputations.\n\n  Finally, any free program is threatened constantly by software\npatents.  We wish to avoid the danger that redistributors of a free\nprogram will individually obtain patent licenses, in effect making the\nprogram proprietary.  To prevent this, we have made it clear that any\npatent must be licensed for everyone''s free use or not licensed at all.\n\n  The precise terms and conditions for copying, distribution and\nmodification follow.\n\n\nGNU GENERAL PUBLIC LICENSE\nTERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION\n---------------------------------------------------------------\n\n  0. This License applies to any program or other work which contains\na notice placed by the copyright holder saying it may be distributed\nunder the terms of this General Public License.  The "Program", below,\nrefers to any such program or work, and a "work based on the Program"\nmeans either the Program or any derivative work under copyright law:\nthat is to say, a work containing the Program or a portion of it,\neither verbatim or with modifications and/or translated into another\nlanguage.  (Hereinafter, translation is included without limitation in\nthe term "modification".)  Each licensee is addressed as "you".\n\nActivities other than copying, distribution and modification are not\ncovered by this License; they are outside its scope.  The act of\nrunning the Program is not restricted, and the output from the Program\nis covered only if its contents constitute a work based on the\nProgram (independent of having been made by running the Program).\nWhether that is true depends on what the Program does.\n\n  1. You may copy and distribute verbatim copies of the Program''s\nsource code as you receive it, in any medium, provided that you\nconspicuously and appropriately publish on each copy an appropriate\ncopyright notice and disclaimer of warranty; keep intact all the\nnotices that refer to this License and to the absence of any warranty;\nand give any other recipients of the Program a copy of this License\nalong with the Program.\n\nYou may charge a fee for the physical act of transferring a copy, and\nyou may at your option offer warranty protection in exchange for a fee.\n\n  2. You may modify your copy or copies of the Program or any portion\nof it, thus forming a work based on the Program, and copy and\ndistribute such modifications or work under the terms of Section 1\nabove, provided that you also meet all of these conditions:\n\n    a) You must cause the modified files to carry prominent notices\n    stating that you changed the files and the date of any change.\n\n    b) You must cause any work that you distribute or publish, that in\n    whole or in part contains or is derived from the Program or any\n    part thereof, to be licensed as a whole at no charge to all third\n    parties under the terms of this License.\n\n    c) If the modified program normally reads commands interactively\n    when run, you must cause it, when started running for such\n    interactive use in the most ordinary way, to print or display an\n    announcement including an appropriate copyright notice and a\n    notice that there is no warranty (or else, saying that you provide\n    a warranty) and that users may redistribute the program under\n    these conditions, and telling the user how to view a copy of this\n    License.  (Exception: if the Program itself is interactive but\n    does not normally print such an announcement, your work based on\n    the Program is not required to print an announcement.)\n\nThese requirements apply to the modified work as a whole.  If\nidentifiable sections of that work are not derived from the Program,\nand can be reasonably considered independent and separate works in\nthemselves, then this License, and its terms, do not apply to those\nsections when you distribute them as separate works.  But when you\ndistribute the same sections as part of a whole which is a work based\non the Program, the distribution of the whole must be on the terms of\nthis License, whose permissions for other licensees extend to the\nentire whole, and thus to each and every part regardless of who wrote it.\n\nThus, it is not the intent of this section to claim rights or contest\nyour rights to work written entirely by you; rather, the intent is to\nexercise the right to control the distribution of derivative or\ncollective works based on the Program.\n\nIn addition, mere aggregation of another work not based on the Program\nwith the Program (or with a work based on the Program) on a volume of\na storage or distribution medium does not bring the other work under\nthe scope of this License.\n\n  3. You may copy and distribute the Program (or a work based on it,\nunder Section 2) in object code or executable form under the terms of\nSections 1 and 2 above provided that you also do one of the following:\n\n    a) Accompany it with the complete corresponding machine-readable\n    source code, which must be distributed under the terms of Sections\n    1 and 2 above on a medium customarily used for software interchange; or,\n\n    b) Accompany it with a written offer, valid for at least three\n    years, to give any third party, for a charge no more than your\n    cost of physically performing source distribution, a complete\n    machine-readable copy of the corresponding source code, to be\n    distributed under the terms of Sections 1 and 2 above on a medium\n    customarily used for software interchange; or,\n\n    c) Accompany it with the information you received as to the offer\n    to distribute corresponding source code.  (This alternative is\n    allowed only for noncommercial distribution and only if you\n    received the program in object code or executable form with such\n    an offer, in accord with Subsection b above.)\n\nThe source code for a work means the preferred form of the work for\nmaking modifications to it.  For an executable work, complete source\ncode means all the source code for all modules it contains, plus any\nassociated interface definition files, plus the scripts used to\ncontrol compilation and installation of the executable.  However, as a\nspecial exception, the source code distributed need not include\nanything that is normally distributed (in either source or binary\nform) with the major components (compiler, kernel, and so on) of the\noperating system on which the executable runs, unless that component\nitself accompanies the executable.\n\nIf distribution of executable or object code is made by offering\naccess to copy from a designated place, then offering equivalent\naccess to copy the source code from the same place counts as\ndistribution of the source code, even though third parties are not\ncompelled to copy the source along with the object code.\n\n  4. You may not copy, modify, sublicense, or distribute the Program\nexcept as expressly provided under this License.  Any attempt\notherwise to copy, modify, sublicense or distribute the Program is\nvoid, and will automatically terminate your rights under this License.\nHowever, parties who have received copies, or rights, from you under\nthis License will not have their licenses terminated so long as such\nparties remain in full compliance.\n\n  5. You are not required to accept this License, since you have not\nsigned it.  However, nothing else grants you permission to modify or\ndistribute the Program or its derivative works.  These actions are\nprohibited by law if you do not accept this License.  Therefore, by\nmodifying or distributing the Program (or any work based on the\nProgram), you indicate your acceptance of this License to do so, and\nall its terms and conditions for copying, distributing or modifying\nthe Program or works based on it.\n\n  6. Each time you redistribute the Program (or any work based on the\nProgram), the recipient automatically receives a license from the\noriginal licensor to copy, distribute or modify the Program subject to\nthese terms and conditions.  You may not impose any further\nrestrictions on the recipients'' exercise of the rights granted herein.\nYou are not responsible for enforcing compliance by third parties to\nthis License.\n\n  7. If, as a consequence of a court judgment or allegation of patent\ninfringement or for any other reason (not limited to patent issues),\nconditions are imposed on you (whether by court order, agreement or\notherwise) that contradict the conditions of this License, they do not\nexcuse you from the conditions of this License.  If you cannot\ndistribute so as to satisfy simultaneously your obligations under this\nLicense and any other pertinent obligations, then as a consequence you\nmay not distribute the Program at all.  For example, if a patent\nlicense would not permit royalty-free redistribution of the Program by\nall those who receive copies directly or indirectly through you, then\nthe only way you could satisfy both it and this License would be to\nrefrain entirely from distribution of the Program.\n\nIf any portion of this section is held invalid or unenforceable under\nany particular circumstance, the balance of the section is intended to\napply and the section as a whole is intended to apply in other\ncircumstances.\n\nIt is not the purpose of this section to induce you to infringe any\npatents or other property right claims or to contest validity of any\nsuch claims; this section has the sole purpose of protecting the\nintegrity of the free software distribution system, which is\nimplemented by public license practices.  Many people have made\ngenerous contributions to the wide range of software distributed\nthrough that system in reliance on consistent application of that\nsystem; it is up to the author/donor to decide if he or she is willing\nto distribute software through any other system and a licensee cannot\nimpose that choice.\n\nThis section is intended to make thoroughly clear what is believed to\nbe a consequence of the rest of this License.\n\n  8. If the distribution and/or use of the Program is restricted in\ncertain countries either by patents or by copyrighted interfaces, the\noriginal copyright holder who places the Program under this License\nmay add an explicit geographical distribution limitation excluding\nthose countries, so that distribution is permitted only in or among\ncountries not thus excluded.  In such case, this License incorporates\nthe limitation as if written in the body of this License.\n\n  9. The Free Software Foundation may publish revised and/or new versions\nof the General Public License from time to time.  Such new versions will\nbe similar in spirit to the present version, but may differ in detail to\naddress new problems or concerns.\n\nEach version is given a distinguishing version number.  If the Program\nspecifies a version number of this License which applies to it and "any\nlater version", you have the option of following the terms and conditions\neither of that version or of any later version published by the Free\nSoftware Foundation.  If the Program does not specify a version number of\nthis License, you may choose any version ever published by the Free Software\nFoundation.\n\n  10. If you wish to incorporate parts of the Program into other free\nprograms whose distribution conditions are different, write to the author\nto ask for permission.  For software which is copyrighted by the Free\nSoftware Foundation, write to the Free Software Foundation; we sometimes\nmake exceptions for this.  Our decision will be guided by the two goals\nof preserving the free status of all derivatives of our free software and\nof promoting the sharing and reuse of software generally.\n\nNO WARRANTY\n-----------\n\n  11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY\nFOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN\nOTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES\nPROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED\nOR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF\nMERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS\nTO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE\nPROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,\nREPAIR OR CORRECTION.\n\n  12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING\nWILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR\nREDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,\nINCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING\nOUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED\nTO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY\nYOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER\nPROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE\nPOSSIBILITY OF SUCH DAMAGES.\n\n---------------------------\nEND OF TERMS AND CONDITIONS";s:6:"readme";s:271:"--------------------\nExtra: Ace\n--------------------\nSince: March 29th, 2012\nAuthor: Danil Kostin <danya.postfactum@gmail.com>\nLicense: GNU GPLv2 (or later at your option)\n\nIntegrates Ace Code Editor into MODx Revolution.\n\nPress Ctrl+Alt+H to see all available shortcuts.";s:9:"changelog";s:3767:"Changelog for Ace integration into MODx Revolution.\n\nAce 1.6.5\n====================================\n- Added: "Twig" syntax for support of Twig in chunks.\n- Changed: Plugin is not static anymore.\n\nAce 1.6.4\n====================================\n- Fixed: Support of emmet in smarty mode. Again.\n\nAce 1.6.3\n====================================\n- Fixed: Support of emmet in smarty mode.\n\nAce 1.6.2\n====================================\n- Fixed: Editor mode handling.\n- Added: "Markdown" syntax for mime type "text/x-markdown".\n\nAce 1.6.1\n====================================\n- Fixed : Work with enabled system setting "compress_js".\n\nAce 1.6.0\n====================================\n- Added: "Smarty" syntax for support of Fenom in chunks.\n- Updated: Ace to version 1.2.0.\n\nAce 1.5.1\n====================================\n- Fixed: Bug with narrowing of the textarea.\n\nAce 1.5.0\n====================================\n- Changed: Assets are moved back to /assets/\n- Fixed: MODx tag completions (was completely broken)\n- Added: Editor height setting\n\nAce 1.4.3\n====================================\n- Added: MODx tag completions (Ctrl+Space)\n- Fixed: Issue caused AjaxManager (MODx Manager speed booster plugin) tree drag''n''drop bug\n\nAce 1.4.2\n====================================\n- Added: Undo coalescing\n- Changed: Mac fullscreen command is bound to Command+F12\n- Added: Drag delay (allow to start new selection inside current one) for Mac\n- Fixed: Use file extension of static chunks to detect code syntax\n\n\nAce 1.4.1\n====================================\n- Fixed: Tab handling\n- Fixed: Emmet shortcut listing by Ctr+Alt+H\n- Added: Expandable snippets support (see ace.snippets setting)\n- Added: Emmet wrap_with_abbreviation command (Alt+W)\n\nAce 1.4.0\n====================================\n- Added: Emmet (aka Zen Coding) support\n- Added: Terminal dark theme\n- Added: Hotkey table (Ctrl+Alt+H)\n- Fixed: Resource overview fatal error\n- Changed: Assets are moved to /manager/assets/components/\n\nAce 1.3.3\n====================================\n- Added: PHP live syntax check\n- Added: Chaos dark theme\n- Added: Setting show_invisibles\n\n\nAce 1.3.2\n====================================\n- Fixed: The bug while installing the Ace\n- Fixed: Broken word_wrap setting\n- Added: Tab settings (tab size, soft tab)\n- Added: Now completele compatible with AjaxManager extra\n\n\nAce 1.3.1\n====================================\n- Changed: Plugin content now is stored in static file\n\n\nAce 1.3.0\n====================================\n- Added: German translation\n- Added: MODx tags highlighting\n- Added: Ambiance and xcode themes\n- Added: less/scss syntax highlighting\n- Added: Fullwindow mode (Ctrl + F11)\n- Changed: Editor now ignores `wich_editor` setting. Set `use_editor` to false to use ACE for Resources.\n\n\nAce 1.2.1\n====================================\n- Changed: Assets are moved to manager folder\n- Added: Font size setting\n- Added: "GitHub" theme\n- Added: Support of html5 drag''n''drop (accepting of dropped text)\n- Added: XML / HTML tag autoclosing\n- Fixed: broken en lexicon and php 5.3 incompatibility\n\n\nAce 1.2.0\n====================================\n- Removed: Some unnecessary options\n- Changed: Editor options are moved to system settings\n- Fixed: Multiple little editor bugs\n- Added: Add missing "OnFileEditFormPrerender" event to MODx\n- Added: Multiline editing\n- Added: Advanced find/replace window\n\n\nAce 1.1.0\n====================================\n- Fixed: Fatal error on document create event\n- Fixed: Changing of properties has no effect\n- Added: File edition support\n- Added: MODx tree elements drag''n''drop support\n- Added: Auto-assigning which_element_editor to Ace\n\n\nAce 1.0.0\n====================================\n- Added: Plugin properties to adjust how Ace behaves\n- Initial commit";s:9:"signature";s:12:"ace-1.6.5-pl";s:13:"initialConfig";s:15:"[object Object]";s:4:"text";s:20:"Продолжить";s:2:"id";s:19:"package-install-btn";s:6:"hidden";s:5:"false";s:7:"handler";s:1749:"function (va){\n        var r;\n        var g = Ext.getCmp(''modx-package-grid'');\n        if (!g) return false;\n        \n        if (va.signature != undefined && va.signature != '''') {\n            r = {signature: va.signature};\n        } else {\n            r = g.menu.record.data ? g.menu.record.data : g.menu.record;\n        }\n		var topic = ''/workspace/package/install/''+r.signature+''/'';\n        g.loadConsole(Ext.getBody(),topic);\n\n		va = va || {};\n        Ext.apply(va,{\n            action: ''workspace/packages/install''\n            ,signature: r.signature\n            ,register: ''mgr''\n            ,topic: topic\n        });\n\n        MODx.Ajax.request({\n            url: MODx.config.connector_url\n            ,params: va\n            ,listeners: {\n                ''success'': {fn:function() {\n                    var bc = Ext.getCmp(''packages-breadcrumbs'');\n                    var trail= bc.data.trail;\n                    trail.pop();\n\n                    if (trail.length > 1) {\n                        last = trail[trail.length - 1];\n\n                        if (last != undefined && last.rec != undefined) {\n                            bc.data.trail.pop();\n                            bc.data.trail.shift();\n                            bc.updateDetail(bc.data);\n\n                            var grid = Ext.getCmp(''modx-package-grid'');\n                            grid.install(last.rec);\n                            return;\n                        }\n                    }\n\n                    this.activate();\n					Ext.getCmp(''modx-package-grid'').refresh();\n                },scope:this}\n                ,''failure'': {fn:function() {\n                    this.activate();\n                },scope:this}\n            }\n        });\n\n        return true;\n	}";s:8:"disabled";s:5:"false";s:5:"scope";s:15:"[object Object]";s:8:"minWidth";s:2:"75";s:10:"removeMode";s:9:"container";s:10:"hideParent";s:4:"true";s:6:"events";s:15:"[object Object]";s:7:"ownerCt";s:15:"[object Object]";s:9:"container";s:15:"[object Object]";s:8:"rendered";s:4:"true";s:8:"template";s:15:"[object Object]";s:5:"btnEl";s:15:"[object Object]";s:4:"mons";s:15:"[object Object]";s:2:"el";s:15:"[object Object]";s:4:"icon";s:0:"";s:7:"iconCls";s:0:"";s:8:"boxReady";s:4:"true";s:8:"lastSize";s:15:"[object Object]";s:11:"useSetClass";s:4:"true";s:6:"oldCls";s:12:"x-btn-noicon";s:3:"doc";s:15:"[object Object]";s:19:"monitoringMouseOver";s:4:"true";s:6:"action";s:26:"workspace/packages/install";s:8:"register";s:3:"mgr";s:5:"topic";s:40:"/workspace/package/install/ace-1.6.5-pl/";s:14:"package_action";i:0;}', 'Ace', 'a:38:{s:2:"id";s:24:"568f9f06dc532f593e002c59";s:7:"package";s:24:"4f6e2782f245544fe8000014";s:12:"display_name";s:12:"ace-1.6.5-pl";s:4:"name";s:3:"Ace";s:7:"version";s:5:"1.6.5";s:13:"version_major";s:1:"1";s:13:"version_minor";s:1:"6";s:13:"version_patch";s:1:"5";s:7:"release";s:2:"pl";s:8:"vrelease";s:2:"pl";s:14:"vrelease_index";s:0:"";s:6:"author";s:9:"bezumkin2";s:11:"description";s:376:"<p>New feature: modx tag code autocompletion! Press Ctrl+Space to get code suggestions with descriptions.</p><p>Works for snippets, chunks, system settings, tvs and resource fields, filters and properties.</p><p>Property sets, lexicon entries are not supported. Unfortunately, I have no idea how to retrieve chunk-specific placeholders, so there is no placeholder support.</p>";s:12:"instructions";s:353:"<p></p><p>Install via Package Management.</p><p>Set editor theme you wish in system settings (change namespace to "ace").</p><p>If you want to use this editor for resources, just set system option <i>use_editor</i> to <b>false</b>&nbsp;(global usage), or&nbsp;<i>richtext</i>&nbsp;setting of certain resource to <b>false</b> (specific usage).</p><p></p>";s:9:"changelog";s:4462:"<p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p>Ace 1.6.5</p><p>====================================</p><p>- Added: "Twig" syntax for support of Twig in chunks.</p><p>- Changed: Plugin is not static anymore.</p><p>Ace 1.6.4</p><p>====================================</p><p>- Fixed: Support of emmet in smarty mode. Again.</p><p>Ace 1.6.3</p><p>====================================</p><p>- Fixed: Support of emmet in smarty mode.</p><p></p><p>Ace 1.6.2</p><p>====================================</p><p>- Fixed: Editor mode handling.</p><p>- Added: "Markdown" syntax for mime type "text/x-markdown".</p><p></p><p>Ace 1.6.1</p><p>====================================</p><p>- Fixed : Work with enabled system setting "compress_js".</p><p></p><p>Ace 1.6.0</p><p>====================================</p><p>- Added: "Smarty" syntax for support of Fenom in chunks.</p><p>- Updated: Ace to version 1.2.0.</p><p></p><p>Ace 1.5.1</p><p>====================================</p><p>- Fixed: Bug with narrowing of the textarea.</p><p>Ace 1.5.0</p><p>====================================</p><p>- Changed: Assets are moved back to /assets/</p><p>- Fixed: MODx tag completions (was completely broken)</p><p>- Added: Editor height setting</p><p>Ace 1.4.3</p><p>====================================</p><p>- Added: MODx tag completions (Ctrl+Space)</p><p>- Fixed: Issue caused AjaxManager (MODx Manager speed booster plugin) tree drag''n''drop bug</p><p>Ace 1.4.2</p><p>====================================</p><p>- Added: Undo coalescing</p><p>- Changed: Mac fullscreen command is bound to Command+F12</p><p>- Added: Drag delay (allow to start new selection inside current one) for Mac</p><p>- Fixed: Use file extension of static chunks to detect code syntax</p><p>Ace 1.4.1</p><p>====================================</p><p>- Fixed: Tab handling</p><p>- Fixed: Emmet shortcut listing by Ctr+Alt+H</p><p>- Added: Expandable snippets support (see ace.snippets setting)</p><p>- Added: Emmet wrap_with_abbreviation command (Alt+W)</p><p></p><p>Ace 1.4.0</p><p>====================================</p><p>- Added: Emmet (aka Zen Coding) support</p><p>- Added: Terminal dark theme</p><p>- Added: Hotkey table (Ctrl+Alt+H)</p><p>- Fixed: Resource overview fatal error</p><p>- Changed: Assets are moved to /manager/assets/components/</p><p></p><p>Ace 1.3.3</p><p>====================================</p><p>- Added: PHP live syntax check</p><p>- Added: Chaos dark theme</p><p>- Added: Setting show_invisibles</p><p></p><p>Ace 1.3.2</p><p>====================================</p><p>- Fixed: The bug while installing the Ace</p><p>- Fixed: Broken word_wrap setting</p><p>- Added: Tab settings (tab size, soft tab)</p><p>- Added: Now completele compatible with AjaxManager extra</p><p>Ace 1.3.1</p><p>====================================</p><p>- Changed: Plugin content now is stored in static file</p><p></p><p>Ace 1.3.0</p><p>====================================</p><p>- Added: German translation</p><p>- Added: MODx tags highlighting</p><p>- Added: Ambiance and xcode themes</p><p>- Added: less/scss syntax highlighting</p><p>- Added: Fullwindow mode (Ctrl + F11)</p><p>- Changed: Editor now ignores `wich_editor` setting. Set `use_editor` to false to use ACE for Resources.</p><p></p><p>Ace 1.2.1</p><p>====================================</p><p>- Changed: Assets are moved to manager folder</p><p>- Added: Font size setting</p><p>- Added: "GitHub" theme</p><p>- Added: Support of html5 drag''n''drop (accepting of dropped text)</p><p>- Added: XML / HTML tag autoclosing</p><p>- Fixed: broken en lexicon and php 5.3 incompatibility</p><p></p><p>Ace 1.2.0</p><p>====================================</p><p>- Removed: Some unnecessary options</p><p>- Changed: Editor options are moved to system settings</p><p>- Fixed: Multiple little editor bugs</p><p>- Added: Add missing "OnFileEditFormPrerender" event to MODx</p><p>- Added: Multiline editing</p><p>- Added: Advanced find/replace window</p><p></p><p></p><p>Ace 1.1.0</p><p>====================================</p><p>- Fixed: Fatal error on document create event</p><p>- Fixed: Changing of properties has no effect</p><p>- Added: File edition support</p><p>- Added: MODx tree elements drag''n''drop support</p><p>- Added: Auto-assigning which_element_editor to Ace</p><p></p><p></p><p>Ace 1.0.0</p><p>====================================</p><p>- Added: Plugin properties to adjust how Ace behaves</p><p>- Initial commit</p><p></p><p></p>";s:9:"createdon";s:24:"2016-01-08T11:35:34+0000";s:9:"createdby";s:9:"bezumkin2";s:8:"editedon";s:24:"2016-06-08T10:27:43+0000";s:10:"releasedon";s:24:"2016-01-08T11:35:34+0000";s:9:"downloads";s:6:"117146";s:8:"approved";s:4:"true";s:7:"audited";s:5:"false";s:8:"featured";s:5:"false";s:10:"deprecated";s:5:"false";s:7:"license";s:5:"GPLv2";s:7:"smf_url";s:0:"";s:10:"repository";s:24:"4d4c3fa6b2b0830da9000001";s:8:"supports";s:3:"2.2";s:8:"location";s:60:"http://modx.com/extras/download/?id=568f9f07dc532f593e002c5b";s:9:"signature";s:12:"ace-1.6.5-pl";s:11:"supports_db";s:12:"mysql,sqlsrv";s:16:"minimum_supports";s:3:"2.2";s:9:"breaks_at";s:8:"10000000";s:10:"screenshot";s:68:"http://modx.s3.amazonaws.com/extras/4f6e2782f245544fe8000014/ace.png";s:4:"file";a:7:{s:2:"id";s:24:"568f9f07dc532f593e002c5b";s:7:"version";s:24:"568f9f06dc532f593e002c59";s:8:"filename";s:26:"ace-1.6.5-pl.transport.zip";s:9:"downloads";s:5:"21665";s:6:"lastip";s:12:"82.146.37.48";s:9:"transport";s:4:"true";s:8:"location";s:60:"http://modx.com/extras/download/?id=568f9f07dc532f593e002c5b";}s:17:"package-signature";s:12:"ace-1.6.5-pl";s:10:"categories";s:15:"richtexteditors";s:4:"tags";s:0:"";}', 1, 6, 5, 'pl', 0);
INSERT INTO `modx_transport_packages` (`signature`, `created`, `updated`, `installed`, `state`, `workspace`, `provider`, `disabled`, `source`, `manifest`, `attributes`, `package_name`, `metadata`, `version_major`, `version_minor`, `version_patch`, `release`, `release_index`) VALUES
('breadcrumbs-1.1.0-pl', '2016-06-08 01:36:54', '2016-06-08 10:53:52', '2016-06-08 13:53:52', 0, 1, 1, 0, 'breadcrumbs-1.1.0-pl.transport.zip', NULL, 'a:34:{s:7:"license";s:15218:"GNU GENERAL PUBLIC LICENSE\n   Version 2, June 1991\n--------------------------\n\nCopyright (C) 1989, 1991 Free Software Foundation, Inc.\n59 Temple Place, Suite 330, Boston, MA  02111-1307  USA\n\nEveryone is permitted to copy and distribute verbatim copies\nof this license document, but changing it is not allowed.\n\nPreamble\n--------\n\n  The licenses for most software are designed to take away your\nfreedom to share and change it.  By contrast, the GNU General Public\nLicense is intended to guarantee your freedom to share and change free\nsoftware--to make sure the software is free for all its users.  This\nGeneral Public License applies to most of the Free Software\nFoundation''s software and to any other program whose authors commit to\nusing it.  (Some other Free Software Foundation software is covered by\nthe GNU Library General Public License instead.)  You can apply it to\nyour programs, too.\n\n  When we speak of free software, we are referring to freedom, not\nprice.  Our General Public Licenses are designed to make sure that you\nhave the freedom to distribute copies of free software (and charge for\nthis service if you wish), that you receive source code or can get it\nif you want it, that you can change the software or use pieces of it\nin new free programs; and that you know you can do these things.\n\n  To protect your rights, we need to make restrictions that forbid\nanyone to deny you these rights or to ask you to surrender the rights.\nThese restrictions translate to certain responsibilities for you if you\ndistribute copies of the software, or if you modify it.\n\n  For example, if you distribute copies of such a program, whether\ngratis or for a fee, you must give the recipients all the rights that\nyou have.  You must make sure that they, too, receive or can get the\nsource code.  And you must show them these terms so they know their\nrights.\n\n  We protect your rights with two steps: (1) copyright the software, and\n(2) offer you this license which gives you legal permission to copy,\ndistribute and/or modify the software.\n\n  Also, for each author''s protection and ours, we want to make certain\nthat everyone understands that there is no warranty for this free\nsoftware.  If the software is modified by someone else and passed on, we\nwant its recipients to know that what they have is not the original, so\nthat any problems introduced by others will not reflect on the original\nauthors'' reputations.\n\n  Finally, any free program is threatened constantly by software\npatents.  We wish to avoid the danger that redistributors of a free\nprogram will individually obtain patent licenses, in effect making the\nprogram proprietary.  To prevent this, we have made it clear that any\npatent must be licensed for everyone''s free use or not licensed at all.\n\n  The precise terms and conditions for copying, distribution and\nmodification follow.\n\n\nGNU GENERAL PUBLIC LICENSE\nTERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION\n---------------------------------------------------------------\n\n  0. This License applies to any program or other work which contains\na notice placed by the copyright holder saying it may be distributed\nunder the terms of this General Public License.  The "Program", below,\nrefers to any such program or work, and a "work based on the Program"\nmeans either the Program or any derivative work under copyright law:\nthat is to say, a work containing the Program or a portion of it,\neither verbatim or with modifications and/or translated into another\nlanguage.  (Hereinafter, translation is included without limitation in\nthe term "modification".)  Each licensee is addressed as "you".\n\nActivities other than copying, distribution and modification are not\ncovered by this License; they are outside its scope.  The act of\nrunning the Program is not restricted, and the output from the Program\nis covered only if its contents constitute a work based on the\nProgram (independent of having been made by running the Program).\nWhether that is true depends on what the Program does.\n\n  1. You may copy and distribute verbatim copies of the Program''s\nsource code as you receive it, in any medium, provided that you\nconspicuously and appropriately publish on each copy an appropriate\ncopyright notice and disclaimer of warranty; keep intact all the\nnotices that refer to this License and to the absence of any warranty;\nand give any other recipients of the Program a copy of this License\nalong with the Program.\n\nYou may charge a fee for the physical act of transferring a copy, and\nyou may at your option offer warranty protection in exchange for a fee.\n\n  2. You may modify your copy or copies of the Program or any portion\nof it, thus forming a work based on the Program, and copy and\ndistribute such modifications or work under the terms of Section 1\nabove, provided that you also meet all of these conditions:\n\n    a) You must cause the modified files to carry prominent notices\n    stating that you changed the files and the date of any change.\n\n    b) You must cause any work that you distribute or publish, that in\n    whole or in part contains or is derived from the Program or any\n    part thereof, to be licensed as a whole at no charge to all third\n    parties under the terms of this License.\n\n    c) If the modified program normally reads commands interactively\n    when run, you must cause it, when started running for such\n    interactive use in the most ordinary way, to print or display an\n    announcement including an appropriate copyright notice and a\n    notice that there is no warranty (or else, saying that you provide\n    a warranty) and that users may redistribute the program under\n    these conditions, and telling the user how to view a copy of this\n    License.  (Exception: if the Program itself is interactive but\n    does not normally print such an announcement, your work based on\n    the Program is not required to print an announcement.)\n\nThese requirements apply to the modified work as a whole.  If\nidentifiable sections of that work are not derived from the Program,\nand can be reasonably considered independent and separate works in\nthemselves, then this License, and its terms, do not apply to those\nsections when you distribute them as separate works.  But when you\ndistribute the same sections as part of a whole which is a work based\non the Program, the distribution of the whole must be on the terms of\nthis License, whose permissions for other licensees extend to the\nentire whole, and thus to each and every part regardless of who wrote it.\n\nThus, it is not the intent of this section to claim rights or contest\nyour rights to work written entirely by you; rather, the intent is to\nexercise the right to control the distribution of derivative or\ncollective works based on the Program.\n\nIn addition, mere aggregation of another work not based on the Program\nwith the Program (or with a work based on the Program) on a volume of\na storage or distribution medium does not bring the other work under\nthe scope of this License.\n\n  3. You may copy and distribute the Program (or a work based on it,\nunder Section 2) in object code or executable form under the terms of\nSections 1 and 2 above provided that you also do one of the following:\n\n    a) Accompany it with the complete corresponding machine-readable\n    source code, which must be distributed under the terms of Sections\n    1 and 2 above on a medium customarily used for software interchange; or,\n\n    b) Accompany it with a written offer, valid for at least three\n    years, to give any third party, for a charge no more than your\n    cost of physically performing source distribution, a complete\n    machine-readable copy of the corresponding source code, to be\n    distributed under the terms of Sections 1 and 2 above on a medium\n    customarily used for software interchange; or,\n\n    c) Accompany it with the information you received as to the offer\n    to distribute corresponding source code.  (This alternative is\n    allowed only for noncommercial distribution and only if you\n    received the program in object code or executable form with such\n    an offer, in accord with Subsection b above.)\n\nThe source code for a work means the preferred form of the work for\nmaking modifications to it.  For an executable work, complete source\ncode means all the source code for all modules it contains, plus any\nassociated interface definition files, plus the scripts used to\ncontrol compilation and installation of the executable.  However, as a\nspecial exception, the source code distributed need not include\nanything that is normally distributed (in either source or binary\nform) with the major components (compiler, kernel, and so on) of the\noperating system on which the executable runs, unless that component\nitself accompanies the executable.\n\nIf distribution of executable or object code is made by offering\naccess to copy from a designated place, then offering equivalent\naccess to copy the source code from the same place counts as\ndistribution of the source code, even though third parties are not\ncompelled to copy the source along with the object code.\n\n  4. You may not copy, modify, sublicense, or distribute the Program\nexcept as expressly provided under this License.  Any attempt\notherwise to copy, modify, sublicense or distribute the Program is\nvoid, and will automatically terminate your rights under this License.\nHowever, parties who have received copies, or rights, from you under\nthis License will not have their licenses terminated so long as such\nparties remain in full compliance.\n\n  5. You are not required to accept this License, since you have not\nsigned it.  However, nothing else grants you permission to modify or\ndistribute the Program or its derivative works.  These actions are\nprohibited by law if you do not accept this License.  Therefore, by\nmodifying or distributing the Program (or any work based on the\nProgram), you indicate your acceptance of this License to do so, and\nall its terms and conditions for copying, distributing or modifying\nthe Program or works based on it.\n\n  6. Each time you redistribute the Program (or any work based on the\nProgram), the recipient automatically receives a license from the\noriginal licensor to copy, distribute or modify the Program subject to\nthese terms and conditions.  You may not impose any further\nrestrictions on the recipients'' exercise of the rights granted herein.\nYou are not responsible for enforcing compliance by third parties to\nthis License.\n\n  7. If, as a consequence of a court judgment or allegation of patent\ninfringement or for any other reason (not limited to patent issues),\nconditions are imposed on you (whether by court order, agreement or\notherwise) that contradict the conditions of this License, they do not\nexcuse you from the conditions of this License.  If you cannot\ndistribute so as to satisfy simultaneously your obligations under this\nLicense and any other pertinent obligations, then as a consequence you\nmay not distribute the Program at all.  For example, if a patent\nlicense would not permit royalty-free redistribution of the Program by\nall those who receive copies directly or indirectly through you, then\nthe only way you could satisfy both it and this License would be to\nrefrain entirely from distribution of the Program.\n\nIf any portion of this section is held invalid or unenforceable under\nany particular circumstance, the balance of the section is intended to\napply and the section as a whole is intended to apply in other\ncircumstances.\n\nIt is not the purpose of this section to induce you to infringe any\npatents or other property right claims or to contest validity of any\nsuch claims; this section has the sole purpose of protecting the\nintegrity of the free software distribution system, which is\nimplemented by public license practices.  Many people have made\ngenerous contributions to the wide range of software distributed\nthrough that system in reliance on consistent application of that\nsystem; it is up to the author/donor to decide if he or she is willing\nto distribute software through any other system and a licensee cannot\nimpose that choice.\n\nThis section is intended to make thoroughly clear what is believed to\nbe a consequence of the rest of this License.\n\n  8. If the distribution and/or use of the Program is restricted in\ncertain countries either by patents or by copyrighted interfaces, the\noriginal copyright holder who places the Program under this License\nmay add an explicit geographical distribution limitation excluding\nthose countries, so that distribution is permitted only in or among\ncountries not thus excluded.  In such case, this License incorporates\nthe limitation as if written in the body of this License.\n\n  9. The Free Software Foundation may publish revised and/or new versions\nof the General Public License from time to time.  Such new versions will\nbe similar in spirit to the present version, but may differ in detail to\naddress new problems or concerns.\n\nEach version is given a distinguishing version number.  If the Program\nspecifies a version number of this License which applies to it and "any\nlater version", you have the option of following the terms and conditions\neither of that version or of any later version published by the Free\nSoftware Foundation.  If the Program does not specify a version number of\nthis License, you may choose any version ever published by the Free Software\nFoundation.\n\n  10. If you wish to incorporate parts of the Program into other free\nprograms whose distribution conditions are different, write to the author\nto ask for permission.  For software which is copyrighted by the Free\nSoftware Foundation, write to the Free Software Foundation; we sometimes\nmake exceptions for this.  Our decision will be guided by the two goals\nof preserving the free status of all derivatives of our free software and\nof promoting the sharing and reuse of software generally.\n\nNO WARRANTY\n-----------\n\n  11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY\nFOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN\nOTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES\nPROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED\nOR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF\nMERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS\nTO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE\nPROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,\nREPAIR OR CORRECTION.\n\n  12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING\nWILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR\nREDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,\nINCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING\nOUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED\nTO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY\nYOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER\nPROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE\nPOSSIBILITY OF SUCH DAMAGES.\n\n---------------------------\nEND OF TERMS AND CONDITIONS";s:6:"readme";s:412:"--------------------\nSnippet: Breadcrumbs\n--------------------\nVersion: 1.0\nDate: 2008.10.08\nAuthor: jaredc@honeydewdesign.com\nEditor: Shaun McCormick <shaun@collabpad.com>\nHonorable mentions:\n- Bill Wilson\n- wendy@djamoer.net\n- grad\n\nThis snippet was designed to show the path through the various levels of site structure\nback to the root. It is NOT necessarily the path the user took to arrive at a given\npage.";s:9:"changelog";s:497:"Changelog file for breadcrumbs.\n\nBreadcrumbs 1.1-rc2 (August 31, 2010)\n====================================\n- Fix bug that skips the immediate children of site_start\n\nBreadcrumbs 1.1-rc1 (March 19, 2010)\n====================================\n- Updated version for Revo RC1\n- [#ADDON-84], [#ADDON-73] Fixed bug with showCrumbsAtHome and showCurrentCrumb\n- Consolidated settings into $scriptProperties, which cuts down snippet file code\n- Added initialize() function to handle default config settings";s:9:"signature";s:20:"breadcrumbs-1.1.0-pl";s:13:"initialConfig";s:15:"[object Object]";s:4:"text";s:20:"Продолжить";s:2:"id";s:19:"package-install-btn";s:6:"hidden";s:5:"false";s:7:"handler";s:1749:"function (va){\n        var r;\n        var g = Ext.getCmp(''modx-package-grid'');\n        if (!g) return false;\n        \n        if (va.signature != undefined && va.signature != '''') {\n            r = {signature: va.signature};\n        } else {\n            r = g.menu.record.data ? g.menu.record.data : g.menu.record;\n        }\n		var topic = ''/workspace/package/install/''+r.signature+''/'';\n        g.loadConsole(Ext.getBody(),topic);\n\n		va = va || {};\n        Ext.apply(va,{\n            action: ''workspace/packages/install''\n            ,signature: r.signature\n            ,register: ''mgr''\n            ,topic: topic\n        });\n\n        MODx.Ajax.request({\n            url: MODx.config.connector_url\n            ,params: va\n            ,listeners: {\n                ''success'': {fn:function() {\n                    var bc = Ext.getCmp(''packages-breadcrumbs'');\n                    var trail= bc.data.trail;\n                    trail.pop();\n\n                    if (trail.length > 1) {\n                        last = trail[trail.length - 1];\n\n                        if (last != undefined && last.rec != undefined) {\n                            bc.data.trail.pop();\n                            bc.data.trail.shift();\n                            bc.updateDetail(bc.data);\n\n                            var grid = Ext.getCmp(''modx-package-grid'');\n                            grid.install(last.rec);\n                            return;\n                        }\n                    }\n\n                    this.activate();\n					Ext.getCmp(''modx-package-grid'').refresh();\n                },scope:this}\n                ,''failure'': {fn:function() {\n                    this.activate();\n                },scope:this}\n            }\n        });\n\n        return true;\n	}";s:8:"disabled";s:5:"false";s:5:"scope";s:15:"[object Object]";s:8:"minWidth";s:2:"75";s:10:"removeMode";s:9:"container";s:10:"hideParent";s:4:"true";s:6:"events";s:15:"[object Object]";s:7:"ownerCt";s:15:"[object Object]";s:9:"container";s:15:"[object Object]";s:8:"rendered";s:4:"true";s:8:"template";s:15:"[object Object]";s:5:"btnEl";s:15:"[object Object]";s:4:"mons";s:15:"[object Object]";s:2:"el";s:15:"[object Object]";s:4:"icon";s:0:"";s:7:"iconCls";s:0:"";s:8:"boxReady";s:4:"true";s:8:"lastSize";s:15:"[object Object]";s:11:"useSetClass";s:4:"true";s:6:"oldCls";s:12:"x-btn-noicon";s:3:"doc";s:15:"[object Object]";s:19:"monitoringMouseOver";s:4:"true";s:6:"action";s:26:"workspace/packages/install";s:8:"register";s:3:"mgr";s:5:"topic";s:48:"/workspace/package/install/breadcrumbs-1.1.0-pl/";s:14:"package_action";i:0;}', 'Breadcrumbs', 'a:38:{s:2:"id";s:24:"4e52ae11f24554618600003c";s:7:"package";s:24:"4d556a9ab2b083396d0000eb";s:12:"display_name";s:20:"breadcrumbs-1.1.0-pl";s:4:"name";s:11:"Breadcrumbs";s:7:"version";s:5:"1.1.0";s:13:"version_major";s:1:"1";s:13:"version_minor";s:1:"1";s:13:"version_patch";s:1:"0";s:7:"release";s:2:"pl";s:8:"vrelease";s:2:"pl";s:14:"vrelease_index";s:1:"0";s:6:"author";s:12:"splittingred";s:11:"description";s:246:"<p>Creates a highly configurable and styleable breadcrumb navigation trail.</p>\n<p>\n</p><p>&nbsp;</p>\n<p>Breadcrumbs 1.1-rc2 (August 31, 2010)</p>\n<p>\n</p><ul>\n<li>Fix bug that skips the immediate children of site_start</li>\n</ul>\n<p></p>\n<p></p>";s:12:"instructions";s:38:"<p>Install via Package Management.</p>";s:9:"changelog";s:22:"<p>Initial content</p>";s:9:"createdon";s:24:"2011-08-22T19:29:21+0000";s:9:"createdby";s:12:"splittingred";s:8:"editedon";s:24:"2016-06-08T10:01:52+0000";s:10:"releasedon";s:24:"2011-08-22T19:29:21+0000";s:9:"downloads";s:5:"88792";s:8:"approved";s:4:"true";s:7:"audited";s:5:"false";s:8:"featured";s:5:"false";s:10:"deprecated";s:5:"false";s:7:"license";s:5:"GPLv2";s:7:"smf_url";s:0:"";s:10:"repository";s:24:"4d4c3fa6b2b0830da9000001";s:8:"supports";s:1:"2";s:8:"location";s:60:"http://modx.com/extras/download/?id=4e52ae12f24554618600003e";s:9:"signature";s:20:"breadcrumbs-1.1.0-pl";s:11:"supports_db";s:5:"mysql";s:16:"minimum_supports";s:1:"2";s:9:"breaks_at";s:8:"10000000";s:10:"screenshot";s:0:"";s:4:"file";a:7:{s:2:"id";s:24:"4e52ae12f24554618600003e";s:7:"version";s:24:"4e52ae11f24554618600003c";s:8:"filename";s:34:"breadcrumbs-1.1.0-pl.transport.zip";s:9:"downloads";s:5:"57326";s:6:"lastip";s:14:"95.213.244.155";s:9:"transport";s:4:"true";s:8:"location";s:60:"http://modx.com/extras/download/?id=4e52ae12f24554618600003e";}s:17:"package-signature";s:20:"breadcrumbs-1.1.0-pl";s:10:"categories";s:10:"navigation";s:4:"tags";s:11:"breadcrumbs";}', 1, 1, 0, 'pl', 0),
('ckeditor-1.3.0-pl', '2016-06-08 01:49:51', '2016-06-08 10:53:59', '2016-06-08 13:53:59', 0, 1, 1, 0, 'ckeditor-1.3.0-pl.transport.zip', NULL, 'a:34:{s:7:"license";s:15504:"GNU GENERAL PUBLIC LICENSE\r\n   Version 2, June 1991\r\n--------------------------\r\n\r\nCopyright (C) 1989, 1991 Free Software Foundation, Inc.\r\n59 Temple Place, Suite 330, Boston, MA  02111-1307  USA\r\n\r\nEveryone is permitted to copy and distribute verbatim copies\r\nof this license document, but changing it is not allowed.\r\n\r\nPreamble\r\n--------\r\n\r\n  The licenses for most software are designed to take away your\r\nfreedom to share and change it.  By contrast, the GNU General Public\r\nLicense is intended to guarantee your freedom to share and change free\r\nsoftware--to make sure the software is free for all its users.  This\r\nGeneral Public License applies to most of the Free Software\r\nFoundation''s software and to any other program whose authors commit to\r\nusing it.  (Some other Free Software Foundation software is covered by\r\nthe GNU Library General Public License instead.)  You can apply it to\r\nyour programs, too.\r\n\r\n  When we speak of free software, we are referring to freedom, not\r\nprice.  Our General Public Licenses are designed to make sure that you\r\nhave the freedom to distribute copies of free software (and charge for\r\nthis service if you wish), that you receive source code or can get it\r\nif you want it, that you can change the software or use pieces of it\r\nin new free programs; and that you know you can do these things.\r\n\r\n  To protect your rights, we need to make restrictions that forbid\r\nanyone to deny you these rights or to ask you to surrender the rights.\r\nThese restrictions translate to certain responsibilities for you if you\r\ndistribute copies of the software, or if you modify it.\r\n\r\n  For example, if you distribute copies of such a program, whether\r\ngratis or for a fee, you must give the recipients all the rights that\r\nyou have.  You must make sure that they, too, receive or can get the\r\nsource code.  And you must show them these terms so they know their\r\nrights.\r\n\r\n  We protect your rights with two steps: (1) copyright the software, and\r\n(2) offer you this license which gives you legal permission to copy,\r\ndistribute and/or modify the software.\r\n\r\n  Also, for each author''s protection and ours, we want to make certain\r\nthat everyone understands that there is no warranty for this free\r\nsoftware.  If the software is modified by someone else and passed on, we\r\nwant its recipients to know that what they have is not the original, so\r\nthat any problems introduced by others will not reflect on the original\r\nauthors'' reputations.\r\n\r\n  Finally, any free program is threatened constantly by software\r\npatents.  We wish to avoid the danger that redistributors of a free\r\nprogram will individually obtain patent licenses, in effect making the\r\nprogram proprietary.  To prevent this, we have made it clear that any\r\npatent must be licensed for everyone''s free use or not licensed at all.\r\n\r\n  The precise terms and conditions for copying, distribution and\r\nmodification follow.\r\n\r\n\r\nGNU GENERAL PUBLIC LICENSE\r\nTERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION\r\n---------------------------------------------------------------\r\n\r\n  0. This License applies to any program or other work which contains\r\na notice placed by the copyright holder saying it may be distributed\r\nunder the terms of this General Public License.  The "Program", below,\r\nrefers to any such program or work, and a "work based on the Program"\r\nmeans either the Program or any derivative work under copyright law:\r\nthat is to say, a work containing the Program or a portion of it,\r\neither verbatim or with modifications and/or translated into another\r\nlanguage.  (Hereinafter, translation is included without limitation in\r\nthe term "modification".)  Each licensee is addressed as "you".\r\n\r\nActivities other than copying, distribution and modification are not\r\ncovered by this License; they are outside its scope.  The act of\r\nrunning the Program is not restricted, and the output from the Program\r\nis covered only if its contents constitute a work based on the\r\nProgram (independent of having been made by running the Program).\r\nWhether that is true depends on what the Program does.\r\n\r\n  1. You may copy and distribute verbatim copies of the Program''s\r\nsource code as you receive it, in any medium, provided that you\r\nconspicuously and appropriately publish on each copy an appropriate\r\ncopyright notice and disclaimer of warranty; keep intact all the\r\nnotices that refer to this License and to the absence of any warranty;\r\nand give any other recipients of the Program a copy of this License\r\nalong with the Program.\r\n\r\nYou may charge a fee for the physical act of transferring a copy, and\r\nyou may at your option offer warranty protection in exchange for a fee.\r\n\r\n  2. You may modify your copy or copies of the Program or any portion\r\nof it, thus forming a work based on the Program, and copy and\r\ndistribute such modifications or work under the terms of Section 1\r\nabove, provided that you also meet all of these conditions:\r\n\r\n    a) You must cause the modified files to carry prominent notices\r\n    stating that you changed the files and the date of any change.\r\n\r\n    b) You must cause any work that you distribute or publish, that in\r\n    whole or in part contains or is derived from the Program or any\r\n    part thereof, to be licensed as a whole at no charge to all third\r\n    parties under the terms of this License.\r\n\r\n    c) If the modified program normally reads commands interactively\r\n    when run, you must cause it, when started running for such\r\n    interactive use in the most ordinary way, to print or display an\r\n    announcement including an appropriate copyright notice and a\r\n    notice that there is no warranty (or else, saying that you provide\r\n    a warranty) and that users may redistribute the program under\r\n    these conditions, and telling the user how to view a copy of this\r\n    License.  (Exception: if the Program itself is interactive but\r\n    does not normally print such an announcement, your work based on\r\n    the Program is not required to print an announcement.)\r\n\r\nThese requirements apply to the modified work as a whole.  If\r\nidentifiable sections of that work are not derived from the Program,\r\nand can be reasonably considered independent and separate works in\r\nthemselves, then this License, and its terms, do not apply to those\r\nsections when you distribute them as separate works.  But when you\r\ndistribute the same sections as part of a whole which is a work based\r\non the Program, the distribution of the whole must be on the terms of\r\nthis License, whose permissions for other licensees extend to the\r\nentire whole, and thus to each and every part regardless of who wrote it.\r\n\r\nThus, it is not the intent of this section to claim rights or contest\r\nyour rights to work written entirely by you; rather, the intent is to\r\nexercise the right to control the distribution of derivative or\r\ncollective works based on the Program.\r\n\r\nIn addition, mere aggregation of another work not based on the Program\r\nwith the Program (or with a work based on the Program) on a volume of\r\na storage or distribution medium does not bring the other work under\r\nthe scope of this License.\r\n\r\n  3. You may copy and distribute the Program (or a work based on it,\r\nunder Section 2) in object code or executable form under the terms of\r\nSections 1 and 2 above provided that you also do one of the following:\r\n\r\n    a) Accompany it with the complete corresponding machine-readable\r\n    source code, which must be distributed under the terms of Sections\r\n    1 and 2 above on a medium customarily used for software interchange; or,\r\n\r\n    b) Accompany it with a written offer, valid for at least three\r\n    years, to give any third party, for a charge no more than your\r\n    cost of physically performing source distribution, a complete\r\n    machine-readable copy of the corresponding source code, to be\r\n    distributed under the terms of Sections 1 and 2 above on a medium\r\n    customarily used for software interchange; or,\r\n\r\n    c) Accompany it with the information you received as to the offer\r\n    to distribute corresponding source code.  (This alternative is\r\n    allowed only for noncommercial distribution and only if you\r\n    received the program in object code or executable form with such\r\n    an offer, in accord with Subsection b above.)\r\n\r\nThe source code for a work means the preferred form of the work for\r\nmaking modifications to it.  For an executable work, complete source\r\ncode means all the source code for all modules it contains, plus any\r\nassociated interface definition files, plus the scripts used to\r\ncontrol compilation and installation of the executable.  However, as a\r\nspecial exception, the source code distributed need not include\r\nanything that is normally distributed (in either source or binary\r\nform) with the major components (compiler, kernel, and so on) of the\r\noperating system on which the executable runs, unless that component\r\nitself accompanies the executable.\r\n\r\nIf distribution of executable or object code is made by offering\r\naccess to copy from a designated place, then offering equivalent\r\naccess to copy the source code from the same place counts as\r\ndistribution of the source code, even though third parties are not\r\ncompelled to copy the source along with the object code.\r\n\r\n  4. You may not copy, modify, sublicense, or distribute the Program\r\nexcept as expressly provided under this License.  Any attempt\r\notherwise to copy, modify, sublicense or distribute the Program is\r\nvoid, and will automatically terminate your rights under this License.\r\nHowever, parties who have received copies, or rights, from you under\r\nthis License will not have their licenses terminated so long as such\r\nparties remain in full compliance.\r\n\r\n  5. You are not required to accept this License, since you have not\r\nsigned it.  However, nothing else grants you permission to modify or\r\ndistribute the Program or its derivative works.  These actions are\r\nprohibited by law if you do not accept this License.  Therefore, by\r\nmodifying or distributing the Program (or any work based on the\r\nProgram), you indicate your acceptance of this License to do so, and\r\nall its terms and conditions for copying, distributing or modifying\r\nthe Program or works based on it.\r\n\r\n  6. Each time you redistribute the Program (or any work based on the\r\nProgram), the recipient automatically receives a license from the\r\noriginal licensor to copy, distribute or modify the Program subject to\r\nthese terms and conditions.  You may not impose any further\r\nrestrictions on the recipients'' exercise of the rights granted herein.\r\nYou are not responsible for enforcing compliance by third parties to\r\nthis License.\r\n\r\n  7. If, as a consequence of a court judgment or allegation of patent\r\ninfringement or for any other reason (not limited to patent issues),\r\nconditions are imposed on you (whether by court order, agreement or\r\notherwise) that contradict the conditions of this License, they do not\r\nexcuse you from the conditions of this License.  If you cannot\r\ndistribute so as to satisfy simultaneously your obligations under this\r\nLicense and any other pertinent obligations, then as a consequence you\r\nmay not distribute the Program at all.  For example, if a patent\r\nlicense would not permit royalty-free redistribution of the Program by\r\nall those who receive copies directly or indirectly through you, then\r\nthe only way you could satisfy both it and this License would be to\r\nrefrain entirely from distribution of the Program.\r\n\r\nIf any portion of this section is held invalid or unenforceable under\r\nany particular circumstance, the balance of the section is intended to\r\napply and the section as a whole is intended to apply in other\r\ncircumstances.\r\n\r\nIt is not the purpose of this section to induce you to infringe any\r\npatents or other property right claims or to contest validity of any\r\nsuch claims; this section has the sole purpose of protecting the\r\nintegrity of the free software distribution system, which is\r\nimplemented by public license practices.  Many people have made\r\ngenerous contributions to the wide range of software distributed\r\nthrough that system in reliance on consistent application of that\r\nsystem; it is up to the author/donor to decide if he or she is willing\r\nto distribute software through any other system and a licensee cannot\r\nimpose that choice.\r\n\r\nThis section is intended to make thoroughly clear what is believed to\r\nbe a consequence of the rest of this License.\r\n\r\n  8. If the distribution and/or use of the Program is restricted in\r\ncertain countries either by patents or by copyrighted interfaces, the\r\noriginal copyright holder who places the Program under this License\r\nmay add an explicit geographical distribution limitation excluding\r\nthose countries, so that distribution is permitted only in or among\r\ncountries not thus excluded.  In such case, this License incorporates\r\nthe limitation as if written in the body of this License.\r\n\r\n  9. The Free Software Foundation may publish revised and/or new versions\r\nof the General Public License from time to time.  Such new versions will\r\nbe similar in spirit to the present version, but may differ in detail to\r\naddress new problems or concerns.\r\n\r\nEach version is given a distinguishing version number.  If the Program\r\nspecifies a version number of this License which applies to it and "any\r\nlater version", you have the option of following the terms and conditions\r\neither of that version or of any later version published by the Free\r\nSoftware Foundation.  If the Program does not specify a version number of\r\nthis License, you may choose any version ever published by the Free Software\r\nFoundation.\r\n\r\n  10. If you wish to incorporate parts of the Program into other free\r\nprograms whose distribution conditions are different, write to the author\r\nto ask for permission.  For software which is copyrighted by the Free\r\nSoftware Foundation, write to the Free Software Foundation; we sometimes\r\nmake exceptions for this.  Our decision will be guided by the two goals\r\nof preserving the free status of all derivatives of our free software and\r\nof promoting the sharing and reuse of software generally.\r\n\r\nNO WARRANTY\r\n-----------\r\n\r\n  11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY\r\nFOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN\r\nOTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES\r\nPROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED\r\nOR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF\r\nMERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS\r\nTO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE\r\nPROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,\r\nREPAIR OR CORRECTION.\r\n\r\n  12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING\r\nWILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR\r\nREDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,\r\nINCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING\r\nOUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED\r\nTO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY\r\nYOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER\r\nPROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE\r\nPOSSIBILITY OF SUCH DAMAGES.\r\n\r\n---------------------------\r\nEND OF TERMS AND CONDITIONS";s:6:"readme";s:279:"--------------------\nExtra: CKEditor\n--------------------\nVersion: 1.2.0\nCreated: December 7th, 2014\nSince: December 5th, 2012\nAuthor: Danil Kostin <danya.postfactum@gmail.com>\nLicense: GNU GPLv2 (or later at your option)\n\nIntegrates CKEditor WYSYWYG Editor into MODx Revolution.";s:9:"changelog";s:1565:"Changelog for CKEditor integration into MODx Revolution.\n\nCKEditor 1.3.0\n====================================\n- Add third-party components support (MODx.loadRTE()) (Romain @rtripault)\n- Add improved image plugin\n\nCKEditor 1.2.0\n====================================\n- Fixed MODx 2.3 compatibility\n- Added new settings (stylesSet, removePlugins, toolbarGroups, formatTags, nativeSpellchecker) (thanks to MokoJumbie and exside [Lukas Zahnd])\n- Addes plugins: SCAYT, QuickTable, AutoGrow (disabled by default)\n- Improved AutoCorrect Plugin (settings dialog, bugfixes, other improvements)\n- Fixed Ctrl+Z in Source mode\n- Simplified toolbar configuration (used less painful "toolbarGroups" option rather then "toolbar")\n\nCKEditor 1.1.1\n====================================\n- Fixed AjaxManager compatibility (tree drag''drop bug)\n- Added de language (settings localization: miduku)\n- Added plugins: youtube, wordcount, autocorrect\n- Simplified drag''n''drop from tree (remove dnd context menu)\n- IE8 support\n\nCKEditor 1.1.0\n====================================\n- MODx file browser integration\n- richtext TV support\n- Custom Resource Classes (Articles for ex.) support\n- Improved drag''d''drop from trees (insert images directly from File Tree)\n- Fixed Ctrl+S shorcut\n- Extra plugins setting\n- Editor skin setting\n\nCKEditor 1.0.2\n====================================\n- Fixed blocking bug occured in Firefox\n\nCKEditor 1.0.1\n====================================\n- Fixed fatal error\n- Inherit height of textarea\n\nCKEditor 1.0.0\n====================================\n- Initial commit";s:9:"signature";s:17:"ckeditor-1.3.0-pl";s:13:"initialConfig";s:15:"[object Object]";s:4:"text";s:20:"Продолжить";s:2:"id";s:19:"package-install-btn";s:6:"hidden";s:5:"false";s:7:"handler";s:1749:"function (va){\n        var r;\n        var g = Ext.getCmp(''modx-package-grid'');\n        if (!g) return false;\n        \n        if (va.signature != undefined && va.signature != '''') {\n            r = {signature: va.signature};\n        } else {\n            r = g.menu.record.data ? g.menu.record.data : g.menu.record;\n        }\n		var topic = ''/workspace/package/install/''+r.signature+''/'';\n        g.loadConsole(Ext.getBody(),topic);\n\n		va = va || {};\n        Ext.apply(va,{\n            action: ''workspace/packages/install''\n            ,signature: r.signature\n            ,register: ''mgr''\n            ,topic: topic\n        });\n\n        MODx.Ajax.request({\n            url: MODx.config.connector_url\n            ,params: va\n            ,listeners: {\n                ''success'': {fn:function() {\n                    var bc = Ext.getCmp(''packages-breadcrumbs'');\n                    var trail= bc.data.trail;\n                    trail.pop();\n\n                    if (trail.length > 1) {\n                        last = trail[trail.length - 1];\n\n                        if (last != undefined && last.rec != undefined) {\n                            bc.data.trail.pop();\n                            bc.data.trail.shift();\n                            bc.updateDetail(bc.data);\n\n                            var grid = Ext.getCmp(''modx-package-grid'');\n                            grid.install(last.rec);\n                            return;\n                        }\n                    }\n\n                    this.activate();\n					Ext.getCmp(''modx-package-grid'').refresh();\n                },scope:this}\n                ,''failure'': {fn:function() {\n                    this.activate();\n                },scope:this}\n            }\n        });\n\n        return true;\n	}";s:8:"disabled";s:5:"false";s:5:"scope";s:15:"[object Object]";s:8:"minWidth";s:2:"75";s:10:"removeMode";s:9:"container";s:10:"hideParent";s:4:"true";s:6:"events";s:15:"[object Object]";s:7:"ownerCt";s:15:"[object Object]";s:9:"container";s:15:"[object Object]";s:8:"rendered";s:4:"true";s:8:"template";s:15:"[object Object]";s:5:"btnEl";s:15:"[object Object]";s:4:"mons";s:15:"[object Object]";s:2:"el";s:15:"[object Object]";s:4:"icon";s:0:"";s:7:"iconCls";s:0:"";s:8:"boxReady";s:4:"true";s:8:"lastSize";s:15:"[object Object]";s:11:"useSetClass";s:4:"true";s:6:"oldCls";s:12:"x-btn-noicon";s:3:"doc";s:15:"[object Object]";s:19:"monitoringMouseOver";s:4:"true";s:6:"action";s:26:"workspace/packages/install";s:8:"register";s:3:"mgr";s:5:"topic";s:45:"/workspace/package/install/ckeditor-1.3.0-pl/";s:14:"package_action";i:0;}', 'CKEditor', 'a:38:{s:2:"id";s:24:"5483f63bdc532f2c59038447";s:7:"package";s:24:"50c06e7af245544310000044";s:12:"display_name";s:17:"ckeditor-1.3.0-pl";s:4:"name";s:8:"CKEditor";s:7:"version";s:5:"1.3.0";s:13:"version_major";s:1:"1";s:13:"version_minor";s:1:"3";s:13:"version_patch";s:1:"0";s:7:"release";s:2:"pl";s:8:"vrelease";s:2:"pl";s:14:"vrelease_index";s:0:"";s:6:"author";s:16:"danya_postfactum";s:11:"description";s:62:"<p>CKEditor is&nbsp;the best web text editor for everyone.</p>";s:12:"instructions";s:169:"<p>Install package, make sure `use_editor` is set to `true`, `wich_editor` is `CKEditor`</p><p>See ckeditor.com examples if you want to change toolbar configuration.</p>";s:9:"changelog";s:1696:"<p></p>CKEditor 1.3.0====================================- Add third-party components support (MODx.loadRTE()) (Romain @rtripault)- Add improved image pluginCKEditor 1.2.0====================================- Fixed MODx 2.3 compatibility- Added new settings (stylesSet, removePlugins, toolbarGroups, formatTags, nativeSpellchecker) (thanks to MokoJumbie and exside &#91;Lukas Zahnd&#93;)- Addes plugins: SCAYT, QuickTable, AutoGrow (disabled by default)- Improved AutoCorrect Plugin (settings dialog, bugfixes, other improvements)- Fixed Ctrl+Z in Source mode- Simplified toolbar configuration (used less painful "toolbarGroups" option rather then "toolbar")<p>CKEditor 1.1.1</p><p>====================================</p><p>- Fixed AjaxManager compatibility (tree drag''drop bug)</p><p>- Added de language (settings localization: miduku)</p><p>- Added plugins: youtube, wordcount, autocorrect</p><p>- Simplified drag''n''drop from tree (remove dnd context menu)</p><p>- IE8 support</p><p></p><p>CKEditor 1.1.0</p><p>====================================</p><p>- MODx file browser integration</p><p>- richtext TV support</p><p>- Custom Resource Classes (Articles for ex.) support</p><p>- Improved drag''d''drop from trees (insert images directly from File Tree)</p><p>- Fixed Ctrl+S shorcut</p><p>- Extra plugins setting</p><p>- Editor skin setting</p><p></p><p>CKEditor 1.0.2</p><p>====================================</p><p>- Fixed blocking bug occured in Firefox</p><p></p><p>CKEditor 1.0.1</p><p>====================================</p><p>- Fixed fatal error</p><p>- Inherit height of textarea</p><p></p><p>CKEditor 1.0.0</p><p>====================================</p><p>- Initial commit</p><p></p>";s:9:"createdon";s:24:"2014-12-07T06:39:55+0000";s:9:"createdby";s:16:"danya_postfactum";s:8:"editedon";s:24:"2016-06-08T09:54:42+0000";s:10:"releasedon";s:24:"2014-12-07T06:39:55+0000";s:9:"downloads";s:5:"27650";s:8:"approved";s:4:"true";s:7:"audited";s:5:"false";s:8:"featured";s:5:"false";s:10:"deprecated";s:5:"false";s:7:"license";s:5:"GPLv2";s:7:"smf_url";s:0:"";s:10:"repository";s:24:"4d4c3fa6b2b0830da9000001";s:8:"supports";s:3:"2.2";s:8:"location";s:60:"http://modx.com/extras/download/?id=5483f63cdc532f2c59038449";s:9:"signature";s:17:"ckeditor-1.3.0-pl";s:11:"supports_db";s:12:"mysql,sqlsrv";s:16:"minimum_supports";s:3:"2.2";s:9:"breaks_at";s:8:"10000000";s:10:"screenshot";s:73:"http://modx.s3.amazonaws.com/extras/50c06e7af245544310000044/ckeditor.jpg";s:4:"file";a:7:{s:2:"id";s:24:"5483f63cdc532f2c59038449";s:7:"version";s:24:"5483f63bdc532f2c59038447";s:8:"filename";s:31:"ckeditor-1.3.0-pl.transport.zip";s:9:"downloads";s:5:"13645";s:6:"lastip";s:13:"178.210.90.90";s:9:"transport";s:4:"true";s:8:"location";s:60:"http://modx.com/extras/download/?id=5483f63cdc532f2c59038449";}s:17:"package-signature";s:17:"ckeditor-1.3.0-pl";s:10:"categories";s:15:"richtexteditors";s:4:"tags";s:0:"";}', 1, 3, 0, 'pl', 0);
INSERT INTO `modx_transport_packages` (`signature`, `created`, `updated`, `installed`, `state`, `workspace`, `provider`, `disabled`, `source`, `manifest`, `attributes`, `package_name`, `metadata`, `version_major`, `version_minor`, `version_patch`, `release`, `release_index`) VALUES
('translit-1.0.0-beta', '2016-06-08 01:53:27', '2016-06-08 10:54:28', '2016-06-08 13:54:28', 0, 1, 1, 0, 'translit-1.0.0-beta.transport.zip', NULL, 'a:33:{s:7:"license";s:15218:"GNU GENERAL PUBLIC LICENSE\n   Version 2, June 1991\n--------------------------\n\nCopyright (C) 1989, 1991 Free Software Foundation, Inc.\n59 Temple Place, Suite 330, Boston, MA  02111-1307  USA\n\nEveryone is permitted to copy and distribute verbatim copies\nof this license document, but changing it is not allowed.\n\nPreamble\n--------\n\n  The licenses for most software are designed to take away your\nfreedom to share and change it.  By contrast, the GNU General Public\nLicense is intended to guarantee your freedom to share and change free\nsoftware--to make sure the software is free for all its users.  This\nGeneral Public License applies to most of the Free Software\nFoundation''s software and to any other program whose authors commit to\nusing it.  (Some other Free Software Foundation software is covered by\nthe GNU Library General Public License instead.)  You can apply it to\nyour programs, too.\n\n  When we speak of free software, we are referring to freedom, not\nprice.  Our General Public Licenses are designed to make sure that you\nhave the freedom to distribute copies of free software (and charge for\nthis service if you wish), that you receive source code or can get it\nif you want it, that you can change the software or use pieces of it\nin new free programs; and that you know you can do these things.\n\n  To protect your rights, we need to make restrictions that forbid\nanyone to deny you these rights or to ask you to surrender the rights.\nThese restrictions translate to certain responsibilities for you if you\ndistribute copies of the software, or if you modify it.\n\n  For example, if you distribute copies of such a program, whether\ngratis or for a fee, you must give the recipients all the rights that\nyou have.  You must make sure that they, too, receive or can get the\nsource code.  And you must show them these terms so they know their\nrights.\n\n  We protect your rights with two steps: (1) copyright the software, and\n(2) offer you this license which gives you legal permission to copy,\ndistribute and/or modify the software.\n\n  Also, for each author''s protection and ours, we want to make certain\nthat everyone understands that there is no warranty for this free\nsoftware.  If the software is modified by someone else and passed on, we\nwant its recipients to know that what they have is not the original, so\nthat any problems introduced by others will not reflect on the original\nauthors'' reputations.\n\n  Finally, any free program is threatened constantly by software\npatents.  We wish to avoid the danger that redistributors of a free\nprogram will individually obtain patent licenses, in effect making the\nprogram proprietary.  To prevent this, we have made it clear that any\npatent must be licensed for everyone''s free use or not licensed at all.\n\n  The precise terms and conditions for copying, distribution and\nmodification follow.\n\n\nGNU GENERAL PUBLIC LICENSE\nTERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION\n---------------------------------------------------------------\n\n  0. This License applies to any program or other work which contains\na notice placed by the copyright holder saying it may be distributed\nunder the terms of this General Public License.  The "Program", below,\nrefers to any such program or work, and a "work based on the Program"\nmeans either the Program or any derivative work under copyright law:\nthat is to say, a work containing the Program or a portion of it,\neither verbatim or with modifications and/or translated into another\nlanguage.  (Hereinafter, translation is included without limitation in\nthe term "modification".)  Each licensee is addressed as "you".\n\nActivities other than copying, distribution and modification are not\ncovered by this License; they are outside its scope.  The act of\nrunning the Program is not restricted, and the output from the Program\nis covered only if its contents constitute a work based on the\nProgram (independent of having been made by running the Program).\nWhether that is true depends on what the Program does.\n\n  1. You may copy and distribute verbatim copies of the Program''s\nsource code as you receive it, in any medium, provided that you\nconspicuously and appropriately publish on each copy an appropriate\ncopyright notice and disclaimer of warranty; keep intact all the\nnotices that refer to this License and to the absence of any warranty;\nand give any other recipients of the Program a copy of this License\nalong with the Program.\n\nYou may charge a fee for the physical act of transferring a copy, and\nyou may at your option offer warranty protection in exchange for a fee.\n\n  2. You may modify your copy or copies of the Program or any portion\nof it, thus forming a work based on the Program, and copy and\ndistribute such modifications or work under the terms of Section 1\nabove, provided that you also meet all of these conditions:\n\n    a) You must cause the modified files to carry prominent notices\n    stating that you changed the files and the date of any change.\n\n    b) You must cause any work that you distribute or publish, that in\n    whole or in part contains or is derived from the Program or any\n    part thereof, to be licensed as a whole at no charge to all third\n    parties under the terms of this License.\n\n    c) If the modified program normally reads commands interactively\n    when run, you must cause it, when started running for such\n    interactive use in the most ordinary way, to print or display an\n    announcement including an appropriate copyright notice and a\n    notice that there is no warranty (or else, saying that you provide\n    a warranty) and that users may redistribute the program under\n    these conditions, and telling the user how to view a copy of this\n    License.  (Exception: if the Program itself is interactive but\n    does not normally print such an announcement, your work based on\n    the Program is not required to print an announcement.)\n\nThese requirements apply to the modified work as a whole.  If\nidentifiable sections of that work are not derived from the Program,\nand can be reasonably considered independent and separate works in\nthemselves, then this License, and its terms, do not apply to those\nsections when you distribute them as separate works.  But when you\ndistribute the same sections as part of a whole which is a work based\non the Program, the distribution of the whole must be on the terms of\nthis License, whose permissions for other licensees extend to the\nentire whole, and thus to each and every part regardless of who wrote it.\n\nThus, it is not the intent of this section to claim rights or contest\nyour rights to work written entirely by you; rather, the intent is to\nexercise the right to control the distribution of derivative or\ncollective works based on the Program.\n\nIn addition, mere aggregation of another work not based on the Program\nwith the Program (or with a work based on the Program) on a volume of\na storage or distribution medium does not bring the other work under\nthe scope of this License.\n\n  3. You may copy and distribute the Program (or a work based on it,\nunder Section 2) in object code or executable form under the terms of\nSections 1 and 2 above provided that you also do one of the following:\n\n    a) Accompany it with the complete corresponding machine-readable\n    source code, which must be distributed under the terms of Sections\n    1 and 2 above on a medium customarily used for software interchange; or,\n\n    b) Accompany it with a written offer, valid for at least three\n    years, to give any third party, for a charge no more than your\n    cost of physically performing source distribution, a complete\n    machine-readable copy of the corresponding source code, to be\n    distributed under the terms of Sections 1 and 2 above on a medium\n    customarily used for software interchange; or,\n\n    c) Accompany it with the information you received as to the offer\n    to distribute corresponding source code.  (This alternative is\n    allowed only for noncommercial distribution and only if you\n    received the program in object code or executable form with such\n    an offer, in accord with Subsection b above.)\n\nThe source code for a work means the preferred form of the work for\nmaking modifications to it.  For an executable work, complete source\ncode means all the source code for all modules it contains, plus any\nassociated interface definition files, plus the scripts used to\ncontrol compilation and installation of the executable.  However, as a\nspecial exception, the source code distributed need not include\nanything that is normally distributed (in either source or binary\nform) with the major components (compiler, kernel, and so on) of the\noperating system on which the executable runs, unless that component\nitself accompanies the executable.\n\nIf distribution of executable or object code is made by offering\naccess to copy from a designated place, then offering equivalent\naccess to copy the source code from the same place counts as\ndistribution of the source code, even though third parties are not\ncompelled to copy the source along with the object code.\n\n  4. You may not copy, modify, sublicense, or distribute the Program\nexcept as expressly provided under this License.  Any attempt\notherwise to copy, modify, sublicense or distribute the Program is\nvoid, and will automatically terminate your rights under this License.\nHowever, parties who have received copies, or rights, from you under\nthis License will not have their licenses terminated so long as such\nparties remain in full compliance.\n\n  5. You are not required to accept this License, since you have not\nsigned it.  However, nothing else grants you permission to modify or\ndistribute the Program or its derivative works.  These actions are\nprohibited by law if you do not accept this License.  Therefore, by\nmodifying or distributing the Program (or any work based on the\nProgram), you indicate your acceptance of this License to do so, and\nall its terms and conditions for copying, distributing or modifying\nthe Program or works based on it.\n\n  6. Each time you redistribute the Program (or any work based on the\nProgram), the recipient automatically receives a license from the\noriginal licensor to copy, distribute or modify the Program subject to\nthese terms and conditions.  You may not impose any further\nrestrictions on the recipients'' exercise of the rights granted herein.\nYou are not responsible for enforcing compliance by third parties to\nthis License.\n\n  7. If, as a consequence of a court judgment or allegation of patent\ninfringement or for any other reason (not limited to patent issues),\nconditions are imposed on you (whether by court order, agreement or\notherwise) that contradict the conditions of this License, they do not\nexcuse you from the conditions of this License.  If you cannot\ndistribute so as to satisfy simultaneously your obligations under this\nLicense and any other pertinent obligations, then as a consequence you\nmay not distribute the Program at all.  For example, if a patent\nlicense would not permit royalty-free redistribution of the Program by\nall those who receive copies directly or indirectly through you, then\nthe only way you could satisfy both it and this License would be to\nrefrain entirely from distribution of the Program.\n\nIf any portion of this section is held invalid or unenforceable under\nany particular circumstance, the balance of the section is intended to\napply and the section as a whole is intended to apply in other\ncircumstances.\n\nIt is not the purpose of this section to induce you to infringe any\npatents or other property right claims or to contest validity of any\nsuch claims; this section has the sole purpose of protecting the\nintegrity of the free software distribution system, which is\nimplemented by public license practices.  Many people have made\ngenerous contributions to the wide range of software distributed\nthrough that system in reliance on consistent application of that\nsystem; it is up to the author/donor to decide if he or she is willing\nto distribute software through any other system and a licensee cannot\nimpose that choice.\n\nThis section is intended to make thoroughly clear what is believed to\nbe a consequence of the rest of this License.\n\n  8. If the distribution and/or use of the Program is restricted in\ncertain countries either by patents or by copyrighted interfaces, the\noriginal copyright holder who places the Program under this License\nmay add an explicit geographical distribution limitation excluding\nthose countries, so that distribution is permitted only in or among\ncountries not thus excluded.  In such case, this License incorporates\nthe limitation as if written in the body of this License.\n\n  9. The Free Software Foundation may publish revised and/or new versions\nof the General Public License from time to time.  Such new versions will\nbe similar in spirit to the present version, but may differ in detail to\naddress new problems or concerns.\n\nEach version is given a distinguishing version number.  If the Program\nspecifies a version number of this License which applies to it and "any\nlater version", you have the option of following the terms and conditions\neither of that version or of any later version published by the Free\nSoftware Foundation.  If the Program does not specify a version number of\nthis License, you may choose any version ever published by the Free Software\nFoundation.\n\n  10. If you wish to incorporate parts of the Program into other free\nprograms whose distribution conditions are different, write to the author\nto ask for permission.  For software which is copyrighted by the Free\nSoftware Foundation, write to the Free Software Foundation; we sometimes\nmake exceptions for this.  Our decision will be guided by the two goals\nof preserving the free status of all derivatives of our free software and\nof promoting the sharing and reuse of software generally.\n\nNO WARRANTY\n-----------\n\n  11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY\nFOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN\nOTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES\nPROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED\nOR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF\nMERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS\nTO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE\nPROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,\nREPAIR OR CORRECTION.\n\n  12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING\nWILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR\nREDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,\nINCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING\nOUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED\nTO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY\nYOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER\nPROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE\nPOSSIBILITY OF SUCH DAMAGES.\n\n---------------------------\nEND OF TERMS AND CONDITIONS";s:6:"readme";s:453:"--------------------\nExtension: translit\n--------------------\nVersion: 1.0.0-beta\nReleased: October 20, 2010\nSince: October 20, 2010\nAuthor: Jason Coward <jason@modx.com>\n\nA MODx Revolution Core Extension, the translit package provides a custom transliteration service class. When installed,\nthis is automatically available for the core Friendly URL alias transliteration process for Resources. You can also use\nthe service in your plugins and snippets.";s:9:"signature";s:19:"translit-1.0.0-beta";s:13:"initialConfig";s:15:"[object Object]";s:4:"text";s:20:"Продолжить";s:2:"id";s:19:"package-install-btn";s:6:"hidden";s:5:"false";s:7:"handler";s:1749:"function (va){\n        var r;\n        var g = Ext.getCmp(''modx-package-grid'');\n        if (!g) return false;\n        \n        if (va.signature != undefined && va.signature != '''') {\n            r = {signature: va.signature};\n        } else {\n            r = g.menu.record.data ? g.menu.record.data : g.menu.record;\n        }\n		var topic = ''/workspace/package/install/''+r.signature+''/'';\n        g.loadConsole(Ext.getBody(),topic);\n\n		va = va || {};\n        Ext.apply(va,{\n            action: ''workspace/packages/install''\n            ,signature: r.signature\n            ,register: ''mgr''\n            ,topic: topic\n        });\n\n        MODx.Ajax.request({\n            url: MODx.config.connector_url\n            ,params: va\n            ,listeners: {\n                ''success'': {fn:function() {\n                    var bc = Ext.getCmp(''packages-breadcrumbs'');\n                    var trail= bc.data.trail;\n                    trail.pop();\n\n                    if (trail.length > 1) {\n                        last = trail[trail.length - 1];\n\n                        if (last != undefined && last.rec != undefined) {\n                            bc.data.trail.pop();\n                            bc.data.trail.shift();\n                            bc.updateDetail(bc.data);\n\n                            var grid = Ext.getCmp(''modx-package-grid'');\n                            grid.install(last.rec);\n                            return;\n                        }\n                    }\n\n                    this.activate();\n					Ext.getCmp(''modx-package-grid'').refresh();\n                },scope:this}\n                ,''failure'': {fn:function() {\n                    this.activate();\n                },scope:this}\n            }\n        });\n\n        return true;\n	}";s:8:"disabled";s:5:"false";s:5:"scope";s:15:"[object Object]";s:8:"minWidth";s:2:"75";s:10:"removeMode";s:9:"container";s:10:"hideParent";s:4:"true";s:6:"events";s:15:"[object Object]";s:7:"ownerCt";s:15:"[object Object]";s:9:"container";s:15:"[object Object]";s:8:"rendered";s:4:"true";s:8:"template";s:15:"[object Object]";s:5:"btnEl";s:15:"[object Object]";s:4:"mons";s:15:"[object Object]";s:2:"el";s:15:"[object Object]";s:4:"icon";s:0:"";s:7:"iconCls";s:0:"";s:8:"boxReady";s:4:"true";s:8:"lastSize";s:15:"[object Object]";s:11:"useSetClass";s:4:"true";s:6:"oldCls";s:12:"x-btn-noicon";s:3:"doc";s:15:"[object Object]";s:19:"monitoringMouseOver";s:4:"true";s:6:"action";s:26:"workspace/packages/install";s:8:"register";s:3:"mgr";s:5:"topic";s:47:"/workspace/package/install/translit-1.0.0-beta/";s:14:"package_action";i:0;}', 'translit', 'a:38:{s:2:"id";s:24:"4d556cf0b2b083396d000eeb";s:7:"package";s:24:"4d556cf0b2b083396d000eea";s:12:"display_name";s:19:"translit-1.0.0-beta";s:4:"name";s:8:"translit";s:7:"version";s:5:"1.0.0";s:13:"version_major";s:1:"1";s:13:"version_minor";s:1:"0";s:13:"version_patch";s:1:"0";s:7:"release";s:4:"beta";s:8:"vrelease";s:4:"beta";s:14:"vrelease_index";s:1:"0";s:6:"author";s:8:"opengeek";s:11:"description";s:585:"<p>A service class that allows custom transliteration tables to be used when auto-creating or sanitizing friendly URL aliases in MODx Revolution. This initial beta release includes three tables that can be specified as the friendly_alias_translit System Setting:</p>\n<p><ol>\n<li>noaccents</li>\n<li>russian</li>\n<li>german</li>\n</ol>\nCustom named transliteration tables can be manually added to the {core_path}components/translit/model/modx/translit/tables/ directory and configured. Additional contributed transliteration tables will be included in future releases of the package.\n</p>";s:12:"instructions";s:37:"<p>Install via Package Management</p>";s:9:"changelog";s:0:"";s:9:"createdon";s:24:"2010-10-20T11:53:35+0000";s:9:"createdby";s:8:"opengeek";s:8:"editedon";s:24:"2016-06-08T10:11:22+0000";s:10:"releasedon";s:24:"2010-10-20T11:57:11+0000";s:9:"downloads";s:5:"76189";s:8:"approved";s:4:"true";s:7:"audited";s:5:"false";s:8:"featured";s:5:"false";s:10:"deprecated";s:5:"false";s:7:"license";s:5:"GPLv2";s:7:"smf_url";s:47:"http://modxcms.com/forums/index.php?topic=56059";s:10:"repository";s:24:"4d4c3fa6b2b0830da9000001";s:8:"supports";s:1:"2";s:8:"location";s:60:"http://modx.com/extras/download/?id=4d556cf1b2b083396d000eec";s:9:"signature";s:19:"translit-1.0.0-beta";s:11:"supports_db";s:5:"mysql";s:16:"minimum_supports";s:1:"2";s:9:"breaks_at";s:8:"10000000";s:10:"screenshot";s:0:"";s:4:"file";a:7:{s:2:"id";s:24:"4d556cf1b2b083396d000eec";s:7:"version";s:24:"4d556cf0b2b083396d000eeb";s:8:"filename";s:33:"translit-1.0.0-beta.transport.zip";s:9:"downloads";s:5:"76391";s:6:"lastip";s:13:"185.87.49.233";s:9:"transport";s:4:"true";s:8:"location";s:60:"http://modx.com/extras/download/?id=4d556cf1b2b083396d000eec";}s:17:"package-signature";s:19:"translit-1.0.0-beta";s:10:"categories";s:36:"internationalization,core-extensions";s:4:"tags";s:15:"transliteration";}', 1, 0, 0, 'beta', 0),
('filetranslit-0.1.2-pl2', '2016-06-08 01:53:32', '2016-06-08 10:54:05', '2016-06-08 13:54:05', 0, 1, 1, 0, 'filetranslit-0.1.2-pl2.transport.zip', NULL, 'a:34:{s:7:"license";s:18092:"                    GNU GENERAL PUBLIC LICENSE\n                       Version 2, June 1991\n\n Copyright (C) 1989, 1991 Free Software Foundation, Inc.,\n 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA\n Everyone is permitted to copy and distribute verbatim copies\n of this license document, but changing it is not allowed.\n\n                            Preamble\n\n  The licenses for most software are designed to take away your\nfreedom to share and change it.  By contrast, the GNU General Public\nLicense is intended to guarantee your freedom to share and change free\nsoftware--to make sure the software is free for all its users.  This\nGeneral Public License applies to most of the Free Software\nFoundation''s software and to any other program whose authors commit to\nusing it.  (Some other Free Software Foundation software is covered by\nthe GNU Lesser General Public License instead.)  You can apply it to\nyour programs, too.\n\n  When we speak of free software, we are referring to freedom, not\nprice.  Our General Public Licenses are designed to make sure that you\nhave the freedom to distribute copies of free software (and charge for\nthis service if you wish), that you receive source code or can get it\nif you want it, that you can change the software or use pieces of it\nin new free programs; and that you know you can do these things.\n\n  To protect your rights, we need to make restrictions that forbid\nanyone to deny you these rights or to ask you to surrender the rights.\nThese restrictions translate to certain responsibilities for you if you\ndistribute copies of the software, or if you modify it.\n\n  For example, if you distribute copies of such a program, whether\ngratis or for a fee, you must give the recipients all the rights that\nyou have.  You must make sure that they, too, receive or can get the\nsource code.  And you must show them these terms so they know their\nrights.\n\n  We protect your rights with two steps: (1) copyright the software, and\n(2) offer you this license which gives you legal permission to copy,\ndistribute and/or modify the software.\n\n  Also, for each author''s protection and ours, we want to make certain\nthat everyone understands that there is no warranty for this free\nsoftware.  If the software is modified by someone else and passed on, we\nwant its recipients to know that what they have is not the original, so\nthat any problems introduced by others will not reflect on the original\nauthors'' reputations.\n\n  Finally, any free program is threatened constantly by software\npatents.  We wish to avoid the danger that redistributors of a free\nprogram will individually obtain patent licenses, in effect making the\nprogram proprietary.  To prevent this, we have made it clear that any\npatent must be licensed for everyone''s free use or not licensed at all.\n\n  The precise terms and conditions for copying, distribution and\nmodification follow.\n\n                    GNU GENERAL PUBLIC LICENSE\n   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION\n\n  0. This License applies to any program or other work which contains\na notice placed by the copyright holder saying it may be distributed\nunder the terms of this General Public License.  The "Program", below,\nrefers to any such program or work, and a "work based on the Program"\nmeans either the Program or any derivative work under copyright law:\nthat is to say, a work containing the Program or a portion of it,\neither verbatim or with modifications and/or translated into another\nlanguage.  (Hereinafter, translation is included without limitation in\nthe term "modification".)  Each licensee is addressed as "you".\n\nActivities other than copying, distribution and modification are not\ncovered by this License; they are outside its scope.  The act of\nrunning the Program is not restricted, and the output from the Program\nis covered only if its contents constitute a work based on the\nProgram (independent of having been made by running the Program).\nWhether that is true depends on what the Program does.\n\n  1. You may copy and distribute verbatim copies of the Program''s\nsource code as you receive it, in any medium, provided that you\nconspicuously and appropriately publish on each copy an appropriate\ncopyright notice and disclaimer of warranty; keep intact all the\nnotices that refer to this License and to the absence of any warranty;\nand give any other recipients of the Program a copy of this License\nalong with the Program.\n\nYou may charge a fee for the physical act of transferring a copy, and\nyou may at your option offer warranty protection in exchange for a fee.\n\n  2. You may modify your copy or copies of the Program or any portion\nof it, thus forming a work based on the Program, and copy and\ndistribute such modifications or work under the terms of Section 1\nabove, provided that you also meet all of these conditions:\n\n    a) You must cause the modified files to carry prominent notices\n    stating that you changed the files and the date of any change.\n\n    b) You must cause any work that you distribute or publish, that in\n    whole or in part contains or is derived from the Program or any\n    part thereof, to be licensed as a whole at no charge to all third\n    parties under the terms of this License.\n\n    c) If the modified program normally reads commands interactively\n    when run, you must cause it, when started running for such\n    interactive use in the most ordinary way, to print or display an\n    announcement including an appropriate copyright notice and a\n    notice that there is no warranty (or else, saying that you provide\n    a warranty) and that users may redistribute the program under\n    these conditions, and telling the user how to view a copy of this\n    License.  (Exception: if the Program itself is interactive but\n    does not normally print such an announcement, your work based on\n    the Program is not required to print an announcement.)\n\nThese requirements apply to the modified work as a whole.  If\nidentifiable sections of that work are not derived from the Program,\nand can be reasonably considered independent and separate works in\nthemselves, then this License, and its terms, do not apply to those\nsections when you distribute them as separate works.  But when you\ndistribute the same sections as part of a whole which is a work based\non the Program, the distribution of the whole must be on the terms of\nthis License, whose permissions for other licensees extend to the\nentire whole, and thus to each and every part regardless of who wrote it.\n\nThus, it is not the intent of this section to claim rights or contest\nyour rights to work written entirely by you; rather, the intent is to\nexercise the right to control the distribution of derivative or\ncollective works based on the Program.\n\nIn addition, mere aggregation of another work not based on the Program\nwith the Program (or with a work based on the Program) on a volume of\na storage or distribution medium does not bring the other work under\nthe scope of this License.\n\n  3. You may copy and distribute the Program (or a work based on it,\nunder Section 2) in object code or executable form under the terms of\nSections 1 and 2 above provided that you also do one of the following:\n\n    a) Accompany it with the complete corresponding machine-readable\n    source code, which must be distributed under the terms of Sections\n    1 and 2 above on a medium customarily used for software interchange; or,\n\n    b) Accompany it with a written offer, valid for at least three\n    years, to give any third party, for a charge no more than your\n    cost of physically performing source distribution, a complete\n    machine-readable copy of the corresponding source code, to be\n    distributed under the terms of Sections 1 and 2 above on a medium\n    customarily used for software interchange; or,\n\n    c) Accompany it with the information you received as to the offer\n    to distribute corresponding source code.  (This alternative is\n    allowed only for noncommercial distribution and only if you\n    received the program in object code or executable form with such\n    an offer, in accord with Subsection b above.)\n\nThe source code for a work means the preferred form of the work for\nmaking modifications to it.  For an executable work, complete source\ncode means all the source code for all modules it contains, plus any\nassociated interface definition files, plus the scripts used to\ncontrol compilation and installation of the executable.  However, as a\nspecial exception, the source code distributed need not include\nanything that is normally distributed (in either source or binary\nform) with the major components (compiler, kernel, and so on) of the\noperating system on which the executable runs, unless that component\nitself accompanies the executable.\n\nIf distribution of executable or object code is made by offering\naccess to copy from a designated place, then offering equivalent\naccess to copy the source code from the same place counts as\ndistribution of the source code, even though third parties are not\ncompelled to copy the source along with the object code.\n\n  4. You may not copy, modify, sublicense, or distribute the Program\nexcept as expressly provided under this License.  Any attempt\notherwise to copy, modify, sublicense or distribute the Program is\nvoid, and will automatically terminate your rights under this License.\nHowever, parties who have received copies, or rights, from you under\nthis License will not have their licenses terminated so long as such\nparties remain in full compliance.\n\n  5. You are not required to accept this License, since you have not\nsigned it.  However, nothing else grants you permission to modify or\ndistribute the Program or its derivative works.  These actions are\nprohibited by law if you do not accept this License.  Therefore, by\nmodifying or distributing the Program (or any work based on the\nProgram), you indicate your acceptance of this License to do so, and\nall its terms and conditions for copying, distributing or modifying\nthe Program or works based on it.\n\n  6. Each time you redistribute the Program (or any work based on the\nProgram), the recipient automatically receives a license from the\noriginal licensor to copy, distribute or modify the Program subject to\nthese terms and conditions.  You may not impose any further\nrestrictions on the recipients'' exercise of the rights granted herein.\nYou are not responsible for enforcing compliance by third parties to\nthis License.\n\n  7. If, as a consequence of a court judgment or allegation of patent\ninfringement or for any other reason (not limited to patent issues),\nconditions are imposed on you (whether by court order, agreement or\notherwise) that contradict the conditions of this License, they do not\nexcuse you from the conditions of this License.  If you cannot\ndistribute so as to satisfy simultaneously your obligations under this\nLicense and any other pertinent obligations, then as a consequence you\nmay not distribute the Program at all.  For example, if a patent\nlicense would not permit royalty-free redistribution of the Program by\nall those who receive copies directly or indirectly through you, then\nthe only way you could satisfy both it and this License would be to\nrefrain entirely from distribution of the Program.\n\nIf any portion of this section is held invalid or unenforceable under\nany particular circumstance, the balance of the section is intended to\napply and the section as a whole is intended to apply in other\ncircumstances.\n\nIt is not the purpose of this section to induce you to infringe any\npatents or other property right claims or to contest validity of any\nsuch claims; this section has the sole purpose of protecting the\nintegrity of the free software distribution system, which is\nimplemented by public license practices.  Many people have made\ngenerous contributions to the wide range of software distributed\nthrough that system in reliance on consistent application of that\nsystem; it is up to the author/donor to decide if he or she is willing\nto distribute software through any other system and a licensee cannot\nimpose that choice.\n\nThis section is intended to make thoroughly clear what is believed to\nbe a consequence of the rest of this License.\n\n  8. If the distribution and/or use of the Program is restricted in\ncertain countries either by patents or by copyrighted interfaces, the\noriginal copyright holder who places the Program under this License\nmay add an explicit geographical distribution limitation excluding\nthose countries, so that distribution is permitted only in or among\ncountries not thus excluded.  In such case, this License incorporates\nthe limitation as if written in the body of this License.\n\n  9. The Free Software Foundation may publish revised and/or new versions\nof the General Public License from time to time.  Such new versions will\nbe similar in spirit to the present version, but may differ in detail to\naddress new problems or concerns.\n\nEach version is given a distinguishing version number.  If the Program\nspecifies a version number of this License which applies to it and "any\nlater version", you have the option of following the terms and conditions\neither of that version or of any later version published by the Free\nSoftware Foundation.  If the Program does not specify a version number of\nthis License, you may choose any version ever published by the Free Software\nFoundation.\n\n  10. If you wish to incorporate parts of the Program into other free\nprograms whose distribution conditions are different, write to the author\nto ask for permission.  For software which is copyrighted by the Free\nSoftware Foundation, write to the Free Software Foundation; we sometimes\nmake exceptions for this.  Our decision will be guided by the two goals\nof preserving the free status of all derivatives of our free software and\nof promoting the sharing and reuse of software generally.\n\n                            NO WARRANTY\n\n  11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY\nFOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN\nOTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES\nPROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED\nOR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF\nMERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS\nTO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE\nPROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,\nREPAIR OR CORRECTION.\n\n  12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING\nWILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR\nREDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,\nINCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING\nOUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED\nTO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY\nYOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER\nPROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE\nPOSSIBILITY OF SUCH DAMAGES.\n\n                     END OF TERMS AND CONDITIONS\n\n            How to Apply These Terms to Your New Programs\n\n  If you develop a new program, and you want it to be of the greatest\npossible use to the public, the best way to achieve this is to make it\nfree software which everyone can redistribute and change under these terms.\n\n  To do so, attach the following notices to the program.  It is safest\nto attach them to the start of each source file to most effectively\nconvey the exclusion of warranty; and each file should have at least\nthe "copyright" line and a pointer to where the full notice is found.\n\n    <one line to give the program''s name and a brief idea of what it does.>\n    Copyright (C) <year>  <name of author>\n\n    This program is free software; you can redistribute it and/or modify\n    it under the terms of the GNU General Public License as published by\n    the Free Software Foundation; either version 2 of the License, or\n    (at your option) any later version.\n\n    This program is distributed in the hope that it will be useful,\n    but WITHOUT ANY WARRANTY; without even the implied warranty of\n    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n    GNU General Public License for more details.\n\n    You should have received a copy of the GNU General Public License along\n    with this program; if not, write to the Free Software Foundation, Inc.,\n    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.\n\nAlso add information on how to contact you by electronic and paper mail.\n\nIf the program is interactive, make it output a short notice like this\nwhen it starts in an interactive mode:\n\n    Gnomovision version 69, Copyright (C) year name of author\n    Gnomovision comes with ABSOLUTELY NO WARRANTY; for details type `show w''.\n    This is free software, and you are welcome to redistribute it\n    under certain conditions; type `show c'' for details.\n\nThe hypothetical commands `show w'' and `show c'' should show the appropriate\nparts of the General Public License.  Of course, the commands you use may\nbe called something other than `show w'' and `show c''; they could even be\nmouse-clicks or menu items--whatever suits your program.\n\nYou should also get your employer (if you work as a programmer) or your\nschool, if any, to sign a "copyright disclaimer" for the program, if\nnecessary.  Here is a sample; alter the names:\n\n  Yoyodyne, Inc., hereby disclaims all copyright interest in the program\n  `Gnomovision'' (which makes passes at compilers) written by James Hacker.\n\n  <signature of Ty Coon>, 1 April 1989\n  Ty Coon, President of Vice\n\nThis General Public License does not permit incorporating your program into\nproprietary programs.  If your program is a subroutine library, you may\nconsider it more useful to permit linking proprietary applications with the\nlibrary.  If this is what you want to do, use the GNU Lesser General\nPublic License instead of this License.\n";s:6:"readme";s:330:"This plugin transliterates names of files uploaded via MODX file manager.\r\n\r\nInstallation:\r\n1. Install filetranslit via MODX Package Manager.\r\n2. Install and configure translit package (http://modx.com/extras/package/translit) if needed.\r\n3. Check friendly_alias_translit system setting for correct value (eg. russian or iconv).\r\n";s:9:"changelog";s:181:"v0.1.2-pl2\r\nFix: Translate plugin description\r\n\r\nv0.1.2-pl1\r\nFix: MODX 2.2.6-pl generates an error while uploading in some cases.\r\nAdd: More friendly docs.\r\n\r\nv0.1.1 - First release";s:9:"signature";s:22:"filetranslit-0.1.2-pl2";s:13:"initialConfig";s:15:"[object Object]";s:4:"text";s:20:"Продолжить";s:2:"id";s:19:"package-install-btn";s:6:"hidden";s:5:"false";s:7:"handler";s:1749:"function (va){\n        var r;\n        var g = Ext.getCmp(''modx-package-grid'');\n        if (!g) return false;\n        \n        if (va.signature != undefined && va.signature != '''') {\n            r = {signature: va.signature};\n        } else {\n            r = g.menu.record.data ? g.menu.record.data : g.menu.record;\n        }\n		var topic = ''/workspace/package/install/''+r.signature+''/'';\n        g.loadConsole(Ext.getBody(),topic);\n\n		va = va || {};\n        Ext.apply(va,{\n            action: ''workspace/packages/install''\n            ,signature: r.signature\n            ,register: ''mgr''\n            ,topic: topic\n        });\n\n        MODx.Ajax.request({\n            url: MODx.config.connector_url\n            ,params: va\n            ,listeners: {\n                ''success'': {fn:function() {\n                    var bc = Ext.getCmp(''packages-breadcrumbs'');\n                    var trail= bc.data.trail;\n                    trail.pop();\n\n                    if (trail.length > 1) {\n                        last = trail[trail.length - 1];\n\n                        if (last != undefined && last.rec != undefined) {\n                            bc.data.trail.pop();\n                            bc.data.trail.shift();\n                            bc.updateDetail(bc.data);\n\n                            var grid = Ext.getCmp(''modx-package-grid'');\n                            grid.install(last.rec);\n                            return;\n                        }\n                    }\n\n                    this.activate();\n					Ext.getCmp(''modx-package-grid'').refresh();\n                },scope:this}\n                ,''failure'': {fn:function() {\n                    this.activate();\n                },scope:this}\n            }\n        });\n\n        return true;\n	}";s:8:"disabled";s:5:"false";s:5:"scope";s:15:"[object Object]";s:8:"minWidth";s:2:"75";s:10:"removeMode";s:9:"container";s:10:"hideParent";s:4:"true";s:6:"events";s:15:"[object Object]";s:7:"ownerCt";s:15:"[object Object]";s:9:"container";s:15:"[object Object]";s:8:"rendered";s:4:"true";s:8:"template";s:15:"[object Object]";s:5:"btnEl";s:15:"[object Object]";s:4:"mons";s:15:"[object Object]";s:2:"el";s:15:"[object Object]";s:4:"icon";s:0:"";s:7:"iconCls";s:0:"";s:8:"boxReady";s:4:"true";s:8:"lastSize";s:15:"[object Object]";s:11:"useSetClass";s:4:"true";s:6:"oldCls";s:12:"x-btn-noicon";s:3:"doc";s:15:"[object Object]";s:19:"monitoringMouseOver";s:4:"true";s:6:"action";s:26:"workspace/packages/install";s:8:"register";s:3:"mgr";s:5:"topic";s:50:"/workspace/package/install/filetranslit-0.1.2-pl2/";s:14:"package_action";i:0;}', 'filetranslit', 'a:38:{s:2:"id";s:24:"52fe943262cf2449ca0005e1";s:7:"package";s:24:"513de5edf2455405d1000061";s:12:"display_name";s:22:"filetranslit-0.1.2-pl2";s:4:"name";s:12:"filetranslit";s:7:"version";s:5:"0.1.2";s:13:"version_major";s:1:"0";s:13:"version_minor";s:1:"1";s:13:"version_patch";s:1:"2";s:7:"release";s:3:"pl2";s:8:"vrelease";s:2:"pl";s:14:"vrelease_index";s:1:"2";s:6:"author";s:8:"trianman";s:11:"description";s:116:"<p>This plugin transliterates names of files&nbsp;with non-latin characters&nbsp;uploaded via MODX file manager.</p>";s:12:"instructions";s:362:"<p><li>Install filetranslit via MODX Package Manager.</li><li>Install and configure translit package (http://modx.com/extras/package/translit) if needed.</li><li>Check friendly_alias_translit system setting for correct value (eg. russian or iconv).</li><li>Upload a file with non-latin characters in it''s name via the MODX filemanager.</li><li>Have fun!</li></p>";s:9:"changelog";s:173:"<p><b>v0.1.2-pl2</b>Fix: Translate plugin description</p><p><b>v0.1.2-pl1</b>Fix: MODX 2.2.6-pl generates an error while uploading in some cases.Add: More friendly docs.</p>";s:9:"createdon";s:24:"2014-02-14T22:09:54+0000";s:9:"createdby";s:8:"trianman";s:8:"editedon";s:24:"2016-06-08T09:27:09+0000";s:10:"releasedon";s:24:"2014-02-24T06:05:00+0000";s:9:"downloads";s:4:"1613";s:8:"approved";s:4:"true";s:7:"audited";s:5:"false";s:8:"featured";s:5:"false";s:10:"deprecated";s:5:"false";s:7:"license";s:5:"GPLv2";s:7:"smf_url";s:0:"";s:10:"repository";s:24:"4d4c3fa6b2b0830da9000001";s:8:"supports";s:3:"2.2";s:8:"location";s:60:"http://modx.com/extras/download/?id=52fe943262cf2449ca0005e2";s:9:"signature";s:22:"filetranslit-0.1.2-pl2";s:11:"supports_db";s:12:"mysql,sqlsrv";s:16:"minimum_supports";s:3:"2.2";s:9:"breaks_at";s:8:"10000000";s:10:"screenshot";s:65:"http://modx.s3.amazonaws.com/extras%2F513de5edf2455405d1000061%2F";s:4:"file";a:7:{s:2:"id";s:24:"52fe943262cf2449ca0005e2";s:7:"version";s:24:"52fe943262cf2449ca0005e1";s:8:"filename";s:36:"filetranslit-0.1.2-pl2.transport.zip";s:9:"downloads";s:4:"1333";s:6:"lastip";s:13:"80.87.193.106";s:9:"transport";s:4:"true";s:8:"location";s:60:"http://modx.com/extras/download/?id=52fe943262cf2449ca0005e2";}s:17:"package-signature";s:22:"filetranslit-0.1.2-pl2";s:10:"categories";s:20:"internationalization";s:4:"tags";s:0:"";}', 0, 1, 2, 'pl', 2);
INSERT INTO `modx_transport_packages` (`signature`, `created`, `updated`, `installed`, `state`, `workspace`, `provider`, `disabled`, `source`, `manifest`, `attributes`, `package_name`, `metadata`, `version_major`, `version_minor`, `version_patch`, `release`, `release_index`) VALUES
('getresourcefield-1.0.3-pl', '2016-06-08 04:04:16', '2016-06-08 13:04:55', '2016-06-08 16:04:55', 0, 1, 1, 0, 'getresourcefield-1.0.3-pl.transport.zip', NULL, 'a:33:{s:7:"license";s:1083:"The MIT License\n\nPermission is hereby granted, free of charge, to any person obtaining a copy\nof this software and associated documentation files (the "Software"), to deal\nin the Software without restriction, including without limitation the rights\nto use, copy, modify, merge, publish, distribute, sublicense, and/or sell\ncopies of the Software, and to permit persons to whom the Software is\nfurnished to do so, subject to the following conditions:\n\nThe above copyright notice and this permission notice shall be included in\nall copies or substantial portions of the Software.\n\nTHE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR\nIMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,\nFITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE\nAUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER\nLIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,\nOUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN\nTHE SOFTWARE.\n\nSoftware Copyright (c) 2010 Paul Merchant\n";s:6:"readme";s:751:"getResourceField\n\nA modx snippet to get a field or a TV value from a given resource. TV values can\nbe processed unprocessed. \n\n@ author Paul Merchant\n@ author Shaun McCormick\n@ copyright 2011 Paul Merchant\n@ version 1.0.3 - August 8, 2011\n@ MIT License\n\nOPTIONS\nid - The resource ID\nfield - (Opt) The field or template variable name. Defaults to pagetitle\nisTV - (Opt) Set as true to return a raw template variable\nprocessTV - (Opt) Set as true to return a rendered template variable\ndefault - (Opt) The value returned if no field value is found\n\nExmaple1: [[getResourceField? &id=`123`]]\nExample2: [[getResourceField? &id=`[[UltimateParent?]]` &field=`myTV` &isTV=`1`]]\nExample3: [[getResourceField? &id=`[[*parent]]` &field=`myTV` &processTV=`1`]]\n\n";s:9:"signature";s:25:"getresourcefield-1.0.3-pl";s:13:"initialConfig";s:15:"[object Object]";s:4:"text";s:20:"Продолжить";s:2:"id";s:19:"package-install-btn";s:6:"hidden";s:5:"false";s:7:"handler";s:1749:"function (va){\n        var r;\n        var g = Ext.getCmp(''modx-package-grid'');\n        if (!g) return false;\n        \n        if (va.signature != undefined && va.signature != '''') {\n            r = {signature: va.signature};\n        } else {\n            r = g.menu.record.data ? g.menu.record.data : g.menu.record;\n        }\n		var topic = ''/workspace/package/install/''+r.signature+''/'';\n        g.loadConsole(Ext.getBody(),topic);\n\n		va = va || {};\n        Ext.apply(va,{\n            action: ''workspace/packages/install''\n            ,signature: r.signature\n            ,register: ''mgr''\n            ,topic: topic\n        });\n\n        MODx.Ajax.request({\n            url: MODx.config.connector_url\n            ,params: va\n            ,listeners: {\n                ''success'': {fn:function() {\n                    var bc = Ext.getCmp(''packages-breadcrumbs'');\n                    var trail= bc.data.trail;\n                    trail.pop();\n\n                    if (trail.length > 1) {\n                        last = trail[trail.length - 1];\n\n                        if (last != undefined && last.rec != undefined) {\n                            bc.data.trail.pop();\n                            bc.data.trail.shift();\n                            bc.updateDetail(bc.data);\n\n                            var grid = Ext.getCmp(''modx-package-grid'');\n                            grid.install(last.rec);\n                            return;\n                        }\n                    }\n\n                    this.activate();\n					Ext.getCmp(''modx-package-grid'').refresh();\n                },scope:this}\n                ,''failure'': {fn:function() {\n                    this.activate();\n                },scope:this}\n            }\n        });\n\n        return true;\n	}";s:8:"disabled";s:5:"false";s:5:"scope";s:15:"[object Object]";s:8:"minWidth";s:2:"75";s:10:"removeMode";s:9:"container";s:10:"hideParent";s:4:"true";s:6:"events";s:15:"[object Object]";s:7:"ownerCt";s:15:"[object Object]";s:9:"container";s:15:"[object Object]";s:8:"rendered";s:4:"true";s:8:"template";s:15:"[object Object]";s:5:"btnEl";s:15:"[object Object]";s:4:"mons";s:15:"[object Object]";s:2:"el";s:15:"[object Object]";s:4:"icon";s:0:"";s:7:"iconCls";s:0:"";s:8:"boxReady";s:4:"true";s:8:"lastSize";s:15:"[object Object]";s:11:"useSetClass";s:4:"true";s:6:"oldCls";s:12:"x-btn-noicon";s:3:"doc";s:15:"[object Object]";s:19:"monitoringMouseOver";s:4:"true";s:6:"action";s:26:"workspace/packages/install";s:8:"register";s:3:"mgr";s:5:"topic";s:53:"/workspace/package/install/getresourcefield-1.0.3-pl/";s:14:"package_action";i:0;}', 'getResourceField', 'a:38:{s:2:"id";s:24:"4d556cd3b2b083396d000e3d";s:7:"package";s:24:"4d556cd3b2b083396d000e3c";s:12:"display_name";s:25:"getresourcefield-1.0.3-pl";s:4:"name";s:16:"getResourceField";s:7:"version";s:5:"1.0.3";s:13:"version_major";s:1:"1";s:13:"version_minor";s:1:"0";s:13:"version_patch";s:1:"3";s:7:"release";s:3:"pl1";s:8:"vrelease";s:2:"pl";s:14:"vrelease_index";s:1:"1";s:6:"author";s:12:"paulmerchant";s:11:"description";s:231:"<p>A snippet to return a field or a TV value from a specified resource. The TV values returned can be processed or unprocessed. getResourceField can be easily combined with UltimateParent or other calls for added functionality.</p>";s:12:"instructions";s:128:"<p>Install with Revolution''s Package Management. Brief documentation and examples are included in the header of the snippet.</p>";s:9:"changelog";s:120:"<p>Fixed possible infinite loop issue when the snippet would start to get the content field of the current resource.</p>";s:9:"createdon";s:24:"2010-07-28T11:43:26+0000";s:9:"createdby";s:12:"paulmerchant";s:8:"editedon";s:24:"2016-06-08T11:47:33+0000";s:10:"releasedon";s:24:"2010-09-16T12:22:55+0000";s:9:"downloads";s:5:"29577";s:8:"approved";s:4:"true";s:7:"audited";s:5:"false";s:8:"featured";s:5:"false";s:10:"deprecated";s:5:"false";s:7:"license";s:3:"MIT";s:7:"smf_url";s:47:"http://modxcms.com/forums/index.php?topic=52181";s:10:"repository";s:24:"4d4c3fa6b2b0830da9000001";s:8:"supports";s:1:"2";s:8:"location";s:60:"http://modx.com/extras/download/?id=4e405c61f24554209200005a";s:9:"signature";s:25:"getresourcefield-1.0.3-pl";s:11:"supports_db";s:5:"mysql";s:16:"minimum_supports";s:1:"2";s:9:"breaks_at";s:8:"10000000";s:10:"screenshot";s:0:"";s:4:"file";a:7:{s:2:"id";s:24:"4e405c61f24554209200005a";s:7:"version";s:24:"4d556cd3b2b083396d000e3d";s:8:"filename";s:39:"getresourcefield-1.0.3-pl.transport.zip";s:9:"downloads";s:5:"25494";s:6:"lastip";s:12:"66.249.65.82";s:9:"transport";s:4:"true";s:8:"location";s:60:"http://modx.com/extras/download/?id=4e405c61f24554209200005a";}s:17:"package-signature";s:25:"getresourcefield-1.0.3-pl";s:10:"categories";s:17:"content,utilities";s:4:"tags";s:0:"";}', 1, 0, 3, 'pl', 0),
('wayfinder-2.3.3-pl', '2016-06-08 05:53:37', '2016-06-08 14:54:49', '2016-06-08 17:54:49', 0, 1, 1, 0, 'wayfinder-2.3.3-pl.transport.zip', NULL, 'a:34:{s:7:"license";s:15218:"GNU GENERAL PUBLIC LICENSE\n   Version 2, June 1991\n--------------------------\n\nCopyright (C) 1989, 1991 Free Software Foundation, Inc.\n59 Temple Place, Suite 330, Boston, MA  02111-1307  USA\n\nEveryone is permitted to copy and distribute verbatim copies\nof this license document, but changing it is not allowed.\n\nPreamble\n--------\n\n  The licenses for most software are designed to take away your\nfreedom to share and change it.  By contrast, the GNU General Public\nLicense is intended to guarantee your freedom to share and change free\nsoftware--to make sure the software is free for all its users.  This\nGeneral Public License applies to most of the Free Software\nFoundation''s software and to any other program whose authors commit to\nusing it.  (Some other Free Software Foundation software is covered by\nthe GNU Library General Public License instead.)  You can apply it to\nyour programs, too.\n\n  When we speak of free software, we are referring to freedom, not\nprice.  Our General Public Licenses are designed to make sure that you\nhave the freedom to distribute copies of free software (and charge for\nthis service if you wish), that you receive source code or can get it\nif you want it, that you can change the software or use pieces of it\nin new free programs; and that you know you can do these things.\n\n  To protect your rights, we need to make restrictions that forbid\nanyone to deny you these rights or to ask you to surrender the rights.\nThese restrictions translate to certain responsibilities for you if you\ndistribute copies of the software, or if you modify it.\n\n  For example, if you distribute copies of such a program, whether\ngratis or for a fee, you must give the recipients all the rights that\nyou have.  You must make sure that they, too, receive or can get the\nsource code.  And you must show them these terms so they know their\nrights.\n\n  We protect your rights with two steps: (1) copyright the software, and\n(2) offer you this license which gives you legal permission to copy,\ndistribute and/or modify the software.\n\n  Also, for each author''s protection and ours, we want to make certain\nthat everyone understands that there is no warranty for this free\nsoftware.  If the software is modified by someone else and passed on, we\nwant its recipients to know that what they have is not the original, so\nthat any problems introduced by others will not reflect on the original\nauthors'' reputations.\n\n  Finally, any free program is threatened constantly by software\npatents.  We wish to avoid the danger that redistributors of a free\nprogram will individually obtain patent licenses, in effect making the\nprogram proprietary.  To prevent this, we have made it clear that any\npatent must be licensed for everyone''s free use or not licensed at all.\n\n  The precise terms and conditions for copying, distribution and\nmodification follow.\n\n\nGNU GENERAL PUBLIC LICENSE\nTERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION\n---------------------------------------------------------------\n\n  0. This License applies to any program or other work which contains\na notice placed by the copyright holder saying it may be distributed\nunder the terms of this General Public License.  The "Program", below,\nrefers to any such program or work, and a "work based on the Program"\nmeans either the Program or any derivative work under copyright law:\nthat is to say, a work containing the Program or a portion of it,\neither verbatim or with modifications and/or translated into another\nlanguage.  (Hereinafter, translation is included without limitation in\nthe term "modification".)  Each licensee is addressed as "you".\n\nActivities other than copying, distribution and modification are not\ncovered by this License; they are outside its scope.  The act of\nrunning the Program is not restricted, and the output from the Program\nis covered only if its contents constitute a work based on the\nProgram (independent of having been made by running the Program).\nWhether that is true depends on what the Program does.\n\n  1. You may copy and distribute verbatim copies of the Program''s\nsource code as you receive it, in any medium, provided that you\nconspicuously and appropriately publish on each copy an appropriate\ncopyright notice and disclaimer of warranty; keep intact all the\nnotices that refer to this License and to the absence of any warranty;\nand give any other recipients of the Program a copy of this License\nalong with the Program.\n\nYou may charge a fee for the physical act of transferring a copy, and\nyou may at your option offer warranty protection in exchange for a fee.\n\n  2. You may modify your copy or copies of the Program or any portion\nof it, thus forming a work based on the Program, and copy and\ndistribute such modifications or work under the terms of Section 1\nabove, provided that you also meet all of these conditions:\n\n    a) You must cause the modified files to carry prominent notices\n    stating that you changed the files and the date of any change.\n\n    b) You must cause any work that you distribute or publish, that in\n    whole or in part contains or is derived from the Program or any\n    part thereof, to be licensed as a whole at no charge to all third\n    parties under the terms of this License.\n\n    c) If the modified program normally reads commands interactively\n    when run, you must cause it, when started running for such\n    interactive use in the most ordinary way, to print or display an\n    announcement including an appropriate copyright notice and a\n    notice that there is no warranty (or else, saying that you provide\n    a warranty) and that users may redistribute the program under\n    these conditions, and telling the user how to view a copy of this\n    License.  (Exception: if the Program itself is interactive but\n    does not normally print such an announcement, your work based on\n    the Program is not required to print an announcement.)\n\nThese requirements apply to the modified work as a whole.  If\nidentifiable sections of that work are not derived from the Program,\nand can be reasonably considered independent and separate works in\nthemselves, then this License, and its terms, do not apply to those\nsections when you distribute them as separate works.  But when you\ndistribute the same sections as part of a whole which is a work based\non the Program, the distribution of the whole must be on the terms of\nthis License, whose permissions for other licensees extend to the\nentire whole, and thus to each and every part regardless of who wrote it.\n\nThus, it is not the intent of this section to claim rights or contest\nyour rights to work written entirely by you; rather, the intent is to\nexercise the right to control the distribution of derivative or\ncollective works based on the Program.\n\nIn addition, mere aggregation of another work not based on the Program\nwith the Program (or with a work based on the Program) on a volume of\na storage or distribution medium does not bring the other work under\nthe scope of this License.\n\n  3. You may copy and distribute the Program (or a work based on it,\nunder Section 2) in object code or executable form under the terms of\nSections 1 and 2 above provided that you also do one of the following:\n\n    a) Accompany it with the complete corresponding machine-readable\n    source code, which must be distributed under the terms of Sections\n    1 and 2 above on a medium customarily used for software interchange; or,\n\n    b) Accompany it with a written offer, valid for at least three\n    years, to give any third party, for a charge no more than your\n    cost of physically performing source distribution, a complete\n    machine-readable copy of the corresponding source code, to be\n    distributed under the terms of Sections 1 and 2 above on a medium\n    customarily used for software interchange; or,\n\n    c) Accompany it with the information you received as to the offer\n    to distribute corresponding source code.  (This alternative is\n    allowed only for noncommercial distribution and only if you\n    received the program in object code or executable form with such\n    an offer, in accord with Subsection b above.)\n\nThe source code for a work means the preferred form of the work for\nmaking modifications to it.  For an executable work, complete source\ncode means all the source code for all modules it contains, plus any\nassociated interface definition files, plus the scripts used to\ncontrol compilation and installation of the executable.  However, as a\nspecial exception, the source code distributed need not include\nanything that is normally distributed (in either source or binary\nform) with the major components (compiler, kernel, and so on) of the\noperating system on which the executable runs, unless that component\nitself accompanies the executable.\n\nIf distribution of executable or object code is made by offering\naccess to copy from a designated place, then offering equivalent\naccess to copy the source code from the same place counts as\ndistribution of the source code, even though third parties are not\ncompelled to copy the source along with the object code.\n\n  4. You may not copy, modify, sublicense, or distribute the Program\nexcept as expressly provided under this License.  Any attempt\notherwise to copy, modify, sublicense or distribute the Program is\nvoid, and will automatically terminate your rights under this License.\nHowever, parties who have received copies, or rights, from you under\nthis License will not have their licenses terminated so long as such\nparties remain in full compliance.\n\n  5. You are not required to accept this License, since you have not\nsigned it.  However, nothing else grants you permission to modify or\ndistribute the Program or its derivative works.  These actions are\nprohibited by law if you do not accept this License.  Therefore, by\nmodifying or distributing the Program (or any work based on the\nProgram), you indicate your acceptance of this License to do so, and\nall its terms and conditions for copying, distributing or modifying\nthe Program or works based on it.\n\n  6. Each time you redistribute the Program (or any work based on the\nProgram), the recipient automatically receives a license from the\noriginal licensor to copy, distribute or modify the Program subject to\nthese terms and conditions.  You may not impose any further\nrestrictions on the recipients'' exercise of the rights granted herein.\nYou are not responsible for enforcing compliance by third parties to\nthis License.\n\n  7. If, as a consequence of a court judgment or allegation of patent\ninfringement or for any other reason (not limited to patent issues),\nconditions are imposed on you (whether by court order, agreement or\notherwise) that contradict the conditions of this License, they do not\nexcuse you from the conditions of this License.  If you cannot\ndistribute so as to satisfy simultaneously your obligations under this\nLicense and any other pertinent obligations, then as a consequence you\nmay not distribute the Program at all.  For example, if a patent\nlicense would not permit royalty-free redistribution of the Program by\nall those who receive copies directly or indirectly through you, then\nthe only way you could satisfy both it and this License would be to\nrefrain entirely from distribution of the Program.\n\nIf any portion of this section is held invalid or unenforceable under\nany particular circumstance, the balance of the section is intended to\napply and the section as a whole is intended to apply in other\ncircumstances.\n\nIt is not the purpose of this section to induce you to infringe any\npatents or other property right claims or to contest validity of any\nsuch claims; this section has the sole purpose of protecting the\nintegrity of the free software distribution system, which is\nimplemented by public license practices.  Many people have made\ngenerous contributions to the wide range of software distributed\nthrough that system in reliance on consistent application of that\nsystem; it is up to the author/donor to decide if he or she is willing\nto distribute software through any other system and a licensee cannot\nimpose that choice.\n\nThis section is intended to make thoroughly clear what is believed to\nbe a consequence of the rest of this License.\n\n  8. If the distribution and/or use of the Program is restricted in\ncertain countries either by patents or by copyrighted interfaces, the\noriginal copyright holder who places the Program under this License\nmay add an explicit geographical distribution limitation excluding\nthose countries, so that distribution is permitted only in or among\ncountries not thus excluded.  In such case, this License incorporates\nthe limitation as if written in the body of this License.\n\n  9. The Free Software Foundation may publish revised and/or new versions\nof the General Public License from time to time.  Such new versions will\nbe similar in spirit to the present version, but may differ in detail to\naddress new problems or concerns.\n\nEach version is given a distinguishing version number.  If the Program\nspecifies a version number of this License which applies to it and "any\nlater version", you have the option of following the terms and conditions\neither of that version or of any later version published by the Free\nSoftware Foundation.  If the Program does not specify a version number of\nthis License, you may choose any version ever published by the Free Software\nFoundation.\n\n  10. If you wish to incorporate parts of the Program into other free\nprograms whose distribution conditions are different, write to the author\nto ask for permission.  For software which is copyrighted by the Free\nSoftware Foundation, write to the Free Software Foundation; we sometimes\nmake exceptions for this.  Our decision will be guided by the two goals\nof preserving the free status of all derivatives of our free software and\nof promoting the sharing and reuse of software generally.\n\nNO WARRANTY\n-----------\n\n  11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY\nFOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN\nOTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES\nPROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED\nOR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF\nMERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS\nTO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE\nPROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,\nREPAIR OR CORRECTION.\n\n  12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING\nWILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR\nREDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,\nINCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING\nOUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED\nTO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY\nYOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER\nPROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE\nPOSSIBILITY OF SUCH DAMAGES.\n\n---------------------------\nEND OF TERMS AND CONDITIONS";s:6:"readme";s:804:"::::::::::::::::::::::::::::::::::::::::\n Snippet name: Wayfinder\n Short Desc: builds site navigation\n Version: 2.3.0 (Revolution compatible)\n Authors: \n    Kyle Jaebker (muddydogpaws.com)\n    Ryan Thrash (vertexworks.com)\n    Shaun McCormick (splittingred.com)\n ::::::::::::::::::::::::::::::::::::::::\nDescription:\n    Totally refactored from original DropMenu nav builder to make it easier to\n    create custom navigation by using chunks as output templates. By using templates,\n    many of the paramaters are no longer needed for flexible output including tables,\n    unordered- or ordered-lists (ULs or OLs), definition lists (DLs) or in any other\n    format you desire.\n::::::::::::::::::::::::::::::::::::::::\nExample Usage:\n    [[Wayfinder? &startId=`0`]]\n::::::::::::::::::::::::::::::::::::::::";s:9:"changelog";s:2655:"Changelog for Wayfinder (for Revolution).\n\nWayfinder 2.3.3\n====================================\n- [#40] Add wf.level placeholder to items for showing current depth\n- [#42] Allow authenticated mgr users with view_unpublished to use new previewUnpublished property to preview unpublished Resources in menus\n- [#41] Fix issue with Wayfinder and truncated result sets due to getIterator call\n\nWayfinder 2.3.2\n====================================\n- [#36] Fix issue with multiple Wayfinder calls using &config\n- [#35] Fix issues with TV bindings rendering\n- Add "protected" placeholder that is 1 if Resource is protected by a Resource Group\n- Updated documentation, snippet properties descriptions\n\nWayfinder 2.3.1\n====================================\n- [#31] Add &scheme property for specifying link schemes\n- [#27] Improve caching in Wayfinder to store cache files in resource cache so cache is synced with modx core caching\n\nWayfinder 2.3.0\n====================================\n- [#14] Fix issue with hideSubMenus when using it with a non-zero startId\n- Add all fields of a Resource to the rowTpl placeholder set, such as menutitle, published, etc\n- Properly optimize TV value grabbing to properly parse and cache TVs to improve load times when using TVs in a result set\n- Ensure that caching also caches by user ID to persist access permissions through cached result sets\n\nWayfinder 2.2.0\n====================================\n- [#23] Fix issue that generated error message in error.log due to &contexts always being processed regardless of empty state\n- [#21] Fix issue with unnecessary groupby that was breaking sorting in older mysql versions\n- [#22] Add &cacheResults parameter, which caches queries for faster loading\n- [#8] Add &contexts parameter, and &startIdContext parameter if navigating across multiple contexts and using a non-0 &startId\n\nWayfinder 2.1.3\n====================================\n- [#14] Fix hideSubMenus property\n- Add templates parameter that accepts a comma-delimited list of template IDs to filter by\n- Add where parameter that accepts a JSON object for where conditions\n- Add hereId parameter for specifying the active location\n\nWayfinder 2.1.2\n====================================\n- Fixed bug with includeDocs parameter\n\nWayfinder 2.1.1\n====================================\n- Wayfinder now properly uses MODx parsing system\n- Fixed issue with includeDocs statement\n- Fixed issues with PDO statements\n- Added the missing permissions check\n- Added wayfinder parameter "permissions" - default to "list", empty to bypass permissions check\n- [#WAYFINDER-20] TemplateVariables not rendering in Wayfinder templates.\n- Added changelog.";s:9:"signature";s:18:"wayfinder-2.3.3-pl";s:13:"initialConfig";s:15:"[object Object]";s:4:"text";s:20:"Продолжить";s:2:"id";s:19:"package-install-btn";s:6:"hidden";s:5:"false";s:7:"handler";s:1749:"function (va){\n        var r;\n        var g = Ext.getCmp(''modx-package-grid'');\n        if (!g) return false;\n        \n        if (va.signature != undefined && va.signature != '''') {\n            r = {signature: va.signature};\n        } else {\n            r = g.menu.record.data ? g.menu.record.data : g.menu.record;\n        }\n		var topic = ''/workspace/package/install/''+r.signature+''/'';\n        g.loadConsole(Ext.getBody(),topic);\n\n		va = va || {};\n        Ext.apply(va,{\n            action: ''workspace/packages/install''\n            ,signature: r.signature\n            ,register: ''mgr''\n            ,topic: topic\n        });\n\n        MODx.Ajax.request({\n            url: MODx.config.connector_url\n            ,params: va\n            ,listeners: {\n                ''success'': {fn:function() {\n                    var bc = Ext.getCmp(''packages-breadcrumbs'');\n                    var trail= bc.data.trail;\n                    trail.pop();\n\n                    if (trail.length > 1) {\n                        last = trail[trail.length - 1];\n\n                        if (last != undefined && last.rec != undefined) {\n                            bc.data.trail.pop();\n                            bc.data.trail.shift();\n                            bc.updateDetail(bc.data);\n\n                            var grid = Ext.getCmp(''modx-package-grid'');\n                            grid.install(last.rec);\n                            return;\n                        }\n                    }\n\n                    this.activate();\n					Ext.getCmp(''modx-package-grid'').refresh();\n                },scope:this}\n                ,''failure'': {fn:function() {\n                    this.activate();\n                },scope:this}\n            }\n        });\n\n        return true;\n	}";s:8:"disabled";s:5:"false";s:5:"scope";s:15:"[object Object]";s:8:"minWidth";s:2:"75";s:10:"removeMode";s:9:"container";s:10:"hideParent";s:4:"true";s:6:"events";s:15:"[object Object]";s:7:"ownerCt";s:15:"[object Object]";s:9:"container";s:15:"[object Object]";s:8:"rendered";s:4:"true";s:8:"template";s:15:"[object Object]";s:5:"btnEl";s:15:"[object Object]";s:4:"mons";s:15:"[object Object]";s:2:"el";s:15:"[object Object]";s:4:"icon";s:0:"";s:7:"iconCls";s:0:"";s:8:"boxReady";s:4:"true";s:8:"lastSize";s:15:"[object Object]";s:11:"useSetClass";s:4:"true";s:6:"oldCls";s:12:"x-btn-noicon";s:3:"doc";s:15:"[object Object]";s:19:"monitoringMouseOver";s:4:"true";s:6:"action";s:26:"workspace/packages/install";s:8:"register";s:3:"mgr";s:5:"topic";s:46:"/workspace/package/install/wayfinder-2.3.3-pl/";s:14:"package_action";i:0;}', 'Wayfinder', 'a:38:{s:2:"id";s:24:"4eaecb1ef24554127d0000b6";s:7:"package";s:24:"4d556be8b2b083396d0008bd";s:12:"display_name";s:18:"wayfinder-2.3.3-pl";s:4:"name";s:9:"Wayfinder";s:7:"version";s:5:"2.3.3";s:13:"version_major";s:1:"2";s:13:"version_minor";s:1:"3";s:13:"version_patch";s:1:"3";s:7:"release";s:2:"pl";s:8:"vrelease";s:2:"pl";s:14:"vrelease_index";s:1:"0";s:6:"author";s:12:"splittingred";s:11:"description";s:230:"<p>Wayfinder is a highly flexible navigation builder for MODx Revolution.</p><p>See the official docs here:&nbsp;<a href="http://rtfm.modx.com/display/ADDON/Wayfinder">http://rtfm.modx.com/display/ADDON/Wayfinder</a></p><ul>\n</ul>";s:12:"instructions";s:38:"<p>Install via Package Management.</p>";s:9:"changelog";s:2306:"<p style="padding-top: 2px; padding-right: 2px; padding-bottom: 2px; padding-left: 2px; "><b>New in 2.3.3</b></p><ul><li>&#91;#40&#93; Add wf.level placeholder to items for showing current depth</li><li>&#91;#42&#93; Allow authenticated mgr users with view_unpublished to use new previewUnpublished property to preview unpublished Resources in menus</li><li>&#91;#41&#93; Fix issue with Wayfinder and truncated result sets due to getIterator call</li></ul><p></p><p style="padding-top: 2px; padding-right: 2px; padding-bottom: 2px; padding-left: 2px; "><b>New in 2.3.2</b></p><ul><li>&#91;#36&#93; Fix issue with multiple Wayfinder calls using &amp;config</li><li>&#91;#35&#93; Fix issues with TV bindings rendering</li><li>Add "protected" placeholder that is 1 if Resource is protected by a Resource Group</li><li>Updated documentation, snippet properties descriptions</li></ul><p></p><p style="padding-top: 2px; padding-right: 2px; padding-bottom: 2px; padding-left: 2px; "><b>New in 2.3.1</b></p><ul><li>&#91;#31&#93; Add &amp;scheme property for specifying link schemes</li><li>&#91;#27&#93; Improve caching in Wayfinder to store cache files in resource cache so cache is synced with modx core caching</li></ul><p></p><p style="padding-top: 2px; padding-right: 2px; padding-bottom: 2px; padding-left: 2px; "><b>New in 2.3.0</b></p><ul><li>&#91;#14&#93; Fix issue with hideSubMenus when using it with a non-zero startId</li><li>Add all fields of a Resource to the rowTpl placeholder set, such as menutitle, published, etc</li><li>Properly optimize TV value grabbing to properly parse and cache TVs to improve load times when using TVs in a result set</li><li>Ensure that caching also caches by user ID to persist access permissions through cached result sets</li></ul><p><b>New in 2.2.0</b></p><ul><li>&#91;#23&#93; Fix issue that generated error message in error.log due to &amp;contexts always being processed regardless of empty state</li><li>&#91;#21&#93; Fix issue with unnecessary groupby that was breaking sorting in older mysql versions</li><li>&#91;#22&#93; Add &amp;cacheResults parameter, which caches queries for faster loading</li><li>&#91;#8&#93; Add &amp;contexts parameter, and &amp;startIdContext parameter if navigating across multiple contexts and using a non-0 &amp;startId</li></ul>";s:9:"createdon";s:24:"2011-10-31T16:21:50+0000";s:9:"createdby";s:12:"splittingred";s:8:"editedon";s:24:"2016-06-08T14:53:03+0000";s:10:"releasedon";s:24:"2011-10-31T16:21:50+0000";s:9:"downloads";s:6:"255993";s:8:"approved";s:4:"true";s:7:"audited";s:5:"false";s:8:"featured";s:4:"true";s:10:"deprecated";s:5:"false";s:7:"license";s:5:"GPLv2";s:7:"smf_url";s:0:"";s:10:"repository";s:24:"4d4c3fa6b2b0830da9000001";s:8:"supports";s:1:"2";s:8:"location";s:60:"http://modx.com/extras/download/?id=4eaecb20f24554127d0000b8";s:9:"signature";s:18:"wayfinder-2.3.3-pl";s:11:"supports_db";s:12:"mysql,sqlsrv";s:16:"minimum_supports";s:1:"2";s:9:"breaks_at";s:8:"10000000";s:10:"screenshot";s:0:"";s:4:"file";a:7:{s:2:"id";s:24:"4eaecb20f24554127d0000b8";s:7:"version";s:24:"4eaecb1ef24554127d0000b6";s:8:"filename";s:32:"wayfinder-2.3.3-pl.transport.zip";s:9:"downloads";s:6:"171337";s:6:"lastip";s:13:"91.227.16.117";s:9:"transport";s:4:"true";s:8:"location";s:60:"http://modx.com/extras/download/?id=4eaecb20f24554127d0000b8";}s:17:"package-signature";s:18:"wayfinder-2.3.3-pl";s:10:"categories";s:15:"menu,navigation";s:4:"tags";s:44:"menus,flyover,navigation,structure,menu,site";}', 2, 3, 3, 'pl', 0);
INSERT INTO `modx_transport_packages` (`signature`, `created`, `updated`, `installed`, `state`, `workspace`, `provider`, `disabled`, `source`, `manifest`, `attributes`, `package_name`, `metadata`, `version_major`, `version_minor`, `version_patch`, `release`, `release_index`) VALUES
('getpage-1.2.4-pl', '2016-06-09 04:26:39', '2016-06-09 13:27:59', '2016-06-09 16:27:59', 0, 1, 1, 0, 'getpage-1.2.4-pl.transport.zip', NULL, 'a:34:{s:7:"license";s:15218:"GNU GENERAL PUBLIC LICENSE\n   Version 2, June 1991\n--------------------------\n\nCopyright (C) 1989, 1991 Free Software Foundation, Inc.\n59 Temple Place, Suite 330, Boston, MA  02111-1307  USA\n\nEveryone is permitted to copy and distribute verbatim copies\nof this license document, but changing it is not allowed.\n\nPreamble\n--------\n\n  The licenses for most software are designed to take away your\nfreedom to share and change it.  By contrast, the GNU General Public\nLicense is intended to guarantee your freedom to share and change free\nsoftware--to make sure the software is free for all its users.  This\nGeneral Public License applies to most of the Free Software\nFoundation''s software and to any other program whose authors commit to\nusing it.  (Some other Free Software Foundation software is covered by\nthe GNU Library General Public License instead.)  You can apply it to\nyour programs, too.\n\n  When we speak of free software, we are referring to freedom, not\nprice.  Our General Public Licenses are designed to make sure that you\nhave the freedom to distribute copies of free software (and charge for\nthis service if you wish), that you receive source code or can get it\nif you want it, that you can change the software or use pieces of it\nin new free programs; and that you know you can do these things.\n\n  To protect your rights, we need to make restrictions that forbid\nanyone to deny you these rights or to ask you to surrender the rights.\nThese restrictions translate to certain responsibilities for you if you\ndistribute copies of the software, or if you modify it.\n\n  For example, if you distribute copies of such a program, whether\ngratis or for a fee, you must give the recipients all the rights that\nyou have.  You must make sure that they, too, receive or can get the\nsource code.  And you must show them these terms so they know their\nrights.\n\n  We protect your rights with two steps: (1) copyright the software, and\n(2) offer you this license which gives you legal permission to copy,\ndistribute and/or modify the software.\n\n  Also, for each author''s protection and ours, we want to make certain\nthat everyone understands that there is no warranty for this free\nsoftware.  If the software is modified by someone else and passed on, we\nwant its recipients to know that what they have is not the original, so\nthat any problems introduced by others will not reflect on the original\nauthors'' reputations.\n\n  Finally, any free program is threatened constantly by software\npatents.  We wish to avoid the danger that redistributors of a free\nprogram will individually obtain patent licenses, in effect making the\nprogram proprietary.  To prevent this, we have made it clear that any\npatent must be licensed for everyone''s free use or not licensed at all.\n\n  The precise terms and conditions for copying, distribution and\nmodification follow.\n\n\nGNU GENERAL PUBLIC LICENSE\nTERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION\n---------------------------------------------------------------\n\n  0. This License applies to any program or other work which contains\na notice placed by the copyright holder saying it may be distributed\nunder the terms of this General Public License.  The "Program", below,\nrefers to any such program or work, and a "work based on the Program"\nmeans either the Program or any derivative work under copyright law:\nthat is to say, a work containing the Program or a portion of it,\neither verbatim or with modifications and/or translated into another\nlanguage.  (Hereinafter, translation is included without limitation in\nthe term "modification".)  Each licensee is addressed as "you".\n\nActivities other than copying, distribution and modification are not\ncovered by this License; they are outside its scope.  The act of\nrunning the Program is not restricted, and the output from the Program\nis covered only if its contents constitute a work based on the\nProgram (independent of having been made by running the Program).\nWhether that is true depends on what the Program does.\n\n  1. You may copy and distribute verbatim copies of the Program''s\nsource code as you receive it, in any medium, provided that you\nconspicuously and appropriately publish on each copy an appropriate\ncopyright notice and disclaimer of warranty; keep intact all the\nnotices that refer to this License and to the absence of any warranty;\nand give any other recipients of the Program a copy of this License\nalong with the Program.\n\nYou may charge a fee for the physical act of transferring a copy, and\nyou may at your option offer warranty protection in exchange for a fee.\n\n  2. You may modify your copy or copies of the Program or any portion\nof it, thus forming a work based on the Program, and copy and\ndistribute such modifications or work under the terms of Section 1\nabove, provided that you also meet all of these conditions:\n\n    a) You must cause the modified files to carry prominent notices\n    stating that you changed the files and the date of any change.\n\n    b) You must cause any work that you distribute or publish, that in\n    whole or in part contains or is derived from the Program or any\n    part thereof, to be licensed as a whole at no charge to all third\n    parties under the terms of this License.\n\n    c) If the modified program normally reads commands interactively\n    when run, you must cause it, when started running for such\n    interactive use in the most ordinary way, to print or display an\n    announcement including an appropriate copyright notice and a\n    notice that there is no warranty (or else, saying that you provide\n    a warranty) and that users may redistribute the program under\n    these conditions, and telling the user how to view a copy of this\n    License.  (Exception: if the Program itself is interactive but\n    does not normally print such an announcement, your work based on\n    the Program is not required to print an announcement.)\n\nThese requirements apply to the modified work as a whole.  If\nidentifiable sections of that work are not derived from the Program,\nand can be reasonably considered independent and separate works in\nthemselves, then this License, and its terms, do not apply to those\nsections when you distribute them as separate works.  But when you\ndistribute the same sections as part of a whole which is a work based\non the Program, the distribution of the whole must be on the terms of\nthis License, whose permissions for other licensees extend to the\nentire whole, and thus to each and every part regardless of who wrote it.\n\nThus, it is not the intent of this section to claim rights or contest\nyour rights to work written entirely by you; rather, the intent is to\nexercise the right to control the distribution of derivative or\ncollective works based on the Program.\n\nIn addition, mere aggregation of another work not based on the Program\nwith the Program (or with a work based on the Program) on a volume of\na storage or distribution medium does not bring the other work under\nthe scope of this License.\n\n  3. You may copy and distribute the Program (or a work based on it,\nunder Section 2) in object code or executable form under the terms of\nSections 1 and 2 above provided that you also do one of the following:\n\n    a) Accompany it with the complete corresponding machine-readable\n    source code, which must be distributed under the terms of Sections\n    1 and 2 above on a medium customarily used for software interchange; or,\n\n    b) Accompany it with a written offer, valid for at least three\n    years, to give any third party, for a charge no more than your\n    cost of physically performing source distribution, a complete\n    machine-readable copy of the corresponding source code, to be\n    distributed under the terms of Sections 1 and 2 above on a medium\n    customarily used for software interchange; or,\n\n    c) Accompany it with the information you received as to the offer\n    to distribute corresponding source code.  (This alternative is\n    allowed only for noncommercial distribution and only if you\n    received the program in object code or executable form with such\n    an offer, in accord with Subsection b above.)\n\nThe source code for a work means the preferred form of the work for\nmaking modifications to it.  For an executable work, complete source\ncode means all the source code for all modules it contains, plus any\nassociated interface definition files, plus the scripts used to\ncontrol compilation and installation of the executable.  However, as a\nspecial exception, the source code distributed need not include\nanything that is normally distributed (in either source or binary\nform) with the major components (compiler, kernel, and so on) of the\noperating system on which the executable runs, unless that component\nitself accompanies the executable.\n\nIf distribution of executable or object code is made by offering\naccess to copy from a designated place, then offering equivalent\naccess to copy the source code from the same place counts as\ndistribution of the source code, even though third parties are not\ncompelled to copy the source along with the object code.\n\n  4. You may not copy, modify, sublicense, or distribute the Program\nexcept as expressly provided under this License.  Any attempt\notherwise to copy, modify, sublicense or distribute the Program is\nvoid, and will automatically terminate your rights under this License.\nHowever, parties who have received copies, or rights, from you under\nthis License will not have their licenses terminated so long as such\nparties remain in full compliance.\n\n  5. You are not required to accept this License, since you have not\nsigned it.  However, nothing else grants you permission to modify or\ndistribute the Program or its derivative works.  These actions are\nprohibited by law if you do not accept this License.  Therefore, by\nmodifying or distributing the Program (or any work based on the\nProgram), you indicate your acceptance of this License to do so, and\nall its terms and conditions for copying, distributing or modifying\nthe Program or works based on it.\n\n  6. Each time you redistribute the Program (or any work based on the\nProgram), the recipient automatically receives a license from the\noriginal licensor to copy, distribute or modify the Program subject to\nthese terms and conditions.  You may not impose any further\nrestrictions on the recipients'' exercise of the rights granted herein.\nYou are not responsible for enforcing compliance by third parties to\nthis License.\n\n  7. If, as a consequence of a court judgment or allegation of patent\ninfringement or for any other reason (not limited to patent issues),\nconditions are imposed on you (whether by court order, agreement or\notherwise) that contradict the conditions of this License, they do not\nexcuse you from the conditions of this License.  If you cannot\ndistribute so as to satisfy simultaneously your obligations under this\nLicense and any other pertinent obligations, then as a consequence you\nmay not distribute the Program at all.  For example, if a patent\nlicense would not permit royalty-free redistribution of the Program by\nall those who receive copies directly or indirectly through you, then\nthe only way you could satisfy both it and this License would be to\nrefrain entirely from distribution of the Program.\n\nIf any portion of this section is held invalid or unenforceable under\nany particular circumstance, the balance of the section is intended to\napply and the section as a whole is intended to apply in other\ncircumstances.\n\nIt is not the purpose of this section to induce you to infringe any\npatents or other property right claims or to contest validity of any\nsuch claims; this section has the sole purpose of protecting the\nintegrity of the free software distribution system, which is\nimplemented by public license practices.  Many people have made\ngenerous contributions to the wide range of software distributed\nthrough that system in reliance on consistent application of that\nsystem; it is up to the author/donor to decide if he or she is willing\nto distribute software through any other system and a licensee cannot\nimpose that choice.\n\nThis section is intended to make thoroughly clear what is believed to\nbe a consequence of the rest of this License.\n\n  8. If the distribution and/or use of the Program is restricted in\ncertain countries either by patents or by copyrighted interfaces, the\noriginal copyright holder who places the Program under this License\nmay add an explicit geographical distribution limitation excluding\nthose countries, so that distribution is permitted only in or among\ncountries not thus excluded.  In such case, this License incorporates\nthe limitation as if written in the body of this License.\n\n  9. The Free Software Foundation may publish revised and/or new versions\nof the General Public License from time to time.  Such new versions will\nbe similar in spirit to the present version, but may differ in detail to\naddress new problems or concerns.\n\nEach version is given a distinguishing version number.  If the Program\nspecifies a version number of this License which applies to it and "any\nlater version", you have the option of following the terms and conditions\neither of that version or of any later version published by the Free\nSoftware Foundation.  If the Program does not specify a version number of\nthis License, you may choose any version ever published by the Free Software\nFoundation.\n\n  10. If you wish to incorporate parts of the Program into other free\nprograms whose distribution conditions are different, write to the author\nto ask for permission.  For software which is copyrighted by the Free\nSoftware Foundation, write to the Free Software Foundation; we sometimes\nmake exceptions for this.  Our decision will be guided by the two goals\nof preserving the free status of all derivatives of our free software and\nof promoting the sharing and reuse of software generally.\n\nNO WARRANTY\n-----------\n\n  11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY\nFOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN\nOTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES\nPROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED\nOR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF\nMERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS\nTO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE\nPROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,\nREPAIR OR CORRECTION.\n\n  12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING\nWILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR\nREDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,\nINCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING\nOUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED\nTO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY\nYOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER\nPROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE\nPOSSIBILITY OF SUCH DAMAGES.\n\n---------------------------\nEND OF TERMS AND CONDITIONS";s:6:"readme";s:393:"--------------------\nSnippet: getPage\n--------------------\nVersion: 1.2.4-pl\nReleased: March 24, 2014\nSince: March 19, 2010\nAuthor: Jason Coward <jason@modx.com>\n\nA generic wrapper snippet for returning paged results and navigation from snippets that return limitable collections. This release requires MODX Revolution 2.1+.\n\nOfficial Documentation:\nhttp://rtfm.modx.com/display/ADDON/getPage\n";s:9:"changelog";s:1869:"Changelog for getPage.\n\ngetPage 1.2.4-pl (March 24, 2014)\n====================================\n- [#19] Add scriptProperties to cachePageKey signature\n- [#24] Cache total results number into correct variable name\n\ngetPage 1.2.3-pl (June 20, 2012)\n====================================\n- [#15] Add ability to specify pageNavScheme for navigation links\n- [#14] Avoid adding pageVarKey to query string when page = 1\n- [#12] Fix cacheKey when multi-dimensional arrays used in $_REQUEST\n\ngetPage 1.2.2-pl (December 9, 2011)\n====================================\n- Check $_GET before $_REQUEST for pageVarKey and limit overrides\n- [#6] Add pageOneLimit to allow different limit on first results page\n\ngetPage 1.2.1-pl (September 15, 2011)\n====================================\n- Remove inadvertent debugging statement in snippet code\n\ngetPage 1.2.0-pl (September 14, 2011)\n====================================\n- [#5] Add pageNavOuterTpl for layout control of page nav elements\n- Allow limit of 0 in $_REQUEST to bypass paging\n- Allow empty nav tpl parameters\n- Allow unlimited page listing with &pageLimit=`0`\n\ngetPage 1.1.0-pl (March 27, 2011)\n====================================\n- Change default cacheKey property to use ''resource'' (for MODX 2.1+)\n\ngetPage 1.0.0-pl (August 18, 2010)\n====================================\n- Add support for snippets that send output toPlaceholder\n- Add firstItem and lastItem properties/placeholders\n- Add pageActiveTpl to differentiate current page link\n\ngetPage 1.0.0-rc1 (June 29, 2010)\n====================================\n- Add per page caching capabilities\n- Add changelog, license, and readme\n\ngetPage 1.0.0-beta2 (May 28, 2010)\n====================================\n- Preserve additional query string parameters when creating paging navigation\n\ngetPage 1.0.0-beta1 (March 19, 2010)\n====================================\n- Initial release\n";s:9:"signature";s:16:"getpage-1.2.4-pl";s:13:"initialConfig";s:15:"[object Object]";s:4:"text";s:20:"Продолжить";s:2:"id";s:19:"package-install-btn";s:6:"hidden";s:5:"false";s:7:"handler";s:1749:"function (va){\n        var r;\n        var g = Ext.getCmp(''modx-package-grid'');\n        if (!g) return false;\n        \n        if (va.signature != undefined && va.signature != '''') {\n            r = {signature: va.signature};\n        } else {\n            r = g.menu.record.data ? g.menu.record.data : g.menu.record;\n        }\n		var topic = ''/workspace/package/install/''+r.signature+''/'';\n        g.loadConsole(Ext.getBody(),topic);\n\n		va = va || {};\n        Ext.apply(va,{\n            action: ''workspace/packages/install''\n            ,signature: r.signature\n            ,register: ''mgr''\n            ,topic: topic\n        });\n\n        MODx.Ajax.request({\n            url: MODx.config.connector_url\n            ,params: va\n            ,listeners: {\n                ''success'': {fn:function() {\n                    var bc = Ext.getCmp(''packages-breadcrumbs'');\n                    var trail= bc.data.trail;\n                    trail.pop();\n\n                    if (trail.length > 1) {\n                        last = trail[trail.length - 1];\n\n                        if (last != undefined && last.rec != undefined) {\n                            bc.data.trail.pop();\n                            bc.data.trail.shift();\n                            bc.updateDetail(bc.data);\n\n                            var grid = Ext.getCmp(''modx-package-grid'');\n                            grid.install(last.rec);\n                            return;\n                        }\n                    }\n\n                    this.activate();\n					Ext.getCmp(''modx-package-grid'').refresh();\n                },scope:this}\n                ,''failure'': {fn:function() {\n                    this.activate();\n                },scope:this}\n            }\n        });\n\n        return true;\n	}";s:8:"disabled";s:5:"false";s:5:"scope";s:15:"[object Object]";s:8:"minWidth";s:2:"75";s:10:"removeMode";s:9:"container";s:10:"hideParent";s:4:"true";s:6:"events";s:15:"[object Object]";s:7:"ownerCt";s:15:"[object Object]";s:9:"container";s:15:"[object Object]";s:8:"rendered";s:4:"true";s:8:"template";s:15:"[object Object]";s:5:"btnEl";s:15:"[object Object]";s:4:"mons";s:15:"[object Object]";s:2:"el";s:15:"[object Object]";s:4:"icon";s:0:"";s:7:"iconCls";s:0:"";s:8:"boxReady";s:4:"true";s:8:"lastSize";s:15:"[object Object]";s:11:"useSetClass";s:4:"true";s:6:"oldCls";s:12:"x-btn-noicon";s:3:"doc";s:15:"[object Object]";s:19:"monitoringMouseOver";s:4:"true";s:6:"action";s:26:"workspace/packages/install";s:8:"register";s:3:"mgr";s:5:"topic";s:44:"/workspace/package/install/getpage-1.2.4-pl/";s:14:"package_action";i:0;}', 'getPage', 'a:38:{s:2:"id";s:24:"5331082662cf24022e002475";s:7:"package";s:24:"4d556c77b2b083396d000c1b";s:12:"display_name";s:16:"getpage-1.2.4-pl";s:4:"name";s:7:"getPage";s:7:"version";s:5:"1.2.4";s:13:"version_major";s:1:"1";s:13:"version_minor";s:1:"2";s:13:"version_patch";s:1:"4";s:7:"release";s:2:"pl";s:8:"vrelease";s:2:"pl";s:14:"vrelease_index";s:0:"";s:6:"author";s:8:"opengeek";s:11:"description";s:390:"<p>A generic wrapper snippet for MODX Revolution 2.1+. getPage will returning paged results and navigation from snippets that return limitable collections, and optionally cache the results of unique page requests.</p><p>The release fixes a bug with caching when getPage is used multiple times on a single page and caches the total value into the proper totalVar variable name.</p><ul>\n</ul>";s:12:"instructions";s:90:"<p>Install via Package Management.\nNOTE: requires MODX Revolution-2.1.0-rc-1 or later.</p>";s:9:"changelog";s:1377:"<p></p><p style="margin-bottom: 15px; "></p><p><b>getPage 1.2.4-pl (March 24, 2014)</b><b></b></p><p><ul><li>&#91;#19&#93; Add scriptProperties to cachePageKey signature</li><li>&#91;#24&#93; Cache total results number into correct variable name</li></ul></p><p><b>getPage 1.2.3-pl (June 20, 2012)</b></p><p></p><p></p><ul><li>&#91;#15&#93; Add ability to specify pageNavScheme for navigation links</li><li>&#91;#14&#93; Avoid adding pageVarKey to query string when page = 1</li><li>&#91;#12&#93; Fix cacheKey when multi-dimensional arrays used in $_REQUEST</li></ul><p></p><p></p><p><b>getPage 1.2.2-pl (December 9, 2011)</b></p><p></p><ul><li>Check $_GET before $_REQUEST for pageVarKey and limit overrides</li><li>&#91;#6&#93; Add pageOneLimit to allow different limit on first results page</li></ul><p></p><p></p><p style="margin-bottom: 15px; "><b>getPage 1.2.1-pl (September 15, 2011)</b></p><p style="margin-bottom: 15px; "></p><ul><li>Remove inadvertent debugging statement in snippet code</li></ul><p></p><p style="margin-bottom: 15px; "><b>getPage 1.2.0-pl (September 14, 2011)</b></p><ul style="margin-bottom: 15px; "><li>&#91;#5&#93; Add pageNavOuterTpl for layout control of page nav elements</li><li>Allow limit of 0 in $_REQUEST to bypass paging</li><li>Allow empty nav tpl parameters</li><li>Allow unlimited page listing with &amp;pageLimit=`0`</li></ul><p></p>";s:9:"createdon";s:24:"2014-03-25T04:37:58+0000";s:9:"createdby";s:8:"opengeek";s:8:"editedon";s:24:"2016-06-09T13:01:13+0000";s:10:"releasedon";s:24:"2014-03-25T04:37:58+0000";s:9:"downloads";s:6:"101958";s:8:"approved";s:4:"true";s:7:"audited";s:5:"false";s:8:"featured";s:5:"false";s:10:"deprecated";s:5:"false";s:7:"license";s:5:"GPLv2";s:7:"smf_url";s:0:"";s:10:"repository";s:24:"4d4c3fa6b2b0830da9000001";s:8:"supports";s:3:"2.1";s:8:"location";s:60:"http://modx.com/extras/download/?id=5331082662cf24022e002477";s:9:"signature";s:16:"getpage-1.2.4-pl";s:11:"supports_db";s:12:"mysql,sqlsrv";s:16:"minimum_supports";s:3:"2.1";s:9:"breaks_at";s:8:"10000000";s:10:"screenshot";s:0:"";s:4:"file";a:7:{s:2:"id";s:24:"5331082662cf24022e002477";s:7:"version";s:24:"5331082662cf24022e002475";s:8:"filename";s:30:"getpage-1.2.4-pl.transport.zip";s:9:"downloads";s:5:"44776";s:6:"lastip";s:13:"80.69.164.169";s:9:"transport";s:4:"true";s:8:"location";s:60:"http://modx.com/extras/download/?id=5331082662cf24022e002477";}s:17:"package-signature";s:16:"getpage-1.2.4-pl";s:10:"categories";s:37:"navigation,blogging,content,utilities";s:4:"tags";s:0:"";}', 1, 2, 4, 'pl', 0);
INSERT INTO `modx_transport_packages` (`signature`, `created`, `updated`, `installed`, `state`, `workspace`, `provider`, `disabled`, `source`, `manifest`, `attributes`, `package_name`, `metadata`, `version_major`, `version_minor`, `version_patch`, `release`, `release_index`) VALUES
('gallery-1.7.0-pl', '2016-06-24 07:07:30', '2016-06-24 16:15:47', '2016-06-24 19:15:47', 0, 1, 1, 0, 'gallery-1.7.0-pl.transport.zip', NULL, 'a:35:{s:7:"license";s:15218:"GNU GENERAL PUBLIC LICENSE\n   Version 2, June 1991\n--------------------------\n\nCopyright (C) 1989, 1991 Free Software Foundation, Inc.\n59 Temple Place, Suite 330, Boston, MA  02111-1307  USA\n\nEveryone is permitted to copy and distribute verbatim copies\nof this license document, but changing it is not allowed.\n\nPreamble\n--------\n\n  The licenses for most software are designed to take away your\nfreedom to share and change it.  By contrast, the GNU General Public\nLicense is intended to guarantee your freedom to share and change free\nsoftware--to make sure the software is free for all its users.  This\nGeneral Public License applies to most of the Free Software\nFoundation''s software and to any other program whose authors commit to\nusing it.  (Some other Free Software Foundation software is covered by\nthe GNU Library General Public License instead.)  You can apply it to\nyour programs, too.\n\n  When we speak of free software, we are referring to freedom, not\nprice.  Our General Public Licenses are designed to make sure that you\nhave the freedom to distribute copies of free software (and charge for\nthis service if you wish), that you receive source code or can get it\nif you want it, that you can change the software or use pieces of it\nin new free programs; and that you know you can do these things.\n\n  To protect your rights, we need to make restrictions that forbid\nanyone to deny you these rights or to ask you to surrender the rights.\nThese restrictions translate to certain responsibilities for you if you\ndistribute copies of the software, or if you modify it.\n\n  For example, if you distribute copies of such a program, whether\ngratis or for a fee, you must give the recipients all the rights that\nyou have.  You must make sure that they, too, receive or can get the\nsource code.  And you must show them these terms so they know their\nrights.\n\n  We protect your rights with two steps: (1) copyright the software, and\n(2) offer you this license which gives you legal permission to copy,\ndistribute and/or modify the software.\n\n  Also, for each author''s protection and ours, we want to make certain\nthat everyone understands that there is no warranty for this free\nsoftware.  If the software is modified by someone else and passed on, we\nwant its recipients to know that what they have is not the original, so\nthat any problems introduced by others will not reflect on the original\nauthors'' reputations.\n\n  Finally, any free program is threatened constantly by software\npatents.  We wish to avoid the danger that redistributors of a free\nprogram will individually obtain patent licenses, in effect making the\nprogram proprietary.  To prevent this, we have made it clear that any\npatent must be licensed for everyone''s free use or not licensed at all.\n\n  The precise terms and conditions for copying, distribution and\nmodification follow.\n\n\nGNU GENERAL PUBLIC LICENSE\nTERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION\n---------------------------------------------------------------\n\n  0. This License applies to any program or other work which contains\na notice placed by the copyright holder saying it may be distributed\nunder the terms of this General Public License.  The "Program", below,\nrefers to any such program or work, and a "work based on the Program"\nmeans either the Program or any derivative work under copyright law:\nthat is to say, a work containing the Program or a portion of it,\neither verbatim or with modifications and/or translated into another\nlanguage.  (Hereinafter, translation is included without limitation in\nthe term "modification".)  Each licensee is addressed as "you".\n\nActivities other than copying, distribution and modification are not\ncovered by this License; they are outside its scope.  The act of\nrunning the Program is not restricted, and the output from the Program\nis covered only if its contents constitute a work based on the\nProgram (independent of having been made by running the Program).\nWhether that is true depends on what the Program does.\n\n  1. You may copy and distribute verbatim copies of the Program''s\nsource code as you receive it, in any medium, provided that you\nconspicuously and appropriately publish on each copy an appropriate\ncopyright notice and disclaimer of warranty; keep intact all the\nnotices that refer to this License and to the absence of any warranty;\nand give any other recipients of the Program a copy of this License\nalong with the Program.\n\nYou may charge a fee for the physical act of transferring a copy, and\nyou may at your option offer warranty protection in exchange for a fee.\n\n  2. You may modify your copy or copies of the Program or any portion\nof it, thus forming a work based on the Program, and copy and\ndistribute such modifications or work under the terms of Section 1\nabove, provided that you also meet all of these conditions:\n\n    a) You must cause the modified files to carry prominent notices\n    stating that you changed the files and the date of any change.\n\n    b) You must cause any work that you distribute or publish, that in\n    whole or in part contains or is derived from the Program or any\n    part thereof, to be licensed as a whole at no charge to all third\n    parties under the terms of this License.\n\n    c) If the modified program normally reads commands interactively\n    when run, you must cause it, when started running for such\n    interactive use in the most ordinary way, to print or display an\n    announcement including an appropriate copyright notice and a\n    notice that there is no warranty (or else, saying that you provide\n    a warranty) and that users may redistribute the program under\n    these conditions, and telling the user how to view a copy of this\n    License.  (Exception: if the Program itself is interactive but\n    does not normally print such an announcement, your work based on\n    the Program is not required to print an announcement.)\n\nThese requirements apply to the modified work as a whole.  If\nidentifiable sections of that work are not derived from the Program,\nand can be reasonably considered independent and separate works in\nthemselves, then this License, and its terms, do not apply to those\nsections when you distribute them as separate works.  But when you\ndistribute the same sections as part of a whole which is a work based\non the Program, the distribution of the whole must be on the terms of\nthis License, whose permissions for other licensees extend to the\nentire whole, and thus to each and every part regardless of who wrote it.\n\nThus, it is not the intent of this section to claim rights or contest\nyour rights to work written entirely by you; rather, the intent is to\nexercise the right to control the distribution of derivative or\ncollective works based on the Program.\n\nIn addition, mere aggregation of another work not based on the Program\nwith the Program (or with a work based on the Program) on a volume of\na storage or distribution medium does not bring the other work under\nthe scope of this License.\n\n  3. You may copy and distribute the Program (or a work based on it,\nunder Section 2) in object code or executable form under the terms of\nSections 1 and 2 above provided that you also do one of the following:\n\n    a) Accompany it with the complete corresponding machine-readable\n    source code, which must be distributed under the terms of Sections\n    1 and 2 above on a medium customarily used for software interchange; or,\n\n    b) Accompany it with a written offer, valid for at least three\n    years, to give any third party, for a charge no more than your\n    cost of physically performing source distribution, a complete\n    machine-readable copy of the corresponding source code, to be\n    distributed under the terms of Sections 1 and 2 above on a medium\n    customarily used for software interchange; or,\n\n    c) Accompany it with the information you received as to the offer\n    to distribute corresponding source code.  (This alternative is\n    allowed only for noncommercial distribution and only if you\n    received the program in object code or executable form with such\n    an offer, in accord with Subsection b above.)\n\nThe source code for a work means the preferred form of the work for\nmaking modifications to it.  For an executable work, complete source\ncode means all the source code for all modules it contains, plus any\nassociated interface definition files, plus the scripts used to\ncontrol compilation and installation of the executable.  However, as a\nspecial exception, the source code distributed need not include\nanything that is normally distributed (in either source or binary\nform) with the major components (compiler, kernel, and so on) of the\noperating system on which the executable runs, unless that component\nitself accompanies the executable.\n\nIf distribution of executable or object code is made by offering\naccess to copy from a designated place, then offering equivalent\naccess to copy the source code from the same place counts as\ndistribution of the source code, even though third parties are not\ncompelled to copy the source along with the object code.\n\n  4. You may not copy, modify, sublicense, or distribute the Program\nexcept as expressly provided under this License.  Any attempt\notherwise to copy, modify, sublicense or distribute the Program is\nvoid, and will automatically terminate your rights under this License.\nHowever, parties who have received copies, or rights, from you under\nthis License will not have their licenses terminated so long as such\nparties remain in full compliance.\n\n  5. You are not required to accept this License, since you have not\nsigned it.  However, nothing else grants you permission to modify or\ndistribute the Program or its derivative works.  These actions are\nprohibited by law if you do not accept this License.  Therefore, by\nmodifying or distributing the Program (or any work based on the\nProgram), you indicate your acceptance of this License to do so, and\nall its terms and conditions for copying, distributing or modifying\nthe Program or works based on it.\n\n  6. Each time you redistribute the Program (or any work based on the\nProgram), the recipient automatically receives a license from the\noriginal licensor to copy, distribute or modify the Program subject to\nthese terms and conditions.  You may not impose any further\nrestrictions on the recipients'' exercise of the rights granted herein.\nYou are not responsible for enforcing compliance by third parties to\nthis License.\n\n  7. If, as a consequence of a court judgment or allegation of patent\ninfringement or for any other reason (not limited to patent issues),\nconditions are imposed on you (whether by court order, agreement or\notherwise) that contradict the conditions of this License, they do not\nexcuse you from the conditions of this License.  If you cannot\ndistribute so as to satisfy simultaneously your obligations under this\nLicense and any other pertinent obligations, then as a consequence you\nmay not distribute the Program at all.  For example, if a patent\nlicense would not permit royalty-free redistribution of the Program by\nall those who receive copies directly or indirectly through you, then\nthe only way you could satisfy both it and this License would be to\nrefrain entirely from distribution of the Program.\n\nIf any portion of this section is held invalid or unenforceable under\nany particular circumstance, the balance of the section is intended to\napply and the section as a whole is intended to apply in other\ncircumstances.\n\nIt is not the purpose of this section to induce you to infringe any\npatents or other property right claims or to contest validity of any\nsuch claims; this section has the sole purpose of protecting the\nintegrity of the free software distribution system, which is\nimplemented by public license practices.  Many people have made\ngenerous contributions to the wide range of software distributed\nthrough that system in reliance on consistent application of that\nsystem; it is up to the author/donor to decide if he or she is willing\nto distribute software through any other system and a licensee cannot\nimpose that choice.\n\nThis section is intended to make thoroughly clear what is believed to\nbe a consequence of the rest of this License.\n\n  8. If the distribution and/or use of the Program is restricted in\ncertain countries either by patents or by copyrighted interfaces, the\noriginal copyright holder who places the Program under this License\nmay add an explicit geographical distribution limitation excluding\nthose countries, so that distribution is permitted only in or among\ncountries not thus excluded.  In such case, this License incorporates\nthe limitation as if written in the body of this License.\n\n  9. The Free Software Foundation may publish revised and/or new versions\nof the General Public License from time to time.  Such new versions will\nbe similar in spirit to the present version, but may differ in detail to\naddress new problems or concerns.\n\nEach version is given a distinguishing version number.  If the Program\nspecifies a version number of this License which applies to it and "any\nlater version", you have the option of following the terms and conditions\neither of that version or of any later version published by the Free\nSoftware Foundation.  If the Program does not specify a version number of\nthis License, you may choose any version ever published by the Free Software\nFoundation.\n\n  10. If you wish to incorporate parts of the Program into other free\nprograms whose distribution conditions are different, write to the author\nto ask for permission.  For software which is copyrighted by the Free\nSoftware Foundation, write to the Free Software Foundation; we sometimes\nmake exceptions for this.  Our decision will be guided by the two goals\nof preserving the free status of all derivatives of our free software and\nof promoting the sharing and reuse of software generally.\n\nNO WARRANTY\n-----------\n\n  11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY\nFOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN\nOTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES\nPROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED\nOR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF\nMERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS\nTO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE\nPROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,\nREPAIR OR CORRECTION.\n\n  12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING\nWILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR\nREDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,\nINCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING\nOUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED\nTO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY\nYOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER\nPROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE\nPOSSIBILITY OF SUCH DAMAGES.\n\n---------------------------\nEND OF TERMS AND CONDITIONS";s:6:"readme";s:292:"--------------------\n3PC: Gallery\n--------------------\nVersion: 0.1.0\nSince: June 4th, 2010\nAuthor: Shaun McCormick <shaun@modx.com>\n\n--------------------\n\nA Gallery system for your photos.\n\nFeel free to suggest ideas/improvements/bugs on GitHub:\nhttp://github.com/splittingred/Gallery/issues";s:9:"changelog";s:8983:"Changelog for Gallery.\n\nGallery 1.7.0\n====================================\n- Fixed showAll parameter in galAlbum\n- Fixed album tree icon\n- Fixed undefined index: Year\n- Updated phpthumb-processor\n- Add browser caching support to phpthumb processor\n- Move Ext.onReady into controllers\n- Add support for &thumbTpl_N tpl in Gallery snippet\n- More 2.3 Fixes\n- Modx 2.3 Compatibility Fixes\n\nGallery 1.6.1\n====================================\n - [#13][#12] Backwards Compatibility Fix\n\nGallery 1.6.0\n====================================\n- [#271] [GalleryAlbums] Added image_absolute placeholder\n- [#270] [GalleryAlbums] Added containerTpl (with navigation placeholders) and totalVar placeholder\n- [#266] Add support for selection of album cover. \n- [#259] Do not cache in getList() when $sort = RAND()\n- [#252] Database Optimization\n- [#248] Fix getPage support in Gallery snippet\n- [#246] Batch import sorts by filename.\n- [#235] show all childs and subchilds of selected parent album by new TV option\n- [#233] Fix property itemCls from being assigned activeCls''s value\n- [#226] German localization\n- [#151] Needed to show links to next/prev albums\n- [#91] Allow false boolean\n- [#258] Bug fix with random sorting when cached \n- Bug fixes\n\nGallery 1.5.3\n====================================\n- [#9] Fixes Access Denied issue when using Manager / Gallery\n\nGallery 1.5.2\n====================================\n- [#219] Ensure that GalleryAlbums custom TV marks resource edit form dirty and enables save button\n- [#214] Enable Gallery Album browsing via MODX.Browser window\n- [#217] Add default cover when using GalleryAlbums with cover displays and no item is in the album\n\nGallery 1.5.1\n====================================\n- Add DB caching to Gallery and GalleryAlbums snippets, reducing page load times\n- [#172] Add total number of items to galleryalbums album item when cover is shown\n- Add Slimbox2 as a front-end plugin\n- Add url to the item attributes for each item\n- Add parameter &checkForRequestAlbumVar to change parameter &parent when you want to show child albums of current album\n\nGallery 1.5.0\n====================================\n- [#205] Add per page box to paging bar in album items view\n- [#108] Improve drag/drop reorder of items in manager by supressing refresh\n- [#192] Fix issues with deactivating items, albums\n- [#198] Fix sort of albums due to urldecode issue\n- Better optimization of DB fields\n- [#197], [#188] Fix issues with upload and file names\n- [#208] Prevent sorting in drag/drop when target and source is the same\n- Gallery 1.5+ only supports MODX 2.2+ and greater\n- Add GalleryAlbums Media Source type\n- [#183], [#168] Fix absolute paths in Gallery settings that caused pain when moving sites\n- [#164] Trim item/album names on save\n\nGallery 1.4.0\n====================================\n- [#145] Add &gallerifficCss property for specifying an alternate CSS file for Galleriffic plugin\n- [#139] Fix issue with thumbnails and caching in certain environments\n- Fix issue with TinyMCE not being re-instantiated when opening the update item window after the first time.\n- [#141] Update ajaxupload to support IE / old-school image upload.\n- [#140] Add ability to clear successful / failed uploads in multi upload window\n\nGallery 1.3.1\n====================================\n- Add more styling to the multi-upload window and upload log.\n- [#141] Fix multi-upload compatibility with IE\n- [#140] Add ability to clear multi-upload log for successful / failed uploads.\n- Fix bug with TinyMCE not being reloaded on different windows.\n\nGallery 1.3.0\n====================================\n- [#137] Add multi-upload to Gallery albums\n- Add richtext (if TinyMCE installed) to Item/Album description page\n\nGallery 1.2.2\n====================================\n- [#92] Fix issue when uploading item with html description\n- [#124] Fix document root issue with watermarks\n- [#121] Add extra placeholders for containerTpl chunk\n- [#113] Fix name of thumbnails being connector.php\n\nGallery 1.2.1\n====================================\n- [#107] Fix issue with abstract method definition in galZipImport class\n- [#95] Fix issue where thumbFar property was incorrectly set as a boolean\n- [#112] Fix issue with images not respecting gallery.thumbs_prepend_site_url setting\n- [#104] Fix issue with Content Type on cached thumbnails\n\nGallery 1.2.0\n====================================\n- [#97] Change thumbnail logic to use phpthumbof style php code in connector, vastly improving load/thumbnailing speed, and properly caching images\n- [#76], [#94], Zip upload now accepts zip files with just files inside\n- [#102] Fix issue with Gallery and Revolution 2.1.1+\n- [#80] Added GalleryAlbumList custom TV, for selecting Albums in a TV\n- [#82] Fix bug where creating Album did not respect parent\n- [#85] Add random sorting to albums in GalleryAlbums\n\nGallery 1.1.1\n====================================\n- Fix trivial issue with js console error when saving albums\n\nGallery 1.1.0\n====================================\n- [#73] Fix issue with absoluteImage galItem issue and gallery.thumbs_prepend_site_url setting\n- Fix issue where marking gallery active/prominent did not stick\n- [#28], [#64] Gallery items are now stored by albumId/itemId.ext in their directories\n- Added showName for GalleryAlbums to toggle whether or not to display Album name\n- [#29], [#69] Add batch upload via Zip file to albums\n- [#42] Add URL field to galItem object for allowing Items to have URLs that load when you click on them\n- [#46] Add imageProperties, thumbProperties properties to all snippets that allow you to pass a JSON object of properties to pass to phpThumb\n- Add rowCls property to GalleryAlbums\n- [#60] Fix issue with galPhpThumb and image passthru\n- [#67] Fix prev lexicon placeholder in Galleriffic plugin\n- [#66] Fix issue with album cover xPDO call in GalleryAlbums snippet\n- Add total property to Gallery snippet for getPage support\n- i18n of Snippet properties\n\nGallery 1.0.2\n====================================\n- [#31] Add &gallerifficUseCss parameter for toggling CSS by Galleriffic\n- Add cover ability to GalleryAlbums by using rowTpl "galAlbumRowWithCoverTpl"\n- Fix issue where Gallery TV albums tree loaded page when clicking on a node\n\nGallery 1.0.1 (December 20th, 2010)\n====================================\n- [#54] Added filepath placeholder for getting absolute path of an image, fileurl for absolute URL\n- [#12] Make left-click do update-album in mgr tree\n- Add additional system setting checks for phpthumb config options.\n- *Many* caching improvements to phpthumb-generated thumbnails\n- Added gallery.thumbs_prepend_site_url setting for environments having issues with thumbnails\n- Fixed bugs with HTML in item thumbs, urls when xhtml_urls is true\n- Added French translation, github readme\n\nGallery 1.0.0\n=============\n- Added checks to prevent spaces in filenames\n- Fixed ampersand issue in gallery URLs\n- Fixed phpthumb cache issue by using core phpthumb\n- Fixed port issue on thumbs\n- Added updated Russian translation\n- Added cropping support to custom TV, fixed bugs\n- Added rotating, watermark and other options to custom TV\n- Added a few settings for customizing backend mgr UX, including default batch upload path\n- Fixed bug with sorting items in album\n- Added Gallery custom TV input, output and properties types\n- Adjusted code to allow batch upload to handle uppercase extensions\n- Updated copyright information\n\nGallery 0.1.2\n====================================\n- Fixed bug with request parameter generation in URLs\n- Abstracted SQL in snippets\n- Added in Russian translation\n\nGallery 0.1.1\n====================================\n- Added "image_absolute" placeholder to thumbTpl in Gallery snippet\n- Fixed possible issues with phpthumb and caching\n\nGallery 0.1.0\n====================================\n- Added in batch upload\n- Fixed bug with limit statement in Gallery snippet\n- Made it so Gallery snippet can be called without pointing to anything\n- More properly integrated system, so that albums can easily be toggled between\n- Added GalleryItem snippet\n- Added parameters to adjust the get params for how albums are selected\n- Fixed bug with setting inactive state on images\n- Optimizing for Revo RC-2\n- Fixed bug that occurs if Gallery is used where FURLs are off\n- Added parent support to galAlbums, GalleryAlbums snippet\n- Fixed drag/drop reordering capability in albums\n- Fixed bug with gallery tables prefix\n- Fixed bug in items view related to recent SVN changes\n- Fixed bug in Gallery class that prohibited using tpls (!)\n- Added copyright info to Gallery\n- Fixed various bugs found in test install\n- Setup build script for initial alpha release\n- Added phpThumb to assets directory\n- Added active indicators, name shortening to album update mgr ui\n- Added album remove processor\n- i18ned the code, filled out mgr UI functionality\n- Added check when removing album to delete items not in any other albums\n- Lots of dev work, got basics mostly functional\n- Initial commit";s:13:"setup-options";s:34:"gallery-1.7.0-pl/setup-options.php";s:9:"signature";s:16:"gallery-1.7.0-pl";s:13:"initialConfig";s:15:"[object Object]";s:4:"text";s:20:"Продолжить";s:2:"id";s:19:"package-install-btn";s:6:"hidden";s:5:"false";s:7:"handler";s:1749:"function (va){\n        var r;\n        var g = Ext.getCmp(''modx-package-grid'');\n        if (!g) return false;\n        \n        if (va.signature != undefined && va.signature != '''') {\n            r = {signature: va.signature};\n        } else {\n            r = g.menu.record.data ? g.menu.record.data : g.menu.record;\n        }\n		var topic = ''/workspace/package/install/''+r.signature+''/'';\n        g.loadConsole(Ext.getBody(),topic);\n\n		va = va || {};\n        Ext.apply(va,{\n            action: ''workspace/packages/install''\n            ,signature: r.signature\n            ,register: ''mgr''\n            ,topic: topic\n        });\n\n        MODx.Ajax.request({\n            url: MODx.config.connector_url\n            ,params: va\n            ,listeners: {\n                ''success'': {fn:function() {\n                    var bc = Ext.getCmp(''packages-breadcrumbs'');\n                    var trail= bc.data.trail;\n                    trail.pop();\n\n                    if (trail.length > 1) {\n                        last = trail[trail.length - 1];\n\n                        if (last != undefined && last.rec != undefined) {\n                            bc.data.trail.pop();\n                            bc.data.trail.shift();\n                            bc.updateDetail(bc.data);\n\n                            var grid = Ext.getCmp(''modx-package-grid'');\n                            grid.install(last.rec);\n                            return;\n                        }\n                    }\n\n                    this.activate();\n					Ext.getCmp(''modx-package-grid'').refresh();\n                },scope:this}\n                ,''failure'': {fn:function() {\n                    this.activate();\n                },scope:this}\n            }\n        });\n\n        return true;\n	}";s:8:"disabled";s:5:"false";s:5:"scope";s:15:"[object Object]";s:8:"minWidth";s:2:"75";s:10:"removeMode";s:9:"container";s:10:"hideParent";s:4:"true";s:6:"events";s:15:"[object Object]";s:7:"ownerCt";s:15:"[object Object]";s:9:"container";s:15:"[object Object]";s:8:"rendered";s:4:"true";s:8:"template";s:15:"[object Object]";s:5:"btnEl";s:15:"[object Object]";s:4:"mons";s:15:"[object Object]";s:2:"el";s:15:"[object Object]";s:4:"icon";s:0:"";s:7:"iconCls";s:0:"";s:8:"boxReady";s:4:"true";s:8:"lastSize";s:15:"[object Object]";s:11:"useSetClass";s:4:"true";s:6:"oldCls";s:12:"x-btn-noicon";s:3:"doc";s:15:"[object Object]";s:19:"monitoringMouseOver";s:4:"true";s:6:"action";s:26:"workspace/packages/install";s:8:"register";s:3:"mgr";s:5:"topic";s:44:"/workspace/package/install/gallery-1.7.0-pl/";s:14:"package_action";i:0;}', 'Gallery', 'a:38:{s:2:"id";s:24:"5410194462cf2432d000b5c5";s:7:"package";s:24:"4d556c94b2b083396d000cc9";s:12:"display_name";s:16:"gallery-1.7.0-pl";s:4:"name";s:7:"Gallery";s:7:"version";s:5:"1.7.0";s:13:"version_major";s:1:"1";s:13:"version_minor";s:1:"7";s:13:"version_patch";s:1:"0";s:7:"release";s:2:"pl";s:8:"vrelease";s:2:"pl";s:14:"vrelease_index";s:0:"";s:6:"author";s:8:"theboxer";s:11:"description";s:365:"<p>Gallery is a dynamic Gallery Extra for MODx Revolution. It allows you to \nquickly and easily put up galleries of images, sort them, tag them, and \ndisplay them in a myriad of ways in the front-end of your site.</p>\n\n<p>See the official documentation here:&nbsp;<a href="http://rtfm.modx.com/display/addon/Gallery">http://rtfm.modx.com/extras/revo/gallery</a></p>";s:12:"instructions";s:625:"<p>Install via Package Management.</p><p><b>Note:</b> Make sure to backup your Gallery files in the location in the ''gallery.files_path'' System Setting before upgrading, as this update moves gallery file storage to an album-centric storage method.</p><p><b>Note</b>: Input Options for the GalleryAlbumList TV type only work for Revolution 2.1.0-rc2 and later.</p><p><b>Note</b>: When moving servers with a Gallery installation, you may need to edit these System Settings:</p><ul><li>gallery.core_path</li><li>gallery.assets_path</li><li>gallery.files_path</li><li>gallery.assets_url</li><li>gallery.files_url</li></ul><p></p>";s:9:"changelog";s:6780:"<p><b>New in 1.7.0</b></p><ul><li>Fixed showAll parameter in galAlbum</li><li>Fixed album tree icon</li><li>Fixed undefined index: Year</li><li>Updated phpthumb-processor</li><li>Add browser caching support to phpthumb processor</li><li>Move Ext.onReady into controllers</li><li>Add support for &amp;thumbTpl_N tpl in Gallery snippet</li><li>More 2.3 Fixes</li><li>Modx 2.3 Compatibility Fixes</li></ul><p><b>New in 1.6.1</b></p><p></p><ul><li>&#91;#13&#93;&#91;#12&#93; Backwards Compatibility Fix</li></ul><p></p><p><b>New in 1.6.0</b></p><p></p><ul><li>&#91;#271&#93; &#91;GalleryAlbums&#93; Added image_absolute placeholder</li><li>&#91;#270&#93; &#91;GalleryAlbums&#93; Added containerTpl (with navigation placeholders) and totalVar placeholder</li><li>&#91;#266&#93; Add support for selection of album cover.&nbsp;</li><li>&#91;#259&#93; Do not cache in getList() when $sort = RAND()</li><li>&#91;#252&#93; Database Optimization</li><li>&#91;#248&#93; Fix getPage support in Gallery snippet</li><li>&#91;#246&#93; Batch import sorts by filename.</li><li>&#91;#235&#93; show all childs and subchilds of selected parent album by new TV option</li><li>&#91;#233&#93; Fix property itemCls from being assigned activeCls''s value</li><li>&#91;#226&#93; German localization</li><li>&#91;#151&#93; Needed to show links to next/prev albums</li><li>&#91;#91&#93; Allow false boolean</li><li>&#91;#258&#93; Bug fix with random sorting when cached&nbsp;</li><li>Bug fixes</li></ul><p></p><p></p><p><b>New in 1.5.3</b></p><p></p><ul><li>&#91;#9&#93; Fixes Access Denied issue when using Manager / Gallery</li></ul><p></p><p><b>New in 1.5.2</b></p><p></p><ul><li>&#91;#219&#93; Ensure that GalleryAlbums custom TV marks resource edit form dirty and enables save button</li><li>&#91;#214&#93; Enable Gallery Album browsing via MODX.Browser window</li><li>&#91;#217&#93; Add default cover when using GalleryAlbums with cover displays and no item is in the album</li></ul><p></p><p><b>New in 1.5.1</b></p><p></p><ul><li>Add DB caching to Gallery and GalleryAlbums snippets, reducing page load times</li><li>&#91;#172&#93; Add total number of items to galleryalbums album item when cover is shown</li><li>Add Slimbox2 as a front-end plugin</li><li>Add url to the item attributes for each item</li><li>Add parameter &amp;checkForRequestAlbumVar to change parameter &amp;parent when you want to show child albums of current album</li></ul><p></p><p><b>New in 1.5.0</b></p><p></p><ul><li>&#91;#205&#93; Add per page box to paging bar in album items view</li><li>&#91;#108&#93; Improve drag/drop reorder of items in manager by supressing refresh</li><li>&#91;#192&#93; Fix issues with deactivating items, albums</li><li>&#91;#198&#93; Fix sort of albums due to urldecode issue</li><li>Better optimization of DB fields</li><li>&#91;#197&#93;, &#91;#188&#93; Fix issues with upload and file names</li><li>&#91;#208&#93; Prevent sorting in drag/drop when target and source is the same</li><li>Gallery 1.5+ only supports MODX 2.2+ and greater</li><li>Add GalleryAlbums Media Source type</li><li>&#91;#183&#93;, &#91;#168&#93; Fix absolute paths in Gallery settings that caused pain when moving sites</li><li>&#91;#164&#93; Trim item/album names on save</li></ul><p></p><p><b>New in 1.4.0</b></p><p></p><ul><li>&#91;#145&#93; Add &amp;gallerifficCss property for specifying an alternate CSS file for Galleriffic plugin</li><li>&#91;#139&#93; Fix issue with thumbnails and caching in certain environments</li><li>Fix issue with TinyMCE not being re-instantiated when opening the update item window after the first time.</li><li>&#91;#141&#93; Update ajaxupload to support IE / old-school image upload.</li><li>&#91;#140&#93; Add ability to clear successful / failed uploads in multi upload window</li></ul><p></p><p><b>New in 1.3.0</b></p><p></p><ul><li>&#91;#137&#93; Add multi-upload to Gallery albums</li><li>Add richtext (if TinyMCE installed) to Item/Album description page</li></ul><p></p><p><b>New in 1.2.2</b></p><p></p><ul><li>&#91;#92&#93; Fix issue when uploading item with html description</li><li>&#91;#124&#93; Fix document root issue with watermarks</li><li>&#91;#121&#93; Add extra placeholders for containerTpl chunk</li><li>&#91;#113&#93; Fix name of thumbnails being connector.php</li></ul><p></p><p><b>New in 1.2.1</b></p><p></p><ul><li>&#91;#107&#93; Fix issue with abstract method definition in galZipImport class</li><li>&#91;#95&#93; Fix issue where thumbFar property was incorrectly set as a boolean</li><li>&#91;#112&#93; Fix issue with images not respecting gallery.thumbs_prepend_site_url setting</li><li>&#91;#104&#93; Fix issue with Content Type on cached thumbnails</li></ul><p></p><p><b>New in 1.2.0</b></p><ul><li>&#91;#97&#93; Change thumbnail logic to use phpthumbof style php code in connector, vastly improving load/thumbnailing speed, and properly caching images</li><li>&#91;#76&#93;, &#91;#94&#93;, Zip upload now accepts zip files with just files inside</li><li>&#91;#102&#93; Fix issue with Gallery thumbnails and Revolution 2.1.1+</li><li>&#91;#80&#93; Added GalleryAlbumList custom TV, for selecting Albums in a TV</li><li>&#91;#82&#93; Fix bug where creating Album did not respect parent</li><li>&#91;#85&#93; Add random sorting to albums in GalleryAlbums</li></ul><p></p><p><b>New in 1.1.0</b></p><ul><li>&#91;#73&#93; Fix issue with absoluteImage galItem issue and gallery.thumbs_prepend_site_url setting</li><li>Fix issue where marking gallery active/prominent did not stick</li><li>&#91;#28&#93;, &#91;#64&#93; Gallery items are now stored by albumId/itemId.ext in their directories</li><li>&#91;#29&#93;, &#91;#69&#93; Add batch upload via Zip file to albums</li><li>&#91;#42&#93; Add URL field to galItem object for allowing Items to have URLs that load when you click on them</li><li>&#91;#46&#93; Add imageProperties, thumbProperties properties to all snippets that allow you to pass a JSON object of properties to pass to phpThumb</li><li>Improve caching of thumbnails on front-end</li><li>Added showName for GalleryAlbums to toggle whether or not to display Album name</li><li>Add rowCls property to GalleryAlbums</li><li>&#91;#60&#93; Fix issue with galPhpThumb and image passthru</li><li>&#91;#67&#93; Fix prev lexicon placeholder in Galleriffic plugin</li><li>&#91;#66&#93; Fix issue with album cover xPDO call in GalleryAlbums snippet</li><li>Add total property to Gallery snippet for getPage support</li><li>i18n of Snippet properties</li></ul><p><b>New in 1.0.2</b></p><ul><li>Add cover ability to GalleryAlbums by using &amp;rowTpl "galAlbumRowWithCoverTpl"</li><li>&#91;#31&#93; Add &amp;gallerifficUseCss parameter for toggling CSS by Galleriffic</li><li>Fix issue where Gallery TV albums tree loaded page when clicking on a node</li></ul>";s:9:"createdon";s:24:"2014-09-10T09:26:28+0000";s:9:"createdby";s:8:"theboxer";s:8:"editedon";s:24:"2016-06-24T15:24:04+0000";s:10:"releasedon";s:24:"2014-09-10T04:26:00+0000";s:9:"downloads";s:6:"125941";s:8:"approved";s:4:"true";s:7:"audited";s:4:"true";s:8:"featured";s:4:"true";s:10:"deprecated";s:5:"false";s:7:"license";s:5:"GPLv2";s:7:"smf_url";s:0:"";s:10:"repository";s:24:"4d4c3fa6b2b0830da9000001";s:8:"supports";s:1:"2";s:8:"location";s:60:"http://modx.com/extras/download/?id=5410194662cf2432d000b5c7";s:9:"signature";s:16:"gallery-1.7.0-pl";s:11:"supports_db";s:5:"mysql";s:16:"minimum_supports";s:1:"2";s:9:"breaks_at";s:8:"10000000";s:10:"screenshot";s:59:"http://modx.s3.amazonaws.com/extras/642/gallery-backend.png";s:4:"file";a:7:{s:2:"id";s:24:"5410194662cf2432d000b5c7";s:7:"version";s:24:"5410194462cf2432d000b5c5";s:8:"filename";s:30:"gallery-1.7.0-pl.transport.zip";s:9:"downloads";s:5:"32127";s:6:"lastip";s:13:"5.101.112.195";s:9:"transport";s:4:"true";s:8:"location";s:60:"http://modx.com/extras/download/?id=5410194662cf2432d000b5c7";}s:17:"package-signature";s:16:"gallery-1.7.0-pl";s:10:"categories";s:7:"gallery";s:4:"tags";s:33:"gallery,image,images,photo,photos";}', 1, 7, 0, 'pl', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `modx_transport_providers`
--

CREATE TABLE `modx_transport_providers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` mediumtext,
  `service_url` tinytext,
  `username` varchar(255) NOT NULL DEFAULT '',
  `api_key` varchar(255) NOT NULL DEFAULT '',
  `created` datetime NOT NULL,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `priority` tinyint(4) NOT NULL DEFAULT '10',
  `properties` mediumtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_transport_providers`
--

INSERT INTO `modx_transport_providers` (`id`, `name`, `description`, `service_url`, `username`, `api_key`, `created`, `updated`, `active`, `priority`, `properties`) VALUES
(1, 'modx.com', 'The official MODX transport facility for 3rd party components.', 'http://rest.modx.com/extras/', '', '', '2016-04-21 09:06:18', NULL, 1, 10, '');

-- --------------------------------------------------------

--
-- Структура таблицы `modx_users`
--

CREATE TABLE `modx_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(100) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  `cachepwd` varchar(100) NOT NULL DEFAULT '',
  `class_key` varchar(100) NOT NULL DEFAULT 'modUser',
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `remote_key` varchar(255) DEFAULT NULL,
  `remote_data` text,
  `hash_class` varchar(100) NOT NULL DEFAULT 'hashing.modPBKDF2',
  `salt` varchar(100) NOT NULL DEFAULT '',
  `primary_group` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `session_stale` text,
  `sudo` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `createdon` int(20) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_users`
--

INSERT INTO `modx_users` (`id`, `username`, `password`, `cachepwd`, `class_key`, `active`, `remote_key`, `remote_data`, `hash_class`, `salt`, `primary_group`, `session_stale`, `sudo`, `createdon`) VALUES
(1, 'admin', '9bZ3r53ND+rzeowGevoMo79xwa1zZYDtNluerByAGMQ=', '', 'modUser', 1, NULL, NULL, 'hashing.modPBKDF2', '43ae9dbe05ea097d451cde9d68417724', 1, NULL, 1, 1465381589);

-- --------------------------------------------------------

--
-- Структура таблицы `modx_user_attributes`
--

CREATE TABLE `modx_user_attributes` (
  `id` int(10) UNSIGNED NOT NULL,
  `internalKey` int(10) NOT NULL,
  `fullname` varchar(100) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `phone` varchar(100) NOT NULL DEFAULT '',
  `mobilephone` varchar(100) NOT NULL DEFAULT '',
  `blocked` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `blockeduntil` int(11) NOT NULL DEFAULT '0',
  `blockedafter` int(11) NOT NULL DEFAULT '0',
  `logincount` int(11) NOT NULL DEFAULT '0',
  `lastlogin` int(11) NOT NULL DEFAULT '0',
  `thislogin` int(11) NOT NULL DEFAULT '0',
  `failedlogincount` int(10) NOT NULL DEFAULT '0',
  `sessionid` varchar(100) NOT NULL DEFAULT '',
  `dob` int(10) NOT NULL DEFAULT '0',
  `gender` int(1) NOT NULL DEFAULT '0',
  `address` text NOT NULL,
  `country` varchar(255) NOT NULL DEFAULT '',
  `city` varchar(255) NOT NULL DEFAULT '',
  `state` varchar(25) NOT NULL DEFAULT '',
  `zip` varchar(25) NOT NULL DEFAULT '',
  `fax` varchar(100) NOT NULL DEFAULT '',
  `photo` varchar(255) NOT NULL DEFAULT '',
  `comment` text NOT NULL,
  `website` varchar(255) NOT NULL DEFAULT '',
  `extended` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_user_attributes`
--

INSERT INTO `modx_user_attributes` (`id`, `internalKey`, `fullname`, `email`, `phone`, `mobilephone`, `blocked`, `blockeduntil`, `blockedafter`, `logincount`, `lastlogin`, `thislogin`, `failedlogincount`, `sessionid`, `dob`, `gender`, `address`, `country`, `city`, `state`, `zip`, `fax`, `photo`, `comment`, `website`, `extended`) VALUES
(1, 1, 'Администратор по умолчанию', 'samodurov.ivan@gmail.com', '', '', 0, 0, 0, 5, 1466762051, 1467451551, 0, 'orf31kgu5ava4f15d1s4p2q3j1', 0, 0, '', '', '', '', '', '', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `modx_user_group_roles`
--

CREATE TABLE `modx_user_group_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` mediumtext,
  `authority` int(10) UNSIGNED NOT NULL DEFAULT '9999'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_user_group_roles`
--

INSERT INTO `modx_user_group_roles` (`id`, `name`, `description`, `authority`) VALUES
(1, 'Member', NULL, 9999),
(2, 'Super User', NULL, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `modx_user_group_settings`
--

CREATE TABLE `modx_user_group_settings` (
  `group` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `key` varchar(50) NOT NULL,
  `value` text,
  `xtype` varchar(75) NOT NULL DEFAULT 'textfield',
  `namespace` varchar(40) NOT NULL DEFAULT 'core',
  `area` varchar(255) NOT NULL DEFAULT '',
  `editedon` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `modx_user_messages`
--

CREATE TABLE `modx_user_messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(15) NOT NULL DEFAULT '',
  `subject` varchar(255) NOT NULL DEFAULT '',
  `message` text NOT NULL,
  `sender` int(10) NOT NULL DEFAULT '0',
  `recipient` int(10) NOT NULL DEFAULT '0',
  `private` tinyint(4) NOT NULL DEFAULT '0',
  `date_sent` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `read` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `modx_user_settings`
--

CREATE TABLE `modx_user_settings` (
  `user` int(11) NOT NULL DEFAULT '0',
  `key` varchar(50) NOT NULL DEFAULT '',
  `value` text,
  `xtype` varchar(75) NOT NULL DEFAULT 'textfield',
  `namespace` varchar(40) NOT NULL DEFAULT 'core',
  `area` varchar(255) NOT NULL DEFAULT '',
  `editedon` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `modx_workspaces`
--

CREATE TABLE `modx_workspaces` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `path` varchar(255) NOT NULL DEFAULT '',
  `created` datetime NOT NULL,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `attributes` mediumtext
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `modx_workspaces`
--

INSERT INTO `modx_workspaces` (`id`, `name`, `path`, `created`, `active`, `attributes`) VALUES
(1, 'Default MODX workspace', '{core_path}', '2016-06-08 13:26:27', 1, NULL);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `modx_access_actiondom`
--
ALTER TABLE `modx_access_actiondom`
  ADD PRIMARY KEY (`id`),
  ADD KEY `target` (`target`),
  ADD KEY `principal_class` (`principal_class`),
  ADD KEY `principal` (`principal`),
  ADD KEY `authority` (`authority`),
  ADD KEY `policy` (`policy`);

--
-- Индексы таблицы `modx_access_actions`
--
ALTER TABLE `modx_access_actions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `target` (`target`),
  ADD KEY `principal_class` (`principal_class`),
  ADD KEY `principal` (`principal`),
  ADD KEY `authority` (`authority`),
  ADD KEY `policy` (`policy`);

--
-- Индексы таблицы `modx_access_category`
--
ALTER TABLE `modx_access_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `target` (`target`),
  ADD KEY `principal_class` (`principal_class`),
  ADD KEY `principal` (`principal`),
  ADD KEY `authority` (`authority`),
  ADD KEY `policy` (`policy`),
  ADD KEY `context_key` (`context_key`);

--
-- Индексы таблицы `modx_access_context`
--
ALTER TABLE `modx_access_context`
  ADD PRIMARY KEY (`id`),
  ADD KEY `target` (`target`),
  ADD KEY `principal_class` (`principal_class`),
  ADD KEY `principal` (`principal`),
  ADD KEY `authority` (`authority`),
  ADD KEY `policy` (`policy`);

--
-- Индексы таблицы `modx_access_elements`
--
ALTER TABLE `modx_access_elements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `target` (`target`),
  ADD KEY `principal_class` (`principal_class`),
  ADD KEY `principal` (`principal`),
  ADD KEY `authority` (`authority`),
  ADD KEY `policy` (`policy`),
  ADD KEY `context_key` (`context_key`);

--
-- Индексы таблицы `modx_access_media_source`
--
ALTER TABLE `modx_access_media_source`
  ADD PRIMARY KEY (`id`),
  ADD KEY `target` (`target`),
  ADD KEY `principal_class` (`principal_class`),
  ADD KEY `principal` (`principal`),
  ADD KEY `authority` (`authority`),
  ADD KEY `policy` (`policy`),
  ADD KEY `context_key` (`context_key`);

--
-- Индексы таблицы `modx_access_menus`
--
ALTER TABLE `modx_access_menus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `target` (`target`),
  ADD KEY `principal_class` (`principal_class`),
  ADD KEY `principal` (`principal`),
  ADD KEY `authority` (`authority`),
  ADD KEY `policy` (`policy`);

--
-- Индексы таблицы `modx_access_namespace`
--
ALTER TABLE `modx_access_namespace`
  ADD PRIMARY KEY (`id`),
  ADD KEY `target` (`target`),
  ADD KEY `principal_class` (`principal_class`),
  ADD KEY `principal` (`principal`),
  ADD KEY `authority` (`authority`),
  ADD KEY `policy` (`policy`),
  ADD KEY `context_key` (`context_key`);

--
-- Индексы таблицы `modx_access_permissions`
--
ALTER TABLE `modx_access_permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `template` (`template`),
  ADD KEY `name` (`name`);

--
-- Индексы таблицы `modx_access_policies`
--
ALTER TABLE `modx_access_policies`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `parent` (`parent`),
  ADD KEY `class` (`class`),
  ADD KEY `template` (`template`);

--
-- Индексы таблицы `modx_access_policy_templates`
--
ALTER TABLE `modx_access_policy_templates`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `modx_access_policy_template_groups`
--
ALTER TABLE `modx_access_policy_template_groups`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `modx_access_resources`
--
ALTER TABLE `modx_access_resources`
  ADD PRIMARY KEY (`id`),
  ADD KEY `target` (`target`),
  ADD KEY `principal_class` (`principal_class`),
  ADD KEY `principal` (`principal`),
  ADD KEY `authority` (`authority`),
  ADD KEY `policy` (`policy`),
  ADD KEY `context_key` (`context_key`);

--
-- Индексы таблицы `modx_access_resource_groups`
--
ALTER TABLE `modx_access_resource_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `target` (`target`),
  ADD KEY `principal_class` (`principal_class`,`target`,`principal`,`authority`),
  ADD KEY `principal` (`principal`),
  ADD KEY `authority` (`authority`),
  ADD KEY `policy` (`policy`),
  ADD KEY `context_key` (`context_key`);

--
-- Индексы таблицы `modx_access_templatevars`
--
ALTER TABLE `modx_access_templatevars`
  ADD PRIMARY KEY (`id`),
  ADD KEY `target` (`target`),
  ADD KEY `principal_class` (`principal_class`),
  ADD KEY `principal` (`principal`),
  ADD KEY `authority` (`authority`),
  ADD KEY `policy` (`policy`),
  ADD KEY `context_key` (`context_key`);

--
-- Индексы таблицы `modx_actiondom`
--
ALTER TABLE `modx_actiondom`
  ADD PRIMARY KEY (`id`),
  ADD KEY `set` (`set`),
  ADD KEY `action` (`action`),
  ADD KEY `name` (`name`),
  ADD KEY `active` (`active`),
  ADD KEY `for_parent` (`for_parent`),
  ADD KEY `rank` (`rank`);

--
-- Индексы таблицы `modx_actions`
--
ALTER TABLE `modx_actions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `namespace` (`namespace`),
  ADD KEY `controller` (`controller`);

--
-- Индексы таблицы `modx_actions_fields`
--
ALTER TABLE `modx_actions_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `action` (`action`),
  ADD KEY `type` (`type`),
  ADD KEY `tab` (`tab`);

--
-- Индексы таблицы `modx_active_users`
--
ALTER TABLE `modx_active_users`
  ADD PRIMARY KEY (`internalKey`);

--
-- Индексы таблицы `modx_categories`
--
ALTER TABLE `modx_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `category` (`parent`,`category`),
  ADD KEY `parent` (`parent`),
  ADD KEY `rank` (`rank`);

--
-- Индексы таблицы `modx_categories_closure`
--
ALTER TABLE `modx_categories_closure`
  ADD PRIMARY KEY (`ancestor`,`descendant`);

--
-- Индексы таблицы `modx_class_map`
--
ALTER TABLE `modx_class_map`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `class` (`class`),
  ADD KEY `parent_class` (`parent_class`),
  ADD KEY `name_field` (`name_field`);

--
-- Индексы таблицы `modx_content_type`
--
ALTER TABLE `modx_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Индексы таблицы `modx_context`
--
ALTER TABLE `modx_context`
  ADD PRIMARY KEY (`key`),
  ADD KEY `name` (`name`),
  ADD KEY `rank` (`rank`);

--
-- Индексы таблицы `modx_context_resource`
--
ALTER TABLE `modx_context_resource`
  ADD PRIMARY KEY (`context_key`,`resource`);

--
-- Индексы таблицы `modx_context_setting`
--
ALTER TABLE `modx_context_setting`
  ADD PRIMARY KEY (`context_key`,`key`);

--
-- Индексы таблицы `modx_dashboard`
--
ALTER TABLE `modx_dashboard`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`),
  ADD KEY `hide_trees` (`hide_trees`);

--
-- Индексы таблицы `modx_dashboard_widget`
--
ALTER TABLE `modx_dashboard_widget`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`),
  ADD KEY `type` (`type`),
  ADD KEY `namespace` (`namespace`),
  ADD KEY `lexicon` (`lexicon`);

--
-- Индексы таблицы `modx_dashboard_widget_placement`
--
ALTER TABLE `modx_dashboard_widget_placement`
  ADD PRIMARY KEY (`dashboard`,`widget`),
  ADD KEY `rank` (`rank`);

--
-- Индексы таблицы `modx_documentgroup_names`
--
ALTER TABLE `modx_documentgroup_names`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Индексы таблицы `modx_document_groups`
--
ALTER TABLE `modx_document_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `document_group` (`document_group`),
  ADD KEY `document` (`document`);

--
-- Индексы таблицы `modx_element_property_sets`
--
ALTER TABLE `modx_element_property_sets`
  ADD PRIMARY KEY (`element`,`element_class`,`property_set`);

--
-- Индексы таблицы `modx_extension_packages`
--
ALTER TABLE `modx_extension_packages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `namespace` (`namespace`),
  ADD KEY `name` (`name`);

--
-- Индексы таблицы `modx_fc_profiles`
--
ALTER TABLE `modx_fc_profiles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`),
  ADD KEY `rank` (`rank`),
  ADD KEY `active` (`active`);

--
-- Индексы таблицы `modx_fc_profiles_usergroups`
--
ALTER TABLE `modx_fc_profiles_usergroups`
  ADD PRIMARY KEY (`usergroup`,`profile`);

--
-- Индексы таблицы `modx_fc_sets`
--
ALTER TABLE `modx_fc_sets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `profile` (`profile`),
  ADD KEY `action` (`action`),
  ADD KEY `active` (`active`),
  ADD KEY `template` (`template`);

--
-- Индексы таблицы `modx_gallery_albums`
--
ALTER TABLE `modx_gallery_albums`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent` (`parent`),
  ADD KEY `name` (`name`),
  ADD KEY `createdby` (`createdby`),
  ADD KEY `rank` (`rank`),
  ADD KEY `active` (`active`),
  ADD KEY `prominent` (`prominent`);

--
-- Индексы таблицы `modx_gallery_album_contexts`
--
ALTER TABLE `modx_gallery_album_contexts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `album` (`album`),
  ADD KEY `context_key` (`context_key`);

--
-- Индексы таблицы `modx_gallery_album_items`
--
ALTER TABLE `modx_gallery_album_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `item` (`item`),
  ADD KEY `album` (`album`),
  ADD KEY `rank` (`rank`);

--
-- Индексы таблицы `modx_gallery_items`
--
ALTER TABLE `modx_gallery_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `createdby` (`createdby`),
  ADD KEY `name` (`name`),
  ADD KEY `active` (`active`),
  ADD KEY `mediatype` (`mediatype`);

--
-- Индексы таблицы `modx_gallery_tags`
--
ALTER TABLE `modx_gallery_tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `item` (`item`),
  ADD KEY `tag` (`tag`);

--
-- Индексы таблицы `modx_lexicon_entries`
--
ALTER TABLE `modx_lexicon_entries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`),
  ADD KEY `topic` (`topic`),
  ADD KEY `namespace` (`namespace`),
  ADD KEY `language` (`language`);

--
-- Индексы таблицы `modx_manager_log`
--
ALTER TABLE `modx_manager_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_occurred` (`user`,`occurred`);

--
-- Индексы таблицы `modx_media_sources`
--
ALTER TABLE `modx_media_sources`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`),
  ADD KEY `class_key` (`class_key`),
  ADD KEY `is_stream` (`is_stream`);

--
-- Индексы таблицы `modx_media_sources_contexts`
--
ALTER TABLE `modx_media_sources_contexts`
  ADD PRIMARY KEY (`source`,`context_key`);

--
-- Индексы таблицы `modx_media_sources_elements`
--
ALTER TABLE `modx_media_sources_elements`
  ADD PRIMARY KEY (`source`,`object`,`object_class`,`context_key`);

--
-- Индексы таблицы `modx_membergroup_names`
--
ALTER TABLE `modx_membergroup_names`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `parent` (`parent`),
  ADD KEY `rank` (`rank`),
  ADD KEY `dashboard` (`dashboard`);

--
-- Индексы таблицы `modx_member_groups`
--
ALTER TABLE `modx_member_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role` (`role`),
  ADD KEY `rank` (`rank`);

--
-- Индексы таблицы `modx_menus`
--
ALTER TABLE `modx_menus`
  ADD PRIMARY KEY (`text`),
  ADD KEY `parent` (`parent`),
  ADD KEY `action` (`action`),
  ADD KEY `namespace` (`namespace`);

--
-- Индексы таблицы `modx_namespaces`
--
ALTER TABLE `modx_namespaces`
  ADD PRIMARY KEY (`name`);

--
-- Индексы таблицы `modx_property_set`
--
ALTER TABLE `modx_property_set`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `category` (`category`);

--
-- Индексы таблицы `modx_register_messages`
--
ALTER TABLE `modx_register_messages`
  ADD PRIMARY KEY (`topic`,`id`),
  ADD KEY `created` (`created`),
  ADD KEY `valid` (`valid`),
  ADD KEY `accessed` (`accessed`),
  ADD KEY `accesses` (`accesses`),
  ADD KEY `expires` (`expires`);

--
-- Индексы таблицы `modx_register_queues`
--
ALTER TABLE `modx_register_queues`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Индексы таблицы `modx_register_topics`
--
ALTER TABLE `modx_register_topics`
  ADD PRIMARY KEY (`id`),
  ADD KEY `queue` (`queue`),
  ADD KEY `name` (`name`);

--
-- Индексы таблицы `modx_session`
--
ALTER TABLE `modx_session`
  ADD PRIMARY KEY (`id`),
  ADD KEY `access` (`access`);

--
-- Индексы таблицы `modx_site_content`
--
ALTER TABLE `modx_site_content`
  ADD PRIMARY KEY (`id`),
  ADD KEY `alias` (`alias`),
  ADD KEY `published` (`published`),
  ADD KEY `pub_date` (`pub_date`),
  ADD KEY `unpub_date` (`unpub_date`),
  ADD KEY `parent` (`parent`),
  ADD KEY `isfolder` (`isfolder`),
  ADD KEY `template` (`template`),
  ADD KEY `menuindex` (`menuindex`),
  ADD KEY `searchable` (`searchable`),
  ADD KEY `cacheable` (`cacheable`),
  ADD KEY `hidemenu` (`hidemenu`),
  ADD KEY `class_key` (`class_key`),
  ADD KEY `context_key` (`context_key`),
  ADD KEY `uri` (`uri`(333)),
  ADD KEY `uri_override` (`uri_override`),
  ADD KEY `hide_children_in_tree` (`hide_children_in_tree`),
  ADD KEY `show_in_tree` (`show_in_tree`),
  ADD KEY `cache_refresh_idx` (`parent`,`menuindex`,`id`);
ALTER TABLE `modx_site_content` ADD FULLTEXT KEY `content_ft_idx` (`pagetitle`,`longtitle`,`description`,`introtext`,`content`);

--
-- Индексы таблицы `modx_site_htmlsnippets`
--
ALTER TABLE `modx_site_htmlsnippets`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `category` (`category`),
  ADD KEY `locked` (`locked`),
  ADD KEY `static` (`static`);

--
-- Индексы таблицы `modx_site_plugins`
--
ALTER TABLE `modx_site_plugins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `category` (`category`),
  ADD KEY `locked` (`locked`),
  ADD KEY `disabled` (`disabled`),
  ADD KEY `static` (`static`);

--
-- Индексы таблицы `modx_site_plugin_events`
--
ALTER TABLE `modx_site_plugin_events`
  ADD PRIMARY KEY (`pluginid`,`event`),
  ADD KEY `priority` (`priority`);

--
-- Индексы таблицы `modx_site_snippets`
--
ALTER TABLE `modx_site_snippets`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `category` (`category`),
  ADD KEY `locked` (`locked`),
  ADD KEY `moduleguid` (`moduleguid`),
  ADD KEY `static` (`static`);

--
-- Индексы таблицы `modx_site_templates`
--
ALTER TABLE `modx_site_templates`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `templatename` (`templatename`),
  ADD KEY `category` (`category`),
  ADD KEY `locked` (`locked`),
  ADD KEY `static` (`static`);

--
-- Индексы таблицы `modx_site_tmplvars`
--
ALTER TABLE `modx_site_tmplvars`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `category` (`category`),
  ADD KEY `locked` (`locked`),
  ADD KEY `rank` (`rank`),
  ADD KEY `static` (`static`);

--
-- Индексы таблицы `modx_site_tmplvar_access`
--
ALTER TABLE `modx_site_tmplvar_access`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tmplvar_template` (`tmplvarid`,`documentgroup`);

--
-- Индексы таблицы `modx_site_tmplvar_contentvalues`
--
ALTER TABLE `modx_site_tmplvar_contentvalues`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tmplvarid` (`tmplvarid`),
  ADD KEY `contentid` (`contentid`),
  ADD KEY `tv_cnt` (`tmplvarid`,`contentid`);

--
-- Индексы таблицы `modx_site_tmplvar_templates`
--
ALTER TABLE `modx_site_tmplvar_templates`
  ADD PRIMARY KEY (`tmplvarid`,`templateid`);

--
-- Индексы таблицы `modx_system_eventnames`
--
ALTER TABLE `modx_system_eventnames`
  ADD PRIMARY KEY (`name`);

--
-- Индексы таблицы `modx_system_settings`
--
ALTER TABLE `modx_system_settings`
  ADD PRIMARY KEY (`key`);

--
-- Индексы таблицы `modx_tagger_groups`
--
ALTER TABLE `modx_tagger_groups`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `modx_tagger_tags`
--
ALTER TABLE `modx_tagger_tags`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `iTagGroup` (`tag`,`group`),
  ADD KEY `iTag` (`tag`),
  ADD KEY `iAlias` (`alias`),
  ADD KEY `iGroup` (`group`);

--
-- Индексы таблицы `modx_tagger_tag_resources`
--
ALTER TABLE `modx_tagger_tag_resources`
  ADD PRIMARY KEY (`tag`,`resource`);

--
-- Индексы таблицы `modx_transport_packages`
--
ALTER TABLE `modx_transport_packages`
  ADD PRIMARY KEY (`signature`),
  ADD KEY `workspace` (`workspace`),
  ADD KEY `provider` (`provider`),
  ADD KEY `disabled` (`disabled`),
  ADD KEY `package_name` (`package_name`),
  ADD KEY `version_major` (`version_major`),
  ADD KEY `version_minor` (`version_minor`),
  ADD KEY `version_patch` (`version_patch`),
  ADD KEY `release` (`release`),
  ADD KEY `release_index` (`release_index`);

--
-- Индексы таблицы `modx_transport_providers`
--
ALTER TABLE `modx_transport_providers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `api_key` (`api_key`),
  ADD KEY `username` (`username`),
  ADD KEY `active` (`active`),
  ADD KEY `priority` (`priority`);

--
-- Индексы таблицы `modx_users`
--
ALTER TABLE `modx_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `class_key` (`class_key`),
  ADD KEY `remote_key` (`remote_key`),
  ADD KEY `primary_group` (`primary_group`);

--
-- Индексы таблицы `modx_user_attributes`
--
ALTER TABLE `modx_user_attributes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `internalKey` (`internalKey`);

--
-- Индексы таблицы `modx_user_group_roles`
--
ALTER TABLE `modx_user_group_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `authority` (`authority`);

--
-- Индексы таблицы `modx_user_group_settings`
--
ALTER TABLE `modx_user_group_settings`
  ADD PRIMARY KEY (`group`,`key`);

--
-- Индексы таблицы `modx_user_messages`
--
ALTER TABLE `modx_user_messages`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `modx_user_settings`
--
ALTER TABLE `modx_user_settings`
  ADD PRIMARY KEY (`user`,`key`);

--
-- Индексы таблицы `modx_workspaces`
--
ALTER TABLE `modx_workspaces`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `path` (`path`),
  ADD KEY `name` (`name`),
  ADD KEY `active` (`active`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `modx_access_actiondom`
--
ALTER TABLE `modx_access_actiondom`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `modx_access_actions`
--
ALTER TABLE `modx_access_actions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `modx_access_category`
--
ALTER TABLE `modx_access_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `modx_access_context`
--
ALTER TABLE `modx_access_context`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `modx_access_elements`
--
ALTER TABLE `modx_access_elements`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `modx_access_media_source`
--
ALTER TABLE `modx_access_media_source`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `modx_access_menus`
--
ALTER TABLE `modx_access_menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `modx_access_namespace`
--
ALTER TABLE `modx_access_namespace`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `modx_access_permissions`
--
ALTER TABLE `modx_access_permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=220;
--
-- AUTO_INCREMENT для таблицы `modx_access_policies`
--
ALTER TABLE `modx_access_policies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT для таблицы `modx_access_policy_templates`
--
ALTER TABLE `modx_access_policy_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `modx_access_policy_template_groups`
--
ALTER TABLE `modx_access_policy_template_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `modx_access_resources`
--
ALTER TABLE `modx_access_resources`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `modx_access_resource_groups`
--
ALTER TABLE `modx_access_resource_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `modx_access_templatevars`
--
ALTER TABLE `modx_access_templatevars`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `modx_actiondom`
--
ALTER TABLE `modx_actiondom`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `modx_actions`
--
ALTER TABLE `modx_actions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `modx_actions_fields`
--
ALTER TABLE `modx_actions_fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
--
-- AUTO_INCREMENT для таблицы `modx_categories`
--
ALTER TABLE `modx_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблицы `modx_class_map`
--
ALTER TABLE `modx_class_map`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблицы `modx_content_type`
--
ALTER TABLE `modx_content_type`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `modx_dashboard`
--
ALTER TABLE `modx_dashboard`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `modx_dashboard_widget`
--
ALTER TABLE `modx_dashboard_widget`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `modx_documentgroup_names`
--
ALTER TABLE `modx_documentgroup_names`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `modx_document_groups`
--
ALTER TABLE `modx_document_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `modx_extension_packages`
--
ALTER TABLE `modx_extension_packages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `modx_fc_profiles`
--
ALTER TABLE `modx_fc_profiles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `modx_fc_sets`
--
ALTER TABLE `modx_fc_sets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `modx_gallery_albums`
--
ALTER TABLE `modx_gallery_albums`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `modx_gallery_album_contexts`
--
ALTER TABLE `modx_gallery_album_contexts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `modx_gallery_album_items`
--
ALTER TABLE `modx_gallery_album_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `modx_gallery_items`
--
ALTER TABLE `modx_gallery_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `modx_gallery_tags`
--
ALTER TABLE `modx_gallery_tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `modx_lexicon_entries`
--
ALTER TABLE `modx_lexicon_entries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `modx_manager_log`
--
ALTER TABLE `modx_manager_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=631;
--
-- AUTO_INCREMENT для таблицы `modx_media_sources`
--
ALTER TABLE `modx_media_sources`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `modx_membergroup_names`
--
ALTER TABLE `modx_membergroup_names`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `modx_member_groups`
--
ALTER TABLE `modx_member_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `modx_property_set`
--
ALTER TABLE `modx_property_set`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `modx_register_queues`
--
ALTER TABLE `modx_register_queues`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `modx_register_topics`
--
ALTER TABLE `modx_register_topics`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `modx_site_content`
--
ALTER TABLE `modx_site_content`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2015;
--
-- AUTO_INCREMENT для таблицы `modx_site_htmlsnippets`
--
ALTER TABLE `modx_site_htmlsnippets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT для таблицы `modx_site_plugins`
--
ALTER TABLE `modx_site_plugins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `modx_site_snippets`
--
ALTER TABLE `modx_site_snippets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT для таблицы `modx_site_templates`
--
ALTER TABLE `modx_site_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `modx_site_tmplvars`
--
ALTER TABLE `modx_site_tmplvars`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT для таблицы `modx_site_tmplvar_access`
--
ALTER TABLE `modx_site_tmplvar_access`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `modx_site_tmplvar_contentvalues`
--
ALTER TABLE `modx_site_tmplvar_contentvalues`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT для таблицы `modx_tagger_groups`
--
ALTER TABLE `modx_tagger_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `modx_tagger_tags`
--
ALTER TABLE `modx_tagger_tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `modx_transport_providers`
--
ALTER TABLE `modx_transport_providers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `modx_users`
--
ALTER TABLE `modx_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `modx_user_attributes`
--
ALTER TABLE `modx_user_attributes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `modx_user_group_roles`
--
ALTER TABLE `modx_user_group_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `modx_user_messages`
--
ALTER TABLE `modx_user_messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `modx_workspaces`
--
ALTER TABLE `modx_workspaces`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
