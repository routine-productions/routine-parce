<?php
use Illuminate\Database\Capsule\Manager as Capsule;

$Capsule = new Capsule;
$Capsule->addConnection([
    'driver' => 'mysql',
    'host' => 'localhost',
    'database' => 'stroyshi_shina',
    'username' => 'root',
    'password' => '987975',
    'charset' => 'utf8',
    'collation' => 'utf8_general_ci'
]);

$Capsule->setAsGlobal();
$Capsule->bootEloquent();

class Schema
{
    public static function __callStatic($Name, $Params)
    {
        return call_user_func_array([Illuminate\Database\Capsule\Manager::schema(), $Name], $Params);
    }
}

class DB
{
    public static function __callStatic($Name, $Params)
    {
        return call_user_func_array(['Illuminate\Database\Capsule\Manager', $Name], $Params);
    }
}