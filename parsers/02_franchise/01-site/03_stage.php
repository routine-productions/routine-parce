<?php
/*
 * Get: Franchise page
 */

define('__HOMEDIR__', __DIR__);
require_once __DIR__ . '/../../core.php';

$Pages = require_once __DIR__ . '/results/02_stage.php';
$Content = [];

foreach ($Pages as $Page_Key => $Page)
{
    $Dom->loadFromURL('http://topfranchise.ru' . $Page['href']);

    // Parse: Get main params
    $Content[$Page_Key] = [
        'first_parent' => $Page['first_parent'],
        'second_parent' => $Page['second_parent'],
        'title' => $Dom->find('h1')->text,
        'content' => trim(html_entity_decode(($Dom->find('.block_fran_info')->innerHtml))),
        'requirements' => ($Dom->find('#requirements > div', 0)) ? $Dom->find('#requirements > div')->innerHtml : '',
        'main_image' => '/assets/gallery' . $Page['img'],
        'logo' => ($Dom->find('#brand .tac img', 0)) ? '/assets/gallery' . $Dom->find('#brand .tac img')->src : '',

        'business' => ($Dom->find('.item_point', 0)) ? $Dom->find('.item_point')[0]->find('span.t_20dblue')->text : '',
        'category' => ($Dom->find('.item_point', 1)) ? $Dom->find('.item_point')[1]->find('span.t_20dblue')->text : '',
        'enterprise' => ($Dom->find('.item_point', 2)) ? $Dom->find('.item_point')[2]->find('span.t_20dblue')->text : '',

        'invest_price' => ($Dom->find('.infograph1 .t_73white ', 0)) ? $Dom->find('.infograph1 .t_73white ')[0]->text : '',
        'payback' => intval(($Dom->find('.infograph1 .t_73white ', 1)) ? $Dom->find('.infograph1 .t_73white ')[1]->text : ''),
        'turnover' => ($Dom->find('.infograph1 .t_73white ', 1)) ? $Dom->find('.infograph1 .t_73white ')[2]->text : '',
    ];

    // Parse: Round params
    if ($Dom->find('.brand_info li'))
    {
        foreach ($Dom->find('.brand_info li') as $Parameter)
        {
            if (trim($Parameter->text) == 'Год основания компании')
            {
                $Content[$Page_Key]['foundation'] = $Parameter->find('div > div')->text;
            }
            if (trim($Parameter->text) == 'Год запуска франшизы в РОССИИ')
            {
                $Content[$Page_Key]['start_year'] = $Parameter->find('div > div')->text;
            }
            if (trim($Parameter->text) == 'Франшизных предприятий')
            {
                $Content[$Page_Key]['franchise_enterprises'] = $Parameter->find('div > div')->text;
            }
            if (trim($Parameter->text) == 'Собственных предприятий')
            {
                $Content[$Page_Key]['own_enterprises'] = $Parameter->find('div > div')->text;
            }
        }
    }

    // Parse: Gallery
    foreach ($Dom->find('.gallery_detail_2 .wrapper div') as $Image)
    {
        $Content[$Page_Key]['gallery'][] = str_replace('\');', '', str_replace('background-image: url(\'', '', $Image->style));
    }
}

// Write: To file
file_put_contents(__DIR__ . '/results/03_pages.php', "<?php \n    return " . var_export($Content, true) . ";");
