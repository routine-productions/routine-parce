<?php
/*
 * Create: Pages 01
 */
define('__HOMEDIR__', __DIR__);
require_once __DIR__ . '/../../core.php';

$Pages = require_once __DIR__ . '/../02_franchise-01/results/03_pages.php';

foreach ($Pages as $Page_Key => $Page)
{
    $Model = Modx_Site_Content::find(3000 + ($Page_Key + 1));

    $Dom->load($Model->content);

    if ($Dom->find('iframe', 0))
    {
        foreach ($Dom->find('iframe') as $Iframe)
        {
            $Extra_Fields = new Modx_Site_Tmplvar_Contentvalues();
            $Extra_Fields->tmplvarid = 35;
            $Extra_Fields->contentid = 3000 + ($Page_Key + 1);
            $Extra_Fields->value = $Iframe->outerHtml;
            $Extra_Fields->save();

            $Iframe->delete();
            unset($Iframe);
            break;
        }
    }

    $Model->content = $Dom->outerHtml;
    $Model->save();
}