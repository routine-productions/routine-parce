<?php

define('__HOMEDIR__', __DIR__);

require_once __DIR__ . '/../core.php';
$enter_data = require __DIR__ . '/enter_data/news.php';
$Newses = [];

$Dom->load($enter_data);

foreach ($Dom->find('.small-news') as $key => $news)
{
    $Newses[$key]['title'] = $news->find('h2 a')->text;
    $Newses[$key]['href'] = $news->find('.category-article-img a')->href;
    $Newses[$key]['short_about'] = $news->find('.short-desc p')->text;
}
print_r($Newses);


// Write: To file

file_put_contents(__DIR__ . '/results/news.php', "<?php \n    return " . var_export($Newses, true) . ";");
