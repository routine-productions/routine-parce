<?php

class B_Iblock_Element extends Illuminate\Database\Eloquent\Model
{
    public $timestamps = false;
    protected $table = "b_iblock_element";

    public function values()
    {
        return $this->hasMany('b_iblock_element_property', 'IBLOCK_ELEMENT_ID', 'ID');
    }

    public function youtube()
    {
        return $this->hasOne('b_iblock_element_property', 'IBLOCK_ELEMENT_ID', 'ID')->where('IBLOCK_PROPERTY_ID', 2);
    }

    public function image()
    {
        return $this->hasOne('b_file', 'ID', 'PREVIEW_PICTURE');
    }
}
