<?php
define('__HOMEDIR__', __DIR__);
require_once __DIR__ . '/../core.php';


SetCat();


function SetCat()
{
    $Items = require __DIR__ . '/results/Items_01.php';

    foreach ($Items as $key => $Category)
    {
        $Categoryes = Create_Cat($key, 124);
        foreach ($Category as $Itemss)
        {
            $Post = Post_Create($Itemss['Content'], $Itemss['title']);
            Relation_Create($Post->id, $Categoryes->id);
            Post_Characteristics($Post->id, $Itemss['Characteristics']);


            $MainImage = new Posts();
            $MainImage->guid = $Itemss['Image'];
            $MainImage->post_mime_type = 'image/jpeg';
            $MainImage->post_type = 'attachment';
            $MainImage->post_status = 'inherit';
            $MainImage->comment_status = 'open';
            $MainImage->post_parent = $Post->id;
            $MainImage->ping_status = 'closed';
            $MainImage->post_name = microtime(true);
            $MainImage->post_title = 'main_image';
            $MainImage->save();

            Post_Meta('post-image', $MainImage->id, $Post->id);
            Post_Meta('_post-image', 'field_57bda5fb5ad6d', $Post->id);

            $Images = new Posts();
            $Images->guid = $Itemss['Image'];
            $Images->post_mime_type = 'image/jpeg';
            $Images->post_type = 'attachment';
            $Images->post_status = 'inherit';
            $Images->comment_status = 'open';
            $Images->post_parent = $Post->id;
            $Images->ping_status = 'closed';
            $Images->post_name = microtime(true);;
            $Images->post_title = 'main_image';
            $Images->save();
            Post_Meta('gallery_0_image', $Images->id, $Post->id);


            Post_Meta('short-description', filter($Itemss['Description']), $Post->id);
            Post_Meta('_short-description', 'field_57bda63b5ad6e', $Post->id);

            Post_Meta('gallery', 1, $Post->id);
            Post_Meta('_gallery', 'field_57c3d4530a013', $Post->id);

            Post_Meta('price', '', $Post->id);
            Post_Meta('_price', 'field_57bda6ad5ad6f', $Post->id);

        }
    }
}


function Create_Cat($Title, $id)
{
    $Category = new Category();
    $Category->name = $Title;
    $Category->slug = Create_Slug($Title);
    $Category->save();

    $Relation = new Taxonomy();
    $Relation->term_taxonomy_id = $Category->id;
    $Relation->term_id = $Category->id;
    $Relation->taxonomy = 'category';
    $Relation->parent = $id;
    $Relation->count = 0;
    $Relation->save();
    return $Category;
}

function Post_Create($Content, $Header)
{
    $Post = new Posts();
    $Post->post_content = $Content;
    $Post->post_title = $Header;
    $Post->post_status = 'publish';
    $Post->ping_status = 'open';
    $Post->post_author = '1';
    $Post->post_date = date("Y-m-d H:i:s");
    $Post->comment_status = 'open';
    $Post->post_type = 'post';
    $Post->post_name = Create_Slug($Header);
    $Post->save();
    return $Post;
}

function Post_Meta($Meta_Key, $Meta_Value, $Post_Id)
{
    $PostMeta = new PostMeta();
    $PostMeta->meta_key = $Meta_Key;
    $PostMeta->meta_value = $Meta_Value;
    $PostMeta->post_id = $Post_Id;
    $PostMeta->save();
}

function Relation_Create($id, $cat_id)
{
    $Relation = new Relation();
    $Relation->object_id = $id;
    $Relation->term_taxonomy_id = $cat_id;
    $Relation->term_order = 0;
    $Relation->save();
}

function Post_Characteristics($Post_Id, $Characteristics = [])
{
    Post_Meta('characteristics', $Characteristics, $Post_Id);
    Post_Meta('_characteristics', 'field_57bda7bc734bc', $Post_Id);
}


function Save_Image($Post_Id, $Src, $Alt = '')
{
    $Attachment = new Posts();
    $Attachment->guid = '/wp-content/uploads/2016/09' . $Src;
    $Attachment->post_mime_type = 'image/jpeg';
    $Attachment->post_type = 'attachment';
    $Attachment->post_status = 'inherit';
    $Attachment->comment_status = 'open';
    $Attachment->post_parent = $Post_Id;
    $Attachment->ping_status = 'closed';
    $Attachment->post_name = microtime(true);
    $Attachment->post_title = $Alt;
    $Attachment->save();
    Save_File($Src, 'http://ekoton.com');
}

function filter($Content)
{
    $remoteTypograf = new RemoteTypograf('');
    $remoteTypograf->htmlEntities();
    $remoteTypograf->br(false);
    $remoteTypograf->p(true);
    $remoteTypograf->nobr(3);
    $remoteTypograf->quotA('laquo raquo');
    $remoteTypograf->quotB('bdquo ldquo');

    $Content = $remoteTypograf->processText($Content);
    return $Content;
}


class RemoteTypograf
{
    var $_entityType = 4;
    var $_useBr = 1;
    var $_useP = 1;
    var $_maxNobr = 3;
    var $_encoding = 'UTF-8';
    var $_quotA = 'laquo raquo';
    var $_quotB = 'bdquo ldquo';

    function RemoteTypograf($encoding)
    {
        if ($encoding) $this->_encoding = $encoding;
    }

    function htmlEntities()
    {
        $this->_entityType = 1;
    }

    function xmlEntities()
    {
        $this->_entityType = 2;
    }

    function mixedEntities()
    {
        $this->_entityType = 4;
    }

    function noEntities()
    {
        $this->_entityType = 3;
    }

    function br($value)
    {
        $this->_useBr = $value ? 1 : 0;
    }

    function p($value)
    {
        $this->_useP = $value ? 1 : 0;
    }

    function nobr($value)
    {
        $this->_maxNobr = $value ? $value : 0;
    }

    function quotA($value)
    {
        $this->_quotA = $value;
    }

    function quotB($value)
    {
        $this->_quotB = $value;
    }

    function processText($text)
    {
        $text = str_replace('&', '&amp;', $text);
        $text = str_replace('<', '&lt;', $text);
        $text = str_replace('>', '&gt;', $text);

        $SOAPBody = '<?xml version="1.0" encoding="' . $this->_encoding . '"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
	<ProcessText xmlns="http://typograf.artlebedev.ru/webservices/">
	  <text>' . $text . '</text>
      <entityType>' . $this->_entityType . '</entityType>
      <useBr>' . $this->_useBr . '</useBr>
      <useP>' . $this->_useP . '</useP>
      <maxNobr>' . $this->_maxNobr . '</maxNobr>
      <quotA>' . $this->_quotA . '</quotA>
      <quotB>' . $this->_quotB . '</quotB>
	</ProcessText>
  </soap:Body>
</soap:Envelope>';

        $host = 'typograf.artlebedev.ru';
        $SOAPRequest = 'POST /webservices/typograf.asmx HTTP/1.1
Host: typograf.artlebedev.ru
Content-Type: text/xml
Content-Length: ' . strlen($SOAPBody) . '
SOAPAction: "http://typograf.artlebedev.ru/webservices/ProcessText"

' .
            $SOAPBody;

        $remoteTypograf = fsockopen($host, 80);
        fwrite($remoteTypograf, $SOAPRequest);
        $typografResponse = '';
        while (!feof($remoteTypograf))
        {
            $typografResponse .= fread($remoteTypograf, 8192);
        }
        fclose($remoteTypograf);

        $startsAt = strpos($typografResponse, '<ProcessTextResult>') + 19;
        $endsAt = strpos($typografResponse, '</ProcessTextResult>');
        $typografResponse = substr($typografResponse, $startsAt, $endsAt - $startsAt - 1);

        $typografResponse = str_replace('&amp;', '&', $typografResponse);
        $typografResponse = str_replace('&lt;', '<', $typografResponse);
        $typografResponse = str_replace('&gt;', '>', $typografResponse);

        return $typografResponse;
    }
}
