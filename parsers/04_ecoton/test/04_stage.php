<?php
/*
 * Get: Items Links
 */

define('__HOMEDIR__', __DIR__);
require_once __DIR__ . '/../../core.php';

SetCat();
function SetCat()
{

    foreach (require __DIR__ . '/../results/01_links.php' as $key => $value)
    {
        $cat_id[] = $key;
    }

    global $Dom;

    for ($index_test = 0; $index_test < 11; $index_test++)
    {
        $Links = require_once __DIR__ . '/../results/Items_links/' . $index_test . '_links.php';
        $Short_description = require_once __DIR__ . '/../results/Items_links/' . $index_test . '_description.php';
        $Items = require_once __DIR__ . '/../results/Items/' . $index_test . '_Item.php';
        $new_counter = 0;

        foreach ($Items as $Item)
        {

            $Page = [];


            //Create Post
            $Post = Post_Create($Item['Content'], $Item['Header']);
            Relation_Create($Post->id, $cat_id[$index_test]);
            Post_Characteristics($Post->id, $Item['Characteristics']);


            //Main images
            foreach ($Post['Images_src'] as $Image_Key => $Image)
            {
                $Images = new Posts();
                $Images->guid = $Image;
                $Images->post_mime_type = 'image/jpeg';
                $Images->post_type = 'attachment';
                $Images->post_status = 'inherit';
                $Images->comment_status = 'open';
                $Images->post_parent = $Post->id;
                $Images->ping_status = 'closed';
                $Images->post_name = microtime(true);;
                $Images->post_title = 'main_image';
                $Images->save();
                Post_Meta('gallery_' . $Image_Key . '_image', $Images->id, $Post->id);
//            Save_File($Image->src, 'http://ekoton.com');
            }

            $img = $Post['Images_src'][0];
            if (!empty($img->src))
            {
                $MainImage = new Posts();
                $MainImage->guid = $img;
                $MainImage->post_mime_type = 'image/jpeg';
                $MainImage->post_type = 'attachment';
                $MainImage->post_status = 'inherit';
                $MainImage->comment_status = 'open';
                $MainImage->post_parent = $Post->id;
                $MainImage->ping_status = 'closed';
                $MainImage->post_name = microtime(true);
                $MainImage->post_title = 'main_image';
                $MainImage->save();

                Post_Meta('post-image', $MainImage->id, $Post->id);
                Post_Meta('_post-image', 'field_57bda5fb5ad6d', $Post->id);
            }

            $Short_description = filter($Short_description[$new_counter]);

            Post_Meta('short-description', $Short_description, $Post->id);
            Post_Meta('_short-description', 'field_57bda63b5ad6e', $Post->id);

            Post_Meta('gallery', count($Post['Images_src']), $Post->id);
            Post_Meta('_gallery', 'field_57c3d4530a013', $Post->id);

            Post_Meta('price', '', $Post->id);
            Post_Meta('_price', 'field_57bda6ad5ad6f', $Post->id);

            $new_counter++;
        }
    }
}

function Post_Create($Content, $Header)
{
    $Post = new Posts();
    $Post->post_content = $Content;
    $Post->post_title = $Header;
    $Post->post_status = 'publish';
    $Post->ping_status = 'open';
    $Post->post_author = '1';
    $Post->post_date = date("Y-m-d H:i:s");
    $Post->comment_status = 'open';
    $Post->post_type = 'post';
    $Post->post_name = Create_Slug($Header);
    $Post->save();
    return $Post;
}

function Post_Meta($Meta_Key, $Meta_Value, $Post_Id)
{
    $PostMeta = new PostMeta();
    $PostMeta->meta_key = $Meta_Key;
    $PostMeta->meta_value = $Meta_Value;
    $PostMeta->post_id = $Post_Id;
    $PostMeta->save();
}

function Relation_Create($id, $cat_id)
{
    $Relation = new Relation();
    $Relation->object_id = $id;
    $Relation->term_taxonomy_id = $cat_id;
    $Relation->term_order = 0;
    $Relation->save();
}

function Post_Characteristics($Post_Id, $Characteristics = [])
{
    foreach ($Characteristics as $Charact_Name => $Charact)
    {

        $Charact = '';
        if ($Charact_Name == 'Description')
        {
            Post_Meta('description', $Charact, $Post_Id);
            Post_Meta('_description', 'field_57bda777734bb', $Post_Id);
        }
        if ($Charact_Name == 'Operating_Principle')
        {
            Post_Meta('operating-principle', $Charact, $Post_Id);
            Post_Meta('_operating-principle', 'field_57bda72a734ba', $Post_Id);
        }
        if ($Charact_Name == 'Advantage')
        {
            $Charact .= $Charact;
        }
        if ($Charact_Name == 'Characteristics')
        {
            $Charact .= $Charact;
        }
        if ($Charact_Name == 'Automation')
        {
            $Charact .= $Charact;
        }
        if ($Charact_Name == 'Contents_of_delivery')
        {
            $Charact .= $Charact;
        }
        Post_Meta('characteristics', $Charact, $Post_Id);
        Post_Meta('_characteristics', 'field_57bda7bc734bc', $Post_Id);
    }
}

//function Save_Image($Post_Id, $Src, $Alt = '')
//{
//    $Attachment = new Posts();
//    $Attachment->guid = '/wp-content/uploads/2016/09' . $Src;
//    $Attachment->post_mime_type = 'image/jpeg';
//    $Attachment->post_type = 'attachment';
//    $Attachment->post_status = 'inherit';
//    $Attachment->comment_status = 'open';
//    $Attachment->post_parent = $Post_Id;
//    $Attachment->ping_status = 'closed';
//    $Attachment->post_name = microtime(true);
//    $Attachment->post_title = $Alt;
//    $Attachment->save();
//    Save_File($Src, 'http://ekoton.com');
//}

function filter($Content)
{

    include __DIR__ . "/../Helpers/remotetypograf.php";


    $remoteTypograf = new RemoteTypograf ('Windows-1251');
    $remoteTypograf->htmlEntities();
    $remoteTypograf->br(false);
    $remoteTypograf->p(true);
    $remoteTypograf->nobr(3);
    $remoteTypograf->quotA('laquo raquo');
    $remoteTypograf->quotB('bdquo ldquo');

    $Content = $remoteTypograf->processText($Content);
    return $Content;
}
