<?php
/*
 * Create: Pages 01
 */
define('__HOMEDIR__', __DIR__);
require_once __DIR__ . '/../core.php';

$Pages = require_once __DIR__ . '/../../02_franchise-02/results/03_pages.php';

foreach ($Pages as $Page_Key => $Page)
{
    $Model = new Modx_Site_Content();

    $Model->id = 5000 + ($Page_Key + 1);

    $Model->parent = 2000 + $Page['parent'];


    $Model->pagetitle = $Page['title'];
    $Model->alias = Create_Slug($Page['title']);
    $Model->published = 1;
    $Model->isfolder = 0;
    $Model->template = 2;
    $Model->uri = Create_Slug($Page['title']) . '/';

    $Model->content = $Page['content'];

    // Iframe
    $Dom->load($Model->content);
    if ($Dom->find('iframe', 0))
    {
        foreach ($Dom->find('iframe') as $Iframe)
        {
            $Extra_Fields = new Modx_Site_Tmplvar_Contentvalues();
            $Extra_Fields->tmplvarid = 35;
            $Extra_Fields->contentid = 5000 + ($Page_Key + 1);
            $Extra_Fields->value = $Iframe->outerHtml;
            $Extra_Fields->save();

            $Iframe->delete();
            unset($Iframe);
            break;
        }

        $Model->content = $Dom->outerHtml;
    }

    if ($Dom->find('img', 0))
    {
        foreach ($Dom->find('img') as $Img)
        {
            $Img->setAttribute('src', '/assets/gallery' . $Img->src);
        }
        $Model->content = $Dom->outerHtml;
    }


    $Model->save();

    if (isset($Page['gallery']))
    {
        $Galley_Album = new Modx_Gallery_Albums();
        $Galley_Album->id = 5000 + ($Page_Key + 1);
        $Galley_Album->name = $Page['title'];
        $Galley_Album->parent = 1;
        $Galley_Album->active = 1;
        $Galley_Album->prominent = 1;
        $Galley_Album->save();


        $Extra_Fields = new Modx_Site_Tmplvar_Contentvalues();
        $Extra_Fields->tmplvarid = 11;
        $Extra_Fields->contentid = 5000 + ($Page_Key + 1);
        $Extra_Fields->value = 5000 + ($Page_Key + 1);
        $Extra_Fields->save();


        foreach ($Page['gallery'] as $Image_Key => $Image)
        {
            $Galley_Album_Item = new Modx_Gallery_Album_Items();
            $Galley_Album_Item->item = 20000 +  ($Page_Key + 1) * 50 + $Image_Key;
            $Galley_Album_Item->album = 5000 + ($Page_Key + 1);
            $Galley_Album_Item->save();

            $Galley_Item = new Modx_Gallery_Items();
            $Galley_Item->id = 20000 + ($Page_Key + 1) * 50 + $Image_Key;
            $Galley_Item->name = 20000 + ($Page_Key + 1) * 50 + $Image_Key;
            $Galley_Item->active = 1;
            $Galley_Item->filename = mb_substr($Image, 1);
            $Galley_Item->save();
        }
    }

    // min_invest
    if (isset($Page['start_capital']))
    {
        $Extra_Fields = new Modx_Site_Tmplvar_Contentvalues();
        $Extra_Fields->tmplvarid = 1;
        $Extra_Fields->contentid = 5000 + ($Page_Key + 1);
        $Extra_Fields->value = $Page['start_capital'];
        $Extra_Fields->save();
    }

    // requirements
    if (isset($Page['requirements']))
    {
        $Dom->load($Page['requirements']);
        if ($Dom->find('img', 0))
        {
            foreach ($Dom->find('img') as $Img)
            {
                $Img->setAttribute('src', '/assets/gallery' . $Img->src);
            }
            $Page['requirements'] = $Dom->outerHtml;
        }

        $Extra_Fields = new Modx_Site_Tmplvar_Contentvalues();
        $Extra_Fields->tmplvarid = 12;
        $Extra_Fields->contentid = 5000 + ($Page_Key + 1);
        $Extra_Fields->value = $Page['requirements'];
        $Extra_Fields->save();
    }

    // anonce_img
    if (isset($Page['main_image']))
    {
        $Extra_Fields = new Modx_Site_Tmplvar_Contentvalues();
        $Extra_Fields->tmplvarid = 2;
        $Extra_Fields->contentid = 5000 + ($Page_Key + 1);
        $Extra_Fields->value = 'assets/gallery' . $Page['main_image'];
        $Extra_Fields->save();
    }

    // business
    if (isset($Page['business']))
    {
        $Extra_Fields = new Modx_Site_Tmplvar_Contentvalues();
        $Extra_Fields->tmplvarid = 14;
        $Extra_Fields->contentid = 3000 + ($Page_Key + 1);
        $Extra_Fields->value = $Page['business'];
        $Extra_Fields->save();
    }

    // category_business
    if (isset($Page['category']))
    {
        $Extra_Fields = new Modx_Site_Tmplvar_Contentvalues();
        $Extra_Fields->tmplvarid = 15;
        $Extra_Fields->contentid = 5000 + ($Page_Key + 1);
        $Extra_Fields->value = $Page['category'];
        $Extra_Fields->save();
    }


    // enterprise
    if (isset($Page['enterprise']))
    {
        $Extra_Fields = new Modx_Site_Tmplvar_Contentvalues();
        $Extra_Fields->tmplvarid = 16;
        $Extra_Fields->contentid = 5000 + ($Page_Key + 1);
        $Extra_Fields->value = $Page['enterprise'];
        $Extra_Fields->save();
    }

    // payback
    if (isset($Page['payback']))
    {
        $Extra_Fields = new Modx_Site_Tmplvar_Contentvalues();
        $Extra_Fields->tmplvarid = 3;
        $Extra_Fields->contentid = 5000 + ($Page_Key + 1);
        $Extra_Fields->value = $Page['payback'];
        $Extra_Fields->save();
    }

    // turnover
    if (isset($Page['turnover']))
    {
        $Extra_Fields = new Modx_Site_Tmplvar_Contentvalues();
        $Extra_Fields->tmplvarid = 17;
        $Extra_Fields->contentid = 5000 + ($Page_Key + 1);
        $Extra_Fields->value = intval(trim(str_replace(' ', '', $Page['turnover'])));
        $Extra_Fields->save();
    }

    // foundation
    if (isset($Page['foundation']))
    {
        $Extra_Fields = new Modx_Site_Tmplvar_Contentvalues();
        $Extra_Fields->tmplvarid = 18;
        $Extra_Fields->contentid = 5000 + ($Page_Key + 1);
        $Extra_Fields->value = $Page['foundation'];
        $Extra_Fields->save();
    }

    // start_year
    if (isset($Page['start_year']))
    {
        $Extra_Fields = new Modx_Site_Tmplvar_Contentvalues();
        $Extra_Fields->tmplvarid = 19;
        $Extra_Fields->contentid = 5000 + ($Page_Key + 1);
        $Extra_Fields->value = $Page['start_year'];
        $Extra_Fields->save();
    }

    // franchise_enterprises
    if (isset($Page['franchise_enterprises']))
    {
        $Extra_Fields = new Modx_Site_Tmplvar_Contentvalues();
        $Extra_Fields->tmplvarid = 20;
        $Extra_Fields->contentid = 5000 + ($Page_Key + 1);
        $Extra_Fields->value = $Page['franchise_enterprises'];
        $Extra_Fields->save();
    }

    // own_enterprises
    if (isset($Page['own_enterprises']))
    {
        $Extra_Fields = new Modx_Site_Tmplvar_Contentvalues();
        $Extra_Fields->tmplvarid = 21;
        $Extra_Fields->contentid = 5000 + ($Page_Key + 1);
        $Extra_Fields->value = $Page['own_enterprises'];
        $Extra_Fields->save();
    }


    // royalty
    if (isset($Page['royalty']))
    {
        $Extra_Fields = new Modx_Site_Tmplvar_Contentvalues();
        $Extra_Fields->tmplvarid = 22;
        $Extra_Fields->contentid = 5000 + ($Page_Key + 1);
        $Extra_Fields->value = $Page['royalty'];
        $Extra_Fields->save();
    }

    // advertising
    if (isset($Page['advertising']))
    {
        $Extra_Fields = new Modx_Site_Tmplvar_Contentvalues();
        $Extra_Fields->tmplvarid = 23;
        $Extra_Fields->contentid = 5000 + ($Page_Key + 1);
        $Extra_Fields->value = $Page['advertising'];
        $Extra_Fields->save();
    }

    // profitability
    if (isset($Page['profitability']))
    {
        $Extra_Fields = new Modx_Site_Tmplvar_Contentvalues();
        $Extra_Fields->tmplvarid = 24;
        $Extra_Fields->contentid = 5000 + ($Page_Key + 1);
        $Extra_Fields->value = $Page['profitability'];
        $Extra_Fields->save();
    }

    // franchise_price
    if (isset($Page['invest_price']))
    {
        $Extra_Fields = new Modx_Site_Tmplvar_Contentvalues();
        $Extra_Fields->tmplvarid = 25;
        $Extra_Fields->contentid = 5000 + ($Page_Key + 1);
        $Extra_Fields->value = $Page['invest_price'];
        $Extra_Fields->save();
    }

    // company_title
    if (isset($Page['company_title']))
    {
        $Extra_Fields = new Modx_Site_Tmplvar_Contentvalues();
        $Extra_Fields->tmplvarid = 26;
        $Extra_Fields->contentid = 5000 + ($Page_Key + 1);
        $Extra_Fields->value = $Page['company_title'];
        $Extra_Fields->save();
    }

    // address
    if (isset($Page['address']))
    {
        $Extra_Fields = new Modx_Site_Tmplvar_Contentvalues();
        $Extra_Fields->tmplvarid = 27;
        $Extra_Fields->contentid = 5000 + ($Page_Key + 1);
        $Extra_Fields->value = $Page['address'];
        $Extra_Fields->save();
    }

    // email
    if (isset($Page['email']))
    {
        $Extra_Fields = new Modx_Site_Tmplvar_Contentvalues();
        $Extra_Fields->tmplvarid = 28;
        $Extra_Fields->contentid = 5000 + ($Page_Key + 1);
        $Extra_Fields->value = $Page['email'];
        $Extra_Fields->save();
    }

    // phone
    if (isset($Page['phone']))
    {
        $Extra_Fields = new Modx_Site_Tmplvar_Contentvalues();
        $Extra_Fields->tmplvarid = 29;
        $Extra_Fields->contentid = 5000 + ($Page_Key + 1);
        $Extra_Fields->value = $Page['phone'];
        $Extra_Fields->save();
    }

    // website
    if (isset($Page['website']))
    {
        $Extra_Fields = new Modx_Site_Tmplvar_Contentvalues();
        $Extra_Fields->tmplvarid = 30;
        $Extra_Fields->contentid = 5000 + ($Page_Key + 1);
        $Extra_Fields->value = $Page['website'];
        $Extra_Fields->save();
    }

    // contact
    if (isset($Page['contact']))
    {
        $Extra_Fields = new Modx_Site_Tmplvar_Contentvalues();
        $Extra_Fields->tmplvarid = 31;
        $Extra_Fields->contentid = 5000 + ($Page_Key + 1);
        $Extra_Fields->value = $Page['contact'];
        $Extra_Fields->save();
    }

    // motherland
    if (isset($Page['motherland']))
    {
        $Extra_Fields = new Modx_Site_Tmplvar_Contentvalues();
        $Extra_Fields->tmplvarid = 32;
        $Extra_Fields->contentid = 5000 + ($Page_Key + 1);
        $Extra_Fields->value = $Page['motherland'];
        $Extra_Fields->save();
    }
}