<?php

class db 
{
		
	private static $_instance = null;
	
	
	static public function getInstance() 
	{
		if(is_null(self::$_instance))
		{
			self::$_instance = new mysqli(
				'localhost',
				'atmosfer_site',
				'bobn1bobn',
				'atmosfer_db'
			);
			if (self::$_instance->connect_errno) {
				die('Connect Error: ' . $mysqli->connect_error);
			}
			/* изменение набора символов на utf8 */
			if (!self::$_instance->set_charset("utf8")) {
				die('Set charset Error: ' . $mysqli->connect_error); 
			}
		}
		return self::$_instance;
	}

}


class atmosferClear
{
	
	private $db;
	private $brand_id = 15;
		
	
	function __construct()
	{
		$this->db = db::getInstance();
	}
	
	
	public function run()
	{

		$queryProd = "SELECT s_products.id FROM s_products WHERE s_products.brand_id={$this->brand_id}";
		$queryProd = $this->db->query($queryProd);
		if($queryProd){
			if(mysqli_num_rows($queryProd) > 0){
				while( $row = $queryProd->fetch_assoc() ){
					$p_id = $row['id'];
					$queryOption = "SELECT * FROM s_options WHERE s_options.product_id={$p_id} AND s_options.feature_id=168";
					$queryOption = $this->db->query($queryOption);
					if($queryOption){
						if(mysqli_num_rows($queryOption) == 0){
							
							$query = "DELETE s_products.* FROM s_products WHERE s_products.brand_id={$this->brand_id} AND s_products.id={$p_id}";
							$query = $this->db->query($query);
							if(!$query){
								echo __LINE__.' '.__FUNCTION__." Error : ({$this->db->errno}) {$this->db->error}";
							}

						}
					}else{
						echo __LINE__.' '.__FUNCTION__." option Error: ({$this->db->errno}) {$this->db->error}";
					}		
				}
			}
		}else{
			echo __LINE__.' '.__FUNCTION__." product Error: ({$this->db->errno}) {$this->db->error}";
		}		

	}
}



$parser = new atmosferClear;
$parser->run();
