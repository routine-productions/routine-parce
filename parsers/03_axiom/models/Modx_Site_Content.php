<?php

class Modx_Site_Content extends Illuminate\Database\Eloquent\Model
{
    public $timestamps = false;
    protected $table = "modx_site_content";

    public function values()
    {
        return $this->hasMany('modx_site_tmplvar_contentvalues', 'contentid', 'id');
    }

    public function youtube()
    {
        return $this->hasOne('modx_site_tmplvar_contentvalues', 'contentid', 'id')->where('tmplvarid', 42);
    }

    public function image()
    {
        return $this->hasOne('modx_site_tmplvar_contentvalues', 'contentid', 'id')->where('tmplvarid', 43);
    }

    public function videos()
    {
        return $this->hasMany('modx_site_tmplvar_contentvalues', 'contentid', 'id')->whereIn('tmplvarid', [22, 24, 25, 26, 27, 28, 29, 30, 31, 32]);
    }

    public function previews()
    {
        return $this->hasMany('modx_site_tmplvar_contentvalues', 'contentid', 'id')->whereIn('tmplvarid', [23, 33, 34, 35, 36, 37, 38, 39, 40, 41]);
    }

    public function images()
    {
        return $this->hasMany('modx_site_tmplvar_contentvalues', 'contentid', 'id')->whereIn('tmplvarid', [12, 13, 14, 15, 16, 17, 18, 19, 20, 21]);
    }

    // Products
    public function product()
    {
        return $this->hasOne('Modx_Product', 'id', 'id');
    }

    public function gallery()
    {
        return $this->hasMany('Modx_Product_Images', 'product_id', 'id')->where('parent', '>', '0');
    }

    public function instruction()
    {
        return $this->hasOne('modx_site_tmplvar_contentvalues', 'contentid', 'id')->where('tmplvarid', 49);
    }

    public function settings()
    {
        return $this->hasOne('modx_site_tmplvar_contentvalues', 'contentid', 'id')->where('tmplvarid', 11);
    }


}