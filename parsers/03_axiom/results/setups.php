<?php return array (
  0 => 
  array (
    'id' => 12,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'KIA Sorento',
    'longtitle' => '',
    'description' => '',
    'alias' => 'kia',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => '<p>Установка AXiOM Car Vision 1000 на KIA Sorento.</p>
<p> </p>',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 0,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1366794945,
    'editedby' => 1,
    'editedon' => 1377788873,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1366794900,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/kia/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
      0 => 
      array (
        'id' => 50,
        'tmplvarid' => 12,
        'contentid' => 12,
        'value' => 'ustanovka/dab6328s-960.jpg',
      ),
      1 => 
      array (
        'id' => 51,
        'tmplvarid' => 13,
        'contentid' => 12,
        'value' => 'ustanovka/7ab6328s-960.jpg',
      ),
      2 => 
      array (
        'id' => 52,
        'tmplvarid' => 14,
        'contentid' => 12,
        'value' => 'ustanovka/f6b6328s-960.jpg',
      ),
    ),
  ),
  1 => 
  array (
    'id' => 13,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Elantra J4',
    'longtitle' => '',
    'description' => '',
    'alias' => 'elantra',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => '<p>Установка AXiOM Car Vision 1000 на автомобиль Elantra J4.</p>
<p>Очень интересное решение одного из владельцев AXiOM. Сам блок был установлен в очечник автомобиля, а изображение с регистратора выведено на штатную систему автомобиля.</p>',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 1,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1366795161,
    'editedby' => 1,
    'editedon' => 1377788885,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1366795140,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/elantra/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
      0 => 
      array (
        'id' => 13,
        'tmplvarid' => 12,
        'contentid' => 13,
        'value' => 'ustanovka/IMAG0794_1200px.jpg',
      ),
      1 => 
      array (
        'id' => 14,
        'tmplvarid' => 13,
        'contentid' => 13,
        'value' => 'ustanovka/IMAG0795_1200px.jpg',
      ),
    ),
  ),
  2 => 
  array (
    'id' => 111,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Opel Mokka',
    'longtitle' => '',
    'description' => '',
    'alias' => 'opel',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => '<p>Установка AXiOM Car Vision 1000 на автомобиль Opel Mokka.</p>
<p> </p>',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 2,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 13,
    'createdon' => 1368019339,
    'editedby' => 1,
    'editedon' => 1377788896,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1368269880,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/opel/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
      0 => 
      array (
        'id' => 174,
        'tmplvarid' => 12,
        'contentid' => 111,
        'value' => 'ustanovka/5.JPG',
      ),
      1 => 
      array (
        'id' => 175,
        'tmplvarid' => 13,
        'contentid' => 111,
        'value' => 'ustanovka/6.JPG',
      ),
    ),
  ),
  3 => 
  array (
    'id' => 237,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Opel Corsa D5',
    'longtitle' => '',
    'description' => '',
    'alias' => 'opel-corsa-d5',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => '<p>Установка AXiOM Car Vision 1000 на Opel Corsa D5</p>',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 4,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1378201285,
    'editedby' => 1,
    'editedon' => 1378201596,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1378201260,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/opel-corsa-d5/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
      0 => 
      array (
        'id' => 254,
        'tmplvarid' => 12,
        'contentid' => 237,
        'value' => 'ustanovka/Opel Corsa D5/88d53f1562da.jpg',
      ),
      1 => 
      array (
        'id' => 255,
        'tmplvarid' => 13,
        'contentid' => 237,
        'value' => 'ustanovka/Opel Corsa D5/195205ecbe37.jpg',
      ),
      2 => 
      array (
        'id' => 256,
        'tmplvarid' => 14,
        'contentid' => 237,
        'value' => 'ustanovka/Opel Corsa D5/42fcbd79b253.jpg',
      ),
      3 => 
      array (
        'id' => 257,
        'tmplvarid' => 15,
        'contentid' => 237,
        'value' => 'ustanovka/Opel Corsa D5/f08c8c389075.jpg',
      ),
      4 => 
      array (
        'id' => 258,
        'tmplvarid' => 16,
        'contentid' => 237,
        'value' => 'ustanovka/Opel Corsa D5/62c9343a49d0.jpg',
      ),
      5 => 
      array (
        'id' => 259,
        'tmplvarid' => 17,
        'contentid' => 237,
        'value' => 'ustanovka/Opel Corsa D5/39c18b9efe35.jpg',
      ),
      6 => 
      array (
        'id' => 260,
        'tmplvarid' => 18,
        'contentid' => 237,
        'value' => 'ustanovka/Opel Corsa D5/71ee8392dd0a.jpg',
      ),
      7 => 
      array (
        'id' => 261,
        'tmplvarid' => 19,
        'contentid' => 237,
        'value' => 'ustanovka/Opel Corsa D5/74b4c828a680.jpg',
      ),
    ),
  ),
  4 => 
  array (
    'id' => 238,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Hyundai Solaris',
    'longtitle' => '',
    'description' => '',
    'alias' => 'hyundai-solaris',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => '<p>Установка AXiOM Car Vision 1000 на Hyundai Solaris.</p>',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 6,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1378201734,
    'editedby' => 1,
    'editedon' => 1380118418,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1378201680,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/hyundai-solaris/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
      0 => 
      array (
        'id' => 273,
        'tmplvarid' => 12,
        'contentid' => 238,
        'value' => 'ustanovka/Hyundai Solaris/725c858s-480.jpg',
      ),
      1 => 
      array (
        'id' => 274,
        'tmplvarid' => 13,
        'contentid' => 238,
        'value' => 'ustanovka/Hyundai Solaris/685c858s-960.jpg',
      ),
      2 => 
      array (
        'id' => 275,
        'tmplvarid' => 14,
        'contentid' => 238,
        'value' => 'ustanovka/Hyundai Solaris/47a68d8s-960.jpg',
      ),
      3 => 
      array (
        'id' => 276,
        'tmplvarid' => 15,
        'contentid' => 238,
        'value' => 'ustanovka/Hyundai Solaris/ba5c858s-960.jpg',
      ),
    ),
  ),
  5 => 
  array (
    'id' => 239,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Land Cruiser Prado',
    'longtitle' => '',
    'description' => '',
    'alias' => 'land-cruiser-prado',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => '<p><strong style="line-height: 1.5em;">Описание от владельца Land Cruiser Prado:</strong></p>
<p>Позвольте и мне поделиться своим опытом установки ВР на Land Cruiser Prado в 95-м кузове.</p>
<p>1. Установка за зеркалом заднего вида, из салона регистратор не видно. Расстояние установки подобрано так, чтобы зеркало сохранило подвижность во всех направлениях.<br />2. Для установки пришлось зеркало на время демонтировать. Подключаем регистратор к камере выставляем горизонт и угол поворота.<br />3. Кабель ведем через стойку лобового стекла в бардачок. Откручиваем ручку.<br />4. Снимаем обшивку, прокладываем кабель.<br />5. Снимаем для удобства бардачок, остатки кабеля упаковываем вместе с реле, о котором чуть ниже.<br />6. Делаем аккуратное отверстие в бардачке и через гофру пускаем провода.<br />7. А теперь, собственно, о реле и Кулибине. В схему дополнительно введено реле контроля напряжения. Реле делает одну простую вещь - прекращает подачу питания на регистратор, если напряжение на аккумуляторе упадет ниже 11.5 вольт. Это позволяет перевести регистратор в полностью автономный режим записи по движению. Т.е. есть движение - пишем, стоим и перед регистратором никто не маячит - не пишем. Стоим очень долго и напряжение падает ниже 11.5 вольт - реле выключает регистратор. По опыту могу сказать, что 5 дней стоянки регистратор записывал без проблем. На более длительный срок оставлять машину пока не приходилось.<br /><br /><br />Вот собственно и все. Теперь все, что нужно, это только достать карту памяти из регистратора при необходимости скинуть что-нибудь на ПК. В остальном он живет своей полностью самостоятельной жизнью, внимания к своей персоне не требует.</p>',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 8,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1378202230,
    'editedby' => 1,
    'editedon' => 1463467708,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1378202220,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/land-cruiser-prado/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
      0 => 
      array (
        'id' => 262,
        'tmplvarid' => 12,
        'contentid' => 239,
        'value' => 'ustanovka/Land Cruiser Prado/DSC03791.jpg',
      ),
      1 => 
      array (
        'id' => 263,
        'tmplvarid' => 13,
        'contentid' => 239,
        'value' => 'ustanovka/Land Cruiser Prado/DSC03798.jpg',
      ),
      2 => 
      array (
        'id' => 264,
        'tmplvarid' => 14,
        'contentid' => 239,
        'value' => 'ustanovka/Land Cruiser Prado/DSC03795.jpg',
      ),
      3 => 
      array (
        'id' => 265,
        'tmplvarid' => 15,
        'contentid' => 239,
        'value' => 'ustanovka/Land Cruiser Prado/1.png',
      ),
    ),
  ),
  6 => 
  array (
    'id' => 170,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Opel Astra',
    'longtitle' => '',
    'description' => '',
    'alias' => 'opel-astra',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => '<p>Установка Axiom Car Vision 1000 на автомобиль Opel Astra.</p>
<p><strong style="line-height: 1.5em;">Описание от владельца Opel Astra:</strong></p>
<p><span style="line-height: 1.5em;">Мой автомобиль OpelAstraj</span><br /><span style="line-height: 1.5em;">Видеорегистратор AXIOMCARVISION 1000, версия с USB charging кабелем (покупала в конце декабря 2012г.)</span></p>
<p><span style="line-height: 1.5em;">Сразу оговорюсь, что на оригинальность подключения не претендую, ввиду того, что у меня предыдущая версия данного регистратора (еще без кнопки контроллера), штатной комплектацией моего автомобиля не предусмотрен экран магнитолы, на который можно было бы вывести изображение с регистратора, и у меня нет ненужного подочечника, в котором можно было бы разместить сам блок регистратора с монитором(((. </span><br /><strong>Но даже без всего вышеперечисленного, видеорегистратор очень удобен в эксплуатации и практически незаметен в салоне автомобиля.</strong><br />Мой вариант подключения: камеру установили на лобовом стекле как бы в продолжение зеркала заднего вида, местом хранения самого блока питания с монитором определили бардачок, провода от камеры спрятали под обшивкой правой стойки и потолка, автомобильную зарядку от регистратора вывели наружу к прикуривателю, а сам провод от зарядки спрятали под панелью. Была мысль подключить регистратор к автомобильному прикуривателю изнутри, но в итоге не стали прятать зарядку, т.к. гнездо прикуривателя 12В, а зарядка 5В, поэтому во избежании замыканий, вывели наружу (т.е. я ее каждый раз должна вставлять в гнездо прикуривателя или оттуда не вынимать, но это тоже не проблема).</p>',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 5,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1377794945,
    'editedby' => 1,
    'editedon' => 1378200965,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1377794940,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/opel-astra/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
      0 => 
      array (
        'id' => 245,
        'tmplvarid' => 14,
        'contentid' => 170,
        'value' => 'ustanovka/Opel astra/0F5A3228_превью.jpg',
      ),
      1 => 
      array (
        'id' => 247,
        'tmplvarid' => 16,
        'contentid' => 170,
        'value' => 'ustanovka/Opel astra/0F5A3242_превью.jpg',
      ),
      2 => 
      array (
        'id' => 243,
        'tmplvarid' => 12,
        'contentid' => 170,
        'value' => 'ustanovka/Opel astra/0F5A3215_превью.jpg',
      ),
      3 => 
      array (
        'id' => 244,
        'tmplvarid' => 13,
        'contentid' => 170,
        'value' => 'ustanovka/Opel astra/0F5A3221_превью.jpg',
      ),
      4 => 
      array (
        'id' => 246,
        'tmplvarid' => 15,
        'contentid' => 170,
        'value' => 'ustanovka/Opel astra/0F5A3241_превью.jpg',
      ),
    ),
  ),
  7 => 
  array (
    'id' => 196,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'dasd',
    'longtitle' => '',
    'description' => '',
    'alias' => 'dasd',
    'link_attributes' => '',
    'published' => 0,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => NULL,
    'content' => 'asdasd',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 7,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 13,
    'createdon' => 1377850340,
    'editedby' => 0,
    'editedon' => 0,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 0,
    'publishedby' => 0,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/dasd/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
      0 => 
      array (
        'id' => 229,
        'tmplvarid' => 22,
        'contentid' => 196,
        'value' => 'http://www.youtube.com/watch?v=2-8gerdBw4g',
      ),
    ),
    'images' => 
    array (
      0 => 
      array (
        'id' => 228,
        'tmplvarid' => 12,
        'contentid' => 196,
        'value' => '/userfoto/1377850317kinopoisk.ru-Lilyhammer-1873765.jpg',
      ),
    ),
  ),
  8 => 
  array (
    'id' => 236,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Лада Калина',
    'longtitle' => '',
    'description' => '',
    'alias' => 'lada-kalina',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => '<p><strong style="line-height: 1.5em;">Описание от владельца:</strong></p>
<p>Что-то все инормарки, да иномарки :) Разбавлю тему, установим Car Vision 1000 на Ладу Калину (1-ая версия) хэтчбек. На оригинальность подключения также не претендую, но просто подключать регистратор к прикуривателю тоже было не интересно, завлекла технология автоматического начала записи при включении зажигания. Так как в прикуривателе всегда 12 Вольт, пришлось искать цепь, где питание подавалось бы только при зажигании. Данной цепью оказался оранжевый провод от предохранителя F1 в монтажном блоке. Соответственно это был плюс. Минус нашел рядом - черный провод.<br />От монтажного блока провод питания был подключен к кнопке-контроллеру, которую прикрепил на липучке на площадке под магнитолой.<br />С другой стороны подключил miniUSB-miniUSB кабель. Длина его оказалась слишком большой в моем случае, хватило бы и 0,5 метра (излишки спрятал под бардачком).<br />В бардачке как раз два отверстия под крепления крышки, поэтому сложностей с прокладкой кабеля не возникло.<br />Благодаря липучкам 3М Dual Lock из комплекта, основной блок регистратора прикрепил в удобном мне месте. При закрытии крышки бардачка он не мешает, кабели тоже не зажимаются.<br />HDMI-кабель уходит под обивку правой передней стойки, которая держится на 3х пистонах.<br />Чтобы кабель не попал под один из них и не повредился, как раз пригодились крепления из комплекта:<br />Сверху кабель был успешно проложен под обшивкой крыши, держатель с камерой закреплен позади зеркала заднего вида:<br />Снаружи увидеть регистратор крайне сложно, чего я и добивался :)<br />Итого получилось:<br />При включении зажигания регистратор начинает сам запись, после выключения зажигания автоматов выключается спустя заданное время в настройках<br />Обзору камера не мешает<br />Снаружи камеру очень сложно заметить и не все поймут, что это именно регистратор<br />Провода не висят, не перегибаются<br />Контроллер-кнопка на видном месте и всегда доступна<br />Основной блок прекрасно уместился в бардачке<br />P.S Не поленился сделать подробный обзор Axiom Car Vision 1000, надеюсь, он поможет тем, кто выбирает регистратор, увидеть все достоинства этой модели и сделать правильный выбор :)</p>',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 3,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1378201014,
    'editedby' => 1,
    'editedon' => 1378201227,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1378200960,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/lada-kalina/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
      0 => 
      array (
        'id' => 248,
        'tmplvarid' => 12,
        'contentid' => 236,
        'value' => 'ustanovka/Lada Kalina/20130813_134154.jpg',
      ),
      1 => 
      array (
        'id' => 249,
        'tmplvarid' => 13,
        'contentid' => 236,
        'value' => 'ustanovka/Lada Kalina/20130813_134601.jpg',
      ),
      2 => 
      array (
        'id' => 250,
        'tmplvarid' => 14,
        'contentid' => 236,
        'value' => 'ustanovka/Lada Kalina/20130813_135745.jpg',
      ),
      3 => 
      array (
        'id' => 251,
        'tmplvarid' => 15,
        'contentid' => 236,
        'value' => 'ustanovka/Lada Kalina/20130813_154035.jpg',
      ),
      4 => 
      array (
        'id' => 252,
        'tmplvarid' => 16,
        'contentid' => 236,
        'value' => 'ustanovka/Lada Kalina/20130816_103228.jpg',
      ),
      5 => 
      array (
        'id' => 253,
        'tmplvarid' => 17,
        'contentid' => 236,
        'value' => 'ustanovka/Lada Kalina/20130813_154651.jpg',
      ),
    ),
  ),
  9 => 
  array (
    'id' => 240,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'BMW X1',
    'longtitle' => '',
    'description' => '',
    'alias' => 'bmw-x1',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => '<p>Установка AXiOM Car Vision 1000 на BMW X1.</p>',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 9,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1380117008,
    'editedby' => 1,
    'editedon' => 1380118321,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1380117000,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/bmw-x1/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
      0 => 
      array (
        'id' => 269,
        'tmplvarid' => 12,
        'contentid' => 240,
        'value' => 'ustanovka/X1/DSC_0002.JPG',
      ),
      1 => 
      array (
        'id' => 270,
        'tmplvarid' => 13,
        'contentid' => 240,
        'value' => 'ustanovka/X1/DSC_0004.JPG',
      ),
      2 => 
      array (
        'id' => 271,
        'tmplvarid' => 14,
        'contentid' => 240,
        'value' => 'ustanovka/X1/DSC_0008.JPG',
      ),
      3 => 
      array (
        'id' => 272,
        'tmplvarid' => 15,
        'contentid' => 240,
        'value' => 'ustanovka/X1/DSC_0013.JPG',
      ),
    ),
  ),
  10 => 
  array (
    'id' => 246,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Volkswagen Tiguan',
    'longtitle' => '',
    'description' => '',
    'alias' => 'volkswagen-tiguan',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => 'Установка AXiOM Car Vision 1000 на Volkswagen Tiguan. </br>
Фотографии взяты из обзора на <a href="http://www.drive2.ru/cars/volkswagen/tiguan/tiguan/vnuks/journal/2063548/">Drive2.ru</a>',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 10,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1380883832,
    'editedby' => 1,
    'editedon' => 1380885102,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1380883800,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/volkswagen-tiguan/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
      0 => 
      array (
        'id' => 277,
        'tmplvarid' => 12,
        'contentid' => 246,
        'value' => 'ustanovka/Tiguan/3fe87f8s-480.jpg',
      ),
      1 => 
      array (
        'id' => 278,
        'tmplvarid' => 13,
        'contentid' => 246,
        'value' => 'ustanovka/Tiguan/65e87f8s-960.jpg',
      ),
      2 => 
      array (
        'id' => 279,
        'tmplvarid' => 14,
        'contentid' => 246,
        'value' => 'ustanovka/Tiguan/90187f8s-960.jpg',
      ),
      3 => 
      array (
        'id' => 280,
        'tmplvarid' => 15,
        'contentid' => 246,
        'value' => 'ustanovka/Tiguan/b3e87f8s-480.jpg',
      ),
      4 => 
      array (
        'id' => 281,
        'tmplvarid' => 16,
        'contentid' => 246,
        'value' => 'ustanovka/Tiguan/b5e87f8s-960.jpg',
      ),
      5 => 
      array (
        'id' => 282,
        'tmplvarid' => 17,
        'contentid' => 246,
        'value' => 'ustanovka/Tiguan/b8187f8s-960.jpg',
      ),
      6 => 
      array (
        'id' => 283,
        'tmplvarid' => 18,
        'contentid' => 246,
        'value' => 'ustanovka/Tiguan/ffe87f8s-480.jpg',
      ),
    ),
  ),
  11 => 
  array (
    'id' => 283,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'BMW X6',
    'longtitle' => '',
    'description' => '',
    'alias' => 'bmw-x6',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => 'Установка AXiOM Car Vision 1000 на BMW X6',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 11,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1383640113,
    'editedby' => 1,
    'editedon' => 1383643925,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1383640080,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/bmw-x6/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
      0 => 
      array (
        'id' => 387,
        'tmplvarid' => 16,
        'contentid' => 283,
        'value' => 'ustanovka/bmwx6/IMG_6800.jpg',
      ),
      1 => 
      array (
        'id' => 386,
        'tmplvarid' => 15,
        'contentid' => 283,
        'value' => 'ustanovka/bmwx6/IMG_6799.jpg',
      ),
      2 => 
      array (
        'id' => 385,
        'tmplvarid' => 14,
        'contentid' => 283,
        'value' => 'ustanovka/bmwx6/IMG_6796.jpg',
      ),
      3 => 
      array (
        'id' => 384,
        'tmplvarid' => 13,
        'contentid' => 283,
        'value' => 'ustanovka/bmwx6/IMG_6789.jpg',
      ),
      4 => 
      array (
        'id' => 383,
        'tmplvarid' => 12,
        'contentid' => 283,
        'value' => 'ustanovka/bmwx6/IMG_6788.jpg',
      ),
    ),
  ),
  12 => 
  array (
    'id' => 356,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Toyota Camry',
    'longtitle' => '',
    'description' => '',
    'alias' => 'toyota-camry',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => '<p>Установка AXiOM Car Vision 1000 на Toyota Camry.</p>
<p> </p>',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 13,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1389701025,
    'editedby' => 1,
    'editedon' => 1389726525,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1389700980,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/toyota-camry/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
      0 => 
      array (
        'id' => 456,
        'tmplvarid' => 12,
        'contentid' => 356,
        'value' => 'ustanovka/toyotacamry/WP_20131225_001.jpg',
      ),
      1 => 
      array (
        'id' => 457,
        'tmplvarid' => 13,
        'contentid' => 356,
        'value' => 'ustanovka/toyotacamry/WP_20131225_002.jpg',
      ),
      2 => 
      array (
        'id' => 458,
        'tmplvarid' => 14,
        'contentid' => 356,
        'value' => 'ustanovka/toyotacamry/WP_20131225_005.jpg',
      ),
    ),
  ),
  13 => 
  array (
    'id' => 351,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Nissan Teana',
    'longtitle' => '',
    'description' => '',
    'alias' => 'nissan-teana',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => '<p>Установка <a href="http://www.axiom-russia.ru/produkcziya/axiom/axiom-car-vision-1100/">AXiOM Car Vision 1100</a> на Nissan Teana.</p>
<p>Отчёт об установке написал <a href= "http://videoregforum.ru/members/turistas.11364/">Turistas</a> </p>
<p>" Всем привет!!! Вот и у меня дошли руки выложить свой отчет об установке ВР в Ниссан Теана.</br>
Правда начиналось все с 1000 модели, в итоге закончилось обновленной моделью.
Причиной стало изготовления кабеля для одновременного питания и вывода видео на ГУ авто из USB 3.0 10 pin. К тому моменту как он был готов, узнал о новой версии ВР, в комплекте которого имеется такой кабель, поэтому установку отложил.</br>
Итак по порядку:</br>
Камера приклеена на лобовом стекле за зеркалом заднего вида, провод от камеры проложен между крышей и обшивкой потолка, далее по правой стойке в бардачок.</br>
В некоторых комплектациях моего авто в бардачке установлен CD/DVD проигрыватель, у меня он в ГУ, поэтому там свободная ниша. Туда я и поместил ВР. Держится с помощью липучки на двустороннем скотче из первого комплекта.
так выглядит в повседневном состоянии.</br>
Провод питания проложен в центральном тоннеле, держится с помощью крепежа из нового комплекта</br>
провод переходит в подлокотник, в котором есть розетка, USB и RCA разъемы</br>
здесь я столкнулся с самой большой проблемой. В существующие отверстия все провода проходили на ура, чего не скажешь о блоке к которому подсоединяются кнопка и провода видео выхода. Сверлить, пилить, резать и ломать не хотелось, поэтому пришлось немного помудрить. Пришлось оставить этот блок и часть провода в подлокотнике.</br>
одно нажатие кнопки и картинка с ВР на экране авто</br>
PS. кнопку устанавливать не стал, т.к. рег прекрасно работает и без нее (включается с запуском двигателя). Но возможность установить осталась.</br>
PS2. Также думаю над установкой VGA камеры, правда пока не нашел подходящее место для установки. </p>

От компании <a href="http://axiom-russia.ru">Axiom</a>: Спасибо за отчет!
',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 12,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1387887366,
    'editedby' => 1,
    'editedon' => 1405668821,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1387887360,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/nissan-teana/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
      0 => 
      array (
        'id' => 437,
        'tmplvarid' => 12,
        'contentid' => 351,
        'value' => 'ustanovka/nissan_teana/axiom_car_vision_1100_1.jpg',
      ),
      1 => 
      array (
        'id' => 438,
        'tmplvarid' => 13,
        'contentid' => 351,
        'value' => 'ustanovka/nissan_teana/axiom_car_vision_1100_2.jpg',
      ),
      2 => 
      array (
        'id' => 439,
        'tmplvarid' => 14,
        'contentid' => 351,
        'value' => 'ustanovka/nissan_teana/axiom_car_vision_1100_3.jpg',
      ),
      3 => 
      array (
        'id' => 440,
        'tmplvarid' => 15,
        'contentid' => 351,
        'value' => 'ustanovka/nissan_teana/axiom_car_vision_1100_4.jpg',
      ),
      4 => 
      array (
        'id' => 441,
        'tmplvarid' => 16,
        'contentid' => 351,
        'value' => 'ustanovka/nissan_teana/axiom_car_vision_1100_5.jpg',
      ),
      5 => 
      array (
        'id' => 442,
        'tmplvarid' => 17,
        'contentid' => 351,
        'value' => 'ustanovka/nissan_teana/axiom_car_vision_1100_6.jpg',
      ),
      6 => 
      array (
        'id' => 443,
        'tmplvarid' => 18,
        'contentid' => 351,
        'value' => 'ustanovka/nissan_teana/axiom_car_vision_1100_7.jpg',
      ),
      7 => 
      array (
        'id' => 444,
        'tmplvarid' => 19,
        'contentid' => 351,
        'value' => 'ustanovka/nissan_teana/axiom_car_vision_1100_8.jpg',
      ),
      8 => 
      array (
        'id' => 445,
        'tmplvarid' => 20,
        'contentid' => 351,
        'value' => 'ustanovka/nissan_teana/axiom_car_vision_1100_9.jpg',
      ),
      9 => 
      array (
        'id' => 446,
        'tmplvarid' => 21,
        'contentid' => 351,
        'value' => 'ustanovka/nissan_teana/axiom_car_vision_1100_10.jpg',
      ),
    ),
  ),
  14 => 
  array (
    'id' => 357,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Subaru Forester',
    'longtitle' => '',
    'description' => '',
    'alias' => 'subaru-forester',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => '<p>Установка AXiOM Car Vision 1100 на Subaru Forester.</p>
<p> </p>',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 14,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1389701998,
    'editedby' => 1,
    'editedon' => 1389726707,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1389701940,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/subaru-forester/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
      0 => 
      array (
        'id' => 459,
        'tmplvarid' => 12,
        'contentid' => 357,
        'value' => 'ustanovka/subaruforester/фотография (2).JPG',
      ),
      1 => 
      array (
        'id' => 460,
        'tmplvarid' => 13,
        'contentid' => 357,
        'value' => 'ustanovka/subaruforester/фотография (3).JPG',
      ),
      2 => 
      array (
        'id' => 461,
        'tmplvarid' => 14,
        'contentid' => 357,
        'value' => 'ustanovka/subaruforester/фотография (4).JPG',
      ),
      3 => 
      array (
        'id' => 462,
        'tmplvarid' => 15,
        'contentid' => 357,
        'value' => 'ustanovka/subaruforester/фотография (5).JPG',
      ),
    ),
  ),
  15 => 
  array (
    'id' => 363,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Toyota Corolla',
    'longtitle' => '',
    'description' => '',
    'alias' => 'corolla-e150',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => 'Установка Axiom Car Vision 1100 на Toyota Corolla</br>
Кузов: E150',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 15,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1390309646,
    'editedby' => 1,
    'editedon' => 1390309811,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1390309620,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/corolla-e150/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
      0 => 
      array (
        'id' => 466,
        'tmplvarid' => 12,
        'contentid' => 363,
        'value' => 'ustanovka/corolla_e150/20140119_132606.jpg',
      ),
      1 => 
      array (
        'id' => 467,
        'tmplvarid' => 13,
        'contentid' => 363,
        'value' => 'ustanovka/corolla_e150/20140119_132709.jpg',
      ),
      2 => 
      array (
        'id' => 468,
        'tmplvarid' => 14,
        'contentid' => 363,
        'value' => 'ustanovka/corolla_e150/20140119_132729.jpg',
      ),
      3 => 
      array (
        'id' => 469,
        'tmplvarid' => 15,
        'contentid' => 363,
        'value' => 'ustanovka/corolla_e150/20140119_132740.jpg',
      ),
      4 => 
      array (
        'id' => 470,
        'tmplvarid' => 16,
        'contentid' => 363,
        'value' => 'ustanovka/corolla_e150/20140119_132748.jpg',
      ),
    ),
  ),
  16 => 
  array (
    'id' => 623,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'BMW X1 F48',
    'longtitle' => '',
    'description' => '',
    'alias' => 'bmw-x1-f48',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => 'Поставил регистратор на новенькую бмв х1 f48, подключился сзади к прикуривателю) все работает отлично)',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 48,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1463466627,
    'editedby' => 1,
    'editedon' => 1463467115,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1459462440,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/bmw-x1-f48/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
      0 => 
      array (
        'id' => 727,
        'tmplvarid' => 12,
        'contentid' => 623,
        'value' => 'ustanovka/bmw_х1_f48/1.jpg',
      ),
      1 => 
      array (
        'id' => 728,
        'tmplvarid' => 13,
        'contentid' => 623,
        'value' => 'ustanovka/bmw_х1_f48/2.jpg',
      ),
      2 => 
      array (
        'id' => 729,
        'tmplvarid' => 14,
        'contentid' => 623,
        'value' => 'ustanovka/bmw_х1_f48/3.jpg',
      ),
      3 => 
      array (
        'id' => 730,
        'tmplvarid' => 15,
        'contentid' => 623,
        'value' => 'ustanovka/bmw_х1_f48/4.jpg',
      ),
      4 => 
      array (
        'id' => 731,
        'tmplvarid' => 16,
        'contentid' => 623,
        'value' => 'ustanovka/bmw_х1_f48/5.jpg',
      ),
      5 => 
      array (
        'id' => 732,
        'tmplvarid' => 17,
        'contentid' => 623,
        'value' => 'ustanovka/bmw_х1_f48/6.jpg',
      ),
    ),
  ),
  17 => 
  array (
    'id' => 387,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Renault Kangoo II',
    'longtitle' => '',
    'description' => '',
    'alias' => 'renault-kangoo-ii',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => 'Установка Axiom Car Vision 1100 на Renault Kangoo II</p>

Комментарий от владельца:</br>
Отличная возможность скрытой установки на автомобиль, БЕЗ подушки безопасности переднего пассажира. В данной комплектации имеется лючок, свободное пространство под которым, позволяет идеально расположить ВР. В комплектациях с подушкой пассажира, в этом месте расположен выключатель подушки. При этом украсть ВР из автомобиля, не открывая пассажирскую дверь будет невозможно (в отличии от установки в бардачке). Настроить регистратор и извлечь флэшку, гораздо проще, чем при монтаже в бардачке.',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 16,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 65,
    'createdon' => 1392050233,
    'editedby' => 1,
    'editedon' => 1393276921,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1392055920,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/renault-kangoo-ii/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
      0 => 
      array (
        'id' => 474,
        'tmplvarid' => 12,
        'contentid' => 387,
        'value' => 'ustanovka/renault/20140208_205950.jpg',
      ),
      1 => 
      array (
        'id' => 475,
        'tmplvarid' => 13,
        'contentid' => 387,
        'value' => 'ustanovka/renault/20140208_222247.jpg',
      ),
      2 => 
      array (
        'id' => 476,
        'tmplvarid' => 14,
        'contentid' => 387,
        'value' => 'ustanovka/renault/kango5.jpg',
      ),
      3 => 
      array (
        'id' => 477,
        'tmplvarid' => 15,
        'contentid' => 387,
        'value' => 'ustanovka/renault/20140210_152435.jpg',
      ),
      4 => 
      array (
        'id' => 478,
        'tmplvarid' => 16,
        'contentid' => 387,
        'value' => 'ustanovka/renault/20140210_152459.jpg',
      ),
      5 => 
      array (
        'id' => 479,
        'tmplvarid' => 17,
        'contentid' => 387,
        'value' => 'ustanovka/renault/20140210_153413.jpg',
      ),
      6 => 
      array (
        'id' => 487,
        'tmplvarid' => 18,
        'contentid' => 387,
        'value' => 'ustanovka/renault/kango1.jpg',
      ),
      7 => 
      array (
        'id' => 488,
        'tmplvarid' => 19,
        'contentid' => 387,
        'value' => 'ustanovka/renault/kango2.jpg',
      ),
      8 => 
      array (
        'id' => 489,
        'tmplvarid' => 20,
        'contentid' => 387,
        'value' => 'ustanovka/renault/kango3.jpg',
      ),
      9 => 
      array (
        'id' => 490,
        'tmplvarid' => 21,
        'contentid' => 387,
        'value' => 'ustanovka/renault/kango5.jpg',
      ),
    ),
  ),
  18 => 
  array (
    'id' => 579,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'AUDI А6/Аllroad 2000-2005 г.в.',
    'longtitle' => '',
    'description' => '',
    'alias' => 'audi-a6/allroad-2000-2005-g.v',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => '<p><strong style="line-height: 1.5em;">Описание от владельца AUDI А6/Аllroad 2000-2005 г.в.:</strong></p>
<p>Уже поставил. До этого у меня стоял ACV-Q2 и провода были спрятаны, шли под обшивкой крыши, потом под обшивкой левой стойки и для подключения питания у меня от блока предохранителей был выведен провод с постоянным питанием и с гнездом прикуривателя. Сначала думал у Axiom поставить основной блок в бардачок, он что бы снять провод питания от старого регистратора пришлось снимать левую обшивку стойки и кожух над педалями. Когда снял кожух, то осознал что в нем есть ниша куда кладу только навигатор когда снимаю и куда блок регистратора отлично встанет и останется место для навигатора. И благо. что все разобрано было так и проложил провод от камеру к блоку под обшивкой крыши и обшивкой левой стойки. 
Значит в задней части ниши ножом прорезал большое прямоугольное отверстие для подводки проводов, проковырял дырочку для кнопки и все поставил (перед приклеиванием пришлось ножом соскабливать в местах приклейки скотчем ворсистость в нише, т.к. к ворсу скотч не приклеится) 
Кнопка удачно встала и её отлично видно если посмотреть вниз, как будто там для неё площадку сделали конструкторы в нише.
Нигде провода не торчат и если надо вынуть основной блок что бы чтото посмотреть, то из-за большой дырки провода вынимаются вслед за ним сантиметров на 20 и при обратной установке провода заходят обратно под кожух и бардачок остался свободен от проводов.</p><br><br>
<p>Теперь как то не привычно, за 3 года привык, что справа от зеркала висит регистратор и закрывает часть окна и не дает правый козырек до конца опустить, кажется как то пустовато.

P.S. Один нюанс, когда будете в нише приклеивать крепление основного блока, смотрите, что бы неподвижная лапка крепления была к стенке, а то будет проблемно основной блок доставать и ставить.</p>',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 30,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1411113463,
    'editedby' => 1,
    'editedon' => 1411113569,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1378202220,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/audi-a6/allroad-2000-2005-g.v/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
      0 => 
      array (
        'id' => 581,
        'tmplvarid' => 16,
        'contentid' => 579,
        'value' => 'ustanovka/AUDI/d87de3ca1e40.jpg',
      ),
      1 => 
      array (
        'id' => 582,
        'tmplvarid' => 17,
        'contentid' => 579,
        'value' => 'ustanovka/AUDI/f6dcb16edd4c.jpg',
      ),
      2 => 
      array (
        'id' => 584,
        'tmplvarid' => 12,
        'contentid' => 579,
        'value' => 'ustanovka/AUDI/308fa8844bc4.jpg',
      ),
      3 => 
      array (
        'id' => 585,
        'tmplvarid' => 13,
        'contentid' => 579,
        'value' => 'ustanovka/AUDI/45648475972f.jpg',
      ),
      4 => 
      array (
        'id' => 586,
        'tmplvarid' => 14,
        'contentid' => 579,
        'value' => 'ustanovka/AUDI/a85c631a487a.jpg',
      ),
      5 => 
      array (
        'id' => 587,
        'tmplvarid' => 15,
        'contentid' => 579,
        'value' => 'ustanovka/AUDI/b75df3065108.jpg',
      ),
    ),
  ),
  19 => 
  array (
    'id' => 414,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Volvo XC-90',
    'longtitle' => '',
    'description' => '',
    'alias' => 'volvo-xc-90',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => '<b>Из обзора с сайта clubvolvo.ru:</b></br>
Ссылка на источник - <a href="http://http://clubvolvo.ru/showthread.php?t=105334">Club Volvo</a></p>
<b>В общем, кому надоело снимать туда и сюда видеорегистраторы, читаем:</b></br>

Мною был приобретен вот такой экземпляр -  <a href="http://www.axiom-russia.ru/produkcziya/axiom/axiom-car-vision-1100/">Car Vision 1100</a><br />
Кому интересны подробные характерестики, читайте здесь: <a href="http://www.axiom-russia.ru/produkcziya/axiom/axiom-car-vision-1100/">AXiOM Beyond Digital</a><br />
Установка заняла 30 минут, сделал фотографии только основных моментов, так что не обессудьте:<br />
Примеряем и приклеиваем площадку камеры(важно чтобы уместилась на чёрной окрашенной площадке лобового стекла)<br />
<img src="http://www.axiom-russia.ru/ustanovka/volvo/1.jpg" border="0" alt="" /><br />
<br />
Снимаем пластиковую крышку и окончательно приклеиваем кронштейн(не забываем обезжирить и в машине должно быть тепло)<br />
<br />
<img src="http://www.axiom-russia.ru/ustanovka/volvo/2.jpg" border="0" alt="" /><br />
<br />
далее вставляем разъём miniHDMI в камеру(его лучше зафиксировать 2-х сторонним скотчем 3м ) и ставим камеру <br />
<br />
<img src="http://www.axiom-russia.ru/ustanovka/volvo/3.jpg" border="0" alt="" /><br />
<br />
Имеющимися в комплекте фиксаторами крепим под обшивкой кабель от камеры(не забываем обезжиривать)<br />
<br />
<img src="http://www.axiom-russia.ru/ustanovka/volvo/4.jpg" border="0" alt="" /><br />
<br />
Защёлкиваем кабель<br />
<br />
<img src="http://www.axiom-russia.ru/ustanovka/volvo/5.jpg" border="0" alt="" /><br />
<br />
Далее таким же фиксатором крепим кабель в углу лобового стекла<br />
<br />
<img src="http://www.axiom-russia.ru/ustanovka/volvo/6.jpg" border="0" alt="" /><br />
<br />
Обезжириваем, приклеиваем и заводим под накладку(накладку не откручивал, отжал пластмассовой линейкой этого хватило)<br />
<br />
<img src="http://www.axiom-russia.ru/ustanovka/volvo/7.jpg" border="0" alt="" /><br />
<br />
Частично снимаем резиновый уплотнитель правой двери и прокладываем кабель далее по стойке вниз<br />
<br />
<img src="http://www.axiom-russia.ru/ustanovka/volvo/8.jpg" border="0" alt="" /><br />
<br />
Далее снимаем боковую крышку торпеды(на которой находится выключатель подушки пассажира, разъёмы не отсоединять ,а то запишется ошибка по подушкам-стирать только компом с видой)<br />
<br />
<img src="http://www.axiom-russia.ru/ustanovka/volvo/9.jpg" border="0" alt="" /><br />
<br />
далее кабель можно пропихнуть в уже имеющееся окно бардачка, а можно снять его, как быстрее не знаю у меня получилось завести провод питания и камеры регистратора без снятия , главное чтобы кабеля не ложились на острые края металлических деталей торпеды и не мешали закрытию бардачка<br />
<br />
<img src="http://www.axiom-russia.ru/ustanovka/volvo/10.jpg" border="0" alt="" /><br />
<br />
Далее надо подключить регистратор к бортовой сети бегемота, здесь возможны варианты:<br />
- если надо что бы Car Vision работал и на стоянке (есть функция записи по движению) то надо подключать к прикуривателю, тот который в багажнике(на нём постоянно есть питание)<br />
- если нет(как в моём случае), то я подключил от прикуривателя, который находится в подлокотнике(на нём нет питания когда авто закрыт),так вот снимаем подлокотник благо это на ХС-90 занимает одну секунду, снимаем площадку под ним(её обратно не ставим),в подлокотнике подпаиваем, ранее купленный разветвитель, для прикуривателя, фиксируем его внутри 2-х сторонним скотчем, подключаем к нему провод питания видеорегистратора и выводим через имеющееся окно внизу<br />
<br />
<img src="http://www.axiom-russia.ru/ustanovka/volvo/11.jpg" border="0" alt="" /><br />
<br />
Выведенные провода питания убираем в образовавшееся окно(снятая крышка под подлокотника) тоннель и задвигаем подлокотник в своё штатное место<br />
<br />
<img src="http://www.axiom-russia.ru/ustanovka/volvo/12.jpg" border="0" alt="" /><br />
<br />
Снимаем боковину тоннеля(она на защёлках)-надо дёргать с края на себя<br />
<br />
<img src="http://www.axiom-russia.ru/ustanovka/volvo/13.jpg" border="0" alt="" /><br />
<br />
и прокладываем кабель питания по направлению к бардачку<br />
<br />
<img src="http://www.axiom-russia.ru/ustanovka/volvo/14.jpg" border="0" alt="" /><br />
<br />
Вытащив провод питания в бардачке через имеющееся окно ,займёмся креплением видеорегистратора,оно изначально предполагает крепиться на 2х сторонний скотч, но так как у нас там ворсистая поверхность и по опыту решил разу закрепить дополнительно 2мя саморезами(если крепление отклеится и будет висеть на проводах разъёмы долго не протянут и начнутся глюки)для этого отклеил бархатную накладку ,засверлил и привернул<br />
<br />
<img src="http://www.axiom-russia.ru/ustanovka/volvo/15.jpg" border="0" alt="" /><br />
<br />
Наклеиваем &quot;бархатку&quot; обрато и наклеиваем &quot;липу&quot; на кронштейн и сам блок регистратора - есть в комплекте(чтоб крепче держался потому как он висит ввех ногами)<br />
<br />
<br />
<img src="http://www.axiom-russia.ru/ustanovka/volvo/16.jpg" border="0" alt="" /><br />
<br />
Ставим блок на место ,предварительно подключив провода<br />
<br />
<img src="http://www.axiom-russia.ru/ustanovka/volvo/17.jpg" border="0" alt="" /><br />
<br />
Проверяем, включаем зажигание и <b>вуаля</b>!<br />
<br />
<img src="http://www.axiom-russia.ru/ustanovka/volvo/18.jpg" border="0" alt="" /><br />
<br />
Осталось настроить камеру(только вверх вниз поэтому не зря привлёк ваше внимание куда крепить площадку под камеру).Регистратор включается и выключается сам(при включении/выключении зажигания), проводов в салоне не дрыгается,установлен скрытно, при желании(я себе купил)можно вставить карту памяти с модулем WIFI и установить приложение в телефон(IOS или Android)и копировать смотреть ролики вообще не вытаскивая регистратор.Ни чего не где не пилил, по этому если снять регистратор, то не останется следов его установки.<br />
Изнутри вот так<br />
<br />
<img src="http://www.axiom-russia.ru/ustanovka/volvo/19.jpg" border="0" alt="" /><br />
<br />
Снаружи так<br />
<br />
<img src="http://www.axiom-russia.ru/ustanovka/volvo/20.jpg" border="0" alt="" /><br />
<br />
как то так, кому интересно спрашивайте<br />
<br />
<br />
<b>P.S.</b> перед тем как всё делать подключите и проверьте работоспособность - был неисправен кабель. да не реклама хотел отметить сервис, кабель производитель прислал в течение трёх дней бесплатно и обещают помогать прошивками, хотя покупал через интернет магазин сотмаркет.</p>',
    'richtext' => 1,
    'template' => 2,
    'menuindex' => 17,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1394621936,
    'editedby' => 1,
    'editedon' => 1394826774,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1394621880,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/volvo-xc-90/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
    ),
  ),
  20 => 
  array (
    'id' => 423,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Hyundai Accent',
    'longtitle' => '',
    'description' => '',
    'alias' => 'hyundai-accent',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => 'Отчет об установке от нашего счастливого обладателя AXiOM Car Vision 1100!</p>

Доброго дня,вечера,утра! </br>
Не знаю, конкурс кончился или нет, но это и неважно. </br>
 Просто хотелось бы выложить парочку фото об установке Axiom car vision 1100 в автомобиль Hyundai Accent! </br>
Итак, регистратор приобрел 31 декабря 2013 года- подарок себе сделал!</br> 
По установке думаю,что всё и так понятно, легко,затруднений не вызвало! </br>
Небольшое описание процесса: </br>
Провод от камеры легко прячется под обшивку крыши,отгибаешь чуть край и пальцами его туда засовываешь. 
Далее снимаешь накладку боковой стойки и видишь там проводку штатную,к ней закрепляешь свой провод от камеры. Клипсы из комплекта использовал все только для крепления проводов в бардачке, в остальных местах использовал стяжки. Кнопку решил не ставить,т.к. когда рег.включается и так слышно,да и места подходящего не придумал,дизайн у неё на любителя!</br>
Кстати камеру сначала хотел ставить со стороны водителя,но там зеркало ей мешало для регулировки. </br>
Далее, преобразующий блок,приклеил на 2-х сторонний скотч под рулем внутри пластиковой обшивки. </br>
А запитал родным штекером от прикуривателя. Задержку выключения рега поставил на 10 сек. поэтому когда заводишь двиг. запись не останавливается.',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 18,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1394828108,
    'editedby' => 1,
    'editedon' => 1394944016,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1394828100,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/hyundai-accent/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
      0 => 
      array (
        'id' => 504,
        'tmplvarid' => 12,
        'contentid' => 423,
        'value' => 'ustanovka/accent/1.jpg',
      ),
      1 => 
      array (
        'id' => 505,
        'tmplvarid' => 13,
        'contentid' => 423,
        'value' => 'ustanovka/accent/2.jpg',
      ),
      2 => 
      array (
        'id' => 506,
        'tmplvarid' => 14,
        'contentid' => 423,
        'value' => 'ustanovka/accent/3.jpg',
      ),
      3 => 
      array (
        'id' => 507,
        'tmplvarid' => 15,
        'contentid' => 423,
        'value' => 'ustanovka/accent/4.jpg',
      ),
      4 => 
      array (
        'id' => 508,
        'tmplvarid' => 16,
        'contentid' => 423,
        'value' => 'ustanovka/accent/5.jpg',
      ),
      5 => 
      array (
        'id' => 509,
        'tmplvarid' => 17,
        'contentid' => 423,
        'value' => 'ustanovka/accent/6.jpg',
      ),
    ),
  ),
  21 => 
  array (
    'id' => 438,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'BMW X3',
    'longtitle' => '',
    'description' => '',
    'alias' => 'bmw-x3',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => 'Установка AXiOM Car Vision 1100 на BMW X3',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 19,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1396199065,
    'editedby' => 1,
    'editedon' => 1396199547,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1396199040,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/bmw-x3/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
      0 => 
      array (
        'id' => 527,
        'tmplvarid' => 12,
        'contentid' => 438,
        'value' => 'ustanovka/bmwx3/IMG_1010.JPG',
      ),
      1 => 
      array (
        'id' => 528,
        'tmplvarid' => 13,
        'contentid' => 438,
        'value' => 'ustanovka/bmwx3/IMG_1011.JPG',
      ),
      2 => 
      array (
        'id' => 529,
        'tmplvarid' => 14,
        'contentid' => 438,
        'value' => 'ustanovka/bmwx3/IMG_1012.JPG',
      ),
      3 => 
      array (
        'id' => 530,
        'tmplvarid' => 15,
        'contentid' => 438,
        'value' => 'ustanovka/bmwx3/IMG_1013.JPG',
      ),
    ),
  ),
  22 => 
  array (
    'id' => 453,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Ford Focus II',
    'longtitle' => '',
    'description' => '',
    'alias' => 'ford-focus-ii',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => 'Установка AXiOM Car Vision 1100 на Ford Focus II</p>


					Общий привет!<br />
Долго прилаживался, куда бы поставить записывающий блок, чтобы и доступ к нему был, и иногда настройки менять, и слышно было его пищание при сменене режимов и контролировать его работу. Пытался установить в подлокотник – не хватило длины кабеля камеры, в бардачёк – неудобно. Поездил с висящими проводами больше 2 месяцев и решил в конце всё же поставить в солнцезащитный козырёк. Тем более ни у кого такого варианта ещё не было. Может кому пригодится.<br />
Основное – как подготовить солнцезащитный козырёк. Оказалось всё возможно.<br />
Авто Форд ФокусII, AXIOM CarVision1100.<br />
Снимаем зеркало. Надрезаем оболочку. Козырёк из пенопласта (или монтажной пены). Режется канцелярским ножом.<br />
<span style="font-family: \'Times New Roman\'">Засада! Обнаруживаем металлический каркас: металлическая обмеднённая проволока.</span><br />

<img src="http://videoregforum.ru/data/attachments/2/2770-c81c6772092d5077d620814f796fb83d.jpg" alt="IMAG0114.jpg">
</a>
			
<br />
<span style="font-family: \'Times New Roman\'">Частично откусываем, частично рассверливаем сначала маленьким сверлом, потом побольше, чтоб отломить. Заусеницы обработать наждаком на перфораторе (примерно таким).<br />
<br />

	
<img src="http://videoregforum.ru/data/attachments/2/2771-1ec39aeaa6848bda82b3b4675860b9dc.jpg" alt="IMAG0125.jpg"></a>
			
</span><br />
Проделываем необходимые каналы и выводим 2 кабеля (на камеру и на контроллер).<br />

	
	<img src="http://videoregforum.ru/data/attachments/2/2772-bd83ac39b14162050a7879492c438751.jpg" alt="IMAG0139.jpg"/></a>
			
<br />
Подгоняем и вставляем записывающий блок. Защёлкиваем обратно зеркало. Всё! Крепим козырёк. Кабель на камеру – под потолок. Кабель на контроллер – под накладку левой стойки  (у кого руль справа - соотвественно под правую стойку).<br />

	
	<img src="http://videoregforum.ru/data/attachments/2/2773-b56b7931cec78d0cdfeb44582e05c3b6.jpg" alt="IMAG0145.jpg"
		/></a>
			
<br />
Все кнопки на виду. Все звуки над ухом, слышно так же, как из подлокотника. Зеркало на месте. Поднял козырёк - ничего не видно, одна камера под стеклом за зеркалом заднего вида.<br />
Установка камеры – стандартно, на лобовом стекле.<br />

	
	<img src="http://videoregforum.ru/data/attachments/2/2785-e385acb9d2b60ec187796d2be427bafc.jpg" alt="IMAG0148.jpg"/></a>
			
<br />
Кнопка некоторое время стояла (по временному варианту) на торпедо перед глазами. Удобно. Однако вечером и ночью всё же била в глаза. Время дневное установил с 7.00 до 8.00. Не помогло. Пришлось уменьшить яркость, вставил наклейку из изоленты. Не очень красиво получилось. Однако удобно. Попробуйте.<br />

	
	<img src="http://videoregforum.ru/data/attachments/2/2786-8c8a4ef3805e1b9253a9acf3caf21e6d.jpg" alt="IMAG0146.jpg" /></a>
			
<br />
Чтобы не сверлить торпедо установил вместо заглушки. Временно. В перспективе по следам форумчан установлю штатную Фордовскую  кнопку, тем более есть схемка распиновки джека кнопки (спасибо LEONавто  mihail-zzz) .<br />

	
	<img src="http://videoregforum.ru/attachments/2/2787-c84f2f35e02c8e7b5ee83871e087b647.jpg" alt="IMAG0150.jpg"/></a>
			
<br />
Самое геморройное – протащить кабель на контроллер, кнопку и питание. Долго ждал тёплых дней, чтобы оторвать накладку левой стойки и торпеды под рулём. Клипсы для кабеля уже есть на стойке, удобные для закрепления кабеля.<br />
Вблизи:<br />

	
<img src="http://videoregforum.ru/data/attachments/2/2788-e076793ce985d11f5d5dbf2b7480950a.jpg" alt="IMAG0151.jpg" /></a>
			
<br />
Вдоль всей стойки:<br />

	
<img src="http://videoregforum.ru/data/attachments/2/2789-85c9f36eb66a55e7578ebeb1962d9d24.jpg" alt="IMAG0152.jpg" /></a>
			
<br />
После протаскивания проводов ставим всё на место. Сами провода (напомню, их 2: на камеру и к контроллеру) из козырька прячем в кабель канал (не во всех автомагазинах есть).<br />
Результат:<br />

	
	<img src="http://videoregforum.ru/data/attachments/2/2790-258fe9a11d602c3c47040e32305dc659.jpg" alt="IMAG0154.jpg"/></a>
			
<br />
<br />

	
	<img src="http://videoregforum.ru/data/attachments/2/2791-ece8abb2ff2c9db6f37e55664ebed3ea.jpg" alt="IMAG0155.jpg" /></a>
			
<br />
<br />

	
<img src="http://videoregforum.ru/data/attachments/2/2792-bbe9884ec7c743c6bc0e7b330f0dd5a3.jpg" alt="IMAG0156.jpg"/></a>
			
<br />
<br />
В итоге перед глазами:<br />

	
<img src="http://videoregforum.ru/data/attachments/2/2793-d84437cf86c1ddf823c7669647dee812.jpg" alt="IMAG0159.jpg" /></a>
			
<br />
<br />
<br />
Одна проблема: доступ к внешней карте памяти и установка обратно после вынимания из гнезда записывающего блока затруднены. На перспективу ищу 10 пиновый miniUSB кабель вер.3.0 для подключения в гнездо вместо аксионовского. В имеющемся используется 7 пинов (7 проводов). Если вместо него подключить комп через USB-miniUSB кабель, то используется в разъёме остальные 3 пина. Глубоко не вникал, но думаю можно будет организовать одновременный доступ к картам памяти ВР из ноутбука без перетыкания штатных проводов, сделав ответвитель USB (7 имеющихся проводов + 3 дополнительных)  для ноутбука. Тогда при необходимости (конечно изредка) подключаете ноутбук и  Вы в каратах памяти – хоть копируй для ГИБДД, хоть удаляй, хоть просматривай. В крайнем случае потребуется только питание ВР отключить.<br />
По кабелям важно мнение Молотка.<br />
<br />
Уважаемый Молоток!<br />
Возможно ли изготовление такой причуды в промышленном производстве, т.е. кабель на контроллер усовершенствовать. Тогда не надо будет доставать ВР НИКОГДА. Прячьте его хоть в недра торпедо, хоть в багажник. Как то так.<br />
Буду рад, если данный опус кому нибудь пригодится.
				',
    'richtext' => 1,
    'template' => 2,
    'menuindex' => 20,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1397742935,
    'editedby' => 1,
    'editedon' => 1398077486,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1398077486,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/ford-focus-ii/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
    ),
  ),
  23 => 
  array (
    'id' => 454,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Opel Antara',
    'longtitle' => '',
    'description' => '',
    'alias' => 'opel-antara',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => 'Установка AXiOM Car Vision 1100 на Opel Antara:</p>

Отчет об установке:</p>

На днях наконец  установил видеорегистратор  <a href="http://www.axiom-russia.ru/produkcziya/axiom/axiom-car-vision-1100">Axiom Car Vision 1000</a> в машину  Opel Antara. В базовом комплекте был (не выполнявший свою работу) кабель питания с кнопкой, но с помощью Максима (Molotok) я получил новый (с разветвителем и выносной кнопкой)<br />
<br />
 Для установки потребовались: тестер, канцелярский/строительный нож, паяльник (флюс, олово) отвертка крестовая, клеевой пистолет, клей супермомент, лист материала АМг-5М 115х192х1,5 (гильотина, гибочный станок) , и нежелание что-либо необратимо сломать (аккуратность)<br />
<br />
Итак, начнем.. Настраиваем глубину резкости камеры (с завода не настроена) – для этого включаем ВР и, глядя на экран, медленно вращаем объектив камеры (на резьбе) сначала в одну, а затем в другую сторону, добиваясь четкой картинки. Затем фиксируем положение объектива каплей суперклея. В качестве альтернативы (чтобы лучше было видно)  можно было подключить блок с камерой к телевизору- и смотреть на нем, но HDMI кабеля у меня в комплекте не оказалось, а покупать его на один раз не стал..<br />
Камеру установил за зеркалом заднего вида- с улицы: спереди совсем не заметно, сбоку – если только приглядываться, и знать, что искать.. </br>
	
<img src="http://videoregforum.ru/attachments/1-jpg.3130/" height="300" width="380">
		
<br />
<br />
Выбираем место для установки блока ВР. Большинство владельцев устанавливают в бардачок – и я абсолютно с ними солидарен. Но, оказалось, что у Антары не очень удобное для этого устройство бардачка - открывается не только крышка, а откидывается он  весь (сделан в виде литого  ящика) Если туда ставить блок – провода при открывании/закрывании будут постоянно сгибаться/разгибаться.  Решено установить блок в районе ног штурмана. <br />

	
	
<img src="http://videoregforum.ru/attachments/3-jpg.3132/" height="300" width="380">
		
 <br />
Сначала установил повыше – понял, что при замене салонного фильтра проблемы будут. Опустил вниз. Приклеиваем держатель блока.<br />
<br />
Питание решил взять с плафона. <br />

	
	<img src="http://videoregforum.ru/attachments/4-jpg.3134/" height="300" width="380">
		
 <br />
Снимаем плафон. Для этого, сначала поддев отверткой, снимаем прозрачную пластиковую накладку. Под ней - 2 самореза. Откручиваем. Открываем очечник. Еще 2 самореза. Откручиваем.  Тянем блок плафона со стороны салона вниз и на себя .<br />

	
	<img src="http://videoregforum.ru/attachments/6-jpg.3135/" height="300" width="380" />
		
 <br />
 Вытянули репку.. Со стороны лобового стекла у плафона 2 пластиковых язычка (главное- их не сломать). Вынимаем плафон вниз и на себя.  Прозваниваем и запоминаем (помечаем маркером)  контакты, к которым подключаться планируем.<br />

	
	<img src="http://videoregforum.ru/attachments/9-jpg.3133/" height="300" width="380" />
		
 <br />
 Отключаем разъемы питания, снимаем, несем добычу домой.<br />
Вилка питания у ВР выполняет только функцию подключения (понижение напряжения происходит в самом блоке) – отрезаем и убираем ее в сундук с проводами до лучших времен.<br />

	
	<img src="http://videoregforum.ru/attachments/13-jpg.3136/" height="300" width="380" />
		
 <br />
 Зачищаем концы, лудим, наносим флюс на дорожки плафона. Ждем. Паяем, соблюдая полярность.<br />

	
	<img src="http://videoregforum.ru/attachments/17-jpg.3138/" height="300" width="380"" />
		
 <br />
 Провод на плафоне в двух местах фиксируем на термоклей.<br />

	
	<img src="http://videoregforum.ru/attachments/18-jpg.3137/" height="300" width="380" />
		
 <br />
<br />
Несем всю конструкцию к машине.<br />
<br />
Подключаем штекеры питания, протаскиваем под обшивкой провод питания ВР , вставляем и крепим плафон.<br />

	
	<img src="http://videoregforum.ru/attachments/20-jpg.3139/" height="300" width="380"/>
		
 <br />
Снимаем декоративную накладку стойки. Для этого отковыриваем заглушку с надписью AirBag- под ней обнаруживаем саморез, откручиваем, тянем накладку на себя, снимаем..<br />

	
	<img src="http://videoregforum.ru/attachments/21-jpg.3140/" height="300" width="380"/>
		
 <br />
Прокладываем провода от камеры и от плафона к основному блоку –  под обшивкой крыши, ведем по стойке , закрепив на фиксаторах (они в комплекте с ВР идут ), <br />

	
	<img src="http://videoregforum.ru/attachments/23-1-jpg.3142/" height="300" width="380"/>
		
 <br />
 под уплотнителем дверного проема <br />

	
	<img src="http://videoregforum.ru/attachments/24-jpg.3143/" height="300" width="380"/>
		
 <br />
и вниз.<br />

	
	<img src="http://videoregforum.ru/attachments/25-jpg.3141/" height="300" width="380"/>
		
 <br />
На стойке также закрепляем и разветвитель – от него (под торпедо) ведем тонкий  провод с кнопкой индикации в левый нижний угол лобового стекла. <br />

	
	<img src="http://videoregforum.ru/attachments/26-jpg.3145/" height="300" width="380" />
		
 <br />
Кнопка мигает красным цветом во время работы ВР. Чтобы во время движения (особенно ночью) она не мельтешила красным перед глазами - тонируем ее до состояния «если приглядется- видно, что мигает»<br />

	
	<img src="http://videoregforum.ru/attachments/27-jpg.3144/" height="300" width="380"/>
		
 <br />
Крепим накладку стойки.<br />
<br />
Для защиты ВР от шаловливых ног штурмана, разметил  и согнул из АМг-5М (типа дюрали)  перекошенный со всех сторон (иначе бы не встал - панель-то кривая) кожух. Покрасил его в черный цвет  (чтобы снаружи в глаза не бросался). Во избежании травматизма, на верхнюю кромку одел окантовочный резиновый профиль (на фото не показан).<br />

	
	<img src="http://videoregforum.ru/attachments/29-jpg.3147/" height="300" width="380"/>
		
 <br />
 Кожух встал на держатель с блоком в натяг  + еще прижался ковриком (дополнительную фиксацию в виде 3М скотча делать не пришлось).<br />

	
	<img src="http://videoregforum.ru/attachments/30-jpg.3146/" height="300" width="380" />
		
 <br />
Осталась еще идея в схему включить реле контроля напряжения (362.3787-01)  - чтобы АКБ в ноль не высаживал, но когда она в голову пришла (идея) все было уже собрано. Так что как-нибудь  попозже сколхозничаю..</p>







<b>Дополнительный вариант установки:</b> </p>				
<img src="http://videoregforum.ru/attachments/v-bardachke-jpg.3264/" height="600" width="700"/></p>
		
 
	
	<img src="http://videoregforum.ru/attachments/vnutri-avto-jpg.3265/" height="600" width="700"></p>
		
 
	
	<img src="http://videoregforum.ru/attachments/knopka-jpg.3266/"height="600" width="700"></p>
		
 
	
	<img src="http://videoregforum.ru/attachments/s-otkrytoj-kryshkoj-jpg.3267/" aheight="600" width="700"/></p>
		

Примерно так. В итоге следов установки или лишних &quot;дырок&quot; нет совсем.<br />
С улицы сфотать не удалось, т.к. стекло подтонировано и бликовало. Если нужно, потом выложу.',
    'richtext' => 1,
    'template' => 2,
    'menuindex' => 21,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1397743106,
    'editedby' => 1,
    'editedon' => 1398078936,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1398078780,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/opel-antara/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
    ),
  ),
  24 => 
  array (
    'id' => 460,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'WV Polo Sedan',
    'longtitle' => '',
    'description' => '',
    'alias' => 'wv-polo-sedan',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => 'Установка AXiOM Car Vision 1100 на WV Polo.',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 22,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1399298709,
    'editedby' => 1,
    'editedon' => 1399298753,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1399298700,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/wv-polo-sedan/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
      0 => 
      array (
        'id' => 546,
        'tmplvarid' => 12,
        'contentid' => 460,
        'value' => 'ustanovka/polo/P1020243.JPG',
      ),
      1 => 
      array (
        'id' => 547,
        'tmplvarid' => 13,
        'contentid' => 460,
        'value' => 'ustanovka/polo/P1020246.JPG',
      ),
      2 => 
      array (
        'id' => 548,
        'tmplvarid' => 14,
        'contentid' => 460,
        'value' => 'ustanovka/polo/P1020250.JPG',
      ),
    ),
  ),
  25 => 
  array (
    'id' => 480,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'FORD EXSPLORER',
    'longtitle' => '',
    'description' => '',
    'alias' => 'ford-exsplorer',
    'link_attributes' => '',
    'published' => 0,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => 'как поставить  Axiom Car Vision 1100',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 23,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 152,
    'createdon' => 1404058429,
    'editedby' => 1,
    'editedon' => 1404912863,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 0,
    'publishedby' => 0,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/ford-exsplorer/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
    ),
  ),
  26 => 
  array (
    'id' => 483,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Skoda Octavia A5 FL',
    'longtitle' => '',
    'description' => '',
    'alias' => 'skoda-octavia-a5-fl',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => 'Представляю Вашему вниманию мой вариант установки Axiom Car Vision 1000 в Skoda Octavia A5 FL.<br />
Сразу заметил что ездить с этими висящими проводами не удобно, по этому воспользовался идеей запитать ВР через штатный блок предохранителей, видел несколько отчетов по ее реализации. Сама идея не плохая, но мне не понравилось как она была реализована, в общем позвал на помощь друга (он любит с проводами возиться), и общими усилиями мы ее доработали. Получилось следующее...<br />
<br />
Получилась такая схема:
<br />
<br />
<img src="http://videoregforum.ru/attachments/sxema-jpg.1874" width="300" height="300" alt="Axiom Car Vision 1000 в Skoda Octavia A5 FL">
<br />
<br />
Теперь как она собиралась и из чего состоит.<br />
Был куплен кабель 2м, штекер под пайку
<br />
<br />
<img src="http://videoregforum.ru/attachments/shteker-jpg.1875/" width="300" height="300" alt="Axiom Car Vision 1000 в Skoda Octavia A5 FL">
<br />
<br />
Маленьким предохранитель автомобильный это питание, клемма и трубка для термоусадки
<br />
<br />
<img src="http://videoregforum.ru/attachments/minus-i-pitanie-jpg.1877/" width="300" height="300" alt="Axiom Car Vision 1000 в Skoda Octavia A5 FL">
<br />
<br />
Большой предохранитель автомобильный был впаян в провод питания,заизолирован термоусадкой и на липучку аккуратно прилеплен к блоку, это видно на 1 фото. Если вдруг перегорит, то его легко можно будет заменить.
<br />
<br />
<img src="http://videoregforum.ru/attachments/preloxranitel-jpg.1878/" width="300" height="300" alt="Axiom Car Vision 1000 в Skoda Octavia A5 FL">
<br />
<br />
Сам блок ВР у меня в бардачке, там все понятно описывать не буду, я как и все протянул провод за правой передней стойкой и дальше уже провод который идет на кнопку-контроллер я пропустил через центральный тоннель мимо переключалки КПП, тут же собрал лишний провод в пучок и закрепил его, чтобы не болтался. На фото видно
<br />
<br />
<img src="http://videoregforum.ru/attachments/tonel-jpg.1881/" width="300" height="300" alt="Axiom Car Vision 1000 в Skoda Octavia A5 FL">
<br />
<br />
И выпустил провод с обратной стороны. Тут же видно и провод питания, который я провел от блока предохранителей.
<br />
<br />
<img src="http://videoregforum.ru/attachments/provoda-jpg.1882/" width="300" height="300" alt="Axiom Car Vision 1000 в Skoda Octavia A5 FL">
<br />
<br />
Аккуратненько укладываем провод и прячем все лишнее
<br />
<br />
<img src="http://videoregforum.ru/attachments/prokladka-jpg.1883/" width="300" height="300" alt="Axiom Car Vision 1000 в Skoda Octavia A5 FL">
<br />
<br />
Ну и в завершении примеряем место установки кнопки.
<br />
<br />
<img src="http://videoregforum.ru/attachments/knopka-jpg.1884/" width="300" height="300" alt="Axiom Car Vision 1000 в Skoda Octavia A5 FL">
<br />
<br />
Ну вот и все, теперь ВР работает от зажигания, на все ушло около 3 часов))
<br />
<br />',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 24,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1404912770,
    'editedby' => 1,
    'editedon' => 1405668634,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1404912900,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/skoda-octavia-a5-fl/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
    ),
  ),
  27 => 
  array (
    'id' => 484,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Honda CR-V в 1-м кузове',
    'longtitle' => '',
    'description' => '',
    'alias' => 'honda-cr-v-v-1-m-kuzove',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => 'Машина - Honda CR-V, в 1-м кузове.<br />
Цель - установка с минимальными вмешательствами в конструкцию автомобиля т.к. времени и нормального теплого гаража для работ у меня нет. Т.о. мою установку правильно отнести к разряду "Установка" :) никаких Кулибиных...<br />
Поехали:<br />
Камеру установил за зеркало. Как видно получилось очень плотно. Но это особенности угла наклона стекла и места установки зеркала.
<br />
<br />
<img src="http://videoregforum.ru/attachments/img_20130806_093008-jpg.1932/" width="300" height="300" alt="Axiom Car Vision в Honda CR-V в 1-м кузове">
<br />
<br />
Снаружи выглядит вот так. Специально старался найти фотографию на которой МАКСИМАЛЬНО заметна камера
<br />
<br />
<img src="http://videoregforum.ru/attachments/img_20131106_105233-jpg.1933/" width="300" height="300" alt="Axiom Car Vision в Honda CR-V в 1-м кузове">
<br />
<br />
Провод от камеры проложен по правой стойке в бардачек, где на заднюю стенку прилеплен сам блок регистратора
<br />
<br />
<img src="http://videoregforum.ru/attachments/img_20130806_092923-jpg.1934/" width="300" height="300" alt="Axiom Car Vision в Honda CR-V в 1-м кузове">
<br />
<br />
Для минимизации перелома проводов использовал держатели из комплекта. Изначально блок был прилеплен на липучку, но позавчера заменил на держатель который был в коробке. Заменил потому что липучка, вместо того чтобы разлепляться, постоянно отклеивалась от стенки бардачка.
<br />
USB провод между блоком регистратора и кнопкой управления пропущен за центральной консолью.
Кнопка установлена рядом с замком зажигания. Вот так:
<br />
<br />
<img src="http://videoregforum.ru/attachments/img_20131106_105045-jpg.1935/" width="300" height="300" alt="Axiom Car Vision в Honda CR-V в 1-м кузове">
<br />
<br />
Провод питания убран под торпедо. Подключен к штатной проводке на которой появляется 12В в момент включения зажигания. Т.о. гнездо прикуривателя свободно и исключается забывчивость, когда гнездо используется для чего-то ещё. Да и в целом гораздо удобнее.
<br />
Кнопка управления разместилась так, что при движении перекрывается рулем, но достаточно немного отклонится в сторону и можно контролировать пишет регистратор или нет. То есть по глазам не мерцает, но и отвлекаться от дороги не нужно. Примерно вот так:
<br />
<br />
<img src="http://videoregforum.ru/attachments/img_20131106_105021-jpg.1937/" width="300" height="300" alt="Axiom Car Vision в Honda CR-V в 1-м кузове">
<br />
<br />
Саму камеру уплотнил как это было описано в теме. В качестве уплотнителя использовал подложку под ламинад. Получилось вот так:
<br />
<br />
<img src="http://videoregforum.ru/attachments/img_20131013_151736-jpg.1936/" width="300" height="300" alt="Axiom Car Vision в Honda CR-V в 1-м кузове">
<br />
<br />
В принципе все!<br />
Как и писал, на кулибинство не претендовал изначально, хотя была шальная мысль врезать кнопку в одну из штатных заглушек. Но понял что скорее всего испохаблю и то и другое и вообще ничего не получится. Поэтому оставил затею.
<br />
Есть примеры ночного и дневного видео, но не знаю нужно ли выкладывать? Его и так вроде много. Из недочетов. На дневном видео наблюдается двоение фонарей на машинах. Ночное зернистое.<br />
Собственно все.',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 25,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1404914142,
    'editedby' => 1,
    'editedon' => 1405668707,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 0,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/honda-cr-v-v-1-m-kuzove/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
    ),
  ),
  28 => 
  array (
    'id' => 485,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Mazda Premacy / Mazda 5',
    'longtitle' => '',
    'description' => '',
    'alias' => 'mazda-premacy-/-mazda-5',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => 'Тут не будет много фото самого процесса, только результат. Установкой полностью доволен, сверлить пилить, паять, ломать ничего не пришлось, провода все протянуты скрытно, ничего нигде не висит, не болтается. Из салона видно только камеру и кнопку. Единственно что не удалось поставить камеру совсем скрытно, из-за конструкции корпуса датчиков на лобовом стекле. <br />
В итоге камеру приклеил к корпусу датчиков, провод протянул под обшивкой потолка, далее по правой стойке в бардачок.
<br />
<br />
<img src="http://i63.fastpic.ru/big/2014/0513/4f/cc39214eb9775fa3d7153cf811cdad4f.jpg" width="300" height="300" alt="Axiom Car Vision 1100 в Mazda Premacy / Mazda5">
<br />
<br />
Питание подключил в гнездо прикуривателя, не стал резать провода и прицеплять без розетки ,как нибудь в другой раз сделаю.
<br />
<br />
<img src="http://i63.fastpic.ru/big/2014/0513/4f/00cb94c554c3178d8eb4c1622145a14f.jpg" width="300" height="300" alt="Axiom Car Vision 1100 в Mazda Premacy / Mazda5">
<br />
<br />
Преобразователь установил внутри консоли селектора АКП, оттуда легко протянуть все провода. В бардачок к основному блоку, к магнитоле для вывода изображения на монитор, и провод кнопки для установки её рядом с левой ногой водителя.
<br />
<br />
<img src="http://i64.fastpic.ru/big/2014/0513/65/37fcbe9b5b4f6a73e86d3e6a5b405d65.jpg" width="300" height="300" alt="Axiom Car Vision 1100 в Mazda Premacy / Mazda5">
<br />
<br />
Снимаем накладку консоли, и снимаем её.
<br />
<br />
<img src="http://i61.fastpic.ru/big/2014/0513/35/c206c37252d15a4badf60f726a628f35.jpg" width="300" height="300" alt="Axiom Car Vision 1100 в Mazda Premacy / Mazda5">
<br />
<br />
Протягиваем провода и устанавливаем кнопку.
<br />
<br />
<img src="http://i61.fastpic.ru/big/2014/0513/59/5552b3eea0190885f8956316b32f8c59.jpg" width="300" height="300" alt="Axiom Car Vision 1100 в Mazda Premacy / Mazda5">
<br />
<br />
Устанавливаем основной блок в бардачок на кронштейне с двойным скотчем.
<br />
<br />
<img src="http://i64.fastpic.ru/big/2014/0513/53/24b80f7aee4e13f77dce42568a2fa353.jpg" width="300" height="300" alt="Axiom Car Vision 1100 в Mazda Premacy / Mazda5">
<br />
<br />
Всё что разобрали в салоне, собираем, настраиваем регистратор, включаем, и удовлетворённые проделанной работой, едем в тестовую поездку.
<br />
<br />
<img src="http://i63.fastpic.ru/big/2014/0513/75/8e02f8495ace308f34d66663a9c7b075.jpg" width="300" height="300" alt="Axiom Car Vision 1100 в Mazda Premacy / Mazda5">
<br />
<img src="http://i61.fastpic.ru/big/2014/0513/17/1e8120c592d4d45e42d7bc32b555ad17.jpg" width="300" height="300" alt="Axiom Car Vision 1100 в Mazda Premacy / Mazda5">
<br />
<br />
На большом мониторе гораздо удобнее настраивать обзор камеры и приятнее просматривать записи в случае необходимости.
<br />',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 26,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1404914652,
    'editedby' => 1,
    'editedon' => 1405668787,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 0,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/mazda-premacy-/-mazda-5/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
    ),
  ),
  29 => 
  array (
    'id' => 563,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Mazda 3',
    'longtitle' => '',
    'description' => '',
    'alias' => 'mazda-3',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => 'Еще один вариант установки. Авто Мазда 3.
Камеру встроил в пластиковый кожух датчика дождя. Для этого в нем было проделано небольшое отверстие. А саму камеру приклеил родным скотчем к лобовому стеклу.
<br />
<br />
Камера смотрится очень миниатюрно и аккуратно.
<br />
<br />
С помощью кабеля <a href="http://www.axiom-russia.ru/produkcziya/axiom/vga-provoda/">RCA (тюльпанов)</a>, идущего в комплекте, вывел изображение на головное устройство.
<br />
<br />
Блок видеорегистратора установил классическим образом, в бардачке. В родном кронштейне. Совсем не занимает места, и не мешает свободному доступу в бардачок.
Регистратор подключил к бортовой сети автомобиля, поэтому проводов не видно. Соответственно прикуриватель свободный.
<br />
<br />
Установлена идущая в комплекте дополнительная кнопка управления регистратором. На фото она между прикуривателем и кнопками подогрева сидений.
<br />
<br />
В моем случае регистратор работает круглосуточно, по датчику движения. Убедиться что регистратор пишет я всегда могу по подсветке дополнительной кнопки.
Со стороны водителя камеру совсем не видно за зеркалом. Как и снаружи автомобиля.
<br />
<br />
Установил быстро, ничего сложного в установке нет. Регистратор очень органично вписался в интерьер. А самое приятное, что его не нужно постоянно вешать и снимать!',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 27,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1406621528,
    'editedby' => 1,
    'editedon' => 1406665411,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 0,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/mazda-3/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
      0 => 
      array (
        'id' => 561,
        'tmplvarid' => 12,
        'contentid' => 563,
        'value' => 'ustanovka/mazda3/DSC_8651.JPG',
      ),
      1 => 
      array (
        'id' => 562,
        'tmplvarid' => 13,
        'contentid' => 563,
        'value' => 'ustanovka/mazda3/DSC_8652.JPG',
      ),
      2 => 
      array (
        'id' => 563,
        'tmplvarid' => 16,
        'contentid' => 563,
        'value' => 'ustanovka/mazda3/DSC_8975.jpg',
      ),
      3 => 
      array (
        'id' => 564,
        'tmplvarid' => 14,
        'contentid' => 563,
        'value' => 'ustanovka/mazda3/DSC_8656.JPG',
      ),
      4 => 
      array (
        'id' => 565,
        'tmplvarid' => 15,
        'contentid' => 563,
        'value' => 'ustanovka/mazda3/DSC_8665.JPG',
      ),
      5 => 
      array (
        'id' => 566,
        'tmplvarid' => 17,
        'contentid' => 563,
        'value' => 'ustanovka/mazda3/DSC_8676.JPG',
      ),
    ),
  ),
  30 => 
  array (
    'id' => 564,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Peugeot 3008',
    'longtitle' => '',
    'description' => '',
    'alias' => 'peugeot-3008',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => 'Уже год использую регистратор на Пежо 3008, а только на днях почти полностью привел систему в порядок, скрыв провода питания и подключив кнопку.
Особо ничего выдумывать не стал с размещением:<br />
1. Блок регистратора в «бардачок».
<br />
<br />
2. Камеру на кожух датчика освещенности и дождя.
<br />
<br />
3. Питание в туннель между ручкой переключения передач и подлокотником. Подключил через клемник к проводам прикуривателя.
<br />
<br />
4. Коммутационный блок в переднюю панель за магнитолу.
<br />
<br />
5. Кнопку слева снизу от руля. 
<br />
<br />
6. Провод от камеры пропустил через кожух датчика освещенности и дождя и проложил за потолочной обшивкой, за правой передней стойкой.<br /><br />
Провод до камеры у меня остался с 1000, поэтому звук с камеры не пишется. Но при апгрейде было лениво его перепрокладывать. Думаю при следующем апгрейде всё же поменяю. ))<br />
Т.к. планирую использовать регистратор в режиме постоянной записи, то подключил питание через приобретённый дополнительно блок отсечки при проседании напряжения ниже установленного уровня, но к цепи постоянного питания не подключил ещё. Жду свободного времени.<br /><br />
Что пришлось дырявить, резать:<br /><br />
а) крышку бардачка - для подведения проводов в место крепления регистратора;<br />
б) кожух датчика освещенности и дождя - для протаскивания провода от камеры;<br />
в) панельку - для протаскивания провода от кнопки;<br />
г) проводку к прикуривателю — для подключения отсекателя.<br /><br />
В качестве замечания хочу отметить, что держатель блока регистратор на липучке из комплекта от жары отклеивается от стенки бардачка. Переклеил на ЗМ-овский красный — всё ок.',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 28,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1406621965,
    'editedby' => 1,
    'editedon' => 1406664857,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 0,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/peugeot-3008/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
      0 => 
      array (
        'id' => 567,
        'tmplvarid' => 12,
        'contentid' => 564,
        'value' => 'ustanovka/peugeot3008/IMG_20140727_135946.jpg',
      ),
      1 => 
      array (
        'id' => 568,
        'tmplvarid' => 13,
        'contentid' => 564,
        'value' => 'ustanovka/peugeot3008/IMG_20140729_080050.jpg',
      ),
      2 => 
      array (
        'id' => 569,
        'tmplvarid' => 14,
        'contentid' => 564,
        'value' => 'ustanovka/peugeot3008/IMG_20140727_160057.jpg',
      ),
      3 => 
      array (
        'id' => 570,
        'tmplvarid' => 15,
        'contentid' => 564,
        'value' => 'ustanovka/peugeot3008/IMG_20140727_160141.jpg',
      ),
    ),
  ),
  31 => 
  array (
    'id' => 565,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Honda Freed',
    'longtitle' => '',
    'description' => '',
    'alias' => 'honda-freed',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => 'Регистратор  <a href="http://www.axiom-russia.ru/produkcziya/axiom/axiom-car-vision-1100">Axiom Car Vision 1100</a> установлен в более доступное место ,но достаточно скрытно.<br />
Все остальное спрятано под торпедой!
<br />',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 29,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1406622452,
    'editedby' => 1,
    'editedon' => 1406665290,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 0,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/honda-freed/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
      0 => 
      array (
        'id' => 571,
        'tmplvarid' => 12,
        'contentid' => 565,
        'value' => 'ustanovka/Honda Freed/DSC03673.JPG',
      ),
      1 => 
      array (
        'id' => 572,
        'tmplvarid' => 13,
        'contentid' => 565,
        'value' => 'ustanovka/Honda Freed/DSC03674.JPG',
      ),
      2 => 
      array (
        'id' => 573,
        'tmplvarid' => 14,
        'contentid' => 565,
        'value' => 'ustanovka/Honda Freed/DSC03675.JPG',
      ),
      3 => 
      array (
        'id' => 574,
        'tmplvarid' => 15,
        'contentid' => 565,
        'value' => 'ustanovka/Honda Freed/DSC03676.JPG',
      ),
      4 => 
      array (
        'id' => 575,
        'tmplvarid' => 16,
        'contentid' => 565,
        'value' => 'ustanovka/Honda Freed/DSC03677.JPG',
      ),
      5 => 
      array (
        'id' => 576,
        'tmplvarid' => 17,
        'contentid' => 565,
        'value' => 'ustanovka/Honda Freed/DSC03679.JPG',
      ),
      6 => 
      array (
        'id' => 577,
        'tmplvarid' => 18,
        'contentid' => 565,
        'value' => 'ustanovka/Honda Freed/DSC03680.JPG',
      ),
      7 => 
      array (
        'id' => 578,
        'tmplvarid' => 19,
        'contentid' => 565,
        'value' => 'ustanovka/Honda Freed/DSC03683.JPG',
      ),
      8 => 
      array (
        'id' => 579,
        'tmplvarid' => 20,
        'contentid' => 565,
        'value' => 'ustanovka/Honda Freed/DSC03686.JPG',
      ),
    ),
  ),
  32 => 
  array (
    'id' => 598,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Audi Q3',
    'longtitle' => '',
    'description' => '',
    'alias' => 'audi-q3',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => '',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 32,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1459460190,
    'editedby' => 0,
    'editedon' => 0,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1459460190,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/audi-q3/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
      0 => 
      array (
        'id' => 616,
        'tmplvarid' => 22,
        'contentid' => 598,
        'value' => 'http://www.youtube.com/embed/m8qm2cqDCP4',
      ),
    ),
    'images' => 
    array (
    ),
  ),
  33 => 
  array (
    'id' => 599,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'BMW 5series',
    'longtitle' => '',
    'description' => '',
    'alias' => 'bmw-5series',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => '<p><img src="ustanovka/bmw5series/1.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/bmw5series/2.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/bmw5series/3.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/bmw5series/4.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/bmw5series/5.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/bmw5series/6.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/bmw5series/7.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/bmw5series/8.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/bmw5series/9.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/bmw5series/10.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/bmw5series/11.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/bmw5series/12.jpg" alt="" width="50%" height="50%" /></p><br>',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 33,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1459460446,
    'editedby' => 0,
    'editedon' => 0,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1459460446,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/bmw-5series/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
      0 => 
      array (
        'id' => 618,
        'tmplvarid' => 12,
        'contentid' => 599,
        'value' => 'ustanovka/bmw5series/10.jpg',
      ),
      1 => 
      array (
        'id' => 619,
        'tmplvarid' => 13,
        'contentid' => 599,
        'value' => 'ustanovka/bmw5series/1.jpg',
      ),
      2 => 
      array (
        'id' => 620,
        'tmplvarid' => 14,
        'contentid' => 599,
        'value' => 'ustanovka/bmw5series/2.jpg',
      ),
      3 => 
      array (
        'id' => 621,
        'tmplvarid' => 15,
        'contentid' => 599,
        'value' => 'ustanovka/bmw5series/3.jpg',
      ),
      4 => 
      array (
        'id' => 622,
        'tmplvarid' => 16,
        'contentid' => 599,
        'value' => 'ustanovka/bmw5series/4.jpg',
      ),
      5 => 
      array (
        'id' => 623,
        'tmplvarid' => 17,
        'contentid' => 599,
        'value' => 'ustanovka/bmw5series/5.jpg',
      ),
      6 => 
      array (
        'id' => 624,
        'tmplvarid' => 18,
        'contentid' => 599,
        'value' => 'ustanovka/bmw5series/6.jpg',
      ),
      7 => 
      array (
        'id' => 625,
        'tmplvarid' => 19,
        'contentid' => 599,
        'value' => 'ustanovka/bmw5series/7.jpg',
      ),
      8 => 
      array (
        'id' => 626,
        'tmplvarid' => 20,
        'contentid' => 599,
        'value' => 'ustanovka/bmw5series/8.jpg',
      ),
      9 => 
      array (
        'id' => 627,
        'tmplvarid' => 21,
        'contentid' => 599,
        'value' => 'ustanovka/bmw5series/9.jpg',
      ),
    ),
  ),
  34 => 
  array (
    'id' => 600,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Nissan Teana J32',
    'longtitle' => '',
    'description' => '',
    'alias' => 'nissan-teana-j32',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => 'Хочу поделиться вариантом установки в Nissan Teana J32<br><br>

Камера рядом с зеркалом. Если бы не большой разъем на камере, то можно бы было придумать крепление , совмещенное с креплением зеркала - был бы идеальный вариант. Провод по потолку и левой стойке.<br><br>

Блок питания рядом с салонным блоком предохранителей. Питание через "мама"-прикуриватель, подключен который к предохранителю.
Блок ВР в отделении для мелочи - идеально поместится. Удобно нажимать на блокировку файла на ходу, не отвлекаясь от дороги.
Хотел протянуть провод видео на ГУ, но передумал. На постоянной основе в нём нет смысла, а если надо оперативно подключить, то провод в подлокотнике лежит, а разъем видео выхода рядом с регистратором в отделении для мелочи. Т.е. можно оперативно подключить. Тем более, что у меня не разблокировано видео в движении на ГУ. Да и отверстие в нише подлокотника делать не хотелось.<br><br>

Тут же (на фото видны свободные места) планирую установить кнопку, но в виде какой-нибудь штатной или близкой к ней (например, как на фото) с индикацией записи.',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 34,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1459460665,
    'editedby' => 0,
    'editedon' => 0,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1459460665,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/nissan-teana-j32/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
      0 => 
      array (
        'id' => 628,
        'tmplvarid' => 12,
        'contentid' => 600,
        'value' => 'ustanovka/nissan_teana_j32/1.jpg',
      ),
      1 => 
      array (
        'id' => 629,
        'tmplvarid' => 13,
        'contentid' => 600,
        'value' => 'ustanovka/nissan_teana_j32/2.jpg',
      ),
      2 => 
      array (
        'id' => 630,
        'tmplvarid' => 14,
        'contentid' => 600,
        'value' => 'ustanovka/nissan_teana_j32/3.jpg',
      ),
      3 => 
      array (
        'id' => 631,
        'tmplvarid' => 15,
        'contentid' => 600,
        'value' => 'ustanovka/nissan_teana_j32/4.jpg',
      ),
      4 => 
      array (
        'id' => 632,
        'tmplvarid' => 16,
        'contentid' => 600,
        'value' => 'ustanovka/nissan_teana_j32/5.jpg',
      ),
    ),
  ),
  35 => 
  array (
    'id' => 601,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Porsche 911 Carrera 4 GTS',
    'longtitle' => '',
    'description' => '',
    'alias' => 'porsche-911-carrera-4-gts',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => '',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 35,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1459460724,
    'editedby' => 0,
    'editedon' => 0,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1459460724,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/porsche-911-carrera-4-gts/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
      0 => 
      array (
        'id' => 633,
        'tmplvarid' => 22,
        'contentid' => 601,
        'value' => 'http://www.youtube.com/embed/E8wSnXj8zLk',
      ),
    ),
    'images' => 
    array (
    ),
  ),
  36 => 
  array (
    'id' => 602,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Renault Scenic Conquest',
    'longtitle' => '',
    'description' => '',
    'alias' => 'renault-scenic-conquest',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => '<p>Установка камеры на родной ножке получилась колхозной, принято решение ножку ампутировать. Ну не совсем, а где-то на половину. Но вот незадача — камера соединяется с блоком посредством mini-hdmi разъемов, а они большие. Перелопатив форум поддержки 1100го аксимома выяснил, что в кабеле только 7 проводов. Из них 4 это 2 витые пары передающие сигнал по LVDS-шине (высокоскоростной интерфейс) 2 провода питание и один непонятный провод. Ну 7 не 20, перепаяем. Сначала я разобрал разъем ножом, кусачками и феном:</p><br>

<p><img src="ustanovka/renault_scenic_conquest/1.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/renault_scenic_conquest/2.jpg" alt="" width="50%" height="50%" /></p><br>

<p>Попытки согнуть провода около разъема привели к отрыванию проводов. Пайка разъема мне к тому же совсем не понравилась. Ну ничего не поделаешь, придется наращивать гибким мегапроводом советского образца и изготовления МГТФ.</p><br>

<p><img src="ustanovka/renault_scenic_conquest/3.jpg" alt="" width="50%" height="50%" /></p><br>

<p>Теперь можно ножку укоротить, и провод согнуть прямо у разъема (без фанатизма, аккуратно)</p><br>

<p><img src="ustanovka/renault_scenic_conquest/4.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/renault_scenic_conquest/5.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/renault_scenic_conquest/6.jpg" alt="" width="50%" height="50%" /></p><br>

<p>Получилась камера с значительно меньшей высотой чем штатная.
В комплекте регистратора 2 липучки для крепления камеры, одну я потратил на колхоз, отдираем ее. Снимается плохо, 3М хорошие липучки делает.
Что бы камера была минимально заметной я продвинул ее вперед, так, чтобы она почти касалась стекла. Далее безжалостно режем колпак датчика так, чтобы у прорези спереди остался бы соединительный мостик миллиметра 3 толщиной. Прикидываем камеру, клеим так, чтобы зазоры меж корпусом и камерой были. Корпус не должен давить на камеру. Это самый трудный момент — приклеить камеру правильно. Регулировка есть только вверх-вниз, вправо-влево ее нет. Поэтому камеру нужно прилепить строго прямо. В общем тут семь раз примерь, один прилепи. Комплектный скотч для камеры у Axiom злой, и лишь коснувшись стекла липнет сразу и безвозвратно.
Далее будем изготавливать, точнее дорабатывать родную крышку датчика в которой я колхозно пропилил дырку. Технологию подсмотрел у людей которые автозвуком занимаются. Они так разные криволинейные формы обтягивают. Нужны — каркас, чулок и смола (эпоксидная или полиэфирная).
Готовим кольцо диаметром более камеры миллиметра на 2-3 из любого пластика который плавится.
Лепим камеру, одеваем пропиленный колпак, на объектив наматываем скотч (для центровки кольца), одеваем заготовленное кольцо.
Берем обрезки пластика, паяльник и неспешно привариваем кольцо вокруг объектива к колпаку, так же вокруг корпуса привариваем Г-образную планку. Процесс не фоткал. Остыло — осторожно снимаем:</p><br>

<p><img src="ustanovka/renault_scenic_conquest/7.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/renault_scenic_conquest/8.jpg" alt="" width="50%" height="50%" /></p><br>

<p>Привариваем для прочности изнутри:</p><br>

<p><img src="ustanovka/renault_scenic_conquest/9.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/renault_scenic_conquest/10.jpg" alt="" width="50%" height="50%" /></p><br>

<p>У своей женщины изымаем чулок (кусок колготки) и надеваем на форму. Вместо камеры втыкаем кусок пластилина. Нужно натянуть чулок без складок, иначе их потом еще вышпаклевывать придется. Мне помогал малярный скотч:</p><br>

<p><img src="ustanovka/renault_scenic_conquest/11.jpg" alt="" width="50%" height="50%" /></p><br>

<p>Щедро обмазываем эпоксидкой:</p><br>

<p><img src="ustanovka/renault_scenic_conquest/12.jpg" alt="" width="50%" height="50%" /></p><br>

<p>Отвердела эпоксидка, отрезаем лишний чулок:</p><br>

<p><img src="ustanovka/renault_scenic_conquest/13.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/renault_scenic_conquest/14.jpg" alt="" width="50%" height="50%" /></p><br>

<p>Примеряем получившуюся конструкцию на камеру. Если подходит — идем далее. Нет — исправляем.
Далее циклический процесс — шпаклюем, шлифуем, смотрим. Если красиво и гладко — идем дальше, нет снова шпаклюем, шлифуем, смотрим. И так пока не понравится. Я уложился в 2 итерации:</p><br>

<p><img src="ustanovka/renault_scenic_conquest/15.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/renault_scenic_conquest/16.jpg" alt="" width="50%" height="50%" /></p><br>

<p>Затем грунтуем тело грунтом по пластику и красим краской в цвет салона. Красим переднюю и заднюю часть колпака, попадание в цвет все таки не 100%:</p><br>

<p><img src="ustanovka/renault_scenic_conquest/17.jpg" alt="" width="50%" height="50%" /></p><br>

<p>Ставим колпак датчика и теперь уже камеры в машину:</p><br>

<p><img src="ustanovka/renault_scenic_conquest/18.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/renault_scenic_conquest/19.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/renault_scenic_conquest/20.jpg" alt="" width="50%" height="50%" /></p><br>

<p>Вот теперь красиво !
Затраты минимальные — баллончик с краской самая дорогая часть — 250 руб.</p>',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 36,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1459460843,
    'editedby' => 0,
    'editedon' => 0,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1459460843,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/renault-scenic-conquest/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
      0 => 
      array (
        'id' => 634,
        'tmplvarid' => 12,
        'contentid' => 602,
        'value' => 'ustanovka/renault_scenic_conquest/1.jpg',
      ),
      1 => 
      array (
        'id' => 635,
        'tmplvarid' => 13,
        'contentid' => 602,
        'value' => 'ustanovka/renault_scenic_conquest/3.jpg',
      ),
      2 => 
      array (
        'id' => 636,
        'tmplvarid' => 14,
        'contentid' => 602,
        'value' => 'ustanovka/renault_scenic_conquest/5.jpg',
      ),
      3 => 
      array (
        'id' => 637,
        'tmplvarid' => 15,
        'contentid' => 602,
        'value' => 'ustanovka/renault_scenic_conquest/7.jpg',
      ),
      4 => 
      array (
        'id' => 638,
        'tmplvarid' => 16,
        'contentid' => 602,
        'value' => 'ustanovka/renault_scenic_conquest/9.jpg',
      ),
      5 => 
      array (
        'id' => 639,
        'tmplvarid' => 17,
        'contentid' => 602,
        'value' => 'ustanovka/renault_scenic_conquest/10.jpg',
      ),
      6 => 
      array (
        'id' => 640,
        'tmplvarid' => 18,
        'contentid' => 602,
        'value' => 'ustanovka/renault_scenic_conquest/16.jpg',
      ),
      7 => 
      array (
        'id' => 641,
        'tmplvarid' => 19,
        'contentid' => 602,
        'value' => 'ustanovka/renault_scenic_conquest/18.jpg',
      ),
      8 => 
      array (
        'id' => 642,
        'tmplvarid' => 20,
        'contentid' => 602,
        'value' => 'ustanovka/renault_scenic_conquest/19.jpg',
      ),
      9 => 
      array (
        'id' => 643,
        'tmplvarid' => 21,
        'contentid' => 602,
        'value' => 'ustanovka/renault_scenic_conquest/20.jpg',
      ),
    ),
  ),
  37 => 
  array (
    'id' => 597,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Audi A3 8V',
    'longtitle' => '',
    'description' => '',
    'alias' => 'audi-a3-8v',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => 'Вот мой вариант установки Axiom Car Vision 1100 в Audi A3 8V:<br>
<br>
Так как в этой Ауди нет отсека для очков в центральном плафоне, блок будет установлен в перчаточном ящике.<br>
Разбираем салон для прокладки проводов: рис.1-5<br>
<br>
Собрано все очень крепко. Для отсоединения деталей требуются значительные усилия. "Поддеть отверткой" без повреждений не получится.<br>
<br>
Снимаем плафон: рис.6-8<br>
<br>
Снимаем бардачок, вынув перед этим с помощью подходящих съемников блок MMI: рис.9-10<br>
<br>
Крепежных винтов очень много. Один как раз за блоком MMI: рис.11<br>
<br>
Что бы основной блок с проводами не болтался по бардачку, я приклеил эпоксидкой вырезанный из старой автомобильной аптечки отдельный отсек: рис.12-14<br>
<br>
Протягиваем провода, используя кронштейны из комплекта регистратора: рис.15-18<br>
<br>
При снятом бардачке прокладка проводов не представляет трудностей. Блок питания приклеивается на корпус воздуховода: рис.19-20<br>
<br>
Никаких проводов резать не стал. Штатный разъем регистратора подключил к выносному разъему прикуривателя. Что бы все это не разваливалось и не грохотало, обмотал детали и провода текстильной лентой.<br>
В блоке предохранителей нашелся подходящий плюс - напряжение подается при включенном зажигании: рис.21-22<br>
<br>
Есть идея установить камеру в основание зеркала заднего вида. Реализовать пока не удалось, т.к. потребуется переделка корпуса камеры и ее разъема, наподобие того, что описывалось на одной из начальных страниц этой темы: рис.23-24<br>
<br>
Пока камера просто висит рядом с зеркалом. Угол обзора действительно велик, и ближе к краю стекла камеру не повесить:рис.25',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 31,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1459459914,
    'editedby' => 1,
    'editedon' => 1459460050,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 0,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/audi-a3-8v/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
      0 => 
      array (
        'id' => 615,
        'tmplvarid' => 21,
        'contentid' => 597,
        'value' => 'ustanovka/audi a3/10.jpg',
      ),
      1 => 
      array (
        'id' => 607,
        'tmplvarid' => 13,
        'contentid' => 597,
        'value' => 'ustanovka/audi a3/02.jpg',
      ),
      2 => 
      array (
        'id' => 606,
        'tmplvarid' => 12,
        'contentid' => 597,
        'value' => 'ustanovka/audi a3/01.jpg',
      ),
      3 => 
      array (
        'id' => 608,
        'tmplvarid' => 14,
        'contentid' => 597,
        'value' => 'ustanovka/audi a3/03.jpg',
      ),
      4 => 
      array (
        'id' => 609,
        'tmplvarid' => 15,
        'contentid' => 597,
        'value' => 'ustanovka/audi a3/04.jpg',
      ),
      5 => 
      array (
        'id' => 610,
        'tmplvarid' => 16,
        'contentid' => 597,
        'value' => 'ustanovka/audi a3/05.jpg',
      ),
      6 => 
      array (
        'id' => 611,
        'tmplvarid' => 17,
        'contentid' => 597,
        'value' => 'ustanovka/audi a3/06.jpg',
      ),
      7 => 
      array (
        'id' => 612,
        'tmplvarid' => 18,
        'contentid' => 597,
        'value' => 'ustanovka/audi a3/07.jpg',
      ),
      8 => 
      array (
        'id' => 613,
        'tmplvarid' => 19,
        'contentid' => 597,
        'value' => 'ustanovka/audi a3/08.jpg',
      ),
      9 => 
      array (
        'id' => 614,
        'tmplvarid' => 20,
        'contentid' => 597,
        'value' => 'ustanovka/audi a3/09.jpg',
      ),
    ),
  ),
  38 => 
  array (
    'id' => 603,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Renault Megane 2',
    'longtitle' => '',
    'description' => '',
    'alias' => 'renault-megane-2',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => 'Мне повезло! Приветствую всех форумчан! Не сочтите за рекламу. Пользую 1100-ю модель уже 14 месяцев, ни одна из проблем, с которыми сталкивались форумчане, меня не коснулась (тьфу-тьфу-тьфу). После стоянки в жару на солнце, проверял запись на предмет расфокусировки - ничего не заметил. Зимой после морозных ночевок ( хотя зима и не очень холодная была) аппарат запускался нормально, Акб живая. В общем я доволен до соплей, получил, что хотел, "поставил и забил". Заметил только один недостаток, у меня на ВР отстают часы, приходится периодически корректировать время. Своим выбором доволен - это мой аппарат. А теперь вопрос ( нижайшая просьба) к службе поддержки: ребенок потерял штатный кронштейн крепления камеры (у меня сейчас она на самодельном на зеркале), можно ли у вас отдельно приобрести другой кронштейн, хочется попробовать приблизить камеру к стеклу, а то сильно бликует. Спасибо хороший девайс. С нетерпением жду новостей о новой версии ВР, хотелось бы знать, что нового будет в новой камере, можно ли программно (или аппаратно) сделать разворот изображения на 90 градусов, предполагается ли сделать камеру более миниатюрной (избавившись от громоздского разъема).',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 37,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1459460987,
    'editedby' => 0,
    'editedon' => 0,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1459460987,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/renault-megane-2/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
      0 => 
      array (
        'id' => 644,
        'tmplvarid' => 12,
        'contentid' => 603,
        'value' => 'ustanovka/renault_megane_2/1.jpg',
      ),
      1 => 
      array (
        'id' => 645,
        'tmplvarid' => 13,
        'contentid' => 603,
        'value' => 'ustanovka/renault_megane_2/1_variant.jpg',
      ),
      2 => 
      array (
        'id' => 646,
        'tmplvarid' => 14,
        'contentid' => 603,
        'value' => 'ustanovka/renault_megane_2/2_variant.jpg',
      ),
      3 => 
      array (
        'id' => 647,
        'tmplvarid' => 15,
        'contentid' => 603,
        'value' => 'ustanovka/renault_megane_2/2_variant_1.jpg',
      ),
      4 => 
      array (
        'id' => 648,
        'tmplvarid' => 16,
        'contentid' => 603,
        'value' => 'ustanovka/renault_megane_2/3_start.jpg',
      ),
      5 => 
      array (
        'id' => 649,
        'tmplvarid' => 17,
        'contentid' => 603,
        'value' => 'ustanovka/renault_megane_2/3_continued.jpg',
      ),
      6 => 
      array (
        'id' => 650,
        'tmplvarid' => 18,
        'contentid' => 603,
        'value' => 'ustanovka/renault_megane_2/3_install_end.jpg',
      ),
      7 => 
      array (
        'id' => 651,
        'tmplvarid' => 19,
        'contentid' => 603,
        'value' => 'ustanovka/renault_megane_2/inside_view.jpg',
      ),
      8 => 
      array (
        'id' => 652,
        'tmplvarid' => 20,
        'contentid' => 603,
        'value' => 'ustanovka/renault_megane_2/stocking_the_inside.jpg',
      ),
      9 => 
      array (
        'id' => 653,
        'tmplvarid' => 21,
        'contentid' => 603,
        'value' => 'ustanovka/renault_megane_2/view_outside.jpg',
      ),
    ),
  ),
  39 => 
  array (
    'id' => 604,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Porsche Cayenne',
    'longtitle' => '',
    'description' => '',
    'alias' => 'porsche-cayenne',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => '',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 38,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1459461035,
    'editedby' => 1,
    'editedon' => 1459461056,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1459460700,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/porsche-cayenne/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
      0 => 
      array (
        'id' => 654,
        'tmplvarid' => 22,
        'contentid' => 604,
        'value' => 'http://www.youtube.com/embed/ZikhJvx4GHs',
      ),
    ),
    'images' => 
    array (
    ),
  ),
  40 => 
  array (
    'id' => 605,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Mitsubishi LX',
    'longtitle' => '',
    'description' => '',
    'alias' => 'mitsubishi-lx',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => '<p>Кнопку на первое время ставил в верхний ящик консоли. Можно заодно использовать в качестве подсветки(у меня там как раз отверстие для этого, резать пластик не пришлось), но имхо она недостаточно яркая для этого.</p><br>

<p><img src="ustanovka/mitsubishi_lx/1.jpg" alt="" width="50%" height="50%" /></p><br>

<p>Благодаря Максиму(еще раз огромное спасибо!) заполучил кнопку на разбор, разбираем битой Т-7:</p><br>

<p><img src="ustanovka/mitsubishi_lx/2.jpg" alt="" width="50%" height="50%" /></p><br>

<p>Три вывода: общий, на кнопку и на светодиод(ы).</p><br>

<p><img src="ustanovka/mitsubishi_lx/3.jpg" alt="" width="50%" height="50%" /></p><br>

<p>Почти никаких отличий от старой версии отсюда:
<a href="http://videoregforum.ru/threads/axiom-car-vision-1100.936/page-13#post-35286">http://videoregforum.ru/...</a>
Нефиксируемая кнопка и два спаренных диода.
Цвета проводов отличаются:</p><br>

<p><img src="ustanovka/mitsubishi_lx/4.jpg" alt="" width="50%" height="50%" /></p><br>

<p>Хотел подписать по цветам, но решил подглядеть на своей(такой же), цветность опять другая:</p><br>

<p><img src="ustanovka/mitsubishi_lx/5.jpg" alt="" width="50%" height="50%" /></p><br>

<p>Видимо в зависимости от партий цветность разная, поэтому лучше кнопку разобрать, а не отрезать. Дорожки на плате видно(на просвет), для наглядности распиновка такая(у всех кнопок одинаково):</p><br>

<p><img src="ustanovka/mitsubishi_lx/6.jpg" alt="" width="50%" height="50%" /></p><br>

<p>Интересно, для чего пустое место на плате?<br><br>

Делал на кнопке парктроника, такая подходит на модели Mitsubishi - АSX, Outlander XL, Lancer Х, Pajerо и пр.</p><br>

<p><img src="ustanovka/mitsubishi_lx/7.jpg" alt="" width="50%" height="50%" /></p><br>

<p>Шесть выводов: Два на кнопку. Два подсветка(лампа с красным колпачком). Два индикатор(лампа с зеленым колпачком).<br><br>

Лампу поменял на красный диод:</p><br>

<p><img src="ustanovka/mitsubishi_lx/8.jpg" alt="" width="50%" height="50%" /></p><br>

<p>Из-за большого диода(другого не было под рукой) пришлось немного попыхтеть чтоб он вошел в паз и ничего не задевал при нажатии на кнопку .</p><br>

<p><img src="ustanovka/mitsubishi_lx/9.jpg" alt="" width="50%" height="50%" /></p><br>

<p>Разьем для этой кнопки синего цвета, не обязательно искать именно такой, можно брать любой на шесть пинов, надо будет лишь отломить направляющую.<br>
Вообще у Mitsubishi много незадействованных разьемов...</p><br>

<p><img src="ustanovka/mitsubishi_lx/10.jpg" alt="" width="50%" height="50%" /></p><br>

<p>1 и 6 соединены вместе --> общий на ВР<br>
2 -------------------------------> кнопка, на ВР<br>
5 -------------------------------> светодиод(индикатор) записи, на ВР<br>
3 и 4 --------------------------> Подветка от габаритов, переткнул из незадействованного разьема.<br><br>

Кнопка стоит слева от руля и перекрывается им, поэтому индикатор глаз "не режет" ))</p><br>',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 39,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1459461093,
    'editedby' => 1,
    'editedon' => 1459461178,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1459460700,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/mitsubishi-lx/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
      0 => 
      array (
        'id' => 656,
        'tmplvarid' => 12,
        'contentid' => 605,
        'value' => 'ustanovka/mitsubishi_lx/1.jpg',
      ),
      1 => 
      array (
        'id' => 657,
        'tmplvarid' => 13,
        'contentid' => 605,
        'value' => 'ustanovka/mitsubishi_lx/2.jpg',
      ),
      2 => 
      array (
        'id' => 658,
        'tmplvarid' => 14,
        'contentid' => 605,
        'value' => 'ustanovka/mitsubishi_lx/3.jpg',
      ),
      3 => 
      array (
        'id' => 659,
        'tmplvarid' => 15,
        'contentid' => 605,
        'value' => 'ustanovka/mitsubishi_lx/4.jpg',
      ),
      4 => 
      array (
        'id' => 660,
        'tmplvarid' => 16,
        'contentid' => 605,
        'value' => 'ustanovka/mitsubishi_lx/5.jpg',
      ),
      5 => 
      array (
        'id' => 661,
        'tmplvarid' => 17,
        'contentid' => 605,
        'value' => 'ustanovka/mitsubishi_lx/6.jpg',
      ),
      6 => 
      array (
        'id' => 662,
        'tmplvarid' => 18,
        'contentid' => 605,
        'value' => 'ustanovka/mitsubishi_lx/7.jpg',
      ),
      7 => 
      array (
        'id' => 663,
        'tmplvarid' => 19,
        'contentid' => 605,
        'value' => 'ustanovka/mitsubishi_lx/8.jpg',
      ),
      8 => 
      array (
        'id' => 664,
        'tmplvarid' => 20,
        'contentid' => 605,
        'value' => 'ustanovka/mitsubishi_lx/9.jpg',
      ),
      9 => 
      array (
        'id' => 665,
        'tmplvarid' => 21,
        'contentid' => 605,
        'value' => 'ustanovka/mitsubishi_lx/10.jpg',
      ),
    ),
  ),
  41 => 
  array (
    'id' => 606,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Hyundai Elantra MD',
    'longtitle' => '',
    'description' => '',
    'alias' => 'hyundai-elantra-md',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => '<p>Переделан кронштейн от камеры. Полностью перепаян разъем на HDMI кабеле, потом залит эпоксидкой и обтянут термоусадкой. Выпилен дополнительный удлинитель кронштейна камеры, чтоб снаружи выглядело как положено от потолка. Использован переходник для штатной магнитолы. Оригинальная проводка была практически полностью заменена на более качественные провода, в том числе экранированные, и максимально укорочена. Блок питания собран совершенно другой, в итоге в сети стоит полная тишина без помех, нет свиста в штатной магнитоле. Подключение блока питания к штатному smart боксу. При установке между датчиками на лобовом стекле, еле хватило места, пришлось спиливать крепежный болт камеры. Сам регистратор установлен в подлокотник. В итоге, камера намного ближе к лобовому стеклу и выше - меньше ее заметно. Блок питания с проводкой не имеют помех от бортовой сети, и не наводят, и не шумят в обратку. В штатной магнитоле стоит полная тишина.</p><br><br>

<p><img src="ustanovka/hyundai_elantra_md/1.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/hyundai_elantra_md/2.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/hyundai_elantra_md/3.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/hyundai_elantra_md/4.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/hyundai_elantra_md/5.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/hyundai_elantra_md/6.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/hyundai_elantra_md/7.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/hyundai_elantra_md/8.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/hyundai_elantra_md/9.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/hyundai_elantra_md/10.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/hyundai_elantra_md/11.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/hyundai_elantra_md/12.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/hyundai_elantra_md/13.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/hyundai_elantra_md/14.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/hyundai_elantra_md/15.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/hyundai_elantra_md/16.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/hyundai_elantra_md/17.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/hyundai_elantra_md/18.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/hyundai_elantra_md/19.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/hyundai_elantra_md/20.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/hyundai_elantra_md/21.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/hyundai_elantra_md/22.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/hyundai_elantra_md/23.jpg" alt="" width="50%" height="50%" /></p><br>
',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 40,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1459461209,
    'editedby' => 1,
    'editedon' => 1459461294,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1459460700,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/hyundai-elantra-md/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
      0 => 
      array (
        'id' => 667,
        'tmplvarid' => 12,
        'contentid' => 606,
        'value' => 'ustanovka/hyundai_elantra_md/1.jpg',
      ),
      1 => 
      array (
        'id' => 668,
        'tmplvarid' => 13,
        'contentid' => 606,
        'value' => 'ustanovka/hyundai_elantra_md/3.jpg',
      ),
      2 => 
      array (
        'id' => 669,
        'tmplvarid' => 14,
        'contentid' => 606,
        'value' => 'ustanovka/hyundai_elantra_md/6.jpg',
      ),
      3 => 
      array (
        'id' => 670,
        'tmplvarid' => 15,
        'contentid' => 606,
        'value' => 'ustanovka/hyundai_elantra_md/9.jpg',
      ),
      4 => 
      array (
        'id' => 671,
        'tmplvarid' => 16,
        'contentid' => 606,
        'value' => 'ustanovka/hyundai_elantra_md/11.jpg',
      ),
      5 => 
      array (
        'id' => 672,
        'tmplvarid' => 17,
        'contentid' => 606,
        'value' => 'ustanovka/hyundai_elantra_md/14.jpg',
      ),
      6 => 
      array (
        'id' => 673,
        'tmplvarid' => 18,
        'contentid' => 606,
        'value' => 'ustanovka/hyundai_elantra_md/17.jpg',
      ),
      7 => 
      array (
        'id' => 674,
        'tmplvarid' => 19,
        'contentid' => 606,
        'value' => 'ustanovka/hyundai_elantra_md/19.jpg',
      ),
      8 => 
      array (
        'id' => 675,
        'tmplvarid' => 20,
        'contentid' => 606,
        'value' => 'ustanovka/hyundai_elantra_md/21.jpg',
      ),
      9 => 
      array (
        'id' => 676,
        'tmplvarid' => 21,
        'contentid' => 606,
        'value' => 'ustanovka/hyundai_elantra_md/23.jpg',
      ),
    ),
  ),
  42 => 
  array (
    'id' => 607,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Подключение без батареи',
    'longtitle' => '',
    'description' => '',
    'alias' => 'podklyuchenie-bez-batarei',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => '<p><h2>Про видео регистратор без внутренней Li Ion батареи! Совсем без батареи!</h2></p>

<p><strong>Здравствуйте! Принимайте на рассмотрение мою инсталляцию.</strong></p>

<p><strong>Вступление и теория:</strong></p>

<p><strong>Для чего видео регистратору AxiomCarVision (далее – ВР или регистратор) нужна собственная батарея? Второе независимое и не снимаемое питание ВР</strong>, в виде внутренней LiIon батареи, необходимо в нескольких режимах работы ВР, а именно:</p>
<p>· в основном режиме, при действии на отключение ВР, работающего в этот момент на видео регистрацию. Такое отключение стандартно происходит по факту снятия с входа miniUSB регистратора управляющего напряжения 5В от контроллера, которое является одновременно и основным напряжением для питания ВР в режиме авто видео регистратора (далее - управляющее напряжение). Снятие управляющего напряжения переводит ВР в дежурное состояние, при котором ВР отключен (далее <strong>– ВРоткл). Не снимаемое батарейное питание</strong> будет использовано регистратором в момент отключения, для формирования видео файла (его правильного завершения и сохранения), для завершения запущенных программных процессов ВР и на его выключение, с переходом в «ВРоткл»;</p>
<p>· для поддержания хода часов регистратора и листание внутреннего календаря, в режиме ВРоткл;</p>
<p>· для обеспечения работы регистратора, в случае его принудительного включения от кнопки включения ВР, когда управляющего напряжение 5В отсутствует.</p>
<p><strong>Тут надо пояснить,</strong> что при наличии поданного от контроллера на ВР, через вход miniUSB, управляющего напряжения «5В» (в стандарте USB, нормально 5+ 0,25В, максимально не более 6,0В), от него будут обеспечиваться <ins>все потребности</ins> питания ВР, включая буферный режим батареи ВР, с подзарядом батареи слабым током. Т.е. управляющее напряжение 5В является тем <strong>основным напряжением («первым»</strong> напряжением), которое обеспечивает все потребности ВР в энергии. При наличии поданного на ВР основного напряжения, заряд внутренней батареи не расходуется.</p>
<p>О подзаряде батареи именно слабым током, можно судить по имеющимся наблюдениям, когда разрядившаяся батарея не успевает восстанавить свой заряд в процессе работы ВР на видео регистрацию, а требует отдельной зарядки, от сетевого зарядника, в режиме ВРоткл. Отсутствие разряда батареи в циклах работы ВР говорит, что подзаряд все-таки происходит (специально не проверял).</p>

<p><strong>Зачем все это надо?:</strong></p>
<p>Хотя считается, что использование Li Ion аккумуляторов (на разряд!) может производиться до минус 20°С, ее рабочие характеристики ухудшаются уже при температуре близкой к 0°С, а <strong>заряжать</strong> Li-Ion аккумуляторные батареи допустимо только в интервале температур от +5° С до +45° С.</p>
<p><strong>Так же была поставлена цель,</strong> разместить ПРН в батарейном отсеке ВР, ведь место-то есть, и оно не должно пустовать! Кроме того, все пониженные рабочие напряжения следует локализовать внутри приборов и не допускать их необоснованного ветвления по жгутам автомобильной сети. Сборка DC\\DC преобразователя с некоторыми внешними (об этом ниже) навесными элементами, вставляемая в штатный батарейный отсек, <strong>названа картриджем ПРН.</strong></p>

<p><strong>Что использовано в качестве ПРН:</strong></p>
<p><strong>Практически «готовый» Mini 3A DC-DC Converter Adjustable Step down Power Supply,</strong> собранныйна базе MP1584. Куплен на еВау (<a href="http://www.ebay.com/itm/Mini-DC-DC-Converter-Step-Down-Module-Adjustable-Power-Supply-4-5-28V-TO-1-3-17V-/360741046604?pt=AU_B_I_Electrical_Test_Equipment&hash=item53fdd7894c">http://www.ebay.com/itm/Mini-DC-DC-...Electrical_Test_Equipment&hash=item53fdd7894c</a>), стоимостью около 55руб/шт, при бесплатной пересылке. Схемка изготовлена на микросхеме MP1584 от известного производителя MonolithicPowerSystems(MPS).</p>
<p>Габариты изделия от еВау внушают уверенность, что в штатный батарейный отсек Axiom CarVision все должно поместиться. Вот как выглядит готовое изделие от еВау, пока без перепайки делителей и с переменным резистором для регулировки выходного напряжения:</p>

<p><img src="ustanovka/podkl_bez_bat/1.jpg" alt="" width="70%" height="70%" /></p><br>
<p><img src="ustanovka/podkl_bez_bat/2.jpg" alt="" width="70%" height="70%" /></p><br>
<p><img src="ustanovka/podkl_bez_bat/3.jpg" alt="" width="70%" height="70%" /></p><br>

<p>Купленный на еВау Mini 3A DC-DC Converter Adjustable Step down Power Supply, принципиально соответствует схеме типового применения MP1584, <strong>но есть исключения, и требуется обязательная адаптация</strong> по ABSOLUTEMAXIMUMRATINGS <strong>из даташитаMP1584</strong> заменой установленного R6 со 100К на любое сопротивление в диапазоне <strong>от 24К до 66К.</strong> Ну и очень желательно убрать переменное сопротивление R1, заменив его на постоянное.</p>

<p><strong>Что было переделано в купленной схеме:</strong></p>
<p>С4 установил номиналом 47nF</p>
<p>R6 100К заменил на <strong>24К</strong>, как по типовому применению, хотя при R5 100К, R6 можно и до 66К, исходя из требования ABSOLUTEMAXIMUMRATINGS: AllOtherPins .................... –0.3Vto +6V. Для питания Axiom Car Vision 1100, возможно, лучше ставить R6 что-то около 50К, исходя из его большей склонности к потере часов, календаря при пропадании батарейного питания (об этой склонности см. ниже).</p>
<p><strong>R2</strong> заменил на <strong>39К</strong>. Использование далее постоянного <strong>R1=160К</strong> дает выходное напряжение <strong>Uвых=4,12В.</strong></p>

<p><strong>С учетом диапазона работы: 3,7В – номинал Li Ion батареи, 4,2В – прекращение заряда Li Ion батареи, напряжение Uвых=4,12В было оставлено для работы ПРН с видео регистратором Axiom Car Vision 1000.</strong></p>

<p><img src="ustanovka/podkl_bez_bat/4.jpg" alt="" width="70%" height="70%" /></p><br>

<p><strong>Дополнительно: </strong></p>
<p>на входе установлена электролитическая емкость 22мкФ 50В, на выходе 220мкФ 16В. Для спасения схемы от возможных выбросов напряжения обратной полярности, на входе ПРН установлен в прямом направлении диод Шоттки ES3B 3A 100В. Установка диода так же уменьшит влияние ПРН на внешние цепи, диод защищает от подачи на схему входного напряжения неверной полярности, ну и в случае беды, сыграет роль предохранителя ;).</p>
<p>Падение напряжения на нем в данном применении не критично.</p>
<p><strong>Были сделаны многие замеры по потреблению</strong> всей сборки ПРН в разных режимах. <strong>Во всех режимах сохраняется U вых=4,12В. Самое основное из замеров в рабочих режимах:</strong> когда управляющее напряжение 5В от контроллера снято (оно же основное питание ВР), потребление по цепи ПРН, со стороны «12В», при напряжения автомобильной сети от 15В до 10В, не превышает 0,5А в любом режиме работы ВР. В случае дальнейшего снижения напряжения автомобильной сети, на «12В» входе ПРН, при Uвх=8В, Iпотр.макс=0,6А; при Uвх=7В, Iпотр.макс=0,7А. Замер при +23грС. Напряжение отключения ПРН (когда начинаем терять U вых=4,12В) получилось 6,5В на «12В» входе ПРН, а обратное его включение при повышении напряжения (когда снова начинаем получать Uвых=4,12В), происходит при 7,7В на «12В» входе ПРН. Это справедливо для R6=24К. ЕслиR6 по величине брать ближе к 50К, пороговое напряжение отключения ПРН будет ниже.</p>
<p>А самое главное то, что на ВР, в режиме ВРоткл, <strong>регистратор по цепи ПРН потребляет менее 1 миллиампера при поданных 14В!</strong> При таком мизерном потреблении данную цепь не требуется прерывать по режиму сохранности автомобильной аккумуляторной батареи от глубокого падения на ней напряжения, поскольку потребление по данной цепи значительно меньше естественного саморазряда нашей автомобильной аккумуляторной батареи. И это питание ВР по цепи ПРН как раз очень важно сохранить неснимаемым, поскольку в режиме ВРоткл оно поддерживает часовой ход и листание внутреннего календаря ВР. Поддержание функции часов и календаря ВР необходима, поскольку все зафиксированные на ВР события теряют половину своего прикладного смысла при не настроенных часах и тем более, при сброшенном календаре! Ну, и вот он, <strong>важный вывод</strong> – режим «ВРоткл» можно поддерживать «бесконечно» долго, без прерывания питания ПРН, не ухудшая при этом свойства и сохранность аккумуляторной батареи автомобиля.</p>

<p>Да, неприятно удивил результат отсутствия сохранности часов и календаря при пропадании «батарейного питания», даже на незначительное время, на ВР AxiomCarVision 1100, на котором и проводились все эти замеры. На ВР AxiomCarVision 1000, при пропадании «батарейного питания», ход часов и календарь сохраняется несколько часов. Следует при тяжелых холодных пусках первоначально проверять сохранность часов и календаря ВР на Axiom CarVision 1100, но не думаю, что напряжение в автомобильной сети просядет ниже 6В. Если сбросы часов – календаря будут, можно увеличить R6 в сторону 50К, тогда влияния тяжелых пусков быть не должно.</p>
<p><strong>В дополнение, по замерам:</strong> при любом режиме работы ВР, его потребление по цепи основного питания, <ins>на входе контроллера управления</ins>, не более 0,5А при напряжения автомобильной сети от 15В до 10В. В случае дальнейшего снижения напряжения автомобильной сети, при Uвх=8В, Iпотр.макс=0,6А; при Uвх=7В, Iпотр.макс=0,7А. <strong>Данный замер важен для известной дискуссии по нагрузке на LUKAS,</strong> используемый мною для защиты от глубокого разряда аккумуляторной батареи автомобиля при работе ВР на стоянке, по датчику движения. LUKAS имеет паспортное ограничение не более 1А по питаемой нагрузке, и сделанные замеры подтверждают, что работа видеорегистраторов линейки Axiom CarVision (1000, 1100) через LUKAS будет долгой и плодотворной.(кому интересно, тема тут http://videoregforum.ru/threads/luk...igatelja-otkljuchaet-rozetki.1339/#post-48094)</p>

<p><strong>Проверка ПНР на источник помех:</strong> сделана обычным радиоприемником на АМ и FM диапазонах – <ins>помех <strong>не выявлено</strong></ins>.</p>
<p><strong>Испытания температурой: холодильником и феном. Вывод – влияние температуры практически отсутствует.</strong> Единственно, желательно помазать все на картридже лаком, т.к. при отпотевании элементов возможно искажение работы ПРН. Правда, если картридж будет помещен внутрь ВР, в том пространстве, скорее всего, отпотевание не проявится. Покажут дальнейшие проверки в реальных условиях.</p>

<p><strong>О картридже:</strong></p>
<p><strong>Картридж</strong> – сборка, выполненная на плате по размерам батарейного отсека ВР, где установлены все элементы, вместе используемые взамен штатной батареи. На сборке установлены: диод Шоттки ES3B 3A 100В, электролит 22мкФ 50В на «12В» входе, сам ПРН, электролит 220мкФ 16В на его 4,12В выходе. Перечисленные элементы закреплены на общей плате из двухстороннего фольгированного стеклотекстолита односторонним навесным монтажом, т.е. без сверления. Электролиты положены боком, утоплены и вклеены в распилы платы картриджа. «12В» на вход картриджа подается через штекер по двухжильному проводу, который засажен в термоусадку и закреплен на плате двумя пластиковыми хомутами. Выходные ламели картриджа ПРН контактируют с + и – выводов ВР в батарейном отсеке, в соответствии с маркировкой на батарее. Контактные ламели выхода картриджа ПРН дополнительно укреплены резьбовым крепежом. На минусовой ламели крепежный винт служит одновременно контактом для подачи минуса на полигон задней сторону платы картриджа. Платка ПРН наклеена на картридж двухсторонним скотчем. Ну, собственно и все. <strong>Картридж</strong>, как и задумано, установлен <strong>в батарейном отсеке</strong> ВР. Для этого спилена тонкая перемычка на боковине корпуса ВР и сделан выпил :) в задней крышке ВР.</p>

<p><img src="ustanovka/podkl_bez_bat/5.jpg" alt="" width="70%" height="70%" /></p><br>

<p>Плата картриджа пока не пролачена… осталось ха-ха в планах… или забить?:))</p>

<p><strong>Про удобства:</strong></p>
<p>В случае необходимости использовать ВР для просмотра записанных файлов, учитывая короткие провода по его рабочему месту, вынести ВР из бардачка в салон на полметра, с отключением его от miniUSB и видеокабеля, можно с использованием специально запасенного для этих целей полуметрового удлинителя цепи входного питания «12В» картриджа. Все разъемные соединения и этот удлинитель так же получены с еВау, бесплатной пересылкой.</p>

<p><strong>Что было отмечено при наладке:</strong></p>
<p>1. Если индикаторная светодиодная полоса ВР перемигивается на «половинный красный» (видно, что одновременно с прочими цветами, светодиодная полоса ВР мигает на половину красным), <ins>это значит, что батарейное питание утеряно</ins>. Типичный случай при недееспособной внутренней батарее. Однако! Пока управляющее напряжение 5В от контроллера на ВР подано, часы-календарь идут, все функции ВР пока выполняются и пока не потеряны. Но если напряжение управления от контроллера сейчас снимется (а это нормально, ведь это управление), то часы-календарь ВР сбросятся к начальному, условно нулевому, значению. Так же нужно понимать, что при этом не будет нормального завершения рабочих процедур ВР, что приведет к сбою последующего включения ВР. А это больший геморрой чем потеря часов – календаря. Сбросу часов - календаря, увы, <strong>при любом снятии</strong> «батарейного питания» (это же и про питание от ПРН), особо подвержена модель Axiom CarVision 1100, где сброс происходит практически сразу. Более ранняя модель, Axiom CarVision 1000, при пропадании «батарейного питания», ход часов и календарь сохраняет довольно долго - несколько часов (видимо, от достаточно мощного внутреннего ионистора).</p>
<p>2. Потребление от батареи ВР (в данном случае потребление от ПРН) начинает происходить только после снятия управляющего напряжения и требуется для штатного завершения всех функций регистратора, с переходом его в режим «ВРоткл». Пока управляющее напряжение подано, потребление от батареи не происходит. При наличии установленной батареи, в этом режиме идет её подзаряд слабым током (буферный режим работы). При установленном вместо батареи ПРН, влияние на него буферного режима нет, за исключением небольшого броска встречного напряжения до 4,21В при подаче управляющего напряжения.</p>

<p><strong>Натурные проверки и испытания:</strong></p>
<p>Электронная сборка, в виде картриджа, установлена на боевое дежурство в реальных автомашинных условиях Мурманской области, в Axiom CarVision 1000 с 26.10.14 и работает там по сегодняшний день. Сбоев не выявлено. Потихоньку забывается и уходит в прошлое ставшее привычным действие зимой по выемке Li Ion аккумуляторной батареи из ВР, перед холодной ночной стоянкой.</p>

<p><strong><h2>Рекомендую к повторению!</h2></strong></p>


<p><strong><h3><ins>Вопросы:</ins></h3></strong></p>

<p>Замечательная идея (и реализация) исключить батарею, которая замерзает и тянет за собой массу неприятностей!</p>
<p><strong>Но возникло несколько вопросов: </strong></p>
<p>1.касаемо "управляющего напряжения" когда оно снимается, каким образом?</p>
<p>2.ВРоткл как производится - нажатием на кнопку отключения видеорегистратора, и таким же образом-нажатием кнопки на ВР он включается, так? И смысл в постоянном подключении - только часы с календарем в ВР запитывать, чтобы не сбивались? Т.е. после запуска двигателя нужно будет ВР вручную включать?</p>
<p>3.При чем тогда вообще режим "Парковка"</p>

<p><ins>Если что-то недопонял, прошу пояснить</ins></p>

<p><strong><h3><ins>Ответы:</ins></h3></strong></p>

<p><strong>Спасибо за хорошие вопросы! А вот и ответы</strong></p>
<p><strong>- На 1.:</strong></p>
<p>Если кратко, то так:</p>

<p>Источником напряжения для ВР является контроллер. Штатным источником напряжения для контроллера является автомобильная розетка. ВР включается в работу, когда на нем появляется напряжение от контроллера. Как правило, напряжение на розетках появляется при включении зажигания и снимается при отключении зажигания. Таким образом, при запущенном двигателе, ВР работает, а при остановке двигателя ВР отключается и делается это автоматически, управляющим напряжением питания от розеток, преобразованным контроллером.</p>

<p><strong>Далее, в подробностях:</strong></p>
<p>«Контроллер» для Axiom CarVision – это отдельное устройство с белой или коричневой кнопкой (белая для 1000 модели и коришшневая для 1100 модели), преобразующее «12В» бортовой сети в «5В» стандарта USB. Это напряжение «5В» подается на ВР через его вход типоразмера miniUSB (формата 10pin). Если на ВР напряжение от контроллера подано, регистратор находится в работе. Термин «находится в работе», означает, что ВР находится в режиме работы, т.е. выполняет свои полезные функции согласно настроенному внутреннему алгоритму, например, пишет или стоит на стопе по датчику движения. Если напряжение с контроллера снять, ВР завершит режим работы, и перейдет в отключенное состояние (в своем изложении я его назвал состоянием ВРоткл, и это дежурное состояние). Таким образом, напряжение, подаваемое на регистратор со стороны контроллера, является управляющим напряжением для изменения состояний ВР, автоматически переключая его из режима ВРоткл в режим работы, и обратно. Теперь о том, каким образом снимается или подается управляющее напряжение. По типовой схеме подключения ВР, напряжение «12В» бортовой сети подается на контроллер из автомобильной розетки. Как правило, напряжение на авторозетках появляется при включении зажигания и снимается при отключении зажигания. Следовательно, при запущенном двигателе, ВР находится в режиме работы, а при останове двигателя ВР переходит в режим ВРоткл, и делается это автоматически, управляющим напряжением питания розеток, преобразованным контроллером.</p>

<p><strong>- На 2.:</strong> легкий способ переключиться регистратору в ВРоткл, это через снятие напряжения с контроллера, которое будет происходить автоматически, при отключении зажигания. ВР отключается и делается это через снятие напряжения питания розеток. ВР завершает режим работы и переходит в отключенное состояние, названное для краткости ВРоткл. Сделается такое переключение при останове двигателя автоматически, т.е. без нашего участия. Обратное включение ВР произойдет так же автоматически, при включении зажигания, через появление напряжения на розетках. Так же, при отдельном желании, можно и кнопкой на корпусе ВР, и кнопкой контроллера, но, как показала моя практика, автоматического переключения для поддержания автомобильной видео регистрации, будет более чем достаточно.А смыслов в постоянном подключении батарейного питания на ВР два или даже мб три, я не считал. Правильно завершить процедуры ВР при переходе его в ВРоткл, например, многие посчитают главной из решаемых при этом задач. Особенно это будут те, кто терял в эксплуатации ВР батарейное питание, они то это знают, и бегут заряжать батарею ВР к далекому сетевому заряднику. Тот еще, доложу, геморрой. Подробнее можно покурить форум тут или есть ветка по Axiom CarVision 1000. Не буду убеждать, но и поддержание на ВР функции часов с календарем архи необходима, поскольку все зафиксированные на ВР события потеряют половину своего прикладного смысла при ненастроенных часах и тем более, при сброшенном календаре! Что будем демонстрировать ППСнику-то (избави нас от беса к ночи!)? Кино про событие, автор неизвестен..?</p>
<p>И как было упомянуто, режим «ВРоткл» описанным способом можно поддерживать «бесконечно» долго, не ухудшая при этом свойства и сохранность автомобильной аккумуляторной батареи, сохраняя полноценно абсолютно все функции ВР. Еще раз подчеркиваю, фсе-фсе!</p>

<p><strong>- На 3.:</strong> а про режим Парковка я не понял вопроса :(, прошу уточнить</p>

<p><strong>Вот и настала она, эта пора схем и чертежей. Похоже, без этого в нашем деле, как у других без пол-литры, – не разберешься!
Выкладываю мое черчение для поддержки разговора. Теперь дело быстрее пойдет</strong></p>
<p><img src="ustanovka/podkl_bez_bat/shema.png" alt="" width="70%" height="70%" /></p><br>',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 41,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1459461400,
    'editedby' => 0,
    'editedon' => 0,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1459461400,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/podklyuchenie-bez-batarei/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
      0 => 
      array (
        'id' => 677,
        'tmplvarid' => 12,
        'contentid' => 607,
        'value' => 'ustanovka/podkl_bez_bat/1.jpg',
      ),
      1 => 
      array (
        'id' => 678,
        'tmplvarid' => 13,
        'contentid' => 607,
        'value' => 'ustanovka/podkl_bez_bat/2.jpg',
      ),
      2 => 
      array (
        'id' => 679,
        'tmplvarid' => 14,
        'contentid' => 607,
        'value' => 'ustanovka/podkl_bez_bat/3.jpg',
      ),
      3 => 
      array (
        'id' => 680,
        'tmplvarid' => 15,
        'contentid' => 607,
        'value' => 'ustanovka/podkl_bez_bat/4.jpg',
      ),
      4 => 
      array (
        'id' => 681,
        'tmplvarid' => 16,
        'contentid' => 607,
        'value' => 'ustanovka/podkl_bez_bat/5.jpg',
      ),
      5 => 
      array (
        'id' => 682,
        'tmplvarid' => 17,
        'contentid' => 607,
        'value' => 'ustanovka/podkl_bez_bat/shema.png',
      ),
      6 => 
      array (
        'id' => 683,
        'tmplvarid' => 18,
        'contentid' => 607,
        'value' => 'ustanovka/podkl_bez_bat/shema_axiom1100.jpg',
      ),
    ),
  ),
  43 => 
  array (
    'id' => 608,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Mitsubishi LANCER Х',
    'longtitle' => '',
    'description' => '',
    'alias' => 'mitsubishi-lancer-x',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => '<p>Проводка по левой стойке(там идёт штатный жгут), камера слева. С глаз водителя:</p><br>
<p><img src="ustanovka/mitsubishi_lancer_x/1.jpg" alt="" width="50%" height="50%" /></p><br>
<p>Блок ВР на крышку предохранителей:</p><br>
<p><img src="ustanovka/mitsubishi_lancer_x/2.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/mitsubishi_lancer_x/3.jpg" alt="" width="50%" height="50%" /></p><br>
<p>Изображение/звук на штатное ГУ ММСS R-03 через AUX.</p><br>
<p><img src="ustanovka/mitsubishi_lancer_x/4.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/mitsubishi_lancer_x/5.jpg" alt="" width="50%" height="50%" /></p><br>
<p>Дополнения.</p><br>
<p>Кнопка изначально была установлена в верхнем ящике консоли, но позже я ее встроил в штатную кнопку от парктроника, тут подробно:
<a href="http://axiom-russia.ru/kak-postavit/mitsubishi-lx/">ссылка</a> <br>
На кабеле HDMI была подрезана оплетка у разьема и укорочен кронштейн.<br>
Отрезать надо чтобы нижняя часть камеры попала в пазы бывшие верхними.<br>
Штатный болт камеры не пилил, случайно нашел среди мелочевки и подкрасил черной краской чтоб не выделялся.</p><br>
<p><img src="ustanovka/mitsubishi_lancer_x/6.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/mitsubishi_lancer_x/7.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/mitsubishi_lancer_x/8.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/mitsubishi_lancer_x/9.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/mitsubishi_lancer_x/10.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/mitsubishi_lancer_x/11.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/mitsubishi_lancer_x/12.jpg" alt="" width="50%" height="50%" /></p><br>
<p>Обновил кнопки :) кто заметит разницу?</p><br>
<p><img src="ustanovka/mitsubishi_lancer_x/button.jpg" alt="" width="50%" height="50%" /></p><br>
<p><img src="ustanovka/mitsubishi_lancer_x/button1.jpg" alt="" width="50%" height="50%" /></p><br>',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 42,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1459461576,
    'editedby' => 0,
    'editedon' => 0,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1459461576,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/mitsubishi-lancer-x/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
      0 => 
      array (
        'id' => 684,
        'tmplvarid' => 12,
        'contentid' => 608,
        'value' => 'ustanovka/mitsubishi_lancer_x/1.jpg',
      ),
      1 => 
      array (
        'id' => 685,
        'tmplvarid' => 13,
        'contentid' => 608,
        'value' => 'ustanovka/mitsubishi_lancer_x/2.jpg',
      ),
      2 => 
      array (
        'id' => 686,
        'tmplvarid' => 14,
        'contentid' => 608,
        'value' => 'ustanovka/mitsubishi_lancer_x/3.jpg',
      ),
      3 => 
      array (
        'id' => 687,
        'tmplvarid' => 15,
        'contentid' => 608,
        'value' => 'ustanovka/mitsubishi_lancer_x/4.jpg',
      ),
      4 => 
      array (
        'id' => 688,
        'tmplvarid' => 16,
        'contentid' => 608,
        'value' => 'ustanovka/mitsubishi_lancer_x/5.jpg',
      ),
      5 => 
      array (
        'id' => 689,
        'tmplvarid' => 17,
        'contentid' => 608,
        'value' => 'ustanovka/mitsubishi_lancer_x/6.jpg',
      ),
      6 => 
      array (
        'id' => 690,
        'tmplvarid' => 18,
        'contentid' => 608,
        'value' => 'ustanovka/mitsubishi_lancer_x/7.jpg',
      ),
      7 => 
      array (
        'id' => 691,
        'tmplvarid' => 19,
        'contentid' => 608,
        'value' => 'ustanovka/mitsubishi_lancer_x/8.jpg',
      ),
      8 => 
      array (
        'id' => 692,
        'tmplvarid' => 20,
        'contentid' => 608,
        'value' => 'ustanovka/mitsubishi_lancer_x/9.jpg',
      ),
      9 => 
      array (
        'id' => 693,
        'tmplvarid' => 21,
        'contentid' => 608,
        'value' => 'ustanovka/mitsubishi_lancer_x/11.jpg',
      ),
    ),
  ),
  44 => 
  array (
    'id' => 609,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Kia Sportage 3',
    'longtitle' => '',
    'description' => '',
    'alias' => 'kia-sportage-3',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => '  
<p>Так было:</p>

<p><a href="ustanovka/kia_sportage3/0.jpg" class="z-ust-pics"><img src="ustanovka/kia_sportage3/0.jpg" alt="" width="50%" height="50%" /></a></p><br>

Вот сделал не большой отчетик. Гайка не лезла, были применены жидкие гвозди. Благо места под блок ну ооочень много. Остальное я думаю все понятно. Все материалы взяты с этого форума. 
Особая благодарность LEONавто за помощь в поисках кнопки.
Обзор от sander33: батарейку делал как тут, только вместо ипаксидки были жидкие гвозди). Донор был предоставлен ребятами при покупке ВР. Про махнатую изоленту тоже где то тут вычитал. Все покупалось на Али<br><br>

<p><a href="ustanovka/kia_sportage3/1.jpg" class="z-ust-pics"><img src="ustanovka/kia_sportage3/1.jpg" alt="" width="50%" height="50%" /></a></p><br>
<p><a href="ustanovka/kia_sportage3/2.jpg" class="z-ust-pics"><img src="ustanovka/kia_sportage3/2.jpg" alt="" width="50%" height="50%" /></a></p><br>
<p><a href="ustanovka/kia_sportage3/3.jpg" class="z-ust-pics"><img src="ustanovka/kia_sportage3/3.jpg" alt="" width="50%" height="50%" /></a></p><br>
<p><a href="ustanovka/kia_sportage3/4.jpg" class="z-ust-pics"><img src="ustanovka/kia_sportage3/4.jpg" alt="" width="50%" height="50%" /></a></p><br>
<p><a href="ustanovka/kia_sportage3/5.jpg" class="z-ust-pics"><img src="ustanovka/kia_sportage3/5.jpg" alt="" width="50%" height="50%" /></a></p><br>
<p><a href="ustanovka/kia_sportage3/6.jpg" class="z-ust-pics"><img src="ustanovka/kia_sportage3/6.jpg" alt="" width="50%" height="50%" /></a></p><br>
<p><a href="ustanovka/kia_sportage3/7.jpg" class="z-ust-pics"><img src="ustanovka/kia_sportage3/7.jpg" alt="" width="50%" height="50%" /></a></p><br>
<p><a href="ustanovka/kia_sportage3/8.jpg" class="z-ust-pics"><img src="ustanovka/kia_sportage3/8.jpg" alt="" width="50%" height="50%" /></a></p><br>
<p><a href="ustanovka/kia_sportage3/9.jpg" class="z-ust-pics"><img src="ustanovka/kia_sportage3/9.jpg" alt="" width="50%" height="50%" /></a></p><br>
<p><a href="ustanovka/kia_sportage3/10.jpg" class="z-ust-pics"><img src="ustanovka/kia_sportage3/10.jpg" alt="" width="50%" height="50%" /></a></p><br>
<p><a href="ustanovka/kia_sportage3/11.jpg" class="z-ust-pics"><img src="ustanovka/kia_sportage3/11.jpg" alt="" width="50%" height="50%" /></a></p><br>
<p><a href="ustanovka/kia_sportage3/12.jpg" class="z-ust-pics"><img src="ustanovka/kia_sportage3/12.jpg" alt="" width="50%" height="50%" /></a></p><br>
<p><a href="ustanovka/kia_sportage3/13.jpg" class="z-ust-pics"><img src="ustanovka/kia_sportage3/13.jpg" alt="" width="50%" height="50%" /></a></p><br>
<p><a href="ustanovka/kia_sportage3/14.jpg" class="z-ust-pics"><img src="ustanovka/kia_sportage3/14.jpg" alt="" width="50%" height="50%" /></a></p><br>
<p><a href="ustanovka/kia_sportage3/15.jpg" class="z-ust-pics"><img src="ustanovka/kia_sportage3/15.jpg" alt="" width="50%" height="50%" /></a></p><br>',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 43,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1459461667,
    'editedby' => 1,
    'editedon' => 1463468473,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1459461660,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/kia-sportage-3/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
      0 => 
      array (
        'id' => 735,
        'tmplvarid' => 14,
        'contentid' => 609,
        'value' => 'ustanovka/kia_sportage3/3.jpg',
      ),
      1 => 
      array (
        'id' => 734,
        'tmplvarid' => 13,
        'contentid' => 609,
        'value' => 'ustanovka/kia_sportage3/2.jpg',
      ),
      2 => 
      array (
        'id' => 733,
        'tmplvarid' => 12,
        'contentid' => 609,
        'value' => 'ustanovka/kia_sportage3/1.jpg',
      ),
      3 => 
      array (
        'id' => 736,
        'tmplvarid' => 15,
        'contentid' => 609,
        'value' => 'ustanovka/kia_sportage3/4.jpg',
      ),
      4 => 
      array (
        'id' => 737,
        'tmplvarid' => 16,
        'contentid' => 609,
        'value' => 'ustanovka/kia_sportage3/5.jpg',
      ),
      5 => 
      array (
        'id' => 738,
        'tmplvarid' => 17,
        'contentid' => 609,
        'value' => 'ustanovka/kia_sportage3/6.jpg',
      ),
      6 => 
      array (
        'id' => 739,
        'tmplvarid' => 18,
        'contentid' => 609,
        'value' => 'ustanovka/kia_sportage3/7.jpg',
      ),
      7 => 
      array (
        'id' => 740,
        'tmplvarid' => 19,
        'contentid' => 609,
        'value' => 'ustanovka/kia_sportage3/8.jpg',
      ),
      8 => 
      array (
        'id' => 741,
        'tmplvarid' => 20,
        'contentid' => 609,
        'value' => 'ustanovka/kia_sportage3/9.jpg',
      ),
      9 => 
      array (
        'id' => 742,
        'tmplvarid' => 21,
        'contentid' => 609,
        'value' => 'ustanovka/kia_sportage3/10.jpg',
      ),
    ),
  ),
  45 => 
  array (
    'id' => 610,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'MERCEDES E-class W212',
    'longtitle' => '',
    'description' => '',
    'alias' => 'mercedes-e-class-w212',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => 'Установка Штатного видеорегистратора AXIOM SPETIAL Wi-Fi в мерседес E кузов W212<br><br>

Накладка с видеорегистратором стала как родная,<br>
постоянный плюс сняли с одного из толстых проводов идущих к блокам управления<br>
(под ногами переднего пассажира)<br>
плюс на включение/выключение видеорегистратора - с прикуривателя задних пассажиров.<br><br>

При первом включении регистратор издавал аварийные звуки - надо синхронизировать<br>
время на регистраторе с телефоном<br>
Скачали приложение Transcend Drive Pro<br>
подключили регистратор к телефону через Wi-Fi , синхронизировали время <br><br>

В настройках выбрали Full HD разрешение, датчик удара поставили на минимальную чувствительность <br>
(иначе по нашим дорогам карта памяти будет забиваться защищенными файлами)<br>
штатную карту на 8Gb сразу поменяли на 32Gb<br><br>

Все работает без проблем,<br>
сам включается/выключается,<br>
снаружи и не поймешь, что установлен регистратор,<br>
а не какой-нибудь штатный ассистент',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 44,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1459461867,
    'editedby' => 1,
    'editedon' => 1459462195,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1459461840,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/mercedes-e-class-w212/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
      0 => 
      array (
        'id' => 694,
        'tmplvarid' => 13,
        'contentid' => 610,
        'value' => 'ustanovka/mercedes_e-class_w212/2_e_out_cam.jpg',
      ),
      1 => 
      array (
        'id' => 695,
        'tmplvarid' => 14,
        'contentid' => 610,
        'value' => 'ustanovka/mercedes_e-class_w212/3_e_in_no_cam.jpg',
      ),
      2 => 
      array (
        'id' => 696,
        'tmplvarid' => 15,
        'contentid' => 610,
        'value' => 'ustanovka/mercedes_e-class_w212/4_e_in_cam.jpg',
      ),
      3 => 
      array (
        'id' => 697,
        'tmplvarid' => 12,
        'contentid' => 610,
        'value' => 'ustanovka/mercedes_e-class_w212/1_e_out_no_cam.jpg',
      ),
      4 => 
      array (
        'id' => 698,
        'tmplvarid' => 16,
        'contentid' => 610,
        'value' => 'ustanovka/mercedes_e-class_w212/5_e_car.jpg',
      ),
    ),
  ),
  46 => 
  array (
    'id' => 611,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'MERCEDES A-class W176',
    'longtitle' => '',
    'description' => '',
    'alias' => 'mercedes-a-class-w176',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => 'Установка Штатного видеорегистратора AXIOM SPECIAL Wi-Fi в MERCEDES A-Class W176<br><br>

-штатная накладка под датчиком дождя<br><br>

<p><img src="ustanovka/mercedes_a-class_w176/8_a_out_no_cam.jpg" alt="" width="70%" height="70%" /></p><br>

- накладка со встроенным видеорегистратором<br><br>

<p><img src="ustanovka/mercedes_a-class_w176/9_a_out_cam.jpg" alt="" width="70%" height="70%" /></p><br>
',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 45,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1459462278,
    'editedby' => 0,
    'editedon' => 0,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1459462278,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/mercedes-a-class-w176/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
      0 => 
      array (
        'id' => 699,
        'tmplvarid' => 12,
        'contentid' => 611,
        'value' => 'ustanovka/mercedes_a-class_w176/6skcafsu1aq.jpg',
      ),
      1 => 
      array (
        'id' => 700,
        'tmplvarid' => 13,
        'contentid' => 611,
        'value' => 'ustanovka/mercedes_a-class_w176/6_gla_no_cam.jpg',
      ),
      2 => 
      array (
        'id' => 701,
        'tmplvarid' => 14,
        'contentid' => 611,
        'value' => 'ustanovka/mercedes_a-class_w176/7_gla_cam.jpg',
      ),
      3 => 
      array (
        'id' => 702,
        'tmplvarid' => 15,
        'contentid' => 611,
        'value' => 'ustanovka/mercedes_a-class_w176/8_a_out_no_cam.jpg',
      ),
      4 => 
      array (
        'id' => 703,
        'tmplvarid' => 16,
        'contentid' => 611,
        'value' => 'ustanovka/mercedes_a-class_w176/9_a_out_cam.jpg',
      ),
    ),
  ),
  47 => 
  array (
    'id' => 612,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'MERCEDES C-class W205',
    'longtitle' => '',
    'description' => '',
    'alias' => 'mercedes-c-class-w205',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => 'Установка видеорегистратора AXIOM SPECIAL Wi-Fi в MERCEDES C-class W205<br><br>

Всем привет!<br><br>

Поставил регистратор AXIOM SPECIAL Wi-Fi на Mercedes W205<br>
Ребята молодцы - наконец кто-то придумал нормальное решение,<br>
без колхоза на стекле<br><br>

Ставили в выходные в Аларме на Ферганской<br>
постоянный плюс и зажигание подключили под полом у переднего пассажира,<br>
хотя Максим говорил мастеру, что удобнее брать зажигание с фишки переключателя света (по левую руку от водителя)<br>
и там же, в торце панели, есть предохранители с постоянным плюсом <br><br>

На телефон закачал приложение, подключился к регистратору по Wi-Fi,<br>
все сразу заработало<br><br>

Для проверки немного покатались по окрестностям,<br>
видео обещали выложить<br><br>

На машине будет ездить жена,<br>
неплохой подарок получился :)<br><br>',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 46,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1459462424,
    'editedby' => 0,
    'editedon' => 0,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1459462424,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/mercedes-c-class-w205/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
      0 => 
      array (
        'id' => 704,
        'tmplvarid' => 12,
        'contentid' => 612,
        'value' => 'ustanovka/mercedes_c-class_w205/c_car.jpg',
      ),
      1 => 
      array (
        'id' => 705,
        'tmplvarid' => 13,
        'contentid' => 612,
        'value' => 'ustanovka/mercedes_c-class_w205/c_out_no_camera.jpg',
      ),
      2 => 
      array (
        'id' => 706,
        'tmplvarid' => 14,
        'contentid' => 612,
        'value' => 'ustanovka/mercedes_c-class_w205/c_out.jpg',
      ),
      3 => 
      array (
        'id' => 707,
        'tmplvarid' => 15,
        'contentid' => 612,
        'value' => 'ustanovka/mercedes_c-class_w205/mercedes_car.jpg',
      ),
      4 => 
      array (
        'id' => 708,
        'tmplvarid' => 16,
        'contentid' => 612,
        'value' => 'ustanovka/mercedes_c-class_w205/mercedes_man.jpg',
      ),
      5 => 
      array (
        'id' => 709,
        'tmplvarid' => 17,
        'contentid' => 612,
        'value' => 'ustanovka/mercedes_c-class_w205/mercedes_0002_out_no_cam.jpg',
      ),
      6 => 
      array (
        'id' => 710,
        'tmplvarid' => 18,
        'contentid' => 612,
        'value' => 'ustanovka/mercedes_c-class_w205/mercedes_0003_out_cam.jpg',
      ),
      7 => 
      array (
        'id' => 711,
        'tmplvarid' => 19,
        'contentid' => 612,
        'value' => 'ustanovka/mercedes_c-class_w205/mercedes_0004_in_no_cam.jpg',
      ),
      8 => 
      array (
        'id' => 712,
        'tmplvarid' => 20,
        'contentid' => 612,
        'value' => 'ustanovka/mercedes_c-class_w205/mercedes_0005_in_cam.jpg',
      ),
    ),
  ),
  48 => 
  array (
    'id' => 613,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'MERCEDES GL X166 / ML W166',
    'longtitle' => '',
    'description' => '',
    'alias' => 'mercedes-gl-x166-/-ml-w166',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => 'Установка видеорегистратора AXIOM SPECIAL Wi-Fi в MERCEDES GL X166 / ML W166 ',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 47,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1459462493,
    'editedby' => 1,
    'editedon' => 1465937209,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1459462440,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/mercedes-gl-x166-/-ml-w166/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
      0 => 
      array (
        'id' => 713,
        'tmplvarid' => 12,
        'contentid' => 613,
        'value' => 'ustanovka/mercedes_gl_x164_ml_w166/gl_car.jpg',
      ),
      1 => 
      array (
        'id' => 714,
        'tmplvarid' => 13,
        'contentid' => 613,
        'value' => 'ustanovka/mercedes_gl_x164_ml_w166/gl_in_no_camera.jpg',
      ),
      2 => 
      array (
        'id' => 715,
        'tmplvarid' => 14,
        'contentid' => 613,
        'value' => 'ustanovka/mercedes_gl_x164_ml_w166/gl_in.jpg',
      ),
      3 => 
      array (
        'id' => 716,
        'tmplvarid' => 15,
        'contentid' => 613,
        'value' => 'ustanovka/mercedes_gl_x164_ml_w166/gl_out_no_camera.jpg',
      ),
      4 => 
      array (
        'id' => 717,
        'tmplvarid' => 16,
        'contentid' => 613,
        'value' => 'ustanovka/mercedes_gl_x164_ml_w166/gl_out.jpg',
      ),
    ),
  ),
  49 => 
  array (
    'id' => 629,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'BMW X3 F25',
    'longtitle' => '',
    'description' => '',
    'alias' => 'bmw-x3-f25',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => 'Добрый день, вот как неделю используем видеорегистратор Axiom BMW Special WiFi F-series со скрытой установкой в штатное место (под зеркало заднего вида), и ощутили полную целесообразность данного девайса.<br>
Инсталлировали нам сей видеорегистратор в IRON SECURITY (Москва), за 1.5 часа, подключали не к люстре, а за бардачёк,<br><br>

<p><img src="ustanovka/bmw_x3_f25/1.jpg" alt="" width="70%" height="70%" /></p><br>

далее через заранее скаченное мой приложение TRANSCEND DRIVE PRO на смартфон, подружили его по wifi, и автоматически прошла синхронизация времени/даты с телефоном, следующим шагом быстро выставил нужные мне настройки, управляя через телефон,и регистратор был готов вести все записи на micro CD карту, кстати в комплекте идет на 8Gb, далее сам поменял на 32 Gb.<br><br>

<p><img src="ustanovka/bmw_x3_f25/2.jpg" alt="" width="70%" height="70%" /></p><br>

Используем настройки записи в Full HD 1080p 30к/сек, и стоит вариант цикличности, также есть поддержка G-sensorа с 5-ти бальной настройкой чувствительности и штамп даты/времени, звук можно отключить. Пишет и сохраняет в формате MPEG-4 Media File.<br><br>

<p><img src="ustanovka/bmw_x3_f25/3.jpg" alt="" width="70%" height="70%" /></p><br>
<p><img src="ustanovka/bmw_x3_f25/4.jpg" alt="" width="70%" height="70%" /></p><br>


<iframe width="560" height="315" src="https://www.youtube.com/embed/TeIuxFi2na4" frameborder="0" allowfullscreen></iframe>',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 49,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1465757223,
    'editedby' => 1,
    'editedon' => 1465757467,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1459462440,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/bmw-x3-f25/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
      0 => 
      array (
        'id' => 760,
        'tmplvarid' => 12,
        'contentid' => 629,
        'value' => 'ustanovka/bmw_x3_f25/1.jpg',
      ),
      1 => 
      array (
        'id' => 761,
        'tmplvarid' => 13,
        'contentid' => 629,
        'value' => 'ustanovka/bmw_x3_f25/2.jpg',
      ),
      2 => 
      array (
        'id' => 762,
        'tmplvarid' => 14,
        'contentid' => 629,
        'value' => 'ustanovka/bmw_x3_f25/3.jpg',
      ),
      3 => 
      array (
        'id' => 763,
        'tmplvarid' => 15,
        'contentid' => 629,
        'value' => 'ustanovka/bmw_x3_f25/4.jpg',
      ),
    ),
  ),
  50 => 
  array (
    'id' => 633,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'VOLVO XC70 AXIOM 1100',
    'longtitle' => '',
    'description' => '',
    'alias' => 'volvo-xc70-axiom-1100',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => 'Авторская установка Видеорегистратора AXIOM CAR VISION 1100 в VOLVO XC70<br><br>

Кронштейн регистратора спрятан под кожухом датчика дождя, снаружи выступает только камера<br><br>

Камера необычная, по просьбе нашего постоянного пользователя, мы дали камеру от серии premium - без HDMI разьема, провода припаяны непосредственно к плате, кронштейн укорочен<br><br>

Это даёт разбег для творчества, камеру легче спрятать и придвинуть ближе к плоскости стекла<br><br>

Обратите внимание на интересную установку блока записи - он спрятан под панелью приборов,<br>
для доступа к видеорегистратору AXIOM 1100<br>
нужно снять боковую заглушку панели приборов<br><br>

Смотрите фото, для нашего пользователя это уже не первый AXIOM и далеко не первая машина для установки регистратора<br><br>

Спасибо за верность! :) <br><br>
',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 50,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1468222612,
    'editedby' => 1,
    'editedon' => 1468222843,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1459462440,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/volvo-xc70-axiom-1100/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
      0 => 
      array (
        'id' => 775,
        'tmplvarid' => 12,
        'contentid' => 633,
        'value' => 'ustanovka/volvo_xc70_ axiom_1100/1.jpg',
      ),
      1 => 
      array (
        'id' => 776,
        'tmplvarid' => 13,
        'contentid' => 633,
        'value' => 'ustanovka/volvo_xc70_ axiom_1100/2.jpg',
      ),
      2 => 
      array (
        'id' => 777,
        'tmplvarid' => 14,
        'contentid' => 633,
        'value' => 'ustanovka/volvo_xc70_ axiom_1100/3.jpg',
      ),
      3 => 
      array (
        'id' => 778,
        'tmplvarid' => 15,
        'contentid' => 633,
        'value' => 'ustanovka/volvo_xc70_ axiom_1100/4.jpg',
      ),
    ),
  ),
  51 => 
  array (
    'id' => 638,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'BMW X6(E71) AXIOM SPECIAL Wi-Fi',
    'longtitle' => '',
    'description' => '',
    'alias' => 'bmw-x6(e71)-axiom-special-wi-fi',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => 'Снова довольный клиент :) <br>
Машина BMW X6 E71<br>
Видеорегистратор<br>
AXIOM SPECIAL Wi-Fi BMW E-series<br>
Установка в Аларм-Сервис на Вавилова<br><br>',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 51,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1469041781,
    'editedby' => 1,
    'editedon' => 1469042593,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1468966440,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/bmw-x6(e71)-axiom-special-wi-fi/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
      0 => 
      array (
        'id' => 781,
        'tmplvarid' => 12,
        'contentid' => 638,
        'value' => 'ustanovka/bmw_x6_e71_axiom_special_wi-fi/bmw_x6_old_0001_bmw_car.jpg',
      ),
      1 => 
      array (
        'id' => 782,
        'tmplvarid' => 13,
        'contentid' => 638,
        'value' => 'ustanovka/bmw_x6_e71_axiom_special_wi-fi/bmw_x6_old_0000_bmw_man.jpg',
      ),
      2 => 
      array (
        'id' => 783,
        'tmplvarid' => 14,
        'contentid' => 638,
        'value' => 'ustanovka/bmw_x6_e71_axiom_special_wi-fi/bmw_x6_in_no_cam.jpg',
      ),
      3 => 
      array (
        'id' => 784,
        'tmplvarid' => 15,
        'contentid' => 638,
        'value' => 'ustanovka/bmw_x6_e71_axiom_special_wi-fi/bmw_x6_in_cam.jpg',
      ),
    ),
  ),
  52 => 
  array (
    'id' => 639,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'INFINITY QX50 AXIOM SPECIAL Wi-Fi',
    'longtitle' => '',
    'description' => '',
    'alias' => 'infinity-qx50-axiom-special-wi-fi',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => 'AXIOM SPECIAL Wi-Fi<br>
Версия для INFINITY QX50/QX70<br><br>

Регистратор монтируется на место штатной накладки под датчиком дождя<br><br>

Установка выполнена в IRON SECURITY <br>
Москва ул.Киевская 14 <br><br>',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 52,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1469042928,
    'editedby' => 1,
    'editedon' => 1469043412,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1468966440,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/infinity-qx50-axiom-special-wi-fi/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
      0 => 
      array (
        'id' => 785,
        'tmplvarid' => 12,
        'contentid' => 639,
        'value' => 'ustanovka/infinity_qx50_axiom_special_wi-fi/1.jpg',
      ),
      1 => 
      array (
        'id' => 786,
        'tmplvarid' => 13,
        'contentid' => 639,
        'value' => 'ustanovka/infinity_qx50_axiom_special_wi-fi/2.jpg',
      ),
      2 => 
      array (
        'id' => 787,
        'tmplvarid' => 14,
        'contentid' => 639,
        'value' => 'ustanovka/infinity_qx50_axiom_special_wi-fi/3.jpg',
      ),
    ),
  ),
  53 => 
  array (
    'id' => 640,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'AUDI A4 (2009) AXIOM SPECIAL Wi-Fi',
    'longtitle' => '',
    'description' => '',
    'alias' => 'audi-a4-(2009)-axiom-special-wi-fi',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => 'Нетривиальная установка видеорегистратора <br>
AXIOM SPECIAL Wi-Fi AUDI 2008 - 2012<br><br>

Внутри оригинальной накладки находился датчик температуры <br>
(он присутствует на некоторых ранних версиях)<br>
Места в накладке с видеорегистратором для него нет<br>
Решили вынуть датчик из корпуса, подготовить поверхность и установить рядом, прикрепив двусторонним скотчем в районе шелкографии <br><br>

Слева стоит антена сигнализации, справа датчик,<br>
А место накладки занял Wi-Fi видеорегистратор для AUDI 2008-2012 <br><br>

На фото Владимир Фёдорович, владелец машины<br><br>

Изначально Владимир хотел купить AXIOM 1100 и вмонтировать камеру в корпус под датчиком дождя, но потом увидел решение AXIOM SPECIAL Wi-Fi и решил устанавливать регистратор в накладке.<br><br>',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 53,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1469043731,
    'editedby' => 1,
    'editedon' => 1469044317,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1468966440,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/audi-a4-(2009)-axiom-special-wi-fi/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
      0 => 
      array (
        'id' => 789,
        'tmplvarid' => 12,
        'contentid' => 640,
        'value' => 'ustanovka/audi_a4_2009_axiom_special_wi-fi/1.jpg',
      ),
      1 => 
      array (
        'id' => 790,
        'tmplvarid' => 13,
        'contentid' => 640,
        'value' => 'ustanovka/audi_a4_2009_axiom_special_wi-fi/2.jpg',
      ),
      2 => 
      array (
        'id' => 791,
        'tmplvarid' => 14,
        'contentid' => 640,
        'value' => 'ustanovka/audi_a4_2009_axiom_special_wi-fi/3.jpg',
      ),
      3 => 
      array (
        'id' => 792,
        'tmplvarid' => 15,
        'contentid' => 640,
        'value' => 'ustanovka/audi_a4_2009_axiom_special_wi-fi/4.jpg',
      ),
      4 => 
      array (
        'id' => 793,
        'tmplvarid' => 16,
        'contentid' => 640,
        'value' => 'ustanovka/audi_a4_2009_axiom_special_wi-fi/5.jpg',
      ),
      5 => 
      array (
        'id' => 794,
        'tmplvarid' => 17,
        'contentid' => 640,
        'value' => 'ustanovka/audi_a4_2009_axiom_special_wi-fi/6.jpg',
      ),
    ),
  ),
  54 => 
  array (
    'id' => 647,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Nissan Qashqai 2014-2016',
    'longtitle' => '',
    'description' => '',
    'alias' => 'nissan-qashqai-2014-2016',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 2,
    'isfolder' => 0,
    'introtext' => '',
    'content' => 'Сначала переворачиваем кронштейн крепления:<br>
было стало<br><br>

<a href="ustanovka/nissan_qashqai/1.jpg" class="z-ust-pics cboxElement"><img src="ustanovka/nissan_qashqai/1.jpg" alt="" width=225 height=300 /></a>
<a href="ustanovka/nissan_qashqai/2.jpg" class="z-ust-pics cboxElement"><img src="ustanovka/nissan_qashqai/2.jpg" alt="" width=225 height=300 /></a><br><br>

Сначала пробуем приложить и по картинке наблюдаем, чтобы обзор камеры не захватывал теснения лобового стекла у зеркала. Я приклеил: справа граница сплошного теснения, снизу самое начало теснения<br><br>

<a href="ustanovka/nissan_qashqai/3.jpg" class="z-ust-pics cboxElement"><img src="ustanovka/nissan_qashqai/3.jpg" alt="" width=225 height=300 /></a><br><br>

Далее провод легко заводится в короб проводов идущий к датчику дождя (с переднего края даже есть выемка) и далее к обшивке потолка<br><br>

<a href="ustanovka/nissan_qashqai/4.jpg" class="z-ust-pics cboxElement"><img src="ustanovka/nissan_qashqai/4.jpg" alt="" width=225 height=300 /></a><br><br>

Беремся двумя руками за боковую накладку стойки и тянем её, отходит от стойки при применении небольшого усилия, отодвигаем немного и протаскиваем провод<br><br>

<a href="ustanovka/nissan_qashqai/5.jpg" class="z-ust-pics cboxElement"><img src="ustanovka/nissan_qashqai/5.jpg" alt="" width=225 height=300 /></a><br><br>

Подключаем кнопку. Принято решение в нижний ряд кнопок, в свободную ячейку рядом с кнопкой ECO.Снимаем крышку предохранителей и потихоньку тянем панель с кнопками фиксаторы два <br>
слева вверху и внизу, один справа и два болта снизу <br><br>

<a href="ustanovka/nissan_qashqai/6.jpg" class="z-ust-pics cboxElement"><img src="ustanovka/nissan_qashqai/6.jpg" alt="" width=225 height=300 /></a>
<a href="ustanovka/nissan_qashqai/7.jpg" class="z-ust-pics cboxElement"><img src="ustanovka/nissan_qashqai/7.jpg" alt="" width=300 height=225 /></a><br><br>

Откручиваем изнутри два самореза крепления верхнего ряда кнопок, отодвигаем<br><br>

<a href="ustanovka/nissan_qashqai/8.jpg" class="z-ust-pics cboxElement"><img src="ustanovka/nissan_qashqai/8.jpg" alt="" width=225 height=300 /></a><br><br>

второй ряд три самореза два видно третий в глубине<br><br>

<a href="ustanovka/nissan_qashqai/9.jpg" class="z-ust-pics cboxElement"><img src="ustanovka/nissan_qashqai/9.jpg" alt="" width=300 height=225 /></a>
<a href="ustanovka/nissan_qashqai/10.jpg" class="z-ust-pics cboxElement"><img src="ustanovka/nissan_qashqai/10.jpg" alt="" width=225 height=300 /></a><br><br>

Отсоединяем штекер ECO кнопки ( в моем случае он был вообще не подсоединен, подсоединил и теперь на панели при её нажатии загорается индикатор на панели приборов). Вынимаем нужную нам заглушку сверлим по центру сначала 4 мм, потом 10 мм и протаскиваем через неё кнопку из комплекта. Кнопку приклеиваем к заглушке через двух сторонний скотч из комплекта.<br><br>

<a href="ustanovka/nissan_qashqai/11.jpg" class="z-ust-pics cboxElement"><img src="ustanovka/nissan_qashqai/11.jpg" alt="" width=225 height=300 /></a><br><br>

Разместить блок управления и сам регистратор решил слева под рулевой колонкой. Площадку завод изготовитель предусмотрел.<br>
Очень все прекрасно разместилось. Штатный крепеж регистратора прекрасно приклеился и разместился на полочке. Блок управления прикрепил с помощью липучек (все идет в комплекте). Доступ шикарный, по моему лучше чем в бардачке. <br><br>

<a href="ustanovka/nissan_qashqai/12.jpg" class="z-ust-pics cboxElement"><img src="ustanovka/nissan_qashqai/12.jpg" alt="" width=225 height=300 /></a>
<a href="ustanovka/nissan_qashqai/13.jpg" class="z-ust-pics cboxElement"><img src="ustanovka/nissan_qashqai/13.jpg" alt="" width=300 height=225 /></a><br><br>

Далее подключаем питание, к прикуривателю который расположен в бардачке, он в отличие от прикуривателя под магнитолой отключается. Блок предохранителей отпадает сразу, так как питание в прикуриватель в подлокотнике идет через реле (слышен щелчок при отключении) то соответственно найти его провод в блоке предохранителей не возможно. Подключаться к другим проводам на которых пропадает напряжение при выключении зажигания не стал. ХЗ к чему это может привести.<br>
Снимаем накладку рядом с педалью газа, вытаскивать её начинаем с правой стороны (защелки), слева крепиться через клипсу в которую надо заводить соответствующий паз накладки и протягиваем провод в центральную консоль<br><br>

<a href="ustanovka/nissan_qashqai/14.jpg" class="z-ust-pics cboxElement"><img src="ustanovka/nissan_qashqai/14.jpg" alt="" width=225 height=300 /></a><br><br>

Предварительно вытащив подстаканники. Я тупо потянул изнутри верх и получилось.<br>
Провода подсоединяем к синему "+" к черному "-"<br><br>

<a href="ustanovka/nissan_qashqai/15.jpg" class="z-ust-pics cboxElement"><img src="ustanovka/nissan_qashqai/15.jpg" alt="" width=225 height=300 /></a><br><br>

Ура все шикарно<br><br>

<a href="ustanovka/nissan_qashqai/16.jpg" class="z-ust-pics cboxElement"><img src="ustanovka/nissan_qashqai/16.jpg" alt="" width=225 height=300 /></a><br><br>
',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 54,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1471015333,
    'editedby' => 1,
    'editedon' => 1471015467,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1468966440,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'kak-postavit/nissan-qashqai-2014-2016/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'videos' => 
    array (
    ),
    'images' => 
    array (
      0 => 
      array (
        'id' => 807,
        'tmplvarid' => 12,
        'contentid' => 647,
        'value' => 'ustanovka/nissan_qashqai/1.jpg',
      ),
      1 => 
      array (
        'id' => 808,
        'tmplvarid' => 13,
        'contentid' => 647,
        'value' => 'ustanovka/nissan_qashqai/3.jpg',
      ),
      2 => 
      array (
        'id' => 809,
        'tmplvarid' => 14,
        'contentid' => 647,
        'value' => 'ustanovka/nissan_qashqai/4.jpg',
      ),
      3 => 
      array (
        'id' => 810,
        'tmplvarid' => 15,
        'contentid' => 647,
        'value' => 'ustanovka/nissan_qashqai/5.jpg',
      ),
      4 => 
      array (
        'id' => 811,
        'tmplvarid' => 16,
        'contentid' => 647,
        'value' => 'ustanovka/nissan_qashqai/6.jpg',
      ),
      5 => 
      array (
        'id' => 812,
        'tmplvarid' => 17,
        'contentid' => 647,
        'value' => 'ustanovka/nissan_qashqai/7.jpg',
      ),
      6 => 
      array (
        'id' => 813,
        'tmplvarid' => 18,
        'contentid' => 647,
        'value' => 'ustanovka/nissan_qashqai/8.jpg',
      ),
      7 => 
      array (
        'id' => 814,
        'tmplvarid' => 19,
        'contentid' => 647,
        'value' => 'ustanovka/nissan_qashqai/9.jpg',
      ),
      8 => 
      array (
        'id' => 815,
        'tmplvarid' => 20,
        'contentid' => 647,
        'value' => 'ustanovka/nissan_qashqai/10.jpg',
      ),
      9 => 
      array (
        'id' => 816,
        'tmplvarid' => 21,
        'contentid' => 647,
        'value' => 'ustanovka/nissan_qashqai/11.jpg',
      ),
    ),
  ),
);