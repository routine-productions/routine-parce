<?php
/*
 * Get:  links and Titles of Category
 */

define('__HOMEDIR__', __DIR__);

require_once __DIR__ . '/../core.php';
require_once '../simple_html_dom.php';

$Links = [];
$LinksForSave = [];
$Dom->loadFromUrl('http://www.mos-nasos.ru/catalog/');
$key = 0;
foreach ($Dom->find('div[class=sidebar-widget] ul[class=sub-menu] li a') as $link)
{
    if(count(file_get_html($link->href)->find('div[class=sidebar-widget] ul[class=sub-menu] li ul[class=sub-menu] li a')) == 0)
    {
        $Links[$key]['title'] = $link->text;
        $Links[$key]['href'] = str_replace('http://www.mos-nasos.ru','wp-content/uploads/2016/09',$link->href);


        /**
         * Parse content parent link
         */
        $dom = file_get_html($link->href);
        $Links[$key]['content'] = '';
        foreach ($dom->find('main') as $p)
        {
            $Links[$key]['content'] = $Links[$key]['content'].$p->plaintext;
        }

        /**
         * Parse passport and doc
         */
        $Links[$key]['description'] = getDescription($dom->find('div.slide p a'));
        
        /**
         * Parse table with technical characteristic
         */
        $characteristic = '';
        foreach ($dom->find('main table') as $keyTable => $tableItem)
        {
            $characteristic .= $tableItem->outertext;
        }
        $Links[$key]['characteristics'] = $characteristic;

        //Отрезаем всё не нужное в описании
        if(strpos($Links[$key]['content'],'Технические характеристики'))
            $Links[$key]['content'] = substr_replace($Links[$key]['content'], '' ,strpos($Links[$key]['content'],'Технические характеристики'));
        if(strpos($Links[$key]['content'],'Модель насоса Подача'))
            $Links[$key]['content'] = substr_replace($Links[$key]['content'], '' ,strpos($Links[$key]['content'],'Модель насоса Подача'));
        if(strpos($Links[$key]['content'],'Технические характеристики насосов ЭЦВ'))
            $Links[$key]['content'] = substr_replace($Links[$key]['content'], '' ,strpos($Links[$key]['content'],'Технические характеристики насосов ЭЦВ'));
        if(strpos($Links[$key]['content'],'Технические характеристики БЦПЭ 0,32'))
            $Links[$key]['content'] = substr_replace($Links[$key]['content'], '' ,strpos($Links[$key]['content'],'Технические характеристики БЦПЭ 0,32'));
        if(strpos($Links[$key]['content'],'Технические характеристики    Марка насоса Подача'))
            $Links[$key]['content'] = substr_replace($Links[$key]['content'], '' ,strpos($Links[$key]['content'],'Технические характеристики    Марка насоса Подача'));
        if(strpos($Links[$key]['content'],'Модельный ряд и характеристики'))
            $Links[$key]['content'] = substr_replace($Links[$key]['content'], '' ,strpos($Links[$key]['content'],'Модельный ряд и характеристики'));
        if(strpos($Links[$key]['content'],'Характеристики насосов ЭСН'))
            $Links[$key]['content'] = substr_replace($Links[$key]['content'], '' ,strpos($Links[$key]['content'],'Характеристики насосов ЭСН'));
        if(strpos($Links[$key]['content'],'&nbsp; ЦМФ 10-10 реж'))//не оригинальная строка для поиска
            $Links[$key]['content'] = substr_replace($Links[$key]['content'], '' ,strpos($Links[$key]['content'],'&nbsp; ЦМФ 10-10 реж'));
        if(strpos($Links[$key]['content'],'&nbsp; &nbsp; Автоматические выключатели DEKraft'))
            $Links[$key]['content'] = substr_replace($Links[$key]['content'], '' ,strpos($Links[$key]['content'],'&nbsp; &nbsp; Автоматические выключатели DEKraft'));
        //*********************************

        //собираем картинки у родительских ссылок
            //если картинка обёрнута в <а> или если нет

        $linksImg = '';
        foreach ($dom->find('main p img, main p a img') as $keyImg => $imgItem)
        {
            $linksImg .= $imgItem->src.' ';
            //Сохраняем на комп
            if($imgItem->src == 'http://www.mos-nasos.ru/wp-content/uploads/2015/01/Бытовые-насосы-БЦПЭУ.jpg')
                continue;
            $imgItem->src = preg_replace('~http://www.mos-nasos.ru~','',$imgItem->src);
            Save_File($imgItem->src,'http://www.mos-nasos.ru');
        }
        $Links[$key]['img'] = $linksImg;
        $key++;

    }else {

        /**
         * If link has child, parse this child
         */

        $Dom->loadFromUrl($link->href);
        foreach ($Dom->find('div[class=sidebar-widget] ul[class=sub-menu] li ul[class=sub-menu] li a') as $link2) {
            $Links[$key]['title'] = $link2->text;
            $Links[$key]['href'] = $link2->href;

            $Links[$key]['href'] = str_replace('http://www.mos-nasos.ru', 'wp-content/uploads/2016/09', $Links[$key]['href']);

            $Dom->loadFromUrl($link2->href);

            //СОБИРАЕМ ВСЕ КАРТИНКИ У ДОЧЕРНИХ ССЫЛОК
            $linksImg = '';
            foreach ($Dom->find('main p img') as $keyImg => $imgItem) {
                $linksImg .= $imgItem->src.' ';
                $imgItem->src = preg_replace('~http://www.mos-nasos.ru~','',$imgItem->src);
                Save_File($imgItem->src,'http://www.mos-nasos.ru');
            }
            $Links[$key]['img'] = $linksImg;

            $dom = file_get_html($link2->href);
            $Links[$key]['content'] = '';
            foreach ($dom->find('main p') as $p) {
                $Links[$key]['content'] = $Links[$key]['content'] . $p->plaintext;
            }

            $Links[$key]['description'] = getDescription($dom->find('div.slide p a'));

            $characteristic = '';
            foreach ($dom->find('main table') as $keyTable => $tableItem) {
                $characteristic .= $tableItem->outertext;
            }
            $Links[$key]['characteristics'] = $characteristic;
            
            $Links[$key]['content'] = substr_replace($Links[$key]['content'], '', strpos($Links[$key]['content'], 'Скачать паспорт'));
            $key++;
        }
    }

    /**
     * ***********************************
     */
    $Dom->loadFromUrl('http://www.mos-nasos.ru/catalog/');
}

unset($Links[18]);
file_put_contents(__DIR__ . '/results/all_links.php', "<?php \n    return " . var_export($Links, true) . ";");


function getDescription($dom)
{
    $description = '';
    foreach ($dom as $passport)
    {
        if(strpos($passport->href, '.jpg'))
            break;
        $description .= $passport->href.' ';
        $passport->href = preg_replace('~http://www.mos-nasos.ru~','',$passport->href);
        Save_File($passport->href,'http://www.mos-nasos.ru');
    }
    return $description;
}




