<?php
/*
 * Get: Pages links
 */
define('__HOMEDIR__', __DIR__);
require_once __DIR__ . '/../../core.php';
$Pages = [];

// Get: Categories
$Categories = require_once __DIR__ . '/results/01_stage.php';


foreach ($Categories as $Category_Key => $Category)
{

    if (empty($Category['children']))
    {
        // Get: First level
        Parse_Links($Category, $Category_Key);
    } else
    {
        // Get: Second level
        foreach ($Category['children'] as $Sub_Category_Key => $Sub_Category)
        {
            Parse_Links($Sub_Category, $Sub_Category_Key, $Category_Key);
        }
    }
}

// Parse: All links with categories
function Parse_Links($Category, $Category_Key, $Parent_Key = false)
{
    global $Dom;
    global $Pages;

    // Load: Category
    $Dom->loadFromURL('http://topfranchise.ru' . $Category['href']);

    // Parse: Links
    foreach ($Dom->find('.block_fran .item') as $Page)
    {
        $Pages[] = [
            'href' => $Page->find('.wrapper a')->href,
            'title' => $Page->find('.wrapper a')->text,
            'img' => str_replace('\');', '', str_replace('background-image: url(\'', '', $Page->find('a')->style)),
            'first_parent' => $Category_Key,
            'second_parent' => $Parent_Key,
        ];
    }
}

// Write: To file
file_put_contents(__DIR__ . '/results/02_stage.php', "<?php \n    return " . var_export($Pages, true) . ";");