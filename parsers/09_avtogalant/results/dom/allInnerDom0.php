<?php 
    return array (
  5 => 
  array (
    'images' => 
    array (
      0 => 
      array (
        'title' => 'Автомобильные шины Белшина Artmotion HP Бел-285 225/45R17 94W',
        'src_full' => 'https://content2.onliner.by/catalog/device/large/ce873926ece4d1c9aebbfc3348da4396.jpg',
        'src_thumb' => 'https://content2.onliner.by/catalog/device/200x200/ce873926ece4d1c9aebbfc3348da4396.jpg',
      ),
      1 => 
      array (
        'title' => 'Автомобильные шины Белшина Artmotion HP Бел-285 225/45R17 94W',
        'src_full' => 'https://content2.onliner.by/catalog/device/large/c8f52f80e08fdbc3d43684a7dee1af37.jpg',
        'src_thumb' => 'https://content2.onliner.by/catalog/device/200x200/c8f52f80e08fdbc3d43684a7dee1af37.jpg',
      ),
    ),
    'descriptions' => 
    array (
      0 => 
      array (
        'title' => ' Назначение ',
        'description' => 'для легковых автомобилей',
      ),
      1 => 
      array (
        'title' => ' Сезон ',
        'description' => 'летние',
      ),
      2 => 
      array (
        'title' => ' Ширина профиля ',
        'description' => '225 мм',
      ),
      3 => 
      array (
        'title' => ' Серия (высота профиля) ',
        'description' => '45',
      ),
      4 => 
      array (
        'title' => ' Посадочный диаметр ',
        'description' => '17"',
      ),
      5 => 
      array (
        'title' => ' Конструкция ',
        'description' => 'радиальные',
      ),
      6 => 
      array (
        'title' => ' Способ герметизации ',
        'description' => 'бескамерные',
      ),
      7 => 
      array (
        'title' => ' Индекс скорости ',
        'description' => 'W (до 270 км/ч)',
      ),
      8 => 
      array (
        'title' => ' Индекс нагрузки ',
        'description' => '94',
      ),
      9 => 
      array (
        'title' => ' Возможность ошиповки ',
      ),
      10 => 
      array (
        'title' => ' Run-flat ',
      ),
    ),
    'manufacturer' => '',
    'category_original' => 'Белшина',
    'category_en' => 'belshina',
  ),
);