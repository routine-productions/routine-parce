<?php
/*
 * Get: Person links
 */
define('__HOMEDIR__', __DIR__);
require_once __DIR__ . '/../core.php';

$products = require_once __DIR__ . '/results/products_state.php';
//print_r($Video);

function replace_links($data)
{
    $data = str_replace('<img src="', '<img src="/upload/products/', $data);
    $data = str_replace('<a href="', '<a href="/upload/products/', $data);

    return $data;
}
foreach ($products as $item) {
    // Save Object
    $object = new B_Iblock_Element();
    $object->IBLOCK_ID = 6;
    $object->IN_SECTIONS = 'N';
    $object->CODE = $item['alias'];
    $object->NAME = $item['pagetitle'];
    $object->PREVIEW_TEXT = $item['pagetitle'];
    $object->save();

    // Price
    $prop = new B_Iblock_Element_Property();
    $prop->IBLOCK_PROPERTY_ID = 39;
    $prop->IBLOCK_ELEMENT_ID = $object->id;
    $prop->VALUE = (int)$item['product']['price'];
    $prop->VALUE_TYPE = 'text';
    $prop->save();

    // Описание
    $prop = new B_Iblock_Element_Property();
    $prop->IBLOCK_PROPERTY_ID = 49;
    $prop->IBLOCK_ELEMENT_ID = $object->id;
    $prop->VALUE = replace_links($item['content']);
    $prop->VALUE_TYPE = 'html';
    $prop->save();

    // Характеристики
    if (!empty($item['settings'])) {
        $prop = new B_Iblock_Element_Property();
        $prop->IBLOCK_PROPERTY_ID = 50;
        $prop->IBLOCK_ELEMENT_ID = $object->id;
        $prop->VALUE = replace_links($item['settings']['value']);
        $prop->VALUE_TYPE = 'html';
        $prop->save();
    }

    // Инструкция
    if (!empty($item['instruction'])) {
        $prop = new B_Iblock_Element_Property();
        $prop->IBLOCK_PROPERTY_ID = 55;
        $prop->IBLOCK_ELEMENT_ID = $object->id;
        $prop->VALUE = replace_links($item['instruction']['value']);
        $prop->VALUE_TYPE = 'html';
        $prop->save();
    }

    // Gallery
    if (!empty($item['gallery'])) {
        foreach ($item['gallery'] as $key => $img) {
            if (strlen($img['path']) < 6) {
                // Save File
                $file = new B_File();
                $file->MODULE_ID = 'iblock';
                $file->SUBDIR = 'iblock/assets/images/products/' . $img['path'];
                $file->CONTENT_TYPE = 'image/jpeg';
                $file->FILE_SIZE = 10136;
                $file->HEIGHT = 255;
                $file->WIDTH = 255;
                $file->FILE_NAME = $img['file'];
                $file->ORIGINAL_NAME = $img['file'];
                $file->save();

                // Extra Field
                $prop = new B_Iblock_Element_Property();
                $prop->IBLOCK_PROPERTY_ID = 48;
                $prop->IBLOCK_ELEMENT_ID = $object->id;
                $prop->VALUE = $file->id;
                $prop->VALUE_TYPE = 'file';
                $prop->save();

                if ($key == 0) {
                    $object->PREVIEW_PICTURE = $file->id;
                    $object->save();
                }
            }
        }
    }
}