<?php

return [
    [
        'title' => 'Автобизнес',
        'href' => 'http://openbusiness.ru/catalog/franshizy-avtobiznes/'
    ],
    [
        'title' => 'Товары и услуги для детей',
        'href' => 'http://openbusiness.ru/catalog/franshizy-tovary-i-uslugi-dlya-detey/'
    ],
    [
        'title' => 'Издательский бизнес и СМИ',
        'href' => 'http://openbusiness.ru/catalog/franshizy-izdatelskiy-biznes/'
    ],
    [
        'title' => 'Инновационные товары, технологии и Производство',
        'href' => 'http://openbusiness.ru/catalog/franshizy-tekhnologii-i-proizvodstvo/'
    ],
    [
        'title' => 'Рекламные услуги',
        'href' => 'http://openbusiness.ru/catalog/franshizy-reklamnye-uslugi/'
    ],
    [
        'title' => 'Красота и Здоровье',
        'href' => 'http://openbusiness.ru/catalog/franshizy-krasota-i-zdorove/'
    ],
    [
        'title' => 'Недвижимость',
        'href' => 'http://openbusiness.ru/catalog/franshizy-nedvizhimost/'
    ],
    [
        'title' => 'Одежда и Обувь',
        'href' => 'http://openbusiness.ru/catalog/franshizy-odezhda-i-obuv/'
    ],
    [
        'title' => 'Общественное питание и продукты',
        'href' => 'http://openbusiness.ru/catalog/franshizy-obshchestvennoe-pitanie-i-produkty/'
    ],
    [
        'title' => 'Образование',
        'href' => 'http://openbusiness.ru/catalog/franshizy-obrazovanie/'
    ],
    [
        'title' => 'Развлечения',
        'href' => 'http://openbusiness.ru/catalog/franshizy-razvlecheniya/'
    ],
    [
        'title' => 'Ритейл',
        'href' => 'http://openbusiness.ru/catalog/franshizy-riteyl/'
    ],
    [
        'title' => 'Строительные материалы',
        'href' => 'http://openbusiness.ru/catalog/franshiza-stroitelnye-materialy/'
    ],
    [
        'title' => 'Сфера услуг',
        'href' => 'http://openbusiness.ru/catalog/franshizy-sfera-uslug/'
    ],
    [
        'title' => 'Финансы',
        'href' => 'http://openbusiness.ru/catalog/franshizy-finansy/'
    ]
];