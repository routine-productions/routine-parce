<?php
/*
 * Get: Franchise page
 */
define('__HOMEDIR__', __DIR__);
require_once __DIR__ . '/../../core.php';

$Pages = require_once __DIR__ . '/results/02_links.php';
$Content = [];

foreach ($Pages as $Page_Key => $Page)
{
    $Dom->loadFromURL('http://openbusiness.ru' . $Page['href']);

    // Parse: Get main params
    $Content[$Page_Key] = [
        'parent' => $Page['parent'],
        'main_image' => '/assets/gallery' . $Page['img'],
        'title' => $Dom->find('h1', 0) ? $Dom->find('h1')->text : '',
        'content' => '',
        'requirements' => ''
    ];


    if ($Dom->find('.fransh_block', 0))
    {
        foreach ($Dom->find('.fransh_block') as $Data)
        {
            $Content[$Page_Key]['content'] .= $Data;
        }
    }

    if ($Dom->find('.fransh_block_trebovania', 0))
    {
        foreach ($Dom->find('.fransh_block_trebovania') as $Data)
        {
            $Content[$Page_Key]['requirements'] .= $Data;
        }
    }

    // Parse: Round params
    if ($Dom->find('.fransh_item_fin_uslovia div', 0))
    {
        foreach ($Dom->find('.fransh_item_fin_uslovia div') as $Parameter)
        {
            if (trim($Parameter->text) == 'Стоимость франшизы (Паушальный взнос)')
            {
                $Content[$Page_Key]['invest_price'] = $Parameter->find('b')->text;
            }
            if (trim($Parameter->text) == 'Роялти')
            {
                $Content[$Page_Key]['royalty'] = $Parameter->find('b')->text;
            }
            if (trim($Parameter->text) == 'Рекламный сбор')
            {
                $Content[$Page_Key]['advertising'] = $Parameter->find('b')->text;
            }
            if (trim($Parameter->text) == 'Рентабельность')
            {
                $Content[$Page_Key]['profitability'] = $Parameter->find('b')->text;
            }
            if (trim($Parameter->text) == 'Срок окупаемости')
            {
                $Content[$Page_Key]['payback'] = intval($Parameter->find('b')->text);
            }
            if (trim($Parameter->text) == 'Стартовый капитал')
            {
                $Content[$Page_Key]['start_capital'] = $Parameter->find('b')->text;
            }
        }
    }


    // Parse: Round params
    if ($Dom->find('.fransh_item_contacts', 0))
    {
        foreach ($Dom->find('.fransh_item_contacts') as $Parameter)
        {
            if ($Parameter->find('b', 0) && trim($Parameter->find('b')->text) == 'Наименование юр. лица:')
            {
                $Content[$Page_Key]['company_title'] = trim($Parameter->text);
            }
            if ($Parameter->find('b', 0) && trim($Parameter->find('b')->text) == 'Адрес:')
            {
                $Content[$Page_Key]['address'] = trim($Parameter->text);
            }
            if ($Parameter->find('b', 0) && $Parameter->find('a', 0) && trim($Parameter->find('b')->text) == 'e-mail:')
            {
                $Content[$Page_Key]['email'] = trim($Parameter->find('a')->text);
            }
            if ($Parameter->find('b', 0) && trim($Parameter->find('b')->text) == 'Телефон:')
            {
                $Content[$Page_Key]['phone'] = trim($Parameter->innerHtml);
            }
            if ($Parameter->find('b', 0) && $Parameter->find('a', 0) && trim($Parameter->find('b')->text) == 'Веб-Сайт:')
            {
                $Content[$Page_Key]['website'] = trim($Parameter->find('a')->href);
            }
            if ($Parameter->find('b', 0) && trim($Parameter->find('b')->text) == 'Контактное лицо:')
            {
                $Content[$Page_Key]['contact'] = trim($Parameter->text);
            }
        }
    }

    // Parse: Gallery
    if ($Dom->find('.main_fransh #RBSlideshow img', 0))
    {
        foreach ($Dom->find('.main_fransh #RBSlideshow img') as $Image)
        {
            $Content[$Page_Key]['gallery'][] = $Image->src;
        }
    }

    if ($Dom->find('.main_fransh .gallery-items img', 0))
    {
        foreach ($Dom->find('.main_fransh .gallery-items img') as $Image)
        {
            $Content[$Page_Key]['gallery'][] = $Image->src;
        }
    }

    // Parse: Round params
    if ($Dom->find('.fransh_item_about_company', 0))
    {
        $Content[$Page_Key]['foundation'] = $Dom->find('.fransh_item_about_company b', 0) ? $Dom->find('.fransh_item_about_company b', 0)->text : '';
        $Content[$Page_Key]['start_year'] = $Dom->find('.fransh_item_about_company b', 1) ? $Dom->find('.fransh_item_about_company b', 1)->text : '';
        $Content[$Page_Key]['motherland'] = $Dom->find('.fransh_item_about_company b', 2) ? $Dom->find('.fransh_item_about_company b', 2)->text : '';
        $Content[$Page_Key]['franchise_enterprises'] = $Dom->find('.fransh_item_about_company b', 3) ? $Dom->find('.fransh_item_about_company b', 3)->text : '';
        $Content[$Page_Key]['own_enterprises'] = $Dom->find('.fransh_item_about_company b', 4) ? $Dom->find('.fransh_item_about_company b', 4)->text : '';
    }

//    print_r($Content);
//    break;
}

// Write: To file
file_put_contents(__DIR__ . '/results/03_pages.php', "<?php \n    return " . var_export($Content, true) . ";");
