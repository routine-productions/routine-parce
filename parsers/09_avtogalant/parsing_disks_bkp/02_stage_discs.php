<?php

define('__HOMEDIR__', __DIR__);

require_once __DIR__ . '/../core.php';

$fetched_outer = include __DIR__ . '/results/all_disks_outer.php';
$count = include __DIR__ . '/db_disks_seeded_count.php';
$counter_saved = $count[0];
$amount = count($fetched_outer);


function set_options($product_id, $options)
{
//    product_id feature_id   lang_id  value   translit

//    echo '<table style="text-align: center; width : 100%">';
//    echo '<tbody>';
//    echo '<tr>';
    foreach ($options as $option) {

        if ($option['id'] && $option['value'] && strlen($option['value']) > 0) {


            $option_existing = Options::where(['feature_id' => $option['id'], 'product_id' => $product_id])->first();

            if (!isset($option_existing)) {
                $Options = new Options();
                $Options->product_id = $product_id;
                $Options->feature_id = $option['id'];
                $Options->value = $option['value'];
                $Options->lang_id = 1;
                $Options->translit = Create_Slug($option['value']);
                $Options->save();

//                echo '<br> Сохраняем опции';
            } else {
//                echo '<br> Не сохраняем опции';
            };
        }

    }
//    echo '</tr>';
//    echo '</tbody>';
//    echo '</table>';

}

function save_image($url, $product_id, $img = null)
{
//    id	name	product_id	filename	position	http_original_name

    $img_url = 'https:' . $url;
    $trim_name = substr($img_url, strrpos($img_url, '/') + 1);
    $path = __DIR__ . '/results/img/disks/' . $trim_name;
    $sitePath = 'design/avtogalant/images/' . $url;

    $image = new Images();
    $image->product_id = $product_id;
    $image->name = $trim_name;
    $image->filename = $trim_name;
    $image->http_original_name = $img_url;
    $image->save();

//    if (!file_exists($path)) {
//        file_put_contents($path, file_get_contents($img_url));
////        echo '<br> file saved ' . $path;
//    } else {
////        echo '<br> file allready saved ';
//    }
}

function get_first_word($string)
{
    return substr($string, 0, strpos(trim($string), ' '));
}


function disk_type($string)
{

    if (strrpos($string, 'литые алюминиевые') !== false) {
        return 'литые алюминиевые';

    } else {
        if (strrpos($string, 'штампованные стальные') !== false) {
            return 'штампованные стальные';

        } else {
            return null;
        }
    }
}

function replace_phrase_from($what, $from)
{
    if ($what == $from) {
        return $what;
    } else {
        return str_replace($what, '', $from);
    }

}
function replace_phrase_from_with($what, $from, $with){
    if ($what == $from) {
        return $what;
    } else {
        return str_replace($what, $with, $from);
    }
}

function trim_string_after_first($string, $symbol, $return_default = true)
{
    $trimmed = substr(strstr($string, $symbol), 1, strlen($string));

    if ($trimmed !== false) {
        return $trimmed;


    } else {
        if ($return_default === true) {
            return $string;
        } else {
            return null;
        }

    }
}

$disc_opt_register = [
    'ET' => [
        'name' => 'вылет',
        'id' => 15,
        'url' => 'vyletet',
        'infilter' => 1,
        'trim_by' => ',',
        'needle' => 'ET'
    ],
    'DIA' => ['name' => 'Диаметр центрального отверстия (DIA)',
        'id' => 14,
        'url' => 'posadochnyjdiametr',
        'trim_by' => ',',
        'needle' => 'DIA'
    ],
    'PCD' => ['name' => 'Диаметр расположения отверстий (PCD)',
        'id' => 13,
        'url' => 'diametrraspolozheniyaotverstijpc',
        'trim_by' => ',',
        'needle' => 'PCD'
    ],


    'литые алюминиевые' => ['name' => 'Тип',
        'id' => 9,
        'url' => 'tip',
        'trim_by' => '\'\'',
        'needle' => 'литые алюминиевые'
    ],
    'штампованные стальные' => ['name' => 'Тип', 'id' => 9,
        'url' => 'tip',
        'trim_by' => '\'\'',
        'needle' => 'штампованные стальные'
    ],
    'ширина обода' => ['name' => 'Тип', 'id' => 9,
        'url' => 'tip',
        'trim_by' => '\'\'',
        'needle' => 'ширина обода'
    ],
//
//
//    'COLOR' => ['name' => 'Цвет диска',
//        'id' => 10,
//        'url' => 'kolichestvokrepezhnyhotverstij',
//        'trim_by' => '\'\'',
//        'needle'=> 'color'
//    ],

];
$disc_add_opt_register = [
    '&quot;' => ['name' => 'Количество крепежных отверстий',
        'id' => 12,
        'url' => 'kolichestvokrepezhnyhotverstij',
        'trim_by' => 'мм'
    ],
//    'POSD' => ['name' => 'Посадочная ширина',
//        'id' => 11,
//        'url' => 'posadochnayashirina',
//        'trim_by' => 'мм'
//    ],
];
function check_posdiam_x($opts_string)
{

    $possible_symbols = [
        'x',
        'х',
        'X',
        'Х'
    ];
    foreach ($possible_symbols as $possible_symbol) {

        if (strpos($opts_string, $possible_symbol)) {
            return $possible_symbol;
        }
    }

    return $opts_string;
}

;
function get_posdiam_fittings($opts_string)
{
    $new_opts = [];
    $symbol_x = check_posdiam_x($opts_string);
    $posadochnayashirina = trim_string_after_first($opts_string, $symbol_x, false);
    $posd = preg_replace('~[^0-9.]+~', '', trim($posadochnayashirina));

    if ($posd !== null && strlen($posd < 2)) {

        $new_opts[0] = [
            'name' => 'Посадочная ширина',
//            'slug' => Create_Slug('ширина'),
            'id' => 11,
            'url' => 'posadochnayashirina',
            'trim_by' => 'мм',
            'string_value' => $posd,
            'value' => $posd,
        ];

    }

    $fittings = preg_replace('~[^0-9.]+~', '', replace_phrase_from($posadochnayashirina, $opts_string));

    if ($fittings !== null) {

        $new_opts[1] = [
            'name' => 'Количество крепежных отверстий',
//            'slug' => Create_Slug('Количество крепежных отверстий'),
            'id' => 12,
            'url' => 'kolichestvokrepezhnyhotverstij',
            'trim_by' => 'х',
            'string_value' => $fittings,
            'value' => $fittings,
        ];

    }

    return $new_opts;
}

;

function get_additional_option($string)
{
    global $disc_add_opt_register;
    $str_value = null;

    foreach ($disc_add_opt_register as $key => $opt) {

        $strtp = strpos($string, $key);


        if ($strtp !== false) {
            $trim_part = substr($string, $strtp);
            $str_value = replace_phrase_from(trim_string_after_first($trim_part, $opt['trim_by']), $trim_part);
        }

    };

    if ($str_value !== null) {

        return get_posdiam_fittings($str_value);

    } else {
        return null;
    }

}

function get_options($string, $opt_register, $additional_string)
{

//    "литые алюминиевые, 17'', ширина обода: 7.5'', PCD 110 мм, DIA 65.1 мм, ET 35 мм"
//    print_r($string);
    $opts_sort = [];
    $extended_length = 0;
    $extended = null;
    $first_part = null;

    foreach ($opt_register as $key => $opt) {

        $strtp = strpos($string, $key);

        if ($strtp !== false) {

            $trim_part = substr($string, $strtp);
            $trim_part_length = strlen($trim_part);
            $str_value = replace_phrase_from(trim_string_after_first($trim_part, $opt['trim_by']), $trim_part);
            $num_value = preg_replace('~[^0-9.]+~', '', trim($str_value));

            $opt = [
                'name' => $key,
                'id' => $opt['id'],
                'category' => $opt,
                'strtp' => $strtp,
                'str_value' => $str_value,
                'num_value' => $num_value,
                'value' => $num_value . ' мм'
            ];

            array_push($opts_sort, $opt);
            if ($trim_part_length > $extended_length) {
                $extended_length = $trim_part_length;
                $extended = $trim_part;
            }
        }
    };

    $opts_to_add = get_additional_option(replace_phrase_from($extended, $additional_string));

    if ($opts_to_add !== null) {
        foreach ($opts_to_add as $opt) {
            array_push($opts_sort, $opt);
        }
    }

    return $opts_sort;
}


function setDBData($fetched_outer)
{
    global $counter_saved;
    global $disc_opt_register;
    global $disc_add_opt_register;


    if (isset($fetched_outer['prices'])) {
        $new_product_price_min = $fetched_outer['prices']['price_min']['amount'];

        $new_product_price_max = $fetched_outer['prices']['price_max']['amount'];
    } else {
        $new_product_price_min = 0;

        $new_product_price_max = 0;
    }

    $descriptions = $fetched_outer['description'];

    $Products = new Products();

    $Variants = new Variants();
    $Lang_Products = new Lang_Products();
    $product_cat = new Products_Categories();


    $new_product_name = replace_phrase_from_with('&quot;',$fetched_outer['name'],'"');
    $new_product_cat_id = 26;//Диски
    $new_product_url = $fetched_outer['key'];


    $new_product_currency_name = $fetched_outer['prices']['price_max']['currency'];
    $new_product_currency_id = 2;
    $brand_name_original = get_first_word($fetched_outer['full_name']);
    $brand_url = Create_Slug($brand_name_original);


    $Products->name = $new_product_name;
    $Products->url = $new_product_url;
    $Products->annotation = $new_product_name;
    $Products->meta_title = $new_product_name;
    $Products->meta_keywords = $new_product_name;
    $Products->save();
    $product_id = $Products->id;

    if (Brands::where('name', $brand_name_original)->first()) {
        $brand = Brands::where('name', $brand_name_original)->first();

        $Products->brand_id = $brand['id'];
        $Products = Products::where('id', $product_id)->first();
        $Products->save();


    } else {
        //        lang_id lang_label brand_id name meta_title meta_keywords meta_description annotation
        echo '<br> Создаем новый бренд: ' . $brand_name_original . '<br>';

        $le_brand = new Brands();

        $le_brand->name = $brand_name_original;
        $le_brand->url = $brand_url;
        $le_brand->image = $brand_url;
        $le_brand->meta_title = $brand_name_original;
        $le_brand->meta_keywords = $brand_name_original;
        $le_brand->save();

        $le_brand_lang = new Lang_Brands();
        $le_brand_lang->brand_id = $le_brand->id;
        $le_brand_lang->lang_id = 1;
        $le_brand_lang->name = $brand_url;
        $le_brand_lang->meta_title = $brand_name_original;
        $le_brand_lang->meta_keywords = $brand_name_original;
        $le_brand_lang->save();
        $Products->brand_id = $le_brand['id'];

        $Products = Products::where('id', $product_id)->first();
        $Products->save();

    }


    $le_opciones = get_options($descriptions, $disc_opt_register, $new_product_name);

    set_options($product_id, $le_opciones);

    $new_product_images = save_image($fetched_outer['images']['header'], $product_id);

    $cat_exist = Products_Categories::where('product_id', $product_id)->first();

    if (!isset($cat_exist)) {

        $product_cat->product_id = $product_id;
        $product_cat->category_id = $new_product_cat_id;
        $product_cat->position = 2;
        $product_cat->save();
    } else {
        echo '<br> В категориях продуктов у данного продукта уже есть категория';
    }

    $Variants->product_id = $product_id;
    $Variants->price = $new_product_price_min;
    if ($new_product_price_min < $new_product_price_max) {
        $Variants->compare_price = $new_product_price_max;
    }
    $Variants->currency_id = $new_product_currency_id;
    $Variants->save();


    $lang_prod_exists = Lang_Products::where(['lang_id' => 1, 'product_id' => $product_id])->first();

    if ($lang_prod_exists !== null) {
        $lang_prod_exists->delete();
        echo '<br> $lang_prod_exists deleted';
    }


    $Lang_Products->product_id = $product_id;
    $Lang_Products->meta_title = $new_product_name;
    $Lang_Products->meta_keywords = $new_product_name;
    $Lang_Products->lang_id = 1;//Русский
    $Lang_Products->name = $new_product_name;
    $Lang_Products->save();


    $counter_saved++;

}


function loopAndSetDbData()
{

    global $fetched_outer;
    global $counter_saved;
    global $amount;

    for ($i = $counter_saved; $i < $amount; $i++) {

        setDBData($fetched_outer[$i]);
        echo '<br> ' . $counter_saved . ' ';
    }

    unset($fetched_products);
}

loopAndSetDbData();
