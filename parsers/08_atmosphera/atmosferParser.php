<?php

class db 
{
		
	private static $_instance = null;
	
	
	static public function getInstance() 
	{
		if(is_null(self::$_instance))
		{
			self::$_instance = new mysqli(
				'localhost',
				'atmosfer_site',
				'bobn1bobn',
				'atmosfer_db'
			);
			if (self::$_instance->connect_errno) {
				die('Connect Error: ' . $mysqli->connect_error);
			}
			/* изменение набора символов на utf8 */
			if (!self::$_instance->set_charset("utf8")) {
				die('Set charset Error: ' . $mysqli->connect_error); 
			}
		}
		return self::$_instance;
	}

}


class atmosferParser
{
	
	private $db;
	private $brand_id = 15;
	private $brand_name = 'Joseph Joseph';
	private $ymlUrl = 'http://mersena.by/bitrix/catalog_export/dom.php';
	private $imgPath = '/home/atmosfer/public_html/files/products';
	private $useragent = 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36';
	private $products = array();
	private $categories = array();
	
	
	function __construct()
	{
		$this->db = db::getInstance();
	}
	
	
	public function run()
	{
		if( $ymlFile = file_get_contents($this->ymlUrl) ){
			$yml = new SimpleXMLElement($ymlFile);
			if(!$yml){
				return false;
			}
			$category = $this->getCategories($yml->shop->categories->category);	
			
			$this->deleteVariants();
			
			$products=array();
			$i=0;
			foreach($yml->shop->offers->offer as $p){
				$attr = $p->attributes();
				$available = (boolean)$attr->available;
				$brand_name = $this->getBrandName($p->param);				
				if($available and $brand_name===$this->brand_name){
					$i++;
				
					$product = $this->getParams($p);
				
					$this->import($product, $category);
					
					//var_dump($product);
				}
			}
			
			echo $i;
			
		}		
	}
	
	protected function deleteVariants()
	{
		$query = "DELETE s_variants.* FROM s_variants LEFT JOIN s_products ON s_variants.product_id=s_products.id WHERE s_products.brand_id={$this->brand_id}";
		$query = $this->db->query($query);
		if(!$query){
			echo __LINE__.' '.__FUNCTION__." Error : ({$this->db->errno}) {$this->db->error}";
		}
		
	}
	
	protected function exists($p)
	{
		$name = $this->db->real_escape_string($p['name']);
		$query = "SELECT s_products.id FROM s_products WHERE s_products.name = '{$name}'";
		$query = $this->db->query($query);
		if($query){
			if(mysqli_num_rows($query) > 0){
				$row = $query->fetch_assoc();
				$this->products[$row['id']] = $p['name'];
				return true;
			}
		}else{
			echo __LINE__.' '.__FUNCTION__." Error: ({$this->db->errno}) {$this->db->error}";
		}
		
		return false;
	}
		
	
	protected function update($p)
	{
		$id = array_search($p['name'], $this->products);
		$price = $p['price'];
		$p = $this->prepare($p);
		$update = array();
		foreach($p as $k=>$v){
			if(is_string($v)){
				$v =  $this->db->real_escape_string($v);
				$v =  "'{$v}'";
			}			
			$update[] = "{$k}={$v}";
		}
		$update = implode(', ',$update);
		$query = "UPDATE s_products SET {$update} WHERE id = {$id}";
		$query = $this->db->query($query);
		if($query){
			$query = "INSERT INTO s_variants (product_id, sku, name, price, position, attachment, external_id) VALUES ({$id}, '', '', {$price}, 0, '', '')";
			$query = $this->db->query($query);
			if(!$query){
				echo __LINE__.' '.__FUNCTION__." s_variants Error: ({$this->db->errno}) {$this->db->error}";
			}
		}else{
			echo __LINE__.' '.__FUNCTION__." Error: ({$this->db->errno}) {$this->db->error}";
		}
		
		echo __FUNCTION__." {$p['name']}\n";
	}
	
	
	protected function insert($p, $category)
	{
		$p = $this->getMoreParams($p);
		$articul = $p['articul'];
		$cname = $category[$p['categoryId']]['name'];
		$parent = $category[$p['categoryId']]['parentId'];
		$parent = $category[$parent]['name'];
		$price = $p['price'];
		$imgs = $p['imgs'];
		$p = $this->prepare($p, true);
		foreach($p as &$v){
			if(is_string($v)){
				$v =  $this->db->real_escape_string($v);
				$v =  "'{$v}'";
			}
		}
		$p['position'] = '(SELECT MAX(p.id) FROM s_products as p)+1';
		$fields = implode(', ', array_keys($p));
		$values = implode(', ', array_values($p));
		
		$query = "INSERT INTO s_products ({$fields}) VALUES ({$values})";
		$query = $this->db->query($query);
		if($query){
			$id = $this->db->insert_id;
			$category_id = $this->getIdCategory($cname, $parent);
			
			$query = "INSERT INTO s_variants (product_id, sku, name, price, position, attachment, external_id) VALUES ({$id}, '', '', {$price}, 0, '', '')";
			$query = $this->db->query($query);
			if(!$query){
				echo __LINE__.' '.__FUNCTION__." s_variants Error: ({$this->db->errno}) {$this->db->error}";
			}
			
			$query = "INSERT INTO s_options (product_id, feature_id, value) VALUES ({$id}, 168, '{$articul}')";
			$query = $this->db->query($query);
			if(!$query){
				echo __LINE__.' '.__FUNCTION__." s_options Error: ({$this->db->errno}) {$this->db->error}";
			}

			$query = "INSERT INTO s_products_categories (product_id, category_id) VALUES ({$id}, {$category_id})";
			$query = $this->db->query($query);
			if(!$query){
				echo __LINE__.' '.__FUNCTION__." s_products_categories Error: ({$this->db->errno}) {$this->db->error}";
			}
			
			foreach($imgs as $img){
				echo "{$img}\n";
				$imgName = md5($img).'.jpg';
				$this->saveImage($img, 100);
				$this->saveImage($img, 150);
				$this->saveImage($img, 270);
				$this->saveImage($img, 320);
				$this->saveImage($img, 1853);
				$this->saveImage($img, 400);

				$query = "INSERT INTO s_images (name, product_id, filename, position) VALUES ('', {$id}, '{$imgName}', 0)";
				$query = $this->db->query($query);
				if(!$query){
					echo __LINE__.' '.__FUNCTION__." s_images Error: ({$this->db->errno}) {$this->db->error}";
				}
			}
			
		}else{
			echo __LINE__.' '.__FUNCTION__." Error: ({$this->db->errno}) {$this->db->error}";
		}
		echo __FUNCTION__." {$p['name']}\n";
	}
	
	
	protected function saveImage($url, $width)
	{
		$d = 'http:';
		$sizeName = array(
			100 =>'100x100',
			150 =>'150x191',
			270 =>'270x200',
			320 =>'320x370',
			1853 =>'1853x1105',
			400 =>'400x400'
		);	
		$imgName = md5($url).".{$sizeName[$width]}.jpg";
		$imgFile = file_get_contents($d.$url);
		if( $img = imagecreatefromstring($imgFile) ){		
			$w = imagesx($img);
			$h = imagesy($img);		
			$height = ($h*$width)/$w;	
			$n_img = imagecreatetruecolor($width, $height);
			imagecopyresampled($n_img, $img, 0, 0, 0, 0, $width, $height, $w, $h);
			imagejpeg($n_img, '/home/atmosfer/public_html/files/products/'.$imgName);
			imagedestroy($n_img);
			imagedestroy($img);				
		}
	}
	
	
	protected function getIdCategory($cname, $parent=false)
	{
		if( isset($his->categories[$cname]) ){
			return $his->categories[$cname];
		}
		
		$parentId = 1;
		if($parent){
			$parentId = $this->getIdCategory($parent);
		}
		
		$query = "SELECT * FROM s_categories WHERE `name`='{$cname}' AND `parent_id`={$parentId}";
		$query = $this->db->query($query);
		if($query){
			if(mysqli_num_rows($query) > 0){
				$row = $query->fetch_assoc();
				$id = $row['id'];
			}else{
				$slug = $this->getSlug($cname);
				$query = "INSERT INTO s_categories (parent_id,name, meta_title,meta_keywords,meta_description,description,url,external_id,наше_поле) VALUES ({$parentId},'{$cname}','{$cname}', '', '', '','{$slug}', '', 0)";
				$query = $this->db->query($query);
				if($query){
					$id = $this->db->insert_id;
				}else{
					echo __LINE__.' '.__FUNCTION__." Error: ({$this->db->errno}) {$this->db->error}";
				}
			}
			
			$his->categories[$cname] = $id;
			return $id;

		}else{
			echo __LINE__.' '.__FUNCTION__." Error: ({$this->db->errno}) {$this->db->error}";
		}
		
		return false;
	}
	
	
	protected function prepare($p, $insert=false)
	{
		$product = array();
		if($insert){
			$product['url'] = $this->getSlug($p['name'], 's_products');
			$product['brand_id'] = $this->brand_id;
		}
		$product['name'] = $p['name'];
		
		if($insert){
			$product['annotation'] = '';
			if(isset($p['razmer'])){
				$product['annotation'] .= '<p><strong>Размер:</strong></p>';
				$product['annotation'] .= "<p>{$p['razmer']}</p>";
			}
			
			$product['body'] = '';
			if(isset($p['material'])){
				$product['body'] .= '<p><strong>Материал:</strong></p>';
				$product['body'] .= "<p>{$p['material']}</p><br/>";
			}
			if(isset($p['razmer'])){
				$product['body'] .= '<p><strong>Размер:</strong></p>';
				$product['body'] .= "<p>{$p['razmer']}</p><br/>";
			}
			if(isset($p['color'])){
				$product['body'] .= '<p><strong>Цвет:</strong></p>';
				$product['body'] .= "<p>{$p['color']}</p><br/>";
			}
			if(isset($p['description'])){
				$product['body'] .= '<p><strong>Описание:</strong></p>';
				$product['body'] .= "<p>{$p['description']}</p><br/>";			
			}
		}
		$product['visible'] = 1;
		if($insert){
			$product['meta_title'] = $p['name'];
			$product['meta_keywords'] = '';
			$product['meta_description'] = $p['m_desc'];
			$product['created'] = date('Y-m-d H:i:s');
		}
		
		return $product;
	}
	
	
	protected function import($product, $category)
	{
		if($this->exists($product)){
			$this->update($product);
		}else{
			$this->insert($product, $category);
		}
	}
	
	
	protected function getParams($p)
	{	
		$product = array();
		$product['name'] = (string)$p->name;
		$product['m_desc'] = (string)$p->description;
		$product['categoryId'] = (int)$p->categoryId;
		//$product['picture'][] = (string)$p->picture;
		$product['price'] = (int)$p->price*10000;
		$product['url'] = (string)$p->url;		
		
		return $product;
	}
	
	
	protected function getMoreParams($prod)
	{
		if( $page = $this->getPage($prod['url']) ){
			$page = str_replace("\n", "", $page);
			//$page = iconv( "utf-8", "windows-1251", $page);
			
			preg_match_all("/<div.class=\"tovar-description-title\"><span>([^\/]*?)<\/span><\/div>/", $page, $ptitle);
			$ptitle = ($ptitle[1]) ? $ptitle[1] : false;

			preg_match_all("/class=\"tovar-option\">(.*?)<\/span>/", $page, $pval);
			$pval = ($pval[1]) ? $pval[1] : false;

			foreach($ptitle as $k=>$t){
				if($t === 'Артикул' and isset($pval[$k]) ){
					$prod['articul'] = trim($pval[$k]);
				}
				if($t === 'Материал' and isset($pval[$k]) ){
					$prod['material'] = trim($pval[$k]);
				}
				if($t === 'Размер' and isset($pval[$k]) ){
					$prod['razmer'] = trim($pval[$k]);
				}
				if($t === 'Цвет' and isset($pval[$k]) ){
					$prod['color'] = trim($pval[$k]);
				}
			}
			
			preg_match_all("/data-fancybox-group=\"gallery\".href=\"([^>]*?)\">/", $page, $prod['imgs']);
			$prod['imgs'] = ($prod['imgs'][1]) ? $prod['imgs'][1] : false;
			
			preg_match_all("/class=\"tovar-descripion-article\">(.*?)<\/div>[^<]*?<\/div>[^<]*?<div class=\"clear\">/", $page, $desc);
			$prod['description'] = ($desc[1]) ? trim($desc[1][0]) : false;
			if($prod['description']){
				$prod['description'] = preg_replace('/<img.*?>/','', $prod['description']);
			}
			
		}
		
		return $prod;
	}
	
	
	protected function getCategories($categories)
	{
		$category = array(0 => false);
		foreach($categories as $c){
			$attr = $c->attributes();
			$attr->parentId = isset($attr->parentId) ? $attr->parentId : 0;
			$category[(int)$attr->id] = array(
				'name'=>(string)$c, 
				'parentId'=>(int)$attr->parentId
			);
		}
		
		return $category;
	}

	
	protected function getBrandName($params)
	{
		foreach($params as $param){
			$attr = $param->attributes();
			if(!empty($attr->name) 
				and (string)$attr->name==='Производитель')
			{
				return (string)$param;
			}		
		}
		return false;
	}
	
	
	protected function getPage($url, $opts = array())
	{
		$curl = curl_init();
		$opts[CURLOPT_URL] = $url;
		$opts[CURLOPT_RETURNTRANSFER] = true;
		$opts[CURLOPT_FOLLOWLOCATION] = true;			
		$opts[CURLOPT_USERAGENT] = $this->useragent;
		curl_setopt_array($curl, $opts);
		$data = curl_exec($curl);
		$code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		//var_dump($opts);
		print curl_error($curl);
		curl_close($curl);
		echo($code." ".$url."\n");
		if($code === 200)
		{
			return $data;
		}	
		
		return false;
	}

	
	protected function getSlug($str, $table=false) 
	{
		$rus = array('А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я', 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я');
		$lat = array('A', 'B', 'V', 'G', 'D', 'E', 'E', 'Gh', 'Z', 'I', 'Y', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', 'C', 'Ch', 'Sh', 'Sch', 'Y', 'Y', 'Y', 'E', 'Yu', 'Ya', 'a', 'b', 'v', 'g', 'd', 'e', 'e', 'gh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'sch', 'y', 'y', 'y', 'e', 'yu', 'ya');
		$str = str_replace($rus, $lat, $str);

		// в нижний регистр
		$str = strtolower($str);
		// заменям все ненужное нам на "-"
		$str = preg_replace("/[^-a-z0-9_\.]+/u", "-", $str);
		// удаляем начальные и конечные '-'
		$str = trim($str, "-");
		$str = mb_strimwidth($str, 0, 100);
		if($table){
			$query = "SELECT count(*) as p_count FROM `{$table}` WHERE `url` like '{$str}'";
			$query = $this->db->query($query);
			if($query){
				$count = $query->fetch_assoc();
				$count = (int)$count['p_count'];
				if($count>0){
					$str .= $count+1;
				}
			}else{
				echo __LINE__.' '.__FUNCTION__." Error: ({$this->db->errno}) {$this->db->error}";
			}
		}
		
		return $str;
	}
	
}



$parser = new atmosferParser;
$parser->run();
