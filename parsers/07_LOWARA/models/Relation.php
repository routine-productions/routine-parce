<?php

class Relation extends Illuminate\Database\Eloquent\Model
{
    public $timestamps = false;
    protected $table = "wp_term_relationships";
}