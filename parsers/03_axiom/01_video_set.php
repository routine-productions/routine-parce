<?php
/*
 * Get: Person links
 */
define('__HOMEDIR__', __DIR__);
require_once __DIR__ . '/../core.php';

$Videos = B_Iblock_Element::where('IBLOCK_ID', 1)->with('youtube', 'image')->get()->toArray();
//print_r($Videos);


$Video = require_once __DIR__ . '/results/videos.php';
//print_r($Video);

foreach ($Video as $item)
{
    // Save File
    $file = new B_File();
    $file->MODULE_ID = 'iblock';
    $file->SUBDIR = 'iblock/assets';
    $file->CONTENT_TYPE = 'image/jpeg';
    $file->FILE_SIZE = 18136;
    $file->HEIGHT = 255;
    $file->WIDTH = 255;
    $file->FILE_NAME = explode('/', $item['image']['value'])[4];
    $file->ORIGINAL_NAME = explode('/', $item['image']['value'])[4];
    $file->save();


    // Save Object
    $object = new B_Iblock_Element();
    $object->IBLOCK_ID = 1;
    $object->IN_SECTIONS = 'Y';
    if ($item['parent'] == 75)
    {
        $object->IBLOCK_SECTION_ID = 1;
    } else
    {
        $object->IBLOCK_SECTION_ID = 2;
    }
    $file->CODE = $item['alias'];
    $object->NAME = $item['pagetitle'];
    $object->PREVIEW_PICTURE = $file->id;
    $object->save();

    // Section
    $section = new B_Iblock_Section_Element();
    $section->IBLOCK_SECTION_ID = $object->IBLOCK_SECTION_ID;
    $section->IBLOCK_ELEMENT_ID = $object->id;
    $section->save();

    // Extra Field
    $prop = new B_Iblock_Element_Property();
    $prop->IBLOCK_PROPERTY_ID = 2;
    $prop->IBLOCK_ELEMENT_ID = $object->id;
    $prop->VALUE = $item['youtube']['value'];
    $prop->VALUE_TYPE = 'text';
    $prop->save();
}



