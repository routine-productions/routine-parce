<?php
/*
 * Create: Pages 01
 */
define('__HOMEDIR__', __DIR__);
require_once __DIR__ . '/../../core.php';

$Pages = require_once __DIR__ . '/../02_franchise-02/results/03_pages.php';

foreach ($Pages as $Page_Key => $Page)
{
    $Model = Modx_Site_Content::find(5000 + ($Page_Key + 1));

    $Dom->load($Model->content);
    
    if ($Dom->find('img', 0))
    {
        foreach ($Dom->find('img') as $Img)
        {
            $Img->setAttribute('src', '/assets/gallery' . $Img->src);
        }
    }

    $Model->content = $Dom->outerHtml;
    $Model->save();
}
