<?php

define('__HOMEDIR__', __DIR__);

require_once __DIR__ . '/../core.php';

$fetched_outer = include __DIR__ . '/results/all_tires_outer.php';
$fetched_inner = include __DIR__ . '/results/allInnerDom.php';

function get_feature_id($iteration)
{
    switch ($iteration) {
        case 0:
            return 5;//назначение
            break;
        case 1:
            return 6;//сезон
            break;
        case 2:
            return 24;//Ширина профиля
            break;
        case 3:
            return 7; //серия (высота профиля)
            break;
        case 4:
            return 8;//Посадочн диаметр
            break;
        case 5:
            return 6; //тип
            break;
        case 6:
            return 3;//Камера
            break;
        case 7:
            return 17;//макс скорость
            break;
        case 8:
            return 16;//макс нагрузка
            break;
    }
}

function set_options($product_id, $options)
{

//    product_id feature_id   lang_id  value   translit


    for ($i = 0; $i < count($options); $i++) {
        $cur_opt = $options[$i];


        if (isset($cur_opt['description']) && !empty($cur_opt['description'])) {
            $Options = new Options();
            $Options->product_id = $product_id;
            $Options->feature_id = get_feature_id($i);
            $Options->value = $cur_opt['description'];
            print_r(['feature_id' => $Options->product_id, 'option_value' => $Options->value]);
            $Options->save();

        } else {
            continue;
        }
    }

}

function save_images($images,$product_id)
{
    for ($i = 0; $i < count($images); $i++) {
        $img_url = $images[$i]['src_full'];
        $image = new Images();
        $image->product_id = $product_id;
        $image->filename;
        $image->http_original_name;


        $image->save();


    }
}

function setDBData($fetched_outer, $fetched_inner)
{
    $new_product_name = $fetched_outer['name'];
    $new_product_cat_id = 25;//Шины
    $new_product_url = $fetched_outer['key'];
    $new_product_price_min = $fetched_outer['prices']['price_min']['amount'];
    $new_product_price_max = $fetched_outer['prices']['price_max']['amount'];
    $new_product_currency_name = $fetched_outer['prices']['price_max']['currency'];
    $new_product_currency_id = 2;
    $new_product_images = $fetched_inner['images'];


    $brand_name_original = $fetched_inner['category_original'];
    $brand_url = $fetched_inner['category_en'];

    $Products = new Products();
    $le_brand = new Brands();

    $Variants = new Variants();
    $Lang_Products = new Lang_Products();
    $product_cat = new Products_Categories();

    $Products->name = $new_product_name;
    $Products->url = $new_product_url;
    $Products->annotation = $new_product_name;
    $Products->meta_title = $new_product_name;
    $Products->meta_keywords = $new_product_name;

//    $Products->timestamps =  date_timestamp_set(date_create(), 1171502725);

    save_images($new_product_images,$Products->id);


    $Products->save();

//    set_options($Products->id, $fetched_inner['descriptions']);


    $product_cat->product_id = $Products->id;
    $product_cat->category_id = $new_product_cat_id;
    $product_cat->position = 2;
    $product_cat->save();


    $Variants->product_id = $Products->id;
    $Variants->price = $new_product_price_max;
    $Variants->compare_price = $new_product_price_min;
    $Variants->currency_id = $new_product_currency_id;


    $Variants->save();
//    $Variants->delete();


//    $Products->delete();
    $Lang_Products->product_id = $Products->id;
    $Lang_Products->meta_title = $new_product_name;
    $Lang_Products->meta_keywords = $new_product_name;
    $Lang_Products->lang_id = 1;//Русский
    $Lang_Products->name = $new_product_name;
    $Lang_Products->save();


    if (Brands::where('name', $fetched_inner['category_original'])->first()) {
        $brand = Brands::where('name', $fetched_inner['category_original'])->first();
        $Products->brand_id = $brand['id'];

    } else {
        //Создать новую категорию


        $le_brand->name = $brand_name_original;
        $le_brand->url = $brand_url;
        $le_brand->meta_title = $brand_name_original;
        $le_brand->meta_keywords = $brand_name_original;
        $le_brand->save();
    }


}

function loopAndSetDbData()
{

    global $fetched_outer;
    global $fetched_inner;

    for ($i = 0; $i < count($fetched_inner); $i++) {
//    for ($i = 0; $i < 1; $i++) {

        setDBData($fetched_outer[$i], $fetched_inner[$i]);

    }

    unset($fetched_products);
}

loopAndSetDbData();


//$AllProducts = Products::get();
//dd($AllProducts->toArray());

