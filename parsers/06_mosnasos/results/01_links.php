<?php 
    return array (
  0 => 
  array (
    'href' => 'http://www.mos-nasos.ru/catalog/gnom/',
    'title' => 'Насосы ГНОМ',
    'parent_href' => 
    array (
      'link' => 
      array (
        0 => 
        array (
          'href' => 'http://www.mos-nasos.ru/catalog/hitemp-gnoms/',
          'title' => 'Погружные насосы для загрязненной воды ГНОМ типа Т, ТР (до 60°C)',
        ),
        1 => 
        array (
          'href' => 'http://www.mos-nasos.ru/catalog/simplegnoms/',
          'title' => 'Погружные насосы для загрязненной воды ГНОМ (до 35°С)',
        ),
        2 => 
        array (
          'href' => 'http://www.mos-nasos.ru/catalog/biggnoms/',
          'title' => 'Мощные погружные насосы для загрязненной воды ГНОМ 50-50, ГНОМ 100-25, ГНОМ 100-25T, ГНОМ 250-17',
        ),
        3 => 
        array (
          'href' => 'http://www.mos-nasos.ru/catalog/minignoms/',
          'title' => 'Бытовые дренажные погружные насосы МИНИГНОМ и ГНОМ 10-10',
        ),
      ),
    ),
  ),
  1 => 
  array (
    'href' => 'http://www.mos-nasos.ru/catalog/fekalnye/',
    'title' => 'Насосы фекальные и канализационные ЦМФ, ЦМК, НПК',
    'parent_href' => 
    array (
      'link' => 
      array (
        0 => 
        array (
          'href' => 'http://www.mos-nasos.ru/catalog/cmk-cmf-npk/',
          'title' => 'Насосы канализационные и фекальные ЦМК, ЦМФ, НПК',
        ),
        1 => 
        array (
          'href' => 'http://www.mos-nasos.ru/catalog/bigcmf-cmk/',
          'title' => 'Насосы канализационные и фекальные ЦМФ 50-10, ЦМФ 50-25, ЦМФ 100-20, ЦМК 50-40',
        ),
        2 => 
        array (
          'href' => 'http://www.mos-nasos.ru/catalog/cuttingpumps/',
          'title' => 'Режущие фекальные ЦМФ и канализационные ЦМК насосы',
        ),
        3 => 
        array (
          'href' => 'http://www.mos-nasos.ru/catalog/minignoms/',
          'title' => 'Бытовые дренажные погружные насосы МИНИГНОМ и ГНОМ 10-10',
        ),
      ),
    ),
  ),
  2 => 
  array (
    'href' => 'http://www.mos-nasos.ru/fekalnye-nasosy-sm-i-sd/',
    'title' => 'Фекальные насосы СМ и СД',
  ),
  3 => 
  array (
    'href' => 'http://www.mos-nasos.ru/catalog/ecvpump/',
    'title' => 'Насосы ЭЦВ',
  ),
  4 => 
  array (
    'href' => 'http://www.mos-nasos.ru/bytovye-nasosy-bcp/',
    'title' => 'Бытовые насосы БЦПЭ У',
  ),
  5 => 
  array (
    'href' => 'http://www.mos-nasos.ru/nasosy-malysh/',
    'title' => 'Насосы Малыш',
  ),
  6 => 
  array (
    'href' => 'http://www.mos-nasos.ru/catalog/bn2-40/',
    'title' => 'Бытовой самовсасывающий электронасос БН 2-40',
  ),
  7 => 
  array (
    'href' => 'http://www.mos-nasos.ru/catalog/konsolnye/',
    'title' => 'Консольные насосы типа К',
  ),
  8 => 
  array (
    'href' => 'http://www.mos-nasos.ru/konsolno-monoblochnye-nasosy-km/',
    'title' => 'Консольно-моноблочные насосы КМ',
  ),
  9 => 
  array (
    'href' => 'http://www.mos-nasos.ru/catalog/ppkpumps/',
    'title' => 'Песковой насос ППК',
  ),
  10 => 
  array (
    'href' => 'http://www.mos-nasos.ru/catalog/sudovye-nasosy-morskoj-vody/',
    'title' => 'Судовые насосы ЭСН для морской воды',
  ),
  11 => 
  array (
    'href' => 'http://www.mos-nasos.ru/stacionarnaya-ustanovka/',
    'title' => 'Стационарная установка на автоматической трубной муфте',
  ),
  12 => 
  array (
    'href' => 'http://www.mos-nasos.ru/catalog/avtomaticheskie-vyklyuchateli-nasosov/',
    'title' => 'Автоматические выключатели защиты двигателя DEKraft',
  ),
);