<?php
class Modx_Gallery_Album_Items extends Illuminate\Database\Eloquent\Model
{
    public $timestamps = false;
    protected $table = "modx_gallery_album_items";
}